
obj/user/breakpoint:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 08 00 00 00       	call   800039 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	asm volatile("int $3"); //break point
  800036:	cc                   	int3   
}
  800037:	5d                   	pop    %ebp
  800038:	c3                   	ret    

00800039 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800039:	55                   	push   %ebp
  80003a:	89 e5                	mov    %esp,%ebp
  80003c:	57                   	push   %edi
  80003d:	56                   	push   %esi
  80003e:	53                   	push   %ebx
  80003f:	83 ec 0c             	sub    $0xc,%esp
  800042:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800045:	8b 75 0c             	mov    0xc(%ebp),%esi
	// set thisenv to point at our Env structure in envs[].
	// LAB 3: Your code here.
	//thisenv = 0;
	int32_t env_Index1 = (int32_t)sys_getenvid();
  800048:	e8 24 0a 00 00       	call   800a71 <sys_getenvid>
  80004d:	89 c7                	mov    %eax,%edi
	cprintf("printing env_Index1: %d\n", env_Index1);
  80004f:	83 ec 08             	sub    $0x8,%esp
  800052:	50                   	push   %eax
  800053:	68 3c 0d 80 00       	push   $0x800d3c
  800058:	e8 0a 01 00 00       	call   800167 <cprintf>
	int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	cprintf("printing env_Index2: %d\n", env_Index2);
  80005d:	83 c4 08             	add    $0x8,%esp
  800060:	81 e7 ff 03 00 00    	and    $0x3ff,%edi
  800066:	57                   	push   %edi
  800067:	68 55 0d 80 00       	push   $0x800d55
  80006c:	e8 f6 00 00 00       	call   800167 <cprintf>

	thisenv = &envs[ENVX(sys_getenvid())];
  800071:	e8 fb 09 00 00       	call   800a71 <sys_getenvid>
  800076:	25 ff 03 00 00       	and    $0x3ff,%eax
  80007b:	8d 14 00             	lea    (%eax,%eax,1),%edx
  80007e:	01 d0                	add    %edx,%eax
  800080:	c1 e0 05             	shl    $0x5,%eax
  800083:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800088:	a3 04 10 80 00       	mov    %eax,0x801004
	cprintf("Addrs of thisenv (envs[0]) is: %p\n", thisenv);
  80008d:	83 c4 08             	add    $0x8,%esp
  800090:	50                   	push   %eax
  800091:	68 78 0d 80 00       	push   $0x800d78
  800096:	e8 cc 00 00 00       	call   800167 <cprintf>
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80009b:	83 c4 10             	add    $0x10,%esp
  80009e:	85 db                	test   %ebx,%ebx
  8000a0:	7e 07                	jle    8000a9 <libmain+0x70>
		binaryname = argv[0];
  8000a2:	8b 06                	mov    (%esi),%eax
  8000a4:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  8000a9:	83 ec 08             	sub    $0x8,%esp
  8000ac:	56                   	push   %esi
  8000ad:	53                   	push   %ebx
  8000ae:	e8 80 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000b3:	e8 0b 00 00 00       	call   8000c3 <exit>
}
  8000b8:	83 c4 10             	add    $0x10,%esp
  8000bb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8000be:	5b                   	pop    %ebx
  8000bf:	5e                   	pop    %esi
  8000c0:	5f                   	pop    %edi
  8000c1:	5d                   	pop    %ebp
  8000c2:	c3                   	ret    

008000c3 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000c3:	55                   	push   %ebp
  8000c4:	89 e5                	mov    %esp,%ebp
  8000c6:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000c9:	6a 00                	push   $0x0
  8000cb:	e8 60 09 00 00       	call   800a30 <sys_env_destroy>
}
  8000d0:	83 c4 10             	add    $0x10,%esp
  8000d3:	c9                   	leave  
  8000d4:	c3                   	ret    

008000d5 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000d5:	55                   	push   %ebp
  8000d6:	89 e5                	mov    %esp,%ebp
  8000d8:	53                   	push   %ebx
  8000d9:	83 ec 04             	sub    $0x4,%esp
  8000dc:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000df:	8b 13                	mov    (%ebx),%edx
  8000e1:	8d 42 01             	lea    0x1(%edx),%eax
  8000e4:	89 03                	mov    %eax,(%ebx)
  8000e6:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000e9:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8000ed:	3d ff 00 00 00       	cmp    $0xff,%eax
  8000f2:	75 1a                	jne    80010e <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8000f4:	83 ec 08             	sub    $0x8,%esp
  8000f7:	68 ff 00 00 00       	push   $0xff
  8000fc:	8d 43 08             	lea    0x8(%ebx),%eax
  8000ff:	50                   	push   %eax
  800100:	e8 ee 08 00 00       	call   8009f3 <sys_cputs>
		b->idx = 0;
  800105:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  80010b:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  80010e:	ff 43 04             	incl   0x4(%ebx)
}
  800111:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800114:	c9                   	leave  
  800115:	c3                   	ret    

00800116 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800116:	55                   	push   %ebp
  800117:	89 e5                	mov    %esp,%ebp
  800119:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  80011f:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800126:	00 00 00 
	b.cnt = 0;
  800129:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800130:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800133:	ff 75 0c             	pushl  0xc(%ebp)
  800136:	ff 75 08             	pushl  0x8(%ebp)
  800139:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80013f:	50                   	push   %eax
  800140:	68 d5 00 80 00       	push   $0x8000d5
  800145:	e8 51 01 00 00       	call   80029b <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80014a:	83 c4 08             	add    $0x8,%esp
  80014d:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800153:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800159:	50                   	push   %eax
  80015a:	e8 94 08 00 00       	call   8009f3 <sys_cputs>

	return b.cnt;
}
  80015f:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800165:	c9                   	leave  
  800166:	c3                   	ret    

00800167 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800167:	55                   	push   %ebp
  800168:	89 e5                	mov    %esp,%ebp
  80016a:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80016d:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800170:	50                   	push   %eax
  800171:	ff 75 08             	pushl  0x8(%ebp)
  800174:	e8 9d ff ff ff       	call   800116 <vcprintf>
	va_end(ap);

	return cnt;
}
  800179:	c9                   	leave  
  80017a:	c3                   	ret    

0080017b <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80017b:	55                   	push   %ebp
  80017c:	89 e5                	mov    %esp,%ebp
  80017e:	57                   	push   %edi
  80017f:	56                   	push   %esi
  800180:	53                   	push   %ebx
  800181:	83 ec 1c             	sub    $0x1c,%esp
  800184:	89 c7                	mov    %eax,%edi
  800186:	89 d6                	mov    %edx,%esi
  800188:	8b 45 08             	mov    0x8(%ebp),%eax
  80018b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80018e:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800191:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800194:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800197:	bb 00 00 00 00       	mov    $0x0,%ebx
  80019c:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  80019f:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001a2:	39 d3                	cmp    %edx,%ebx
  8001a4:	72 05                	jb     8001ab <printnum+0x30>
  8001a6:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001a9:	77 45                	ja     8001f0 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001ab:	83 ec 0c             	sub    $0xc,%esp
  8001ae:	ff 75 18             	pushl  0x18(%ebp)
  8001b1:	8b 45 14             	mov    0x14(%ebp),%eax
  8001b4:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001b7:	53                   	push   %ebx
  8001b8:	ff 75 10             	pushl  0x10(%ebp)
  8001bb:	83 ec 08             	sub    $0x8,%esp
  8001be:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001c1:	ff 75 e0             	pushl  -0x20(%ebp)
  8001c4:	ff 75 dc             	pushl  -0x24(%ebp)
  8001c7:	ff 75 d8             	pushl  -0x28(%ebp)
  8001ca:	e8 09 09 00 00       	call   800ad8 <__udivdi3>
  8001cf:	83 c4 18             	add    $0x18,%esp
  8001d2:	52                   	push   %edx
  8001d3:	50                   	push   %eax
  8001d4:	89 f2                	mov    %esi,%edx
  8001d6:	89 f8                	mov    %edi,%eax
  8001d8:	e8 9e ff ff ff       	call   80017b <printnum>
  8001dd:	83 c4 20             	add    $0x20,%esp
  8001e0:	eb 16                	jmp    8001f8 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001e2:	83 ec 08             	sub    $0x8,%esp
  8001e5:	56                   	push   %esi
  8001e6:	ff 75 18             	pushl  0x18(%ebp)
  8001e9:	ff d7                	call   *%edi
  8001eb:	83 c4 10             	add    $0x10,%esp
  8001ee:	eb 03                	jmp    8001f3 <printnum+0x78>
  8001f0:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8001f3:	4b                   	dec    %ebx
  8001f4:	85 db                	test   %ebx,%ebx
  8001f6:	7f ea                	jg     8001e2 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8001f8:	83 ec 08             	sub    $0x8,%esp
  8001fb:	56                   	push   %esi
  8001fc:	83 ec 04             	sub    $0x4,%esp
  8001ff:	ff 75 e4             	pushl  -0x1c(%ebp)
  800202:	ff 75 e0             	pushl  -0x20(%ebp)
  800205:	ff 75 dc             	pushl  -0x24(%ebp)
  800208:	ff 75 d8             	pushl  -0x28(%ebp)
  80020b:	e8 d8 09 00 00       	call   800be8 <__umoddi3>
  800210:	83 c4 14             	add    $0x14,%esp
  800213:	0f be 80 9b 0d 80 00 	movsbl 0x800d9b(%eax),%eax
  80021a:	50                   	push   %eax
  80021b:	ff d7                	call   *%edi
}
  80021d:	83 c4 10             	add    $0x10,%esp
  800220:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800223:	5b                   	pop    %ebx
  800224:	5e                   	pop    %esi
  800225:	5f                   	pop    %edi
  800226:	5d                   	pop    %ebp
  800227:	c3                   	ret    

00800228 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800228:	55                   	push   %ebp
  800229:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80022b:	83 fa 01             	cmp    $0x1,%edx
  80022e:	7e 0e                	jle    80023e <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800230:	8b 10                	mov    (%eax),%edx
  800232:	8d 4a 08             	lea    0x8(%edx),%ecx
  800235:	89 08                	mov    %ecx,(%eax)
  800237:	8b 02                	mov    (%edx),%eax
  800239:	8b 52 04             	mov    0x4(%edx),%edx
  80023c:	eb 22                	jmp    800260 <getuint+0x38>
	else if (lflag)
  80023e:	85 d2                	test   %edx,%edx
  800240:	74 10                	je     800252 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800242:	8b 10                	mov    (%eax),%edx
  800244:	8d 4a 04             	lea    0x4(%edx),%ecx
  800247:	89 08                	mov    %ecx,(%eax)
  800249:	8b 02                	mov    (%edx),%eax
  80024b:	ba 00 00 00 00       	mov    $0x0,%edx
  800250:	eb 0e                	jmp    800260 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800252:	8b 10                	mov    (%eax),%edx
  800254:	8d 4a 04             	lea    0x4(%edx),%ecx
  800257:	89 08                	mov    %ecx,(%eax)
  800259:	8b 02                	mov    (%edx),%eax
  80025b:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800260:	5d                   	pop    %ebp
  800261:	c3                   	ret    

00800262 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800262:	55                   	push   %ebp
  800263:	89 e5                	mov    %esp,%ebp
  800265:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800268:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80026b:	8b 10                	mov    (%eax),%edx
  80026d:	3b 50 04             	cmp    0x4(%eax),%edx
  800270:	73 0a                	jae    80027c <sprintputch+0x1a>
		*b->buf++ = ch;
  800272:	8d 4a 01             	lea    0x1(%edx),%ecx
  800275:	89 08                	mov    %ecx,(%eax)
  800277:	8b 45 08             	mov    0x8(%ebp),%eax
  80027a:	88 02                	mov    %al,(%edx)
}
  80027c:	5d                   	pop    %ebp
  80027d:	c3                   	ret    

0080027e <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80027e:	55                   	push   %ebp
  80027f:	89 e5                	mov    %esp,%ebp
  800281:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800284:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800287:	50                   	push   %eax
  800288:	ff 75 10             	pushl  0x10(%ebp)
  80028b:	ff 75 0c             	pushl  0xc(%ebp)
  80028e:	ff 75 08             	pushl  0x8(%ebp)
  800291:	e8 05 00 00 00       	call   80029b <vprintfmt>
	va_end(ap);
}
  800296:	83 c4 10             	add    $0x10,%esp
  800299:	c9                   	leave  
  80029a:	c3                   	ret    

0080029b <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80029b:	55                   	push   %ebp
  80029c:	89 e5                	mov    %esp,%ebp
  80029e:	57                   	push   %edi
  80029f:	56                   	push   %esi
  8002a0:	53                   	push   %ebx
  8002a1:	83 ec 2c             	sub    $0x2c,%esp
  8002a4:	8b 75 08             	mov    0x8(%ebp),%esi
  8002a7:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002aa:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002ad:	eb 12                	jmp    8002c1 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002af:	85 c0                	test   %eax,%eax
  8002b1:	0f 84 68 03 00 00    	je     80061f <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002b7:	83 ec 08             	sub    $0x8,%esp
  8002ba:	53                   	push   %ebx
  8002bb:	50                   	push   %eax
  8002bc:	ff d6                	call   *%esi
  8002be:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002c1:	47                   	inc    %edi
  8002c2:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002c6:	83 f8 25             	cmp    $0x25,%eax
  8002c9:	75 e4                	jne    8002af <vprintfmt+0x14>
  8002cb:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002cf:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002d6:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002dd:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002e4:	ba 00 00 00 00       	mov    $0x0,%edx
  8002e9:	eb 07                	jmp    8002f2 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002eb:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8002ee:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002f2:	8d 47 01             	lea    0x1(%edi),%eax
  8002f5:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8002f8:	0f b6 0f             	movzbl (%edi),%ecx
  8002fb:	8a 07                	mov    (%edi),%al
  8002fd:	83 e8 23             	sub    $0x23,%eax
  800300:	3c 55                	cmp    $0x55,%al
  800302:	0f 87 fe 02 00 00    	ja     800606 <vprintfmt+0x36b>
  800308:	0f b6 c0             	movzbl %al,%eax
  80030b:	ff 24 85 28 0e 80 00 	jmp    *0x800e28(,%eax,4)
  800312:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800315:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800319:	eb d7                	jmp    8002f2 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80031b:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80031e:	b8 00 00 00 00       	mov    $0x0,%eax
  800323:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800326:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800329:	01 c0                	add    %eax,%eax
  80032b:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80032f:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800332:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800335:	83 fa 09             	cmp    $0x9,%edx
  800338:	77 34                	ja     80036e <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80033a:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80033b:	eb e9                	jmp    800326 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80033d:	8b 45 14             	mov    0x14(%ebp),%eax
  800340:	8d 48 04             	lea    0x4(%eax),%ecx
  800343:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800346:	8b 00                	mov    (%eax),%eax
  800348:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80034b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80034e:	eb 24                	jmp    800374 <vprintfmt+0xd9>
  800350:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800354:	79 07                	jns    80035d <vprintfmt+0xc2>
  800356:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80035d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800360:	eb 90                	jmp    8002f2 <vprintfmt+0x57>
  800362:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800365:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80036c:	eb 84                	jmp    8002f2 <vprintfmt+0x57>
  80036e:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800371:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800374:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800378:	0f 89 74 ff ff ff    	jns    8002f2 <vprintfmt+0x57>
				width = precision, precision = -1;
  80037e:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800381:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800384:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80038b:	e9 62 ff ff ff       	jmp    8002f2 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800390:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800391:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800394:	e9 59 ff ff ff       	jmp    8002f2 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800399:	8b 45 14             	mov    0x14(%ebp),%eax
  80039c:	8d 50 04             	lea    0x4(%eax),%edx
  80039f:	89 55 14             	mov    %edx,0x14(%ebp)
  8003a2:	83 ec 08             	sub    $0x8,%esp
  8003a5:	53                   	push   %ebx
  8003a6:	ff 30                	pushl  (%eax)
  8003a8:	ff d6                	call   *%esi
			break;
  8003aa:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ad:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003b0:	e9 0c ff ff ff       	jmp    8002c1 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003b5:	8b 45 14             	mov    0x14(%ebp),%eax
  8003b8:	8d 50 04             	lea    0x4(%eax),%edx
  8003bb:	89 55 14             	mov    %edx,0x14(%ebp)
  8003be:	8b 00                	mov    (%eax),%eax
  8003c0:	85 c0                	test   %eax,%eax
  8003c2:	79 02                	jns    8003c6 <vprintfmt+0x12b>
  8003c4:	f7 d8                	neg    %eax
  8003c6:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003c8:	83 f8 06             	cmp    $0x6,%eax
  8003cb:	7f 0b                	jg     8003d8 <vprintfmt+0x13d>
  8003cd:	8b 04 85 80 0f 80 00 	mov    0x800f80(,%eax,4),%eax
  8003d4:	85 c0                	test   %eax,%eax
  8003d6:	75 18                	jne    8003f0 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003d8:	52                   	push   %edx
  8003d9:	68 b3 0d 80 00       	push   $0x800db3
  8003de:	53                   	push   %ebx
  8003df:	56                   	push   %esi
  8003e0:	e8 99 fe ff ff       	call   80027e <printfmt>
  8003e5:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003e8:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8003eb:	e9 d1 fe ff ff       	jmp    8002c1 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8003f0:	50                   	push   %eax
  8003f1:	68 bc 0d 80 00       	push   $0x800dbc
  8003f6:	53                   	push   %ebx
  8003f7:	56                   	push   %esi
  8003f8:	e8 81 fe ff ff       	call   80027e <printfmt>
  8003fd:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800400:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800403:	e9 b9 fe ff ff       	jmp    8002c1 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800408:	8b 45 14             	mov    0x14(%ebp),%eax
  80040b:	8d 50 04             	lea    0x4(%eax),%edx
  80040e:	89 55 14             	mov    %edx,0x14(%ebp)
  800411:	8b 38                	mov    (%eax),%edi
  800413:	85 ff                	test   %edi,%edi
  800415:	75 05                	jne    80041c <vprintfmt+0x181>
				p = "(null)";
  800417:	bf ac 0d 80 00       	mov    $0x800dac,%edi
			if (width > 0 && padc != '-')
  80041c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800420:	0f 8e 90 00 00 00    	jle    8004b6 <vprintfmt+0x21b>
  800426:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80042a:	0f 84 8e 00 00 00    	je     8004be <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800430:	83 ec 08             	sub    $0x8,%esp
  800433:	ff 75 d0             	pushl  -0x30(%ebp)
  800436:	57                   	push   %edi
  800437:	e8 70 02 00 00       	call   8006ac <strnlen>
  80043c:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  80043f:	29 c1                	sub    %eax,%ecx
  800441:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800444:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800447:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80044b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80044e:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800451:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800453:	eb 0d                	jmp    800462 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800455:	83 ec 08             	sub    $0x8,%esp
  800458:	53                   	push   %ebx
  800459:	ff 75 e4             	pushl  -0x1c(%ebp)
  80045c:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80045e:	4f                   	dec    %edi
  80045f:	83 c4 10             	add    $0x10,%esp
  800462:	85 ff                	test   %edi,%edi
  800464:	7f ef                	jg     800455 <vprintfmt+0x1ba>
  800466:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800469:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80046c:	89 c8                	mov    %ecx,%eax
  80046e:	85 c9                	test   %ecx,%ecx
  800470:	79 05                	jns    800477 <vprintfmt+0x1dc>
  800472:	b8 00 00 00 00       	mov    $0x0,%eax
  800477:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80047a:	29 c1                	sub    %eax,%ecx
  80047c:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  80047f:	89 75 08             	mov    %esi,0x8(%ebp)
  800482:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800485:	eb 3d                	jmp    8004c4 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800487:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80048b:	74 19                	je     8004a6 <vprintfmt+0x20b>
  80048d:	0f be c0             	movsbl %al,%eax
  800490:	83 e8 20             	sub    $0x20,%eax
  800493:	83 f8 5e             	cmp    $0x5e,%eax
  800496:	76 0e                	jbe    8004a6 <vprintfmt+0x20b>
					putch('?', putdat);
  800498:	83 ec 08             	sub    $0x8,%esp
  80049b:	53                   	push   %ebx
  80049c:	6a 3f                	push   $0x3f
  80049e:	ff 55 08             	call   *0x8(%ebp)
  8004a1:	83 c4 10             	add    $0x10,%esp
  8004a4:	eb 0b                	jmp    8004b1 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004a6:	83 ec 08             	sub    $0x8,%esp
  8004a9:	53                   	push   %ebx
  8004aa:	52                   	push   %edx
  8004ab:	ff 55 08             	call   *0x8(%ebp)
  8004ae:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004b1:	ff 4d e4             	decl   -0x1c(%ebp)
  8004b4:	eb 0e                	jmp    8004c4 <vprintfmt+0x229>
  8004b6:	89 75 08             	mov    %esi,0x8(%ebp)
  8004b9:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004bc:	eb 06                	jmp    8004c4 <vprintfmt+0x229>
  8004be:	89 75 08             	mov    %esi,0x8(%ebp)
  8004c1:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004c4:	47                   	inc    %edi
  8004c5:	8a 47 ff             	mov    -0x1(%edi),%al
  8004c8:	0f be d0             	movsbl %al,%edx
  8004cb:	85 d2                	test   %edx,%edx
  8004cd:	74 1d                	je     8004ec <vprintfmt+0x251>
  8004cf:	85 f6                	test   %esi,%esi
  8004d1:	78 b4                	js     800487 <vprintfmt+0x1ec>
  8004d3:	4e                   	dec    %esi
  8004d4:	79 b1                	jns    800487 <vprintfmt+0x1ec>
  8004d6:	8b 75 08             	mov    0x8(%ebp),%esi
  8004d9:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004dc:	eb 14                	jmp    8004f2 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004de:	83 ec 08             	sub    $0x8,%esp
  8004e1:	53                   	push   %ebx
  8004e2:	6a 20                	push   $0x20
  8004e4:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004e6:	4f                   	dec    %edi
  8004e7:	83 c4 10             	add    $0x10,%esp
  8004ea:	eb 06                	jmp    8004f2 <vprintfmt+0x257>
  8004ec:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004ef:	8b 75 08             	mov    0x8(%ebp),%esi
  8004f2:	85 ff                	test   %edi,%edi
  8004f4:	7f e8                	jg     8004de <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004f6:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004f9:	e9 c3 fd ff ff       	jmp    8002c1 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8004fe:	83 fa 01             	cmp    $0x1,%edx
  800501:	7e 16                	jle    800519 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800503:	8b 45 14             	mov    0x14(%ebp),%eax
  800506:	8d 50 08             	lea    0x8(%eax),%edx
  800509:	89 55 14             	mov    %edx,0x14(%ebp)
  80050c:	8b 50 04             	mov    0x4(%eax),%edx
  80050f:	8b 00                	mov    (%eax),%eax
  800511:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800514:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800517:	eb 32                	jmp    80054b <vprintfmt+0x2b0>
	else if (lflag)
  800519:	85 d2                	test   %edx,%edx
  80051b:	74 18                	je     800535 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  80051d:	8b 45 14             	mov    0x14(%ebp),%eax
  800520:	8d 50 04             	lea    0x4(%eax),%edx
  800523:	89 55 14             	mov    %edx,0x14(%ebp)
  800526:	8b 00                	mov    (%eax),%eax
  800528:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80052b:	89 c1                	mov    %eax,%ecx
  80052d:	c1 f9 1f             	sar    $0x1f,%ecx
  800530:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800533:	eb 16                	jmp    80054b <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800535:	8b 45 14             	mov    0x14(%ebp),%eax
  800538:	8d 50 04             	lea    0x4(%eax),%edx
  80053b:	89 55 14             	mov    %edx,0x14(%ebp)
  80053e:	8b 00                	mov    (%eax),%eax
  800540:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800543:	89 c1                	mov    %eax,%ecx
  800545:	c1 f9 1f             	sar    $0x1f,%ecx
  800548:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80054b:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80054e:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800551:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800555:	79 76                	jns    8005cd <vprintfmt+0x332>
				putch('-', putdat);
  800557:	83 ec 08             	sub    $0x8,%esp
  80055a:	53                   	push   %ebx
  80055b:	6a 2d                	push   $0x2d
  80055d:	ff d6                	call   *%esi
				num = -(long long) num;
  80055f:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800562:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800565:	f7 d8                	neg    %eax
  800567:	83 d2 00             	adc    $0x0,%edx
  80056a:	f7 da                	neg    %edx
  80056c:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80056f:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800574:	eb 5c                	jmp    8005d2 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800576:	8d 45 14             	lea    0x14(%ebp),%eax
  800579:	e8 aa fc ff ff       	call   800228 <getuint>
			base = 10;
  80057e:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800583:	eb 4d                	jmp    8005d2 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800585:	8d 45 14             	lea    0x14(%ebp),%eax
  800588:	e8 9b fc ff ff       	call   800228 <getuint>
			base = 8;
  80058d:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800592:	eb 3e                	jmp    8005d2 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800594:	83 ec 08             	sub    $0x8,%esp
  800597:	53                   	push   %ebx
  800598:	6a 30                	push   $0x30
  80059a:	ff d6                	call   *%esi
			putch('x', putdat);
  80059c:	83 c4 08             	add    $0x8,%esp
  80059f:	53                   	push   %ebx
  8005a0:	6a 78                	push   $0x78
  8005a2:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005a4:	8b 45 14             	mov    0x14(%ebp),%eax
  8005a7:	8d 50 04             	lea    0x4(%eax),%edx
  8005aa:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005ad:	8b 00                	mov    (%eax),%eax
  8005af:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005b4:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005b7:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005bc:	eb 14                	jmp    8005d2 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005be:	8d 45 14             	lea    0x14(%ebp),%eax
  8005c1:	e8 62 fc ff ff       	call   800228 <getuint>
			base = 16;
  8005c6:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005cb:	eb 05                	jmp    8005d2 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005cd:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005d2:	83 ec 0c             	sub    $0xc,%esp
  8005d5:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005d9:	57                   	push   %edi
  8005da:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005dd:	51                   	push   %ecx
  8005de:	52                   	push   %edx
  8005df:	50                   	push   %eax
  8005e0:	89 da                	mov    %ebx,%edx
  8005e2:	89 f0                	mov    %esi,%eax
  8005e4:	e8 92 fb ff ff       	call   80017b <printnum>
			break;
  8005e9:	83 c4 20             	add    $0x20,%esp
  8005ec:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005ef:	e9 cd fc ff ff       	jmp    8002c1 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8005f4:	83 ec 08             	sub    $0x8,%esp
  8005f7:	53                   	push   %ebx
  8005f8:	51                   	push   %ecx
  8005f9:	ff d6                	call   *%esi
			break;
  8005fb:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005fe:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800601:	e9 bb fc ff ff       	jmp    8002c1 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800606:	83 ec 08             	sub    $0x8,%esp
  800609:	53                   	push   %ebx
  80060a:	6a 25                	push   $0x25
  80060c:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  80060e:	83 c4 10             	add    $0x10,%esp
  800611:	eb 01                	jmp    800614 <vprintfmt+0x379>
  800613:	4f                   	dec    %edi
  800614:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800618:	75 f9                	jne    800613 <vprintfmt+0x378>
  80061a:	e9 a2 fc ff ff       	jmp    8002c1 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  80061f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800622:	5b                   	pop    %ebx
  800623:	5e                   	pop    %esi
  800624:	5f                   	pop    %edi
  800625:	5d                   	pop    %ebp
  800626:	c3                   	ret    

00800627 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800627:	55                   	push   %ebp
  800628:	89 e5                	mov    %esp,%ebp
  80062a:	83 ec 18             	sub    $0x18,%esp
  80062d:	8b 45 08             	mov    0x8(%ebp),%eax
  800630:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800633:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800636:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80063a:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  80063d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800644:	85 c0                	test   %eax,%eax
  800646:	74 26                	je     80066e <vsnprintf+0x47>
  800648:	85 d2                	test   %edx,%edx
  80064a:	7e 29                	jle    800675 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80064c:	ff 75 14             	pushl  0x14(%ebp)
  80064f:	ff 75 10             	pushl  0x10(%ebp)
  800652:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800655:	50                   	push   %eax
  800656:	68 62 02 80 00       	push   $0x800262
  80065b:	e8 3b fc ff ff       	call   80029b <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800660:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800663:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800666:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800669:	83 c4 10             	add    $0x10,%esp
  80066c:	eb 0c                	jmp    80067a <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80066e:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800673:	eb 05                	jmp    80067a <vsnprintf+0x53>
  800675:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80067a:	c9                   	leave  
  80067b:	c3                   	ret    

0080067c <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80067c:	55                   	push   %ebp
  80067d:	89 e5                	mov    %esp,%ebp
  80067f:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800682:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800685:	50                   	push   %eax
  800686:	ff 75 10             	pushl  0x10(%ebp)
  800689:	ff 75 0c             	pushl  0xc(%ebp)
  80068c:	ff 75 08             	pushl  0x8(%ebp)
  80068f:	e8 93 ff ff ff       	call   800627 <vsnprintf>
	va_end(ap);

	return rc;
}
  800694:	c9                   	leave  
  800695:	c3                   	ret    

00800696 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800696:	55                   	push   %ebp
  800697:	89 e5                	mov    %esp,%ebp
  800699:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  80069c:	b8 00 00 00 00       	mov    $0x0,%eax
  8006a1:	eb 01                	jmp    8006a4 <strlen+0xe>
		n++;
  8006a3:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006a4:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006a8:	75 f9                	jne    8006a3 <strlen+0xd>
		n++;
	return n;
}
  8006aa:	5d                   	pop    %ebp
  8006ab:	c3                   	ret    

008006ac <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006ac:	55                   	push   %ebp
  8006ad:	89 e5                	mov    %esp,%ebp
  8006af:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006b2:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006b5:	ba 00 00 00 00       	mov    $0x0,%edx
  8006ba:	eb 01                	jmp    8006bd <strnlen+0x11>
		n++;
  8006bc:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006bd:	39 c2                	cmp    %eax,%edx
  8006bf:	74 08                	je     8006c9 <strnlen+0x1d>
  8006c1:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006c5:	75 f5                	jne    8006bc <strnlen+0x10>
  8006c7:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006c9:	5d                   	pop    %ebp
  8006ca:	c3                   	ret    

008006cb <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006cb:	55                   	push   %ebp
  8006cc:	89 e5                	mov    %esp,%ebp
  8006ce:	53                   	push   %ebx
  8006cf:	8b 45 08             	mov    0x8(%ebp),%eax
  8006d2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006d5:	89 c2                	mov    %eax,%edx
  8006d7:	42                   	inc    %edx
  8006d8:	41                   	inc    %ecx
  8006d9:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006dc:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006df:	84 db                	test   %bl,%bl
  8006e1:	75 f4                	jne    8006d7 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006e3:	5b                   	pop    %ebx
  8006e4:	5d                   	pop    %ebp
  8006e5:	c3                   	ret    

008006e6 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006e6:	55                   	push   %ebp
  8006e7:	89 e5                	mov    %esp,%ebp
  8006e9:	53                   	push   %ebx
  8006ea:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8006ed:	53                   	push   %ebx
  8006ee:	e8 a3 ff ff ff       	call   800696 <strlen>
  8006f3:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8006f6:	ff 75 0c             	pushl  0xc(%ebp)
  8006f9:	01 d8                	add    %ebx,%eax
  8006fb:	50                   	push   %eax
  8006fc:	e8 ca ff ff ff       	call   8006cb <strcpy>
	return dst;
}
  800701:	89 d8                	mov    %ebx,%eax
  800703:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800706:	c9                   	leave  
  800707:	c3                   	ret    

00800708 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800708:	55                   	push   %ebp
  800709:	89 e5                	mov    %esp,%ebp
  80070b:	56                   	push   %esi
  80070c:	53                   	push   %ebx
  80070d:	8b 75 08             	mov    0x8(%ebp),%esi
  800710:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800713:	89 f3                	mov    %esi,%ebx
  800715:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800718:	89 f2                	mov    %esi,%edx
  80071a:	eb 0c                	jmp    800728 <strncpy+0x20>
		*dst++ = *src;
  80071c:	42                   	inc    %edx
  80071d:	8a 01                	mov    (%ecx),%al
  80071f:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800722:	80 39 01             	cmpb   $0x1,(%ecx)
  800725:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800728:	39 da                	cmp    %ebx,%edx
  80072a:	75 f0                	jne    80071c <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  80072c:	89 f0                	mov    %esi,%eax
  80072e:	5b                   	pop    %ebx
  80072f:	5e                   	pop    %esi
  800730:	5d                   	pop    %ebp
  800731:	c3                   	ret    

00800732 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800732:	55                   	push   %ebp
  800733:	89 e5                	mov    %esp,%ebp
  800735:	56                   	push   %esi
  800736:	53                   	push   %ebx
  800737:	8b 75 08             	mov    0x8(%ebp),%esi
  80073a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80073d:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800740:	85 c0                	test   %eax,%eax
  800742:	74 1e                	je     800762 <strlcpy+0x30>
  800744:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800748:	89 f2                	mov    %esi,%edx
  80074a:	eb 05                	jmp    800751 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  80074c:	42                   	inc    %edx
  80074d:	41                   	inc    %ecx
  80074e:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800751:	39 c2                	cmp    %eax,%edx
  800753:	74 08                	je     80075d <strlcpy+0x2b>
  800755:	8a 19                	mov    (%ecx),%bl
  800757:	84 db                	test   %bl,%bl
  800759:	75 f1                	jne    80074c <strlcpy+0x1a>
  80075b:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  80075d:	c6 00 00             	movb   $0x0,(%eax)
  800760:	eb 02                	jmp    800764 <strlcpy+0x32>
  800762:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800764:	29 f0                	sub    %esi,%eax
}
  800766:	5b                   	pop    %ebx
  800767:	5e                   	pop    %esi
  800768:	5d                   	pop    %ebp
  800769:	c3                   	ret    

0080076a <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80076a:	55                   	push   %ebp
  80076b:	89 e5                	mov    %esp,%ebp
  80076d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800770:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800773:	eb 02                	jmp    800777 <strcmp+0xd>
		p++, q++;
  800775:	41                   	inc    %ecx
  800776:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800777:	8a 01                	mov    (%ecx),%al
  800779:	84 c0                	test   %al,%al
  80077b:	74 04                	je     800781 <strcmp+0x17>
  80077d:	3a 02                	cmp    (%edx),%al
  80077f:	74 f4                	je     800775 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800781:	0f b6 c0             	movzbl %al,%eax
  800784:	0f b6 12             	movzbl (%edx),%edx
  800787:	29 d0                	sub    %edx,%eax
}
  800789:	5d                   	pop    %ebp
  80078a:	c3                   	ret    

0080078b <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  80078b:	55                   	push   %ebp
  80078c:	89 e5                	mov    %esp,%ebp
  80078e:	53                   	push   %ebx
  80078f:	8b 45 08             	mov    0x8(%ebp),%eax
  800792:	8b 55 0c             	mov    0xc(%ebp),%edx
  800795:	89 c3                	mov    %eax,%ebx
  800797:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  80079a:	eb 02                	jmp    80079e <strncmp+0x13>
		n--, p++, q++;
  80079c:	40                   	inc    %eax
  80079d:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  80079e:	39 d8                	cmp    %ebx,%eax
  8007a0:	74 14                	je     8007b6 <strncmp+0x2b>
  8007a2:	8a 08                	mov    (%eax),%cl
  8007a4:	84 c9                	test   %cl,%cl
  8007a6:	74 04                	je     8007ac <strncmp+0x21>
  8007a8:	3a 0a                	cmp    (%edx),%cl
  8007aa:	74 f0                	je     80079c <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007ac:	0f b6 00             	movzbl (%eax),%eax
  8007af:	0f b6 12             	movzbl (%edx),%edx
  8007b2:	29 d0                	sub    %edx,%eax
  8007b4:	eb 05                	jmp    8007bb <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007b6:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007bb:	5b                   	pop    %ebx
  8007bc:	5d                   	pop    %ebp
  8007bd:	c3                   	ret    

008007be <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007be:	55                   	push   %ebp
  8007bf:	89 e5                	mov    %esp,%ebp
  8007c1:	8b 45 08             	mov    0x8(%ebp),%eax
  8007c4:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007c7:	eb 05                	jmp    8007ce <strchr+0x10>
		if (*s == c)
  8007c9:	38 ca                	cmp    %cl,%dl
  8007cb:	74 0c                	je     8007d9 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007cd:	40                   	inc    %eax
  8007ce:	8a 10                	mov    (%eax),%dl
  8007d0:	84 d2                	test   %dl,%dl
  8007d2:	75 f5                	jne    8007c9 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007d4:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007d9:	5d                   	pop    %ebp
  8007da:	c3                   	ret    

008007db <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007db:	55                   	push   %ebp
  8007dc:	89 e5                	mov    %esp,%ebp
  8007de:	8b 45 08             	mov    0x8(%ebp),%eax
  8007e1:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007e4:	eb 05                	jmp    8007eb <strfind+0x10>
		if (*s == c)
  8007e6:	38 ca                	cmp    %cl,%dl
  8007e8:	74 07                	je     8007f1 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007ea:	40                   	inc    %eax
  8007eb:	8a 10                	mov    (%eax),%dl
  8007ed:	84 d2                	test   %dl,%dl
  8007ef:	75 f5                	jne    8007e6 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8007f1:	5d                   	pop    %ebp
  8007f2:	c3                   	ret    

008007f3 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8007f3:	55                   	push   %ebp
  8007f4:	89 e5                	mov    %esp,%ebp
  8007f6:	57                   	push   %edi
  8007f7:	56                   	push   %esi
  8007f8:	53                   	push   %ebx
  8007f9:	8b 7d 08             	mov    0x8(%ebp),%edi
  8007fc:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8007ff:	85 c9                	test   %ecx,%ecx
  800801:	74 36                	je     800839 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800803:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800809:	75 28                	jne    800833 <memset+0x40>
  80080b:	f6 c1 03             	test   $0x3,%cl
  80080e:	75 23                	jne    800833 <memset+0x40>
		c &= 0xFF;
  800810:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800814:	89 d3                	mov    %edx,%ebx
  800816:	c1 e3 08             	shl    $0x8,%ebx
  800819:	89 d6                	mov    %edx,%esi
  80081b:	c1 e6 18             	shl    $0x18,%esi
  80081e:	89 d0                	mov    %edx,%eax
  800820:	c1 e0 10             	shl    $0x10,%eax
  800823:	09 f0                	or     %esi,%eax
  800825:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800827:	89 d8                	mov    %ebx,%eax
  800829:	09 d0                	or     %edx,%eax
  80082b:	c1 e9 02             	shr    $0x2,%ecx
  80082e:	fc                   	cld    
  80082f:	f3 ab                	rep stos %eax,%es:(%edi)
  800831:	eb 06                	jmp    800839 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800833:	8b 45 0c             	mov    0xc(%ebp),%eax
  800836:	fc                   	cld    
  800837:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800839:	89 f8                	mov    %edi,%eax
  80083b:	5b                   	pop    %ebx
  80083c:	5e                   	pop    %esi
  80083d:	5f                   	pop    %edi
  80083e:	5d                   	pop    %ebp
  80083f:	c3                   	ret    

00800840 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800840:	55                   	push   %ebp
  800841:	89 e5                	mov    %esp,%ebp
  800843:	57                   	push   %edi
  800844:	56                   	push   %esi
  800845:	8b 45 08             	mov    0x8(%ebp),%eax
  800848:	8b 75 0c             	mov    0xc(%ebp),%esi
  80084b:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  80084e:	39 c6                	cmp    %eax,%esi
  800850:	73 33                	jae    800885 <memmove+0x45>
  800852:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800855:	39 d0                	cmp    %edx,%eax
  800857:	73 2c                	jae    800885 <memmove+0x45>
		s += n;
		d += n;
  800859:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80085c:	89 d6                	mov    %edx,%esi
  80085e:	09 fe                	or     %edi,%esi
  800860:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800866:	75 13                	jne    80087b <memmove+0x3b>
  800868:	f6 c1 03             	test   $0x3,%cl
  80086b:	75 0e                	jne    80087b <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  80086d:	83 ef 04             	sub    $0x4,%edi
  800870:	8d 72 fc             	lea    -0x4(%edx),%esi
  800873:	c1 e9 02             	shr    $0x2,%ecx
  800876:	fd                   	std    
  800877:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800879:	eb 07                	jmp    800882 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  80087b:	4f                   	dec    %edi
  80087c:	8d 72 ff             	lea    -0x1(%edx),%esi
  80087f:	fd                   	std    
  800880:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800882:	fc                   	cld    
  800883:	eb 1d                	jmp    8008a2 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800885:	89 f2                	mov    %esi,%edx
  800887:	09 c2                	or     %eax,%edx
  800889:	f6 c2 03             	test   $0x3,%dl
  80088c:	75 0f                	jne    80089d <memmove+0x5d>
  80088e:	f6 c1 03             	test   $0x3,%cl
  800891:	75 0a                	jne    80089d <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800893:	c1 e9 02             	shr    $0x2,%ecx
  800896:	89 c7                	mov    %eax,%edi
  800898:	fc                   	cld    
  800899:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80089b:	eb 05                	jmp    8008a2 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  80089d:	89 c7                	mov    %eax,%edi
  80089f:	fc                   	cld    
  8008a0:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008a2:	5e                   	pop    %esi
  8008a3:	5f                   	pop    %edi
  8008a4:	5d                   	pop    %ebp
  8008a5:	c3                   	ret    

008008a6 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008a6:	55                   	push   %ebp
  8008a7:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008a9:	ff 75 10             	pushl  0x10(%ebp)
  8008ac:	ff 75 0c             	pushl  0xc(%ebp)
  8008af:	ff 75 08             	pushl  0x8(%ebp)
  8008b2:	e8 89 ff ff ff       	call   800840 <memmove>
}
  8008b7:	c9                   	leave  
  8008b8:	c3                   	ret    

008008b9 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008b9:	55                   	push   %ebp
  8008ba:	89 e5                	mov    %esp,%ebp
  8008bc:	56                   	push   %esi
  8008bd:	53                   	push   %ebx
  8008be:	8b 45 08             	mov    0x8(%ebp),%eax
  8008c1:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008c4:	89 c6                	mov    %eax,%esi
  8008c6:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008c9:	eb 14                	jmp    8008df <memcmp+0x26>
		if (*s1 != *s2)
  8008cb:	8a 08                	mov    (%eax),%cl
  8008cd:	8a 1a                	mov    (%edx),%bl
  8008cf:	38 d9                	cmp    %bl,%cl
  8008d1:	74 0a                	je     8008dd <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008d3:	0f b6 c1             	movzbl %cl,%eax
  8008d6:	0f b6 db             	movzbl %bl,%ebx
  8008d9:	29 d8                	sub    %ebx,%eax
  8008db:	eb 0b                	jmp    8008e8 <memcmp+0x2f>
		s1++, s2++;
  8008dd:	40                   	inc    %eax
  8008de:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008df:	39 f0                	cmp    %esi,%eax
  8008e1:	75 e8                	jne    8008cb <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008e3:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008e8:	5b                   	pop    %ebx
  8008e9:	5e                   	pop    %esi
  8008ea:	5d                   	pop    %ebp
  8008eb:	c3                   	ret    

008008ec <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8008ec:	55                   	push   %ebp
  8008ed:	89 e5                	mov    %esp,%ebp
  8008ef:	53                   	push   %ebx
  8008f0:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8008f3:	89 c1                	mov    %eax,%ecx
  8008f5:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8008f8:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008fc:	eb 08                	jmp    800906 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8008fe:	0f b6 10             	movzbl (%eax),%edx
  800901:	39 da                	cmp    %ebx,%edx
  800903:	74 05                	je     80090a <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800905:	40                   	inc    %eax
  800906:	39 c8                	cmp    %ecx,%eax
  800908:	72 f4                	jb     8008fe <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  80090a:	5b                   	pop    %ebx
  80090b:	5d                   	pop    %ebp
  80090c:	c3                   	ret    

0080090d <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  80090d:	55                   	push   %ebp
  80090e:	89 e5                	mov    %esp,%ebp
  800910:	57                   	push   %edi
  800911:	56                   	push   %esi
  800912:	53                   	push   %ebx
  800913:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800916:	eb 01                	jmp    800919 <strtol+0xc>
		s++;
  800918:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800919:	8a 01                	mov    (%ecx),%al
  80091b:	3c 20                	cmp    $0x20,%al
  80091d:	74 f9                	je     800918 <strtol+0xb>
  80091f:	3c 09                	cmp    $0x9,%al
  800921:	74 f5                	je     800918 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800923:	3c 2b                	cmp    $0x2b,%al
  800925:	75 08                	jne    80092f <strtol+0x22>
		s++;
  800927:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800928:	bf 00 00 00 00       	mov    $0x0,%edi
  80092d:	eb 11                	jmp    800940 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  80092f:	3c 2d                	cmp    $0x2d,%al
  800931:	75 08                	jne    80093b <strtol+0x2e>
		s++, neg = 1;
  800933:	41                   	inc    %ecx
  800934:	bf 01 00 00 00       	mov    $0x1,%edi
  800939:	eb 05                	jmp    800940 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  80093b:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800940:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800944:	0f 84 87 00 00 00    	je     8009d1 <strtol+0xc4>
  80094a:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  80094e:	75 27                	jne    800977 <strtol+0x6a>
  800950:	80 39 30             	cmpb   $0x30,(%ecx)
  800953:	75 22                	jne    800977 <strtol+0x6a>
  800955:	e9 88 00 00 00       	jmp    8009e2 <strtol+0xd5>
		s += 2, base = 16;
  80095a:	83 c1 02             	add    $0x2,%ecx
  80095d:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800964:	eb 11                	jmp    800977 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800966:	41                   	inc    %ecx
  800967:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  80096e:	eb 07                	jmp    800977 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800970:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800977:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  80097c:	8a 11                	mov    (%ecx),%dl
  80097e:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800981:	80 fb 09             	cmp    $0x9,%bl
  800984:	77 08                	ja     80098e <strtol+0x81>
			dig = *s - '0';
  800986:	0f be d2             	movsbl %dl,%edx
  800989:	83 ea 30             	sub    $0x30,%edx
  80098c:	eb 22                	jmp    8009b0 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  80098e:	8d 72 9f             	lea    -0x61(%edx),%esi
  800991:	89 f3                	mov    %esi,%ebx
  800993:	80 fb 19             	cmp    $0x19,%bl
  800996:	77 08                	ja     8009a0 <strtol+0x93>
			dig = *s - 'a' + 10;
  800998:	0f be d2             	movsbl %dl,%edx
  80099b:	83 ea 57             	sub    $0x57,%edx
  80099e:	eb 10                	jmp    8009b0 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009a0:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009a3:	89 f3                	mov    %esi,%ebx
  8009a5:	80 fb 19             	cmp    $0x19,%bl
  8009a8:	77 14                	ja     8009be <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009aa:	0f be d2             	movsbl %dl,%edx
  8009ad:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009b0:	3b 55 10             	cmp    0x10(%ebp),%edx
  8009b3:	7d 09                	jge    8009be <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009b5:	41                   	inc    %ecx
  8009b6:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009ba:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009bc:	eb be                	jmp    80097c <strtol+0x6f>

	if (endptr)
  8009be:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009c2:	74 05                	je     8009c9 <strtol+0xbc>
		*endptr = (char *) s;
  8009c4:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009c7:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009c9:	85 ff                	test   %edi,%edi
  8009cb:	74 21                	je     8009ee <strtol+0xe1>
  8009cd:	f7 d8                	neg    %eax
  8009cf:	eb 1d                	jmp    8009ee <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009d1:	80 39 30             	cmpb   $0x30,(%ecx)
  8009d4:	75 9a                	jne    800970 <strtol+0x63>
  8009d6:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009da:	0f 84 7a ff ff ff    	je     80095a <strtol+0x4d>
  8009e0:	eb 84                	jmp    800966 <strtol+0x59>
  8009e2:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009e6:	0f 84 6e ff ff ff    	je     80095a <strtol+0x4d>
  8009ec:	eb 89                	jmp    800977 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  8009ee:	5b                   	pop    %ebx
  8009ef:	5e                   	pop    %esi
  8009f0:	5f                   	pop    %edi
  8009f1:	5d                   	pop    %ebp
  8009f2:	c3                   	ret    

008009f3 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8009f3:	55                   	push   %ebp
  8009f4:	89 e5                	mov    %esp,%ebp
  8009f6:	57                   	push   %edi
  8009f7:	56                   	push   %esi
  8009f8:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8009f9:	b8 00 00 00 00       	mov    $0x0,%eax
  8009fe:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a01:	8b 55 08             	mov    0x8(%ebp),%edx
  800a04:	89 c3                	mov    %eax,%ebx
  800a06:	89 c7                	mov    %eax,%edi
  800a08:	89 c6                	mov    %eax,%esi
  800a0a:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a0c:	5b                   	pop    %ebx
  800a0d:	5e                   	pop    %esi
  800a0e:	5f                   	pop    %edi
  800a0f:	5d                   	pop    %ebp
  800a10:	c3                   	ret    

00800a11 <sys_cgetc>:

int
sys_cgetc(void)
{
  800a11:	55                   	push   %ebp
  800a12:	89 e5                	mov    %esp,%ebp
  800a14:	57                   	push   %edi
  800a15:	56                   	push   %esi
  800a16:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a17:	ba 00 00 00 00       	mov    $0x0,%edx
  800a1c:	b8 01 00 00 00       	mov    $0x1,%eax
  800a21:	89 d1                	mov    %edx,%ecx
  800a23:	89 d3                	mov    %edx,%ebx
  800a25:	89 d7                	mov    %edx,%edi
  800a27:	89 d6                	mov    %edx,%esi
  800a29:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a2b:	5b                   	pop    %ebx
  800a2c:	5e                   	pop    %esi
  800a2d:	5f                   	pop    %edi
  800a2e:	5d                   	pop    %ebp
  800a2f:	c3                   	ret    

00800a30 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a30:	55                   	push   %ebp
  800a31:	89 e5                	mov    %esp,%ebp
  800a33:	57                   	push   %edi
  800a34:	56                   	push   %esi
  800a35:	53                   	push   %ebx
  800a36:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a39:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a3e:	b8 03 00 00 00       	mov    $0x3,%eax
  800a43:	8b 55 08             	mov    0x8(%ebp),%edx
  800a46:	89 cb                	mov    %ecx,%ebx
  800a48:	89 cf                	mov    %ecx,%edi
  800a4a:	89 ce                	mov    %ecx,%esi
  800a4c:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a4e:	85 c0                	test   %eax,%eax
  800a50:	7e 17                	jle    800a69 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a52:	83 ec 0c             	sub    $0xc,%esp
  800a55:	50                   	push   %eax
  800a56:	6a 03                	push   $0x3
  800a58:	68 9c 0f 80 00       	push   $0x800f9c
  800a5d:	6a 23                	push   $0x23
  800a5f:	68 b9 0f 80 00       	push   $0x800fb9
  800a64:	e8 27 00 00 00       	call   800a90 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a69:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a6c:	5b                   	pop    %ebx
  800a6d:	5e                   	pop    %esi
  800a6e:	5f                   	pop    %edi
  800a6f:	5d                   	pop    %ebp
  800a70:	c3                   	ret    

00800a71 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a71:	55                   	push   %ebp
  800a72:	89 e5                	mov    %esp,%ebp
  800a74:	57                   	push   %edi
  800a75:	56                   	push   %esi
  800a76:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a77:	ba 00 00 00 00       	mov    $0x0,%edx
  800a7c:	b8 02 00 00 00       	mov    $0x2,%eax
  800a81:	89 d1                	mov    %edx,%ecx
  800a83:	89 d3                	mov    %edx,%ebx
  800a85:	89 d7                	mov    %edx,%edi
  800a87:	89 d6                	mov    %edx,%esi
  800a89:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800a8b:	5b                   	pop    %ebx
  800a8c:	5e                   	pop    %esi
  800a8d:	5f                   	pop    %edi
  800a8e:	5d                   	pop    %ebp
  800a8f:	c3                   	ret    

00800a90 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800a90:	55                   	push   %ebp
  800a91:	89 e5                	mov    %esp,%ebp
  800a93:	56                   	push   %esi
  800a94:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800a95:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800a98:	8b 35 00 10 80 00    	mov    0x801000,%esi
  800a9e:	e8 ce ff ff ff       	call   800a71 <sys_getenvid>
  800aa3:	83 ec 0c             	sub    $0xc,%esp
  800aa6:	ff 75 0c             	pushl  0xc(%ebp)
  800aa9:	ff 75 08             	pushl  0x8(%ebp)
  800aac:	56                   	push   %esi
  800aad:	50                   	push   %eax
  800aae:	68 c8 0f 80 00       	push   $0x800fc8
  800ab3:	e8 af f6 ff ff       	call   800167 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800ab8:	83 c4 18             	add    $0x18,%esp
  800abb:	53                   	push   %ebx
  800abc:	ff 75 10             	pushl  0x10(%ebp)
  800abf:	e8 52 f6 ff ff       	call   800116 <vcprintf>
	cprintf("\n");
  800ac4:	c7 04 24 53 0d 80 00 	movl   $0x800d53,(%esp)
  800acb:	e8 97 f6 ff ff       	call   800167 <cprintf>
  800ad0:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800ad3:	cc                   	int3   
  800ad4:	eb fd                	jmp    800ad3 <_panic+0x43>
  800ad6:	66 90                	xchg   %ax,%ax

00800ad8 <__udivdi3>:
  800ad8:	55                   	push   %ebp
  800ad9:	57                   	push   %edi
  800ada:	56                   	push   %esi
  800adb:	53                   	push   %ebx
  800adc:	83 ec 1c             	sub    $0x1c,%esp
  800adf:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ae3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800ae7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800aeb:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800aef:	89 ca                	mov    %ecx,%edx
  800af1:	89 f8                	mov    %edi,%eax
  800af3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800af7:	85 f6                	test   %esi,%esi
  800af9:	75 2d                	jne    800b28 <__udivdi3+0x50>
  800afb:	39 cf                	cmp    %ecx,%edi
  800afd:	77 65                	ja     800b64 <__udivdi3+0x8c>
  800aff:	89 fd                	mov    %edi,%ebp
  800b01:	85 ff                	test   %edi,%edi
  800b03:	75 0b                	jne    800b10 <__udivdi3+0x38>
  800b05:	b8 01 00 00 00       	mov    $0x1,%eax
  800b0a:	31 d2                	xor    %edx,%edx
  800b0c:	f7 f7                	div    %edi
  800b0e:	89 c5                	mov    %eax,%ebp
  800b10:	31 d2                	xor    %edx,%edx
  800b12:	89 c8                	mov    %ecx,%eax
  800b14:	f7 f5                	div    %ebp
  800b16:	89 c1                	mov    %eax,%ecx
  800b18:	89 d8                	mov    %ebx,%eax
  800b1a:	f7 f5                	div    %ebp
  800b1c:	89 cf                	mov    %ecx,%edi
  800b1e:	89 fa                	mov    %edi,%edx
  800b20:	83 c4 1c             	add    $0x1c,%esp
  800b23:	5b                   	pop    %ebx
  800b24:	5e                   	pop    %esi
  800b25:	5f                   	pop    %edi
  800b26:	5d                   	pop    %ebp
  800b27:	c3                   	ret    
  800b28:	39 ce                	cmp    %ecx,%esi
  800b2a:	77 28                	ja     800b54 <__udivdi3+0x7c>
  800b2c:	0f bd fe             	bsr    %esi,%edi
  800b2f:	83 f7 1f             	xor    $0x1f,%edi
  800b32:	75 40                	jne    800b74 <__udivdi3+0x9c>
  800b34:	39 ce                	cmp    %ecx,%esi
  800b36:	72 0a                	jb     800b42 <__udivdi3+0x6a>
  800b38:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b3c:	0f 87 9e 00 00 00    	ja     800be0 <__udivdi3+0x108>
  800b42:	b8 01 00 00 00       	mov    $0x1,%eax
  800b47:	89 fa                	mov    %edi,%edx
  800b49:	83 c4 1c             	add    $0x1c,%esp
  800b4c:	5b                   	pop    %ebx
  800b4d:	5e                   	pop    %esi
  800b4e:	5f                   	pop    %edi
  800b4f:	5d                   	pop    %ebp
  800b50:	c3                   	ret    
  800b51:	8d 76 00             	lea    0x0(%esi),%esi
  800b54:	31 ff                	xor    %edi,%edi
  800b56:	31 c0                	xor    %eax,%eax
  800b58:	89 fa                	mov    %edi,%edx
  800b5a:	83 c4 1c             	add    $0x1c,%esp
  800b5d:	5b                   	pop    %ebx
  800b5e:	5e                   	pop    %esi
  800b5f:	5f                   	pop    %edi
  800b60:	5d                   	pop    %ebp
  800b61:	c3                   	ret    
  800b62:	66 90                	xchg   %ax,%ax
  800b64:	89 d8                	mov    %ebx,%eax
  800b66:	f7 f7                	div    %edi
  800b68:	31 ff                	xor    %edi,%edi
  800b6a:	89 fa                	mov    %edi,%edx
  800b6c:	83 c4 1c             	add    $0x1c,%esp
  800b6f:	5b                   	pop    %ebx
  800b70:	5e                   	pop    %esi
  800b71:	5f                   	pop    %edi
  800b72:	5d                   	pop    %ebp
  800b73:	c3                   	ret    
  800b74:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b79:	89 eb                	mov    %ebp,%ebx
  800b7b:	29 fb                	sub    %edi,%ebx
  800b7d:	89 f9                	mov    %edi,%ecx
  800b7f:	d3 e6                	shl    %cl,%esi
  800b81:	89 c5                	mov    %eax,%ebp
  800b83:	88 d9                	mov    %bl,%cl
  800b85:	d3 ed                	shr    %cl,%ebp
  800b87:	89 e9                	mov    %ebp,%ecx
  800b89:	09 f1                	or     %esi,%ecx
  800b8b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b8f:	89 f9                	mov    %edi,%ecx
  800b91:	d3 e0                	shl    %cl,%eax
  800b93:	89 c5                	mov    %eax,%ebp
  800b95:	89 d6                	mov    %edx,%esi
  800b97:	88 d9                	mov    %bl,%cl
  800b99:	d3 ee                	shr    %cl,%esi
  800b9b:	89 f9                	mov    %edi,%ecx
  800b9d:	d3 e2                	shl    %cl,%edx
  800b9f:	8b 44 24 08          	mov    0x8(%esp),%eax
  800ba3:	88 d9                	mov    %bl,%cl
  800ba5:	d3 e8                	shr    %cl,%eax
  800ba7:	09 c2                	or     %eax,%edx
  800ba9:	89 d0                	mov    %edx,%eax
  800bab:	89 f2                	mov    %esi,%edx
  800bad:	f7 74 24 0c          	divl   0xc(%esp)
  800bb1:	89 d6                	mov    %edx,%esi
  800bb3:	89 c3                	mov    %eax,%ebx
  800bb5:	f7 e5                	mul    %ebp
  800bb7:	39 d6                	cmp    %edx,%esi
  800bb9:	72 19                	jb     800bd4 <__udivdi3+0xfc>
  800bbb:	74 0b                	je     800bc8 <__udivdi3+0xf0>
  800bbd:	89 d8                	mov    %ebx,%eax
  800bbf:	31 ff                	xor    %edi,%edi
  800bc1:	e9 58 ff ff ff       	jmp    800b1e <__udivdi3+0x46>
  800bc6:	66 90                	xchg   %ax,%ax
  800bc8:	8b 54 24 08          	mov    0x8(%esp),%edx
  800bcc:	89 f9                	mov    %edi,%ecx
  800bce:	d3 e2                	shl    %cl,%edx
  800bd0:	39 c2                	cmp    %eax,%edx
  800bd2:	73 e9                	jae    800bbd <__udivdi3+0xe5>
  800bd4:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800bd7:	31 ff                	xor    %edi,%edi
  800bd9:	e9 40 ff ff ff       	jmp    800b1e <__udivdi3+0x46>
  800bde:	66 90                	xchg   %ax,%ax
  800be0:	31 c0                	xor    %eax,%eax
  800be2:	e9 37 ff ff ff       	jmp    800b1e <__udivdi3+0x46>
  800be7:	90                   	nop

00800be8 <__umoddi3>:
  800be8:	55                   	push   %ebp
  800be9:	57                   	push   %edi
  800bea:	56                   	push   %esi
  800beb:	53                   	push   %ebx
  800bec:	83 ec 1c             	sub    $0x1c,%esp
  800bef:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bf3:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bf7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800bfb:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800bff:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800c03:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800c07:	89 f3                	mov    %esi,%ebx
  800c09:	89 fa                	mov    %edi,%edx
  800c0b:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800c0f:	89 34 24             	mov    %esi,(%esp)
  800c12:	85 c0                	test   %eax,%eax
  800c14:	75 1a                	jne    800c30 <__umoddi3+0x48>
  800c16:	39 f7                	cmp    %esi,%edi
  800c18:	0f 86 a2 00 00 00    	jbe    800cc0 <__umoddi3+0xd8>
  800c1e:	89 c8                	mov    %ecx,%eax
  800c20:	89 f2                	mov    %esi,%edx
  800c22:	f7 f7                	div    %edi
  800c24:	89 d0                	mov    %edx,%eax
  800c26:	31 d2                	xor    %edx,%edx
  800c28:	83 c4 1c             	add    $0x1c,%esp
  800c2b:	5b                   	pop    %ebx
  800c2c:	5e                   	pop    %esi
  800c2d:	5f                   	pop    %edi
  800c2e:	5d                   	pop    %ebp
  800c2f:	c3                   	ret    
  800c30:	39 f0                	cmp    %esi,%eax
  800c32:	0f 87 ac 00 00 00    	ja     800ce4 <__umoddi3+0xfc>
  800c38:	0f bd e8             	bsr    %eax,%ebp
  800c3b:	83 f5 1f             	xor    $0x1f,%ebp
  800c3e:	0f 84 ac 00 00 00    	je     800cf0 <__umoddi3+0x108>
  800c44:	bf 20 00 00 00       	mov    $0x20,%edi
  800c49:	29 ef                	sub    %ebp,%edi
  800c4b:	89 fe                	mov    %edi,%esi
  800c4d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c51:	89 e9                	mov    %ebp,%ecx
  800c53:	d3 e0                	shl    %cl,%eax
  800c55:	89 d7                	mov    %edx,%edi
  800c57:	89 f1                	mov    %esi,%ecx
  800c59:	d3 ef                	shr    %cl,%edi
  800c5b:	09 c7                	or     %eax,%edi
  800c5d:	89 e9                	mov    %ebp,%ecx
  800c5f:	d3 e2                	shl    %cl,%edx
  800c61:	89 14 24             	mov    %edx,(%esp)
  800c64:	89 d8                	mov    %ebx,%eax
  800c66:	d3 e0                	shl    %cl,%eax
  800c68:	89 c2                	mov    %eax,%edx
  800c6a:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c6e:	d3 e0                	shl    %cl,%eax
  800c70:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c74:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c78:	89 f1                	mov    %esi,%ecx
  800c7a:	d3 e8                	shr    %cl,%eax
  800c7c:	09 d0                	or     %edx,%eax
  800c7e:	d3 eb                	shr    %cl,%ebx
  800c80:	89 da                	mov    %ebx,%edx
  800c82:	f7 f7                	div    %edi
  800c84:	89 d3                	mov    %edx,%ebx
  800c86:	f7 24 24             	mull   (%esp)
  800c89:	89 c6                	mov    %eax,%esi
  800c8b:	89 d1                	mov    %edx,%ecx
  800c8d:	39 d3                	cmp    %edx,%ebx
  800c8f:	0f 82 87 00 00 00    	jb     800d1c <__umoddi3+0x134>
  800c95:	0f 84 91 00 00 00    	je     800d2c <__umoddi3+0x144>
  800c9b:	8b 54 24 04          	mov    0x4(%esp),%edx
  800c9f:	29 f2                	sub    %esi,%edx
  800ca1:	19 cb                	sbb    %ecx,%ebx
  800ca3:	89 d8                	mov    %ebx,%eax
  800ca5:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800ca9:	d3 e0                	shl    %cl,%eax
  800cab:	89 e9                	mov    %ebp,%ecx
  800cad:	d3 ea                	shr    %cl,%edx
  800caf:	09 d0                	or     %edx,%eax
  800cb1:	89 e9                	mov    %ebp,%ecx
  800cb3:	d3 eb                	shr    %cl,%ebx
  800cb5:	89 da                	mov    %ebx,%edx
  800cb7:	83 c4 1c             	add    $0x1c,%esp
  800cba:	5b                   	pop    %ebx
  800cbb:	5e                   	pop    %esi
  800cbc:	5f                   	pop    %edi
  800cbd:	5d                   	pop    %ebp
  800cbe:	c3                   	ret    
  800cbf:	90                   	nop
  800cc0:	89 fd                	mov    %edi,%ebp
  800cc2:	85 ff                	test   %edi,%edi
  800cc4:	75 0b                	jne    800cd1 <__umoddi3+0xe9>
  800cc6:	b8 01 00 00 00       	mov    $0x1,%eax
  800ccb:	31 d2                	xor    %edx,%edx
  800ccd:	f7 f7                	div    %edi
  800ccf:	89 c5                	mov    %eax,%ebp
  800cd1:	89 f0                	mov    %esi,%eax
  800cd3:	31 d2                	xor    %edx,%edx
  800cd5:	f7 f5                	div    %ebp
  800cd7:	89 c8                	mov    %ecx,%eax
  800cd9:	f7 f5                	div    %ebp
  800cdb:	89 d0                	mov    %edx,%eax
  800cdd:	e9 44 ff ff ff       	jmp    800c26 <__umoddi3+0x3e>
  800ce2:	66 90                	xchg   %ax,%ax
  800ce4:	89 c8                	mov    %ecx,%eax
  800ce6:	89 f2                	mov    %esi,%edx
  800ce8:	83 c4 1c             	add    $0x1c,%esp
  800ceb:	5b                   	pop    %ebx
  800cec:	5e                   	pop    %esi
  800ced:	5f                   	pop    %edi
  800cee:	5d                   	pop    %ebp
  800cef:	c3                   	ret    
  800cf0:	3b 04 24             	cmp    (%esp),%eax
  800cf3:	72 06                	jb     800cfb <__umoddi3+0x113>
  800cf5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800cf9:	77 0f                	ja     800d0a <__umoddi3+0x122>
  800cfb:	89 f2                	mov    %esi,%edx
  800cfd:	29 f9                	sub    %edi,%ecx
  800cff:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800d03:	89 14 24             	mov    %edx,(%esp)
  800d06:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800d0a:	8b 44 24 04          	mov    0x4(%esp),%eax
  800d0e:	8b 14 24             	mov    (%esp),%edx
  800d11:	83 c4 1c             	add    $0x1c,%esp
  800d14:	5b                   	pop    %ebx
  800d15:	5e                   	pop    %esi
  800d16:	5f                   	pop    %edi
  800d17:	5d                   	pop    %ebp
  800d18:	c3                   	ret    
  800d19:	8d 76 00             	lea    0x0(%esi),%esi
  800d1c:	2b 04 24             	sub    (%esp),%eax
  800d1f:	19 fa                	sbb    %edi,%edx
  800d21:	89 d1                	mov    %edx,%ecx
  800d23:	89 c6                	mov    %eax,%esi
  800d25:	e9 71 ff ff ff       	jmp    800c9b <__umoddi3+0xb3>
  800d2a:	66 90                	xchg   %ax,%ax
  800d2c:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d30:	72 ea                	jb     800d1c <__umoddi3+0x134>
  800d32:	89 d9                	mov    %ebx,%ecx
  800d34:	e9 62 ff ff ff       	jmp    800c9b <__umoddi3+0xb3>
