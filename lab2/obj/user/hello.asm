
obj/user/hello:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 2d 00 00 00       	call   80005e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:
// hello, world
#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 14             	sub    $0x14,%esp
	cprintf("hello, world\n");
  800039:	68 60 0d 80 00       	push   $0x800d60
  80003e:	e8 49 01 00 00       	call   80018c <cprintf>
	cprintf("i am environment %08x\n", thisenv->env_id);
  800043:	a1 04 20 80 00       	mov    0x802004,%eax
  800048:	8b 40 48             	mov    0x48(%eax),%eax
  80004b:	83 c4 08             	add    $0x8,%esp
  80004e:	50                   	push   %eax
  80004f:	68 6e 0d 80 00       	push   $0x800d6e
  800054:	e8 33 01 00 00       	call   80018c <cprintf>
}
  800059:	83 c4 10             	add    $0x10,%esp
  80005c:	c9                   	leave  
  80005d:	c3                   	ret    

0080005e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80005e:	55                   	push   %ebp
  80005f:	89 e5                	mov    %esp,%ebp
  800061:	57                   	push   %edi
  800062:	56                   	push   %esi
  800063:	53                   	push   %ebx
  800064:	83 ec 0c             	sub    $0xc,%esp
  800067:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80006a:	8b 75 0c             	mov    0xc(%ebp),%esi
	// set thisenv to point at our Env structure in envs[].
	// LAB 3: Your code here.
	//thisenv = 0;
	int32_t env_Index1 = (int32_t)sys_getenvid();
  80006d:	e8 24 0a 00 00       	call   800a96 <sys_getenvid>
  800072:	89 c7                	mov    %eax,%edi
	cprintf("printing env_Index1: %d\n", env_Index1);
  800074:	83 ec 08             	sub    $0x8,%esp
  800077:	50                   	push   %eax
  800078:	68 85 0d 80 00       	push   $0x800d85
  80007d:	e8 0a 01 00 00       	call   80018c <cprintf>
	int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	cprintf("printing env_Index2: %d\n", env_Index2);
  800082:	83 c4 08             	add    $0x8,%esp
  800085:	81 e7 ff 03 00 00    	and    $0x3ff,%edi
  80008b:	57                   	push   %edi
  80008c:	68 9e 0d 80 00       	push   $0x800d9e
  800091:	e8 f6 00 00 00       	call   80018c <cprintf>

	thisenv = &envs[ENVX(sys_getenvid())];
  800096:	e8 fb 09 00 00       	call   800a96 <sys_getenvid>
  80009b:	25 ff 03 00 00       	and    $0x3ff,%eax
  8000a0:	8d 14 00             	lea    (%eax,%eax,1),%edx
  8000a3:	01 d0                	add    %edx,%eax
  8000a5:	c1 e0 05             	shl    $0x5,%eax
  8000a8:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8000ad:	a3 04 20 80 00       	mov    %eax,0x802004
	cprintf("Addrs of thisenv (envs[0]) is: %p\n", thisenv);
  8000b2:	83 c4 08             	add    $0x8,%esp
  8000b5:	50                   	push   %eax
  8000b6:	68 c4 0d 80 00       	push   $0x800dc4
  8000bb:	e8 cc 00 00 00       	call   80018c <cprintf>
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000c0:	83 c4 10             	add    $0x10,%esp
  8000c3:	85 db                	test   %ebx,%ebx
  8000c5:	7e 07                	jle    8000ce <libmain+0x70>
		binaryname = argv[0];
  8000c7:	8b 06                	mov    (%esi),%eax
  8000c9:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  8000ce:	83 ec 08             	sub    $0x8,%esp
  8000d1:	56                   	push   %esi
  8000d2:	53                   	push   %ebx
  8000d3:	e8 5b ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000d8:	e8 0b 00 00 00       	call   8000e8 <exit>
}
  8000dd:	83 c4 10             	add    $0x10,%esp
  8000e0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8000e3:	5b                   	pop    %ebx
  8000e4:	5e                   	pop    %esi
  8000e5:	5f                   	pop    %edi
  8000e6:	5d                   	pop    %ebp
  8000e7:	c3                   	ret    

008000e8 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000e8:	55                   	push   %ebp
  8000e9:	89 e5                	mov    %esp,%ebp
  8000eb:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000ee:	6a 00                	push   $0x0
  8000f0:	e8 60 09 00 00       	call   800a55 <sys_env_destroy>
}
  8000f5:	83 c4 10             	add    $0x10,%esp
  8000f8:	c9                   	leave  
  8000f9:	c3                   	ret    

008000fa <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000fa:	55                   	push   %ebp
  8000fb:	89 e5                	mov    %esp,%ebp
  8000fd:	53                   	push   %ebx
  8000fe:	83 ec 04             	sub    $0x4,%esp
  800101:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800104:	8b 13                	mov    (%ebx),%edx
  800106:	8d 42 01             	lea    0x1(%edx),%eax
  800109:	89 03                	mov    %eax,(%ebx)
  80010b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80010e:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800112:	3d ff 00 00 00       	cmp    $0xff,%eax
  800117:	75 1a                	jne    800133 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800119:	83 ec 08             	sub    $0x8,%esp
  80011c:	68 ff 00 00 00       	push   $0xff
  800121:	8d 43 08             	lea    0x8(%ebx),%eax
  800124:	50                   	push   %eax
  800125:	e8 ee 08 00 00       	call   800a18 <sys_cputs>
		b->idx = 0;
  80012a:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800130:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800133:	ff 43 04             	incl   0x4(%ebx)
}
  800136:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800139:	c9                   	leave  
  80013a:	c3                   	ret    

0080013b <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80013b:	55                   	push   %ebp
  80013c:	89 e5                	mov    %esp,%ebp
  80013e:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800144:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80014b:	00 00 00 
	b.cnt = 0;
  80014e:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800155:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800158:	ff 75 0c             	pushl  0xc(%ebp)
  80015b:	ff 75 08             	pushl  0x8(%ebp)
  80015e:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800164:	50                   	push   %eax
  800165:	68 fa 00 80 00       	push   $0x8000fa
  80016a:	e8 51 01 00 00       	call   8002c0 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80016f:	83 c4 08             	add    $0x8,%esp
  800172:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800178:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80017e:	50                   	push   %eax
  80017f:	e8 94 08 00 00       	call   800a18 <sys_cputs>

	return b.cnt;
}
  800184:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80018a:	c9                   	leave  
  80018b:	c3                   	ret    

0080018c <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80018c:	55                   	push   %ebp
  80018d:	89 e5                	mov    %esp,%ebp
  80018f:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800192:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800195:	50                   	push   %eax
  800196:	ff 75 08             	pushl  0x8(%ebp)
  800199:	e8 9d ff ff ff       	call   80013b <vcprintf>
	va_end(ap);

	return cnt;
}
  80019e:	c9                   	leave  
  80019f:	c3                   	ret    

008001a0 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8001a0:	55                   	push   %ebp
  8001a1:	89 e5                	mov    %esp,%ebp
  8001a3:	57                   	push   %edi
  8001a4:	56                   	push   %esi
  8001a5:	53                   	push   %ebx
  8001a6:	83 ec 1c             	sub    $0x1c,%esp
  8001a9:	89 c7                	mov    %eax,%edi
  8001ab:	89 d6                	mov    %edx,%esi
  8001ad:	8b 45 08             	mov    0x8(%ebp),%eax
  8001b0:	8b 55 0c             	mov    0xc(%ebp),%edx
  8001b3:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8001b6:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001b9:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8001bc:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001c1:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001c4:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001c7:	39 d3                	cmp    %edx,%ebx
  8001c9:	72 05                	jb     8001d0 <printnum+0x30>
  8001cb:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001ce:	77 45                	ja     800215 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001d0:	83 ec 0c             	sub    $0xc,%esp
  8001d3:	ff 75 18             	pushl  0x18(%ebp)
  8001d6:	8b 45 14             	mov    0x14(%ebp),%eax
  8001d9:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001dc:	53                   	push   %ebx
  8001dd:	ff 75 10             	pushl  0x10(%ebp)
  8001e0:	83 ec 08             	sub    $0x8,%esp
  8001e3:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001e6:	ff 75 e0             	pushl  -0x20(%ebp)
  8001e9:	ff 75 dc             	pushl  -0x24(%ebp)
  8001ec:	ff 75 d8             	pushl  -0x28(%ebp)
  8001ef:	e8 08 09 00 00       	call   800afc <__udivdi3>
  8001f4:	83 c4 18             	add    $0x18,%esp
  8001f7:	52                   	push   %edx
  8001f8:	50                   	push   %eax
  8001f9:	89 f2                	mov    %esi,%edx
  8001fb:	89 f8                	mov    %edi,%eax
  8001fd:	e8 9e ff ff ff       	call   8001a0 <printnum>
  800202:	83 c4 20             	add    $0x20,%esp
  800205:	eb 16                	jmp    80021d <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800207:	83 ec 08             	sub    $0x8,%esp
  80020a:	56                   	push   %esi
  80020b:	ff 75 18             	pushl  0x18(%ebp)
  80020e:	ff d7                	call   *%edi
  800210:	83 c4 10             	add    $0x10,%esp
  800213:	eb 03                	jmp    800218 <printnum+0x78>
  800215:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800218:	4b                   	dec    %ebx
  800219:	85 db                	test   %ebx,%ebx
  80021b:	7f ea                	jg     800207 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80021d:	83 ec 08             	sub    $0x8,%esp
  800220:	56                   	push   %esi
  800221:	83 ec 04             	sub    $0x4,%esp
  800224:	ff 75 e4             	pushl  -0x1c(%ebp)
  800227:	ff 75 e0             	pushl  -0x20(%ebp)
  80022a:	ff 75 dc             	pushl  -0x24(%ebp)
  80022d:	ff 75 d8             	pushl  -0x28(%ebp)
  800230:	e8 d7 09 00 00       	call   800c0c <__umoddi3>
  800235:	83 c4 14             	add    $0x14,%esp
  800238:	0f be 80 e7 0d 80 00 	movsbl 0x800de7(%eax),%eax
  80023f:	50                   	push   %eax
  800240:	ff d7                	call   *%edi
}
  800242:	83 c4 10             	add    $0x10,%esp
  800245:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800248:	5b                   	pop    %ebx
  800249:	5e                   	pop    %esi
  80024a:	5f                   	pop    %edi
  80024b:	5d                   	pop    %ebp
  80024c:	c3                   	ret    

0080024d <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80024d:	55                   	push   %ebp
  80024e:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800250:	83 fa 01             	cmp    $0x1,%edx
  800253:	7e 0e                	jle    800263 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800255:	8b 10                	mov    (%eax),%edx
  800257:	8d 4a 08             	lea    0x8(%edx),%ecx
  80025a:	89 08                	mov    %ecx,(%eax)
  80025c:	8b 02                	mov    (%edx),%eax
  80025e:	8b 52 04             	mov    0x4(%edx),%edx
  800261:	eb 22                	jmp    800285 <getuint+0x38>
	else if (lflag)
  800263:	85 d2                	test   %edx,%edx
  800265:	74 10                	je     800277 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800267:	8b 10                	mov    (%eax),%edx
  800269:	8d 4a 04             	lea    0x4(%edx),%ecx
  80026c:	89 08                	mov    %ecx,(%eax)
  80026e:	8b 02                	mov    (%edx),%eax
  800270:	ba 00 00 00 00       	mov    $0x0,%edx
  800275:	eb 0e                	jmp    800285 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800277:	8b 10                	mov    (%eax),%edx
  800279:	8d 4a 04             	lea    0x4(%edx),%ecx
  80027c:	89 08                	mov    %ecx,(%eax)
  80027e:	8b 02                	mov    (%edx),%eax
  800280:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800285:	5d                   	pop    %ebp
  800286:	c3                   	ret    

00800287 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800287:	55                   	push   %ebp
  800288:	89 e5                	mov    %esp,%ebp
  80028a:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80028d:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800290:	8b 10                	mov    (%eax),%edx
  800292:	3b 50 04             	cmp    0x4(%eax),%edx
  800295:	73 0a                	jae    8002a1 <sprintputch+0x1a>
		*b->buf++ = ch;
  800297:	8d 4a 01             	lea    0x1(%edx),%ecx
  80029a:	89 08                	mov    %ecx,(%eax)
  80029c:	8b 45 08             	mov    0x8(%ebp),%eax
  80029f:	88 02                	mov    %al,(%edx)
}
  8002a1:	5d                   	pop    %ebp
  8002a2:	c3                   	ret    

008002a3 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8002a3:	55                   	push   %ebp
  8002a4:	89 e5                	mov    %esp,%ebp
  8002a6:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8002a9:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8002ac:	50                   	push   %eax
  8002ad:	ff 75 10             	pushl  0x10(%ebp)
  8002b0:	ff 75 0c             	pushl  0xc(%ebp)
  8002b3:	ff 75 08             	pushl  0x8(%ebp)
  8002b6:	e8 05 00 00 00       	call   8002c0 <vprintfmt>
	va_end(ap);
}
  8002bb:	83 c4 10             	add    $0x10,%esp
  8002be:	c9                   	leave  
  8002bf:	c3                   	ret    

008002c0 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8002c0:	55                   	push   %ebp
  8002c1:	89 e5                	mov    %esp,%ebp
  8002c3:	57                   	push   %edi
  8002c4:	56                   	push   %esi
  8002c5:	53                   	push   %ebx
  8002c6:	83 ec 2c             	sub    $0x2c,%esp
  8002c9:	8b 75 08             	mov    0x8(%ebp),%esi
  8002cc:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002cf:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002d2:	eb 12                	jmp    8002e6 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002d4:	85 c0                	test   %eax,%eax
  8002d6:	0f 84 68 03 00 00    	je     800644 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002dc:	83 ec 08             	sub    $0x8,%esp
  8002df:	53                   	push   %ebx
  8002e0:	50                   	push   %eax
  8002e1:	ff d6                	call   *%esi
  8002e3:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002e6:	47                   	inc    %edi
  8002e7:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002eb:	83 f8 25             	cmp    $0x25,%eax
  8002ee:	75 e4                	jne    8002d4 <vprintfmt+0x14>
  8002f0:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002f4:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002fb:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800302:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800309:	ba 00 00 00 00       	mov    $0x0,%edx
  80030e:	eb 07                	jmp    800317 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800310:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800313:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800317:	8d 47 01             	lea    0x1(%edi),%eax
  80031a:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80031d:	0f b6 0f             	movzbl (%edi),%ecx
  800320:	8a 07                	mov    (%edi),%al
  800322:	83 e8 23             	sub    $0x23,%eax
  800325:	3c 55                	cmp    $0x55,%al
  800327:	0f 87 fe 02 00 00    	ja     80062b <vprintfmt+0x36b>
  80032d:	0f b6 c0             	movzbl %al,%eax
  800330:	ff 24 85 74 0e 80 00 	jmp    *0x800e74(,%eax,4)
  800337:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80033a:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80033e:	eb d7                	jmp    800317 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800340:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800343:	b8 00 00 00 00       	mov    $0x0,%eax
  800348:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80034b:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80034e:	01 c0                	add    %eax,%eax
  800350:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800354:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800357:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80035a:	83 fa 09             	cmp    $0x9,%edx
  80035d:	77 34                	ja     800393 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80035f:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800360:	eb e9                	jmp    80034b <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800362:	8b 45 14             	mov    0x14(%ebp),%eax
  800365:	8d 48 04             	lea    0x4(%eax),%ecx
  800368:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80036b:	8b 00                	mov    (%eax),%eax
  80036d:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800370:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800373:	eb 24                	jmp    800399 <vprintfmt+0xd9>
  800375:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800379:	79 07                	jns    800382 <vprintfmt+0xc2>
  80037b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800382:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800385:	eb 90                	jmp    800317 <vprintfmt+0x57>
  800387:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80038a:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800391:	eb 84                	jmp    800317 <vprintfmt+0x57>
  800393:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800396:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800399:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80039d:	0f 89 74 ff ff ff    	jns    800317 <vprintfmt+0x57>
				width = precision, precision = -1;
  8003a3:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8003a6:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8003a9:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003b0:	e9 62 ff ff ff       	jmp    800317 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8003b5:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003b6:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8003b9:	e9 59 ff ff ff       	jmp    800317 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8003be:	8b 45 14             	mov    0x14(%ebp),%eax
  8003c1:	8d 50 04             	lea    0x4(%eax),%edx
  8003c4:	89 55 14             	mov    %edx,0x14(%ebp)
  8003c7:	83 ec 08             	sub    $0x8,%esp
  8003ca:	53                   	push   %ebx
  8003cb:	ff 30                	pushl  (%eax)
  8003cd:	ff d6                	call   *%esi
			break;
  8003cf:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003d2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003d5:	e9 0c ff ff ff       	jmp    8002e6 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003da:	8b 45 14             	mov    0x14(%ebp),%eax
  8003dd:	8d 50 04             	lea    0x4(%eax),%edx
  8003e0:	89 55 14             	mov    %edx,0x14(%ebp)
  8003e3:	8b 00                	mov    (%eax),%eax
  8003e5:	85 c0                	test   %eax,%eax
  8003e7:	79 02                	jns    8003eb <vprintfmt+0x12b>
  8003e9:	f7 d8                	neg    %eax
  8003eb:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003ed:	83 f8 06             	cmp    $0x6,%eax
  8003f0:	7f 0b                	jg     8003fd <vprintfmt+0x13d>
  8003f2:	8b 04 85 cc 0f 80 00 	mov    0x800fcc(,%eax,4),%eax
  8003f9:	85 c0                	test   %eax,%eax
  8003fb:	75 18                	jne    800415 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003fd:	52                   	push   %edx
  8003fe:	68 ff 0d 80 00       	push   $0x800dff
  800403:	53                   	push   %ebx
  800404:	56                   	push   %esi
  800405:	e8 99 fe ff ff       	call   8002a3 <printfmt>
  80040a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80040d:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800410:	e9 d1 fe ff ff       	jmp    8002e6 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800415:	50                   	push   %eax
  800416:	68 08 0e 80 00       	push   $0x800e08
  80041b:	53                   	push   %ebx
  80041c:	56                   	push   %esi
  80041d:	e8 81 fe ff ff       	call   8002a3 <printfmt>
  800422:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800425:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800428:	e9 b9 fe ff ff       	jmp    8002e6 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80042d:	8b 45 14             	mov    0x14(%ebp),%eax
  800430:	8d 50 04             	lea    0x4(%eax),%edx
  800433:	89 55 14             	mov    %edx,0x14(%ebp)
  800436:	8b 38                	mov    (%eax),%edi
  800438:	85 ff                	test   %edi,%edi
  80043a:	75 05                	jne    800441 <vprintfmt+0x181>
				p = "(null)";
  80043c:	bf f8 0d 80 00       	mov    $0x800df8,%edi
			if (width > 0 && padc != '-')
  800441:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800445:	0f 8e 90 00 00 00    	jle    8004db <vprintfmt+0x21b>
  80044b:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80044f:	0f 84 8e 00 00 00    	je     8004e3 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800455:	83 ec 08             	sub    $0x8,%esp
  800458:	ff 75 d0             	pushl  -0x30(%ebp)
  80045b:	57                   	push   %edi
  80045c:	e8 70 02 00 00       	call   8006d1 <strnlen>
  800461:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800464:	29 c1                	sub    %eax,%ecx
  800466:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800469:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80046c:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800470:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800473:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800476:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800478:	eb 0d                	jmp    800487 <vprintfmt+0x1c7>
					putch(padc, putdat);
  80047a:	83 ec 08             	sub    $0x8,%esp
  80047d:	53                   	push   %ebx
  80047e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800481:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800483:	4f                   	dec    %edi
  800484:	83 c4 10             	add    $0x10,%esp
  800487:	85 ff                	test   %edi,%edi
  800489:	7f ef                	jg     80047a <vprintfmt+0x1ba>
  80048b:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80048e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800491:	89 c8                	mov    %ecx,%eax
  800493:	85 c9                	test   %ecx,%ecx
  800495:	79 05                	jns    80049c <vprintfmt+0x1dc>
  800497:	b8 00 00 00 00       	mov    $0x0,%eax
  80049c:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80049f:	29 c1                	sub    %eax,%ecx
  8004a1:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8004a4:	89 75 08             	mov    %esi,0x8(%ebp)
  8004a7:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004aa:	eb 3d                	jmp    8004e9 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8004ac:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8004b0:	74 19                	je     8004cb <vprintfmt+0x20b>
  8004b2:	0f be c0             	movsbl %al,%eax
  8004b5:	83 e8 20             	sub    $0x20,%eax
  8004b8:	83 f8 5e             	cmp    $0x5e,%eax
  8004bb:	76 0e                	jbe    8004cb <vprintfmt+0x20b>
					putch('?', putdat);
  8004bd:	83 ec 08             	sub    $0x8,%esp
  8004c0:	53                   	push   %ebx
  8004c1:	6a 3f                	push   $0x3f
  8004c3:	ff 55 08             	call   *0x8(%ebp)
  8004c6:	83 c4 10             	add    $0x10,%esp
  8004c9:	eb 0b                	jmp    8004d6 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004cb:	83 ec 08             	sub    $0x8,%esp
  8004ce:	53                   	push   %ebx
  8004cf:	52                   	push   %edx
  8004d0:	ff 55 08             	call   *0x8(%ebp)
  8004d3:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004d6:	ff 4d e4             	decl   -0x1c(%ebp)
  8004d9:	eb 0e                	jmp    8004e9 <vprintfmt+0x229>
  8004db:	89 75 08             	mov    %esi,0x8(%ebp)
  8004de:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004e1:	eb 06                	jmp    8004e9 <vprintfmt+0x229>
  8004e3:	89 75 08             	mov    %esi,0x8(%ebp)
  8004e6:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004e9:	47                   	inc    %edi
  8004ea:	8a 47 ff             	mov    -0x1(%edi),%al
  8004ed:	0f be d0             	movsbl %al,%edx
  8004f0:	85 d2                	test   %edx,%edx
  8004f2:	74 1d                	je     800511 <vprintfmt+0x251>
  8004f4:	85 f6                	test   %esi,%esi
  8004f6:	78 b4                	js     8004ac <vprintfmt+0x1ec>
  8004f8:	4e                   	dec    %esi
  8004f9:	79 b1                	jns    8004ac <vprintfmt+0x1ec>
  8004fb:	8b 75 08             	mov    0x8(%ebp),%esi
  8004fe:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800501:	eb 14                	jmp    800517 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800503:	83 ec 08             	sub    $0x8,%esp
  800506:	53                   	push   %ebx
  800507:	6a 20                	push   $0x20
  800509:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80050b:	4f                   	dec    %edi
  80050c:	83 c4 10             	add    $0x10,%esp
  80050f:	eb 06                	jmp    800517 <vprintfmt+0x257>
  800511:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800514:	8b 75 08             	mov    0x8(%ebp),%esi
  800517:	85 ff                	test   %edi,%edi
  800519:	7f e8                	jg     800503 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80051b:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80051e:	e9 c3 fd ff ff       	jmp    8002e6 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800523:	83 fa 01             	cmp    $0x1,%edx
  800526:	7e 16                	jle    80053e <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800528:	8b 45 14             	mov    0x14(%ebp),%eax
  80052b:	8d 50 08             	lea    0x8(%eax),%edx
  80052e:	89 55 14             	mov    %edx,0x14(%ebp)
  800531:	8b 50 04             	mov    0x4(%eax),%edx
  800534:	8b 00                	mov    (%eax),%eax
  800536:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800539:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80053c:	eb 32                	jmp    800570 <vprintfmt+0x2b0>
	else if (lflag)
  80053e:	85 d2                	test   %edx,%edx
  800540:	74 18                	je     80055a <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800542:	8b 45 14             	mov    0x14(%ebp),%eax
  800545:	8d 50 04             	lea    0x4(%eax),%edx
  800548:	89 55 14             	mov    %edx,0x14(%ebp)
  80054b:	8b 00                	mov    (%eax),%eax
  80054d:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800550:	89 c1                	mov    %eax,%ecx
  800552:	c1 f9 1f             	sar    $0x1f,%ecx
  800555:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800558:	eb 16                	jmp    800570 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80055a:	8b 45 14             	mov    0x14(%ebp),%eax
  80055d:	8d 50 04             	lea    0x4(%eax),%edx
  800560:	89 55 14             	mov    %edx,0x14(%ebp)
  800563:	8b 00                	mov    (%eax),%eax
  800565:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800568:	89 c1                	mov    %eax,%ecx
  80056a:	c1 f9 1f             	sar    $0x1f,%ecx
  80056d:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800570:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800573:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800576:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80057a:	79 76                	jns    8005f2 <vprintfmt+0x332>
				putch('-', putdat);
  80057c:	83 ec 08             	sub    $0x8,%esp
  80057f:	53                   	push   %ebx
  800580:	6a 2d                	push   $0x2d
  800582:	ff d6                	call   *%esi
				num = -(long long) num;
  800584:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800587:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80058a:	f7 d8                	neg    %eax
  80058c:	83 d2 00             	adc    $0x0,%edx
  80058f:	f7 da                	neg    %edx
  800591:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800594:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800599:	eb 5c                	jmp    8005f7 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80059b:	8d 45 14             	lea    0x14(%ebp),%eax
  80059e:	e8 aa fc ff ff       	call   80024d <getuint>
			base = 10;
  8005a3:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8005a8:	eb 4d                	jmp    8005f7 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8005aa:	8d 45 14             	lea    0x14(%ebp),%eax
  8005ad:	e8 9b fc ff ff       	call   80024d <getuint>
			base = 8;
  8005b2:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8005b7:	eb 3e                	jmp    8005f7 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8005b9:	83 ec 08             	sub    $0x8,%esp
  8005bc:	53                   	push   %ebx
  8005bd:	6a 30                	push   $0x30
  8005bf:	ff d6                	call   *%esi
			putch('x', putdat);
  8005c1:	83 c4 08             	add    $0x8,%esp
  8005c4:	53                   	push   %ebx
  8005c5:	6a 78                	push   $0x78
  8005c7:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005c9:	8b 45 14             	mov    0x14(%ebp),%eax
  8005cc:	8d 50 04             	lea    0x4(%eax),%edx
  8005cf:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005d2:	8b 00                	mov    (%eax),%eax
  8005d4:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005d9:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005dc:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005e1:	eb 14                	jmp    8005f7 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005e3:	8d 45 14             	lea    0x14(%ebp),%eax
  8005e6:	e8 62 fc ff ff       	call   80024d <getuint>
			base = 16;
  8005eb:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005f0:	eb 05                	jmp    8005f7 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005f2:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005f7:	83 ec 0c             	sub    $0xc,%esp
  8005fa:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005fe:	57                   	push   %edi
  8005ff:	ff 75 e4             	pushl  -0x1c(%ebp)
  800602:	51                   	push   %ecx
  800603:	52                   	push   %edx
  800604:	50                   	push   %eax
  800605:	89 da                	mov    %ebx,%edx
  800607:	89 f0                	mov    %esi,%eax
  800609:	e8 92 fb ff ff       	call   8001a0 <printnum>
			break;
  80060e:	83 c4 20             	add    $0x20,%esp
  800611:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800614:	e9 cd fc ff ff       	jmp    8002e6 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800619:	83 ec 08             	sub    $0x8,%esp
  80061c:	53                   	push   %ebx
  80061d:	51                   	push   %ecx
  80061e:	ff d6                	call   *%esi
			break;
  800620:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800623:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800626:	e9 bb fc ff ff       	jmp    8002e6 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80062b:	83 ec 08             	sub    $0x8,%esp
  80062e:	53                   	push   %ebx
  80062f:	6a 25                	push   $0x25
  800631:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800633:	83 c4 10             	add    $0x10,%esp
  800636:	eb 01                	jmp    800639 <vprintfmt+0x379>
  800638:	4f                   	dec    %edi
  800639:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  80063d:	75 f9                	jne    800638 <vprintfmt+0x378>
  80063f:	e9 a2 fc ff ff       	jmp    8002e6 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800644:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800647:	5b                   	pop    %ebx
  800648:	5e                   	pop    %esi
  800649:	5f                   	pop    %edi
  80064a:	5d                   	pop    %ebp
  80064b:	c3                   	ret    

0080064c <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80064c:	55                   	push   %ebp
  80064d:	89 e5                	mov    %esp,%ebp
  80064f:	83 ec 18             	sub    $0x18,%esp
  800652:	8b 45 08             	mov    0x8(%ebp),%eax
  800655:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800658:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80065b:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80065f:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800662:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800669:	85 c0                	test   %eax,%eax
  80066b:	74 26                	je     800693 <vsnprintf+0x47>
  80066d:	85 d2                	test   %edx,%edx
  80066f:	7e 29                	jle    80069a <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800671:	ff 75 14             	pushl  0x14(%ebp)
  800674:	ff 75 10             	pushl  0x10(%ebp)
  800677:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80067a:	50                   	push   %eax
  80067b:	68 87 02 80 00       	push   $0x800287
  800680:	e8 3b fc ff ff       	call   8002c0 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800685:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800688:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80068b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80068e:	83 c4 10             	add    $0x10,%esp
  800691:	eb 0c                	jmp    80069f <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800693:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800698:	eb 05                	jmp    80069f <vsnprintf+0x53>
  80069a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80069f:	c9                   	leave  
  8006a0:	c3                   	ret    

008006a1 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8006a1:	55                   	push   %ebp
  8006a2:	89 e5                	mov    %esp,%ebp
  8006a4:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8006a7:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8006aa:	50                   	push   %eax
  8006ab:	ff 75 10             	pushl  0x10(%ebp)
  8006ae:	ff 75 0c             	pushl  0xc(%ebp)
  8006b1:	ff 75 08             	pushl  0x8(%ebp)
  8006b4:	e8 93 ff ff ff       	call   80064c <vsnprintf>
	va_end(ap);

	return rc;
}
  8006b9:	c9                   	leave  
  8006ba:	c3                   	ret    

008006bb <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8006bb:	55                   	push   %ebp
  8006bc:	89 e5                	mov    %esp,%ebp
  8006be:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8006c1:	b8 00 00 00 00       	mov    $0x0,%eax
  8006c6:	eb 01                	jmp    8006c9 <strlen+0xe>
		n++;
  8006c8:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006c9:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006cd:	75 f9                	jne    8006c8 <strlen+0xd>
		n++;
	return n;
}
  8006cf:	5d                   	pop    %ebp
  8006d0:	c3                   	ret    

008006d1 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006d1:	55                   	push   %ebp
  8006d2:	89 e5                	mov    %esp,%ebp
  8006d4:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006d7:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006da:	ba 00 00 00 00       	mov    $0x0,%edx
  8006df:	eb 01                	jmp    8006e2 <strnlen+0x11>
		n++;
  8006e1:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006e2:	39 c2                	cmp    %eax,%edx
  8006e4:	74 08                	je     8006ee <strnlen+0x1d>
  8006e6:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006ea:	75 f5                	jne    8006e1 <strnlen+0x10>
  8006ec:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006ee:	5d                   	pop    %ebp
  8006ef:	c3                   	ret    

008006f0 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006f0:	55                   	push   %ebp
  8006f1:	89 e5                	mov    %esp,%ebp
  8006f3:	53                   	push   %ebx
  8006f4:	8b 45 08             	mov    0x8(%ebp),%eax
  8006f7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006fa:	89 c2                	mov    %eax,%edx
  8006fc:	42                   	inc    %edx
  8006fd:	41                   	inc    %ecx
  8006fe:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800701:	88 5a ff             	mov    %bl,-0x1(%edx)
  800704:	84 db                	test   %bl,%bl
  800706:	75 f4                	jne    8006fc <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800708:	5b                   	pop    %ebx
  800709:	5d                   	pop    %ebp
  80070a:	c3                   	ret    

0080070b <strcat>:

char *
strcat(char *dst, const char *src)
{
  80070b:	55                   	push   %ebp
  80070c:	89 e5                	mov    %esp,%ebp
  80070e:	53                   	push   %ebx
  80070f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800712:	53                   	push   %ebx
  800713:	e8 a3 ff ff ff       	call   8006bb <strlen>
  800718:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80071b:	ff 75 0c             	pushl  0xc(%ebp)
  80071e:	01 d8                	add    %ebx,%eax
  800720:	50                   	push   %eax
  800721:	e8 ca ff ff ff       	call   8006f0 <strcpy>
	return dst;
}
  800726:	89 d8                	mov    %ebx,%eax
  800728:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80072b:	c9                   	leave  
  80072c:	c3                   	ret    

0080072d <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80072d:	55                   	push   %ebp
  80072e:	89 e5                	mov    %esp,%ebp
  800730:	56                   	push   %esi
  800731:	53                   	push   %ebx
  800732:	8b 75 08             	mov    0x8(%ebp),%esi
  800735:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800738:	89 f3                	mov    %esi,%ebx
  80073a:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80073d:	89 f2                	mov    %esi,%edx
  80073f:	eb 0c                	jmp    80074d <strncpy+0x20>
		*dst++ = *src;
  800741:	42                   	inc    %edx
  800742:	8a 01                	mov    (%ecx),%al
  800744:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800747:	80 39 01             	cmpb   $0x1,(%ecx)
  80074a:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80074d:	39 da                	cmp    %ebx,%edx
  80074f:	75 f0                	jne    800741 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800751:	89 f0                	mov    %esi,%eax
  800753:	5b                   	pop    %ebx
  800754:	5e                   	pop    %esi
  800755:	5d                   	pop    %ebp
  800756:	c3                   	ret    

00800757 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800757:	55                   	push   %ebp
  800758:	89 e5                	mov    %esp,%ebp
  80075a:	56                   	push   %esi
  80075b:	53                   	push   %ebx
  80075c:	8b 75 08             	mov    0x8(%ebp),%esi
  80075f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800762:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800765:	85 c0                	test   %eax,%eax
  800767:	74 1e                	je     800787 <strlcpy+0x30>
  800769:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  80076d:	89 f2                	mov    %esi,%edx
  80076f:	eb 05                	jmp    800776 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800771:	42                   	inc    %edx
  800772:	41                   	inc    %ecx
  800773:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800776:	39 c2                	cmp    %eax,%edx
  800778:	74 08                	je     800782 <strlcpy+0x2b>
  80077a:	8a 19                	mov    (%ecx),%bl
  80077c:	84 db                	test   %bl,%bl
  80077e:	75 f1                	jne    800771 <strlcpy+0x1a>
  800780:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800782:	c6 00 00             	movb   $0x0,(%eax)
  800785:	eb 02                	jmp    800789 <strlcpy+0x32>
  800787:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800789:	29 f0                	sub    %esi,%eax
}
  80078b:	5b                   	pop    %ebx
  80078c:	5e                   	pop    %esi
  80078d:	5d                   	pop    %ebp
  80078e:	c3                   	ret    

0080078f <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80078f:	55                   	push   %ebp
  800790:	89 e5                	mov    %esp,%ebp
  800792:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800795:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800798:	eb 02                	jmp    80079c <strcmp+0xd>
		p++, q++;
  80079a:	41                   	inc    %ecx
  80079b:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80079c:	8a 01                	mov    (%ecx),%al
  80079e:	84 c0                	test   %al,%al
  8007a0:	74 04                	je     8007a6 <strcmp+0x17>
  8007a2:	3a 02                	cmp    (%edx),%al
  8007a4:	74 f4                	je     80079a <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  8007a6:	0f b6 c0             	movzbl %al,%eax
  8007a9:	0f b6 12             	movzbl (%edx),%edx
  8007ac:	29 d0                	sub    %edx,%eax
}
  8007ae:	5d                   	pop    %ebp
  8007af:	c3                   	ret    

008007b0 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  8007b0:	55                   	push   %ebp
  8007b1:	89 e5                	mov    %esp,%ebp
  8007b3:	53                   	push   %ebx
  8007b4:	8b 45 08             	mov    0x8(%ebp),%eax
  8007b7:	8b 55 0c             	mov    0xc(%ebp),%edx
  8007ba:	89 c3                	mov    %eax,%ebx
  8007bc:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8007bf:	eb 02                	jmp    8007c3 <strncmp+0x13>
		n--, p++, q++;
  8007c1:	40                   	inc    %eax
  8007c2:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8007c3:	39 d8                	cmp    %ebx,%eax
  8007c5:	74 14                	je     8007db <strncmp+0x2b>
  8007c7:	8a 08                	mov    (%eax),%cl
  8007c9:	84 c9                	test   %cl,%cl
  8007cb:	74 04                	je     8007d1 <strncmp+0x21>
  8007cd:	3a 0a                	cmp    (%edx),%cl
  8007cf:	74 f0                	je     8007c1 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007d1:	0f b6 00             	movzbl (%eax),%eax
  8007d4:	0f b6 12             	movzbl (%edx),%edx
  8007d7:	29 d0                	sub    %edx,%eax
  8007d9:	eb 05                	jmp    8007e0 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007db:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007e0:	5b                   	pop    %ebx
  8007e1:	5d                   	pop    %ebp
  8007e2:	c3                   	ret    

008007e3 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007e3:	55                   	push   %ebp
  8007e4:	89 e5                	mov    %esp,%ebp
  8007e6:	8b 45 08             	mov    0x8(%ebp),%eax
  8007e9:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007ec:	eb 05                	jmp    8007f3 <strchr+0x10>
		if (*s == c)
  8007ee:	38 ca                	cmp    %cl,%dl
  8007f0:	74 0c                	je     8007fe <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007f2:	40                   	inc    %eax
  8007f3:	8a 10                	mov    (%eax),%dl
  8007f5:	84 d2                	test   %dl,%dl
  8007f7:	75 f5                	jne    8007ee <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007f9:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007fe:	5d                   	pop    %ebp
  8007ff:	c3                   	ret    

00800800 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800800:	55                   	push   %ebp
  800801:	89 e5                	mov    %esp,%ebp
  800803:	8b 45 08             	mov    0x8(%ebp),%eax
  800806:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800809:	eb 05                	jmp    800810 <strfind+0x10>
		if (*s == c)
  80080b:	38 ca                	cmp    %cl,%dl
  80080d:	74 07                	je     800816 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  80080f:	40                   	inc    %eax
  800810:	8a 10                	mov    (%eax),%dl
  800812:	84 d2                	test   %dl,%dl
  800814:	75 f5                	jne    80080b <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800816:	5d                   	pop    %ebp
  800817:	c3                   	ret    

00800818 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800818:	55                   	push   %ebp
  800819:	89 e5                	mov    %esp,%ebp
  80081b:	57                   	push   %edi
  80081c:	56                   	push   %esi
  80081d:	53                   	push   %ebx
  80081e:	8b 7d 08             	mov    0x8(%ebp),%edi
  800821:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800824:	85 c9                	test   %ecx,%ecx
  800826:	74 36                	je     80085e <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800828:	f7 c7 03 00 00 00    	test   $0x3,%edi
  80082e:	75 28                	jne    800858 <memset+0x40>
  800830:	f6 c1 03             	test   $0x3,%cl
  800833:	75 23                	jne    800858 <memset+0x40>
		c &= 0xFF;
  800835:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800839:	89 d3                	mov    %edx,%ebx
  80083b:	c1 e3 08             	shl    $0x8,%ebx
  80083e:	89 d6                	mov    %edx,%esi
  800840:	c1 e6 18             	shl    $0x18,%esi
  800843:	89 d0                	mov    %edx,%eax
  800845:	c1 e0 10             	shl    $0x10,%eax
  800848:	09 f0                	or     %esi,%eax
  80084a:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  80084c:	89 d8                	mov    %ebx,%eax
  80084e:	09 d0                	or     %edx,%eax
  800850:	c1 e9 02             	shr    $0x2,%ecx
  800853:	fc                   	cld    
  800854:	f3 ab                	rep stos %eax,%es:(%edi)
  800856:	eb 06                	jmp    80085e <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800858:	8b 45 0c             	mov    0xc(%ebp),%eax
  80085b:	fc                   	cld    
  80085c:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  80085e:	89 f8                	mov    %edi,%eax
  800860:	5b                   	pop    %ebx
  800861:	5e                   	pop    %esi
  800862:	5f                   	pop    %edi
  800863:	5d                   	pop    %ebp
  800864:	c3                   	ret    

00800865 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800865:	55                   	push   %ebp
  800866:	89 e5                	mov    %esp,%ebp
  800868:	57                   	push   %edi
  800869:	56                   	push   %esi
  80086a:	8b 45 08             	mov    0x8(%ebp),%eax
  80086d:	8b 75 0c             	mov    0xc(%ebp),%esi
  800870:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800873:	39 c6                	cmp    %eax,%esi
  800875:	73 33                	jae    8008aa <memmove+0x45>
  800877:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80087a:	39 d0                	cmp    %edx,%eax
  80087c:	73 2c                	jae    8008aa <memmove+0x45>
		s += n;
		d += n;
  80087e:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800881:	89 d6                	mov    %edx,%esi
  800883:	09 fe                	or     %edi,%esi
  800885:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80088b:	75 13                	jne    8008a0 <memmove+0x3b>
  80088d:	f6 c1 03             	test   $0x3,%cl
  800890:	75 0e                	jne    8008a0 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800892:	83 ef 04             	sub    $0x4,%edi
  800895:	8d 72 fc             	lea    -0x4(%edx),%esi
  800898:	c1 e9 02             	shr    $0x2,%ecx
  80089b:	fd                   	std    
  80089c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80089e:	eb 07                	jmp    8008a7 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  8008a0:	4f                   	dec    %edi
  8008a1:	8d 72 ff             	lea    -0x1(%edx),%esi
  8008a4:	fd                   	std    
  8008a5:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  8008a7:	fc                   	cld    
  8008a8:	eb 1d                	jmp    8008c7 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008aa:	89 f2                	mov    %esi,%edx
  8008ac:	09 c2                	or     %eax,%edx
  8008ae:	f6 c2 03             	test   $0x3,%dl
  8008b1:	75 0f                	jne    8008c2 <memmove+0x5d>
  8008b3:	f6 c1 03             	test   $0x3,%cl
  8008b6:	75 0a                	jne    8008c2 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  8008b8:	c1 e9 02             	shr    $0x2,%ecx
  8008bb:	89 c7                	mov    %eax,%edi
  8008bd:	fc                   	cld    
  8008be:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008c0:	eb 05                	jmp    8008c7 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8008c2:	89 c7                	mov    %eax,%edi
  8008c4:	fc                   	cld    
  8008c5:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008c7:	5e                   	pop    %esi
  8008c8:	5f                   	pop    %edi
  8008c9:	5d                   	pop    %ebp
  8008ca:	c3                   	ret    

008008cb <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008cb:	55                   	push   %ebp
  8008cc:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008ce:	ff 75 10             	pushl  0x10(%ebp)
  8008d1:	ff 75 0c             	pushl  0xc(%ebp)
  8008d4:	ff 75 08             	pushl  0x8(%ebp)
  8008d7:	e8 89 ff ff ff       	call   800865 <memmove>
}
  8008dc:	c9                   	leave  
  8008dd:	c3                   	ret    

008008de <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008de:	55                   	push   %ebp
  8008df:	89 e5                	mov    %esp,%ebp
  8008e1:	56                   	push   %esi
  8008e2:	53                   	push   %ebx
  8008e3:	8b 45 08             	mov    0x8(%ebp),%eax
  8008e6:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008e9:	89 c6                	mov    %eax,%esi
  8008eb:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008ee:	eb 14                	jmp    800904 <memcmp+0x26>
		if (*s1 != *s2)
  8008f0:	8a 08                	mov    (%eax),%cl
  8008f2:	8a 1a                	mov    (%edx),%bl
  8008f4:	38 d9                	cmp    %bl,%cl
  8008f6:	74 0a                	je     800902 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008f8:	0f b6 c1             	movzbl %cl,%eax
  8008fb:	0f b6 db             	movzbl %bl,%ebx
  8008fe:	29 d8                	sub    %ebx,%eax
  800900:	eb 0b                	jmp    80090d <memcmp+0x2f>
		s1++, s2++;
  800902:	40                   	inc    %eax
  800903:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800904:	39 f0                	cmp    %esi,%eax
  800906:	75 e8                	jne    8008f0 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800908:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80090d:	5b                   	pop    %ebx
  80090e:	5e                   	pop    %esi
  80090f:	5d                   	pop    %ebp
  800910:	c3                   	ret    

00800911 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800911:	55                   	push   %ebp
  800912:	89 e5                	mov    %esp,%ebp
  800914:	53                   	push   %ebx
  800915:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800918:	89 c1                	mov    %eax,%ecx
  80091a:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  80091d:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800921:	eb 08                	jmp    80092b <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800923:	0f b6 10             	movzbl (%eax),%edx
  800926:	39 da                	cmp    %ebx,%edx
  800928:	74 05                	je     80092f <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  80092a:	40                   	inc    %eax
  80092b:	39 c8                	cmp    %ecx,%eax
  80092d:	72 f4                	jb     800923 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  80092f:	5b                   	pop    %ebx
  800930:	5d                   	pop    %ebp
  800931:	c3                   	ret    

00800932 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800932:	55                   	push   %ebp
  800933:	89 e5                	mov    %esp,%ebp
  800935:	57                   	push   %edi
  800936:	56                   	push   %esi
  800937:	53                   	push   %ebx
  800938:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80093b:	eb 01                	jmp    80093e <strtol+0xc>
		s++;
  80093d:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80093e:	8a 01                	mov    (%ecx),%al
  800940:	3c 20                	cmp    $0x20,%al
  800942:	74 f9                	je     80093d <strtol+0xb>
  800944:	3c 09                	cmp    $0x9,%al
  800946:	74 f5                	je     80093d <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800948:	3c 2b                	cmp    $0x2b,%al
  80094a:	75 08                	jne    800954 <strtol+0x22>
		s++;
  80094c:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  80094d:	bf 00 00 00 00       	mov    $0x0,%edi
  800952:	eb 11                	jmp    800965 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800954:	3c 2d                	cmp    $0x2d,%al
  800956:	75 08                	jne    800960 <strtol+0x2e>
		s++, neg = 1;
  800958:	41                   	inc    %ecx
  800959:	bf 01 00 00 00       	mov    $0x1,%edi
  80095e:	eb 05                	jmp    800965 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800960:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800965:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800969:	0f 84 87 00 00 00    	je     8009f6 <strtol+0xc4>
  80096f:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800973:	75 27                	jne    80099c <strtol+0x6a>
  800975:	80 39 30             	cmpb   $0x30,(%ecx)
  800978:	75 22                	jne    80099c <strtol+0x6a>
  80097a:	e9 88 00 00 00       	jmp    800a07 <strtol+0xd5>
		s += 2, base = 16;
  80097f:	83 c1 02             	add    $0x2,%ecx
  800982:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800989:	eb 11                	jmp    80099c <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  80098b:	41                   	inc    %ecx
  80098c:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800993:	eb 07                	jmp    80099c <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800995:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  80099c:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  8009a1:	8a 11                	mov    (%ecx),%dl
  8009a3:	8d 5a d0             	lea    -0x30(%edx),%ebx
  8009a6:	80 fb 09             	cmp    $0x9,%bl
  8009a9:	77 08                	ja     8009b3 <strtol+0x81>
			dig = *s - '0';
  8009ab:	0f be d2             	movsbl %dl,%edx
  8009ae:	83 ea 30             	sub    $0x30,%edx
  8009b1:	eb 22                	jmp    8009d5 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  8009b3:	8d 72 9f             	lea    -0x61(%edx),%esi
  8009b6:	89 f3                	mov    %esi,%ebx
  8009b8:	80 fb 19             	cmp    $0x19,%bl
  8009bb:	77 08                	ja     8009c5 <strtol+0x93>
			dig = *s - 'a' + 10;
  8009bd:	0f be d2             	movsbl %dl,%edx
  8009c0:	83 ea 57             	sub    $0x57,%edx
  8009c3:	eb 10                	jmp    8009d5 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009c5:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009c8:	89 f3                	mov    %esi,%ebx
  8009ca:	80 fb 19             	cmp    $0x19,%bl
  8009cd:	77 14                	ja     8009e3 <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009cf:	0f be d2             	movsbl %dl,%edx
  8009d2:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009d5:	3b 55 10             	cmp    0x10(%ebp),%edx
  8009d8:	7d 09                	jge    8009e3 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009da:	41                   	inc    %ecx
  8009db:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009df:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009e1:	eb be                	jmp    8009a1 <strtol+0x6f>

	if (endptr)
  8009e3:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009e7:	74 05                	je     8009ee <strtol+0xbc>
		*endptr = (char *) s;
  8009e9:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009ec:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009ee:	85 ff                	test   %edi,%edi
  8009f0:	74 21                	je     800a13 <strtol+0xe1>
  8009f2:	f7 d8                	neg    %eax
  8009f4:	eb 1d                	jmp    800a13 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009f6:	80 39 30             	cmpb   $0x30,(%ecx)
  8009f9:	75 9a                	jne    800995 <strtol+0x63>
  8009fb:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009ff:	0f 84 7a ff ff ff    	je     80097f <strtol+0x4d>
  800a05:	eb 84                	jmp    80098b <strtol+0x59>
  800a07:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a0b:	0f 84 6e ff ff ff    	je     80097f <strtol+0x4d>
  800a11:	eb 89                	jmp    80099c <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a13:	5b                   	pop    %ebx
  800a14:	5e                   	pop    %esi
  800a15:	5f                   	pop    %edi
  800a16:	5d                   	pop    %ebp
  800a17:	c3                   	ret    

00800a18 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a18:	55                   	push   %ebp
  800a19:	89 e5                	mov    %esp,%ebp
  800a1b:	57                   	push   %edi
  800a1c:	56                   	push   %esi
  800a1d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a1e:	b8 00 00 00 00       	mov    $0x0,%eax
  800a23:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a26:	8b 55 08             	mov    0x8(%ebp),%edx
  800a29:	89 c3                	mov    %eax,%ebx
  800a2b:	89 c7                	mov    %eax,%edi
  800a2d:	89 c6                	mov    %eax,%esi
  800a2f:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a31:	5b                   	pop    %ebx
  800a32:	5e                   	pop    %esi
  800a33:	5f                   	pop    %edi
  800a34:	5d                   	pop    %ebp
  800a35:	c3                   	ret    

00800a36 <sys_cgetc>:

int
sys_cgetc(void)
{
  800a36:	55                   	push   %ebp
  800a37:	89 e5                	mov    %esp,%ebp
  800a39:	57                   	push   %edi
  800a3a:	56                   	push   %esi
  800a3b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a3c:	ba 00 00 00 00       	mov    $0x0,%edx
  800a41:	b8 01 00 00 00       	mov    $0x1,%eax
  800a46:	89 d1                	mov    %edx,%ecx
  800a48:	89 d3                	mov    %edx,%ebx
  800a4a:	89 d7                	mov    %edx,%edi
  800a4c:	89 d6                	mov    %edx,%esi
  800a4e:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a50:	5b                   	pop    %ebx
  800a51:	5e                   	pop    %esi
  800a52:	5f                   	pop    %edi
  800a53:	5d                   	pop    %ebp
  800a54:	c3                   	ret    

00800a55 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a55:	55                   	push   %ebp
  800a56:	89 e5                	mov    %esp,%ebp
  800a58:	57                   	push   %edi
  800a59:	56                   	push   %esi
  800a5a:	53                   	push   %ebx
  800a5b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a5e:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a63:	b8 03 00 00 00       	mov    $0x3,%eax
  800a68:	8b 55 08             	mov    0x8(%ebp),%edx
  800a6b:	89 cb                	mov    %ecx,%ebx
  800a6d:	89 cf                	mov    %ecx,%edi
  800a6f:	89 ce                	mov    %ecx,%esi
  800a71:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a73:	85 c0                	test   %eax,%eax
  800a75:	7e 17                	jle    800a8e <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a77:	83 ec 0c             	sub    $0xc,%esp
  800a7a:	50                   	push   %eax
  800a7b:	6a 03                	push   $0x3
  800a7d:	68 e8 0f 80 00       	push   $0x800fe8
  800a82:	6a 23                	push   $0x23
  800a84:	68 05 10 80 00       	push   $0x801005
  800a89:	e8 27 00 00 00       	call   800ab5 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a8e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a91:	5b                   	pop    %ebx
  800a92:	5e                   	pop    %esi
  800a93:	5f                   	pop    %edi
  800a94:	5d                   	pop    %ebp
  800a95:	c3                   	ret    

00800a96 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a96:	55                   	push   %ebp
  800a97:	89 e5                	mov    %esp,%ebp
  800a99:	57                   	push   %edi
  800a9a:	56                   	push   %esi
  800a9b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a9c:	ba 00 00 00 00       	mov    $0x0,%edx
  800aa1:	b8 02 00 00 00       	mov    $0x2,%eax
  800aa6:	89 d1                	mov    %edx,%ecx
  800aa8:	89 d3                	mov    %edx,%ebx
  800aaa:	89 d7                	mov    %edx,%edi
  800aac:	89 d6                	mov    %edx,%esi
  800aae:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800ab0:	5b                   	pop    %ebx
  800ab1:	5e                   	pop    %esi
  800ab2:	5f                   	pop    %edi
  800ab3:	5d                   	pop    %ebp
  800ab4:	c3                   	ret    

00800ab5 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800ab5:	55                   	push   %ebp
  800ab6:	89 e5                	mov    %esp,%ebp
  800ab8:	56                   	push   %esi
  800ab9:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800aba:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800abd:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800ac3:	e8 ce ff ff ff       	call   800a96 <sys_getenvid>
  800ac8:	83 ec 0c             	sub    $0xc,%esp
  800acb:	ff 75 0c             	pushl  0xc(%ebp)
  800ace:	ff 75 08             	pushl  0x8(%ebp)
  800ad1:	56                   	push   %esi
  800ad2:	50                   	push   %eax
  800ad3:	68 14 10 80 00       	push   $0x801014
  800ad8:	e8 af f6 ff ff       	call   80018c <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800add:	83 c4 18             	add    $0x18,%esp
  800ae0:	53                   	push   %ebx
  800ae1:	ff 75 10             	pushl  0x10(%ebp)
  800ae4:	e8 52 f6 ff ff       	call   80013b <vcprintf>
	cprintf("\n");
  800ae9:	c7 04 24 9c 0d 80 00 	movl   $0x800d9c,(%esp)
  800af0:	e8 97 f6 ff ff       	call   80018c <cprintf>
  800af5:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800af8:	cc                   	int3   
  800af9:	eb fd                	jmp    800af8 <_panic+0x43>
  800afb:	90                   	nop

00800afc <__udivdi3>:
  800afc:	55                   	push   %ebp
  800afd:	57                   	push   %edi
  800afe:	56                   	push   %esi
  800aff:	53                   	push   %ebx
  800b00:	83 ec 1c             	sub    $0x1c,%esp
  800b03:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800b07:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800b0b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800b0f:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800b13:	89 ca                	mov    %ecx,%edx
  800b15:	89 f8                	mov    %edi,%eax
  800b17:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800b1b:	85 f6                	test   %esi,%esi
  800b1d:	75 2d                	jne    800b4c <__udivdi3+0x50>
  800b1f:	39 cf                	cmp    %ecx,%edi
  800b21:	77 65                	ja     800b88 <__udivdi3+0x8c>
  800b23:	89 fd                	mov    %edi,%ebp
  800b25:	85 ff                	test   %edi,%edi
  800b27:	75 0b                	jne    800b34 <__udivdi3+0x38>
  800b29:	b8 01 00 00 00       	mov    $0x1,%eax
  800b2e:	31 d2                	xor    %edx,%edx
  800b30:	f7 f7                	div    %edi
  800b32:	89 c5                	mov    %eax,%ebp
  800b34:	31 d2                	xor    %edx,%edx
  800b36:	89 c8                	mov    %ecx,%eax
  800b38:	f7 f5                	div    %ebp
  800b3a:	89 c1                	mov    %eax,%ecx
  800b3c:	89 d8                	mov    %ebx,%eax
  800b3e:	f7 f5                	div    %ebp
  800b40:	89 cf                	mov    %ecx,%edi
  800b42:	89 fa                	mov    %edi,%edx
  800b44:	83 c4 1c             	add    $0x1c,%esp
  800b47:	5b                   	pop    %ebx
  800b48:	5e                   	pop    %esi
  800b49:	5f                   	pop    %edi
  800b4a:	5d                   	pop    %ebp
  800b4b:	c3                   	ret    
  800b4c:	39 ce                	cmp    %ecx,%esi
  800b4e:	77 28                	ja     800b78 <__udivdi3+0x7c>
  800b50:	0f bd fe             	bsr    %esi,%edi
  800b53:	83 f7 1f             	xor    $0x1f,%edi
  800b56:	75 40                	jne    800b98 <__udivdi3+0x9c>
  800b58:	39 ce                	cmp    %ecx,%esi
  800b5a:	72 0a                	jb     800b66 <__udivdi3+0x6a>
  800b5c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b60:	0f 87 9e 00 00 00    	ja     800c04 <__udivdi3+0x108>
  800b66:	b8 01 00 00 00       	mov    $0x1,%eax
  800b6b:	89 fa                	mov    %edi,%edx
  800b6d:	83 c4 1c             	add    $0x1c,%esp
  800b70:	5b                   	pop    %ebx
  800b71:	5e                   	pop    %esi
  800b72:	5f                   	pop    %edi
  800b73:	5d                   	pop    %ebp
  800b74:	c3                   	ret    
  800b75:	8d 76 00             	lea    0x0(%esi),%esi
  800b78:	31 ff                	xor    %edi,%edi
  800b7a:	31 c0                	xor    %eax,%eax
  800b7c:	89 fa                	mov    %edi,%edx
  800b7e:	83 c4 1c             	add    $0x1c,%esp
  800b81:	5b                   	pop    %ebx
  800b82:	5e                   	pop    %esi
  800b83:	5f                   	pop    %edi
  800b84:	5d                   	pop    %ebp
  800b85:	c3                   	ret    
  800b86:	66 90                	xchg   %ax,%ax
  800b88:	89 d8                	mov    %ebx,%eax
  800b8a:	f7 f7                	div    %edi
  800b8c:	31 ff                	xor    %edi,%edi
  800b8e:	89 fa                	mov    %edi,%edx
  800b90:	83 c4 1c             	add    $0x1c,%esp
  800b93:	5b                   	pop    %ebx
  800b94:	5e                   	pop    %esi
  800b95:	5f                   	pop    %edi
  800b96:	5d                   	pop    %ebp
  800b97:	c3                   	ret    
  800b98:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b9d:	89 eb                	mov    %ebp,%ebx
  800b9f:	29 fb                	sub    %edi,%ebx
  800ba1:	89 f9                	mov    %edi,%ecx
  800ba3:	d3 e6                	shl    %cl,%esi
  800ba5:	89 c5                	mov    %eax,%ebp
  800ba7:	88 d9                	mov    %bl,%cl
  800ba9:	d3 ed                	shr    %cl,%ebp
  800bab:	89 e9                	mov    %ebp,%ecx
  800bad:	09 f1                	or     %esi,%ecx
  800baf:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800bb3:	89 f9                	mov    %edi,%ecx
  800bb5:	d3 e0                	shl    %cl,%eax
  800bb7:	89 c5                	mov    %eax,%ebp
  800bb9:	89 d6                	mov    %edx,%esi
  800bbb:	88 d9                	mov    %bl,%cl
  800bbd:	d3 ee                	shr    %cl,%esi
  800bbf:	89 f9                	mov    %edi,%ecx
  800bc1:	d3 e2                	shl    %cl,%edx
  800bc3:	8b 44 24 08          	mov    0x8(%esp),%eax
  800bc7:	88 d9                	mov    %bl,%cl
  800bc9:	d3 e8                	shr    %cl,%eax
  800bcb:	09 c2                	or     %eax,%edx
  800bcd:	89 d0                	mov    %edx,%eax
  800bcf:	89 f2                	mov    %esi,%edx
  800bd1:	f7 74 24 0c          	divl   0xc(%esp)
  800bd5:	89 d6                	mov    %edx,%esi
  800bd7:	89 c3                	mov    %eax,%ebx
  800bd9:	f7 e5                	mul    %ebp
  800bdb:	39 d6                	cmp    %edx,%esi
  800bdd:	72 19                	jb     800bf8 <__udivdi3+0xfc>
  800bdf:	74 0b                	je     800bec <__udivdi3+0xf0>
  800be1:	89 d8                	mov    %ebx,%eax
  800be3:	31 ff                	xor    %edi,%edi
  800be5:	e9 58 ff ff ff       	jmp    800b42 <__udivdi3+0x46>
  800bea:	66 90                	xchg   %ax,%ax
  800bec:	8b 54 24 08          	mov    0x8(%esp),%edx
  800bf0:	89 f9                	mov    %edi,%ecx
  800bf2:	d3 e2                	shl    %cl,%edx
  800bf4:	39 c2                	cmp    %eax,%edx
  800bf6:	73 e9                	jae    800be1 <__udivdi3+0xe5>
  800bf8:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800bfb:	31 ff                	xor    %edi,%edi
  800bfd:	e9 40 ff ff ff       	jmp    800b42 <__udivdi3+0x46>
  800c02:	66 90                	xchg   %ax,%ax
  800c04:	31 c0                	xor    %eax,%eax
  800c06:	e9 37 ff ff ff       	jmp    800b42 <__udivdi3+0x46>
  800c0b:	90                   	nop

00800c0c <__umoddi3>:
  800c0c:	55                   	push   %ebp
  800c0d:	57                   	push   %edi
  800c0e:	56                   	push   %esi
  800c0f:	53                   	push   %ebx
  800c10:	83 ec 1c             	sub    $0x1c,%esp
  800c13:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800c17:	8b 74 24 34          	mov    0x34(%esp),%esi
  800c1b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800c1f:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800c23:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800c27:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800c2b:	89 f3                	mov    %esi,%ebx
  800c2d:	89 fa                	mov    %edi,%edx
  800c2f:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800c33:	89 34 24             	mov    %esi,(%esp)
  800c36:	85 c0                	test   %eax,%eax
  800c38:	75 1a                	jne    800c54 <__umoddi3+0x48>
  800c3a:	39 f7                	cmp    %esi,%edi
  800c3c:	0f 86 a2 00 00 00    	jbe    800ce4 <__umoddi3+0xd8>
  800c42:	89 c8                	mov    %ecx,%eax
  800c44:	89 f2                	mov    %esi,%edx
  800c46:	f7 f7                	div    %edi
  800c48:	89 d0                	mov    %edx,%eax
  800c4a:	31 d2                	xor    %edx,%edx
  800c4c:	83 c4 1c             	add    $0x1c,%esp
  800c4f:	5b                   	pop    %ebx
  800c50:	5e                   	pop    %esi
  800c51:	5f                   	pop    %edi
  800c52:	5d                   	pop    %ebp
  800c53:	c3                   	ret    
  800c54:	39 f0                	cmp    %esi,%eax
  800c56:	0f 87 ac 00 00 00    	ja     800d08 <__umoddi3+0xfc>
  800c5c:	0f bd e8             	bsr    %eax,%ebp
  800c5f:	83 f5 1f             	xor    $0x1f,%ebp
  800c62:	0f 84 ac 00 00 00    	je     800d14 <__umoddi3+0x108>
  800c68:	bf 20 00 00 00       	mov    $0x20,%edi
  800c6d:	29 ef                	sub    %ebp,%edi
  800c6f:	89 fe                	mov    %edi,%esi
  800c71:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c75:	89 e9                	mov    %ebp,%ecx
  800c77:	d3 e0                	shl    %cl,%eax
  800c79:	89 d7                	mov    %edx,%edi
  800c7b:	89 f1                	mov    %esi,%ecx
  800c7d:	d3 ef                	shr    %cl,%edi
  800c7f:	09 c7                	or     %eax,%edi
  800c81:	89 e9                	mov    %ebp,%ecx
  800c83:	d3 e2                	shl    %cl,%edx
  800c85:	89 14 24             	mov    %edx,(%esp)
  800c88:	89 d8                	mov    %ebx,%eax
  800c8a:	d3 e0                	shl    %cl,%eax
  800c8c:	89 c2                	mov    %eax,%edx
  800c8e:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c92:	d3 e0                	shl    %cl,%eax
  800c94:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c98:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c9c:	89 f1                	mov    %esi,%ecx
  800c9e:	d3 e8                	shr    %cl,%eax
  800ca0:	09 d0                	or     %edx,%eax
  800ca2:	d3 eb                	shr    %cl,%ebx
  800ca4:	89 da                	mov    %ebx,%edx
  800ca6:	f7 f7                	div    %edi
  800ca8:	89 d3                	mov    %edx,%ebx
  800caa:	f7 24 24             	mull   (%esp)
  800cad:	89 c6                	mov    %eax,%esi
  800caf:	89 d1                	mov    %edx,%ecx
  800cb1:	39 d3                	cmp    %edx,%ebx
  800cb3:	0f 82 87 00 00 00    	jb     800d40 <__umoddi3+0x134>
  800cb9:	0f 84 91 00 00 00    	je     800d50 <__umoddi3+0x144>
  800cbf:	8b 54 24 04          	mov    0x4(%esp),%edx
  800cc3:	29 f2                	sub    %esi,%edx
  800cc5:	19 cb                	sbb    %ecx,%ebx
  800cc7:	89 d8                	mov    %ebx,%eax
  800cc9:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800ccd:	d3 e0                	shl    %cl,%eax
  800ccf:	89 e9                	mov    %ebp,%ecx
  800cd1:	d3 ea                	shr    %cl,%edx
  800cd3:	09 d0                	or     %edx,%eax
  800cd5:	89 e9                	mov    %ebp,%ecx
  800cd7:	d3 eb                	shr    %cl,%ebx
  800cd9:	89 da                	mov    %ebx,%edx
  800cdb:	83 c4 1c             	add    $0x1c,%esp
  800cde:	5b                   	pop    %ebx
  800cdf:	5e                   	pop    %esi
  800ce0:	5f                   	pop    %edi
  800ce1:	5d                   	pop    %ebp
  800ce2:	c3                   	ret    
  800ce3:	90                   	nop
  800ce4:	89 fd                	mov    %edi,%ebp
  800ce6:	85 ff                	test   %edi,%edi
  800ce8:	75 0b                	jne    800cf5 <__umoddi3+0xe9>
  800cea:	b8 01 00 00 00       	mov    $0x1,%eax
  800cef:	31 d2                	xor    %edx,%edx
  800cf1:	f7 f7                	div    %edi
  800cf3:	89 c5                	mov    %eax,%ebp
  800cf5:	89 f0                	mov    %esi,%eax
  800cf7:	31 d2                	xor    %edx,%edx
  800cf9:	f7 f5                	div    %ebp
  800cfb:	89 c8                	mov    %ecx,%eax
  800cfd:	f7 f5                	div    %ebp
  800cff:	89 d0                	mov    %edx,%eax
  800d01:	e9 44 ff ff ff       	jmp    800c4a <__umoddi3+0x3e>
  800d06:	66 90                	xchg   %ax,%ax
  800d08:	89 c8                	mov    %ecx,%eax
  800d0a:	89 f2                	mov    %esi,%edx
  800d0c:	83 c4 1c             	add    $0x1c,%esp
  800d0f:	5b                   	pop    %ebx
  800d10:	5e                   	pop    %esi
  800d11:	5f                   	pop    %edi
  800d12:	5d                   	pop    %ebp
  800d13:	c3                   	ret    
  800d14:	3b 04 24             	cmp    (%esp),%eax
  800d17:	72 06                	jb     800d1f <__umoddi3+0x113>
  800d19:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800d1d:	77 0f                	ja     800d2e <__umoddi3+0x122>
  800d1f:	89 f2                	mov    %esi,%edx
  800d21:	29 f9                	sub    %edi,%ecx
  800d23:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800d27:	89 14 24             	mov    %edx,(%esp)
  800d2a:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800d2e:	8b 44 24 04          	mov    0x4(%esp),%eax
  800d32:	8b 14 24             	mov    (%esp),%edx
  800d35:	83 c4 1c             	add    $0x1c,%esp
  800d38:	5b                   	pop    %ebx
  800d39:	5e                   	pop    %esi
  800d3a:	5f                   	pop    %edi
  800d3b:	5d                   	pop    %ebp
  800d3c:	c3                   	ret    
  800d3d:	8d 76 00             	lea    0x0(%esi),%esi
  800d40:	2b 04 24             	sub    (%esp),%eax
  800d43:	19 fa                	sbb    %edi,%edx
  800d45:	89 d1                	mov    %edx,%ecx
  800d47:	89 c6                	mov    %eax,%esi
  800d49:	e9 71 ff ff ff       	jmp    800cbf <__umoddi3+0xb3>
  800d4e:	66 90                	xchg   %ax,%ax
  800d50:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d54:	72 ea                	jb     800d40 <__umoddi3+0x134>
  800d56:	89 d9                	mov    %ebx,%ecx
  800d58:	e9 62 ff ff ff       	jmp    800cbf <__umoddi3+0xb3>
