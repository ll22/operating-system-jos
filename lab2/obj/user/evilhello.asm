
obj/user/evilhello:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 19 00 00 00       	call   80004a <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	// try to print the kernel entry point as a string!  mua ha ha!
	sys_cputs((char*)0xf010000c, 100);
  800039:	6a 64                	push   $0x64
  80003b:	68 0c 00 10 f0       	push   $0xf010000c
  800040:	e8 bf 09 00 00       	call   800a04 <sys_cputs>
}
  800045:	83 c4 10             	add    $0x10,%esp
  800048:	c9                   	leave  
  800049:	c3                   	ret    

0080004a <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80004a:	55                   	push   %ebp
  80004b:	89 e5                	mov    %esp,%ebp
  80004d:	57                   	push   %edi
  80004e:	56                   	push   %esi
  80004f:	53                   	push   %ebx
  800050:	83 ec 0c             	sub    $0xc,%esp
  800053:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800056:	8b 75 0c             	mov    0xc(%ebp),%esi
	// set thisenv to point at our Env structure in envs[].
	// LAB 3: Your code here.
	//thisenv = 0;
	int32_t env_Index1 = (int32_t)sys_getenvid();
  800059:	e8 24 0a 00 00       	call   800a82 <sys_getenvid>
  80005e:	89 c7                	mov    %eax,%edi
	cprintf("printing env_Index1: %d\n", env_Index1);
  800060:	83 ec 08             	sub    $0x8,%esp
  800063:	50                   	push   %eax
  800064:	68 4c 0d 80 00       	push   $0x800d4c
  800069:	e8 0a 01 00 00       	call   800178 <cprintf>
	int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	cprintf("printing env_Index2: %d\n", env_Index2);
  80006e:	83 c4 08             	add    $0x8,%esp
  800071:	81 e7 ff 03 00 00    	and    $0x3ff,%edi
  800077:	57                   	push   %edi
  800078:	68 65 0d 80 00       	push   $0x800d65
  80007d:	e8 f6 00 00 00       	call   800178 <cprintf>

	thisenv = &envs[ENVX(sys_getenvid())];
  800082:	e8 fb 09 00 00       	call   800a82 <sys_getenvid>
  800087:	25 ff 03 00 00       	and    $0x3ff,%eax
  80008c:	8d 14 00             	lea    (%eax,%eax,1),%edx
  80008f:	01 d0                	add    %edx,%eax
  800091:	c1 e0 05             	shl    $0x5,%eax
  800094:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800099:	a3 04 10 80 00       	mov    %eax,0x801004
	cprintf("Addrs of thisenv (envs[0]) is: %p\n", thisenv);
  80009e:	83 c4 08             	add    $0x8,%esp
  8000a1:	50                   	push   %eax
  8000a2:	68 88 0d 80 00       	push   $0x800d88
  8000a7:	e8 cc 00 00 00       	call   800178 <cprintf>
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000ac:	83 c4 10             	add    $0x10,%esp
  8000af:	85 db                	test   %ebx,%ebx
  8000b1:	7e 07                	jle    8000ba <libmain+0x70>
		binaryname = argv[0];
  8000b3:	8b 06                	mov    (%esi),%eax
  8000b5:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  8000ba:	83 ec 08             	sub    $0x8,%esp
  8000bd:	56                   	push   %esi
  8000be:	53                   	push   %ebx
  8000bf:	e8 6f ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000c4:	e8 0b 00 00 00       	call   8000d4 <exit>
}
  8000c9:	83 c4 10             	add    $0x10,%esp
  8000cc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8000cf:	5b                   	pop    %ebx
  8000d0:	5e                   	pop    %esi
  8000d1:	5f                   	pop    %edi
  8000d2:	5d                   	pop    %ebp
  8000d3:	c3                   	ret    

008000d4 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000d4:	55                   	push   %ebp
  8000d5:	89 e5                	mov    %esp,%ebp
  8000d7:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000da:	6a 00                	push   $0x0
  8000dc:	e8 60 09 00 00       	call   800a41 <sys_env_destroy>
}
  8000e1:	83 c4 10             	add    $0x10,%esp
  8000e4:	c9                   	leave  
  8000e5:	c3                   	ret    

008000e6 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000e6:	55                   	push   %ebp
  8000e7:	89 e5                	mov    %esp,%ebp
  8000e9:	53                   	push   %ebx
  8000ea:	83 ec 04             	sub    $0x4,%esp
  8000ed:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000f0:	8b 13                	mov    (%ebx),%edx
  8000f2:	8d 42 01             	lea    0x1(%edx),%eax
  8000f5:	89 03                	mov    %eax,(%ebx)
  8000f7:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000fa:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8000fe:	3d ff 00 00 00       	cmp    $0xff,%eax
  800103:	75 1a                	jne    80011f <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800105:	83 ec 08             	sub    $0x8,%esp
  800108:	68 ff 00 00 00       	push   $0xff
  80010d:	8d 43 08             	lea    0x8(%ebx),%eax
  800110:	50                   	push   %eax
  800111:	e8 ee 08 00 00       	call   800a04 <sys_cputs>
		b->idx = 0;
  800116:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  80011c:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  80011f:	ff 43 04             	incl   0x4(%ebx)
}
  800122:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800125:	c9                   	leave  
  800126:	c3                   	ret    

00800127 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800127:	55                   	push   %ebp
  800128:	89 e5                	mov    %esp,%ebp
  80012a:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800130:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800137:	00 00 00 
	b.cnt = 0;
  80013a:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800141:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800144:	ff 75 0c             	pushl  0xc(%ebp)
  800147:	ff 75 08             	pushl  0x8(%ebp)
  80014a:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800150:	50                   	push   %eax
  800151:	68 e6 00 80 00       	push   $0x8000e6
  800156:	e8 51 01 00 00       	call   8002ac <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80015b:	83 c4 08             	add    $0x8,%esp
  80015e:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800164:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80016a:	50                   	push   %eax
  80016b:	e8 94 08 00 00       	call   800a04 <sys_cputs>

	return b.cnt;
}
  800170:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800176:	c9                   	leave  
  800177:	c3                   	ret    

00800178 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800178:	55                   	push   %ebp
  800179:	89 e5                	mov    %esp,%ebp
  80017b:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80017e:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800181:	50                   	push   %eax
  800182:	ff 75 08             	pushl  0x8(%ebp)
  800185:	e8 9d ff ff ff       	call   800127 <vcprintf>
	va_end(ap);

	return cnt;
}
  80018a:	c9                   	leave  
  80018b:	c3                   	ret    

0080018c <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80018c:	55                   	push   %ebp
  80018d:	89 e5                	mov    %esp,%ebp
  80018f:	57                   	push   %edi
  800190:	56                   	push   %esi
  800191:	53                   	push   %ebx
  800192:	83 ec 1c             	sub    $0x1c,%esp
  800195:	89 c7                	mov    %eax,%edi
  800197:	89 d6                	mov    %edx,%esi
  800199:	8b 45 08             	mov    0x8(%ebp),%eax
  80019c:	8b 55 0c             	mov    0xc(%ebp),%edx
  80019f:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8001a2:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001a5:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8001a8:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001ad:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001b0:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001b3:	39 d3                	cmp    %edx,%ebx
  8001b5:	72 05                	jb     8001bc <printnum+0x30>
  8001b7:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001ba:	77 45                	ja     800201 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001bc:	83 ec 0c             	sub    $0xc,%esp
  8001bf:	ff 75 18             	pushl  0x18(%ebp)
  8001c2:	8b 45 14             	mov    0x14(%ebp),%eax
  8001c5:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001c8:	53                   	push   %ebx
  8001c9:	ff 75 10             	pushl  0x10(%ebp)
  8001cc:	83 ec 08             	sub    $0x8,%esp
  8001cf:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001d2:	ff 75 e0             	pushl  -0x20(%ebp)
  8001d5:	ff 75 dc             	pushl  -0x24(%ebp)
  8001d8:	ff 75 d8             	pushl  -0x28(%ebp)
  8001db:	e8 08 09 00 00       	call   800ae8 <__udivdi3>
  8001e0:	83 c4 18             	add    $0x18,%esp
  8001e3:	52                   	push   %edx
  8001e4:	50                   	push   %eax
  8001e5:	89 f2                	mov    %esi,%edx
  8001e7:	89 f8                	mov    %edi,%eax
  8001e9:	e8 9e ff ff ff       	call   80018c <printnum>
  8001ee:	83 c4 20             	add    $0x20,%esp
  8001f1:	eb 16                	jmp    800209 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001f3:	83 ec 08             	sub    $0x8,%esp
  8001f6:	56                   	push   %esi
  8001f7:	ff 75 18             	pushl  0x18(%ebp)
  8001fa:	ff d7                	call   *%edi
  8001fc:	83 c4 10             	add    $0x10,%esp
  8001ff:	eb 03                	jmp    800204 <printnum+0x78>
  800201:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800204:	4b                   	dec    %ebx
  800205:	85 db                	test   %ebx,%ebx
  800207:	7f ea                	jg     8001f3 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800209:	83 ec 08             	sub    $0x8,%esp
  80020c:	56                   	push   %esi
  80020d:	83 ec 04             	sub    $0x4,%esp
  800210:	ff 75 e4             	pushl  -0x1c(%ebp)
  800213:	ff 75 e0             	pushl  -0x20(%ebp)
  800216:	ff 75 dc             	pushl  -0x24(%ebp)
  800219:	ff 75 d8             	pushl  -0x28(%ebp)
  80021c:	e8 d7 09 00 00       	call   800bf8 <__umoddi3>
  800221:	83 c4 14             	add    $0x14,%esp
  800224:	0f be 80 ab 0d 80 00 	movsbl 0x800dab(%eax),%eax
  80022b:	50                   	push   %eax
  80022c:	ff d7                	call   *%edi
}
  80022e:	83 c4 10             	add    $0x10,%esp
  800231:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800234:	5b                   	pop    %ebx
  800235:	5e                   	pop    %esi
  800236:	5f                   	pop    %edi
  800237:	5d                   	pop    %ebp
  800238:	c3                   	ret    

00800239 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800239:	55                   	push   %ebp
  80023a:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80023c:	83 fa 01             	cmp    $0x1,%edx
  80023f:	7e 0e                	jle    80024f <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800241:	8b 10                	mov    (%eax),%edx
  800243:	8d 4a 08             	lea    0x8(%edx),%ecx
  800246:	89 08                	mov    %ecx,(%eax)
  800248:	8b 02                	mov    (%edx),%eax
  80024a:	8b 52 04             	mov    0x4(%edx),%edx
  80024d:	eb 22                	jmp    800271 <getuint+0x38>
	else if (lflag)
  80024f:	85 d2                	test   %edx,%edx
  800251:	74 10                	je     800263 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800253:	8b 10                	mov    (%eax),%edx
  800255:	8d 4a 04             	lea    0x4(%edx),%ecx
  800258:	89 08                	mov    %ecx,(%eax)
  80025a:	8b 02                	mov    (%edx),%eax
  80025c:	ba 00 00 00 00       	mov    $0x0,%edx
  800261:	eb 0e                	jmp    800271 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800263:	8b 10                	mov    (%eax),%edx
  800265:	8d 4a 04             	lea    0x4(%edx),%ecx
  800268:	89 08                	mov    %ecx,(%eax)
  80026a:	8b 02                	mov    (%edx),%eax
  80026c:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800271:	5d                   	pop    %ebp
  800272:	c3                   	ret    

00800273 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800273:	55                   	push   %ebp
  800274:	89 e5                	mov    %esp,%ebp
  800276:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800279:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80027c:	8b 10                	mov    (%eax),%edx
  80027e:	3b 50 04             	cmp    0x4(%eax),%edx
  800281:	73 0a                	jae    80028d <sprintputch+0x1a>
		*b->buf++ = ch;
  800283:	8d 4a 01             	lea    0x1(%edx),%ecx
  800286:	89 08                	mov    %ecx,(%eax)
  800288:	8b 45 08             	mov    0x8(%ebp),%eax
  80028b:	88 02                	mov    %al,(%edx)
}
  80028d:	5d                   	pop    %ebp
  80028e:	c3                   	ret    

0080028f <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80028f:	55                   	push   %ebp
  800290:	89 e5                	mov    %esp,%ebp
  800292:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800295:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800298:	50                   	push   %eax
  800299:	ff 75 10             	pushl  0x10(%ebp)
  80029c:	ff 75 0c             	pushl  0xc(%ebp)
  80029f:	ff 75 08             	pushl  0x8(%ebp)
  8002a2:	e8 05 00 00 00       	call   8002ac <vprintfmt>
	va_end(ap);
}
  8002a7:	83 c4 10             	add    $0x10,%esp
  8002aa:	c9                   	leave  
  8002ab:	c3                   	ret    

008002ac <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8002ac:	55                   	push   %ebp
  8002ad:	89 e5                	mov    %esp,%ebp
  8002af:	57                   	push   %edi
  8002b0:	56                   	push   %esi
  8002b1:	53                   	push   %ebx
  8002b2:	83 ec 2c             	sub    $0x2c,%esp
  8002b5:	8b 75 08             	mov    0x8(%ebp),%esi
  8002b8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002bb:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002be:	eb 12                	jmp    8002d2 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002c0:	85 c0                	test   %eax,%eax
  8002c2:	0f 84 68 03 00 00    	je     800630 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002c8:	83 ec 08             	sub    $0x8,%esp
  8002cb:	53                   	push   %ebx
  8002cc:	50                   	push   %eax
  8002cd:	ff d6                	call   *%esi
  8002cf:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002d2:	47                   	inc    %edi
  8002d3:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002d7:	83 f8 25             	cmp    $0x25,%eax
  8002da:	75 e4                	jne    8002c0 <vprintfmt+0x14>
  8002dc:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002e0:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002e7:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002ee:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002f5:	ba 00 00 00 00       	mov    $0x0,%edx
  8002fa:	eb 07                	jmp    800303 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002fc:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8002ff:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800303:	8d 47 01             	lea    0x1(%edi),%eax
  800306:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800309:	0f b6 0f             	movzbl (%edi),%ecx
  80030c:	8a 07                	mov    (%edi),%al
  80030e:	83 e8 23             	sub    $0x23,%eax
  800311:	3c 55                	cmp    $0x55,%al
  800313:	0f 87 fe 02 00 00    	ja     800617 <vprintfmt+0x36b>
  800319:	0f b6 c0             	movzbl %al,%eax
  80031c:	ff 24 85 38 0e 80 00 	jmp    *0x800e38(,%eax,4)
  800323:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800326:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80032a:	eb d7                	jmp    800303 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80032c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80032f:	b8 00 00 00 00       	mov    $0x0,%eax
  800334:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800337:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80033a:	01 c0                	add    %eax,%eax
  80033c:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800340:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800343:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800346:	83 fa 09             	cmp    $0x9,%edx
  800349:	77 34                	ja     80037f <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80034b:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80034c:	eb e9                	jmp    800337 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80034e:	8b 45 14             	mov    0x14(%ebp),%eax
  800351:	8d 48 04             	lea    0x4(%eax),%ecx
  800354:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800357:	8b 00                	mov    (%eax),%eax
  800359:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80035c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80035f:	eb 24                	jmp    800385 <vprintfmt+0xd9>
  800361:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800365:	79 07                	jns    80036e <vprintfmt+0xc2>
  800367:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80036e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800371:	eb 90                	jmp    800303 <vprintfmt+0x57>
  800373:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800376:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80037d:	eb 84                	jmp    800303 <vprintfmt+0x57>
  80037f:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800382:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800385:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800389:	0f 89 74 ff ff ff    	jns    800303 <vprintfmt+0x57>
				width = precision, precision = -1;
  80038f:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800392:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800395:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80039c:	e9 62 ff ff ff       	jmp    800303 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8003a1:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003a2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8003a5:	e9 59 ff ff ff       	jmp    800303 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8003aa:	8b 45 14             	mov    0x14(%ebp),%eax
  8003ad:	8d 50 04             	lea    0x4(%eax),%edx
  8003b0:	89 55 14             	mov    %edx,0x14(%ebp)
  8003b3:	83 ec 08             	sub    $0x8,%esp
  8003b6:	53                   	push   %ebx
  8003b7:	ff 30                	pushl  (%eax)
  8003b9:	ff d6                	call   *%esi
			break;
  8003bb:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003be:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003c1:	e9 0c ff ff ff       	jmp    8002d2 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003c6:	8b 45 14             	mov    0x14(%ebp),%eax
  8003c9:	8d 50 04             	lea    0x4(%eax),%edx
  8003cc:	89 55 14             	mov    %edx,0x14(%ebp)
  8003cf:	8b 00                	mov    (%eax),%eax
  8003d1:	85 c0                	test   %eax,%eax
  8003d3:	79 02                	jns    8003d7 <vprintfmt+0x12b>
  8003d5:	f7 d8                	neg    %eax
  8003d7:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003d9:	83 f8 06             	cmp    $0x6,%eax
  8003dc:	7f 0b                	jg     8003e9 <vprintfmt+0x13d>
  8003de:	8b 04 85 90 0f 80 00 	mov    0x800f90(,%eax,4),%eax
  8003e5:	85 c0                	test   %eax,%eax
  8003e7:	75 18                	jne    800401 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003e9:	52                   	push   %edx
  8003ea:	68 c3 0d 80 00       	push   $0x800dc3
  8003ef:	53                   	push   %ebx
  8003f0:	56                   	push   %esi
  8003f1:	e8 99 fe ff ff       	call   80028f <printfmt>
  8003f6:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f9:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8003fc:	e9 d1 fe ff ff       	jmp    8002d2 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800401:	50                   	push   %eax
  800402:	68 cc 0d 80 00       	push   $0x800dcc
  800407:	53                   	push   %ebx
  800408:	56                   	push   %esi
  800409:	e8 81 fe ff ff       	call   80028f <printfmt>
  80040e:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800411:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800414:	e9 b9 fe ff ff       	jmp    8002d2 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800419:	8b 45 14             	mov    0x14(%ebp),%eax
  80041c:	8d 50 04             	lea    0x4(%eax),%edx
  80041f:	89 55 14             	mov    %edx,0x14(%ebp)
  800422:	8b 38                	mov    (%eax),%edi
  800424:	85 ff                	test   %edi,%edi
  800426:	75 05                	jne    80042d <vprintfmt+0x181>
				p = "(null)";
  800428:	bf bc 0d 80 00       	mov    $0x800dbc,%edi
			if (width > 0 && padc != '-')
  80042d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800431:	0f 8e 90 00 00 00    	jle    8004c7 <vprintfmt+0x21b>
  800437:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80043b:	0f 84 8e 00 00 00    	je     8004cf <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800441:	83 ec 08             	sub    $0x8,%esp
  800444:	ff 75 d0             	pushl  -0x30(%ebp)
  800447:	57                   	push   %edi
  800448:	e8 70 02 00 00       	call   8006bd <strnlen>
  80044d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800450:	29 c1                	sub    %eax,%ecx
  800452:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800455:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800458:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80045c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80045f:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800462:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800464:	eb 0d                	jmp    800473 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800466:	83 ec 08             	sub    $0x8,%esp
  800469:	53                   	push   %ebx
  80046a:	ff 75 e4             	pushl  -0x1c(%ebp)
  80046d:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80046f:	4f                   	dec    %edi
  800470:	83 c4 10             	add    $0x10,%esp
  800473:	85 ff                	test   %edi,%edi
  800475:	7f ef                	jg     800466 <vprintfmt+0x1ba>
  800477:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80047a:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80047d:	89 c8                	mov    %ecx,%eax
  80047f:	85 c9                	test   %ecx,%ecx
  800481:	79 05                	jns    800488 <vprintfmt+0x1dc>
  800483:	b8 00 00 00 00       	mov    $0x0,%eax
  800488:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80048b:	29 c1                	sub    %eax,%ecx
  80048d:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800490:	89 75 08             	mov    %esi,0x8(%ebp)
  800493:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800496:	eb 3d                	jmp    8004d5 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800498:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80049c:	74 19                	je     8004b7 <vprintfmt+0x20b>
  80049e:	0f be c0             	movsbl %al,%eax
  8004a1:	83 e8 20             	sub    $0x20,%eax
  8004a4:	83 f8 5e             	cmp    $0x5e,%eax
  8004a7:	76 0e                	jbe    8004b7 <vprintfmt+0x20b>
					putch('?', putdat);
  8004a9:	83 ec 08             	sub    $0x8,%esp
  8004ac:	53                   	push   %ebx
  8004ad:	6a 3f                	push   $0x3f
  8004af:	ff 55 08             	call   *0x8(%ebp)
  8004b2:	83 c4 10             	add    $0x10,%esp
  8004b5:	eb 0b                	jmp    8004c2 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004b7:	83 ec 08             	sub    $0x8,%esp
  8004ba:	53                   	push   %ebx
  8004bb:	52                   	push   %edx
  8004bc:	ff 55 08             	call   *0x8(%ebp)
  8004bf:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004c2:	ff 4d e4             	decl   -0x1c(%ebp)
  8004c5:	eb 0e                	jmp    8004d5 <vprintfmt+0x229>
  8004c7:	89 75 08             	mov    %esi,0x8(%ebp)
  8004ca:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004cd:	eb 06                	jmp    8004d5 <vprintfmt+0x229>
  8004cf:	89 75 08             	mov    %esi,0x8(%ebp)
  8004d2:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004d5:	47                   	inc    %edi
  8004d6:	8a 47 ff             	mov    -0x1(%edi),%al
  8004d9:	0f be d0             	movsbl %al,%edx
  8004dc:	85 d2                	test   %edx,%edx
  8004de:	74 1d                	je     8004fd <vprintfmt+0x251>
  8004e0:	85 f6                	test   %esi,%esi
  8004e2:	78 b4                	js     800498 <vprintfmt+0x1ec>
  8004e4:	4e                   	dec    %esi
  8004e5:	79 b1                	jns    800498 <vprintfmt+0x1ec>
  8004e7:	8b 75 08             	mov    0x8(%ebp),%esi
  8004ea:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004ed:	eb 14                	jmp    800503 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004ef:	83 ec 08             	sub    $0x8,%esp
  8004f2:	53                   	push   %ebx
  8004f3:	6a 20                	push   $0x20
  8004f5:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004f7:	4f                   	dec    %edi
  8004f8:	83 c4 10             	add    $0x10,%esp
  8004fb:	eb 06                	jmp    800503 <vprintfmt+0x257>
  8004fd:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800500:	8b 75 08             	mov    0x8(%ebp),%esi
  800503:	85 ff                	test   %edi,%edi
  800505:	7f e8                	jg     8004ef <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800507:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80050a:	e9 c3 fd ff ff       	jmp    8002d2 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  80050f:	83 fa 01             	cmp    $0x1,%edx
  800512:	7e 16                	jle    80052a <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800514:	8b 45 14             	mov    0x14(%ebp),%eax
  800517:	8d 50 08             	lea    0x8(%eax),%edx
  80051a:	89 55 14             	mov    %edx,0x14(%ebp)
  80051d:	8b 50 04             	mov    0x4(%eax),%edx
  800520:	8b 00                	mov    (%eax),%eax
  800522:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800525:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800528:	eb 32                	jmp    80055c <vprintfmt+0x2b0>
	else if (lflag)
  80052a:	85 d2                	test   %edx,%edx
  80052c:	74 18                	je     800546 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  80052e:	8b 45 14             	mov    0x14(%ebp),%eax
  800531:	8d 50 04             	lea    0x4(%eax),%edx
  800534:	89 55 14             	mov    %edx,0x14(%ebp)
  800537:	8b 00                	mov    (%eax),%eax
  800539:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80053c:	89 c1                	mov    %eax,%ecx
  80053e:	c1 f9 1f             	sar    $0x1f,%ecx
  800541:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800544:	eb 16                	jmp    80055c <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800546:	8b 45 14             	mov    0x14(%ebp),%eax
  800549:	8d 50 04             	lea    0x4(%eax),%edx
  80054c:	89 55 14             	mov    %edx,0x14(%ebp)
  80054f:	8b 00                	mov    (%eax),%eax
  800551:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800554:	89 c1                	mov    %eax,%ecx
  800556:	c1 f9 1f             	sar    $0x1f,%ecx
  800559:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80055c:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80055f:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800562:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800566:	79 76                	jns    8005de <vprintfmt+0x332>
				putch('-', putdat);
  800568:	83 ec 08             	sub    $0x8,%esp
  80056b:	53                   	push   %ebx
  80056c:	6a 2d                	push   $0x2d
  80056e:	ff d6                	call   *%esi
				num = -(long long) num;
  800570:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800573:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800576:	f7 d8                	neg    %eax
  800578:	83 d2 00             	adc    $0x0,%edx
  80057b:	f7 da                	neg    %edx
  80057d:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800580:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800585:	eb 5c                	jmp    8005e3 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800587:	8d 45 14             	lea    0x14(%ebp),%eax
  80058a:	e8 aa fc ff ff       	call   800239 <getuint>
			base = 10;
  80058f:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800594:	eb 4d                	jmp    8005e3 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800596:	8d 45 14             	lea    0x14(%ebp),%eax
  800599:	e8 9b fc ff ff       	call   800239 <getuint>
			base = 8;
  80059e:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8005a3:	eb 3e                	jmp    8005e3 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8005a5:	83 ec 08             	sub    $0x8,%esp
  8005a8:	53                   	push   %ebx
  8005a9:	6a 30                	push   $0x30
  8005ab:	ff d6                	call   *%esi
			putch('x', putdat);
  8005ad:	83 c4 08             	add    $0x8,%esp
  8005b0:	53                   	push   %ebx
  8005b1:	6a 78                	push   $0x78
  8005b3:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005b5:	8b 45 14             	mov    0x14(%ebp),%eax
  8005b8:	8d 50 04             	lea    0x4(%eax),%edx
  8005bb:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005be:	8b 00                	mov    (%eax),%eax
  8005c0:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005c5:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005c8:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005cd:	eb 14                	jmp    8005e3 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005cf:	8d 45 14             	lea    0x14(%ebp),%eax
  8005d2:	e8 62 fc ff ff       	call   800239 <getuint>
			base = 16;
  8005d7:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005dc:	eb 05                	jmp    8005e3 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005de:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005e3:	83 ec 0c             	sub    $0xc,%esp
  8005e6:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005ea:	57                   	push   %edi
  8005eb:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005ee:	51                   	push   %ecx
  8005ef:	52                   	push   %edx
  8005f0:	50                   	push   %eax
  8005f1:	89 da                	mov    %ebx,%edx
  8005f3:	89 f0                	mov    %esi,%eax
  8005f5:	e8 92 fb ff ff       	call   80018c <printnum>
			break;
  8005fa:	83 c4 20             	add    $0x20,%esp
  8005fd:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800600:	e9 cd fc ff ff       	jmp    8002d2 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800605:	83 ec 08             	sub    $0x8,%esp
  800608:	53                   	push   %ebx
  800609:	51                   	push   %ecx
  80060a:	ff d6                	call   *%esi
			break;
  80060c:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80060f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800612:	e9 bb fc ff ff       	jmp    8002d2 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800617:	83 ec 08             	sub    $0x8,%esp
  80061a:	53                   	push   %ebx
  80061b:	6a 25                	push   $0x25
  80061d:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  80061f:	83 c4 10             	add    $0x10,%esp
  800622:	eb 01                	jmp    800625 <vprintfmt+0x379>
  800624:	4f                   	dec    %edi
  800625:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800629:	75 f9                	jne    800624 <vprintfmt+0x378>
  80062b:	e9 a2 fc ff ff       	jmp    8002d2 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800630:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800633:	5b                   	pop    %ebx
  800634:	5e                   	pop    %esi
  800635:	5f                   	pop    %edi
  800636:	5d                   	pop    %ebp
  800637:	c3                   	ret    

00800638 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800638:	55                   	push   %ebp
  800639:	89 e5                	mov    %esp,%ebp
  80063b:	83 ec 18             	sub    $0x18,%esp
  80063e:	8b 45 08             	mov    0x8(%ebp),%eax
  800641:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800644:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800647:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80064b:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  80064e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800655:	85 c0                	test   %eax,%eax
  800657:	74 26                	je     80067f <vsnprintf+0x47>
  800659:	85 d2                	test   %edx,%edx
  80065b:	7e 29                	jle    800686 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80065d:	ff 75 14             	pushl  0x14(%ebp)
  800660:	ff 75 10             	pushl  0x10(%ebp)
  800663:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800666:	50                   	push   %eax
  800667:	68 73 02 80 00       	push   $0x800273
  80066c:	e8 3b fc ff ff       	call   8002ac <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800671:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800674:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800677:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80067a:	83 c4 10             	add    $0x10,%esp
  80067d:	eb 0c                	jmp    80068b <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80067f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800684:	eb 05                	jmp    80068b <vsnprintf+0x53>
  800686:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80068b:	c9                   	leave  
  80068c:	c3                   	ret    

0080068d <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80068d:	55                   	push   %ebp
  80068e:	89 e5                	mov    %esp,%ebp
  800690:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800693:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800696:	50                   	push   %eax
  800697:	ff 75 10             	pushl  0x10(%ebp)
  80069a:	ff 75 0c             	pushl  0xc(%ebp)
  80069d:	ff 75 08             	pushl  0x8(%ebp)
  8006a0:	e8 93 ff ff ff       	call   800638 <vsnprintf>
	va_end(ap);

	return rc;
}
  8006a5:	c9                   	leave  
  8006a6:	c3                   	ret    

008006a7 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8006a7:	55                   	push   %ebp
  8006a8:	89 e5                	mov    %esp,%ebp
  8006aa:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8006ad:	b8 00 00 00 00       	mov    $0x0,%eax
  8006b2:	eb 01                	jmp    8006b5 <strlen+0xe>
		n++;
  8006b4:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006b5:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006b9:	75 f9                	jne    8006b4 <strlen+0xd>
		n++;
	return n;
}
  8006bb:	5d                   	pop    %ebp
  8006bc:	c3                   	ret    

008006bd <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006bd:	55                   	push   %ebp
  8006be:	89 e5                	mov    %esp,%ebp
  8006c0:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006c3:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006c6:	ba 00 00 00 00       	mov    $0x0,%edx
  8006cb:	eb 01                	jmp    8006ce <strnlen+0x11>
		n++;
  8006cd:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006ce:	39 c2                	cmp    %eax,%edx
  8006d0:	74 08                	je     8006da <strnlen+0x1d>
  8006d2:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006d6:	75 f5                	jne    8006cd <strnlen+0x10>
  8006d8:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006da:	5d                   	pop    %ebp
  8006db:	c3                   	ret    

008006dc <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006dc:	55                   	push   %ebp
  8006dd:	89 e5                	mov    %esp,%ebp
  8006df:	53                   	push   %ebx
  8006e0:	8b 45 08             	mov    0x8(%ebp),%eax
  8006e3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006e6:	89 c2                	mov    %eax,%edx
  8006e8:	42                   	inc    %edx
  8006e9:	41                   	inc    %ecx
  8006ea:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006ed:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006f0:	84 db                	test   %bl,%bl
  8006f2:	75 f4                	jne    8006e8 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006f4:	5b                   	pop    %ebx
  8006f5:	5d                   	pop    %ebp
  8006f6:	c3                   	ret    

008006f7 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006f7:	55                   	push   %ebp
  8006f8:	89 e5                	mov    %esp,%ebp
  8006fa:	53                   	push   %ebx
  8006fb:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8006fe:	53                   	push   %ebx
  8006ff:	e8 a3 ff ff ff       	call   8006a7 <strlen>
  800704:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800707:	ff 75 0c             	pushl  0xc(%ebp)
  80070a:	01 d8                	add    %ebx,%eax
  80070c:	50                   	push   %eax
  80070d:	e8 ca ff ff ff       	call   8006dc <strcpy>
	return dst;
}
  800712:	89 d8                	mov    %ebx,%eax
  800714:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800717:	c9                   	leave  
  800718:	c3                   	ret    

00800719 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800719:	55                   	push   %ebp
  80071a:	89 e5                	mov    %esp,%ebp
  80071c:	56                   	push   %esi
  80071d:	53                   	push   %ebx
  80071e:	8b 75 08             	mov    0x8(%ebp),%esi
  800721:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800724:	89 f3                	mov    %esi,%ebx
  800726:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800729:	89 f2                	mov    %esi,%edx
  80072b:	eb 0c                	jmp    800739 <strncpy+0x20>
		*dst++ = *src;
  80072d:	42                   	inc    %edx
  80072e:	8a 01                	mov    (%ecx),%al
  800730:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800733:	80 39 01             	cmpb   $0x1,(%ecx)
  800736:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800739:	39 da                	cmp    %ebx,%edx
  80073b:	75 f0                	jne    80072d <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  80073d:	89 f0                	mov    %esi,%eax
  80073f:	5b                   	pop    %ebx
  800740:	5e                   	pop    %esi
  800741:	5d                   	pop    %ebp
  800742:	c3                   	ret    

00800743 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800743:	55                   	push   %ebp
  800744:	89 e5                	mov    %esp,%ebp
  800746:	56                   	push   %esi
  800747:	53                   	push   %ebx
  800748:	8b 75 08             	mov    0x8(%ebp),%esi
  80074b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80074e:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800751:	85 c0                	test   %eax,%eax
  800753:	74 1e                	je     800773 <strlcpy+0x30>
  800755:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800759:	89 f2                	mov    %esi,%edx
  80075b:	eb 05                	jmp    800762 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  80075d:	42                   	inc    %edx
  80075e:	41                   	inc    %ecx
  80075f:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800762:	39 c2                	cmp    %eax,%edx
  800764:	74 08                	je     80076e <strlcpy+0x2b>
  800766:	8a 19                	mov    (%ecx),%bl
  800768:	84 db                	test   %bl,%bl
  80076a:	75 f1                	jne    80075d <strlcpy+0x1a>
  80076c:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  80076e:	c6 00 00             	movb   $0x0,(%eax)
  800771:	eb 02                	jmp    800775 <strlcpy+0x32>
  800773:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800775:	29 f0                	sub    %esi,%eax
}
  800777:	5b                   	pop    %ebx
  800778:	5e                   	pop    %esi
  800779:	5d                   	pop    %ebp
  80077a:	c3                   	ret    

0080077b <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80077b:	55                   	push   %ebp
  80077c:	89 e5                	mov    %esp,%ebp
  80077e:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800781:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800784:	eb 02                	jmp    800788 <strcmp+0xd>
		p++, q++;
  800786:	41                   	inc    %ecx
  800787:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800788:	8a 01                	mov    (%ecx),%al
  80078a:	84 c0                	test   %al,%al
  80078c:	74 04                	je     800792 <strcmp+0x17>
  80078e:	3a 02                	cmp    (%edx),%al
  800790:	74 f4                	je     800786 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800792:	0f b6 c0             	movzbl %al,%eax
  800795:	0f b6 12             	movzbl (%edx),%edx
  800798:	29 d0                	sub    %edx,%eax
}
  80079a:	5d                   	pop    %ebp
  80079b:	c3                   	ret    

0080079c <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  80079c:	55                   	push   %ebp
  80079d:	89 e5                	mov    %esp,%ebp
  80079f:	53                   	push   %ebx
  8007a0:	8b 45 08             	mov    0x8(%ebp),%eax
  8007a3:	8b 55 0c             	mov    0xc(%ebp),%edx
  8007a6:	89 c3                	mov    %eax,%ebx
  8007a8:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8007ab:	eb 02                	jmp    8007af <strncmp+0x13>
		n--, p++, q++;
  8007ad:	40                   	inc    %eax
  8007ae:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8007af:	39 d8                	cmp    %ebx,%eax
  8007b1:	74 14                	je     8007c7 <strncmp+0x2b>
  8007b3:	8a 08                	mov    (%eax),%cl
  8007b5:	84 c9                	test   %cl,%cl
  8007b7:	74 04                	je     8007bd <strncmp+0x21>
  8007b9:	3a 0a                	cmp    (%edx),%cl
  8007bb:	74 f0                	je     8007ad <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007bd:	0f b6 00             	movzbl (%eax),%eax
  8007c0:	0f b6 12             	movzbl (%edx),%edx
  8007c3:	29 d0                	sub    %edx,%eax
  8007c5:	eb 05                	jmp    8007cc <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007c7:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007cc:	5b                   	pop    %ebx
  8007cd:	5d                   	pop    %ebp
  8007ce:	c3                   	ret    

008007cf <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007cf:	55                   	push   %ebp
  8007d0:	89 e5                	mov    %esp,%ebp
  8007d2:	8b 45 08             	mov    0x8(%ebp),%eax
  8007d5:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007d8:	eb 05                	jmp    8007df <strchr+0x10>
		if (*s == c)
  8007da:	38 ca                	cmp    %cl,%dl
  8007dc:	74 0c                	je     8007ea <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007de:	40                   	inc    %eax
  8007df:	8a 10                	mov    (%eax),%dl
  8007e1:	84 d2                	test   %dl,%dl
  8007e3:	75 f5                	jne    8007da <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007e5:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007ea:	5d                   	pop    %ebp
  8007eb:	c3                   	ret    

008007ec <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007ec:	55                   	push   %ebp
  8007ed:	89 e5                	mov    %esp,%ebp
  8007ef:	8b 45 08             	mov    0x8(%ebp),%eax
  8007f2:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007f5:	eb 05                	jmp    8007fc <strfind+0x10>
		if (*s == c)
  8007f7:	38 ca                	cmp    %cl,%dl
  8007f9:	74 07                	je     800802 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007fb:	40                   	inc    %eax
  8007fc:	8a 10                	mov    (%eax),%dl
  8007fe:	84 d2                	test   %dl,%dl
  800800:	75 f5                	jne    8007f7 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800802:	5d                   	pop    %ebp
  800803:	c3                   	ret    

00800804 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800804:	55                   	push   %ebp
  800805:	89 e5                	mov    %esp,%ebp
  800807:	57                   	push   %edi
  800808:	56                   	push   %esi
  800809:	53                   	push   %ebx
  80080a:	8b 7d 08             	mov    0x8(%ebp),%edi
  80080d:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800810:	85 c9                	test   %ecx,%ecx
  800812:	74 36                	je     80084a <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800814:	f7 c7 03 00 00 00    	test   $0x3,%edi
  80081a:	75 28                	jne    800844 <memset+0x40>
  80081c:	f6 c1 03             	test   $0x3,%cl
  80081f:	75 23                	jne    800844 <memset+0x40>
		c &= 0xFF;
  800821:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800825:	89 d3                	mov    %edx,%ebx
  800827:	c1 e3 08             	shl    $0x8,%ebx
  80082a:	89 d6                	mov    %edx,%esi
  80082c:	c1 e6 18             	shl    $0x18,%esi
  80082f:	89 d0                	mov    %edx,%eax
  800831:	c1 e0 10             	shl    $0x10,%eax
  800834:	09 f0                	or     %esi,%eax
  800836:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800838:	89 d8                	mov    %ebx,%eax
  80083a:	09 d0                	or     %edx,%eax
  80083c:	c1 e9 02             	shr    $0x2,%ecx
  80083f:	fc                   	cld    
  800840:	f3 ab                	rep stos %eax,%es:(%edi)
  800842:	eb 06                	jmp    80084a <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800844:	8b 45 0c             	mov    0xc(%ebp),%eax
  800847:	fc                   	cld    
  800848:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  80084a:	89 f8                	mov    %edi,%eax
  80084c:	5b                   	pop    %ebx
  80084d:	5e                   	pop    %esi
  80084e:	5f                   	pop    %edi
  80084f:	5d                   	pop    %ebp
  800850:	c3                   	ret    

00800851 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800851:	55                   	push   %ebp
  800852:	89 e5                	mov    %esp,%ebp
  800854:	57                   	push   %edi
  800855:	56                   	push   %esi
  800856:	8b 45 08             	mov    0x8(%ebp),%eax
  800859:	8b 75 0c             	mov    0xc(%ebp),%esi
  80085c:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  80085f:	39 c6                	cmp    %eax,%esi
  800861:	73 33                	jae    800896 <memmove+0x45>
  800863:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800866:	39 d0                	cmp    %edx,%eax
  800868:	73 2c                	jae    800896 <memmove+0x45>
		s += n;
		d += n;
  80086a:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80086d:	89 d6                	mov    %edx,%esi
  80086f:	09 fe                	or     %edi,%esi
  800871:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800877:	75 13                	jne    80088c <memmove+0x3b>
  800879:	f6 c1 03             	test   $0x3,%cl
  80087c:	75 0e                	jne    80088c <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  80087e:	83 ef 04             	sub    $0x4,%edi
  800881:	8d 72 fc             	lea    -0x4(%edx),%esi
  800884:	c1 e9 02             	shr    $0x2,%ecx
  800887:	fd                   	std    
  800888:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80088a:	eb 07                	jmp    800893 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  80088c:	4f                   	dec    %edi
  80088d:	8d 72 ff             	lea    -0x1(%edx),%esi
  800890:	fd                   	std    
  800891:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800893:	fc                   	cld    
  800894:	eb 1d                	jmp    8008b3 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800896:	89 f2                	mov    %esi,%edx
  800898:	09 c2                	or     %eax,%edx
  80089a:	f6 c2 03             	test   $0x3,%dl
  80089d:	75 0f                	jne    8008ae <memmove+0x5d>
  80089f:	f6 c1 03             	test   $0x3,%cl
  8008a2:	75 0a                	jne    8008ae <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  8008a4:	c1 e9 02             	shr    $0x2,%ecx
  8008a7:	89 c7                	mov    %eax,%edi
  8008a9:	fc                   	cld    
  8008aa:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008ac:	eb 05                	jmp    8008b3 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8008ae:	89 c7                	mov    %eax,%edi
  8008b0:	fc                   	cld    
  8008b1:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008b3:	5e                   	pop    %esi
  8008b4:	5f                   	pop    %edi
  8008b5:	5d                   	pop    %ebp
  8008b6:	c3                   	ret    

008008b7 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008b7:	55                   	push   %ebp
  8008b8:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008ba:	ff 75 10             	pushl  0x10(%ebp)
  8008bd:	ff 75 0c             	pushl  0xc(%ebp)
  8008c0:	ff 75 08             	pushl  0x8(%ebp)
  8008c3:	e8 89 ff ff ff       	call   800851 <memmove>
}
  8008c8:	c9                   	leave  
  8008c9:	c3                   	ret    

008008ca <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008ca:	55                   	push   %ebp
  8008cb:	89 e5                	mov    %esp,%ebp
  8008cd:	56                   	push   %esi
  8008ce:	53                   	push   %ebx
  8008cf:	8b 45 08             	mov    0x8(%ebp),%eax
  8008d2:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008d5:	89 c6                	mov    %eax,%esi
  8008d7:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008da:	eb 14                	jmp    8008f0 <memcmp+0x26>
		if (*s1 != *s2)
  8008dc:	8a 08                	mov    (%eax),%cl
  8008de:	8a 1a                	mov    (%edx),%bl
  8008e0:	38 d9                	cmp    %bl,%cl
  8008e2:	74 0a                	je     8008ee <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008e4:	0f b6 c1             	movzbl %cl,%eax
  8008e7:	0f b6 db             	movzbl %bl,%ebx
  8008ea:	29 d8                	sub    %ebx,%eax
  8008ec:	eb 0b                	jmp    8008f9 <memcmp+0x2f>
		s1++, s2++;
  8008ee:	40                   	inc    %eax
  8008ef:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008f0:	39 f0                	cmp    %esi,%eax
  8008f2:	75 e8                	jne    8008dc <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008f4:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008f9:	5b                   	pop    %ebx
  8008fa:	5e                   	pop    %esi
  8008fb:	5d                   	pop    %ebp
  8008fc:	c3                   	ret    

008008fd <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8008fd:	55                   	push   %ebp
  8008fe:	89 e5                	mov    %esp,%ebp
  800900:	53                   	push   %ebx
  800901:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800904:	89 c1                	mov    %eax,%ecx
  800906:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800909:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  80090d:	eb 08                	jmp    800917 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  80090f:	0f b6 10             	movzbl (%eax),%edx
  800912:	39 da                	cmp    %ebx,%edx
  800914:	74 05                	je     80091b <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800916:	40                   	inc    %eax
  800917:	39 c8                	cmp    %ecx,%eax
  800919:	72 f4                	jb     80090f <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  80091b:	5b                   	pop    %ebx
  80091c:	5d                   	pop    %ebp
  80091d:	c3                   	ret    

0080091e <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  80091e:	55                   	push   %ebp
  80091f:	89 e5                	mov    %esp,%ebp
  800921:	57                   	push   %edi
  800922:	56                   	push   %esi
  800923:	53                   	push   %ebx
  800924:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800927:	eb 01                	jmp    80092a <strtol+0xc>
		s++;
  800929:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80092a:	8a 01                	mov    (%ecx),%al
  80092c:	3c 20                	cmp    $0x20,%al
  80092e:	74 f9                	je     800929 <strtol+0xb>
  800930:	3c 09                	cmp    $0x9,%al
  800932:	74 f5                	je     800929 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800934:	3c 2b                	cmp    $0x2b,%al
  800936:	75 08                	jne    800940 <strtol+0x22>
		s++;
  800938:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800939:	bf 00 00 00 00       	mov    $0x0,%edi
  80093e:	eb 11                	jmp    800951 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800940:	3c 2d                	cmp    $0x2d,%al
  800942:	75 08                	jne    80094c <strtol+0x2e>
		s++, neg = 1;
  800944:	41                   	inc    %ecx
  800945:	bf 01 00 00 00       	mov    $0x1,%edi
  80094a:	eb 05                	jmp    800951 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  80094c:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800951:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800955:	0f 84 87 00 00 00    	je     8009e2 <strtol+0xc4>
  80095b:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  80095f:	75 27                	jne    800988 <strtol+0x6a>
  800961:	80 39 30             	cmpb   $0x30,(%ecx)
  800964:	75 22                	jne    800988 <strtol+0x6a>
  800966:	e9 88 00 00 00       	jmp    8009f3 <strtol+0xd5>
		s += 2, base = 16;
  80096b:	83 c1 02             	add    $0x2,%ecx
  80096e:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800975:	eb 11                	jmp    800988 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800977:	41                   	inc    %ecx
  800978:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  80097f:	eb 07                	jmp    800988 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800981:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800988:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  80098d:	8a 11                	mov    (%ecx),%dl
  80098f:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800992:	80 fb 09             	cmp    $0x9,%bl
  800995:	77 08                	ja     80099f <strtol+0x81>
			dig = *s - '0';
  800997:	0f be d2             	movsbl %dl,%edx
  80099a:	83 ea 30             	sub    $0x30,%edx
  80099d:	eb 22                	jmp    8009c1 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  80099f:	8d 72 9f             	lea    -0x61(%edx),%esi
  8009a2:	89 f3                	mov    %esi,%ebx
  8009a4:	80 fb 19             	cmp    $0x19,%bl
  8009a7:	77 08                	ja     8009b1 <strtol+0x93>
			dig = *s - 'a' + 10;
  8009a9:	0f be d2             	movsbl %dl,%edx
  8009ac:	83 ea 57             	sub    $0x57,%edx
  8009af:	eb 10                	jmp    8009c1 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009b1:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009b4:	89 f3                	mov    %esi,%ebx
  8009b6:	80 fb 19             	cmp    $0x19,%bl
  8009b9:	77 14                	ja     8009cf <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009bb:	0f be d2             	movsbl %dl,%edx
  8009be:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009c1:	3b 55 10             	cmp    0x10(%ebp),%edx
  8009c4:	7d 09                	jge    8009cf <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009c6:	41                   	inc    %ecx
  8009c7:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009cb:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009cd:	eb be                	jmp    80098d <strtol+0x6f>

	if (endptr)
  8009cf:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009d3:	74 05                	je     8009da <strtol+0xbc>
		*endptr = (char *) s;
  8009d5:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009d8:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009da:	85 ff                	test   %edi,%edi
  8009dc:	74 21                	je     8009ff <strtol+0xe1>
  8009de:	f7 d8                	neg    %eax
  8009e0:	eb 1d                	jmp    8009ff <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009e2:	80 39 30             	cmpb   $0x30,(%ecx)
  8009e5:	75 9a                	jne    800981 <strtol+0x63>
  8009e7:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009eb:	0f 84 7a ff ff ff    	je     80096b <strtol+0x4d>
  8009f1:	eb 84                	jmp    800977 <strtol+0x59>
  8009f3:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009f7:	0f 84 6e ff ff ff    	je     80096b <strtol+0x4d>
  8009fd:	eb 89                	jmp    800988 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  8009ff:	5b                   	pop    %ebx
  800a00:	5e                   	pop    %esi
  800a01:	5f                   	pop    %edi
  800a02:	5d                   	pop    %ebp
  800a03:	c3                   	ret    

00800a04 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a04:	55                   	push   %ebp
  800a05:	89 e5                	mov    %esp,%ebp
  800a07:	57                   	push   %edi
  800a08:	56                   	push   %esi
  800a09:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a0a:	b8 00 00 00 00       	mov    $0x0,%eax
  800a0f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a12:	8b 55 08             	mov    0x8(%ebp),%edx
  800a15:	89 c3                	mov    %eax,%ebx
  800a17:	89 c7                	mov    %eax,%edi
  800a19:	89 c6                	mov    %eax,%esi
  800a1b:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a1d:	5b                   	pop    %ebx
  800a1e:	5e                   	pop    %esi
  800a1f:	5f                   	pop    %edi
  800a20:	5d                   	pop    %ebp
  800a21:	c3                   	ret    

00800a22 <sys_cgetc>:

int
sys_cgetc(void)
{
  800a22:	55                   	push   %ebp
  800a23:	89 e5                	mov    %esp,%ebp
  800a25:	57                   	push   %edi
  800a26:	56                   	push   %esi
  800a27:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a28:	ba 00 00 00 00       	mov    $0x0,%edx
  800a2d:	b8 01 00 00 00       	mov    $0x1,%eax
  800a32:	89 d1                	mov    %edx,%ecx
  800a34:	89 d3                	mov    %edx,%ebx
  800a36:	89 d7                	mov    %edx,%edi
  800a38:	89 d6                	mov    %edx,%esi
  800a3a:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a3c:	5b                   	pop    %ebx
  800a3d:	5e                   	pop    %esi
  800a3e:	5f                   	pop    %edi
  800a3f:	5d                   	pop    %ebp
  800a40:	c3                   	ret    

00800a41 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a41:	55                   	push   %ebp
  800a42:	89 e5                	mov    %esp,%ebp
  800a44:	57                   	push   %edi
  800a45:	56                   	push   %esi
  800a46:	53                   	push   %ebx
  800a47:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a4a:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a4f:	b8 03 00 00 00       	mov    $0x3,%eax
  800a54:	8b 55 08             	mov    0x8(%ebp),%edx
  800a57:	89 cb                	mov    %ecx,%ebx
  800a59:	89 cf                	mov    %ecx,%edi
  800a5b:	89 ce                	mov    %ecx,%esi
  800a5d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a5f:	85 c0                	test   %eax,%eax
  800a61:	7e 17                	jle    800a7a <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a63:	83 ec 0c             	sub    $0xc,%esp
  800a66:	50                   	push   %eax
  800a67:	6a 03                	push   $0x3
  800a69:	68 ac 0f 80 00       	push   $0x800fac
  800a6e:	6a 23                	push   $0x23
  800a70:	68 c9 0f 80 00       	push   $0x800fc9
  800a75:	e8 27 00 00 00       	call   800aa1 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a7a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a7d:	5b                   	pop    %ebx
  800a7e:	5e                   	pop    %esi
  800a7f:	5f                   	pop    %edi
  800a80:	5d                   	pop    %ebp
  800a81:	c3                   	ret    

00800a82 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a82:	55                   	push   %ebp
  800a83:	89 e5                	mov    %esp,%ebp
  800a85:	57                   	push   %edi
  800a86:	56                   	push   %esi
  800a87:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a88:	ba 00 00 00 00       	mov    $0x0,%edx
  800a8d:	b8 02 00 00 00       	mov    $0x2,%eax
  800a92:	89 d1                	mov    %edx,%ecx
  800a94:	89 d3                	mov    %edx,%ebx
  800a96:	89 d7                	mov    %edx,%edi
  800a98:	89 d6                	mov    %edx,%esi
  800a9a:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800a9c:	5b                   	pop    %ebx
  800a9d:	5e                   	pop    %esi
  800a9e:	5f                   	pop    %edi
  800a9f:	5d                   	pop    %ebp
  800aa0:	c3                   	ret    

00800aa1 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800aa1:	55                   	push   %ebp
  800aa2:	89 e5                	mov    %esp,%ebp
  800aa4:	56                   	push   %esi
  800aa5:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800aa6:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800aa9:	8b 35 00 10 80 00    	mov    0x801000,%esi
  800aaf:	e8 ce ff ff ff       	call   800a82 <sys_getenvid>
  800ab4:	83 ec 0c             	sub    $0xc,%esp
  800ab7:	ff 75 0c             	pushl  0xc(%ebp)
  800aba:	ff 75 08             	pushl  0x8(%ebp)
  800abd:	56                   	push   %esi
  800abe:	50                   	push   %eax
  800abf:	68 d8 0f 80 00       	push   $0x800fd8
  800ac4:	e8 af f6 ff ff       	call   800178 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800ac9:	83 c4 18             	add    $0x18,%esp
  800acc:	53                   	push   %ebx
  800acd:	ff 75 10             	pushl  0x10(%ebp)
  800ad0:	e8 52 f6 ff ff       	call   800127 <vcprintf>
	cprintf("\n");
  800ad5:	c7 04 24 63 0d 80 00 	movl   $0x800d63,(%esp)
  800adc:	e8 97 f6 ff ff       	call   800178 <cprintf>
  800ae1:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800ae4:	cc                   	int3   
  800ae5:	eb fd                	jmp    800ae4 <_panic+0x43>
  800ae7:	90                   	nop

00800ae8 <__udivdi3>:
  800ae8:	55                   	push   %ebp
  800ae9:	57                   	push   %edi
  800aea:	56                   	push   %esi
  800aeb:	53                   	push   %ebx
  800aec:	83 ec 1c             	sub    $0x1c,%esp
  800aef:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800af3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800af7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800afb:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800aff:	89 ca                	mov    %ecx,%edx
  800b01:	89 f8                	mov    %edi,%eax
  800b03:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800b07:	85 f6                	test   %esi,%esi
  800b09:	75 2d                	jne    800b38 <__udivdi3+0x50>
  800b0b:	39 cf                	cmp    %ecx,%edi
  800b0d:	77 65                	ja     800b74 <__udivdi3+0x8c>
  800b0f:	89 fd                	mov    %edi,%ebp
  800b11:	85 ff                	test   %edi,%edi
  800b13:	75 0b                	jne    800b20 <__udivdi3+0x38>
  800b15:	b8 01 00 00 00       	mov    $0x1,%eax
  800b1a:	31 d2                	xor    %edx,%edx
  800b1c:	f7 f7                	div    %edi
  800b1e:	89 c5                	mov    %eax,%ebp
  800b20:	31 d2                	xor    %edx,%edx
  800b22:	89 c8                	mov    %ecx,%eax
  800b24:	f7 f5                	div    %ebp
  800b26:	89 c1                	mov    %eax,%ecx
  800b28:	89 d8                	mov    %ebx,%eax
  800b2a:	f7 f5                	div    %ebp
  800b2c:	89 cf                	mov    %ecx,%edi
  800b2e:	89 fa                	mov    %edi,%edx
  800b30:	83 c4 1c             	add    $0x1c,%esp
  800b33:	5b                   	pop    %ebx
  800b34:	5e                   	pop    %esi
  800b35:	5f                   	pop    %edi
  800b36:	5d                   	pop    %ebp
  800b37:	c3                   	ret    
  800b38:	39 ce                	cmp    %ecx,%esi
  800b3a:	77 28                	ja     800b64 <__udivdi3+0x7c>
  800b3c:	0f bd fe             	bsr    %esi,%edi
  800b3f:	83 f7 1f             	xor    $0x1f,%edi
  800b42:	75 40                	jne    800b84 <__udivdi3+0x9c>
  800b44:	39 ce                	cmp    %ecx,%esi
  800b46:	72 0a                	jb     800b52 <__udivdi3+0x6a>
  800b48:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b4c:	0f 87 9e 00 00 00    	ja     800bf0 <__udivdi3+0x108>
  800b52:	b8 01 00 00 00       	mov    $0x1,%eax
  800b57:	89 fa                	mov    %edi,%edx
  800b59:	83 c4 1c             	add    $0x1c,%esp
  800b5c:	5b                   	pop    %ebx
  800b5d:	5e                   	pop    %esi
  800b5e:	5f                   	pop    %edi
  800b5f:	5d                   	pop    %ebp
  800b60:	c3                   	ret    
  800b61:	8d 76 00             	lea    0x0(%esi),%esi
  800b64:	31 ff                	xor    %edi,%edi
  800b66:	31 c0                	xor    %eax,%eax
  800b68:	89 fa                	mov    %edi,%edx
  800b6a:	83 c4 1c             	add    $0x1c,%esp
  800b6d:	5b                   	pop    %ebx
  800b6e:	5e                   	pop    %esi
  800b6f:	5f                   	pop    %edi
  800b70:	5d                   	pop    %ebp
  800b71:	c3                   	ret    
  800b72:	66 90                	xchg   %ax,%ax
  800b74:	89 d8                	mov    %ebx,%eax
  800b76:	f7 f7                	div    %edi
  800b78:	31 ff                	xor    %edi,%edi
  800b7a:	89 fa                	mov    %edi,%edx
  800b7c:	83 c4 1c             	add    $0x1c,%esp
  800b7f:	5b                   	pop    %ebx
  800b80:	5e                   	pop    %esi
  800b81:	5f                   	pop    %edi
  800b82:	5d                   	pop    %ebp
  800b83:	c3                   	ret    
  800b84:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b89:	89 eb                	mov    %ebp,%ebx
  800b8b:	29 fb                	sub    %edi,%ebx
  800b8d:	89 f9                	mov    %edi,%ecx
  800b8f:	d3 e6                	shl    %cl,%esi
  800b91:	89 c5                	mov    %eax,%ebp
  800b93:	88 d9                	mov    %bl,%cl
  800b95:	d3 ed                	shr    %cl,%ebp
  800b97:	89 e9                	mov    %ebp,%ecx
  800b99:	09 f1                	or     %esi,%ecx
  800b9b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b9f:	89 f9                	mov    %edi,%ecx
  800ba1:	d3 e0                	shl    %cl,%eax
  800ba3:	89 c5                	mov    %eax,%ebp
  800ba5:	89 d6                	mov    %edx,%esi
  800ba7:	88 d9                	mov    %bl,%cl
  800ba9:	d3 ee                	shr    %cl,%esi
  800bab:	89 f9                	mov    %edi,%ecx
  800bad:	d3 e2                	shl    %cl,%edx
  800baf:	8b 44 24 08          	mov    0x8(%esp),%eax
  800bb3:	88 d9                	mov    %bl,%cl
  800bb5:	d3 e8                	shr    %cl,%eax
  800bb7:	09 c2                	or     %eax,%edx
  800bb9:	89 d0                	mov    %edx,%eax
  800bbb:	89 f2                	mov    %esi,%edx
  800bbd:	f7 74 24 0c          	divl   0xc(%esp)
  800bc1:	89 d6                	mov    %edx,%esi
  800bc3:	89 c3                	mov    %eax,%ebx
  800bc5:	f7 e5                	mul    %ebp
  800bc7:	39 d6                	cmp    %edx,%esi
  800bc9:	72 19                	jb     800be4 <__udivdi3+0xfc>
  800bcb:	74 0b                	je     800bd8 <__udivdi3+0xf0>
  800bcd:	89 d8                	mov    %ebx,%eax
  800bcf:	31 ff                	xor    %edi,%edi
  800bd1:	e9 58 ff ff ff       	jmp    800b2e <__udivdi3+0x46>
  800bd6:	66 90                	xchg   %ax,%ax
  800bd8:	8b 54 24 08          	mov    0x8(%esp),%edx
  800bdc:	89 f9                	mov    %edi,%ecx
  800bde:	d3 e2                	shl    %cl,%edx
  800be0:	39 c2                	cmp    %eax,%edx
  800be2:	73 e9                	jae    800bcd <__udivdi3+0xe5>
  800be4:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800be7:	31 ff                	xor    %edi,%edi
  800be9:	e9 40 ff ff ff       	jmp    800b2e <__udivdi3+0x46>
  800bee:	66 90                	xchg   %ax,%ax
  800bf0:	31 c0                	xor    %eax,%eax
  800bf2:	e9 37 ff ff ff       	jmp    800b2e <__udivdi3+0x46>
  800bf7:	90                   	nop

00800bf8 <__umoddi3>:
  800bf8:	55                   	push   %ebp
  800bf9:	57                   	push   %edi
  800bfa:	56                   	push   %esi
  800bfb:	53                   	push   %ebx
  800bfc:	83 ec 1c             	sub    $0x1c,%esp
  800bff:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800c03:	8b 74 24 34          	mov    0x34(%esp),%esi
  800c07:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800c0b:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800c0f:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800c13:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800c17:	89 f3                	mov    %esi,%ebx
  800c19:	89 fa                	mov    %edi,%edx
  800c1b:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800c1f:	89 34 24             	mov    %esi,(%esp)
  800c22:	85 c0                	test   %eax,%eax
  800c24:	75 1a                	jne    800c40 <__umoddi3+0x48>
  800c26:	39 f7                	cmp    %esi,%edi
  800c28:	0f 86 a2 00 00 00    	jbe    800cd0 <__umoddi3+0xd8>
  800c2e:	89 c8                	mov    %ecx,%eax
  800c30:	89 f2                	mov    %esi,%edx
  800c32:	f7 f7                	div    %edi
  800c34:	89 d0                	mov    %edx,%eax
  800c36:	31 d2                	xor    %edx,%edx
  800c38:	83 c4 1c             	add    $0x1c,%esp
  800c3b:	5b                   	pop    %ebx
  800c3c:	5e                   	pop    %esi
  800c3d:	5f                   	pop    %edi
  800c3e:	5d                   	pop    %ebp
  800c3f:	c3                   	ret    
  800c40:	39 f0                	cmp    %esi,%eax
  800c42:	0f 87 ac 00 00 00    	ja     800cf4 <__umoddi3+0xfc>
  800c48:	0f bd e8             	bsr    %eax,%ebp
  800c4b:	83 f5 1f             	xor    $0x1f,%ebp
  800c4e:	0f 84 ac 00 00 00    	je     800d00 <__umoddi3+0x108>
  800c54:	bf 20 00 00 00       	mov    $0x20,%edi
  800c59:	29 ef                	sub    %ebp,%edi
  800c5b:	89 fe                	mov    %edi,%esi
  800c5d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c61:	89 e9                	mov    %ebp,%ecx
  800c63:	d3 e0                	shl    %cl,%eax
  800c65:	89 d7                	mov    %edx,%edi
  800c67:	89 f1                	mov    %esi,%ecx
  800c69:	d3 ef                	shr    %cl,%edi
  800c6b:	09 c7                	or     %eax,%edi
  800c6d:	89 e9                	mov    %ebp,%ecx
  800c6f:	d3 e2                	shl    %cl,%edx
  800c71:	89 14 24             	mov    %edx,(%esp)
  800c74:	89 d8                	mov    %ebx,%eax
  800c76:	d3 e0                	shl    %cl,%eax
  800c78:	89 c2                	mov    %eax,%edx
  800c7a:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c7e:	d3 e0                	shl    %cl,%eax
  800c80:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c84:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c88:	89 f1                	mov    %esi,%ecx
  800c8a:	d3 e8                	shr    %cl,%eax
  800c8c:	09 d0                	or     %edx,%eax
  800c8e:	d3 eb                	shr    %cl,%ebx
  800c90:	89 da                	mov    %ebx,%edx
  800c92:	f7 f7                	div    %edi
  800c94:	89 d3                	mov    %edx,%ebx
  800c96:	f7 24 24             	mull   (%esp)
  800c99:	89 c6                	mov    %eax,%esi
  800c9b:	89 d1                	mov    %edx,%ecx
  800c9d:	39 d3                	cmp    %edx,%ebx
  800c9f:	0f 82 87 00 00 00    	jb     800d2c <__umoddi3+0x134>
  800ca5:	0f 84 91 00 00 00    	je     800d3c <__umoddi3+0x144>
  800cab:	8b 54 24 04          	mov    0x4(%esp),%edx
  800caf:	29 f2                	sub    %esi,%edx
  800cb1:	19 cb                	sbb    %ecx,%ebx
  800cb3:	89 d8                	mov    %ebx,%eax
  800cb5:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800cb9:	d3 e0                	shl    %cl,%eax
  800cbb:	89 e9                	mov    %ebp,%ecx
  800cbd:	d3 ea                	shr    %cl,%edx
  800cbf:	09 d0                	or     %edx,%eax
  800cc1:	89 e9                	mov    %ebp,%ecx
  800cc3:	d3 eb                	shr    %cl,%ebx
  800cc5:	89 da                	mov    %ebx,%edx
  800cc7:	83 c4 1c             	add    $0x1c,%esp
  800cca:	5b                   	pop    %ebx
  800ccb:	5e                   	pop    %esi
  800ccc:	5f                   	pop    %edi
  800ccd:	5d                   	pop    %ebp
  800cce:	c3                   	ret    
  800ccf:	90                   	nop
  800cd0:	89 fd                	mov    %edi,%ebp
  800cd2:	85 ff                	test   %edi,%edi
  800cd4:	75 0b                	jne    800ce1 <__umoddi3+0xe9>
  800cd6:	b8 01 00 00 00       	mov    $0x1,%eax
  800cdb:	31 d2                	xor    %edx,%edx
  800cdd:	f7 f7                	div    %edi
  800cdf:	89 c5                	mov    %eax,%ebp
  800ce1:	89 f0                	mov    %esi,%eax
  800ce3:	31 d2                	xor    %edx,%edx
  800ce5:	f7 f5                	div    %ebp
  800ce7:	89 c8                	mov    %ecx,%eax
  800ce9:	f7 f5                	div    %ebp
  800ceb:	89 d0                	mov    %edx,%eax
  800ced:	e9 44 ff ff ff       	jmp    800c36 <__umoddi3+0x3e>
  800cf2:	66 90                	xchg   %ax,%ax
  800cf4:	89 c8                	mov    %ecx,%eax
  800cf6:	89 f2                	mov    %esi,%edx
  800cf8:	83 c4 1c             	add    $0x1c,%esp
  800cfb:	5b                   	pop    %ebx
  800cfc:	5e                   	pop    %esi
  800cfd:	5f                   	pop    %edi
  800cfe:	5d                   	pop    %ebp
  800cff:	c3                   	ret    
  800d00:	3b 04 24             	cmp    (%esp),%eax
  800d03:	72 06                	jb     800d0b <__umoddi3+0x113>
  800d05:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800d09:	77 0f                	ja     800d1a <__umoddi3+0x122>
  800d0b:	89 f2                	mov    %esi,%edx
  800d0d:	29 f9                	sub    %edi,%ecx
  800d0f:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800d13:	89 14 24             	mov    %edx,(%esp)
  800d16:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800d1a:	8b 44 24 04          	mov    0x4(%esp),%eax
  800d1e:	8b 14 24             	mov    (%esp),%edx
  800d21:	83 c4 1c             	add    $0x1c,%esp
  800d24:	5b                   	pop    %ebx
  800d25:	5e                   	pop    %esi
  800d26:	5f                   	pop    %edi
  800d27:	5d                   	pop    %ebp
  800d28:	c3                   	ret    
  800d29:	8d 76 00             	lea    0x0(%esi),%esi
  800d2c:	2b 04 24             	sub    (%esp),%eax
  800d2f:	19 fa                	sbb    %edi,%edx
  800d31:	89 d1                	mov    %edx,%ecx
  800d33:	89 c6                	mov    %eax,%esi
  800d35:	e9 71 ff ff ff       	jmp    800cab <__umoddi3+0xb3>
  800d3a:	66 90                	xchg   %ax,%ax
  800d3c:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d40:	72 ea                	jb     800d2c <__umoddi3+0x134>
  800d42:	89 d9                	mov    %ebx,%ecx
  800d44:	e9 62 ff ff ff       	jmp    800cab <__umoddi3+0xb3>
