
obj/user/divzero:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 2f 00 00 00       	call   800060 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

int zero;

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	zero = 0;
  800039:	c7 05 04 20 80 00 00 	movl   $0x0,0x802004
  800040:	00 00 00 
	cprintf("1/0 is %08x!\n", 1/zero);
  800043:	b8 01 00 00 00       	mov    $0x1,%eax
  800048:	b9 00 00 00 00       	mov    $0x0,%ecx
  80004d:	99                   	cltd   
  80004e:	f7 f9                	idiv   %ecx
  800050:	50                   	push   %eax
  800051:	68 64 0d 80 00       	push   $0x800d64
  800056:	e8 33 01 00 00       	call   80018e <cprintf>
}
  80005b:	83 c4 10             	add    $0x10,%esp
  80005e:	c9                   	leave  
  80005f:	c3                   	ret    

00800060 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800060:	55                   	push   %ebp
  800061:	89 e5                	mov    %esp,%ebp
  800063:	57                   	push   %edi
  800064:	56                   	push   %esi
  800065:	53                   	push   %ebx
  800066:	83 ec 0c             	sub    $0xc,%esp
  800069:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80006c:	8b 75 0c             	mov    0xc(%ebp),%esi
	// set thisenv to point at our Env structure in envs[].
	// LAB 3: Your code here.
	//thisenv = 0;
	int32_t env_Index1 = (int32_t)sys_getenvid();
  80006f:	e8 24 0a 00 00       	call   800a98 <sys_getenvid>
  800074:	89 c7                	mov    %eax,%edi
	cprintf("printing env_Index1: %d\n", env_Index1);
  800076:	83 ec 08             	sub    $0x8,%esp
  800079:	50                   	push   %eax
  80007a:	68 72 0d 80 00       	push   $0x800d72
  80007f:	e8 0a 01 00 00       	call   80018e <cprintf>
	int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	cprintf("printing env_Index2: %d\n", env_Index2);
  800084:	83 c4 08             	add    $0x8,%esp
  800087:	81 e7 ff 03 00 00    	and    $0x3ff,%edi
  80008d:	57                   	push   %edi
  80008e:	68 8b 0d 80 00       	push   $0x800d8b
  800093:	e8 f6 00 00 00       	call   80018e <cprintf>

	thisenv = &envs[ENVX(sys_getenvid())];
  800098:	e8 fb 09 00 00       	call   800a98 <sys_getenvid>
  80009d:	25 ff 03 00 00       	and    $0x3ff,%eax
  8000a2:	8d 14 00             	lea    (%eax,%eax,1),%edx
  8000a5:	01 d0                	add    %edx,%eax
  8000a7:	c1 e0 05             	shl    $0x5,%eax
  8000aa:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  8000af:	a3 08 20 80 00       	mov    %eax,0x802008
	cprintf("Addrs of thisenv (envs[0]) is: %p\n", thisenv);
  8000b4:	83 c4 08             	add    $0x8,%esp
  8000b7:	50                   	push   %eax
  8000b8:	68 b0 0d 80 00       	push   $0x800db0
  8000bd:	e8 cc 00 00 00       	call   80018e <cprintf>
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000c2:	83 c4 10             	add    $0x10,%esp
  8000c5:	85 db                	test   %ebx,%ebx
  8000c7:	7e 07                	jle    8000d0 <libmain+0x70>
		binaryname = argv[0];
  8000c9:	8b 06                	mov    (%esi),%eax
  8000cb:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  8000d0:	83 ec 08             	sub    $0x8,%esp
  8000d3:	56                   	push   %esi
  8000d4:	53                   	push   %ebx
  8000d5:	e8 59 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000da:	e8 0b 00 00 00       	call   8000ea <exit>
}
  8000df:	83 c4 10             	add    $0x10,%esp
  8000e2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8000e5:	5b                   	pop    %ebx
  8000e6:	5e                   	pop    %esi
  8000e7:	5f                   	pop    %edi
  8000e8:	5d                   	pop    %ebp
  8000e9:	c3                   	ret    

008000ea <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000ea:	55                   	push   %ebp
  8000eb:	89 e5                	mov    %esp,%ebp
  8000ed:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000f0:	6a 00                	push   $0x0
  8000f2:	e8 60 09 00 00       	call   800a57 <sys_env_destroy>
}
  8000f7:	83 c4 10             	add    $0x10,%esp
  8000fa:	c9                   	leave  
  8000fb:	c3                   	ret    

008000fc <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000fc:	55                   	push   %ebp
  8000fd:	89 e5                	mov    %esp,%ebp
  8000ff:	53                   	push   %ebx
  800100:	83 ec 04             	sub    $0x4,%esp
  800103:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800106:	8b 13                	mov    (%ebx),%edx
  800108:	8d 42 01             	lea    0x1(%edx),%eax
  80010b:	89 03                	mov    %eax,(%ebx)
  80010d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800110:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800114:	3d ff 00 00 00       	cmp    $0xff,%eax
  800119:	75 1a                	jne    800135 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80011b:	83 ec 08             	sub    $0x8,%esp
  80011e:	68 ff 00 00 00       	push   $0xff
  800123:	8d 43 08             	lea    0x8(%ebx),%eax
  800126:	50                   	push   %eax
  800127:	e8 ee 08 00 00       	call   800a1a <sys_cputs>
		b->idx = 0;
  80012c:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800132:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800135:	ff 43 04             	incl   0x4(%ebx)
}
  800138:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80013b:	c9                   	leave  
  80013c:	c3                   	ret    

0080013d <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80013d:	55                   	push   %ebp
  80013e:	89 e5                	mov    %esp,%ebp
  800140:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800146:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80014d:	00 00 00 
	b.cnt = 0;
  800150:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800157:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80015a:	ff 75 0c             	pushl  0xc(%ebp)
  80015d:	ff 75 08             	pushl  0x8(%ebp)
  800160:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800166:	50                   	push   %eax
  800167:	68 fc 00 80 00       	push   $0x8000fc
  80016c:	e8 51 01 00 00       	call   8002c2 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800171:	83 c4 08             	add    $0x8,%esp
  800174:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80017a:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800180:	50                   	push   %eax
  800181:	e8 94 08 00 00       	call   800a1a <sys_cputs>

	return b.cnt;
}
  800186:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80018c:	c9                   	leave  
  80018d:	c3                   	ret    

0080018e <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80018e:	55                   	push   %ebp
  80018f:	89 e5                	mov    %esp,%ebp
  800191:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800194:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800197:	50                   	push   %eax
  800198:	ff 75 08             	pushl  0x8(%ebp)
  80019b:	e8 9d ff ff ff       	call   80013d <vcprintf>
	va_end(ap);

	return cnt;
}
  8001a0:	c9                   	leave  
  8001a1:	c3                   	ret    

008001a2 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8001a2:	55                   	push   %ebp
  8001a3:	89 e5                	mov    %esp,%ebp
  8001a5:	57                   	push   %edi
  8001a6:	56                   	push   %esi
  8001a7:	53                   	push   %ebx
  8001a8:	83 ec 1c             	sub    $0x1c,%esp
  8001ab:	89 c7                	mov    %eax,%edi
  8001ad:	89 d6                	mov    %edx,%esi
  8001af:	8b 45 08             	mov    0x8(%ebp),%eax
  8001b2:	8b 55 0c             	mov    0xc(%ebp),%edx
  8001b5:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8001b8:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001bb:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8001be:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001c3:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001c6:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001c9:	39 d3                	cmp    %edx,%ebx
  8001cb:	72 05                	jb     8001d2 <printnum+0x30>
  8001cd:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001d0:	77 45                	ja     800217 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001d2:	83 ec 0c             	sub    $0xc,%esp
  8001d5:	ff 75 18             	pushl  0x18(%ebp)
  8001d8:	8b 45 14             	mov    0x14(%ebp),%eax
  8001db:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001de:	53                   	push   %ebx
  8001df:	ff 75 10             	pushl  0x10(%ebp)
  8001e2:	83 ec 08             	sub    $0x8,%esp
  8001e5:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001e8:	ff 75 e0             	pushl  -0x20(%ebp)
  8001eb:	ff 75 dc             	pushl  -0x24(%ebp)
  8001ee:	ff 75 d8             	pushl  -0x28(%ebp)
  8001f1:	e8 0a 09 00 00       	call   800b00 <__udivdi3>
  8001f6:	83 c4 18             	add    $0x18,%esp
  8001f9:	52                   	push   %edx
  8001fa:	50                   	push   %eax
  8001fb:	89 f2                	mov    %esi,%edx
  8001fd:	89 f8                	mov    %edi,%eax
  8001ff:	e8 9e ff ff ff       	call   8001a2 <printnum>
  800204:	83 c4 20             	add    $0x20,%esp
  800207:	eb 16                	jmp    80021f <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800209:	83 ec 08             	sub    $0x8,%esp
  80020c:	56                   	push   %esi
  80020d:	ff 75 18             	pushl  0x18(%ebp)
  800210:	ff d7                	call   *%edi
  800212:	83 c4 10             	add    $0x10,%esp
  800215:	eb 03                	jmp    80021a <printnum+0x78>
  800217:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80021a:	4b                   	dec    %ebx
  80021b:	85 db                	test   %ebx,%ebx
  80021d:	7f ea                	jg     800209 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80021f:	83 ec 08             	sub    $0x8,%esp
  800222:	56                   	push   %esi
  800223:	83 ec 04             	sub    $0x4,%esp
  800226:	ff 75 e4             	pushl  -0x1c(%ebp)
  800229:	ff 75 e0             	pushl  -0x20(%ebp)
  80022c:	ff 75 dc             	pushl  -0x24(%ebp)
  80022f:	ff 75 d8             	pushl  -0x28(%ebp)
  800232:	e8 d9 09 00 00       	call   800c10 <__umoddi3>
  800237:	83 c4 14             	add    $0x14,%esp
  80023a:	0f be 80 d3 0d 80 00 	movsbl 0x800dd3(%eax),%eax
  800241:	50                   	push   %eax
  800242:	ff d7                	call   *%edi
}
  800244:	83 c4 10             	add    $0x10,%esp
  800247:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80024a:	5b                   	pop    %ebx
  80024b:	5e                   	pop    %esi
  80024c:	5f                   	pop    %edi
  80024d:	5d                   	pop    %ebp
  80024e:	c3                   	ret    

0080024f <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80024f:	55                   	push   %ebp
  800250:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800252:	83 fa 01             	cmp    $0x1,%edx
  800255:	7e 0e                	jle    800265 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800257:	8b 10                	mov    (%eax),%edx
  800259:	8d 4a 08             	lea    0x8(%edx),%ecx
  80025c:	89 08                	mov    %ecx,(%eax)
  80025e:	8b 02                	mov    (%edx),%eax
  800260:	8b 52 04             	mov    0x4(%edx),%edx
  800263:	eb 22                	jmp    800287 <getuint+0x38>
	else if (lflag)
  800265:	85 d2                	test   %edx,%edx
  800267:	74 10                	je     800279 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800269:	8b 10                	mov    (%eax),%edx
  80026b:	8d 4a 04             	lea    0x4(%edx),%ecx
  80026e:	89 08                	mov    %ecx,(%eax)
  800270:	8b 02                	mov    (%edx),%eax
  800272:	ba 00 00 00 00       	mov    $0x0,%edx
  800277:	eb 0e                	jmp    800287 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800279:	8b 10                	mov    (%eax),%edx
  80027b:	8d 4a 04             	lea    0x4(%edx),%ecx
  80027e:	89 08                	mov    %ecx,(%eax)
  800280:	8b 02                	mov    (%edx),%eax
  800282:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800287:	5d                   	pop    %ebp
  800288:	c3                   	ret    

00800289 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800289:	55                   	push   %ebp
  80028a:	89 e5                	mov    %esp,%ebp
  80028c:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80028f:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800292:	8b 10                	mov    (%eax),%edx
  800294:	3b 50 04             	cmp    0x4(%eax),%edx
  800297:	73 0a                	jae    8002a3 <sprintputch+0x1a>
		*b->buf++ = ch;
  800299:	8d 4a 01             	lea    0x1(%edx),%ecx
  80029c:	89 08                	mov    %ecx,(%eax)
  80029e:	8b 45 08             	mov    0x8(%ebp),%eax
  8002a1:	88 02                	mov    %al,(%edx)
}
  8002a3:	5d                   	pop    %ebp
  8002a4:	c3                   	ret    

008002a5 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8002a5:	55                   	push   %ebp
  8002a6:	89 e5                	mov    %esp,%ebp
  8002a8:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  8002ab:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  8002ae:	50                   	push   %eax
  8002af:	ff 75 10             	pushl  0x10(%ebp)
  8002b2:	ff 75 0c             	pushl  0xc(%ebp)
  8002b5:	ff 75 08             	pushl  0x8(%ebp)
  8002b8:	e8 05 00 00 00       	call   8002c2 <vprintfmt>
	va_end(ap);
}
  8002bd:	83 c4 10             	add    $0x10,%esp
  8002c0:	c9                   	leave  
  8002c1:	c3                   	ret    

008002c2 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8002c2:	55                   	push   %ebp
  8002c3:	89 e5                	mov    %esp,%ebp
  8002c5:	57                   	push   %edi
  8002c6:	56                   	push   %esi
  8002c7:	53                   	push   %ebx
  8002c8:	83 ec 2c             	sub    $0x2c,%esp
  8002cb:	8b 75 08             	mov    0x8(%ebp),%esi
  8002ce:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002d1:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002d4:	eb 12                	jmp    8002e8 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002d6:	85 c0                	test   %eax,%eax
  8002d8:	0f 84 68 03 00 00    	je     800646 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002de:	83 ec 08             	sub    $0x8,%esp
  8002e1:	53                   	push   %ebx
  8002e2:	50                   	push   %eax
  8002e3:	ff d6                	call   *%esi
  8002e5:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002e8:	47                   	inc    %edi
  8002e9:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002ed:	83 f8 25             	cmp    $0x25,%eax
  8002f0:	75 e4                	jne    8002d6 <vprintfmt+0x14>
  8002f2:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002f6:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002fd:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800304:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80030b:	ba 00 00 00 00       	mov    $0x0,%edx
  800310:	eb 07                	jmp    800319 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800312:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800315:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800319:	8d 47 01             	lea    0x1(%edi),%eax
  80031c:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80031f:	0f b6 0f             	movzbl (%edi),%ecx
  800322:	8a 07                	mov    (%edi),%al
  800324:	83 e8 23             	sub    $0x23,%eax
  800327:	3c 55                	cmp    $0x55,%al
  800329:	0f 87 fe 02 00 00    	ja     80062d <vprintfmt+0x36b>
  80032f:	0f b6 c0             	movzbl %al,%eax
  800332:	ff 24 85 60 0e 80 00 	jmp    *0x800e60(,%eax,4)
  800339:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80033c:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800340:	eb d7                	jmp    800319 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800342:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800345:	b8 00 00 00 00       	mov    $0x0,%eax
  80034a:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80034d:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800350:	01 c0                	add    %eax,%eax
  800352:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800356:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800359:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80035c:	83 fa 09             	cmp    $0x9,%edx
  80035f:	77 34                	ja     800395 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800361:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800362:	eb e9                	jmp    80034d <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800364:	8b 45 14             	mov    0x14(%ebp),%eax
  800367:	8d 48 04             	lea    0x4(%eax),%ecx
  80036a:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80036d:	8b 00                	mov    (%eax),%eax
  80036f:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800372:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800375:	eb 24                	jmp    80039b <vprintfmt+0xd9>
  800377:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80037b:	79 07                	jns    800384 <vprintfmt+0xc2>
  80037d:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800384:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800387:	eb 90                	jmp    800319 <vprintfmt+0x57>
  800389:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80038c:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800393:	eb 84                	jmp    800319 <vprintfmt+0x57>
  800395:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800398:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80039b:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80039f:	0f 89 74 ff ff ff    	jns    800319 <vprintfmt+0x57>
				width = precision, precision = -1;
  8003a5:	8b 45 d0             	mov    -0x30(%ebp),%eax
  8003a8:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8003ab:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003b2:	e9 62 ff ff ff       	jmp    800319 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8003b7:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003b8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8003bb:	e9 59 ff ff ff       	jmp    800319 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8003c0:	8b 45 14             	mov    0x14(%ebp),%eax
  8003c3:	8d 50 04             	lea    0x4(%eax),%edx
  8003c6:	89 55 14             	mov    %edx,0x14(%ebp)
  8003c9:	83 ec 08             	sub    $0x8,%esp
  8003cc:	53                   	push   %ebx
  8003cd:	ff 30                	pushl  (%eax)
  8003cf:	ff d6                	call   *%esi
			break;
  8003d1:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003d4:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003d7:	e9 0c ff ff ff       	jmp    8002e8 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003dc:	8b 45 14             	mov    0x14(%ebp),%eax
  8003df:	8d 50 04             	lea    0x4(%eax),%edx
  8003e2:	89 55 14             	mov    %edx,0x14(%ebp)
  8003e5:	8b 00                	mov    (%eax),%eax
  8003e7:	85 c0                	test   %eax,%eax
  8003e9:	79 02                	jns    8003ed <vprintfmt+0x12b>
  8003eb:	f7 d8                	neg    %eax
  8003ed:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003ef:	83 f8 06             	cmp    $0x6,%eax
  8003f2:	7f 0b                	jg     8003ff <vprintfmt+0x13d>
  8003f4:	8b 04 85 b8 0f 80 00 	mov    0x800fb8(,%eax,4),%eax
  8003fb:	85 c0                	test   %eax,%eax
  8003fd:	75 18                	jne    800417 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003ff:	52                   	push   %edx
  800400:	68 eb 0d 80 00       	push   $0x800deb
  800405:	53                   	push   %ebx
  800406:	56                   	push   %esi
  800407:	e8 99 fe ff ff       	call   8002a5 <printfmt>
  80040c:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80040f:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800412:	e9 d1 fe ff ff       	jmp    8002e8 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800417:	50                   	push   %eax
  800418:	68 f4 0d 80 00       	push   $0x800df4
  80041d:	53                   	push   %ebx
  80041e:	56                   	push   %esi
  80041f:	e8 81 fe ff ff       	call   8002a5 <printfmt>
  800424:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800427:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80042a:	e9 b9 fe ff ff       	jmp    8002e8 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80042f:	8b 45 14             	mov    0x14(%ebp),%eax
  800432:	8d 50 04             	lea    0x4(%eax),%edx
  800435:	89 55 14             	mov    %edx,0x14(%ebp)
  800438:	8b 38                	mov    (%eax),%edi
  80043a:	85 ff                	test   %edi,%edi
  80043c:	75 05                	jne    800443 <vprintfmt+0x181>
				p = "(null)";
  80043e:	bf e4 0d 80 00       	mov    $0x800de4,%edi
			if (width > 0 && padc != '-')
  800443:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800447:	0f 8e 90 00 00 00    	jle    8004dd <vprintfmt+0x21b>
  80044d:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800451:	0f 84 8e 00 00 00    	je     8004e5 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800457:	83 ec 08             	sub    $0x8,%esp
  80045a:	ff 75 d0             	pushl  -0x30(%ebp)
  80045d:	57                   	push   %edi
  80045e:	e8 70 02 00 00       	call   8006d3 <strnlen>
  800463:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800466:	29 c1                	sub    %eax,%ecx
  800468:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80046b:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80046e:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800472:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800475:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800478:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80047a:	eb 0d                	jmp    800489 <vprintfmt+0x1c7>
					putch(padc, putdat);
  80047c:	83 ec 08             	sub    $0x8,%esp
  80047f:	53                   	push   %ebx
  800480:	ff 75 e4             	pushl  -0x1c(%ebp)
  800483:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800485:	4f                   	dec    %edi
  800486:	83 c4 10             	add    $0x10,%esp
  800489:	85 ff                	test   %edi,%edi
  80048b:	7f ef                	jg     80047c <vprintfmt+0x1ba>
  80048d:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800490:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800493:	89 c8                	mov    %ecx,%eax
  800495:	85 c9                	test   %ecx,%ecx
  800497:	79 05                	jns    80049e <vprintfmt+0x1dc>
  800499:	b8 00 00 00 00       	mov    $0x0,%eax
  80049e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  8004a1:	29 c1                	sub    %eax,%ecx
  8004a3:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  8004a6:	89 75 08             	mov    %esi,0x8(%ebp)
  8004a9:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004ac:	eb 3d                	jmp    8004eb <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  8004ae:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8004b2:	74 19                	je     8004cd <vprintfmt+0x20b>
  8004b4:	0f be c0             	movsbl %al,%eax
  8004b7:	83 e8 20             	sub    $0x20,%eax
  8004ba:	83 f8 5e             	cmp    $0x5e,%eax
  8004bd:	76 0e                	jbe    8004cd <vprintfmt+0x20b>
					putch('?', putdat);
  8004bf:	83 ec 08             	sub    $0x8,%esp
  8004c2:	53                   	push   %ebx
  8004c3:	6a 3f                	push   $0x3f
  8004c5:	ff 55 08             	call   *0x8(%ebp)
  8004c8:	83 c4 10             	add    $0x10,%esp
  8004cb:	eb 0b                	jmp    8004d8 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004cd:	83 ec 08             	sub    $0x8,%esp
  8004d0:	53                   	push   %ebx
  8004d1:	52                   	push   %edx
  8004d2:	ff 55 08             	call   *0x8(%ebp)
  8004d5:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004d8:	ff 4d e4             	decl   -0x1c(%ebp)
  8004db:	eb 0e                	jmp    8004eb <vprintfmt+0x229>
  8004dd:	89 75 08             	mov    %esi,0x8(%ebp)
  8004e0:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004e3:	eb 06                	jmp    8004eb <vprintfmt+0x229>
  8004e5:	89 75 08             	mov    %esi,0x8(%ebp)
  8004e8:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004eb:	47                   	inc    %edi
  8004ec:	8a 47 ff             	mov    -0x1(%edi),%al
  8004ef:	0f be d0             	movsbl %al,%edx
  8004f2:	85 d2                	test   %edx,%edx
  8004f4:	74 1d                	je     800513 <vprintfmt+0x251>
  8004f6:	85 f6                	test   %esi,%esi
  8004f8:	78 b4                	js     8004ae <vprintfmt+0x1ec>
  8004fa:	4e                   	dec    %esi
  8004fb:	79 b1                	jns    8004ae <vprintfmt+0x1ec>
  8004fd:	8b 75 08             	mov    0x8(%ebp),%esi
  800500:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800503:	eb 14                	jmp    800519 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800505:	83 ec 08             	sub    $0x8,%esp
  800508:	53                   	push   %ebx
  800509:	6a 20                	push   $0x20
  80050b:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80050d:	4f                   	dec    %edi
  80050e:	83 c4 10             	add    $0x10,%esp
  800511:	eb 06                	jmp    800519 <vprintfmt+0x257>
  800513:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800516:	8b 75 08             	mov    0x8(%ebp),%esi
  800519:	85 ff                	test   %edi,%edi
  80051b:	7f e8                	jg     800505 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80051d:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800520:	e9 c3 fd ff ff       	jmp    8002e8 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800525:	83 fa 01             	cmp    $0x1,%edx
  800528:	7e 16                	jle    800540 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  80052a:	8b 45 14             	mov    0x14(%ebp),%eax
  80052d:	8d 50 08             	lea    0x8(%eax),%edx
  800530:	89 55 14             	mov    %edx,0x14(%ebp)
  800533:	8b 50 04             	mov    0x4(%eax),%edx
  800536:	8b 00                	mov    (%eax),%eax
  800538:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80053b:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80053e:	eb 32                	jmp    800572 <vprintfmt+0x2b0>
	else if (lflag)
  800540:	85 d2                	test   %edx,%edx
  800542:	74 18                	je     80055c <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800544:	8b 45 14             	mov    0x14(%ebp),%eax
  800547:	8d 50 04             	lea    0x4(%eax),%edx
  80054a:	89 55 14             	mov    %edx,0x14(%ebp)
  80054d:	8b 00                	mov    (%eax),%eax
  80054f:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800552:	89 c1                	mov    %eax,%ecx
  800554:	c1 f9 1f             	sar    $0x1f,%ecx
  800557:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80055a:	eb 16                	jmp    800572 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80055c:	8b 45 14             	mov    0x14(%ebp),%eax
  80055f:	8d 50 04             	lea    0x4(%eax),%edx
  800562:	89 55 14             	mov    %edx,0x14(%ebp)
  800565:	8b 00                	mov    (%eax),%eax
  800567:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80056a:	89 c1                	mov    %eax,%ecx
  80056c:	c1 f9 1f             	sar    $0x1f,%ecx
  80056f:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800572:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800575:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800578:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80057c:	79 76                	jns    8005f4 <vprintfmt+0x332>
				putch('-', putdat);
  80057e:	83 ec 08             	sub    $0x8,%esp
  800581:	53                   	push   %ebx
  800582:	6a 2d                	push   $0x2d
  800584:	ff d6                	call   *%esi
				num = -(long long) num;
  800586:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800589:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80058c:	f7 d8                	neg    %eax
  80058e:	83 d2 00             	adc    $0x0,%edx
  800591:	f7 da                	neg    %edx
  800593:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800596:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80059b:	eb 5c                	jmp    8005f9 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80059d:	8d 45 14             	lea    0x14(%ebp),%eax
  8005a0:	e8 aa fc ff ff       	call   80024f <getuint>
			base = 10;
  8005a5:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  8005aa:	eb 4d                	jmp    8005f9 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  8005ac:	8d 45 14             	lea    0x14(%ebp),%eax
  8005af:	e8 9b fc ff ff       	call   80024f <getuint>
			base = 8;
  8005b4:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8005b9:	eb 3e                	jmp    8005f9 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8005bb:	83 ec 08             	sub    $0x8,%esp
  8005be:	53                   	push   %ebx
  8005bf:	6a 30                	push   $0x30
  8005c1:	ff d6                	call   *%esi
			putch('x', putdat);
  8005c3:	83 c4 08             	add    $0x8,%esp
  8005c6:	53                   	push   %ebx
  8005c7:	6a 78                	push   $0x78
  8005c9:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005cb:	8b 45 14             	mov    0x14(%ebp),%eax
  8005ce:	8d 50 04             	lea    0x4(%eax),%edx
  8005d1:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005d4:	8b 00                	mov    (%eax),%eax
  8005d6:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005db:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005de:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005e3:	eb 14                	jmp    8005f9 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005e5:	8d 45 14             	lea    0x14(%ebp),%eax
  8005e8:	e8 62 fc ff ff       	call   80024f <getuint>
			base = 16;
  8005ed:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005f2:	eb 05                	jmp    8005f9 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005f4:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005f9:	83 ec 0c             	sub    $0xc,%esp
  8005fc:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800600:	57                   	push   %edi
  800601:	ff 75 e4             	pushl  -0x1c(%ebp)
  800604:	51                   	push   %ecx
  800605:	52                   	push   %edx
  800606:	50                   	push   %eax
  800607:	89 da                	mov    %ebx,%edx
  800609:	89 f0                	mov    %esi,%eax
  80060b:	e8 92 fb ff ff       	call   8001a2 <printnum>
			break;
  800610:	83 c4 20             	add    $0x20,%esp
  800613:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800616:	e9 cd fc ff ff       	jmp    8002e8 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80061b:	83 ec 08             	sub    $0x8,%esp
  80061e:	53                   	push   %ebx
  80061f:	51                   	push   %ecx
  800620:	ff d6                	call   *%esi
			break;
  800622:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800625:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800628:	e9 bb fc ff ff       	jmp    8002e8 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80062d:	83 ec 08             	sub    $0x8,%esp
  800630:	53                   	push   %ebx
  800631:	6a 25                	push   $0x25
  800633:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800635:	83 c4 10             	add    $0x10,%esp
  800638:	eb 01                	jmp    80063b <vprintfmt+0x379>
  80063a:	4f                   	dec    %edi
  80063b:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  80063f:	75 f9                	jne    80063a <vprintfmt+0x378>
  800641:	e9 a2 fc ff ff       	jmp    8002e8 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800646:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800649:	5b                   	pop    %ebx
  80064a:	5e                   	pop    %esi
  80064b:	5f                   	pop    %edi
  80064c:	5d                   	pop    %ebp
  80064d:	c3                   	ret    

0080064e <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80064e:	55                   	push   %ebp
  80064f:	89 e5                	mov    %esp,%ebp
  800651:	83 ec 18             	sub    $0x18,%esp
  800654:	8b 45 08             	mov    0x8(%ebp),%eax
  800657:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80065a:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80065d:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800661:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800664:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80066b:	85 c0                	test   %eax,%eax
  80066d:	74 26                	je     800695 <vsnprintf+0x47>
  80066f:	85 d2                	test   %edx,%edx
  800671:	7e 29                	jle    80069c <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800673:	ff 75 14             	pushl  0x14(%ebp)
  800676:	ff 75 10             	pushl  0x10(%ebp)
  800679:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80067c:	50                   	push   %eax
  80067d:	68 89 02 80 00       	push   $0x800289
  800682:	e8 3b fc ff ff       	call   8002c2 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800687:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80068a:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80068d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800690:	83 c4 10             	add    $0x10,%esp
  800693:	eb 0c                	jmp    8006a1 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800695:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80069a:	eb 05                	jmp    8006a1 <vsnprintf+0x53>
  80069c:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  8006a1:	c9                   	leave  
  8006a2:	c3                   	ret    

008006a3 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8006a3:	55                   	push   %ebp
  8006a4:	89 e5                	mov    %esp,%ebp
  8006a6:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8006a9:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  8006ac:	50                   	push   %eax
  8006ad:	ff 75 10             	pushl  0x10(%ebp)
  8006b0:	ff 75 0c             	pushl  0xc(%ebp)
  8006b3:	ff 75 08             	pushl  0x8(%ebp)
  8006b6:	e8 93 ff ff ff       	call   80064e <vsnprintf>
	va_end(ap);

	return rc;
}
  8006bb:	c9                   	leave  
  8006bc:	c3                   	ret    

008006bd <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8006bd:	55                   	push   %ebp
  8006be:	89 e5                	mov    %esp,%ebp
  8006c0:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8006c3:	b8 00 00 00 00       	mov    $0x0,%eax
  8006c8:	eb 01                	jmp    8006cb <strlen+0xe>
		n++;
  8006ca:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006cb:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006cf:	75 f9                	jne    8006ca <strlen+0xd>
		n++;
	return n;
}
  8006d1:	5d                   	pop    %ebp
  8006d2:	c3                   	ret    

008006d3 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006d3:	55                   	push   %ebp
  8006d4:	89 e5                	mov    %esp,%ebp
  8006d6:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006d9:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006dc:	ba 00 00 00 00       	mov    $0x0,%edx
  8006e1:	eb 01                	jmp    8006e4 <strnlen+0x11>
		n++;
  8006e3:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006e4:	39 c2                	cmp    %eax,%edx
  8006e6:	74 08                	je     8006f0 <strnlen+0x1d>
  8006e8:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006ec:	75 f5                	jne    8006e3 <strnlen+0x10>
  8006ee:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006f0:	5d                   	pop    %ebp
  8006f1:	c3                   	ret    

008006f2 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006f2:	55                   	push   %ebp
  8006f3:	89 e5                	mov    %esp,%ebp
  8006f5:	53                   	push   %ebx
  8006f6:	8b 45 08             	mov    0x8(%ebp),%eax
  8006f9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006fc:	89 c2                	mov    %eax,%edx
  8006fe:	42                   	inc    %edx
  8006ff:	41                   	inc    %ecx
  800700:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800703:	88 5a ff             	mov    %bl,-0x1(%edx)
  800706:	84 db                	test   %bl,%bl
  800708:	75 f4                	jne    8006fe <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80070a:	5b                   	pop    %ebx
  80070b:	5d                   	pop    %ebp
  80070c:	c3                   	ret    

0080070d <strcat>:

char *
strcat(char *dst, const char *src)
{
  80070d:	55                   	push   %ebp
  80070e:	89 e5                	mov    %esp,%ebp
  800710:	53                   	push   %ebx
  800711:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800714:	53                   	push   %ebx
  800715:	e8 a3 ff ff ff       	call   8006bd <strlen>
  80071a:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80071d:	ff 75 0c             	pushl  0xc(%ebp)
  800720:	01 d8                	add    %ebx,%eax
  800722:	50                   	push   %eax
  800723:	e8 ca ff ff ff       	call   8006f2 <strcpy>
	return dst;
}
  800728:	89 d8                	mov    %ebx,%eax
  80072a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80072d:	c9                   	leave  
  80072e:	c3                   	ret    

0080072f <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80072f:	55                   	push   %ebp
  800730:	89 e5                	mov    %esp,%ebp
  800732:	56                   	push   %esi
  800733:	53                   	push   %ebx
  800734:	8b 75 08             	mov    0x8(%ebp),%esi
  800737:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80073a:	89 f3                	mov    %esi,%ebx
  80073c:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80073f:	89 f2                	mov    %esi,%edx
  800741:	eb 0c                	jmp    80074f <strncpy+0x20>
		*dst++ = *src;
  800743:	42                   	inc    %edx
  800744:	8a 01                	mov    (%ecx),%al
  800746:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800749:	80 39 01             	cmpb   $0x1,(%ecx)
  80074c:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80074f:	39 da                	cmp    %ebx,%edx
  800751:	75 f0                	jne    800743 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800753:	89 f0                	mov    %esi,%eax
  800755:	5b                   	pop    %ebx
  800756:	5e                   	pop    %esi
  800757:	5d                   	pop    %ebp
  800758:	c3                   	ret    

00800759 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800759:	55                   	push   %ebp
  80075a:	89 e5                	mov    %esp,%ebp
  80075c:	56                   	push   %esi
  80075d:	53                   	push   %ebx
  80075e:	8b 75 08             	mov    0x8(%ebp),%esi
  800761:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800764:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800767:	85 c0                	test   %eax,%eax
  800769:	74 1e                	je     800789 <strlcpy+0x30>
  80076b:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  80076f:	89 f2                	mov    %esi,%edx
  800771:	eb 05                	jmp    800778 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800773:	42                   	inc    %edx
  800774:	41                   	inc    %ecx
  800775:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800778:	39 c2                	cmp    %eax,%edx
  80077a:	74 08                	je     800784 <strlcpy+0x2b>
  80077c:	8a 19                	mov    (%ecx),%bl
  80077e:	84 db                	test   %bl,%bl
  800780:	75 f1                	jne    800773 <strlcpy+0x1a>
  800782:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800784:	c6 00 00             	movb   $0x0,(%eax)
  800787:	eb 02                	jmp    80078b <strlcpy+0x32>
  800789:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  80078b:	29 f0                	sub    %esi,%eax
}
  80078d:	5b                   	pop    %ebx
  80078e:	5e                   	pop    %esi
  80078f:	5d                   	pop    %ebp
  800790:	c3                   	ret    

00800791 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800791:	55                   	push   %ebp
  800792:	89 e5                	mov    %esp,%ebp
  800794:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800797:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80079a:	eb 02                	jmp    80079e <strcmp+0xd>
		p++, q++;
  80079c:	41                   	inc    %ecx
  80079d:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80079e:	8a 01                	mov    (%ecx),%al
  8007a0:	84 c0                	test   %al,%al
  8007a2:	74 04                	je     8007a8 <strcmp+0x17>
  8007a4:	3a 02                	cmp    (%edx),%al
  8007a6:	74 f4                	je     80079c <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  8007a8:	0f b6 c0             	movzbl %al,%eax
  8007ab:	0f b6 12             	movzbl (%edx),%edx
  8007ae:	29 d0                	sub    %edx,%eax
}
  8007b0:	5d                   	pop    %ebp
  8007b1:	c3                   	ret    

008007b2 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  8007b2:	55                   	push   %ebp
  8007b3:	89 e5                	mov    %esp,%ebp
  8007b5:	53                   	push   %ebx
  8007b6:	8b 45 08             	mov    0x8(%ebp),%eax
  8007b9:	8b 55 0c             	mov    0xc(%ebp),%edx
  8007bc:	89 c3                	mov    %eax,%ebx
  8007be:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8007c1:	eb 02                	jmp    8007c5 <strncmp+0x13>
		n--, p++, q++;
  8007c3:	40                   	inc    %eax
  8007c4:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8007c5:	39 d8                	cmp    %ebx,%eax
  8007c7:	74 14                	je     8007dd <strncmp+0x2b>
  8007c9:	8a 08                	mov    (%eax),%cl
  8007cb:	84 c9                	test   %cl,%cl
  8007cd:	74 04                	je     8007d3 <strncmp+0x21>
  8007cf:	3a 0a                	cmp    (%edx),%cl
  8007d1:	74 f0                	je     8007c3 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007d3:	0f b6 00             	movzbl (%eax),%eax
  8007d6:	0f b6 12             	movzbl (%edx),%edx
  8007d9:	29 d0                	sub    %edx,%eax
  8007db:	eb 05                	jmp    8007e2 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007dd:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007e2:	5b                   	pop    %ebx
  8007e3:	5d                   	pop    %ebp
  8007e4:	c3                   	ret    

008007e5 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007e5:	55                   	push   %ebp
  8007e6:	89 e5                	mov    %esp,%ebp
  8007e8:	8b 45 08             	mov    0x8(%ebp),%eax
  8007eb:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007ee:	eb 05                	jmp    8007f5 <strchr+0x10>
		if (*s == c)
  8007f0:	38 ca                	cmp    %cl,%dl
  8007f2:	74 0c                	je     800800 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007f4:	40                   	inc    %eax
  8007f5:	8a 10                	mov    (%eax),%dl
  8007f7:	84 d2                	test   %dl,%dl
  8007f9:	75 f5                	jne    8007f0 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007fb:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800800:	5d                   	pop    %ebp
  800801:	c3                   	ret    

00800802 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800802:	55                   	push   %ebp
  800803:	89 e5                	mov    %esp,%ebp
  800805:	8b 45 08             	mov    0x8(%ebp),%eax
  800808:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80080b:	eb 05                	jmp    800812 <strfind+0x10>
		if (*s == c)
  80080d:	38 ca                	cmp    %cl,%dl
  80080f:	74 07                	je     800818 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800811:	40                   	inc    %eax
  800812:	8a 10                	mov    (%eax),%dl
  800814:	84 d2                	test   %dl,%dl
  800816:	75 f5                	jne    80080d <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800818:	5d                   	pop    %ebp
  800819:	c3                   	ret    

0080081a <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  80081a:	55                   	push   %ebp
  80081b:	89 e5                	mov    %esp,%ebp
  80081d:	57                   	push   %edi
  80081e:	56                   	push   %esi
  80081f:	53                   	push   %ebx
  800820:	8b 7d 08             	mov    0x8(%ebp),%edi
  800823:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800826:	85 c9                	test   %ecx,%ecx
  800828:	74 36                	je     800860 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  80082a:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800830:	75 28                	jne    80085a <memset+0x40>
  800832:	f6 c1 03             	test   $0x3,%cl
  800835:	75 23                	jne    80085a <memset+0x40>
		c &= 0xFF;
  800837:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  80083b:	89 d3                	mov    %edx,%ebx
  80083d:	c1 e3 08             	shl    $0x8,%ebx
  800840:	89 d6                	mov    %edx,%esi
  800842:	c1 e6 18             	shl    $0x18,%esi
  800845:	89 d0                	mov    %edx,%eax
  800847:	c1 e0 10             	shl    $0x10,%eax
  80084a:	09 f0                	or     %esi,%eax
  80084c:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  80084e:	89 d8                	mov    %ebx,%eax
  800850:	09 d0                	or     %edx,%eax
  800852:	c1 e9 02             	shr    $0x2,%ecx
  800855:	fc                   	cld    
  800856:	f3 ab                	rep stos %eax,%es:(%edi)
  800858:	eb 06                	jmp    800860 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  80085a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80085d:	fc                   	cld    
  80085e:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800860:	89 f8                	mov    %edi,%eax
  800862:	5b                   	pop    %ebx
  800863:	5e                   	pop    %esi
  800864:	5f                   	pop    %edi
  800865:	5d                   	pop    %ebp
  800866:	c3                   	ret    

00800867 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800867:	55                   	push   %ebp
  800868:	89 e5                	mov    %esp,%ebp
  80086a:	57                   	push   %edi
  80086b:	56                   	push   %esi
  80086c:	8b 45 08             	mov    0x8(%ebp),%eax
  80086f:	8b 75 0c             	mov    0xc(%ebp),%esi
  800872:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800875:	39 c6                	cmp    %eax,%esi
  800877:	73 33                	jae    8008ac <memmove+0x45>
  800879:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80087c:	39 d0                	cmp    %edx,%eax
  80087e:	73 2c                	jae    8008ac <memmove+0x45>
		s += n;
		d += n;
  800880:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800883:	89 d6                	mov    %edx,%esi
  800885:	09 fe                	or     %edi,%esi
  800887:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80088d:	75 13                	jne    8008a2 <memmove+0x3b>
  80088f:	f6 c1 03             	test   $0x3,%cl
  800892:	75 0e                	jne    8008a2 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800894:	83 ef 04             	sub    $0x4,%edi
  800897:	8d 72 fc             	lea    -0x4(%edx),%esi
  80089a:	c1 e9 02             	shr    $0x2,%ecx
  80089d:	fd                   	std    
  80089e:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008a0:	eb 07                	jmp    8008a9 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  8008a2:	4f                   	dec    %edi
  8008a3:	8d 72 ff             	lea    -0x1(%edx),%esi
  8008a6:	fd                   	std    
  8008a7:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  8008a9:	fc                   	cld    
  8008aa:	eb 1d                	jmp    8008c9 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  8008ac:	89 f2                	mov    %esi,%edx
  8008ae:	09 c2                	or     %eax,%edx
  8008b0:	f6 c2 03             	test   $0x3,%dl
  8008b3:	75 0f                	jne    8008c4 <memmove+0x5d>
  8008b5:	f6 c1 03             	test   $0x3,%cl
  8008b8:	75 0a                	jne    8008c4 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  8008ba:	c1 e9 02             	shr    $0x2,%ecx
  8008bd:	89 c7                	mov    %eax,%edi
  8008bf:	fc                   	cld    
  8008c0:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008c2:	eb 05                	jmp    8008c9 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8008c4:	89 c7                	mov    %eax,%edi
  8008c6:	fc                   	cld    
  8008c7:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008c9:	5e                   	pop    %esi
  8008ca:	5f                   	pop    %edi
  8008cb:	5d                   	pop    %ebp
  8008cc:	c3                   	ret    

008008cd <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008cd:	55                   	push   %ebp
  8008ce:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008d0:	ff 75 10             	pushl  0x10(%ebp)
  8008d3:	ff 75 0c             	pushl  0xc(%ebp)
  8008d6:	ff 75 08             	pushl  0x8(%ebp)
  8008d9:	e8 89 ff ff ff       	call   800867 <memmove>
}
  8008de:	c9                   	leave  
  8008df:	c3                   	ret    

008008e0 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008e0:	55                   	push   %ebp
  8008e1:	89 e5                	mov    %esp,%ebp
  8008e3:	56                   	push   %esi
  8008e4:	53                   	push   %ebx
  8008e5:	8b 45 08             	mov    0x8(%ebp),%eax
  8008e8:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008eb:	89 c6                	mov    %eax,%esi
  8008ed:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008f0:	eb 14                	jmp    800906 <memcmp+0x26>
		if (*s1 != *s2)
  8008f2:	8a 08                	mov    (%eax),%cl
  8008f4:	8a 1a                	mov    (%edx),%bl
  8008f6:	38 d9                	cmp    %bl,%cl
  8008f8:	74 0a                	je     800904 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008fa:	0f b6 c1             	movzbl %cl,%eax
  8008fd:	0f b6 db             	movzbl %bl,%ebx
  800900:	29 d8                	sub    %ebx,%eax
  800902:	eb 0b                	jmp    80090f <memcmp+0x2f>
		s1++, s2++;
  800904:	40                   	inc    %eax
  800905:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800906:	39 f0                	cmp    %esi,%eax
  800908:	75 e8                	jne    8008f2 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  80090a:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80090f:	5b                   	pop    %ebx
  800910:	5e                   	pop    %esi
  800911:	5d                   	pop    %ebp
  800912:	c3                   	ret    

00800913 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800913:	55                   	push   %ebp
  800914:	89 e5                	mov    %esp,%ebp
  800916:	53                   	push   %ebx
  800917:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  80091a:	89 c1                	mov    %eax,%ecx
  80091c:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  80091f:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800923:	eb 08                	jmp    80092d <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800925:	0f b6 10             	movzbl (%eax),%edx
  800928:	39 da                	cmp    %ebx,%edx
  80092a:	74 05                	je     800931 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  80092c:	40                   	inc    %eax
  80092d:	39 c8                	cmp    %ecx,%eax
  80092f:	72 f4                	jb     800925 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800931:	5b                   	pop    %ebx
  800932:	5d                   	pop    %ebp
  800933:	c3                   	ret    

00800934 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800934:	55                   	push   %ebp
  800935:	89 e5                	mov    %esp,%ebp
  800937:	57                   	push   %edi
  800938:	56                   	push   %esi
  800939:	53                   	push   %ebx
  80093a:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80093d:	eb 01                	jmp    800940 <strtol+0xc>
		s++;
  80093f:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800940:	8a 01                	mov    (%ecx),%al
  800942:	3c 20                	cmp    $0x20,%al
  800944:	74 f9                	je     80093f <strtol+0xb>
  800946:	3c 09                	cmp    $0x9,%al
  800948:	74 f5                	je     80093f <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  80094a:	3c 2b                	cmp    $0x2b,%al
  80094c:	75 08                	jne    800956 <strtol+0x22>
		s++;
  80094e:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  80094f:	bf 00 00 00 00       	mov    $0x0,%edi
  800954:	eb 11                	jmp    800967 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800956:	3c 2d                	cmp    $0x2d,%al
  800958:	75 08                	jne    800962 <strtol+0x2e>
		s++, neg = 1;
  80095a:	41                   	inc    %ecx
  80095b:	bf 01 00 00 00       	mov    $0x1,%edi
  800960:	eb 05                	jmp    800967 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800962:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800967:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80096b:	0f 84 87 00 00 00    	je     8009f8 <strtol+0xc4>
  800971:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800975:	75 27                	jne    80099e <strtol+0x6a>
  800977:	80 39 30             	cmpb   $0x30,(%ecx)
  80097a:	75 22                	jne    80099e <strtol+0x6a>
  80097c:	e9 88 00 00 00       	jmp    800a09 <strtol+0xd5>
		s += 2, base = 16;
  800981:	83 c1 02             	add    $0x2,%ecx
  800984:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  80098b:	eb 11                	jmp    80099e <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  80098d:	41                   	inc    %ecx
  80098e:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800995:	eb 07                	jmp    80099e <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800997:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  80099e:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  8009a3:	8a 11                	mov    (%ecx),%dl
  8009a5:	8d 5a d0             	lea    -0x30(%edx),%ebx
  8009a8:	80 fb 09             	cmp    $0x9,%bl
  8009ab:	77 08                	ja     8009b5 <strtol+0x81>
			dig = *s - '0';
  8009ad:	0f be d2             	movsbl %dl,%edx
  8009b0:	83 ea 30             	sub    $0x30,%edx
  8009b3:	eb 22                	jmp    8009d7 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  8009b5:	8d 72 9f             	lea    -0x61(%edx),%esi
  8009b8:	89 f3                	mov    %esi,%ebx
  8009ba:	80 fb 19             	cmp    $0x19,%bl
  8009bd:	77 08                	ja     8009c7 <strtol+0x93>
			dig = *s - 'a' + 10;
  8009bf:	0f be d2             	movsbl %dl,%edx
  8009c2:	83 ea 57             	sub    $0x57,%edx
  8009c5:	eb 10                	jmp    8009d7 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009c7:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009ca:	89 f3                	mov    %esi,%ebx
  8009cc:	80 fb 19             	cmp    $0x19,%bl
  8009cf:	77 14                	ja     8009e5 <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009d1:	0f be d2             	movsbl %dl,%edx
  8009d4:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009d7:	3b 55 10             	cmp    0x10(%ebp),%edx
  8009da:	7d 09                	jge    8009e5 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009dc:	41                   	inc    %ecx
  8009dd:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009e1:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009e3:	eb be                	jmp    8009a3 <strtol+0x6f>

	if (endptr)
  8009e5:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009e9:	74 05                	je     8009f0 <strtol+0xbc>
		*endptr = (char *) s;
  8009eb:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009ee:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009f0:	85 ff                	test   %edi,%edi
  8009f2:	74 21                	je     800a15 <strtol+0xe1>
  8009f4:	f7 d8                	neg    %eax
  8009f6:	eb 1d                	jmp    800a15 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009f8:	80 39 30             	cmpb   $0x30,(%ecx)
  8009fb:	75 9a                	jne    800997 <strtol+0x63>
  8009fd:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a01:	0f 84 7a ff ff ff    	je     800981 <strtol+0x4d>
  800a07:	eb 84                	jmp    80098d <strtol+0x59>
  800a09:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a0d:	0f 84 6e ff ff ff    	je     800981 <strtol+0x4d>
  800a13:	eb 89                	jmp    80099e <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a15:	5b                   	pop    %ebx
  800a16:	5e                   	pop    %esi
  800a17:	5f                   	pop    %edi
  800a18:	5d                   	pop    %ebp
  800a19:	c3                   	ret    

00800a1a <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a1a:	55                   	push   %ebp
  800a1b:	89 e5                	mov    %esp,%ebp
  800a1d:	57                   	push   %edi
  800a1e:	56                   	push   %esi
  800a1f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a20:	b8 00 00 00 00       	mov    $0x0,%eax
  800a25:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a28:	8b 55 08             	mov    0x8(%ebp),%edx
  800a2b:	89 c3                	mov    %eax,%ebx
  800a2d:	89 c7                	mov    %eax,%edi
  800a2f:	89 c6                	mov    %eax,%esi
  800a31:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a33:	5b                   	pop    %ebx
  800a34:	5e                   	pop    %esi
  800a35:	5f                   	pop    %edi
  800a36:	5d                   	pop    %ebp
  800a37:	c3                   	ret    

00800a38 <sys_cgetc>:

int
sys_cgetc(void)
{
  800a38:	55                   	push   %ebp
  800a39:	89 e5                	mov    %esp,%ebp
  800a3b:	57                   	push   %edi
  800a3c:	56                   	push   %esi
  800a3d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a3e:	ba 00 00 00 00       	mov    $0x0,%edx
  800a43:	b8 01 00 00 00       	mov    $0x1,%eax
  800a48:	89 d1                	mov    %edx,%ecx
  800a4a:	89 d3                	mov    %edx,%ebx
  800a4c:	89 d7                	mov    %edx,%edi
  800a4e:	89 d6                	mov    %edx,%esi
  800a50:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a52:	5b                   	pop    %ebx
  800a53:	5e                   	pop    %esi
  800a54:	5f                   	pop    %edi
  800a55:	5d                   	pop    %ebp
  800a56:	c3                   	ret    

00800a57 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a57:	55                   	push   %ebp
  800a58:	89 e5                	mov    %esp,%ebp
  800a5a:	57                   	push   %edi
  800a5b:	56                   	push   %esi
  800a5c:	53                   	push   %ebx
  800a5d:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a60:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a65:	b8 03 00 00 00       	mov    $0x3,%eax
  800a6a:	8b 55 08             	mov    0x8(%ebp),%edx
  800a6d:	89 cb                	mov    %ecx,%ebx
  800a6f:	89 cf                	mov    %ecx,%edi
  800a71:	89 ce                	mov    %ecx,%esi
  800a73:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a75:	85 c0                	test   %eax,%eax
  800a77:	7e 17                	jle    800a90 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a79:	83 ec 0c             	sub    $0xc,%esp
  800a7c:	50                   	push   %eax
  800a7d:	6a 03                	push   $0x3
  800a7f:	68 d4 0f 80 00       	push   $0x800fd4
  800a84:	6a 23                	push   $0x23
  800a86:	68 f1 0f 80 00       	push   $0x800ff1
  800a8b:	e8 27 00 00 00       	call   800ab7 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a90:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a93:	5b                   	pop    %ebx
  800a94:	5e                   	pop    %esi
  800a95:	5f                   	pop    %edi
  800a96:	5d                   	pop    %ebp
  800a97:	c3                   	ret    

00800a98 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a98:	55                   	push   %ebp
  800a99:	89 e5                	mov    %esp,%ebp
  800a9b:	57                   	push   %edi
  800a9c:	56                   	push   %esi
  800a9d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a9e:	ba 00 00 00 00       	mov    $0x0,%edx
  800aa3:	b8 02 00 00 00       	mov    $0x2,%eax
  800aa8:	89 d1                	mov    %edx,%ecx
  800aaa:	89 d3                	mov    %edx,%ebx
  800aac:	89 d7                	mov    %edx,%edi
  800aae:	89 d6                	mov    %edx,%esi
  800ab0:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800ab2:	5b                   	pop    %ebx
  800ab3:	5e                   	pop    %esi
  800ab4:	5f                   	pop    %edi
  800ab5:	5d                   	pop    %ebp
  800ab6:	c3                   	ret    

00800ab7 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800ab7:	55                   	push   %ebp
  800ab8:	89 e5                	mov    %esp,%ebp
  800aba:	56                   	push   %esi
  800abb:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800abc:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800abf:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800ac5:	e8 ce ff ff ff       	call   800a98 <sys_getenvid>
  800aca:	83 ec 0c             	sub    $0xc,%esp
  800acd:	ff 75 0c             	pushl  0xc(%ebp)
  800ad0:	ff 75 08             	pushl  0x8(%ebp)
  800ad3:	56                   	push   %esi
  800ad4:	50                   	push   %eax
  800ad5:	68 00 10 80 00       	push   $0x801000
  800ada:	e8 af f6 ff ff       	call   80018e <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800adf:	83 c4 18             	add    $0x18,%esp
  800ae2:	53                   	push   %ebx
  800ae3:	ff 75 10             	pushl  0x10(%ebp)
  800ae6:	e8 52 f6 ff ff       	call   80013d <vcprintf>
	cprintf("\n");
  800aeb:	c7 04 24 70 0d 80 00 	movl   $0x800d70,(%esp)
  800af2:	e8 97 f6 ff ff       	call   80018e <cprintf>
  800af7:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800afa:	cc                   	int3   
  800afb:	eb fd                	jmp    800afa <_panic+0x43>
  800afd:	66 90                	xchg   %ax,%ax
  800aff:	90                   	nop

00800b00 <__udivdi3>:
  800b00:	55                   	push   %ebp
  800b01:	57                   	push   %edi
  800b02:	56                   	push   %esi
  800b03:	53                   	push   %ebx
  800b04:	83 ec 1c             	sub    $0x1c,%esp
  800b07:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800b0b:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800b0f:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800b13:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800b17:	89 ca                	mov    %ecx,%edx
  800b19:	89 f8                	mov    %edi,%eax
  800b1b:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800b1f:	85 f6                	test   %esi,%esi
  800b21:	75 2d                	jne    800b50 <__udivdi3+0x50>
  800b23:	39 cf                	cmp    %ecx,%edi
  800b25:	77 65                	ja     800b8c <__udivdi3+0x8c>
  800b27:	89 fd                	mov    %edi,%ebp
  800b29:	85 ff                	test   %edi,%edi
  800b2b:	75 0b                	jne    800b38 <__udivdi3+0x38>
  800b2d:	b8 01 00 00 00       	mov    $0x1,%eax
  800b32:	31 d2                	xor    %edx,%edx
  800b34:	f7 f7                	div    %edi
  800b36:	89 c5                	mov    %eax,%ebp
  800b38:	31 d2                	xor    %edx,%edx
  800b3a:	89 c8                	mov    %ecx,%eax
  800b3c:	f7 f5                	div    %ebp
  800b3e:	89 c1                	mov    %eax,%ecx
  800b40:	89 d8                	mov    %ebx,%eax
  800b42:	f7 f5                	div    %ebp
  800b44:	89 cf                	mov    %ecx,%edi
  800b46:	89 fa                	mov    %edi,%edx
  800b48:	83 c4 1c             	add    $0x1c,%esp
  800b4b:	5b                   	pop    %ebx
  800b4c:	5e                   	pop    %esi
  800b4d:	5f                   	pop    %edi
  800b4e:	5d                   	pop    %ebp
  800b4f:	c3                   	ret    
  800b50:	39 ce                	cmp    %ecx,%esi
  800b52:	77 28                	ja     800b7c <__udivdi3+0x7c>
  800b54:	0f bd fe             	bsr    %esi,%edi
  800b57:	83 f7 1f             	xor    $0x1f,%edi
  800b5a:	75 40                	jne    800b9c <__udivdi3+0x9c>
  800b5c:	39 ce                	cmp    %ecx,%esi
  800b5e:	72 0a                	jb     800b6a <__udivdi3+0x6a>
  800b60:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b64:	0f 87 9e 00 00 00    	ja     800c08 <__udivdi3+0x108>
  800b6a:	b8 01 00 00 00       	mov    $0x1,%eax
  800b6f:	89 fa                	mov    %edi,%edx
  800b71:	83 c4 1c             	add    $0x1c,%esp
  800b74:	5b                   	pop    %ebx
  800b75:	5e                   	pop    %esi
  800b76:	5f                   	pop    %edi
  800b77:	5d                   	pop    %ebp
  800b78:	c3                   	ret    
  800b79:	8d 76 00             	lea    0x0(%esi),%esi
  800b7c:	31 ff                	xor    %edi,%edi
  800b7e:	31 c0                	xor    %eax,%eax
  800b80:	89 fa                	mov    %edi,%edx
  800b82:	83 c4 1c             	add    $0x1c,%esp
  800b85:	5b                   	pop    %ebx
  800b86:	5e                   	pop    %esi
  800b87:	5f                   	pop    %edi
  800b88:	5d                   	pop    %ebp
  800b89:	c3                   	ret    
  800b8a:	66 90                	xchg   %ax,%ax
  800b8c:	89 d8                	mov    %ebx,%eax
  800b8e:	f7 f7                	div    %edi
  800b90:	31 ff                	xor    %edi,%edi
  800b92:	89 fa                	mov    %edi,%edx
  800b94:	83 c4 1c             	add    $0x1c,%esp
  800b97:	5b                   	pop    %ebx
  800b98:	5e                   	pop    %esi
  800b99:	5f                   	pop    %edi
  800b9a:	5d                   	pop    %ebp
  800b9b:	c3                   	ret    
  800b9c:	bd 20 00 00 00       	mov    $0x20,%ebp
  800ba1:	89 eb                	mov    %ebp,%ebx
  800ba3:	29 fb                	sub    %edi,%ebx
  800ba5:	89 f9                	mov    %edi,%ecx
  800ba7:	d3 e6                	shl    %cl,%esi
  800ba9:	89 c5                	mov    %eax,%ebp
  800bab:	88 d9                	mov    %bl,%cl
  800bad:	d3 ed                	shr    %cl,%ebp
  800baf:	89 e9                	mov    %ebp,%ecx
  800bb1:	09 f1                	or     %esi,%ecx
  800bb3:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800bb7:	89 f9                	mov    %edi,%ecx
  800bb9:	d3 e0                	shl    %cl,%eax
  800bbb:	89 c5                	mov    %eax,%ebp
  800bbd:	89 d6                	mov    %edx,%esi
  800bbf:	88 d9                	mov    %bl,%cl
  800bc1:	d3 ee                	shr    %cl,%esi
  800bc3:	89 f9                	mov    %edi,%ecx
  800bc5:	d3 e2                	shl    %cl,%edx
  800bc7:	8b 44 24 08          	mov    0x8(%esp),%eax
  800bcb:	88 d9                	mov    %bl,%cl
  800bcd:	d3 e8                	shr    %cl,%eax
  800bcf:	09 c2                	or     %eax,%edx
  800bd1:	89 d0                	mov    %edx,%eax
  800bd3:	89 f2                	mov    %esi,%edx
  800bd5:	f7 74 24 0c          	divl   0xc(%esp)
  800bd9:	89 d6                	mov    %edx,%esi
  800bdb:	89 c3                	mov    %eax,%ebx
  800bdd:	f7 e5                	mul    %ebp
  800bdf:	39 d6                	cmp    %edx,%esi
  800be1:	72 19                	jb     800bfc <__udivdi3+0xfc>
  800be3:	74 0b                	je     800bf0 <__udivdi3+0xf0>
  800be5:	89 d8                	mov    %ebx,%eax
  800be7:	31 ff                	xor    %edi,%edi
  800be9:	e9 58 ff ff ff       	jmp    800b46 <__udivdi3+0x46>
  800bee:	66 90                	xchg   %ax,%ax
  800bf0:	8b 54 24 08          	mov    0x8(%esp),%edx
  800bf4:	89 f9                	mov    %edi,%ecx
  800bf6:	d3 e2                	shl    %cl,%edx
  800bf8:	39 c2                	cmp    %eax,%edx
  800bfa:	73 e9                	jae    800be5 <__udivdi3+0xe5>
  800bfc:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800bff:	31 ff                	xor    %edi,%edi
  800c01:	e9 40 ff ff ff       	jmp    800b46 <__udivdi3+0x46>
  800c06:	66 90                	xchg   %ax,%ax
  800c08:	31 c0                	xor    %eax,%eax
  800c0a:	e9 37 ff ff ff       	jmp    800b46 <__udivdi3+0x46>
  800c0f:	90                   	nop

00800c10 <__umoddi3>:
  800c10:	55                   	push   %ebp
  800c11:	57                   	push   %edi
  800c12:	56                   	push   %esi
  800c13:	53                   	push   %ebx
  800c14:	83 ec 1c             	sub    $0x1c,%esp
  800c17:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800c1b:	8b 74 24 34          	mov    0x34(%esp),%esi
  800c1f:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800c23:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800c27:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800c2b:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800c2f:	89 f3                	mov    %esi,%ebx
  800c31:	89 fa                	mov    %edi,%edx
  800c33:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800c37:	89 34 24             	mov    %esi,(%esp)
  800c3a:	85 c0                	test   %eax,%eax
  800c3c:	75 1a                	jne    800c58 <__umoddi3+0x48>
  800c3e:	39 f7                	cmp    %esi,%edi
  800c40:	0f 86 a2 00 00 00    	jbe    800ce8 <__umoddi3+0xd8>
  800c46:	89 c8                	mov    %ecx,%eax
  800c48:	89 f2                	mov    %esi,%edx
  800c4a:	f7 f7                	div    %edi
  800c4c:	89 d0                	mov    %edx,%eax
  800c4e:	31 d2                	xor    %edx,%edx
  800c50:	83 c4 1c             	add    $0x1c,%esp
  800c53:	5b                   	pop    %ebx
  800c54:	5e                   	pop    %esi
  800c55:	5f                   	pop    %edi
  800c56:	5d                   	pop    %ebp
  800c57:	c3                   	ret    
  800c58:	39 f0                	cmp    %esi,%eax
  800c5a:	0f 87 ac 00 00 00    	ja     800d0c <__umoddi3+0xfc>
  800c60:	0f bd e8             	bsr    %eax,%ebp
  800c63:	83 f5 1f             	xor    $0x1f,%ebp
  800c66:	0f 84 ac 00 00 00    	je     800d18 <__umoddi3+0x108>
  800c6c:	bf 20 00 00 00       	mov    $0x20,%edi
  800c71:	29 ef                	sub    %ebp,%edi
  800c73:	89 fe                	mov    %edi,%esi
  800c75:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c79:	89 e9                	mov    %ebp,%ecx
  800c7b:	d3 e0                	shl    %cl,%eax
  800c7d:	89 d7                	mov    %edx,%edi
  800c7f:	89 f1                	mov    %esi,%ecx
  800c81:	d3 ef                	shr    %cl,%edi
  800c83:	09 c7                	or     %eax,%edi
  800c85:	89 e9                	mov    %ebp,%ecx
  800c87:	d3 e2                	shl    %cl,%edx
  800c89:	89 14 24             	mov    %edx,(%esp)
  800c8c:	89 d8                	mov    %ebx,%eax
  800c8e:	d3 e0                	shl    %cl,%eax
  800c90:	89 c2                	mov    %eax,%edx
  800c92:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c96:	d3 e0                	shl    %cl,%eax
  800c98:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c9c:	8b 44 24 08          	mov    0x8(%esp),%eax
  800ca0:	89 f1                	mov    %esi,%ecx
  800ca2:	d3 e8                	shr    %cl,%eax
  800ca4:	09 d0                	or     %edx,%eax
  800ca6:	d3 eb                	shr    %cl,%ebx
  800ca8:	89 da                	mov    %ebx,%edx
  800caa:	f7 f7                	div    %edi
  800cac:	89 d3                	mov    %edx,%ebx
  800cae:	f7 24 24             	mull   (%esp)
  800cb1:	89 c6                	mov    %eax,%esi
  800cb3:	89 d1                	mov    %edx,%ecx
  800cb5:	39 d3                	cmp    %edx,%ebx
  800cb7:	0f 82 87 00 00 00    	jb     800d44 <__umoddi3+0x134>
  800cbd:	0f 84 91 00 00 00    	je     800d54 <__umoddi3+0x144>
  800cc3:	8b 54 24 04          	mov    0x4(%esp),%edx
  800cc7:	29 f2                	sub    %esi,%edx
  800cc9:	19 cb                	sbb    %ecx,%ebx
  800ccb:	89 d8                	mov    %ebx,%eax
  800ccd:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800cd1:	d3 e0                	shl    %cl,%eax
  800cd3:	89 e9                	mov    %ebp,%ecx
  800cd5:	d3 ea                	shr    %cl,%edx
  800cd7:	09 d0                	or     %edx,%eax
  800cd9:	89 e9                	mov    %ebp,%ecx
  800cdb:	d3 eb                	shr    %cl,%ebx
  800cdd:	89 da                	mov    %ebx,%edx
  800cdf:	83 c4 1c             	add    $0x1c,%esp
  800ce2:	5b                   	pop    %ebx
  800ce3:	5e                   	pop    %esi
  800ce4:	5f                   	pop    %edi
  800ce5:	5d                   	pop    %ebp
  800ce6:	c3                   	ret    
  800ce7:	90                   	nop
  800ce8:	89 fd                	mov    %edi,%ebp
  800cea:	85 ff                	test   %edi,%edi
  800cec:	75 0b                	jne    800cf9 <__umoddi3+0xe9>
  800cee:	b8 01 00 00 00       	mov    $0x1,%eax
  800cf3:	31 d2                	xor    %edx,%edx
  800cf5:	f7 f7                	div    %edi
  800cf7:	89 c5                	mov    %eax,%ebp
  800cf9:	89 f0                	mov    %esi,%eax
  800cfb:	31 d2                	xor    %edx,%edx
  800cfd:	f7 f5                	div    %ebp
  800cff:	89 c8                	mov    %ecx,%eax
  800d01:	f7 f5                	div    %ebp
  800d03:	89 d0                	mov    %edx,%eax
  800d05:	e9 44 ff ff ff       	jmp    800c4e <__umoddi3+0x3e>
  800d0a:	66 90                	xchg   %ax,%ax
  800d0c:	89 c8                	mov    %ecx,%eax
  800d0e:	89 f2                	mov    %esi,%edx
  800d10:	83 c4 1c             	add    $0x1c,%esp
  800d13:	5b                   	pop    %ebx
  800d14:	5e                   	pop    %esi
  800d15:	5f                   	pop    %edi
  800d16:	5d                   	pop    %ebp
  800d17:	c3                   	ret    
  800d18:	3b 04 24             	cmp    (%esp),%eax
  800d1b:	72 06                	jb     800d23 <__umoddi3+0x113>
  800d1d:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800d21:	77 0f                	ja     800d32 <__umoddi3+0x122>
  800d23:	89 f2                	mov    %esi,%edx
  800d25:	29 f9                	sub    %edi,%ecx
  800d27:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800d2b:	89 14 24             	mov    %edx,(%esp)
  800d2e:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800d32:	8b 44 24 04          	mov    0x4(%esp),%eax
  800d36:	8b 14 24             	mov    (%esp),%edx
  800d39:	83 c4 1c             	add    $0x1c,%esp
  800d3c:	5b                   	pop    %ebx
  800d3d:	5e                   	pop    %esi
  800d3e:	5f                   	pop    %edi
  800d3f:	5d                   	pop    %ebp
  800d40:	c3                   	ret    
  800d41:	8d 76 00             	lea    0x0(%esi),%esi
  800d44:	2b 04 24             	sub    (%esp),%eax
  800d47:	19 fa                	sbb    %edi,%edx
  800d49:	89 d1                	mov    %edx,%ecx
  800d4b:	89 c6                	mov    %eax,%esi
  800d4d:	e9 71 ff ff ff       	jmp    800cc3 <__umoddi3+0xb3>
  800d52:	66 90                	xchg   %ax,%ax
  800d54:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d58:	72 ea                	jb     800d44 <__umoddi3+0x134>
  800d5a:	89 d9                	mov    %ebx,%ecx
  800d5c:	e9 62 ff ff ff       	jmp    800cc3 <__umoddi3+0xb3>
