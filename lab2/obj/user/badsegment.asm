
obj/user/badsegment:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 0d 00 00 00       	call   80003e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	// Try to load the kernel's TSS selector into the DS register.
	asm volatile("movw $0x28,%ax; movw %ax,%ds");
  800036:	66 b8 28 00          	mov    $0x28,%ax
  80003a:	8e d8                	mov    %eax,%ds
}
  80003c:	5d                   	pop    %ebp
  80003d:	c3                   	ret    

0080003e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80003e:	55                   	push   %ebp
  80003f:	89 e5                	mov    %esp,%ebp
  800041:	57                   	push   %edi
  800042:	56                   	push   %esi
  800043:	53                   	push   %ebx
  800044:	83 ec 0c             	sub    $0xc,%esp
  800047:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80004a:	8b 75 0c             	mov    0xc(%ebp),%esi
	// set thisenv to point at our Env structure in envs[].
	// LAB 3: Your code here.
	//thisenv = 0;
	int32_t env_Index1 = (int32_t)sys_getenvid();
  80004d:	e8 24 0a 00 00       	call   800a76 <sys_getenvid>
  800052:	89 c7                	mov    %eax,%edi
	cprintf("printing env_Index1: %d\n", env_Index1);
  800054:	83 ec 08             	sub    $0x8,%esp
  800057:	50                   	push   %eax
  800058:	68 40 0d 80 00       	push   $0x800d40
  80005d:	e8 0a 01 00 00       	call   80016c <cprintf>
	int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	cprintf("printing env_Index2: %d\n", env_Index2);
  800062:	83 c4 08             	add    $0x8,%esp
  800065:	81 e7 ff 03 00 00    	and    $0x3ff,%edi
  80006b:	57                   	push   %edi
  80006c:	68 59 0d 80 00       	push   $0x800d59
  800071:	e8 f6 00 00 00       	call   80016c <cprintf>

	thisenv = &envs[ENVX(sys_getenvid())];
  800076:	e8 fb 09 00 00       	call   800a76 <sys_getenvid>
  80007b:	25 ff 03 00 00       	and    $0x3ff,%eax
  800080:	8d 14 00             	lea    (%eax,%eax,1),%edx
  800083:	01 d0                	add    %edx,%eax
  800085:	c1 e0 05             	shl    $0x5,%eax
  800088:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80008d:	a3 04 10 80 00       	mov    %eax,0x801004
	cprintf("Addrs of thisenv (envs[0]) is: %p\n", thisenv);
  800092:	83 c4 08             	add    $0x8,%esp
  800095:	50                   	push   %eax
  800096:	68 7c 0d 80 00       	push   $0x800d7c
  80009b:	e8 cc 00 00 00       	call   80016c <cprintf>
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000a0:	83 c4 10             	add    $0x10,%esp
  8000a3:	85 db                	test   %ebx,%ebx
  8000a5:	7e 07                	jle    8000ae <libmain+0x70>
		binaryname = argv[0];
  8000a7:	8b 06                	mov    (%esi),%eax
  8000a9:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  8000ae:	83 ec 08             	sub    $0x8,%esp
  8000b1:	56                   	push   %esi
  8000b2:	53                   	push   %ebx
  8000b3:	e8 7b ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000b8:	e8 0b 00 00 00       	call   8000c8 <exit>
}
  8000bd:	83 c4 10             	add    $0x10,%esp
  8000c0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8000c3:	5b                   	pop    %ebx
  8000c4:	5e                   	pop    %esi
  8000c5:	5f                   	pop    %edi
  8000c6:	5d                   	pop    %ebp
  8000c7:	c3                   	ret    

008000c8 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000c8:	55                   	push   %ebp
  8000c9:	89 e5                	mov    %esp,%ebp
  8000cb:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000ce:	6a 00                	push   $0x0
  8000d0:	e8 60 09 00 00       	call   800a35 <sys_env_destroy>
}
  8000d5:	83 c4 10             	add    $0x10,%esp
  8000d8:	c9                   	leave  
  8000d9:	c3                   	ret    

008000da <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000da:	55                   	push   %ebp
  8000db:	89 e5                	mov    %esp,%ebp
  8000dd:	53                   	push   %ebx
  8000de:	83 ec 04             	sub    $0x4,%esp
  8000e1:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000e4:	8b 13                	mov    (%ebx),%edx
  8000e6:	8d 42 01             	lea    0x1(%edx),%eax
  8000e9:	89 03                	mov    %eax,(%ebx)
  8000eb:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000ee:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8000f2:	3d ff 00 00 00       	cmp    $0xff,%eax
  8000f7:	75 1a                	jne    800113 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8000f9:	83 ec 08             	sub    $0x8,%esp
  8000fc:	68 ff 00 00 00       	push   $0xff
  800101:	8d 43 08             	lea    0x8(%ebx),%eax
  800104:	50                   	push   %eax
  800105:	e8 ee 08 00 00       	call   8009f8 <sys_cputs>
		b->idx = 0;
  80010a:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800110:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800113:	ff 43 04             	incl   0x4(%ebx)
}
  800116:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800119:	c9                   	leave  
  80011a:	c3                   	ret    

0080011b <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80011b:	55                   	push   %ebp
  80011c:	89 e5                	mov    %esp,%ebp
  80011e:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800124:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80012b:	00 00 00 
	b.cnt = 0;
  80012e:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800135:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800138:	ff 75 0c             	pushl  0xc(%ebp)
  80013b:	ff 75 08             	pushl  0x8(%ebp)
  80013e:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800144:	50                   	push   %eax
  800145:	68 da 00 80 00       	push   $0x8000da
  80014a:	e8 51 01 00 00       	call   8002a0 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80014f:	83 c4 08             	add    $0x8,%esp
  800152:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800158:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80015e:	50                   	push   %eax
  80015f:	e8 94 08 00 00       	call   8009f8 <sys_cputs>

	return b.cnt;
}
  800164:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80016a:	c9                   	leave  
  80016b:	c3                   	ret    

0080016c <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80016c:	55                   	push   %ebp
  80016d:	89 e5                	mov    %esp,%ebp
  80016f:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800172:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800175:	50                   	push   %eax
  800176:	ff 75 08             	pushl  0x8(%ebp)
  800179:	e8 9d ff ff ff       	call   80011b <vcprintf>
	va_end(ap);

	return cnt;
}
  80017e:	c9                   	leave  
  80017f:	c3                   	ret    

00800180 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800180:	55                   	push   %ebp
  800181:	89 e5                	mov    %esp,%ebp
  800183:	57                   	push   %edi
  800184:	56                   	push   %esi
  800185:	53                   	push   %ebx
  800186:	83 ec 1c             	sub    $0x1c,%esp
  800189:	89 c7                	mov    %eax,%edi
  80018b:	89 d6                	mov    %edx,%esi
  80018d:	8b 45 08             	mov    0x8(%ebp),%eax
  800190:	8b 55 0c             	mov    0xc(%ebp),%edx
  800193:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800196:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800199:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80019c:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001a1:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001a4:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001a7:	39 d3                	cmp    %edx,%ebx
  8001a9:	72 05                	jb     8001b0 <printnum+0x30>
  8001ab:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001ae:	77 45                	ja     8001f5 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001b0:	83 ec 0c             	sub    $0xc,%esp
  8001b3:	ff 75 18             	pushl  0x18(%ebp)
  8001b6:	8b 45 14             	mov    0x14(%ebp),%eax
  8001b9:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001bc:	53                   	push   %ebx
  8001bd:	ff 75 10             	pushl  0x10(%ebp)
  8001c0:	83 ec 08             	sub    $0x8,%esp
  8001c3:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001c6:	ff 75 e0             	pushl  -0x20(%ebp)
  8001c9:	ff 75 dc             	pushl  -0x24(%ebp)
  8001cc:	ff 75 d8             	pushl  -0x28(%ebp)
  8001cf:	e8 08 09 00 00       	call   800adc <__udivdi3>
  8001d4:	83 c4 18             	add    $0x18,%esp
  8001d7:	52                   	push   %edx
  8001d8:	50                   	push   %eax
  8001d9:	89 f2                	mov    %esi,%edx
  8001db:	89 f8                	mov    %edi,%eax
  8001dd:	e8 9e ff ff ff       	call   800180 <printnum>
  8001e2:	83 c4 20             	add    $0x20,%esp
  8001e5:	eb 16                	jmp    8001fd <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001e7:	83 ec 08             	sub    $0x8,%esp
  8001ea:	56                   	push   %esi
  8001eb:	ff 75 18             	pushl  0x18(%ebp)
  8001ee:	ff d7                	call   *%edi
  8001f0:	83 c4 10             	add    $0x10,%esp
  8001f3:	eb 03                	jmp    8001f8 <printnum+0x78>
  8001f5:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8001f8:	4b                   	dec    %ebx
  8001f9:	85 db                	test   %ebx,%ebx
  8001fb:	7f ea                	jg     8001e7 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8001fd:	83 ec 08             	sub    $0x8,%esp
  800200:	56                   	push   %esi
  800201:	83 ec 04             	sub    $0x4,%esp
  800204:	ff 75 e4             	pushl  -0x1c(%ebp)
  800207:	ff 75 e0             	pushl  -0x20(%ebp)
  80020a:	ff 75 dc             	pushl  -0x24(%ebp)
  80020d:	ff 75 d8             	pushl  -0x28(%ebp)
  800210:	e8 d7 09 00 00       	call   800bec <__umoddi3>
  800215:	83 c4 14             	add    $0x14,%esp
  800218:	0f be 80 9f 0d 80 00 	movsbl 0x800d9f(%eax),%eax
  80021f:	50                   	push   %eax
  800220:	ff d7                	call   *%edi
}
  800222:	83 c4 10             	add    $0x10,%esp
  800225:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800228:	5b                   	pop    %ebx
  800229:	5e                   	pop    %esi
  80022a:	5f                   	pop    %edi
  80022b:	5d                   	pop    %ebp
  80022c:	c3                   	ret    

0080022d <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80022d:	55                   	push   %ebp
  80022e:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800230:	83 fa 01             	cmp    $0x1,%edx
  800233:	7e 0e                	jle    800243 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800235:	8b 10                	mov    (%eax),%edx
  800237:	8d 4a 08             	lea    0x8(%edx),%ecx
  80023a:	89 08                	mov    %ecx,(%eax)
  80023c:	8b 02                	mov    (%edx),%eax
  80023e:	8b 52 04             	mov    0x4(%edx),%edx
  800241:	eb 22                	jmp    800265 <getuint+0x38>
	else if (lflag)
  800243:	85 d2                	test   %edx,%edx
  800245:	74 10                	je     800257 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800247:	8b 10                	mov    (%eax),%edx
  800249:	8d 4a 04             	lea    0x4(%edx),%ecx
  80024c:	89 08                	mov    %ecx,(%eax)
  80024e:	8b 02                	mov    (%edx),%eax
  800250:	ba 00 00 00 00       	mov    $0x0,%edx
  800255:	eb 0e                	jmp    800265 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800257:	8b 10                	mov    (%eax),%edx
  800259:	8d 4a 04             	lea    0x4(%edx),%ecx
  80025c:	89 08                	mov    %ecx,(%eax)
  80025e:	8b 02                	mov    (%edx),%eax
  800260:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800265:	5d                   	pop    %ebp
  800266:	c3                   	ret    

00800267 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800267:	55                   	push   %ebp
  800268:	89 e5                	mov    %esp,%ebp
  80026a:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80026d:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800270:	8b 10                	mov    (%eax),%edx
  800272:	3b 50 04             	cmp    0x4(%eax),%edx
  800275:	73 0a                	jae    800281 <sprintputch+0x1a>
		*b->buf++ = ch;
  800277:	8d 4a 01             	lea    0x1(%edx),%ecx
  80027a:	89 08                	mov    %ecx,(%eax)
  80027c:	8b 45 08             	mov    0x8(%ebp),%eax
  80027f:	88 02                	mov    %al,(%edx)
}
  800281:	5d                   	pop    %ebp
  800282:	c3                   	ret    

00800283 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800283:	55                   	push   %ebp
  800284:	89 e5                	mov    %esp,%ebp
  800286:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800289:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80028c:	50                   	push   %eax
  80028d:	ff 75 10             	pushl  0x10(%ebp)
  800290:	ff 75 0c             	pushl  0xc(%ebp)
  800293:	ff 75 08             	pushl  0x8(%ebp)
  800296:	e8 05 00 00 00       	call   8002a0 <vprintfmt>
	va_end(ap);
}
  80029b:	83 c4 10             	add    $0x10,%esp
  80029e:	c9                   	leave  
  80029f:	c3                   	ret    

008002a0 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8002a0:	55                   	push   %ebp
  8002a1:	89 e5                	mov    %esp,%ebp
  8002a3:	57                   	push   %edi
  8002a4:	56                   	push   %esi
  8002a5:	53                   	push   %ebx
  8002a6:	83 ec 2c             	sub    $0x2c,%esp
  8002a9:	8b 75 08             	mov    0x8(%ebp),%esi
  8002ac:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002af:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002b2:	eb 12                	jmp    8002c6 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002b4:	85 c0                	test   %eax,%eax
  8002b6:	0f 84 68 03 00 00    	je     800624 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002bc:	83 ec 08             	sub    $0x8,%esp
  8002bf:	53                   	push   %ebx
  8002c0:	50                   	push   %eax
  8002c1:	ff d6                	call   *%esi
  8002c3:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002c6:	47                   	inc    %edi
  8002c7:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002cb:	83 f8 25             	cmp    $0x25,%eax
  8002ce:	75 e4                	jne    8002b4 <vprintfmt+0x14>
  8002d0:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002d4:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002db:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002e2:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002e9:	ba 00 00 00 00       	mov    $0x0,%edx
  8002ee:	eb 07                	jmp    8002f7 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002f0:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8002f3:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002f7:	8d 47 01             	lea    0x1(%edi),%eax
  8002fa:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8002fd:	0f b6 0f             	movzbl (%edi),%ecx
  800300:	8a 07                	mov    (%edi),%al
  800302:	83 e8 23             	sub    $0x23,%eax
  800305:	3c 55                	cmp    $0x55,%al
  800307:	0f 87 fe 02 00 00    	ja     80060b <vprintfmt+0x36b>
  80030d:	0f b6 c0             	movzbl %al,%eax
  800310:	ff 24 85 2c 0e 80 00 	jmp    *0x800e2c(,%eax,4)
  800317:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80031a:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80031e:	eb d7                	jmp    8002f7 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800320:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800323:	b8 00 00 00 00       	mov    $0x0,%eax
  800328:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80032b:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80032e:	01 c0                	add    %eax,%eax
  800330:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800334:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800337:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80033a:	83 fa 09             	cmp    $0x9,%edx
  80033d:	77 34                	ja     800373 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80033f:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800340:	eb e9                	jmp    80032b <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800342:	8b 45 14             	mov    0x14(%ebp),%eax
  800345:	8d 48 04             	lea    0x4(%eax),%ecx
  800348:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80034b:	8b 00                	mov    (%eax),%eax
  80034d:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800350:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800353:	eb 24                	jmp    800379 <vprintfmt+0xd9>
  800355:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800359:	79 07                	jns    800362 <vprintfmt+0xc2>
  80035b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800362:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800365:	eb 90                	jmp    8002f7 <vprintfmt+0x57>
  800367:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80036a:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800371:	eb 84                	jmp    8002f7 <vprintfmt+0x57>
  800373:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800376:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800379:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80037d:	0f 89 74 ff ff ff    	jns    8002f7 <vprintfmt+0x57>
				width = precision, precision = -1;
  800383:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800386:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800389:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800390:	e9 62 ff ff ff       	jmp    8002f7 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800395:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800396:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800399:	e9 59 ff ff ff       	jmp    8002f7 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80039e:	8b 45 14             	mov    0x14(%ebp),%eax
  8003a1:	8d 50 04             	lea    0x4(%eax),%edx
  8003a4:	89 55 14             	mov    %edx,0x14(%ebp)
  8003a7:	83 ec 08             	sub    $0x8,%esp
  8003aa:	53                   	push   %ebx
  8003ab:	ff 30                	pushl  (%eax)
  8003ad:	ff d6                	call   *%esi
			break;
  8003af:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003b2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003b5:	e9 0c ff ff ff       	jmp    8002c6 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003ba:	8b 45 14             	mov    0x14(%ebp),%eax
  8003bd:	8d 50 04             	lea    0x4(%eax),%edx
  8003c0:	89 55 14             	mov    %edx,0x14(%ebp)
  8003c3:	8b 00                	mov    (%eax),%eax
  8003c5:	85 c0                	test   %eax,%eax
  8003c7:	79 02                	jns    8003cb <vprintfmt+0x12b>
  8003c9:	f7 d8                	neg    %eax
  8003cb:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003cd:	83 f8 06             	cmp    $0x6,%eax
  8003d0:	7f 0b                	jg     8003dd <vprintfmt+0x13d>
  8003d2:	8b 04 85 84 0f 80 00 	mov    0x800f84(,%eax,4),%eax
  8003d9:	85 c0                	test   %eax,%eax
  8003db:	75 18                	jne    8003f5 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003dd:	52                   	push   %edx
  8003de:	68 b7 0d 80 00       	push   $0x800db7
  8003e3:	53                   	push   %ebx
  8003e4:	56                   	push   %esi
  8003e5:	e8 99 fe ff ff       	call   800283 <printfmt>
  8003ea:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ed:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8003f0:	e9 d1 fe ff ff       	jmp    8002c6 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8003f5:	50                   	push   %eax
  8003f6:	68 c0 0d 80 00       	push   $0x800dc0
  8003fb:	53                   	push   %ebx
  8003fc:	56                   	push   %esi
  8003fd:	e8 81 fe ff ff       	call   800283 <printfmt>
  800402:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800405:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800408:	e9 b9 fe ff ff       	jmp    8002c6 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80040d:	8b 45 14             	mov    0x14(%ebp),%eax
  800410:	8d 50 04             	lea    0x4(%eax),%edx
  800413:	89 55 14             	mov    %edx,0x14(%ebp)
  800416:	8b 38                	mov    (%eax),%edi
  800418:	85 ff                	test   %edi,%edi
  80041a:	75 05                	jne    800421 <vprintfmt+0x181>
				p = "(null)";
  80041c:	bf b0 0d 80 00       	mov    $0x800db0,%edi
			if (width > 0 && padc != '-')
  800421:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800425:	0f 8e 90 00 00 00    	jle    8004bb <vprintfmt+0x21b>
  80042b:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80042f:	0f 84 8e 00 00 00    	je     8004c3 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800435:	83 ec 08             	sub    $0x8,%esp
  800438:	ff 75 d0             	pushl  -0x30(%ebp)
  80043b:	57                   	push   %edi
  80043c:	e8 70 02 00 00       	call   8006b1 <strnlen>
  800441:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800444:	29 c1                	sub    %eax,%ecx
  800446:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800449:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80044c:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800450:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800453:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800456:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800458:	eb 0d                	jmp    800467 <vprintfmt+0x1c7>
					putch(padc, putdat);
  80045a:	83 ec 08             	sub    $0x8,%esp
  80045d:	53                   	push   %ebx
  80045e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800461:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800463:	4f                   	dec    %edi
  800464:	83 c4 10             	add    $0x10,%esp
  800467:	85 ff                	test   %edi,%edi
  800469:	7f ef                	jg     80045a <vprintfmt+0x1ba>
  80046b:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80046e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800471:	89 c8                	mov    %ecx,%eax
  800473:	85 c9                	test   %ecx,%ecx
  800475:	79 05                	jns    80047c <vprintfmt+0x1dc>
  800477:	b8 00 00 00 00       	mov    $0x0,%eax
  80047c:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80047f:	29 c1                	sub    %eax,%ecx
  800481:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800484:	89 75 08             	mov    %esi,0x8(%ebp)
  800487:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80048a:	eb 3d                	jmp    8004c9 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80048c:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800490:	74 19                	je     8004ab <vprintfmt+0x20b>
  800492:	0f be c0             	movsbl %al,%eax
  800495:	83 e8 20             	sub    $0x20,%eax
  800498:	83 f8 5e             	cmp    $0x5e,%eax
  80049b:	76 0e                	jbe    8004ab <vprintfmt+0x20b>
					putch('?', putdat);
  80049d:	83 ec 08             	sub    $0x8,%esp
  8004a0:	53                   	push   %ebx
  8004a1:	6a 3f                	push   $0x3f
  8004a3:	ff 55 08             	call   *0x8(%ebp)
  8004a6:	83 c4 10             	add    $0x10,%esp
  8004a9:	eb 0b                	jmp    8004b6 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004ab:	83 ec 08             	sub    $0x8,%esp
  8004ae:	53                   	push   %ebx
  8004af:	52                   	push   %edx
  8004b0:	ff 55 08             	call   *0x8(%ebp)
  8004b3:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004b6:	ff 4d e4             	decl   -0x1c(%ebp)
  8004b9:	eb 0e                	jmp    8004c9 <vprintfmt+0x229>
  8004bb:	89 75 08             	mov    %esi,0x8(%ebp)
  8004be:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004c1:	eb 06                	jmp    8004c9 <vprintfmt+0x229>
  8004c3:	89 75 08             	mov    %esi,0x8(%ebp)
  8004c6:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004c9:	47                   	inc    %edi
  8004ca:	8a 47 ff             	mov    -0x1(%edi),%al
  8004cd:	0f be d0             	movsbl %al,%edx
  8004d0:	85 d2                	test   %edx,%edx
  8004d2:	74 1d                	je     8004f1 <vprintfmt+0x251>
  8004d4:	85 f6                	test   %esi,%esi
  8004d6:	78 b4                	js     80048c <vprintfmt+0x1ec>
  8004d8:	4e                   	dec    %esi
  8004d9:	79 b1                	jns    80048c <vprintfmt+0x1ec>
  8004db:	8b 75 08             	mov    0x8(%ebp),%esi
  8004de:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004e1:	eb 14                	jmp    8004f7 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004e3:	83 ec 08             	sub    $0x8,%esp
  8004e6:	53                   	push   %ebx
  8004e7:	6a 20                	push   $0x20
  8004e9:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004eb:	4f                   	dec    %edi
  8004ec:	83 c4 10             	add    $0x10,%esp
  8004ef:	eb 06                	jmp    8004f7 <vprintfmt+0x257>
  8004f1:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004f4:	8b 75 08             	mov    0x8(%ebp),%esi
  8004f7:	85 ff                	test   %edi,%edi
  8004f9:	7f e8                	jg     8004e3 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004fb:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004fe:	e9 c3 fd ff ff       	jmp    8002c6 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800503:	83 fa 01             	cmp    $0x1,%edx
  800506:	7e 16                	jle    80051e <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800508:	8b 45 14             	mov    0x14(%ebp),%eax
  80050b:	8d 50 08             	lea    0x8(%eax),%edx
  80050e:	89 55 14             	mov    %edx,0x14(%ebp)
  800511:	8b 50 04             	mov    0x4(%eax),%edx
  800514:	8b 00                	mov    (%eax),%eax
  800516:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800519:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80051c:	eb 32                	jmp    800550 <vprintfmt+0x2b0>
	else if (lflag)
  80051e:	85 d2                	test   %edx,%edx
  800520:	74 18                	je     80053a <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800522:	8b 45 14             	mov    0x14(%ebp),%eax
  800525:	8d 50 04             	lea    0x4(%eax),%edx
  800528:	89 55 14             	mov    %edx,0x14(%ebp)
  80052b:	8b 00                	mov    (%eax),%eax
  80052d:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800530:	89 c1                	mov    %eax,%ecx
  800532:	c1 f9 1f             	sar    $0x1f,%ecx
  800535:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800538:	eb 16                	jmp    800550 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80053a:	8b 45 14             	mov    0x14(%ebp),%eax
  80053d:	8d 50 04             	lea    0x4(%eax),%edx
  800540:	89 55 14             	mov    %edx,0x14(%ebp)
  800543:	8b 00                	mov    (%eax),%eax
  800545:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800548:	89 c1                	mov    %eax,%ecx
  80054a:	c1 f9 1f             	sar    $0x1f,%ecx
  80054d:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800550:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800553:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800556:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80055a:	79 76                	jns    8005d2 <vprintfmt+0x332>
				putch('-', putdat);
  80055c:	83 ec 08             	sub    $0x8,%esp
  80055f:	53                   	push   %ebx
  800560:	6a 2d                	push   $0x2d
  800562:	ff d6                	call   *%esi
				num = -(long long) num;
  800564:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800567:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80056a:	f7 d8                	neg    %eax
  80056c:	83 d2 00             	adc    $0x0,%edx
  80056f:	f7 da                	neg    %edx
  800571:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800574:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800579:	eb 5c                	jmp    8005d7 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80057b:	8d 45 14             	lea    0x14(%ebp),%eax
  80057e:	e8 aa fc ff ff       	call   80022d <getuint>
			base = 10;
  800583:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800588:	eb 4d                	jmp    8005d7 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80058a:	8d 45 14             	lea    0x14(%ebp),%eax
  80058d:	e8 9b fc ff ff       	call   80022d <getuint>
			base = 8;
  800592:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800597:	eb 3e                	jmp    8005d7 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800599:	83 ec 08             	sub    $0x8,%esp
  80059c:	53                   	push   %ebx
  80059d:	6a 30                	push   $0x30
  80059f:	ff d6                	call   *%esi
			putch('x', putdat);
  8005a1:	83 c4 08             	add    $0x8,%esp
  8005a4:	53                   	push   %ebx
  8005a5:	6a 78                	push   $0x78
  8005a7:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005a9:	8b 45 14             	mov    0x14(%ebp),%eax
  8005ac:	8d 50 04             	lea    0x4(%eax),%edx
  8005af:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005b2:	8b 00                	mov    (%eax),%eax
  8005b4:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005b9:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005bc:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005c1:	eb 14                	jmp    8005d7 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005c3:	8d 45 14             	lea    0x14(%ebp),%eax
  8005c6:	e8 62 fc ff ff       	call   80022d <getuint>
			base = 16;
  8005cb:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005d0:	eb 05                	jmp    8005d7 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005d2:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005d7:	83 ec 0c             	sub    $0xc,%esp
  8005da:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005de:	57                   	push   %edi
  8005df:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005e2:	51                   	push   %ecx
  8005e3:	52                   	push   %edx
  8005e4:	50                   	push   %eax
  8005e5:	89 da                	mov    %ebx,%edx
  8005e7:	89 f0                	mov    %esi,%eax
  8005e9:	e8 92 fb ff ff       	call   800180 <printnum>
			break;
  8005ee:	83 c4 20             	add    $0x20,%esp
  8005f1:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005f4:	e9 cd fc ff ff       	jmp    8002c6 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8005f9:	83 ec 08             	sub    $0x8,%esp
  8005fc:	53                   	push   %ebx
  8005fd:	51                   	push   %ecx
  8005fe:	ff d6                	call   *%esi
			break;
  800600:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800603:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800606:	e9 bb fc ff ff       	jmp    8002c6 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80060b:	83 ec 08             	sub    $0x8,%esp
  80060e:	53                   	push   %ebx
  80060f:	6a 25                	push   $0x25
  800611:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800613:	83 c4 10             	add    $0x10,%esp
  800616:	eb 01                	jmp    800619 <vprintfmt+0x379>
  800618:	4f                   	dec    %edi
  800619:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  80061d:	75 f9                	jne    800618 <vprintfmt+0x378>
  80061f:	e9 a2 fc ff ff       	jmp    8002c6 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800624:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800627:	5b                   	pop    %ebx
  800628:	5e                   	pop    %esi
  800629:	5f                   	pop    %edi
  80062a:	5d                   	pop    %ebp
  80062b:	c3                   	ret    

0080062c <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80062c:	55                   	push   %ebp
  80062d:	89 e5                	mov    %esp,%ebp
  80062f:	83 ec 18             	sub    $0x18,%esp
  800632:	8b 45 08             	mov    0x8(%ebp),%eax
  800635:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800638:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80063b:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80063f:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800642:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800649:	85 c0                	test   %eax,%eax
  80064b:	74 26                	je     800673 <vsnprintf+0x47>
  80064d:	85 d2                	test   %edx,%edx
  80064f:	7e 29                	jle    80067a <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800651:	ff 75 14             	pushl  0x14(%ebp)
  800654:	ff 75 10             	pushl  0x10(%ebp)
  800657:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80065a:	50                   	push   %eax
  80065b:	68 67 02 80 00       	push   $0x800267
  800660:	e8 3b fc ff ff       	call   8002a0 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800665:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800668:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80066b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80066e:	83 c4 10             	add    $0x10,%esp
  800671:	eb 0c                	jmp    80067f <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800673:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800678:	eb 05                	jmp    80067f <vsnprintf+0x53>
  80067a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80067f:	c9                   	leave  
  800680:	c3                   	ret    

00800681 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800681:	55                   	push   %ebp
  800682:	89 e5                	mov    %esp,%ebp
  800684:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800687:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80068a:	50                   	push   %eax
  80068b:	ff 75 10             	pushl  0x10(%ebp)
  80068e:	ff 75 0c             	pushl  0xc(%ebp)
  800691:	ff 75 08             	pushl  0x8(%ebp)
  800694:	e8 93 ff ff ff       	call   80062c <vsnprintf>
	va_end(ap);

	return rc;
}
  800699:	c9                   	leave  
  80069a:	c3                   	ret    

0080069b <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80069b:	55                   	push   %ebp
  80069c:	89 e5                	mov    %esp,%ebp
  80069e:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8006a1:	b8 00 00 00 00       	mov    $0x0,%eax
  8006a6:	eb 01                	jmp    8006a9 <strlen+0xe>
		n++;
  8006a8:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006a9:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006ad:	75 f9                	jne    8006a8 <strlen+0xd>
		n++;
	return n;
}
  8006af:	5d                   	pop    %ebp
  8006b0:	c3                   	ret    

008006b1 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006b1:	55                   	push   %ebp
  8006b2:	89 e5                	mov    %esp,%ebp
  8006b4:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006b7:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006ba:	ba 00 00 00 00       	mov    $0x0,%edx
  8006bf:	eb 01                	jmp    8006c2 <strnlen+0x11>
		n++;
  8006c1:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006c2:	39 c2                	cmp    %eax,%edx
  8006c4:	74 08                	je     8006ce <strnlen+0x1d>
  8006c6:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006ca:	75 f5                	jne    8006c1 <strnlen+0x10>
  8006cc:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006ce:	5d                   	pop    %ebp
  8006cf:	c3                   	ret    

008006d0 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006d0:	55                   	push   %ebp
  8006d1:	89 e5                	mov    %esp,%ebp
  8006d3:	53                   	push   %ebx
  8006d4:	8b 45 08             	mov    0x8(%ebp),%eax
  8006d7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006da:	89 c2                	mov    %eax,%edx
  8006dc:	42                   	inc    %edx
  8006dd:	41                   	inc    %ecx
  8006de:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006e1:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006e4:	84 db                	test   %bl,%bl
  8006e6:	75 f4                	jne    8006dc <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006e8:	5b                   	pop    %ebx
  8006e9:	5d                   	pop    %ebp
  8006ea:	c3                   	ret    

008006eb <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006eb:	55                   	push   %ebp
  8006ec:	89 e5                	mov    %esp,%ebp
  8006ee:	53                   	push   %ebx
  8006ef:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8006f2:	53                   	push   %ebx
  8006f3:	e8 a3 ff ff ff       	call   80069b <strlen>
  8006f8:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8006fb:	ff 75 0c             	pushl  0xc(%ebp)
  8006fe:	01 d8                	add    %ebx,%eax
  800700:	50                   	push   %eax
  800701:	e8 ca ff ff ff       	call   8006d0 <strcpy>
	return dst;
}
  800706:	89 d8                	mov    %ebx,%eax
  800708:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80070b:	c9                   	leave  
  80070c:	c3                   	ret    

0080070d <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80070d:	55                   	push   %ebp
  80070e:	89 e5                	mov    %esp,%ebp
  800710:	56                   	push   %esi
  800711:	53                   	push   %ebx
  800712:	8b 75 08             	mov    0x8(%ebp),%esi
  800715:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800718:	89 f3                	mov    %esi,%ebx
  80071a:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80071d:	89 f2                	mov    %esi,%edx
  80071f:	eb 0c                	jmp    80072d <strncpy+0x20>
		*dst++ = *src;
  800721:	42                   	inc    %edx
  800722:	8a 01                	mov    (%ecx),%al
  800724:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800727:	80 39 01             	cmpb   $0x1,(%ecx)
  80072a:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80072d:	39 da                	cmp    %ebx,%edx
  80072f:	75 f0                	jne    800721 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800731:	89 f0                	mov    %esi,%eax
  800733:	5b                   	pop    %ebx
  800734:	5e                   	pop    %esi
  800735:	5d                   	pop    %ebp
  800736:	c3                   	ret    

00800737 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800737:	55                   	push   %ebp
  800738:	89 e5                	mov    %esp,%ebp
  80073a:	56                   	push   %esi
  80073b:	53                   	push   %ebx
  80073c:	8b 75 08             	mov    0x8(%ebp),%esi
  80073f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800742:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800745:	85 c0                	test   %eax,%eax
  800747:	74 1e                	je     800767 <strlcpy+0x30>
  800749:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  80074d:	89 f2                	mov    %esi,%edx
  80074f:	eb 05                	jmp    800756 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800751:	42                   	inc    %edx
  800752:	41                   	inc    %ecx
  800753:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800756:	39 c2                	cmp    %eax,%edx
  800758:	74 08                	je     800762 <strlcpy+0x2b>
  80075a:	8a 19                	mov    (%ecx),%bl
  80075c:	84 db                	test   %bl,%bl
  80075e:	75 f1                	jne    800751 <strlcpy+0x1a>
  800760:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800762:	c6 00 00             	movb   $0x0,(%eax)
  800765:	eb 02                	jmp    800769 <strlcpy+0x32>
  800767:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800769:	29 f0                	sub    %esi,%eax
}
  80076b:	5b                   	pop    %ebx
  80076c:	5e                   	pop    %esi
  80076d:	5d                   	pop    %ebp
  80076e:	c3                   	ret    

0080076f <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80076f:	55                   	push   %ebp
  800770:	89 e5                	mov    %esp,%ebp
  800772:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800775:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800778:	eb 02                	jmp    80077c <strcmp+0xd>
		p++, q++;
  80077a:	41                   	inc    %ecx
  80077b:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80077c:	8a 01                	mov    (%ecx),%al
  80077e:	84 c0                	test   %al,%al
  800780:	74 04                	je     800786 <strcmp+0x17>
  800782:	3a 02                	cmp    (%edx),%al
  800784:	74 f4                	je     80077a <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800786:	0f b6 c0             	movzbl %al,%eax
  800789:	0f b6 12             	movzbl (%edx),%edx
  80078c:	29 d0                	sub    %edx,%eax
}
  80078e:	5d                   	pop    %ebp
  80078f:	c3                   	ret    

00800790 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800790:	55                   	push   %ebp
  800791:	89 e5                	mov    %esp,%ebp
  800793:	53                   	push   %ebx
  800794:	8b 45 08             	mov    0x8(%ebp),%eax
  800797:	8b 55 0c             	mov    0xc(%ebp),%edx
  80079a:	89 c3                	mov    %eax,%ebx
  80079c:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  80079f:	eb 02                	jmp    8007a3 <strncmp+0x13>
		n--, p++, q++;
  8007a1:	40                   	inc    %eax
  8007a2:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8007a3:	39 d8                	cmp    %ebx,%eax
  8007a5:	74 14                	je     8007bb <strncmp+0x2b>
  8007a7:	8a 08                	mov    (%eax),%cl
  8007a9:	84 c9                	test   %cl,%cl
  8007ab:	74 04                	je     8007b1 <strncmp+0x21>
  8007ad:	3a 0a                	cmp    (%edx),%cl
  8007af:	74 f0                	je     8007a1 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007b1:	0f b6 00             	movzbl (%eax),%eax
  8007b4:	0f b6 12             	movzbl (%edx),%edx
  8007b7:	29 d0                	sub    %edx,%eax
  8007b9:	eb 05                	jmp    8007c0 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007bb:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007c0:	5b                   	pop    %ebx
  8007c1:	5d                   	pop    %ebp
  8007c2:	c3                   	ret    

008007c3 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007c3:	55                   	push   %ebp
  8007c4:	89 e5                	mov    %esp,%ebp
  8007c6:	8b 45 08             	mov    0x8(%ebp),%eax
  8007c9:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007cc:	eb 05                	jmp    8007d3 <strchr+0x10>
		if (*s == c)
  8007ce:	38 ca                	cmp    %cl,%dl
  8007d0:	74 0c                	je     8007de <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007d2:	40                   	inc    %eax
  8007d3:	8a 10                	mov    (%eax),%dl
  8007d5:	84 d2                	test   %dl,%dl
  8007d7:	75 f5                	jne    8007ce <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007d9:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007de:	5d                   	pop    %ebp
  8007df:	c3                   	ret    

008007e0 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007e0:	55                   	push   %ebp
  8007e1:	89 e5                	mov    %esp,%ebp
  8007e3:	8b 45 08             	mov    0x8(%ebp),%eax
  8007e6:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007e9:	eb 05                	jmp    8007f0 <strfind+0x10>
		if (*s == c)
  8007eb:	38 ca                	cmp    %cl,%dl
  8007ed:	74 07                	je     8007f6 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007ef:	40                   	inc    %eax
  8007f0:	8a 10                	mov    (%eax),%dl
  8007f2:	84 d2                	test   %dl,%dl
  8007f4:	75 f5                	jne    8007eb <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8007f6:	5d                   	pop    %ebp
  8007f7:	c3                   	ret    

008007f8 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8007f8:	55                   	push   %ebp
  8007f9:	89 e5                	mov    %esp,%ebp
  8007fb:	57                   	push   %edi
  8007fc:	56                   	push   %esi
  8007fd:	53                   	push   %ebx
  8007fe:	8b 7d 08             	mov    0x8(%ebp),%edi
  800801:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800804:	85 c9                	test   %ecx,%ecx
  800806:	74 36                	je     80083e <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800808:	f7 c7 03 00 00 00    	test   $0x3,%edi
  80080e:	75 28                	jne    800838 <memset+0x40>
  800810:	f6 c1 03             	test   $0x3,%cl
  800813:	75 23                	jne    800838 <memset+0x40>
		c &= 0xFF;
  800815:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800819:	89 d3                	mov    %edx,%ebx
  80081b:	c1 e3 08             	shl    $0x8,%ebx
  80081e:	89 d6                	mov    %edx,%esi
  800820:	c1 e6 18             	shl    $0x18,%esi
  800823:	89 d0                	mov    %edx,%eax
  800825:	c1 e0 10             	shl    $0x10,%eax
  800828:	09 f0                	or     %esi,%eax
  80082a:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  80082c:	89 d8                	mov    %ebx,%eax
  80082e:	09 d0                	or     %edx,%eax
  800830:	c1 e9 02             	shr    $0x2,%ecx
  800833:	fc                   	cld    
  800834:	f3 ab                	rep stos %eax,%es:(%edi)
  800836:	eb 06                	jmp    80083e <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800838:	8b 45 0c             	mov    0xc(%ebp),%eax
  80083b:	fc                   	cld    
  80083c:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  80083e:	89 f8                	mov    %edi,%eax
  800840:	5b                   	pop    %ebx
  800841:	5e                   	pop    %esi
  800842:	5f                   	pop    %edi
  800843:	5d                   	pop    %ebp
  800844:	c3                   	ret    

00800845 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800845:	55                   	push   %ebp
  800846:	89 e5                	mov    %esp,%ebp
  800848:	57                   	push   %edi
  800849:	56                   	push   %esi
  80084a:	8b 45 08             	mov    0x8(%ebp),%eax
  80084d:	8b 75 0c             	mov    0xc(%ebp),%esi
  800850:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800853:	39 c6                	cmp    %eax,%esi
  800855:	73 33                	jae    80088a <memmove+0x45>
  800857:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80085a:	39 d0                	cmp    %edx,%eax
  80085c:	73 2c                	jae    80088a <memmove+0x45>
		s += n;
		d += n;
  80085e:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800861:	89 d6                	mov    %edx,%esi
  800863:	09 fe                	or     %edi,%esi
  800865:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80086b:	75 13                	jne    800880 <memmove+0x3b>
  80086d:	f6 c1 03             	test   $0x3,%cl
  800870:	75 0e                	jne    800880 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800872:	83 ef 04             	sub    $0x4,%edi
  800875:	8d 72 fc             	lea    -0x4(%edx),%esi
  800878:	c1 e9 02             	shr    $0x2,%ecx
  80087b:	fd                   	std    
  80087c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80087e:	eb 07                	jmp    800887 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800880:	4f                   	dec    %edi
  800881:	8d 72 ff             	lea    -0x1(%edx),%esi
  800884:	fd                   	std    
  800885:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800887:	fc                   	cld    
  800888:	eb 1d                	jmp    8008a7 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80088a:	89 f2                	mov    %esi,%edx
  80088c:	09 c2                	or     %eax,%edx
  80088e:	f6 c2 03             	test   $0x3,%dl
  800891:	75 0f                	jne    8008a2 <memmove+0x5d>
  800893:	f6 c1 03             	test   $0x3,%cl
  800896:	75 0a                	jne    8008a2 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800898:	c1 e9 02             	shr    $0x2,%ecx
  80089b:	89 c7                	mov    %eax,%edi
  80089d:	fc                   	cld    
  80089e:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008a0:	eb 05                	jmp    8008a7 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8008a2:	89 c7                	mov    %eax,%edi
  8008a4:	fc                   	cld    
  8008a5:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008a7:	5e                   	pop    %esi
  8008a8:	5f                   	pop    %edi
  8008a9:	5d                   	pop    %ebp
  8008aa:	c3                   	ret    

008008ab <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008ab:	55                   	push   %ebp
  8008ac:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008ae:	ff 75 10             	pushl  0x10(%ebp)
  8008b1:	ff 75 0c             	pushl  0xc(%ebp)
  8008b4:	ff 75 08             	pushl  0x8(%ebp)
  8008b7:	e8 89 ff ff ff       	call   800845 <memmove>
}
  8008bc:	c9                   	leave  
  8008bd:	c3                   	ret    

008008be <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008be:	55                   	push   %ebp
  8008bf:	89 e5                	mov    %esp,%ebp
  8008c1:	56                   	push   %esi
  8008c2:	53                   	push   %ebx
  8008c3:	8b 45 08             	mov    0x8(%ebp),%eax
  8008c6:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008c9:	89 c6                	mov    %eax,%esi
  8008cb:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008ce:	eb 14                	jmp    8008e4 <memcmp+0x26>
		if (*s1 != *s2)
  8008d0:	8a 08                	mov    (%eax),%cl
  8008d2:	8a 1a                	mov    (%edx),%bl
  8008d4:	38 d9                	cmp    %bl,%cl
  8008d6:	74 0a                	je     8008e2 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008d8:	0f b6 c1             	movzbl %cl,%eax
  8008db:	0f b6 db             	movzbl %bl,%ebx
  8008de:	29 d8                	sub    %ebx,%eax
  8008e0:	eb 0b                	jmp    8008ed <memcmp+0x2f>
		s1++, s2++;
  8008e2:	40                   	inc    %eax
  8008e3:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008e4:	39 f0                	cmp    %esi,%eax
  8008e6:	75 e8                	jne    8008d0 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008e8:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008ed:	5b                   	pop    %ebx
  8008ee:	5e                   	pop    %esi
  8008ef:	5d                   	pop    %ebp
  8008f0:	c3                   	ret    

008008f1 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8008f1:	55                   	push   %ebp
  8008f2:	89 e5                	mov    %esp,%ebp
  8008f4:	53                   	push   %ebx
  8008f5:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8008f8:	89 c1                	mov    %eax,%ecx
  8008fa:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8008fd:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800901:	eb 08                	jmp    80090b <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800903:	0f b6 10             	movzbl (%eax),%edx
  800906:	39 da                	cmp    %ebx,%edx
  800908:	74 05                	je     80090f <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  80090a:	40                   	inc    %eax
  80090b:	39 c8                	cmp    %ecx,%eax
  80090d:	72 f4                	jb     800903 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  80090f:	5b                   	pop    %ebx
  800910:	5d                   	pop    %ebp
  800911:	c3                   	ret    

00800912 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800912:	55                   	push   %ebp
  800913:	89 e5                	mov    %esp,%ebp
  800915:	57                   	push   %edi
  800916:	56                   	push   %esi
  800917:	53                   	push   %ebx
  800918:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80091b:	eb 01                	jmp    80091e <strtol+0xc>
		s++;
  80091d:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80091e:	8a 01                	mov    (%ecx),%al
  800920:	3c 20                	cmp    $0x20,%al
  800922:	74 f9                	je     80091d <strtol+0xb>
  800924:	3c 09                	cmp    $0x9,%al
  800926:	74 f5                	je     80091d <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800928:	3c 2b                	cmp    $0x2b,%al
  80092a:	75 08                	jne    800934 <strtol+0x22>
		s++;
  80092c:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  80092d:	bf 00 00 00 00       	mov    $0x0,%edi
  800932:	eb 11                	jmp    800945 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800934:	3c 2d                	cmp    $0x2d,%al
  800936:	75 08                	jne    800940 <strtol+0x2e>
		s++, neg = 1;
  800938:	41                   	inc    %ecx
  800939:	bf 01 00 00 00       	mov    $0x1,%edi
  80093e:	eb 05                	jmp    800945 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800940:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800945:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800949:	0f 84 87 00 00 00    	je     8009d6 <strtol+0xc4>
  80094f:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800953:	75 27                	jne    80097c <strtol+0x6a>
  800955:	80 39 30             	cmpb   $0x30,(%ecx)
  800958:	75 22                	jne    80097c <strtol+0x6a>
  80095a:	e9 88 00 00 00       	jmp    8009e7 <strtol+0xd5>
		s += 2, base = 16;
  80095f:	83 c1 02             	add    $0x2,%ecx
  800962:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800969:	eb 11                	jmp    80097c <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  80096b:	41                   	inc    %ecx
  80096c:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800973:	eb 07                	jmp    80097c <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800975:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  80097c:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800981:	8a 11                	mov    (%ecx),%dl
  800983:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800986:	80 fb 09             	cmp    $0x9,%bl
  800989:	77 08                	ja     800993 <strtol+0x81>
			dig = *s - '0';
  80098b:	0f be d2             	movsbl %dl,%edx
  80098e:	83 ea 30             	sub    $0x30,%edx
  800991:	eb 22                	jmp    8009b5 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800993:	8d 72 9f             	lea    -0x61(%edx),%esi
  800996:	89 f3                	mov    %esi,%ebx
  800998:	80 fb 19             	cmp    $0x19,%bl
  80099b:	77 08                	ja     8009a5 <strtol+0x93>
			dig = *s - 'a' + 10;
  80099d:	0f be d2             	movsbl %dl,%edx
  8009a0:	83 ea 57             	sub    $0x57,%edx
  8009a3:	eb 10                	jmp    8009b5 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009a5:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009a8:	89 f3                	mov    %esi,%ebx
  8009aa:	80 fb 19             	cmp    $0x19,%bl
  8009ad:	77 14                	ja     8009c3 <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009af:	0f be d2             	movsbl %dl,%edx
  8009b2:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009b5:	3b 55 10             	cmp    0x10(%ebp),%edx
  8009b8:	7d 09                	jge    8009c3 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009ba:	41                   	inc    %ecx
  8009bb:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009bf:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009c1:	eb be                	jmp    800981 <strtol+0x6f>

	if (endptr)
  8009c3:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009c7:	74 05                	je     8009ce <strtol+0xbc>
		*endptr = (char *) s;
  8009c9:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009cc:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009ce:	85 ff                	test   %edi,%edi
  8009d0:	74 21                	je     8009f3 <strtol+0xe1>
  8009d2:	f7 d8                	neg    %eax
  8009d4:	eb 1d                	jmp    8009f3 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009d6:	80 39 30             	cmpb   $0x30,(%ecx)
  8009d9:	75 9a                	jne    800975 <strtol+0x63>
  8009db:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009df:	0f 84 7a ff ff ff    	je     80095f <strtol+0x4d>
  8009e5:	eb 84                	jmp    80096b <strtol+0x59>
  8009e7:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009eb:	0f 84 6e ff ff ff    	je     80095f <strtol+0x4d>
  8009f1:	eb 89                	jmp    80097c <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  8009f3:	5b                   	pop    %ebx
  8009f4:	5e                   	pop    %esi
  8009f5:	5f                   	pop    %edi
  8009f6:	5d                   	pop    %ebp
  8009f7:	c3                   	ret    

008009f8 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8009f8:	55                   	push   %ebp
  8009f9:	89 e5                	mov    %esp,%ebp
  8009fb:	57                   	push   %edi
  8009fc:	56                   	push   %esi
  8009fd:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8009fe:	b8 00 00 00 00       	mov    $0x0,%eax
  800a03:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a06:	8b 55 08             	mov    0x8(%ebp),%edx
  800a09:	89 c3                	mov    %eax,%ebx
  800a0b:	89 c7                	mov    %eax,%edi
  800a0d:	89 c6                	mov    %eax,%esi
  800a0f:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a11:	5b                   	pop    %ebx
  800a12:	5e                   	pop    %esi
  800a13:	5f                   	pop    %edi
  800a14:	5d                   	pop    %ebp
  800a15:	c3                   	ret    

00800a16 <sys_cgetc>:

int
sys_cgetc(void)
{
  800a16:	55                   	push   %ebp
  800a17:	89 e5                	mov    %esp,%ebp
  800a19:	57                   	push   %edi
  800a1a:	56                   	push   %esi
  800a1b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a1c:	ba 00 00 00 00       	mov    $0x0,%edx
  800a21:	b8 01 00 00 00       	mov    $0x1,%eax
  800a26:	89 d1                	mov    %edx,%ecx
  800a28:	89 d3                	mov    %edx,%ebx
  800a2a:	89 d7                	mov    %edx,%edi
  800a2c:	89 d6                	mov    %edx,%esi
  800a2e:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a30:	5b                   	pop    %ebx
  800a31:	5e                   	pop    %esi
  800a32:	5f                   	pop    %edi
  800a33:	5d                   	pop    %ebp
  800a34:	c3                   	ret    

00800a35 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a35:	55                   	push   %ebp
  800a36:	89 e5                	mov    %esp,%ebp
  800a38:	57                   	push   %edi
  800a39:	56                   	push   %esi
  800a3a:	53                   	push   %ebx
  800a3b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a3e:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a43:	b8 03 00 00 00       	mov    $0x3,%eax
  800a48:	8b 55 08             	mov    0x8(%ebp),%edx
  800a4b:	89 cb                	mov    %ecx,%ebx
  800a4d:	89 cf                	mov    %ecx,%edi
  800a4f:	89 ce                	mov    %ecx,%esi
  800a51:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a53:	85 c0                	test   %eax,%eax
  800a55:	7e 17                	jle    800a6e <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a57:	83 ec 0c             	sub    $0xc,%esp
  800a5a:	50                   	push   %eax
  800a5b:	6a 03                	push   $0x3
  800a5d:	68 a0 0f 80 00       	push   $0x800fa0
  800a62:	6a 23                	push   $0x23
  800a64:	68 bd 0f 80 00       	push   $0x800fbd
  800a69:	e8 27 00 00 00       	call   800a95 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a6e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a71:	5b                   	pop    %ebx
  800a72:	5e                   	pop    %esi
  800a73:	5f                   	pop    %edi
  800a74:	5d                   	pop    %ebp
  800a75:	c3                   	ret    

00800a76 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a76:	55                   	push   %ebp
  800a77:	89 e5                	mov    %esp,%ebp
  800a79:	57                   	push   %edi
  800a7a:	56                   	push   %esi
  800a7b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a7c:	ba 00 00 00 00       	mov    $0x0,%edx
  800a81:	b8 02 00 00 00       	mov    $0x2,%eax
  800a86:	89 d1                	mov    %edx,%ecx
  800a88:	89 d3                	mov    %edx,%ebx
  800a8a:	89 d7                	mov    %edx,%edi
  800a8c:	89 d6                	mov    %edx,%esi
  800a8e:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800a90:	5b                   	pop    %ebx
  800a91:	5e                   	pop    %esi
  800a92:	5f                   	pop    %edi
  800a93:	5d                   	pop    %ebp
  800a94:	c3                   	ret    

00800a95 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800a95:	55                   	push   %ebp
  800a96:	89 e5                	mov    %esp,%ebp
  800a98:	56                   	push   %esi
  800a99:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800a9a:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800a9d:	8b 35 00 10 80 00    	mov    0x801000,%esi
  800aa3:	e8 ce ff ff ff       	call   800a76 <sys_getenvid>
  800aa8:	83 ec 0c             	sub    $0xc,%esp
  800aab:	ff 75 0c             	pushl  0xc(%ebp)
  800aae:	ff 75 08             	pushl  0x8(%ebp)
  800ab1:	56                   	push   %esi
  800ab2:	50                   	push   %eax
  800ab3:	68 cc 0f 80 00       	push   $0x800fcc
  800ab8:	e8 af f6 ff ff       	call   80016c <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800abd:	83 c4 18             	add    $0x18,%esp
  800ac0:	53                   	push   %ebx
  800ac1:	ff 75 10             	pushl  0x10(%ebp)
  800ac4:	e8 52 f6 ff ff       	call   80011b <vcprintf>
	cprintf("\n");
  800ac9:	c7 04 24 57 0d 80 00 	movl   $0x800d57,(%esp)
  800ad0:	e8 97 f6 ff ff       	call   80016c <cprintf>
  800ad5:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800ad8:	cc                   	int3   
  800ad9:	eb fd                	jmp    800ad8 <_panic+0x43>
  800adb:	90                   	nop

00800adc <__udivdi3>:
  800adc:	55                   	push   %ebp
  800add:	57                   	push   %edi
  800ade:	56                   	push   %esi
  800adf:	53                   	push   %ebx
  800ae0:	83 ec 1c             	sub    $0x1c,%esp
  800ae3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ae7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800aeb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800aef:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800af3:	89 ca                	mov    %ecx,%edx
  800af5:	89 f8                	mov    %edi,%eax
  800af7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800afb:	85 f6                	test   %esi,%esi
  800afd:	75 2d                	jne    800b2c <__udivdi3+0x50>
  800aff:	39 cf                	cmp    %ecx,%edi
  800b01:	77 65                	ja     800b68 <__udivdi3+0x8c>
  800b03:	89 fd                	mov    %edi,%ebp
  800b05:	85 ff                	test   %edi,%edi
  800b07:	75 0b                	jne    800b14 <__udivdi3+0x38>
  800b09:	b8 01 00 00 00       	mov    $0x1,%eax
  800b0e:	31 d2                	xor    %edx,%edx
  800b10:	f7 f7                	div    %edi
  800b12:	89 c5                	mov    %eax,%ebp
  800b14:	31 d2                	xor    %edx,%edx
  800b16:	89 c8                	mov    %ecx,%eax
  800b18:	f7 f5                	div    %ebp
  800b1a:	89 c1                	mov    %eax,%ecx
  800b1c:	89 d8                	mov    %ebx,%eax
  800b1e:	f7 f5                	div    %ebp
  800b20:	89 cf                	mov    %ecx,%edi
  800b22:	89 fa                	mov    %edi,%edx
  800b24:	83 c4 1c             	add    $0x1c,%esp
  800b27:	5b                   	pop    %ebx
  800b28:	5e                   	pop    %esi
  800b29:	5f                   	pop    %edi
  800b2a:	5d                   	pop    %ebp
  800b2b:	c3                   	ret    
  800b2c:	39 ce                	cmp    %ecx,%esi
  800b2e:	77 28                	ja     800b58 <__udivdi3+0x7c>
  800b30:	0f bd fe             	bsr    %esi,%edi
  800b33:	83 f7 1f             	xor    $0x1f,%edi
  800b36:	75 40                	jne    800b78 <__udivdi3+0x9c>
  800b38:	39 ce                	cmp    %ecx,%esi
  800b3a:	72 0a                	jb     800b46 <__udivdi3+0x6a>
  800b3c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b40:	0f 87 9e 00 00 00    	ja     800be4 <__udivdi3+0x108>
  800b46:	b8 01 00 00 00       	mov    $0x1,%eax
  800b4b:	89 fa                	mov    %edi,%edx
  800b4d:	83 c4 1c             	add    $0x1c,%esp
  800b50:	5b                   	pop    %ebx
  800b51:	5e                   	pop    %esi
  800b52:	5f                   	pop    %edi
  800b53:	5d                   	pop    %ebp
  800b54:	c3                   	ret    
  800b55:	8d 76 00             	lea    0x0(%esi),%esi
  800b58:	31 ff                	xor    %edi,%edi
  800b5a:	31 c0                	xor    %eax,%eax
  800b5c:	89 fa                	mov    %edi,%edx
  800b5e:	83 c4 1c             	add    $0x1c,%esp
  800b61:	5b                   	pop    %ebx
  800b62:	5e                   	pop    %esi
  800b63:	5f                   	pop    %edi
  800b64:	5d                   	pop    %ebp
  800b65:	c3                   	ret    
  800b66:	66 90                	xchg   %ax,%ax
  800b68:	89 d8                	mov    %ebx,%eax
  800b6a:	f7 f7                	div    %edi
  800b6c:	31 ff                	xor    %edi,%edi
  800b6e:	89 fa                	mov    %edi,%edx
  800b70:	83 c4 1c             	add    $0x1c,%esp
  800b73:	5b                   	pop    %ebx
  800b74:	5e                   	pop    %esi
  800b75:	5f                   	pop    %edi
  800b76:	5d                   	pop    %ebp
  800b77:	c3                   	ret    
  800b78:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b7d:	89 eb                	mov    %ebp,%ebx
  800b7f:	29 fb                	sub    %edi,%ebx
  800b81:	89 f9                	mov    %edi,%ecx
  800b83:	d3 e6                	shl    %cl,%esi
  800b85:	89 c5                	mov    %eax,%ebp
  800b87:	88 d9                	mov    %bl,%cl
  800b89:	d3 ed                	shr    %cl,%ebp
  800b8b:	89 e9                	mov    %ebp,%ecx
  800b8d:	09 f1                	or     %esi,%ecx
  800b8f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b93:	89 f9                	mov    %edi,%ecx
  800b95:	d3 e0                	shl    %cl,%eax
  800b97:	89 c5                	mov    %eax,%ebp
  800b99:	89 d6                	mov    %edx,%esi
  800b9b:	88 d9                	mov    %bl,%cl
  800b9d:	d3 ee                	shr    %cl,%esi
  800b9f:	89 f9                	mov    %edi,%ecx
  800ba1:	d3 e2                	shl    %cl,%edx
  800ba3:	8b 44 24 08          	mov    0x8(%esp),%eax
  800ba7:	88 d9                	mov    %bl,%cl
  800ba9:	d3 e8                	shr    %cl,%eax
  800bab:	09 c2                	or     %eax,%edx
  800bad:	89 d0                	mov    %edx,%eax
  800baf:	89 f2                	mov    %esi,%edx
  800bb1:	f7 74 24 0c          	divl   0xc(%esp)
  800bb5:	89 d6                	mov    %edx,%esi
  800bb7:	89 c3                	mov    %eax,%ebx
  800bb9:	f7 e5                	mul    %ebp
  800bbb:	39 d6                	cmp    %edx,%esi
  800bbd:	72 19                	jb     800bd8 <__udivdi3+0xfc>
  800bbf:	74 0b                	je     800bcc <__udivdi3+0xf0>
  800bc1:	89 d8                	mov    %ebx,%eax
  800bc3:	31 ff                	xor    %edi,%edi
  800bc5:	e9 58 ff ff ff       	jmp    800b22 <__udivdi3+0x46>
  800bca:	66 90                	xchg   %ax,%ax
  800bcc:	8b 54 24 08          	mov    0x8(%esp),%edx
  800bd0:	89 f9                	mov    %edi,%ecx
  800bd2:	d3 e2                	shl    %cl,%edx
  800bd4:	39 c2                	cmp    %eax,%edx
  800bd6:	73 e9                	jae    800bc1 <__udivdi3+0xe5>
  800bd8:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800bdb:	31 ff                	xor    %edi,%edi
  800bdd:	e9 40 ff ff ff       	jmp    800b22 <__udivdi3+0x46>
  800be2:	66 90                	xchg   %ax,%ax
  800be4:	31 c0                	xor    %eax,%eax
  800be6:	e9 37 ff ff ff       	jmp    800b22 <__udivdi3+0x46>
  800beb:	90                   	nop

00800bec <__umoddi3>:
  800bec:	55                   	push   %ebp
  800bed:	57                   	push   %edi
  800bee:	56                   	push   %esi
  800bef:	53                   	push   %ebx
  800bf0:	83 ec 1c             	sub    $0x1c,%esp
  800bf3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bf7:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bfb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800bff:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800c03:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800c07:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800c0b:	89 f3                	mov    %esi,%ebx
  800c0d:	89 fa                	mov    %edi,%edx
  800c0f:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800c13:	89 34 24             	mov    %esi,(%esp)
  800c16:	85 c0                	test   %eax,%eax
  800c18:	75 1a                	jne    800c34 <__umoddi3+0x48>
  800c1a:	39 f7                	cmp    %esi,%edi
  800c1c:	0f 86 a2 00 00 00    	jbe    800cc4 <__umoddi3+0xd8>
  800c22:	89 c8                	mov    %ecx,%eax
  800c24:	89 f2                	mov    %esi,%edx
  800c26:	f7 f7                	div    %edi
  800c28:	89 d0                	mov    %edx,%eax
  800c2a:	31 d2                	xor    %edx,%edx
  800c2c:	83 c4 1c             	add    $0x1c,%esp
  800c2f:	5b                   	pop    %ebx
  800c30:	5e                   	pop    %esi
  800c31:	5f                   	pop    %edi
  800c32:	5d                   	pop    %ebp
  800c33:	c3                   	ret    
  800c34:	39 f0                	cmp    %esi,%eax
  800c36:	0f 87 ac 00 00 00    	ja     800ce8 <__umoddi3+0xfc>
  800c3c:	0f bd e8             	bsr    %eax,%ebp
  800c3f:	83 f5 1f             	xor    $0x1f,%ebp
  800c42:	0f 84 ac 00 00 00    	je     800cf4 <__umoddi3+0x108>
  800c48:	bf 20 00 00 00       	mov    $0x20,%edi
  800c4d:	29 ef                	sub    %ebp,%edi
  800c4f:	89 fe                	mov    %edi,%esi
  800c51:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c55:	89 e9                	mov    %ebp,%ecx
  800c57:	d3 e0                	shl    %cl,%eax
  800c59:	89 d7                	mov    %edx,%edi
  800c5b:	89 f1                	mov    %esi,%ecx
  800c5d:	d3 ef                	shr    %cl,%edi
  800c5f:	09 c7                	or     %eax,%edi
  800c61:	89 e9                	mov    %ebp,%ecx
  800c63:	d3 e2                	shl    %cl,%edx
  800c65:	89 14 24             	mov    %edx,(%esp)
  800c68:	89 d8                	mov    %ebx,%eax
  800c6a:	d3 e0                	shl    %cl,%eax
  800c6c:	89 c2                	mov    %eax,%edx
  800c6e:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c72:	d3 e0                	shl    %cl,%eax
  800c74:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c78:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c7c:	89 f1                	mov    %esi,%ecx
  800c7e:	d3 e8                	shr    %cl,%eax
  800c80:	09 d0                	or     %edx,%eax
  800c82:	d3 eb                	shr    %cl,%ebx
  800c84:	89 da                	mov    %ebx,%edx
  800c86:	f7 f7                	div    %edi
  800c88:	89 d3                	mov    %edx,%ebx
  800c8a:	f7 24 24             	mull   (%esp)
  800c8d:	89 c6                	mov    %eax,%esi
  800c8f:	89 d1                	mov    %edx,%ecx
  800c91:	39 d3                	cmp    %edx,%ebx
  800c93:	0f 82 87 00 00 00    	jb     800d20 <__umoddi3+0x134>
  800c99:	0f 84 91 00 00 00    	je     800d30 <__umoddi3+0x144>
  800c9f:	8b 54 24 04          	mov    0x4(%esp),%edx
  800ca3:	29 f2                	sub    %esi,%edx
  800ca5:	19 cb                	sbb    %ecx,%ebx
  800ca7:	89 d8                	mov    %ebx,%eax
  800ca9:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800cad:	d3 e0                	shl    %cl,%eax
  800caf:	89 e9                	mov    %ebp,%ecx
  800cb1:	d3 ea                	shr    %cl,%edx
  800cb3:	09 d0                	or     %edx,%eax
  800cb5:	89 e9                	mov    %ebp,%ecx
  800cb7:	d3 eb                	shr    %cl,%ebx
  800cb9:	89 da                	mov    %ebx,%edx
  800cbb:	83 c4 1c             	add    $0x1c,%esp
  800cbe:	5b                   	pop    %ebx
  800cbf:	5e                   	pop    %esi
  800cc0:	5f                   	pop    %edi
  800cc1:	5d                   	pop    %ebp
  800cc2:	c3                   	ret    
  800cc3:	90                   	nop
  800cc4:	89 fd                	mov    %edi,%ebp
  800cc6:	85 ff                	test   %edi,%edi
  800cc8:	75 0b                	jne    800cd5 <__umoddi3+0xe9>
  800cca:	b8 01 00 00 00       	mov    $0x1,%eax
  800ccf:	31 d2                	xor    %edx,%edx
  800cd1:	f7 f7                	div    %edi
  800cd3:	89 c5                	mov    %eax,%ebp
  800cd5:	89 f0                	mov    %esi,%eax
  800cd7:	31 d2                	xor    %edx,%edx
  800cd9:	f7 f5                	div    %ebp
  800cdb:	89 c8                	mov    %ecx,%eax
  800cdd:	f7 f5                	div    %ebp
  800cdf:	89 d0                	mov    %edx,%eax
  800ce1:	e9 44 ff ff ff       	jmp    800c2a <__umoddi3+0x3e>
  800ce6:	66 90                	xchg   %ax,%ax
  800ce8:	89 c8                	mov    %ecx,%eax
  800cea:	89 f2                	mov    %esi,%edx
  800cec:	83 c4 1c             	add    $0x1c,%esp
  800cef:	5b                   	pop    %ebx
  800cf0:	5e                   	pop    %esi
  800cf1:	5f                   	pop    %edi
  800cf2:	5d                   	pop    %ebp
  800cf3:	c3                   	ret    
  800cf4:	3b 04 24             	cmp    (%esp),%eax
  800cf7:	72 06                	jb     800cff <__umoddi3+0x113>
  800cf9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800cfd:	77 0f                	ja     800d0e <__umoddi3+0x122>
  800cff:	89 f2                	mov    %esi,%edx
  800d01:	29 f9                	sub    %edi,%ecx
  800d03:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800d07:	89 14 24             	mov    %edx,(%esp)
  800d0a:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800d0e:	8b 44 24 04          	mov    0x4(%esp),%eax
  800d12:	8b 14 24             	mov    (%esp),%edx
  800d15:	83 c4 1c             	add    $0x1c,%esp
  800d18:	5b                   	pop    %ebx
  800d19:	5e                   	pop    %esi
  800d1a:	5f                   	pop    %edi
  800d1b:	5d                   	pop    %ebp
  800d1c:	c3                   	ret    
  800d1d:	8d 76 00             	lea    0x0(%esi),%esi
  800d20:	2b 04 24             	sub    (%esp),%eax
  800d23:	19 fa                	sbb    %edi,%edx
  800d25:	89 d1                	mov    %edx,%ecx
  800d27:	89 c6                	mov    %eax,%esi
  800d29:	e9 71 ff ff ff       	jmp    800c9f <__umoddi3+0xb3>
  800d2e:	66 90                	xchg   %ax,%ax
  800d30:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d34:	72 ea                	jb     800d20 <__umoddi3+0x134>
  800d36:	89 d9                	mov    %ebx,%ecx
  800d38:	e9 62 ff ff ff       	jmp    800c9f <__umoddi3+0xb3>
