
obj/user/faultwrite:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 11 00 00 00       	call   800042 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	*(unsigned*)0 = 0;
  800036:	c7 05 00 00 00 00 00 	movl   $0x0,0x0
  80003d:	00 00 00 
}
  800040:	5d                   	pop    %ebp
  800041:	c3                   	ret    

00800042 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800042:	55                   	push   %ebp
  800043:	89 e5                	mov    %esp,%ebp
  800045:	57                   	push   %edi
  800046:	56                   	push   %esi
  800047:	53                   	push   %ebx
  800048:	83 ec 0c             	sub    $0xc,%esp
  80004b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80004e:	8b 75 0c             	mov    0xc(%ebp),%esi
	// set thisenv to point at our Env structure in envs[].
	// LAB 3: Your code here.
	//thisenv = 0;
	int32_t env_Index1 = (int32_t)sys_getenvid();
  800051:	e8 24 0a 00 00       	call   800a7a <sys_getenvid>
  800056:	89 c7                	mov    %eax,%edi
	cprintf("printing env_Index1: %d\n", env_Index1);
  800058:	83 ec 08             	sub    $0x8,%esp
  80005b:	50                   	push   %eax
  80005c:	68 44 0d 80 00       	push   $0x800d44
  800061:	e8 0a 01 00 00       	call   800170 <cprintf>
	int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	cprintf("printing env_Index2: %d\n", env_Index2);
  800066:	83 c4 08             	add    $0x8,%esp
  800069:	81 e7 ff 03 00 00    	and    $0x3ff,%edi
  80006f:	57                   	push   %edi
  800070:	68 5d 0d 80 00       	push   $0x800d5d
  800075:	e8 f6 00 00 00       	call   800170 <cprintf>

	thisenv = &envs[ENVX(sys_getenvid())];
  80007a:	e8 fb 09 00 00       	call   800a7a <sys_getenvid>
  80007f:	25 ff 03 00 00       	and    $0x3ff,%eax
  800084:	8d 14 00             	lea    (%eax,%eax,1),%edx
  800087:	01 d0                	add    %edx,%eax
  800089:	c1 e0 05             	shl    $0x5,%eax
  80008c:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800091:	a3 04 10 80 00       	mov    %eax,0x801004
	cprintf("Addrs of thisenv (envs[0]) is: %p\n", thisenv);
  800096:	83 c4 08             	add    $0x8,%esp
  800099:	50                   	push   %eax
  80009a:	68 80 0d 80 00       	push   $0x800d80
  80009f:	e8 cc 00 00 00       	call   800170 <cprintf>
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000a4:	83 c4 10             	add    $0x10,%esp
  8000a7:	85 db                	test   %ebx,%ebx
  8000a9:	7e 07                	jle    8000b2 <libmain+0x70>
		binaryname = argv[0];
  8000ab:	8b 06                	mov    (%esi),%eax
  8000ad:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  8000b2:	83 ec 08             	sub    $0x8,%esp
  8000b5:	56                   	push   %esi
  8000b6:	53                   	push   %ebx
  8000b7:	e8 77 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000bc:	e8 0b 00 00 00       	call   8000cc <exit>
}
  8000c1:	83 c4 10             	add    $0x10,%esp
  8000c4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8000c7:	5b                   	pop    %ebx
  8000c8:	5e                   	pop    %esi
  8000c9:	5f                   	pop    %edi
  8000ca:	5d                   	pop    %ebp
  8000cb:	c3                   	ret    

008000cc <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000cc:	55                   	push   %ebp
  8000cd:	89 e5                	mov    %esp,%ebp
  8000cf:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000d2:	6a 00                	push   $0x0
  8000d4:	e8 60 09 00 00       	call   800a39 <sys_env_destroy>
}
  8000d9:	83 c4 10             	add    $0x10,%esp
  8000dc:	c9                   	leave  
  8000dd:	c3                   	ret    

008000de <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000de:	55                   	push   %ebp
  8000df:	89 e5                	mov    %esp,%ebp
  8000e1:	53                   	push   %ebx
  8000e2:	83 ec 04             	sub    $0x4,%esp
  8000e5:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000e8:	8b 13                	mov    (%ebx),%edx
  8000ea:	8d 42 01             	lea    0x1(%edx),%eax
  8000ed:	89 03                	mov    %eax,(%ebx)
  8000ef:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000f2:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8000f6:	3d ff 00 00 00       	cmp    $0xff,%eax
  8000fb:	75 1a                	jne    800117 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8000fd:	83 ec 08             	sub    $0x8,%esp
  800100:	68 ff 00 00 00       	push   $0xff
  800105:	8d 43 08             	lea    0x8(%ebx),%eax
  800108:	50                   	push   %eax
  800109:	e8 ee 08 00 00       	call   8009fc <sys_cputs>
		b->idx = 0;
  80010e:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800114:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800117:	ff 43 04             	incl   0x4(%ebx)
}
  80011a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80011d:	c9                   	leave  
  80011e:	c3                   	ret    

0080011f <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80011f:	55                   	push   %ebp
  800120:	89 e5                	mov    %esp,%ebp
  800122:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800128:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80012f:	00 00 00 
	b.cnt = 0;
  800132:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800139:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80013c:	ff 75 0c             	pushl  0xc(%ebp)
  80013f:	ff 75 08             	pushl  0x8(%ebp)
  800142:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800148:	50                   	push   %eax
  800149:	68 de 00 80 00       	push   $0x8000de
  80014e:	e8 51 01 00 00       	call   8002a4 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800153:	83 c4 08             	add    $0x8,%esp
  800156:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80015c:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800162:	50                   	push   %eax
  800163:	e8 94 08 00 00       	call   8009fc <sys_cputs>

	return b.cnt;
}
  800168:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80016e:	c9                   	leave  
  80016f:	c3                   	ret    

00800170 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800170:	55                   	push   %ebp
  800171:	89 e5                	mov    %esp,%ebp
  800173:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800176:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800179:	50                   	push   %eax
  80017a:	ff 75 08             	pushl  0x8(%ebp)
  80017d:	e8 9d ff ff ff       	call   80011f <vcprintf>
	va_end(ap);

	return cnt;
}
  800182:	c9                   	leave  
  800183:	c3                   	ret    

00800184 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800184:	55                   	push   %ebp
  800185:	89 e5                	mov    %esp,%ebp
  800187:	57                   	push   %edi
  800188:	56                   	push   %esi
  800189:	53                   	push   %ebx
  80018a:	83 ec 1c             	sub    $0x1c,%esp
  80018d:	89 c7                	mov    %eax,%edi
  80018f:	89 d6                	mov    %edx,%esi
  800191:	8b 45 08             	mov    0x8(%ebp),%eax
  800194:	8b 55 0c             	mov    0xc(%ebp),%edx
  800197:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80019a:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80019d:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8001a0:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001a5:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001a8:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001ab:	39 d3                	cmp    %edx,%ebx
  8001ad:	72 05                	jb     8001b4 <printnum+0x30>
  8001af:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001b2:	77 45                	ja     8001f9 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001b4:	83 ec 0c             	sub    $0xc,%esp
  8001b7:	ff 75 18             	pushl  0x18(%ebp)
  8001ba:	8b 45 14             	mov    0x14(%ebp),%eax
  8001bd:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001c0:	53                   	push   %ebx
  8001c1:	ff 75 10             	pushl  0x10(%ebp)
  8001c4:	83 ec 08             	sub    $0x8,%esp
  8001c7:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001ca:	ff 75 e0             	pushl  -0x20(%ebp)
  8001cd:	ff 75 dc             	pushl  -0x24(%ebp)
  8001d0:	ff 75 d8             	pushl  -0x28(%ebp)
  8001d3:	e8 08 09 00 00       	call   800ae0 <__udivdi3>
  8001d8:	83 c4 18             	add    $0x18,%esp
  8001db:	52                   	push   %edx
  8001dc:	50                   	push   %eax
  8001dd:	89 f2                	mov    %esi,%edx
  8001df:	89 f8                	mov    %edi,%eax
  8001e1:	e8 9e ff ff ff       	call   800184 <printnum>
  8001e6:	83 c4 20             	add    $0x20,%esp
  8001e9:	eb 16                	jmp    800201 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001eb:	83 ec 08             	sub    $0x8,%esp
  8001ee:	56                   	push   %esi
  8001ef:	ff 75 18             	pushl  0x18(%ebp)
  8001f2:	ff d7                	call   *%edi
  8001f4:	83 c4 10             	add    $0x10,%esp
  8001f7:	eb 03                	jmp    8001fc <printnum+0x78>
  8001f9:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8001fc:	4b                   	dec    %ebx
  8001fd:	85 db                	test   %ebx,%ebx
  8001ff:	7f ea                	jg     8001eb <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800201:	83 ec 08             	sub    $0x8,%esp
  800204:	56                   	push   %esi
  800205:	83 ec 04             	sub    $0x4,%esp
  800208:	ff 75 e4             	pushl  -0x1c(%ebp)
  80020b:	ff 75 e0             	pushl  -0x20(%ebp)
  80020e:	ff 75 dc             	pushl  -0x24(%ebp)
  800211:	ff 75 d8             	pushl  -0x28(%ebp)
  800214:	e8 d7 09 00 00       	call   800bf0 <__umoddi3>
  800219:	83 c4 14             	add    $0x14,%esp
  80021c:	0f be 80 a3 0d 80 00 	movsbl 0x800da3(%eax),%eax
  800223:	50                   	push   %eax
  800224:	ff d7                	call   *%edi
}
  800226:	83 c4 10             	add    $0x10,%esp
  800229:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80022c:	5b                   	pop    %ebx
  80022d:	5e                   	pop    %esi
  80022e:	5f                   	pop    %edi
  80022f:	5d                   	pop    %ebp
  800230:	c3                   	ret    

00800231 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800231:	55                   	push   %ebp
  800232:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800234:	83 fa 01             	cmp    $0x1,%edx
  800237:	7e 0e                	jle    800247 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800239:	8b 10                	mov    (%eax),%edx
  80023b:	8d 4a 08             	lea    0x8(%edx),%ecx
  80023e:	89 08                	mov    %ecx,(%eax)
  800240:	8b 02                	mov    (%edx),%eax
  800242:	8b 52 04             	mov    0x4(%edx),%edx
  800245:	eb 22                	jmp    800269 <getuint+0x38>
	else if (lflag)
  800247:	85 d2                	test   %edx,%edx
  800249:	74 10                	je     80025b <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80024b:	8b 10                	mov    (%eax),%edx
  80024d:	8d 4a 04             	lea    0x4(%edx),%ecx
  800250:	89 08                	mov    %ecx,(%eax)
  800252:	8b 02                	mov    (%edx),%eax
  800254:	ba 00 00 00 00       	mov    $0x0,%edx
  800259:	eb 0e                	jmp    800269 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80025b:	8b 10                	mov    (%eax),%edx
  80025d:	8d 4a 04             	lea    0x4(%edx),%ecx
  800260:	89 08                	mov    %ecx,(%eax)
  800262:	8b 02                	mov    (%edx),%eax
  800264:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800269:	5d                   	pop    %ebp
  80026a:	c3                   	ret    

0080026b <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80026b:	55                   	push   %ebp
  80026c:	89 e5                	mov    %esp,%ebp
  80026e:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800271:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800274:	8b 10                	mov    (%eax),%edx
  800276:	3b 50 04             	cmp    0x4(%eax),%edx
  800279:	73 0a                	jae    800285 <sprintputch+0x1a>
		*b->buf++ = ch;
  80027b:	8d 4a 01             	lea    0x1(%edx),%ecx
  80027e:	89 08                	mov    %ecx,(%eax)
  800280:	8b 45 08             	mov    0x8(%ebp),%eax
  800283:	88 02                	mov    %al,(%edx)
}
  800285:	5d                   	pop    %ebp
  800286:	c3                   	ret    

00800287 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800287:	55                   	push   %ebp
  800288:	89 e5                	mov    %esp,%ebp
  80028a:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80028d:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800290:	50                   	push   %eax
  800291:	ff 75 10             	pushl  0x10(%ebp)
  800294:	ff 75 0c             	pushl  0xc(%ebp)
  800297:	ff 75 08             	pushl  0x8(%ebp)
  80029a:	e8 05 00 00 00       	call   8002a4 <vprintfmt>
	va_end(ap);
}
  80029f:	83 c4 10             	add    $0x10,%esp
  8002a2:	c9                   	leave  
  8002a3:	c3                   	ret    

008002a4 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8002a4:	55                   	push   %ebp
  8002a5:	89 e5                	mov    %esp,%ebp
  8002a7:	57                   	push   %edi
  8002a8:	56                   	push   %esi
  8002a9:	53                   	push   %ebx
  8002aa:	83 ec 2c             	sub    $0x2c,%esp
  8002ad:	8b 75 08             	mov    0x8(%ebp),%esi
  8002b0:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002b3:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002b6:	eb 12                	jmp    8002ca <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002b8:	85 c0                	test   %eax,%eax
  8002ba:	0f 84 68 03 00 00    	je     800628 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002c0:	83 ec 08             	sub    $0x8,%esp
  8002c3:	53                   	push   %ebx
  8002c4:	50                   	push   %eax
  8002c5:	ff d6                	call   *%esi
  8002c7:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002ca:	47                   	inc    %edi
  8002cb:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002cf:	83 f8 25             	cmp    $0x25,%eax
  8002d2:	75 e4                	jne    8002b8 <vprintfmt+0x14>
  8002d4:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002d8:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002df:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002e6:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002ed:	ba 00 00 00 00       	mov    $0x0,%edx
  8002f2:	eb 07                	jmp    8002fb <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002f4:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8002f7:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002fb:	8d 47 01             	lea    0x1(%edi),%eax
  8002fe:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800301:	0f b6 0f             	movzbl (%edi),%ecx
  800304:	8a 07                	mov    (%edi),%al
  800306:	83 e8 23             	sub    $0x23,%eax
  800309:	3c 55                	cmp    $0x55,%al
  80030b:	0f 87 fe 02 00 00    	ja     80060f <vprintfmt+0x36b>
  800311:	0f b6 c0             	movzbl %al,%eax
  800314:	ff 24 85 30 0e 80 00 	jmp    *0x800e30(,%eax,4)
  80031b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80031e:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800322:	eb d7                	jmp    8002fb <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800324:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800327:	b8 00 00 00 00       	mov    $0x0,%eax
  80032c:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80032f:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800332:	01 c0                	add    %eax,%eax
  800334:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800338:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80033b:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80033e:	83 fa 09             	cmp    $0x9,%edx
  800341:	77 34                	ja     800377 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800343:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800344:	eb e9                	jmp    80032f <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800346:	8b 45 14             	mov    0x14(%ebp),%eax
  800349:	8d 48 04             	lea    0x4(%eax),%ecx
  80034c:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80034f:	8b 00                	mov    (%eax),%eax
  800351:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800354:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800357:	eb 24                	jmp    80037d <vprintfmt+0xd9>
  800359:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80035d:	79 07                	jns    800366 <vprintfmt+0xc2>
  80035f:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800366:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800369:	eb 90                	jmp    8002fb <vprintfmt+0x57>
  80036b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80036e:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800375:	eb 84                	jmp    8002fb <vprintfmt+0x57>
  800377:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80037a:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80037d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800381:	0f 89 74 ff ff ff    	jns    8002fb <vprintfmt+0x57>
				width = precision, precision = -1;
  800387:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80038a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80038d:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800394:	e9 62 ff ff ff       	jmp    8002fb <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800399:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80039a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80039d:	e9 59 ff ff ff       	jmp    8002fb <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8003a2:	8b 45 14             	mov    0x14(%ebp),%eax
  8003a5:	8d 50 04             	lea    0x4(%eax),%edx
  8003a8:	89 55 14             	mov    %edx,0x14(%ebp)
  8003ab:	83 ec 08             	sub    $0x8,%esp
  8003ae:	53                   	push   %ebx
  8003af:	ff 30                	pushl  (%eax)
  8003b1:	ff d6                	call   *%esi
			break;
  8003b3:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003b6:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003b9:	e9 0c ff ff ff       	jmp    8002ca <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003be:	8b 45 14             	mov    0x14(%ebp),%eax
  8003c1:	8d 50 04             	lea    0x4(%eax),%edx
  8003c4:	89 55 14             	mov    %edx,0x14(%ebp)
  8003c7:	8b 00                	mov    (%eax),%eax
  8003c9:	85 c0                	test   %eax,%eax
  8003cb:	79 02                	jns    8003cf <vprintfmt+0x12b>
  8003cd:	f7 d8                	neg    %eax
  8003cf:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003d1:	83 f8 06             	cmp    $0x6,%eax
  8003d4:	7f 0b                	jg     8003e1 <vprintfmt+0x13d>
  8003d6:	8b 04 85 88 0f 80 00 	mov    0x800f88(,%eax,4),%eax
  8003dd:	85 c0                	test   %eax,%eax
  8003df:	75 18                	jne    8003f9 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003e1:	52                   	push   %edx
  8003e2:	68 bb 0d 80 00       	push   $0x800dbb
  8003e7:	53                   	push   %ebx
  8003e8:	56                   	push   %esi
  8003e9:	e8 99 fe ff ff       	call   800287 <printfmt>
  8003ee:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f1:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8003f4:	e9 d1 fe ff ff       	jmp    8002ca <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8003f9:	50                   	push   %eax
  8003fa:	68 c4 0d 80 00       	push   $0x800dc4
  8003ff:	53                   	push   %ebx
  800400:	56                   	push   %esi
  800401:	e8 81 fe ff ff       	call   800287 <printfmt>
  800406:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800409:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80040c:	e9 b9 fe ff ff       	jmp    8002ca <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800411:	8b 45 14             	mov    0x14(%ebp),%eax
  800414:	8d 50 04             	lea    0x4(%eax),%edx
  800417:	89 55 14             	mov    %edx,0x14(%ebp)
  80041a:	8b 38                	mov    (%eax),%edi
  80041c:	85 ff                	test   %edi,%edi
  80041e:	75 05                	jne    800425 <vprintfmt+0x181>
				p = "(null)";
  800420:	bf b4 0d 80 00       	mov    $0x800db4,%edi
			if (width > 0 && padc != '-')
  800425:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800429:	0f 8e 90 00 00 00    	jle    8004bf <vprintfmt+0x21b>
  80042f:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800433:	0f 84 8e 00 00 00    	je     8004c7 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800439:	83 ec 08             	sub    $0x8,%esp
  80043c:	ff 75 d0             	pushl  -0x30(%ebp)
  80043f:	57                   	push   %edi
  800440:	e8 70 02 00 00       	call   8006b5 <strnlen>
  800445:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800448:	29 c1                	sub    %eax,%ecx
  80044a:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80044d:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800450:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800454:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800457:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  80045a:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80045c:	eb 0d                	jmp    80046b <vprintfmt+0x1c7>
					putch(padc, putdat);
  80045e:	83 ec 08             	sub    $0x8,%esp
  800461:	53                   	push   %ebx
  800462:	ff 75 e4             	pushl  -0x1c(%ebp)
  800465:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800467:	4f                   	dec    %edi
  800468:	83 c4 10             	add    $0x10,%esp
  80046b:	85 ff                	test   %edi,%edi
  80046d:	7f ef                	jg     80045e <vprintfmt+0x1ba>
  80046f:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800472:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800475:	89 c8                	mov    %ecx,%eax
  800477:	85 c9                	test   %ecx,%ecx
  800479:	79 05                	jns    800480 <vprintfmt+0x1dc>
  80047b:	b8 00 00 00 00       	mov    $0x0,%eax
  800480:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800483:	29 c1                	sub    %eax,%ecx
  800485:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800488:	89 75 08             	mov    %esi,0x8(%ebp)
  80048b:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80048e:	eb 3d                	jmp    8004cd <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800490:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800494:	74 19                	je     8004af <vprintfmt+0x20b>
  800496:	0f be c0             	movsbl %al,%eax
  800499:	83 e8 20             	sub    $0x20,%eax
  80049c:	83 f8 5e             	cmp    $0x5e,%eax
  80049f:	76 0e                	jbe    8004af <vprintfmt+0x20b>
					putch('?', putdat);
  8004a1:	83 ec 08             	sub    $0x8,%esp
  8004a4:	53                   	push   %ebx
  8004a5:	6a 3f                	push   $0x3f
  8004a7:	ff 55 08             	call   *0x8(%ebp)
  8004aa:	83 c4 10             	add    $0x10,%esp
  8004ad:	eb 0b                	jmp    8004ba <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004af:	83 ec 08             	sub    $0x8,%esp
  8004b2:	53                   	push   %ebx
  8004b3:	52                   	push   %edx
  8004b4:	ff 55 08             	call   *0x8(%ebp)
  8004b7:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004ba:	ff 4d e4             	decl   -0x1c(%ebp)
  8004bd:	eb 0e                	jmp    8004cd <vprintfmt+0x229>
  8004bf:	89 75 08             	mov    %esi,0x8(%ebp)
  8004c2:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004c5:	eb 06                	jmp    8004cd <vprintfmt+0x229>
  8004c7:	89 75 08             	mov    %esi,0x8(%ebp)
  8004ca:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004cd:	47                   	inc    %edi
  8004ce:	8a 47 ff             	mov    -0x1(%edi),%al
  8004d1:	0f be d0             	movsbl %al,%edx
  8004d4:	85 d2                	test   %edx,%edx
  8004d6:	74 1d                	je     8004f5 <vprintfmt+0x251>
  8004d8:	85 f6                	test   %esi,%esi
  8004da:	78 b4                	js     800490 <vprintfmt+0x1ec>
  8004dc:	4e                   	dec    %esi
  8004dd:	79 b1                	jns    800490 <vprintfmt+0x1ec>
  8004df:	8b 75 08             	mov    0x8(%ebp),%esi
  8004e2:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004e5:	eb 14                	jmp    8004fb <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004e7:	83 ec 08             	sub    $0x8,%esp
  8004ea:	53                   	push   %ebx
  8004eb:	6a 20                	push   $0x20
  8004ed:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004ef:	4f                   	dec    %edi
  8004f0:	83 c4 10             	add    $0x10,%esp
  8004f3:	eb 06                	jmp    8004fb <vprintfmt+0x257>
  8004f5:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004f8:	8b 75 08             	mov    0x8(%ebp),%esi
  8004fb:	85 ff                	test   %edi,%edi
  8004fd:	7f e8                	jg     8004e7 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004ff:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800502:	e9 c3 fd ff ff       	jmp    8002ca <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800507:	83 fa 01             	cmp    $0x1,%edx
  80050a:	7e 16                	jle    800522 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  80050c:	8b 45 14             	mov    0x14(%ebp),%eax
  80050f:	8d 50 08             	lea    0x8(%eax),%edx
  800512:	89 55 14             	mov    %edx,0x14(%ebp)
  800515:	8b 50 04             	mov    0x4(%eax),%edx
  800518:	8b 00                	mov    (%eax),%eax
  80051a:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80051d:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800520:	eb 32                	jmp    800554 <vprintfmt+0x2b0>
	else if (lflag)
  800522:	85 d2                	test   %edx,%edx
  800524:	74 18                	je     80053e <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800526:	8b 45 14             	mov    0x14(%ebp),%eax
  800529:	8d 50 04             	lea    0x4(%eax),%edx
  80052c:	89 55 14             	mov    %edx,0x14(%ebp)
  80052f:	8b 00                	mov    (%eax),%eax
  800531:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800534:	89 c1                	mov    %eax,%ecx
  800536:	c1 f9 1f             	sar    $0x1f,%ecx
  800539:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80053c:	eb 16                	jmp    800554 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80053e:	8b 45 14             	mov    0x14(%ebp),%eax
  800541:	8d 50 04             	lea    0x4(%eax),%edx
  800544:	89 55 14             	mov    %edx,0x14(%ebp)
  800547:	8b 00                	mov    (%eax),%eax
  800549:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80054c:	89 c1                	mov    %eax,%ecx
  80054e:	c1 f9 1f             	sar    $0x1f,%ecx
  800551:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800554:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800557:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  80055a:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80055e:	79 76                	jns    8005d6 <vprintfmt+0x332>
				putch('-', putdat);
  800560:	83 ec 08             	sub    $0x8,%esp
  800563:	53                   	push   %ebx
  800564:	6a 2d                	push   $0x2d
  800566:	ff d6                	call   *%esi
				num = -(long long) num;
  800568:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80056b:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80056e:	f7 d8                	neg    %eax
  800570:	83 d2 00             	adc    $0x0,%edx
  800573:	f7 da                	neg    %edx
  800575:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800578:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80057d:	eb 5c                	jmp    8005db <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80057f:	8d 45 14             	lea    0x14(%ebp),%eax
  800582:	e8 aa fc ff ff       	call   800231 <getuint>
			base = 10;
  800587:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80058c:	eb 4d                	jmp    8005db <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80058e:	8d 45 14             	lea    0x14(%ebp),%eax
  800591:	e8 9b fc ff ff       	call   800231 <getuint>
			base = 8;
  800596:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80059b:	eb 3e                	jmp    8005db <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80059d:	83 ec 08             	sub    $0x8,%esp
  8005a0:	53                   	push   %ebx
  8005a1:	6a 30                	push   $0x30
  8005a3:	ff d6                	call   *%esi
			putch('x', putdat);
  8005a5:	83 c4 08             	add    $0x8,%esp
  8005a8:	53                   	push   %ebx
  8005a9:	6a 78                	push   $0x78
  8005ab:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005ad:	8b 45 14             	mov    0x14(%ebp),%eax
  8005b0:	8d 50 04             	lea    0x4(%eax),%edx
  8005b3:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005b6:	8b 00                	mov    (%eax),%eax
  8005b8:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005bd:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005c0:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005c5:	eb 14                	jmp    8005db <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005c7:	8d 45 14             	lea    0x14(%ebp),%eax
  8005ca:	e8 62 fc ff ff       	call   800231 <getuint>
			base = 16;
  8005cf:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005d4:	eb 05                	jmp    8005db <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005d6:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005db:	83 ec 0c             	sub    $0xc,%esp
  8005de:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005e2:	57                   	push   %edi
  8005e3:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005e6:	51                   	push   %ecx
  8005e7:	52                   	push   %edx
  8005e8:	50                   	push   %eax
  8005e9:	89 da                	mov    %ebx,%edx
  8005eb:	89 f0                	mov    %esi,%eax
  8005ed:	e8 92 fb ff ff       	call   800184 <printnum>
			break;
  8005f2:	83 c4 20             	add    $0x20,%esp
  8005f5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005f8:	e9 cd fc ff ff       	jmp    8002ca <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8005fd:	83 ec 08             	sub    $0x8,%esp
  800600:	53                   	push   %ebx
  800601:	51                   	push   %ecx
  800602:	ff d6                	call   *%esi
			break;
  800604:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800607:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  80060a:	e9 bb fc ff ff       	jmp    8002ca <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80060f:	83 ec 08             	sub    $0x8,%esp
  800612:	53                   	push   %ebx
  800613:	6a 25                	push   $0x25
  800615:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800617:	83 c4 10             	add    $0x10,%esp
  80061a:	eb 01                	jmp    80061d <vprintfmt+0x379>
  80061c:	4f                   	dec    %edi
  80061d:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800621:	75 f9                	jne    80061c <vprintfmt+0x378>
  800623:	e9 a2 fc ff ff       	jmp    8002ca <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800628:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80062b:	5b                   	pop    %ebx
  80062c:	5e                   	pop    %esi
  80062d:	5f                   	pop    %edi
  80062e:	5d                   	pop    %ebp
  80062f:	c3                   	ret    

00800630 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800630:	55                   	push   %ebp
  800631:	89 e5                	mov    %esp,%ebp
  800633:	83 ec 18             	sub    $0x18,%esp
  800636:	8b 45 08             	mov    0x8(%ebp),%eax
  800639:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80063c:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80063f:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800643:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800646:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80064d:	85 c0                	test   %eax,%eax
  80064f:	74 26                	je     800677 <vsnprintf+0x47>
  800651:	85 d2                	test   %edx,%edx
  800653:	7e 29                	jle    80067e <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800655:	ff 75 14             	pushl  0x14(%ebp)
  800658:	ff 75 10             	pushl  0x10(%ebp)
  80065b:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80065e:	50                   	push   %eax
  80065f:	68 6b 02 80 00       	push   $0x80026b
  800664:	e8 3b fc ff ff       	call   8002a4 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800669:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80066c:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80066f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800672:	83 c4 10             	add    $0x10,%esp
  800675:	eb 0c                	jmp    800683 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800677:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80067c:	eb 05                	jmp    800683 <vsnprintf+0x53>
  80067e:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800683:	c9                   	leave  
  800684:	c3                   	ret    

00800685 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800685:	55                   	push   %ebp
  800686:	89 e5                	mov    %esp,%ebp
  800688:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80068b:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80068e:	50                   	push   %eax
  80068f:	ff 75 10             	pushl  0x10(%ebp)
  800692:	ff 75 0c             	pushl  0xc(%ebp)
  800695:	ff 75 08             	pushl  0x8(%ebp)
  800698:	e8 93 ff ff ff       	call   800630 <vsnprintf>
	va_end(ap);

	return rc;
}
  80069d:	c9                   	leave  
  80069e:	c3                   	ret    

0080069f <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80069f:	55                   	push   %ebp
  8006a0:	89 e5                	mov    %esp,%ebp
  8006a2:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8006a5:	b8 00 00 00 00       	mov    $0x0,%eax
  8006aa:	eb 01                	jmp    8006ad <strlen+0xe>
		n++;
  8006ac:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006ad:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006b1:	75 f9                	jne    8006ac <strlen+0xd>
		n++;
	return n;
}
  8006b3:	5d                   	pop    %ebp
  8006b4:	c3                   	ret    

008006b5 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006b5:	55                   	push   %ebp
  8006b6:	89 e5                	mov    %esp,%ebp
  8006b8:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006bb:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006be:	ba 00 00 00 00       	mov    $0x0,%edx
  8006c3:	eb 01                	jmp    8006c6 <strnlen+0x11>
		n++;
  8006c5:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006c6:	39 c2                	cmp    %eax,%edx
  8006c8:	74 08                	je     8006d2 <strnlen+0x1d>
  8006ca:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006ce:	75 f5                	jne    8006c5 <strnlen+0x10>
  8006d0:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006d2:	5d                   	pop    %ebp
  8006d3:	c3                   	ret    

008006d4 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006d4:	55                   	push   %ebp
  8006d5:	89 e5                	mov    %esp,%ebp
  8006d7:	53                   	push   %ebx
  8006d8:	8b 45 08             	mov    0x8(%ebp),%eax
  8006db:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006de:	89 c2                	mov    %eax,%edx
  8006e0:	42                   	inc    %edx
  8006e1:	41                   	inc    %ecx
  8006e2:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006e5:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006e8:	84 db                	test   %bl,%bl
  8006ea:	75 f4                	jne    8006e0 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006ec:	5b                   	pop    %ebx
  8006ed:	5d                   	pop    %ebp
  8006ee:	c3                   	ret    

008006ef <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006ef:	55                   	push   %ebp
  8006f0:	89 e5                	mov    %esp,%ebp
  8006f2:	53                   	push   %ebx
  8006f3:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8006f6:	53                   	push   %ebx
  8006f7:	e8 a3 ff ff ff       	call   80069f <strlen>
  8006fc:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8006ff:	ff 75 0c             	pushl  0xc(%ebp)
  800702:	01 d8                	add    %ebx,%eax
  800704:	50                   	push   %eax
  800705:	e8 ca ff ff ff       	call   8006d4 <strcpy>
	return dst;
}
  80070a:	89 d8                	mov    %ebx,%eax
  80070c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80070f:	c9                   	leave  
  800710:	c3                   	ret    

00800711 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800711:	55                   	push   %ebp
  800712:	89 e5                	mov    %esp,%ebp
  800714:	56                   	push   %esi
  800715:	53                   	push   %ebx
  800716:	8b 75 08             	mov    0x8(%ebp),%esi
  800719:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80071c:	89 f3                	mov    %esi,%ebx
  80071e:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800721:	89 f2                	mov    %esi,%edx
  800723:	eb 0c                	jmp    800731 <strncpy+0x20>
		*dst++ = *src;
  800725:	42                   	inc    %edx
  800726:	8a 01                	mov    (%ecx),%al
  800728:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  80072b:	80 39 01             	cmpb   $0x1,(%ecx)
  80072e:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800731:	39 da                	cmp    %ebx,%edx
  800733:	75 f0                	jne    800725 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800735:	89 f0                	mov    %esi,%eax
  800737:	5b                   	pop    %ebx
  800738:	5e                   	pop    %esi
  800739:	5d                   	pop    %ebp
  80073a:	c3                   	ret    

0080073b <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  80073b:	55                   	push   %ebp
  80073c:	89 e5                	mov    %esp,%ebp
  80073e:	56                   	push   %esi
  80073f:	53                   	push   %ebx
  800740:	8b 75 08             	mov    0x8(%ebp),%esi
  800743:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800746:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800749:	85 c0                	test   %eax,%eax
  80074b:	74 1e                	je     80076b <strlcpy+0x30>
  80074d:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800751:	89 f2                	mov    %esi,%edx
  800753:	eb 05                	jmp    80075a <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800755:	42                   	inc    %edx
  800756:	41                   	inc    %ecx
  800757:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  80075a:	39 c2                	cmp    %eax,%edx
  80075c:	74 08                	je     800766 <strlcpy+0x2b>
  80075e:	8a 19                	mov    (%ecx),%bl
  800760:	84 db                	test   %bl,%bl
  800762:	75 f1                	jne    800755 <strlcpy+0x1a>
  800764:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800766:	c6 00 00             	movb   $0x0,(%eax)
  800769:	eb 02                	jmp    80076d <strlcpy+0x32>
  80076b:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  80076d:	29 f0                	sub    %esi,%eax
}
  80076f:	5b                   	pop    %ebx
  800770:	5e                   	pop    %esi
  800771:	5d                   	pop    %ebp
  800772:	c3                   	ret    

00800773 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800773:	55                   	push   %ebp
  800774:	89 e5                	mov    %esp,%ebp
  800776:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800779:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80077c:	eb 02                	jmp    800780 <strcmp+0xd>
		p++, q++;
  80077e:	41                   	inc    %ecx
  80077f:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800780:	8a 01                	mov    (%ecx),%al
  800782:	84 c0                	test   %al,%al
  800784:	74 04                	je     80078a <strcmp+0x17>
  800786:	3a 02                	cmp    (%edx),%al
  800788:	74 f4                	je     80077e <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80078a:	0f b6 c0             	movzbl %al,%eax
  80078d:	0f b6 12             	movzbl (%edx),%edx
  800790:	29 d0                	sub    %edx,%eax
}
  800792:	5d                   	pop    %ebp
  800793:	c3                   	ret    

00800794 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800794:	55                   	push   %ebp
  800795:	89 e5                	mov    %esp,%ebp
  800797:	53                   	push   %ebx
  800798:	8b 45 08             	mov    0x8(%ebp),%eax
  80079b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80079e:	89 c3                	mov    %eax,%ebx
  8007a0:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8007a3:	eb 02                	jmp    8007a7 <strncmp+0x13>
		n--, p++, q++;
  8007a5:	40                   	inc    %eax
  8007a6:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8007a7:	39 d8                	cmp    %ebx,%eax
  8007a9:	74 14                	je     8007bf <strncmp+0x2b>
  8007ab:	8a 08                	mov    (%eax),%cl
  8007ad:	84 c9                	test   %cl,%cl
  8007af:	74 04                	je     8007b5 <strncmp+0x21>
  8007b1:	3a 0a                	cmp    (%edx),%cl
  8007b3:	74 f0                	je     8007a5 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007b5:	0f b6 00             	movzbl (%eax),%eax
  8007b8:	0f b6 12             	movzbl (%edx),%edx
  8007bb:	29 d0                	sub    %edx,%eax
  8007bd:	eb 05                	jmp    8007c4 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007bf:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007c4:	5b                   	pop    %ebx
  8007c5:	5d                   	pop    %ebp
  8007c6:	c3                   	ret    

008007c7 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007c7:	55                   	push   %ebp
  8007c8:	89 e5                	mov    %esp,%ebp
  8007ca:	8b 45 08             	mov    0x8(%ebp),%eax
  8007cd:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007d0:	eb 05                	jmp    8007d7 <strchr+0x10>
		if (*s == c)
  8007d2:	38 ca                	cmp    %cl,%dl
  8007d4:	74 0c                	je     8007e2 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007d6:	40                   	inc    %eax
  8007d7:	8a 10                	mov    (%eax),%dl
  8007d9:	84 d2                	test   %dl,%dl
  8007db:	75 f5                	jne    8007d2 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007dd:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007e2:	5d                   	pop    %ebp
  8007e3:	c3                   	ret    

008007e4 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007e4:	55                   	push   %ebp
  8007e5:	89 e5                	mov    %esp,%ebp
  8007e7:	8b 45 08             	mov    0x8(%ebp),%eax
  8007ea:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007ed:	eb 05                	jmp    8007f4 <strfind+0x10>
		if (*s == c)
  8007ef:	38 ca                	cmp    %cl,%dl
  8007f1:	74 07                	je     8007fa <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007f3:	40                   	inc    %eax
  8007f4:	8a 10                	mov    (%eax),%dl
  8007f6:	84 d2                	test   %dl,%dl
  8007f8:	75 f5                	jne    8007ef <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8007fa:	5d                   	pop    %ebp
  8007fb:	c3                   	ret    

008007fc <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8007fc:	55                   	push   %ebp
  8007fd:	89 e5                	mov    %esp,%ebp
  8007ff:	57                   	push   %edi
  800800:	56                   	push   %esi
  800801:	53                   	push   %ebx
  800802:	8b 7d 08             	mov    0x8(%ebp),%edi
  800805:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800808:	85 c9                	test   %ecx,%ecx
  80080a:	74 36                	je     800842 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  80080c:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800812:	75 28                	jne    80083c <memset+0x40>
  800814:	f6 c1 03             	test   $0x3,%cl
  800817:	75 23                	jne    80083c <memset+0x40>
		c &= 0xFF;
  800819:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  80081d:	89 d3                	mov    %edx,%ebx
  80081f:	c1 e3 08             	shl    $0x8,%ebx
  800822:	89 d6                	mov    %edx,%esi
  800824:	c1 e6 18             	shl    $0x18,%esi
  800827:	89 d0                	mov    %edx,%eax
  800829:	c1 e0 10             	shl    $0x10,%eax
  80082c:	09 f0                	or     %esi,%eax
  80082e:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800830:	89 d8                	mov    %ebx,%eax
  800832:	09 d0                	or     %edx,%eax
  800834:	c1 e9 02             	shr    $0x2,%ecx
  800837:	fc                   	cld    
  800838:	f3 ab                	rep stos %eax,%es:(%edi)
  80083a:	eb 06                	jmp    800842 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  80083c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80083f:	fc                   	cld    
  800840:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800842:	89 f8                	mov    %edi,%eax
  800844:	5b                   	pop    %ebx
  800845:	5e                   	pop    %esi
  800846:	5f                   	pop    %edi
  800847:	5d                   	pop    %ebp
  800848:	c3                   	ret    

00800849 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800849:	55                   	push   %ebp
  80084a:	89 e5                	mov    %esp,%ebp
  80084c:	57                   	push   %edi
  80084d:	56                   	push   %esi
  80084e:	8b 45 08             	mov    0x8(%ebp),%eax
  800851:	8b 75 0c             	mov    0xc(%ebp),%esi
  800854:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800857:	39 c6                	cmp    %eax,%esi
  800859:	73 33                	jae    80088e <memmove+0x45>
  80085b:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80085e:	39 d0                	cmp    %edx,%eax
  800860:	73 2c                	jae    80088e <memmove+0x45>
		s += n;
		d += n;
  800862:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800865:	89 d6                	mov    %edx,%esi
  800867:	09 fe                	or     %edi,%esi
  800869:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80086f:	75 13                	jne    800884 <memmove+0x3b>
  800871:	f6 c1 03             	test   $0x3,%cl
  800874:	75 0e                	jne    800884 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800876:	83 ef 04             	sub    $0x4,%edi
  800879:	8d 72 fc             	lea    -0x4(%edx),%esi
  80087c:	c1 e9 02             	shr    $0x2,%ecx
  80087f:	fd                   	std    
  800880:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800882:	eb 07                	jmp    80088b <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800884:	4f                   	dec    %edi
  800885:	8d 72 ff             	lea    -0x1(%edx),%esi
  800888:	fd                   	std    
  800889:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80088b:	fc                   	cld    
  80088c:	eb 1d                	jmp    8008ab <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80088e:	89 f2                	mov    %esi,%edx
  800890:	09 c2                	or     %eax,%edx
  800892:	f6 c2 03             	test   $0x3,%dl
  800895:	75 0f                	jne    8008a6 <memmove+0x5d>
  800897:	f6 c1 03             	test   $0x3,%cl
  80089a:	75 0a                	jne    8008a6 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80089c:	c1 e9 02             	shr    $0x2,%ecx
  80089f:	89 c7                	mov    %eax,%edi
  8008a1:	fc                   	cld    
  8008a2:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008a4:	eb 05                	jmp    8008ab <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8008a6:	89 c7                	mov    %eax,%edi
  8008a8:	fc                   	cld    
  8008a9:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008ab:	5e                   	pop    %esi
  8008ac:	5f                   	pop    %edi
  8008ad:	5d                   	pop    %ebp
  8008ae:	c3                   	ret    

008008af <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008af:	55                   	push   %ebp
  8008b0:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008b2:	ff 75 10             	pushl  0x10(%ebp)
  8008b5:	ff 75 0c             	pushl  0xc(%ebp)
  8008b8:	ff 75 08             	pushl  0x8(%ebp)
  8008bb:	e8 89 ff ff ff       	call   800849 <memmove>
}
  8008c0:	c9                   	leave  
  8008c1:	c3                   	ret    

008008c2 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008c2:	55                   	push   %ebp
  8008c3:	89 e5                	mov    %esp,%ebp
  8008c5:	56                   	push   %esi
  8008c6:	53                   	push   %ebx
  8008c7:	8b 45 08             	mov    0x8(%ebp),%eax
  8008ca:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008cd:	89 c6                	mov    %eax,%esi
  8008cf:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008d2:	eb 14                	jmp    8008e8 <memcmp+0x26>
		if (*s1 != *s2)
  8008d4:	8a 08                	mov    (%eax),%cl
  8008d6:	8a 1a                	mov    (%edx),%bl
  8008d8:	38 d9                	cmp    %bl,%cl
  8008da:	74 0a                	je     8008e6 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008dc:	0f b6 c1             	movzbl %cl,%eax
  8008df:	0f b6 db             	movzbl %bl,%ebx
  8008e2:	29 d8                	sub    %ebx,%eax
  8008e4:	eb 0b                	jmp    8008f1 <memcmp+0x2f>
		s1++, s2++;
  8008e6:	40                   	inc    %eax
  8008e7:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008e8:	39 f0                	cmp    %esi,%eax
  8008ea:	75 e8                	jne    8008d4 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008ec:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008f1:	5b                   	pop    %ebx
  8008f2:	5e                   	pop    %esi
  8008f3:	5d                   	pop    %ebp
  8008f4:	c3                   	ret    

008008f5 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8008f5:	55                   	push   %ebp
  8008f6:	89 e5                	mov    %esp,%ebp
  8008f8:	53                   	push   %ebx
  8008f9:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8008fc:	89 c1                	mov    %eax,%ecx
  8008fe:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800901:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800905:	eb 08                	jmp    80090f <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800907:	0f b6 10             	movzbl (%eax),%edx
  80090a:	39 da                	cmp    %ebx,%edx
  80090c:	74 05                	je     800913 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  80090e:	40                   	inc    %eax
  80090f:	39 c8                	cmp    %ecx,%eax
  800911:	72 f4                	jb     800907 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800913:	5b                   	pop    %ebx
  800914:	5d                   	pop    %ebp
  800915:	c3                   	ret    

00800916 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800916:	55                   	push   %ebp
  800917:	89 e5                	mov    %esp,%ebp
  800919:	57                   	push   %edi
  80091a:	56                   	push   %esi
  80091b:	53                   	push   %ebx
  80091c:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80091f:	eb 01                	jmp    800922 <strtol+0xc>
		s++;
  800921:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800922:	8a 01                	mov    (%ecx),%al
  800924:	3c 20                	cmp    $0x20,%al
  800926:	74 f9                	je     800921 <strtol+0xb>
  800928:	3c 09                	cmp    $0x9,%al
  80092a:	74 f5                	je     800921 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  80092c:	3c 2b                	cmp    $0x2b,%al
  80092e:	75 08                	jne    800938 <strtol+0x22>
		s++;
  800930:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800931:	bf 00 00 00 00       	mov    $0x0,%edi
  800936:	eb 11                	jmp    800949 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800938:	3c 2d                	cmp    $0x2d,%al
  80093a:	75 08                	jne    800944 <strtol+0x2e>
		s++, neg = 1;
  80093c:	41                   	inc    %ecx
  80093d:	bf 01 00 00 00       	mov    $0x1,%edi
  800942:	eb 05                	jmp    800949 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800944:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800949:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80094d:	0f 84 87 00 00 00    	je     8009da <strtol+0xc4>
  800953:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800957:	75 27                	jne    800980 <strtol+0x6a>
  800959:	80 39 30             	cmpb   $0x30,(%ecx)
  80095c:	75 22                	jne    800980 <strtol+0x6a>
  80095e:	e9 88 00 00 00       	jmp    8009eb <strtol+0xd5>
		s += 2, base = 16;
  800963:	83 c1 02             	add    $0x2,%ecx
  800966:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  80096d:	eb 11                	jmp    800980 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  80096f:	41                   	inc    %ecx
  800970:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800977:	eb 07                	jmp    800980 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800979:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800980:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800985:	8a 11                	mov    (%ecx),%dl
  800987:	8d 5a d0             	lea    -0x30(%edx),%ebx
  80098a:	80 fb 09             	cmp    $0x9,%bl
  80098d:	77 08                	ja     800997 <strtol+0x81>
			dig = *s - '0';
  80098f:	0f be d2             	movsbl %dl,%edx
  800992:	83 ea 30             	sub    $0x30,%edx
  800995:	eb 22                	jmp    8009b9 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800997:	8d 72 9f             	lea    -0x61(%edx),%esi
  80099a:	89 f3                	mov    %esi,%ebx
  80099c:	80 fb 19             	cmp    $0x19,%bl
  80099f:	77 08                	ja     8009a9 <strtol+0x93>
			dig = *s - 'a' + 10;
  8009a1:	0f be d2             	movsbl %dl,%edx
  8009a4:	83 ea 57             	sub    $0x57,%edx
  8009a7:	eb 10                	jmp    8009b9 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009a9:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009ac:	89 f3                	mov    %esi,%ebx
  8009ae:	80 fb 19             	cmp    $0x19,%bl
  8009b1:	77 14                	ja     8009c7 <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009b3:	0f be d2             	movsbl %dl,%edx
  8009b6:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009b9:	3b 55 10             	cmp    0x10(%ebp),%edx
  8009bc:	7d 09                	jge    8009c7 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009be:	41                   	inc    %ecx
  8009bf:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009c3:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009c5:	eb be                	jmp    800985 <strtol+0x6f>

	if (endptr)
  8009c7:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009cb:	74 05                	je     8009d2 <strtol+0xbc>
		*endptr = (char *) s;
  8009cd:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009d0:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009d2:	85 ff                	test   %edi,%edi
  8009d4:	74 21                	je     8009f7 <strtol+0xe1>
  8009d6:	f7 d8                	neg    %eax
  8009d8:	eb 1d                	jmp    8009f7 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009da:	80 39 30             	cmpb   $0x30,(%ecx)
  8009dd:	75 9a                	jne    800979 <strtol+0x63>
  8009df:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009e3:	0f 84 7a ff ff ff    	je     800963 <strtol+0x4d>
  8009e9:	eb 84                	jmp    80096f <strtol+0x59>
  8009eb:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009ef:	0f 84 6e ff ff ff    	je     800963 <strtol+0x4d>
  8009f5:	eb 89                	jmp    800980 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  8009f7:	5b                   	pop    %ebx
  8009f8:	5e                   	pop    %esi
  8009f9:	5f                   	pop    %edi
  8009fa:	5d                   	pop    %ebp
  8009fb:	c3                   	ret    

008009fc <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8009fc:	55                   	push   %ebp
  8009fd:	89 e5                	mov    %esp,%ebp
  8009ff:	57                   	push   %edi
  800a00:	56                   	push   %esi
  800a01:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a02:	b8 00 00 00 00       	mov    $0x0,%eax
  800a07:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a0a:	8b 55 08             	mov    0x8(%ebp),%edx
  800a0d:	89 c3                	mov    %eax,%ebx
  800a0f:	89 c7                	mov    %eax,%edi
  800a11:	89 c6                	mov    %eax,%esi
  800a13:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a15:	5b                   	pop    %ebx
  800a16:	5e                   	pop    %esi
  800a17:	5f                   	pop    %edi
  800a18:	5d                   	pop    %ebp
  800a19:	c3                   	ret    

00800a1a <sys_cgetc>:

int
sys_cgetc(void)
{
  800a1a:	55                   	push   %ebp
  800a1b:	89 e5                	mov    %esp,%ebp
  800a1d:	57                   	push   %edi
  800a1e:	56                   	push   %esi
  800a1f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a20:	ba 00 00 00 00       	mov    $0x0,%edx
  800a25:	b8 01 00 00 00       	mov    $0x1,%eax
  800a2a:	89 d1                	mov    %edx,%ecx
  800a2c:	89 d3                	mov    %edx,%ebx
  800a2e:	89 d7                	mov    %edx,%edi
  800a30:	89 d6                	mov    %edx,%esi
  800a32:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a34:	5b                   	pop    %ebx
  800a35:	5e                   	pop    %esi
  800a36:	5f                   	pop    %edi
  800a37:	5d                   	pop    %ebp
  800a38:	c3                   	ret    

00800a39 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a39:	55                   	push   %ebp
  800a3a:	89 e5                	mov    %esp,%ebp
  800a3c:	57                   	push   %edi
  800a3d:	56                   	push   %esi
  800a3e:	53                   	push   %ebx
  800a3f:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a42:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a47:	b8 03 00 00 00       	mov    $0x3,%eax
  800a4c:	8b 55 08             	mov    0x8(%ebp),%edx
  800a4f:	89 cb                	mov    %ecx,%ebx
  800a51:	89 cf                	mov    %ecx,%edi
  800a53:	89 ce                	mov    %ecx,%esi
  800a55:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a57:	85 c0                	test   %eax,%eax
  800a59:	7e 17                	jle    800a72 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a5b:	83 ec 0c             	sub    $0xc,%esp
  800a5e:	50                   	push   %eax
  800a5f:	6a 03                	push   $0x3
  800a61:	68 a4 0f 80 00       	push   $0x800fa4
  800a66:	6a 23                	push   $0x23
  800a68:	68 c1 0f 80 00       	push   $0x800fc1
  800a6d:	e8 27 00 00 00       	call   800a99 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a72:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a75:	5b                   	pop    %ebx
  800a76:	5e                   	pop    %esi
  800a77:	5f                   	pop    %edi
  800a78:	5d                   	pop    %ebp
  800a79:	c3                   	ret    

00800a7a <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a7a:	55                   	push   %ebp
  800a7b:	89 e5                	mov    %esp,%ebp
  800a7d:	57                   	push   %edi
  800a7e:	56                   	push   %esi
  800a7f:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a80:	ba 00 00 00 00       	mov    $0x0,%edx
  800a85:	b8 02 00 00 00       	mov    $0x2,%eax
  800a8a:	89 d1                	mov    %edx,%ecx
  800a8c:	89 d3                	mov    %edx,%ebx
  800a8e:	89 d7                	mov    %edx,%edi
  800a90:	89 d6                	mov    %edx,%esi
  800a92:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800a94:	5b                   	pop    %ebx
  800a95:	5e                   	pop    %esi
  800a96:	5f                   	pop    %edi
  800a97:	5d                   	pop    %ebp
  800a98:	c3                   	ret    

00800a99 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800a99:	55                   	push   %ebp
  800a9a:	89 e5                	mov    %esp,%ebp
  800a9c:	56                   	push   %esi
  800a9d:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800a9e:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800aa1:	8b 35 00 10 80 00    	mov    0x801000,%esi
  800aa7:	e8 ce ff ff ff       	call   800a7a <sys_getenvid>
  800aac:	83 ec 0c             	sub    $0xc,%esp
  800aaf:	ff 75 0c             	pushl  0xc(%ebp)
  800ab2:	ff 75 08             	pushl  0x8(%ebp)
  800ab5:	56                   	push   %esi
  800ab6:	50                   	push   %eax
  800ab7:	68 d0 0f 80 00       	push   $0x800fd0
  800abc:	e8 af f6 ff ff       	call   800170 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800ac1:	83 c4 18             	add    $0x18,%esp
  800ac4:	53                   	push   %ebx
  800ac5:	ff 75 10             	pushl  0x10(%ebp)
  800ac8:	e8 52 f6 ff ff       	call   80011f <vcprintf>
	cprintf("\n");
  800acd:	c7 04 24 5b 0d 80 00 	movl   $0x800d5b,(%esp)
  800ad4:	e8 97 f6 ff ff       	call   800170 <cprintf>
  800ad9:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800adc:	cc                   	int3   
  800add:	eb fd                	jmp    800adc <_panic+0x43>
  800adf:	90                   	nop

00800ae0 <__udivdi3>:
  800ae0:	55                   	push   %ebp
  800ae1:	57                   	push   %edi
  800ae2:	56                   	push   %esi
  800ae3:	53                   	push   %ebx
  800ae4:	83 ec 1c             	sub    $0x1c,%esp
  800ae7:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800aeb:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800aef:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800af3:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800af7:	89 ca                	mov    %ecx,%edx
  800af9:	89 f8                	mov    %edi,%eax
  800afb:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800aff:	85 f6                	test   %esi,%esi
  800b01:	75 2d                	jne    800b30 <__udivdi3+0x50>
  800b03:	39 cf                	cmp    %ecx,%edi
  800b05:	77 65                	ja     800b6c <__udivdi3+0x8c>
  800b07:	89 fd                	mov    %edi,%ebp
  800b09:	85 ff                	test   %edi,%edi
  800b0b:	75 0b                	jne    800b18 <__udivdi3+0x38>
  800b0d:	b8 01 00 00 00       	mov    $0x1,%eax
  800b12:	31 d2                	xor    %edx,%edx
  800b14:	f7 f7                	div    %edi
  800b16:	89 c5                	mov    %eax,%ebp
  800b18:	31 d2                	xor    %edx,%edx
  800b1a:	89 c8                	mov    %ecx,%eax
  800b1c:	f7 f5                	div    %ebp
  800b1e:	89 c1                	mov    %eax,%ecx
  800b20:	89 d8                	mov    %ebx,%eax
  800b22:	f7 f5                	div    %ebp
  800b24:	89 cf                	mov    %ecx,%edi
  800b26:	89 fa                	mov    %edi,%edx
  800b28:	83 c4 1c             	add    $0x1c,%esp
  800b2b:	5b                   	pop    %ebx
  800b2c:	5e                   	pop    %esi
  800b2d:	5f                   	pop    %edi
  800b2e:	5d                   	pop    %ebp
  800b2f:	c3                   	ret    
  800b30:	39 ce                	cmp    %ecx,%esi
  800b32:	77 28                	ja     800b5c <__udivdi3+0x7c>
  800b34:	0f bd fe             	bsr    %esi,%edi
  800b37:	83 f7 1f             	xor    $0x1f,%edi
  800b3a:	75 40                	jne    800b7c <__udivdi3+0x9c>
  800b3c:	39 ce                	cmp    %ecx,%esi
  800b3e:	72 0a                	jb     800b4a <__udivdi3+0x6a>
  800b40:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b44:	0f 87 9e 00 00 00    	ja     800be8 <__udivdi3+0x108>
  800b4a:	b8 01 00 00 00       	mov    $0x1,%eax
  800b4f:	89 fa                	mov    %edi,%edx
  800b51:	83 c4 1c             	add    $0x1c,%esp
  800b54:	5b                   	pop    %ebx
  800b55:	5e                   	pop    %esi
  800b56:	5f                   	pop    %edi
  800b57:	5d                   	pop    %ebp
  800b58:	c3                   	ret    
  800b59:	8d 76 00             	lea    0x0(%esi),%esi
  800b5c:	31 ff                	xor    %edi,%edi
  800b5e:	31 c0                	xor    %eax,%eax
  800b60:	89 fa                	mov    %edi,%edx
  800b62:	83 c4 1c             	add    $0x1c,%esp
  800b65:	5b                   	pop    %ebx
  800b66:	5e                   	pop    %esi
  800b67:	5f                   	pop    %edi
  800b68:	5d                   	pop    %ebp
  800b69:	c3                   	ret    
  800b6a:	66 90                	xchg   %ax,%ax
  800b6c:	89 d8                	mov    %ebx,%eax
  800b6e:	f7 f7                	div    %edi
  800b70:	31 ff                	xor    %edi,%edi
  800b72:	89 fa                	mov    %edi,%edx
  800b74:	83 c4 1c             	add    $0x1c,%esp
  800b77:	5b                   	pop    %ebx
  800b78:	5e                   	pop    %esi
  800b79:	5f                   	pop    %edi
  800b7a:	5d                   	pop    %ebp
  800b7b:	c3                   	ret    
  800b7c:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b81:	89 eb                	mov    %ebp,%ebx
  800b83:	29 fb                	sub    %edi,%ebx
  800b85:	89 f9                	mov    %edi,%ecx
  800b87:	d3 e6                	shl    %cl,%esi
  800b89:	89 c5                	mov    %eax,%ebp
  800b8b:	88 d9                	mov    %bl,%cl
  800b8d:	d3 ed                	shr    %cl,%ebp
  800b8f:	89 e9                	mov    %ebp,%ecx
  800b91:	09 f1                	or     %esi,%ecx
  800b93:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b97:	89 f9                	mov    %edi,%ecx
  800b99:	d3 e0                	shl    %cl,%eax
  800b9b:	89 c5                	mov    %eax,%ebp
  800b9d:	89 d6                	mov    %edx,%esi
  800b9f:	88 d9                	mov    %bl,%cl
  800ba1:	d3 ee                	shr    %cl,%esi
  800ba3:	89 f9                	mov    %edi,%ecx
  800ba5:	d3 e2                	shl    %cl,%edx
  800ba7:	8b 44 24 08          	mov    0x8(%esp),%eax
  800bab:	88 d9                	mov    %bl,%cl
  800bad:	d3 e8                	shr    %cl,%eax
  800baf:	09 c2                	or     %eax,%edx
  800bb1:	89 d0                	mov    %edx,%eax
  800bb3:	89 f2                	mov    %esi,%edx
  800bb5:	f7 74 24 0c          	divl   0xc(%esp)
  800bb9:	89 d6                	mov    %edx,%esi
  800bbb:	89 c3                	mov    %eax,%ebx
  800bbd:	f7 e5                	mul    %ebp
  800bbf:	39 d6                	cmp    %edx,%esi
  800bc1:	72 19                	jb     800bdc <__udivdi3+0xfc>
  800bc3:	74 0b                	je     800bd0 <__udivdi3+0xf0>
  800bc5:	89 d8                	mov    %ebx,%eax
  800bc7:	31 ff                	xor    %edi,%edi
  800bc9:	e9 58 ff ff ff       	jmp    800b26 <__udivdi3+0x46>
  800bce:	66 90                	xchg   %ax,%ax
  800bd0:	8b 54 24 08          	mov    0x8(%esp),%edx
  800bd4:	89 f9                	mov    %edi,%ecx
  800bd6:	d3 e2                	shl    %cl,%edx
  800bd8:	39 c2                	cmp    %eax,%edx
  800bda:	73 e9                	jae    800bc5 <__udivdi3+0xe5>
  800bdc:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800bdf:	31 ff                	xor    %edi,%edi
  800be1:	e9 40 ff ff ff       	jmp    800b26 <__udivdi3+0x46>
  800be6:	66 90                	xchg   %ax,%ax
  800be8:	31 c0                	xor    %eax,%eax
  800bea:	e9 37 ff ff ff       	jmp    800b26 <__udivdi3+0x46>
  800bef:	90                   	nop

00800bf0 <__umoddi3>:
  800bf0:	55                   	push   %ebp
  800bf1:	57                   	push   %edi
  800bf2:	56                   	push   %esi
  800bf3:	53                   	push   %ebx
  800bf4:	83 ec 1c             	sub    $0x1c,%esp
  800bf7:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bfb:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bff:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800c03:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800c07:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800c0b:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800c0f:	89 f3                	mov    %esi,%ebx
  800c11:	89 fa                	mov    %edi,%edx
  800c13:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800c17:	89 34 24             	mov    %esi,(%esp)
  800c1a:	85 c0                	test   %eax,%eax
  800c1c:	75 1a                	jne    800c38 <__umoddi3+0x48>
  800c1e:	39 f7                	cmp    %esi,%edi
  800c20:	0f 86 a2 00 00 00    	jbe    800cc8 <__umoddi3+0xd8>
  800c26:	89 c8                	mov    %ecx,%eax
  800c28:	89 f2                	mov    %esi,%edx
  800c2a:	f7 f7                	div    %edi
  800c2c:	89 d0                	mov    %edx,%eax
  800c2e:	31 d2                	xor    %edx,%edx
  800c30:	83 c4 1c             	add    $0x1c,%esp
  800c33:	5b                   	pop    %ebx
  800c34:	5e                   	pop    %esi
  800c35:	5f                   	pop    %edi
  800c36:	5d                   	pop    %ebp
  800c37:	c3                   	ret    
  800c38:	39 f0                	cmp    %esi,%eax
  800c3a:	0f 87 ac 00 00 00    	ja     800cec <__umoddi3+0xfc>
  800c40:	0f bd e8             	bsr    %eax,%ebp
  800c43:	83 f5 1f             	xor    $0x1f,%ebp
  800c46:	0f 84 ac 00 00 00    	je     800cf8 <__umoddi3+0x108>
  800c4c:	bf 20 00 00 00       	mov    $0x20,%edi
  800c51:	29 ef                	sub    %ebp,%edi
  800c53:	89 fe                	mov    %edi,%esi
  800c55:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c59:	89 e9                	mov    %ebp,%ecx
  800c5b:	d3 e0                	shl    %cl,%eax
  800c5d:	89 d7                	mov    %edx,%edi
  800c5f:	89 f1                	mov    %esi,%ecx
  800c61:	d3 ef                	shr    %cl,%edi
  800c63:	09 c7                	or     %eax,%edi
  800c65:	89 e9                	mov    %ebp,%ecx
  800c67:	d3 e2                	shl    %cl,%edx
  800c69:	89 14 24             	mov    %edx,(%esp)
  800c6c:	89 d8                	mov    %ebx,%eax
  800c6e:	d3 e0                	shl    %cl,%eax
  800c70:	89 c2                	mov    %eax,%edx
  800c72:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c76:	d3 e0                	shl    %cl,%eax
  800c78:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c7c:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c80:	89 f1                	mov    %esi,%ecx
  800c82:	d3 e8                	shr    %cl,%eax
  800c84:	09 d0                	or     %edx,%eax
  800c86:	d3 eb                	shr    %cl,%ebx
  800c88:	89 da                	mov    %ebx,%edx
  800c8a:	f7 f7                	div    %edi
  800c8c:	89 d3                	mov    %edx,%ebx
  800c8e:	f7 24 24             	mull   (%esp)
  800c91:	89 c6                	mov    %eax,%esi
  800c93:	89 d1                	mov    %edx,%ecx
  800c95:	39 d3                	cmp    %edx,%ebx
  800c97:	0f 82 87 00 00 00    	jb     800d24 <__umoddi3+0x134>
  800c9d:	0f 84 91 00 00 00    	je     800d34 <__umoddi3+0x144>
  800ca3:	8b 54 24 04          	mov    0x4(%esp),%edx
  800ca7:	29 f2                	sub    %esi,%edx
  800ca9:	19 cb                	sbb    %ecx,%ebx
  800cab:	89 d8                	mov    %ebx,%eax
  800cad:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800cb1:	d3 e0                	shl    %cl,%eax
  800cb3:	89 e9                	mov    %ebp,%ecx
  800cb5:	d3 ea                	shr    %cl,%edx
  800cb7:	09 d0                	or     %edx,%eax
  800cb9:	89 e9                	mov    %ebp,%ecx
  800cbb:	d3 eb                	shr    %cl,%ebx
  800cbd:	89 da                	mov    %ebx,%edx
  800cbf:	83 c4 1c             	add    $0x1c,%esp
  800cc2:	5b                   	pop    %ebx
  800cc3:	5e                   	pop    %esi
  800cc4:	5f                   	pop    %edi
  800cc5:	5d                   	pop    %ebp
  800cc6:	c3                   	ret    
  800cc7:	90                   	nop
  800cc8:	89 fd                	mov    %edi,%ebp
  800cca:	85 ff                	test   %edi,%edi
  800ccc:	75 0b                	jne    800cd9 <__umoddi3+0xe9>
  800cce:	b8 01 00 00 00       	mov    $0x1,%eax
  800cd3:	31 d2                	xor    %edx,%edx
  800cd5:	f7 f7                	div    %edi
  800cd7:	89 c5                	mov    %eax,%ebp
  800cd9:	89 f0                	mov    %esi,%eax
  800cdb:	31 d2                	xor    %edx,%edx
  800cdd:	f7 f5                	div    %ebp
  800cdf:	89 c8                	mov    %ecx,%eax
  800ce1:	f7 f5                	div    %ebp
  800ce3:	89 d0                	mov    %edx,%eax
  800ce5:	e9 44 ff ff ff       	jmp    800c2e <__umoddi3+0x3e>
  800cea:	66 90                	xchg   %ax,%ax
  800cec:	89 c8                	mov    %ecx,%eax
  800cee:	89 f2                	mov    %esi,%edx
  800cf0:	83 c4 1c             	add    $0x1c,%esp
  800cf3:	5b                   	pop    %ebx
  800cf4:	5e                   	pop    %esi
  800cf5:	5f                   	pop    %edi
  800cf6:	5d                   	pop    %ebp
  800cf7:	c3                   	ret    
  800cf8:	3b 04 24             	cmp    (%esp),%eax
  800cfb:	72 06                	jb     800d03 <__umoddi3+0x113>
  800cfd:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800d01:	77 0f                	ja     800d12 <__umoddi3+0x122>
  800d03:	89 f2                	mov    %esi,%edx
  800d05:	29 f9                	sub    %edi,%ecx
  800d07:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800d0b:	89 14 24             	mov    %edx,(%esp)
  800d0e:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800d12:	8b 44 24 04          	mov    0x4(%esp),%eax
  800d16:	8b 14 24             	mov    (%esp),%edx
  800d19:	83 c4 1c             	add    $0x1c,%esp
  800d1c:	5b                   	pop    %ebx
  800d1d:	5e                   	pop    %esi
  800d1e:	5f                   	pop    %edi
  800d1f:	5d                   	pop    %ebp
  800d20:	c3                   	ret    
  800d21:	8d 76 00             	lea    0x0(%esi),%esi
  800d24:	2b 04 24             	sub    (%esp),%eax
  800d27:	19 fa                	sbb    %edi,%edx
  800d29:	89 d1                	mov    %edx,%ecx
  800d2b:	89 c6                	mov    %eax,%esi
  800d2d:	e9 71 ff ff ff       	jmp    800ca3 <__umoddi3+0xb3>
  800d32:	66 90                	xchg   %ax,%ax
  800d34:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d38:	72 ea                	jb     800d24 <__umoddi3+0x134>
  800d3a:	89 d9                	mov    %ebx,%ecx
  800d3c:	e9 62 ff ff ff       	jmp    800ca3 <__umoddi3+0xb3>
