
obj/user/softint:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 09 00 00 00       	call   80003a <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	asm volatile("int $14");	// page fault
  800036:	cd 0e                	int    $0xe
}
  800038:	5d                   	pop    %ebp
  800039:	c3                   	ret    

0080003a <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80003a:	55                   	push   %ebp
  80003b:	89 e5                	mov    %esp,%ebp
  80003d:	57                   	push   %edi
  80003e:	56                   	push   %esi
  80003f:	53                   	push   %ebx
  800040:	83 ec 0c             	sub    $0xc,%esp
  800043:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800046:	8b 75 0c             	mov    0xc(%ebp),%esi
	// set thisenv to point at our Env structure in envs[].
	// LAB 3: Your code here.
	//thisenv = 0;
	int32_t env_Index1 = (int32_t)sys_getenvid();
  800049:	e8 24 0a 00 00       	call   800a72 <sys_getenvid>
  80004e:	89 c7                	mov    %eax,%edi
	cprintf("printing env_Index1: %d\n", env_Index1);
  800050:	83 ec 08             	sub    $0x8,%esp
  800053:	50                   	push   %eax
  800054:	68 3c 0d 80 00       	push   $0x800d3c
  800059:	e8 0a 01 00 00       	call   800168 <cprintf>
	int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	cprintf("printing env_Index2: %d\n", env_Index2);
  80005e:	83 c4 08             	add    $0x8,%esp
  800061:	81 e7 ff 03 00 00    	and    $0x3ff,%edi
  800067:	57                   	push   %edi
  800068:	68 55 0d 80 00       	push   $0x800d55
  80006d:	e8 f6 00 00 00       	call   800168 <cprintf>

	thisenv = &envs[ENVX(sys_getenvid())];
  800072:	e8 fb 09 00 00       	call   800a72 <sys_getenvid>
  800077:	25 ff 03 00 00       	and    $0x3ff,%eax
  80007c:	8d 14 00             	lea    (%eax,%eax,1),%edx
  80007f:	01 d0                	add    %edx,%eax
  800081:	c1 e0 05             	shl    $0x5,%eax
  800084:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800089:	a3 04 10 80 00       	mov    %eax,0x801004
	cprintf("Addrs of thisenv (envs[0]) is: %p\n", thisenv);
  80008e:	83 c4 08             	add    $0x8,%esp
  800091:	50                   	push   %eax
  800092:	68 78 0d 80 00       	push   $0x800d78
  800097:	e8 cc 00 00 00       	call   800168 <cprintf>
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80009c:	83 c4 10             	add    $0x10,%esp
  80009f:	85 db                	test   %ebx,%ebx
  8000a1:	7e 07                	jle    8000aa <libmain+0x70>
		binaryname = argv[0];
  8000a3:	8b 06                	mov    (%esi),%eax
  8000a5:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  8000aa:	83 ec 08             	sub    $0x8,%esp
  8000ad:	56                   	push   %esi
  8000ae:	53                   	push   %ebx
  8000af:	e8 7f ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000b4:	e8 0b 00 00 00       	call   8000c4 <exit>
}
  8000b9:	83 c4 10             	add    $0x10,%esp
  8000bc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8000bf:	5b                   	pop    %ebx
  8000c0:	5e                   	pop    %esi
  8000c1:	5f                   	pop    %edi
  8000c2:	5d                   	pop    %ebp
  8000c3:	c3                   	ret    

008000c4 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000c4:	55                   	push   %ebp
  8000c5:	89 e5                	mov    %esp,%ebp
  8000c7:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000ca:	6a 00                	push   $0x0
  8000cc:	e8 60 09 00 00       	call   800a31 <sys_env_destroy>
}
  8000d1:	83 c4 10             	add    $0x10,%esp
  8000d4:	c9                   	leave  
  8000d5:	c3                   	ret    

008000d6 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000d6:	55                   	push   %ebp
  8000d7:	89 e5                	mov    %esp,%ebp
  8000d9:	53                   	push   %ebx
  8000da:	83 ec 04             	sub    $0x4,%esp
  8000dd:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000e0:	8b 13                	mov    (%ebx),%edx
  8000e2:	8d 42 01             	lea    0x1(%edx),%eax
  8000e5:	89 03                	mov    %eax,(%ebx)
  8000e7:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000ea:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8000ee:	3d ff 00 00 00       	cmp    $0xff,%eax
  8000f3:	75 1a                	jne    80010f <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8000f5:	83 ec 08             	sub    $0x8,%esp
  8000f8:	68 ff 00 00 00       	push   $0xff
  8000fd:	8d 43 08             	lea    0x8(%ebx),%eax
  800100:	50                   	push   %eax
  800101:	e8 ee 08 00 00       	call   8009f4 <sys_cputs>
		b->idx = 0;
  800106:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  80010c:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  80010f:	ff 43 04             	incl   0x4(%ebx)
}
  800112:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800115:	c9                   	leave  
  800116:	c3                   	ret    

00800117 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800117:	55                   	push   %ebp
  800118:	89 e5                	mov    %esp,%ebp
  80011a:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800120:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800127:	00 00 00 
	b.cnt = 0;
  80012a:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800131:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800134:	ff 75 0c             	pushl  0xc(%ebp)
  800137:	ff 75 08             	pushl  0x8(%ebp)
  80013a:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800140:	50                   	push   %eax
  800141:	68 d6 00 80 00       	push   $0x8000d6
  800146:	e8 51 01 00 00       	call   80029c <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80014b:	83 c4 08             	add    $0x8,%esp
  80014e:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800154:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80015a:	50                   	push   %eax
  80015b:	e8 94 08 00 00       	call   8009f4 <sys_cputs>

	return b.cnt;
}
  800160:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800166:	c9                   	leave  
  800167:	c3                   	ret    

00800168 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800168:	55                   	push   %ebp
  800169:	89 e5                	mov    %esp,%ebp
  80016b:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80016e:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800171:	50                   	push   %eax
  800172:	ff 75 08             	pushl  0x8(%ebp)
  800175:	e8 9d ff ff ff       	call   800117 <vcprintf>
	va_end(ap);

	return cnt;
}
  80017a:	c9                   	leave  
  80017b:	c3                   	ret    

0080017c <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80017c:	55                   	push   %ebp
  80017d:	89 e5                	mov    %esp,%ebp
  80017f:	57                   	push   %edi
  800180:	56                   	push   %esi
  800181:	53                   	push   %ebx
  800182:	83 ec 1c             	sub    $0x1c,%esp
  800185:	89 c7                	mov    %eax,%edi
  800187:	89 d6                	mov    %edx,%esi
  800189:	8b 45 08             	mov    0x8(%ebp),%eax
  80018c:	8b 55 0c             	mov    0xc(%ebp),%edx
  80018f:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800192:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800195:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800198:	bb 00 00 00 00       	mov    $0x0,%ebx
  80019d:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001a0:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001a3:	39 d3                	cmp    %edx,%ebx
  8001a5:	72 05                	jb     8001ac <printnum+0x30>
  8001a7:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001aa:	77 45                	ja     8001f1 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001ac:	83 ec 0c             	sub    $0xc,%esp
  8001af:	ff 75 18             	pushl  0x18(%ebp)
  8001b2:	8b 45 14             	mov    0x14(%ebp),%eax
  8001b5:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001b8:	53                   	push   %ebx
  8001b9:	ff 75 10             	pushl  0x10(%ebp)
  8001bc:	83 ec 08             	sub    $0x8,%esp
  8001bf:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001c2:	ff 75 e0             	pushl  -0x20(%ebp)
  8001c5:	ff 75 dc             	pushl  -0x24(%ebp)
  8001c8:	ff 75 d8             	pushl  -0x28(%ebp)
  8001cb:	e8 08 09 00 00       	call   800ad8 <__udivdi3>
  8001d0:	83 c4 18             	add    $0x18,%esp
  8001d3:	52                   	push   %edx
  8001d4:	50                   	push   %eax
  8001d5:	89 f2                	mov    %esi,%edx
  8001d7:	89 f8                	mov    %edi,%eax
  8001d9:	e8 9e ff ff ff       	call   80017c <printnum>
  8001de:	83 c4 20             	add    $0x20,%esp
  8001e1:	eb 16                	jmp    8001f9 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001e3:	83 ec 08             	sub    $0x8,%esp
  8001e6:	56                   	push   %esi
  8001e7:	ff 75 18             	pushl  0x18(%ebp)
  8001ea:	ff d7                	call   *%edi
  8001ec:	83 c4 10             	add    $0x10,%esp
  8001ef:	eb 03                	jmp    8001f4 <printnum+0x78>
  8001f1:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8001f4:	4b                   	dec    %ebx
  8001f5:	85 db                	test   %ebx,%ebx
  8001f7:	7f ea                	jg     8001e3 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8001f9:	83 ec 08             	sub    $0x8,%esp
  8001fc:	56                   	push   %esi
  8001fd:	83 ec 04             	sub    $0x4,%esp
  800200:	ff 75 e4             	pushl  -0x1c(%ebp)
  800203:	ff 75 e0             	pushl  -0x20(%ebp)
  800206:	ff 75 dc             	pushl  -0x24(%ebp)
  800209:	ff 75 d8             	pushl  -0x28(%ebp)
  80020c:	e8 d7 09 00 00       	call   800be8 <__umoddi3>
  800211:	83 c4 14             	add    $0x14,%esp
  800214:	0f be 80 9b 0d 80 00 	movsbl 0x800d9b(%eax),%eax
  80021b:	50                   	push   %eax
  80021c:	ff d7                	call   *%edi
}
  80021e:	83 c4 10             	add    $0x10,%esp
  800221:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800224:	5b                   	pop    %ebx
  800225:	5e                   	pop    %esi
  800226:	5f                   	pop    %edi
  800227:	5d                   	pop    %ebp
  800228:	c3                   	ret    

00800229 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800229:	55                   	push   %ebp
  80022a:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80022c:	83 fa 01             	cmp    $0x1,%edx
  80022f:	7e 0e                	jle    80023f <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800231:	8b 10                	mov    (%eax),%edx
  800233:	8d 4a 08             	lea    0x8(%edx),%ecx
  800236:	89 08                	mov    %ecx,(%eax)
  800238:	8b 02                	mov    (%edx),%eax
  80023a:	8b 52 04             	mov    0x4(%edx),%edx
  80023d:	eb 22                	jmp    800261 <getuint+0x38>
	else if (lflag)
  80023f:	85 d2                	test   %edx,%edx
  800241:	74 10                	je     800253 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800243:	8b 10                	mov    (%eax),%edx
  800245:	8d 4a 04             	lea    0x4(%edx),%ecx
  800248:	89 08                	mov    %ecx,(%eax)
  80024a:	8b 02                	mov    (%edx),%eax
  80024c:	ba 00 00 00 00       	mov    $0x0,%edx
  800251:	eb 0e                	jmp    800261 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800253:	8b 10                	mov    (%eax),%edx
  800255:	8d 4a 04             	lea    0x4(%edx),%ecx
  800258:	89 08                	mov    %ecx,(%eax)
  80025a:	8b 02                	mov    (%edx),%eax
  80025c:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800261:	5d                   	pop    %ebp
  800262:	c3                   	ret    

00800263 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800263:	55                   	push   %ebp
  800264:	89 e5                	mov    %esp,%ebp
  800266:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800269:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80026c:	8b 10                	mov    (%eax),%edx
  80026e:	3b 50 04             	cmp    0x4(%eax),%edx
  800271:	73 0a                	jae    80027d <sprintputch+0x1a>
		*b->buf++ = ch;
  800273:	8d 4a 01             	lea    0x1(%edx),%ecx
  800276:	89 08                	mov    %ecx,(%eax)
  800278:	8b 45 08             	mov    0x8(%ebp),%eax
  80027b:	88 02                	mov    %al,(%edx)
}
  80027d:	5d                   	pop    %ebp
  80027e:	c3                   	ret    

0080027f <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80027f:	55                   	push   %ebp
  800280:	89 e5                	mov    %esp,%ebp
  800282:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800285:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800288:	50                   	push   %eax
  800289:	ff 75 10             	pushl  0x10(%ebp)
  80028c:	ff 75 0c             	pushl  0xc(%ebp)
  80028f:	ff 75 08             	pushl  0x8(%ebp)
  800292:	e8 05 00 00 00       	call   80029c <vprintfmt>
	va_end(ap);
}
  800297:	83 c4 10             	add    $0x10,%esp
  80029a:	c9                   	leave  
  80029b:	c3                   	ret    

0080029c <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80029c:	55                   	push   %ebp
  80029d:	89 e5                	mov    %esp,%ebp
  80029f:	57                   	push   %edi
  8002a0:	56                   	push   %esi
  8002a1:	53                   	push   %ebx
  8002a2:	83 ec 2c             	sub    $0x2c,%esp
  8002a5:	8b 75 08             	mov    0x8(%ebp),%esi
  8002a8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002ab:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002ae:	eb 12                	jmp    8002c2 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002b0:	85 c0                	test   %eax,%eax
  8002b2:	0f 84 68 03 00 00    	je     800620 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002b8:	83 ec 08             	sub    $0x8,%esp
  8002bb:	53                   	push   %ebx
  8002bc:	50                   	push   %eax
  8002bd:	ff d6                	call   *%esi
  8002bf:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002c2:	47                   	inc    %edi
  8002c3:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002c7:	83 f8 25             	cmp    $0x25,%eax
  8002ca:	75 e4                	jne    8002b0 <vprintfmt+0x14>
  8002cc:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002d0:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002d7:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002de:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002e5:	ba 00 00 00 00       	mov    $0x0,%edx
  8002ea:	eb 07                	jmp    8002f3 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002ec:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8002ef:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002f3:	8d 47 01             	lea    0x1(%edi),%eax
  8002f6:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8002f9:	0f b6 0f             	movzbl (%edi),%ecx
  8002fc:	8a 07                	mov    (%edi),%al
  8002fe:	83 e8 23             	sub    $0x23,%eax
  800301:	3c 55                	cmp    $0x55,%al
  800303:	0f 87 fe 02 00 00    	ja     800607 <vprintfmt+0x36b>
  800309:	0f b6 c0             	movzbl %al,%eax
  80030c:	ff 24 85 28 0e 80 00 	jmp    *0x800e28(,%eax,4)
  800313:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800316:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80031a:	eb d7                	jmp    8002f3 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80031c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80031f:	b8 00 00 00 00       	mov    $0x0,%eax
  800324:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800327:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80032a:	01 c0                	add    %eax,%eax
  80032c:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800330:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800333:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800336:	83 fa 09             	cmp    $0x9,%edx
  800339:	77 34                	ja     80036f <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80033b:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80033c:	eb e9                	jmp    800327 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80033e:	8b 45 14             	mov    0x14(%ebp),%eax
  800341:	8d 48 04             	lea    0x4(%eax),%ecx
  800344:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800347:	8b 00                	mov    (%eax),%eax
  800349:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80034c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80034f:	eb 24                	jmp    800375 <vprintfmt+0xd9>
  800351:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800355:	79 07                	jns    80035e <vprintfmt+0xc2>
  800357:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80035e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800361:	eb 90                	jmp    8002f3 <vprintfmt+0x57>
  800363:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800366:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80036d:	eb 84                	jmp    8002f3 <vprintfmt+0x57>
  80036f:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800372:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800375:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800379:	0f 89 74 ff ff ff    	jns    8002f3 <vprintfmt+0x57>
				width = precision, precision = -1;
  80037f:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800382:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800385:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80038c:	e9 62 ff ff ff       	jmp    8002f3 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800391:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800392:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800395:	e9 59 ff ff ff       	jmp    8002f3 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80039a:	8b 45 14             	mov    0x14(%ebp),%eax
  80039d:	8d 50 04             	lea    0x4(%eax),%edx
  8003a0:	89 55 14             	mov    %edx,0x14(%ebp)
  8003a3:	83 ec 08             	sub    $0x8,%esp
  8003a6:	53                   	push   %ebx
  8003a7:	ff 30                	pushl  (%eax)
  8003a9:	ff d6                	call   *%esi
			break;
  8003ab:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ae:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003b1:	e9 0c ff ff ff       	jmp    8002c2 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003b6:	8b 45 14             	mov    0x14(%ebp),%eax
  8003b9:	8d 50 04             	lea    0x4(%eax),%edx
  8003bc:	89 55 14             	mov    %edx,0x14(%ebp)
  8003bf:	8b 00                	mov    (%eax),%eax
  8003c1:	85 c0                	test   %eax,%eax
  8003c3:	79 02                	jns    8003c7 <vprintfmt+0x12b>
  8003c5:	f7 d8                	neg    %eax
  8003c7:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003c9:	83 f8 06             	cmp    $0x6,%eax
  8003cc:	7f 0b                	jg     8003d9 <vprintfmt+0x13d>
  8003ce:	8b 04 85 80 0f 80 00 	mov    0x800f80(,%eax,4),%eax
  8003d5:	85 c0                	test   %eax,%eax
  8003d7:	75 18                	jne    8003f1 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003d9:	52                   	push   %edx
  8003da:	68 b3 0d 80 00       	push   $0x800db3
  8003df:	53                   	push   %ebx
  8003e0:	56                   	push   %esi
  8003e1:	e8 99 fe ff ff       	call   80027f <printfmt>
  8003e6:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003e9:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8003ec:	e9 d1 fe ff ff       	jmp    8002c2 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8003f1:	50                   	push   %eax
  8003f2:	68 bc 0d 80 00       	push   $0x800dbc
  8003f7:	53                   	push   %ebx
  8003f8:	56                   	push   %esi
  8003f9:	e8 81 fe ff ff       	call   80027f <printfmt>
  8003fe:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800401:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800404:	e9 b9 fe ff ff       	jmp    8002c2 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800409:	8b 45 14             	mov    0x14(%ebp),%eax
  80040c:	8d 50 04             	lea    0x4(%eax),%edx
  80040f:	89 55 14             	mov    %edx,0x14(%ebp)
  800412:	8b 38                	mov    (%eax),%edi
  800414:	85 ff                	test   %edi,%edi
  800416:	75 05                	jne    80041d <vprintfmt+0x181>
				p = "(null)";
  800418:	bf ac 0d 80 00       	mov    $0x800dac,%edi
			if (width > 0 && padc != '-')
  80041d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800421:	0f 8e 90 00 00 00    	jle    8004b7 <vprintfmt+0x21b>
  800427:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80042b:	0f 84 8e 00 00 00    	je     8004bf <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800431:	83 ec 08             	sub    $0x8,%esp
  800434:	ff 75 d0             	pushl  -0x30(%ebp)
  800437:	57                   	push   %edi
  800438:	e8 70 02 00 00       	call   8006ad <strnlen>
  80043d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800440:	29 c1                	sub    %eax,%ecx
  800442:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800445:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800448:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80044c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80044f:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800452:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800454:	eb 0d                	jmp    800463 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800456:	83 ec 08             	sub    $0x8,%esp
  800459:	53                   	push   %ebx
  80045a:	ff 75 e4             	pushl  -0x1c(%ebp)
  80045d:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80045f:	4f                   	dec    %edi
  800460:	83 c4 10             	add    $0x10,%esp
  800463:	85 ff                	test   %edi,%edi
  800465:	7f ef                	jg     800456 <vprintfmt+0x1ba>
  800467:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80046a:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80046d:	89 c8                	mov    %ecx,%eax
  80046f:	85 c9                	test   %ecx,%ecx
  800471:	79 05                	jns    800478 <vprintfmt+0x1dc>
  800473:	b8 00 00 00 00       	mov    $0x0,%eax
  800478:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80047b:	29 c1                	sub    %eax,%ecx
  80047d:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800480:	89 75 08             	mov    %esi,0x8(%ebp)
  800483:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800486:	eb 3d                	jmp    8004c5 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800488:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80048c:	74 19                	je     8004a7 <vprintfmt+0x20b>
  80048e:	0f be c0             	movsbl %al,%eax
  800491:	83 e8 20             	sub    $0x20,%eax
  800494:	83 f8 5e             	cmp    $0x5e,%eax
  800497:	76 0e                	jbe    8004a7 <vprintfmt+0x20b>
					putch('?', putdat);
  800499:	83 ec 08             	sub    $0x8,%esp
  80049c:	53                   	push   %ebx
  80049d:	6a 3f                	push   $0x3f
  80049f:	ff 55 08             	call   *0x8(%ebp)
  8004a2:	83 c4 10             	add    $0x10,%esp
  8004a5:	eb 0b                	jmp    8004b2 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004a7:	83 ec 08             	sub    $0x8,%esp
  8004aa:	53                   	push   %ebx
  8004ab:	52                   	push   %edx
  8004ac:	ff 55 08             	call   *0x8(%ebp)
  8004af:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004b2:	ff 4d e4             	decl   -0x1c(%ebp)
  8004b5:	eb 0e                	jmp    8004c5 <vprintfmt+0x229>
  8004b7:	89 75 08             	mov    %esi,0x8(%ebp)
  8004ba:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004bd:	eb 06                	jmp    8004c5 <vprintfmt+0x229>
  8004bf:	89 75 08             	mov    %esi,0x8(%ebp)
  8004c2:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004c5:	47                   	inc    %edi
  8004c6:	8a 47 ff             	mov    -0x1(%edi),%al
  8004c9:	0f be d0             	movsbl %al,%edx
  8004cc:	85 d2                	test   %edx,%edx
  8004ce:	74 1d                	je     8004ed <vprintfmt+0x251>
  8004d0:	85 f6                	test   %esi,%esi
  8004d2:	78 b4                	js     800488 <vprintfmt+0x1ec>
  8004d4:	4e                   	dec    %esi
  8004d5:	79 b1                	jns    800488 <vprintfmt+0x1ec>
  8004d7:	8b 75 08             	mov    0x8(%ebp),%esi
  8004da:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004dd:	eb 14                	jmp    8004f3 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004df:	83 ec 08             	sub    $0x8,%esp
  8004e2:	53                   	push   %ebx
  8004e3:	6a 20                	push   $0x20
  8004e5:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004e7:	4f                   	dec    %edi
  8004e8:	83 c4 10             	add    $0x10,%esp
  8004eb:	eb 06                	jmp    8004f3 <vprintfmt+0x257>
  8004ed:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004f0:	8b 75 08             	mov    0x8(%ebp),%esi
  8004f3:	85 ff                	test   %edi,%edi
  8004f5:	7f e8                	jg     8004df <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004f7:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004fa:	e9 c3 fd ff ff       	jmp    8002c2 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8004ff:	83 fa 01             	cmp    $0x1,%edx
  800502:	7e 16                	jle    80051a <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800504:	8b 45 14             	mov    0x14(%ebp),%eax
  800507:	8d 50 08             	lea    0x8(%eax),%edx
  80050a:	89 55 14             	mov    %edx,0x14(%ebp)
  80050d:	8b 50 04             	mov    0x4(%eax),%edx
  800510:	8b 00                	mov    (%eax),%eax
  800512:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800515:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800518:	eb 32                	jmp    80054c <vprintfmt+0x2b0>
	else if (lflag)
  80051a:	85 d2                	test   %edx,%edx
  80051c:	74 18                	je     800536 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  80051e:	8b 45 14             	mov    0x14(%ebp),%eax
  800521:	8d 50 04             	lea    0x4(%eax),%edx
  800524:	89 55 14             	mov    %edx,0x14(%ebp)
  800527:	8b 00                	mov    (%eax),%eax
  800529:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80052c:	89 c1                	mov    %eax,%ecx
  80052e:	c1 f9 1f             	sar    $0x1f,%ecx
  800531:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800534:	eb 16                	jmp    80054c <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800536:	8b 45 14             	mov    0x14(%ebp),%eax
  800539:	8d 50 04             	lea    0x4(%eax),%edx
  80053c:	89 55 14             	mov    %edx,0x14(%ebp)
  80053f:	8b 00                	mov    (%eax),%eax
  800541:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800544:	89 c1                	mov    %eax,%ecx
  800546:	c1 f9 1f             	sar    $0x1f,%ecx
  800549:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80054c:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80054f:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800552:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800556:	79 76                	jns    8005ce <vprintfmt+0x332>
				putch('-', putdat);
  800558:	83 ec 08             	sub    $0x8,%esp
  80055b:	53                   	push   %ebx
  80055c:	6a 2d                	push   $0x2d
  80055e:	ff d6                	call   *%esi
				num = -(long long) num;
  800560:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800563:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800566:	f7 d8                	neg    %eax
  800568:	83 d2 00             	adc    $0x0,%edx
  80056b:	f7 da                	neg    %edx
  80056d:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800570:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800575:	eb 5c                	jmp    8005d3 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800577:	8d 45 14             	lea    0x14(%ebp),%eax
  80057a:	e8 aa fc ff ff       	call   800229 <getuint>
			base = 10;
  80057f:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800584:	eb 4d                	jmp    8005d3 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800586:	8d 45 14             	lea    0x14(%ebp),%eax
  800589:	e8 9b fc ff ff       	call   800229 <getuint>
			base = 8;
  80058e:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800593:	eb 3e                	jmp    8005d3 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800595:	83 ec 08             	sub    $0x8,%esp
  800598:	53                   	push   %ebx
  800599:	6a 30                	push   $0x30
  80059b:	ff d6                	call   *%esi
			putch('x', putdat);
  80059d:	83 c4 08             	add    $0x8,%esp
  8005a0:	53                   	push   %ebx
  8005a1:	6a 78                	push   $0x78
  8005a3:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005a5:	8b 45 14             	mov    0x14(%ebp),%eax
  8005a8:	8d 50 04             	lea    0x4(%eax),%edx
  8005ab:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005ae:	8b 00                	mov    (%eax),%eax
  8005b0:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005b5:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005b8:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005bd:	eb 14                	jmp    8005d3 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005bf:	8d 45 14             	lea    0x14(%ebp),%eax
  8005c2:	e8 62 fc ff ff       	call   800229 <getuint>
			base = 16;
  8005c7:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005cc:	eb 05                	jmp    8005d3 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005ce:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005d3:	83 ec 0c             	sub    $0xc,%esp
  8005d6:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005da:	57                   	push   %edi
  8005db:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005de:	51                   	push   %ecx
  8005df:	52                   	push   %edx
  8005e0:	50                   	push   %eax
  8005e1:	89 da                	mov    %ebx,%edx
  8005e3:	89 f0                	mov    %esi,%eax
  8005e5:	e8 92 fb ff ff       	call   80017c <printnum>
			break;
  8005ea:	83 c4 20             	add    $0x20,%esp
  8005ed:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005f0:	e9 cd fc ff ff       	jmp    8002c2 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8005f5:	83 ec 08             	sub    $0x8,%esp
  8005f8:	53                   	push   %ebx
  8005f9:	51                   	push   %ecx
  8005fa:	ff d6                	call   *%esi
			break;
  8005fc:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005ff:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800602:	e9 bb fc ff ff       	jmp    8002c2 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800607:	83 ec 08             	sub    $0x8,%esp
  80060a:	53                   	push   %ebx
  80060b:	6a 25                	push   $0x25
  80060d:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  80060f:	83 c4 10             	add    $0x10,%esp
  800612:	eb 01                	jmp    800615 <vprintfmt+0x379>
  800614:	4f                   	dec    %edi
  800615:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800619:	75 f9                	jne    800614 <vprintfmt+0x378>
  80061b:	e9 a2 fc ff ff       	jmp    8002c2 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800620:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800623:	5b                   	pop    %ebx
  800624:	5e                   	pop    %esi
  800625:	5f                   	pop    %edi
  800626:	5d                   	pop    %ebp
  800627:	c3                   	ret    

00800628 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800628:	55                   	push   %ebp
  800629:	89 e5                	mov    %esp,%ebp
  80062b:	83 ec 18             	sub    $0x18,%esp
  80062e:	8b 45 08             	mov    0x8(%ebp),%eax
  800631:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800634:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800637:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80063b:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  80063e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800645:	85 c0                	test   %eax,%eax
  800647:	74 26                	je     80066f <vsnprintf+0x47>
  800649:	85 d2                	test   %edx,%edx
  80064b:	7e 29                	jle    800676 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80064d:	ff 75 14             	pushl  0x14(%ebp)
  800650:	ff 75 10             	pushl  0x10(%ebp)
  800653:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800656:	50                   	push   %eax
  800657:	68 63 02 80 00       	push   $0x800263
  80065c:	e8 3b fc ff ff       	call   80029c <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800661:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800664:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800667:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80066a:	83 c4 10             	add    $0x10,%esp
  80066d:	eb 0c                	jmp    80067b <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80066f:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800674:	eb 05                	jmp    80067b <vsnprintf+0x53>
  800676:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80067b:	c9                   	leave  
  80067c:	c3                   	ret    

0080067d <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80067d:	55                   	push   %ebp
  80067e:	89 e5                	mov    %esp,%ebp
  800680:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800683:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800686:	50                   	push   %eax
  800687:	ff 75 10             	pushl  0x10(%ebp)
  80068a:	ff 75 0c             	pushl  0xc(%ebp)
  80068d:	ff 75 08             	pushl  0x8(%ebp)
  800690:	e8 93 ff ff ff       	call   800628 <vsnprintf>
	va_end(ap);

	return rc;
}
  800695:	c9                   	leave  
  800696:	c3                   	ret    

00800697 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800697:	55                   	push   %ebp
  800698:	89 e5                	mov    %esp,%ebp
  80069a:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  80069d:	b8 00 00 00 00       	mov    $0x0,%eax
  8006a2:	eb 01                	jmp    8006a5 <strlen+0xe>
		n++;
  8006a4:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006a5:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006a9:	75 f9                	jne    8006a4 <strlen+0xd>
		n++;
	return n;
}
  8006ab:	5d                   	pop    %ebp
  8006ac:	c3                   	ret    

008006ad <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006ad:	55                   	push   %ebp
  8006ae:	89 e5                	mov    %esp,%ebp
  8006b0:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006b3:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006b6:	ba 00 00 00 00       	mov    $0x0,%edx
  8006bb:	eb 01                	jmp    8006be <strnlen+0x11>
		n++;
  8006bd:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006be:	39 c2                	cmp    %eax,%edx
  8006c0:	74 08                	je     8006ca <strnlen+0x1d>
  8006c2:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006c6:	75 f5                	jne    8006bd <strnlen+0x10>
  8006c8:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006ca:	5d                   	pop    %ebp
  8006cb:	c3                   	ret    

008006cc <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006cc:	55                   	push   %ebp
  8006cd:	89 e5                	mov    %esp,%ebp
  8006cf:	53                   	push   %ebx
  8006d0:	8b 45 08             	mov    0x8(%ebp),%eax
  8006d3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006d6:	89 c2                	mov    %eax,%edx
  8006d8:	42                   	inc    %edx
  8006d9:	41                   	inc    %ecx
  8006da:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006dd:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006e0:	84 db                	test   %bl,%bl
  8006e2:	75 f4                	jne    8006d8 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006e4:	5b                   	pop    %ebx
  8006e5:	5d                   	pop    %ebp
  8006e6:	c3                   	ret    

008006e7 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006e7:	55                   	push   %ebp
  8006e8:	89 e5                	mov    %esp,%ebp
  8006ea:	53                   	push   %ebx
  8006eb:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8006ee:	53                   	push   %ebx
  8006ef:	e8 a3 ff ff ff       	call   800697 <strlen>
  8006f4:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8006f7:	ff 75 0c             	pushl  0xc(%ebp)
  8006fa:	01 d8                	add    %ebx,%eax
  8006fc:	50                   	push   %eax
  8006fd:	e8 ca ff ff ff       	call   8006cc <strcpy>
	return dst;
}
  800702:	89 d8                	mov    %ebx,%eax
  800704:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800707:	c9                   	leave  
  800708:	c3                   	ret    

00800709 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800709:	55                   	push   %ebp
  80070a:	89 e5                	mov    %esp,%ebp
  80070c:	56                   	push   %esi
  80070d:	53                   	push   %ebx
  80070e:	8b 75 08             	mov    0x8(%ebp),%esi
  800711:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800714:	89 f3                	mov    %esi,%ebx
  800716:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800719:	89 f2                	mov    %esi,%edx
  80071b:	eb 0c                	jmp    800729 <strncpy+0x20>
		*dst++ = *src;
  80071d:	42                   	inc    %edx
  80071e:	8a 01                	mov    (%ecx),%al
  800720:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800723:	80 39 01             	cmpb   $0x1,(%ecx)
  800726:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800729:	39 da                	cmp    %ebx,%edx
  80072b:	75 f0                	jne    80071d <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  80072d:	89 f0                	mov    %esi,%eax
  80072f:	5b                   	pop    %ebx
  800730:	5e                   	pop    %esi
  800731:	5d                   	pop    %ebp
  800732:	c3                   	ret    

00800733 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800733:	55                   	push   %ebp
  800734:	89 e5                	mov    %esp,%ebp
  800736:	56                   	push   %esi
  800737:	53                   	push   %ebx
  800738:	8b 75 08             	mov    0x8(%ebp),%esi
  80073b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80073e:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800741:	85 c0                	test   %eax,%eax
  800743:	74 1e                	je     800763 <strlcpy+0x30>
  800745:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800749:	89 f2                	mov    %esi,%edx
  80074b:	eb 05                	jmp    800752 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  80074d:	42                   	inc    %edx
  80074e:	41                   	inc    %ecx
  80074f:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800752:	39 c2                	cmp    %eax,%edx
  800754:	74 08                	je     80075e <strlcpy+0x2b>
  800756:	8a 19                	mov    (%ecx),%bl
  800758:	84 db                	test   %bl,%bl
  80075a:	75 f1                	jne    80074d <strlcpy+0x1a>
  80075c:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  80075e:	c6 00 00             	movb   $0x0,(%eax)
  800761:	eb 02                	jmp    800765 <strlcpy+0x32>
  800763:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800765:	29 f0                	sub    %esi,%eax
}
  800767:	5b                   	pop    %ebx
  800768:	5e                   	pop    %esi
  800769:	5d                   	pop    %ebp
  80076a:	c3                   	ret    

0080076b <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80076b:	55                   	push   %ebp
  80076c:	89 e5                	mov    %esp,%ebp
  80076e:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800771:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800774:	eb 02                	jmp    800778 <strcmp+0xd>
		p++, q++;
  800776:	41                   	inc    %ecx
  800777:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800778:	8a 01                	mov    (%ecx),%al
  80077a:	84 c0                	test   %al,%al
  80077c:	74 04                	je     800782 <strcmp+0x17>
  80077e:	3a 02                	cmp    (%edx),%al
  800780:	74 f4                	je     800776 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800782:	0f b6 c0             	movzbl %al,%eax
  800785:	0f b6 12             	movzbl (%edx),%edx
  800788:	29 d0                	sub    %edx,%eax
}
  80078a:	5d                   	pop    %ebp
  80078b:	c3                   	ret    

0080078c <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  80078c:	55                   	push   %ebp
  80078d:	89 e5                	mov    %esp,%ebp
  80078f:	53                   	push   %ebx
  800790:	8b 45 08             	mov    0x8(%ebp),%eax
  800793:	8b 55 0c             	mov    0xc(%ebp),%edx
  800796:	89 c3                	mov    %eax,%ebx
  800798:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  80079b:	eb 02                	jmp    80079f <strncmp+0x13>
		n--, p++, q++;
  80079d:	40                   	inc    %eax
  80079e:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  80079f:	39 d8                	cmp    %ebx,%eax
  8007a1:	74 14                	je     8007b7 <strncmp+0x2b>
  8007a3:	8a 08                	mov    (%eax),%cl
  8007a5:	84 c9                	test   %cl,%cl
  8007a7:	74 04                	je     8007ad <strncmp+0x21>
  8007a9:	3a 0a                	cmp    (%edx),%cl
  8007ab:	74 f0                	je     80079d <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007ad:	0f b6 00             	movzbl (%eax),%eax
  8007b0:	0f b6 12             	movzbl (%edx),%edx
  8007b3:	29 d0                	sub    %edx,%eax
  8007b5:	eb 05                	jmp    8007bc <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007b7:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007bc:	5b                   	pop    %ebx
  8007bd:	5d                   	pop    %ebp
  8007be:	c3                   	ret    

008007bf <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007bf:	55                   	push   %ebp
  8007c0:	89 e5                	mov    %esp,%ebp
  8007c2:	8b 45 08             	mov    0x8(%ebp),%eax
  8007c5:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007c8:	eb 05                	jmp    8007cf <strchr+0x10>
		if (*s == c)
  8007ca:	38 ca                	cmp    %cl,%dl
  8007cc:	74 0c                	je     8007da <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007ce:	40                   	inc    %eax
  8007cf:	8a 10                	mov    (%eax),%dl
  8007d1:	84 d2                	test   %dl,%dl
  8007d3:	75 f5                	jne    8007ca <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007d5:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007da:	5d                   	pop    %ebp
  8007db:	c3                   	ret    

008007dc <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007dc:	55                   	push   %ebp
  8007dd:	89 e5                	mov    %esp,%ebp
  8007df:	8b 45 08             	mov    0x8(%ebp),%eax
  8007e2:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007e5:	eb 05                	jmp    8007ec <strfind+0x10>
		if (*s == c)
  8007e7:	38 ca                	cmp    %cl,%dl
  8007e9:	74 07                	je     8007f2 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007eb:	40                   	inc    %eax
  8007ec:	8a 10                	mov    (%eax),%dl
  8007ee:	84 d2                	test   %dl,%dl
  8007f0:	75 f5                	jne    8007e7 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8007f2:	5d                   	pop    %ebp
  8007f3:	c3                   	ret    

008007f4 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8007f4:	55                   	push   %ebp
  8007f5:	89 e5                	mov    %esp,%ebp
  8007f7:	57                   	push   %edi
  8007f8:	56                   	push   %esi
  8007f9:	53                   	push   %ebx
  8007fa:	8b 7d 08             	mov    0x8(%ebp),%edi
  8007fd:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800800:	85 c9                	test   %ecx,%ecx
  800802:	74 36                	je     80083a <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800804:	f7 c7 03 00 00 00    	test   $0x3,%edi
  80080a:	75 28                	jne    800834 <memset+0x40>
  80080c:	f6 c1 03             	test   $0x3,%cl
  80080f:	75 23                	jne    800834 <memset+0x40>
		c &= 0xFF;
  800811:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800815:	89 d3                	mov    %edx,%ebx
  800817:	c1 e3 08             	shl    $0x8,%ebx
  80081a:	89 d6                	mov    %edx,%esi
  80081c:	c1 e6 18             	shl    $0x18,%esi
  80081f:	89 d0                	mov    %edx,%eax
  800821:	c1 e0 10             	shl    $0x10,%eax
  800824:	09 f0                	or     %esi,%eax
  800826:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800828:	89 d8                	mov    %ebx,%eax
  80082a:	09 d0                	or     %edx,%eax
  80082c:	c1 e9 02             	shr    $0x2,%ecx
  80082f:	fc                   	cld    
  800830:	f3 ab                	rep stos %eax,%es:(%edi)
  800832:	eb 06                	jmp    80083a <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800834:	8b 45 0c             	mov    0xc(%ebp),%eax
  800837:	fc                   	cld    
  800838:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  80083a:	89 f8                	mov    %edi,%eax
  80083c:	5b                   	pop    %ebx
  80083d:	5e                   	pop    %esi
  80083e:	5f                   	pop    %edi
  80083f:	5d                   	pop    %ebp
  800840:	c3                   	ret    

00800841 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800841:	55                   	push   %ebp
  800842:	89 e5                	mov    %esp,%ebp
  800844:	57                   	push   %edi
  800845:	56                   	push   %esi
  800846:	8b 45 08             	mov    0x8(%ebp),%eax
  800849:	8b 75 0c             	mov    0xc(%ebp),%esi
  80084c:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  80084f:	39 c6                	cmp    %eax,%esi
  800851:	73 33                	jae    800886 <memmove+0x45>
  800853:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800856:	39 d0                	cmp    %edx,%eax
  800858:	73 2c                	jae    800886 <memmove+0x45>
		s += n;
		d += n;
  80085a:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80085d:	89 d6                	mov    %edx,%esi
  80085f:	09 fe                	or     %edi,%esi
  800861:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800867:	75 13                	jne    80087c <memmove+0x3b>
  800869:	f6 c1 03             	test   $0x3,%cl
  80086c:	75 0e                	jne    80087c <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  80086e:	83 ef 04             	sub    $0x4,%edi
  800871:	8d 72 fc             	lea    -0x4(%edx),%esi
  800874:	c1 e9 02             	shr    $0x2,%ecx
  800877:	fd                   	std    
  800878:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80087a:	eb 07                	jmp    800883 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  80087c:	4f                   	dec    %edi
  80087d:	8d 72 ff             	lea    -0x1(%edx),%esi
  800880:	fd                   	std    
  800881:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800883:	fc                   	cld    
  800884:	eb 1d                	jmp    8008a3 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800886:	89 f2                	mov    %esi,%edx
  800888:	09 c2                	or     %eax,%edx
  80088a:	f6 c2 03             	test   $0x3,%dl
  80088d:	75 0f                	jne    80089e <memmove+0x5d>
  80088f:	f6 c1 03             	test   $0x3,%cl
  800892:	75 0a                	jne    80089e <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800894:	c1 e9 02             	shr    $0x2,%ecx
  800897:	89 c7                	mov    %eax,%edi
  800899:	fc                   	cld    
  80089a:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80089c:	eb 05                	jmp    8008a3 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  80089e:	89 c7                	mov    %eax,%edi
  8008a0:	fc                   	cld    
  8008a1:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008a3:	5e                   	pop    %esi
  8008a4:	5f                   	pop    %edi
  8008a5:	5d                   	pop    %ebp
  8008a6:	c3                   	ret    

008008a7 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008a7:	55                   	push   %ebp
  8008a8:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008aa:	ff 75 10             	pushl  0x10(%ebp)
  8008ad:	ff 75 0c             	pushl  0xc(%ebp)
  8008b0:	ff 75 08             	pushl  0x8(%ebp)
  8008b3:	e8 89 ff ff ff       	call   800841 <memmove>
}
  8008b8:	c9                   	leave  
  8008b9:	c3                   	ret    

008008ba <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008ba:	55                   	push   %ebp
  8008bb:	89 e5                	mov    %esp,%ebp
  8008bd:	56                   	push   %esi
  8008be:	53                   	push   %ebx
  8008bf:	8b 45 08             	mov    0x8(%ebp),%eax
  8008c2:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008c5:	89 c6                	mov    %eax,%esi
  8008c7:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008ca:	eb 14                	jmp    8008e0 <memcmp+0x26>
		if (*s1 != *s2)
  8008cc:	8a 08                	mov    (%eax),%cl
  8008ce:	8a 1a                	mov    (%edx),%bl
  8008d0:	38 d9                	cmp    %bl,%cl
  8008d2:	74 0a                	je     8008de <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008d4:	0f b6 c1             	movzbl %cl,%eax
  8008d7:	0f b6 db             	movzbl %bl,%ebx
  8008da:	29 d8                	sub    %ebx,%eax
  8008dc:	eb 0b                	jmp    8008e9 <memcmp+0x2f>
		s1++, s2++;
  8008de:	40                   	inc    %eax
  8008df:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008e0:	39 f0                	cmp    %esi,%eax
  8008e2:	75 e8                	jne    8008cc <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008e4:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008e9:	5b                   	pop    %ebx
  8008ea:	5e                   	pop    %esi
  8008eb:	5d                   	pop    %ebp
  8008ec:	c3                   	ret    

008008ed <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8008ed:	55                   	push   %ebp
  8008ee:	89 e5                	mov    %esp,%ebp
  8008f0:	53                   	push   %ebx
  8008f1:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8008f4:	89 c1                	mov    %eax,%ecx
  8008f6:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8008f9:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008fd:	eb 08                	jmp    800907 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8008ff:	0f b6 10             	movzbl (%eax),%edx
  800902:	39 da                	cmp    %ebx,%edx
  800904:	74 05                	je     80090b <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800906:	40                   	inc    %eax
  800907:	39 c8                	cmp    %ecx,%eax
  800909:	72 f4                	jb     8008ff <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  80090b:	5b                   	pop    %ebx
  80090c:	5d                   	pop    %ebp
  80090d:	c3                   	ret    

0080090e <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  80090e:	55                   	push   %ebp
  80090f:	89 e5                	mov    %esp,%ebp
  800911:	57                   	push   %edi
  800912:	56                   	push   %esi
  800913:	53                   	push   %ebx
  800914:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800917:	eb 01                	jmp    80091a <strtol+0xc>
		s++;
  800919:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80091a:	8a 01                	mov    (%ecx),%al
  80091c:	3c 20                	cmp    $0x20,%al
  80091e:	74 f9                	je     800919 <strtol+0xb>
  800920:	3c 09                	cmp    $0x9,%al
  800922:	74 f5                	je     800919 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800924:	3c 2b                	cmp    $0x2b,%al
  800926:	75 08                	jne    800930 <strtol+0x22>
		s++;
  800928:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800929:	bf 00 00 00 00       	mov    $0x0,%edi
  80092e:	eb 11                	jmp    800941 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800930:	3c 2d                	cmp    $0x2d,%al
  800932:	75 08                	jne    80093c <strtol+0x2e>
		s++, neg = 1;
  800934:	41                   	inc    %ecx
  800935:	bf 01 00 00 00       	mov    $0x1,%edi
  80093a:	eb 05                	jmp    800941 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  80093c:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800941:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800945:	0f 84 87 00 00 00    	je     8009d2 <strtol+0xc4>
  80094b:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  80094f:	75 27                	jne    800978 <strtol+0x6a>
  800951:	80 39 30             	cmpb   $0x30,(%ecx)
  800954:	75 22                	jne    800978 <strtol+0x6a>
  800956:	e9 88 00 00 00       	jmp    8009e3 <strtol+0xd5>
		s += 2, base = 16;
  80095b:	83 c1 02             	add    $0x2,%ecx
  80095e:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800965:	eb 11                	jmp    800978 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800967:	41                   	inc    %ecx
  800968:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  80096f:	eb 07                	jmp    800978 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800971:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800978:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  80097d:	8a 11                	mov    (%ecx),%dl
  80097f:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800982:	80 fb 09             	cmp    $0x9,%bl
  800985:	77 08                	ja     80098f <strtol+0x81>
			dig = *s - '0';
  800987:	0f be d2             	movsbl %dl,%edx
  80098a:	83 ea 30             	sub    $0x30,%edx
  80098d:	eb 22                	jmp    8009b1 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  80098f:	8d 72 9f             	lea    -0x61(%edx),%esi
  800992:	89 f3                	mov    %esi,%ebx
  800994:	80 fb 19             	cmp    $0x19,%bl
  800997:	77 08                	ja     8009a1 <strtol+0x93>
			dig = *s - 'a' + 10;
  800999:	0f be d2             	movsbl %dl,%edx
  80099c:	83 ea 57             	sub    $0x57,%edx
  80099f:	eb 10                	jmp    8009b1 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009a1:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009a4:	89 f3                	mov    %esi,%ebx
  8009a6:	80 fb 19             	cmp    $0x19,%bl
  8009a9:	77 14                	ja     8009bf <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009ab:	0f be d2             	movsbl %dl,%edx
  8009ae:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009b1:	3b 55 10             	cmp    0x10(%ebp),%edx
  8009b4:	7d 09                	jge    8009bf <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009b6:	41                   	inc    %ecx
  8009b7:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009bb:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009bd:	eb be                	jmp    80097d <strtol+0x6f>

	if (endptr)
  8009bf:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009c3:	74 05                	je     8009ca <strtol+0xbc>
		*endptr = (char *) s;
  8009c5:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009c8:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009ca:	85 ff                	test   %edi,%edi
  8009cc:	74 21                	je     8009ef <strtol+0xe1>
  8009ce:	f7 d8                	neg    %eax
  8009d0:	eb 1d                	jmp    8009ef <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009d2:	80 39 30             	cmpb   $0x30,(%ecx)
  8009d5:	75 9a                	jne    800971 <strtol+0x63>
  8009d7:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009db:	0f 84 7a ff ff ff    	je     80095b <strtol+0x4d>
  8009e1:	eb 84                	jmp    800967 <strtol+0x59>
  8009e3:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009e7:	0f 84 6e ff ff ff    	je     80095b <strtol+0x4d>
  8009ed:	eb 89                	jmp    800978 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  8009ef:	5b                   	pop    %ebx
  8009f0:	5e                   	pop    %esi
  8009f1:	5f                   	pop    %edi
  8009f2:	5d                   	pop    %ebp
  8009f3:	c3                   	ret    

008009f4 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8009f4:	55                   	push   %ebp
  8009f5:	89 e5                	mov    %esp,%ebp
  8009f7:	57                   	push   %edi
  8009f8:	56                   	push   %esi
  8009f9:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8009fa:	b8 00 00 00 00       	mov    $0x0,%eax
  8009ff:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a02:	8b 55 08             	mov    0x8(%ebp),%edx
  800a05:	89 c3                	mov    %eax,%ebx
  800a07:	89 c7                	mov    %eax,%edi
  800a09:	89 c6                	mov    %eax,%esi
  800a0b:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a0d:	5b                   	pop    %ebx
  800a0e:	5e                   	pop    %esi
  800a0f:	5f                   	pop    %edi
  800a10:	5d                   	pop    %ebp
  800a11:	c3                   	ret    

00800a12 <sys_cgetc>:

int
sys_cgetc(void)
{
  800a12:	55                   	push   %ebp
  800a13:	89 e5                	mov    %esp,%ebp
  800a15:	57                   	push   %edi
  800a16:	56                   	push   %esi
  800a17:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a18:	ba 00 00 00 00       	mov    $0x0,%edx
  800a1d:	b8 01 00 00 00       	mov    $0x1,%eax
  800a22:	89 d1                	mov    %edx,%ecx
  800a24:	89 d3                	mov    %edx,%ebx
  800a26:	89 d7                	mov    %edx,%edi
  800a28:	89 d6                	mov    %edx,%esi
  800a2a:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a2c:	5b                   	pop    %ebx
  800a2d:	5e                   	pop    %esi
  800a2e:	5f                   	pop    %edi
  800a2f:	5d                   	pop    %ebp
  800a30:	c3                   	ret    

00800a31 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a31:	55                   	push   %ebp
  800a32:	89 e5                	mov    %esp,%ebp
  800a34:	57                   	push   %edi
  800a35:	56                   	push   %esi
  800a36:	53                   	push   %ebx
  800a37:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a3a:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a3f:	b8 03 00 00 00       	mov    $0x3,%eax
  800a44:	8b 55 08             	mov    0x8(%ebp),%edx
  800a47:	89 cb                	mov    %ecx,%ebx
  800a49:	89 cf                	mov    %ecx,%edi
  800a4b:	89 ce                	mov    %ecx,%esi
  800a4d:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a4f:	85 c0                	test   %eax,%eax
  800a51:	7e 17                	jle    800a6a <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a53:	83 ec 0c             	sub    $0xc,%esp
  800a56:	50                   	push   %eax
  800a57:	6a 03                	push   $0x3
  800a59:	68 9c 0f 80 00       	push   $0x800f9c
  800a5e:	6a 23                	push   $0x23
  800a60:	68 b9 0f 80 00       	push   $0x800fb9
  800a65:	e8 27 00 00 00       	call   800a91 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a6a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a6d:	5b                   	pop    %ebx
  800a6e:	5e                   	pop    %esi
  800a6f:	5f                   	pop    %edi
  800a70:	5d                   	pop    %ebp
  800a71:	c3                   	ret    

00800a72 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a72:	55                   	push   %ebp
  800a73:	89 e5                	mov    %esp,%ebp
  800a75:	57                   	push   %edi
  800a76:	56                   	push   %esi
  800a77:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a78:	ba 00 00 00 00       	mov    $0x0,%edx
  800a7d:	b8 02 00 00 00       	mov    $0x2,%eax
  800a82:	89 d1                	mov    %edx,%ecx
  800a84:	89 d3                	mov    %edx,%ebx
  800a86:	89 d7                	mov    %edx,%edi
  800a88:	89 d6                	mov    %edx,%esi
  800a8a:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800a8c:	5b                   	pop    %ebx
  800a8d:	5e                   	pop    %esi
  800a8e:	5f                   	pop    %edi
  800a8f:	5d                   	pop    %ebp
  800a90:	c3                   	ret    

00800a91 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800a91:	55                   	push   %ebp
  800a92:	89 e5                	mov    %esp,%ebp
  800a94:	56                   	push   %esi
  800a95:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800a96:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800a99:	8b 35 00 10 80 00    	mov    0x801000,%esi
  800a9f:	e8 ce ff ff ff       	call   800a72 <sys_getenvid>
  800aa4:	83 ec 0c             	sub    $0xc,%esp
  800aa7:	ff 75 0c             	pushl  0xc(%ebp)
  800aaa:	ff 75 08             	pushl  0x8(%ebp)
  800aad:	56                   	push   %esi
  800aae:	50                   	push   %eax
  800aaf:	68 c8 0f 80 00       	push   $0x800fc8
  800ab4:	e8 af f6 ff ff       	call   800168 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800ab9:	83 c4 18             	add    $0x18,%esp
  800abc:	53                   	push   %ebx
  800abd:	ff 75 10             	pushl  0x10(%ebp)
  800ac0:	e8 52 f6 ff ff       	call   800117 <vcprintf>
	cprintf("\n");
  800ac5:	c7 04 24 53 0d 80 00 	movl   $0x800d53,(%esp)
  800acc:	e8 97 f6 ff ff       	call   800168 <cprintf>
  800ad1:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800ad4:	cc                   	int3   
  800ad5:	eb fd                	jmp    800ad4 <_panic+0x43>
  800ad7:	90                   	nop

00800ad8 <__udivdi3>:
  800ad8:	55                   	push   %ebp
  800ad9:	57                   	push   %edi
  800ada:	56                   	push   %esi
  800adb:	53                   	push   %ebx
  800adc:	83 ec 1c             	sub    $0x1c,%esp
  800adf:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ae3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800ae7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800aeb:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800aef:	89 ca                	mov    %ecx,%edx
  800af1:	89 f8                	mov    %edi,%eax
  800af3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800af7:	85 f6                	test   %esi,%esi
  800af9:	75 2d                	jne    800b28 <__udivdi3+0x50>
  800afb:	39 cf                	cmp    %ecx,%edi
  800afd:	77 65                	ja     800b64 <__udivdi3+0x8c>
  800aff:	89 fd                	mov    %edi,%ebp
  800b01:	85 ff                	test   %edi,%edi
  800b03:	75 0b                	jne    800b10 <__udivdi3+0x38>
  800b05:	b8 01 00 00 00       	mov    $0x1,%eax
  800b0a:	31 d2                	xor    %edx,%edx
  800b0c:	f7 f7                	div    %edi
  800b0e:	89 c5                	mov    %eax,%ebp
  800b10:	31 d2                	xor    %edx,%edx
  800b12:	89 c8                	mov    %ecx,%eax
  800b14:	f7 f5                	div    %ebp
  800b16:	89 c1                	mov    %eax,%ecx
  800b18:	89 d8                	mov    %ebx,%eax
  800b1a:	f7 f5                	div    %ebp
  800b1c:	89 cf                	mov    %ecx,%edi
  800b1e:	89 fa                	mov    %edi,%edx
  800b20:	83 c4 1c             	add    $0x1c,%esp
  800b23:	5b                   	pop    %ebx
  800b24:	5e                   	pop    %esi
  800b25:	5f                   	pop    %edi
  800b26:	5d                   	pop    %ebp
  800b27:	c3                   	ret    
  800b28:	39 ce                	cmp    %ecx,%esi
  800b2a:	77 28                	ja     800b54 <__udivdi3+0x7c>
  800b2c:	0f bd fe             	bsr    %esi,%edi
  800b2f:	83 f7 1f             	xor    $0x1f,%edi
  800b32:	75 40                	jne    800b74 <__udivdi3+0x9c>
  800b34:	39 ce                	cmp    %ecx,%esi
  800b36:	72 0a                	jb     800b42 <__udivdi3+0x6a>
  800b38:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b3c:	0f 87 9e 00 00 00    	ja     800be0 <__udivdi3+0x108>
  800b42:	b8 01 00 00 00       	mov    $0x1,%eax
  800b47:	89 fa                	mov    %edi,%edx
  800b49:	83 c4 1c             	add    $0x1c,%esp
  800b4c:	5b                   	pop    %ebx
  800b4d:	5e                   	pop    %esi
  800b4e:	5f                   	pop    %edi
  800b4f:	5d                   	pop    %ebp
  800b50:	c3                   	ret    
  800b51:	8d 76 00             	lea    0x0(%esi),%esi
  800b54:	31 ff                	xor    %edi,%edi
  800b56:	31 c0                	xor    %eax,%eax
  800b58:	89 fa                	mov    %edi,%edx
  800b5a:	83 c4 1c             	add    $0x1c,%esp
  800b5d:	5b                   	pop    %ebx
  800b5e:	5e                   	pop    %esi
  800b5f:	5f                   	pop    %edi
  800b60:	5d                   	pop    %ebp
  800b61:	c3                   	ret    
  800b62:	66 90                	xchg   %ax,%ax
  800b64:	89 d8                	mov    %ebx,%eax
  800b66:	f7 f7                	div    %edi
  800b68:	31 ff                	xor    %edi,%edi
  800b6a:	89 fa                	mov    %edi,%edx
  800b6c:	83 c4 1c             	add    $0x1c,%esp
  800b6f:	5b                   	pop    %ebx
  800b70:	5e                   	pop    %esi
  800b71:	5f                   	pop    %edi
  800b72:	5d                   	pop    %ebp
  800b73:	c3                   	ret    
  800b74:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b79:	89 eb                	mov    %ebp,%ebx
  800b7b:	29 fb                	sub    %edi,%ebx
  800b7d:	89 f9                	mov    %edi,%ecx
  800b7f:	d3 e6                	shl    %cl,%esi
  800b81:	89 c5                	mov    %eax,%ebp
  800b83:	88 d9                	mov    %bl,%cl
  800b85:	d3 ed                	shr    %cl,%ebp
  800b87:	89 e9                	mov    %ebp,%ecx
  800b89:	09 f1                	or     %esi,%ecx
  800b8b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b8f:	89 f9                	mov    %edi,%ecx
  800b91:	d3 e0                	shl    %cl,%eax
  800b93:	89 c5                	mov    %eax,%ebp
  800b95:	89 d6                	mov    %edx,%esi
  800b97:	88 d9                	mov    %bl,%cl
  800b99:	d3 ee                	shr    %cl,%esi
  800b9b:	89 f9                	mov    %edi,%ecx
  800b9d:	d3 e2                	shl    %cl,%edx
  800b9f:	8b 44 24 08          	mov    0x8(%esp),%eax
  800ba3:	88 d9                	mov    %bl,%cl
  800ba5:	d3 e8                	shr    %cl,%eax
  800ba7:	09 c2                	or     %eax,%edx
  800ba9:	89 d0                	mov    %edx,%eax
  800bab:	89 f2                	mov    %esi,%edx
  800bad:	f7 74 24 0c          	divl   0xc(%esp)
  800bb1:	89 d6                	mov    %edx,%esi
  800bb3:	89 c3                	mov    %eax,%ebx
  800bb5:	f7 e5                	mul    %ebp
  800bb7:	39 d6                	cmp    %edx,%esi
  800bb9:	72 19                	jb     800bd4 <__udivdi3+0xfc>
  800bbb:	74 0b                	je     800bc8 <__udivdi3+0xf0>
  800bbd:	89 d8                	mov    %ebx,%eax
  800bbf:	31 ff                	xor    %edi,%edi
  800bc1:	e9 58 ff ff ff       	jmp    800b1e <__udivdi3+0x46>
  800bc6:	66 90                	xchg   %ax,%ax
  800bc8:	8b 54 24 08          	mov    0x8(%esp),%edx
  800bcc:	89 f9                	mov    %edi,%ecx
  800bce:	d3 e2                	shl    %cl,%edx
  800bd0:	39 c2                	cmp    %eax,%edx
  800bd2:	73 e9                	jae    800bbd <__udivdi3+0xe5>
  800bd4:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800bd7:	31 ff                	xor    %edi,%edi
  800bd9:	e9 40 ff ff ff       	jmp    800b1e <__udivdi3+0x46>
  800bde:	66 90                	xchg   %ax,%ax
  800be0:	31 c0                	xor    %eax,%eax
  800be2:	e9 37 ff ff ff       	jmp    800b1e <__udivdi3+0x46>
  800be7:	90                   	nop

00800be8 <__umoddi3>:
  800be8:	55                   	push   %ebp
  800be9:	57                   	push   %edi
  800bea:	56                   	push   %esi
  800beb:	53                   	push   %ebx
  800bec:	83 ec 1c             	sub    $0x1c,%esp
  800bef:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bf3:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bf7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800bfb:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800bff:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800c03:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800c07:	89 f3                	mov    %esi,%ebx
  800c09:	89 fa                	mov    %edi,%edx
  800c0b:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800c0f:	89 34 24             	mov    %esi,(%esp)
  800c12:	85 c0                	test   %eax,%eax
  800c14:	75 1a                	jne    800c30 <__umoddi3+0x48>
  800c16:	39 f7                	cmp    %esi,%edi
  800c18:	0f 86 a2 00 00 00    	jbe    800cc0 <__umoddi3+0xd8>
  800c1e:	89 c8                	mov    %ecx,%eax
  800c20:	89 f2                	mov    %esi,%edx
  800c22:	f7 f7                	div    %edi
  800c24:	89 d0                	mov    %edx,%eax
  800c26:	31 d2                	xor    %edx,%edx
  800c28:	83 c4 1c             	add    $0x1c,%esp
  800c2b:	5b                   	pop    %ebx
  800c2c:	5e                   	pop    %esi
  800c2d:	5f                   	pop    %edi
  800c2e:	5d                   	pop    %ebp
  800c2f:	c3                   	ret    
  800c30:	39 f0                	cmp    %esi,%eax
  800c32:	0f 87 ac 00 00 00    	ja     800ce4 <__umoddi3+0xfc>
  800c38:	0f bd e8             	bsr    %eax,%ebp
  800c3b:	83 f5 1f             	xor    $0x1f,%ebp
  800c3e:	0f 84 ac 00 00 00    	je     800cf0 <__umoddi3+0x108>
  800c44:	bf 20 00 00 00       	mov    $0x20,%edi
  800c49:	29 ef                	sub    %ebp,%edi
  800c4b:	89 fe                	mov    %edi,%esi
  800c4d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c51:	89 e9                	mov    %ebp,%ecx
  800c53:	d3 e0                	shl    %cl,%eax
  800c55:	89 d7                	mov    %edx,%edi
  800c57:	89 f1                	mov    %esi,%ecx
  800c59:	d3 ef                	shr    %cl,%edi
  800c5b:	09 c7                	or     %eax,%edi
  800c5d:	89 e9                	mov    %ebp,%ecx
  800c5f:	d3 e2                	shl    %cl,%edx
  800c61:	89 14 24             	mov    %edx,(%esp)
  800c64:	89 d8                	mov    %ebx,%eax
  800c66:	d3 e0                	shl    %cl,%eax
  800c68:	89 c2                	mov    %eax,%edx
  800c6a:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c6e:	d3 e0                	shl    %cl,%eax
  800c70:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c74:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c78:	89 f1                	mov    %esi,%ecx
  800c7a:	d3 e8                	shr    %cl,%eax
  800c7c:	09 d0                	or     %edx,%eax
  800c7e:	d3 eb                	shr    %cl,%ebx
  800c80:	89 da                	mov    %ebx,%edx
  800c82:	f7 f7                	div    %edi
  800c84:	89 d3                	mov    %edx,%ebx
  800c86:	f7 24 24             	mull   (%esp)
  800c89:	89 c6                	mov    %eax,%esi
  800c8b:	89 d1                	mov    %edx,%ecx
  800c8d:	39 d3                	cmp    %edx,%ebx
  800c8f:	0f 82 87 00 00 00    	jb     800d1c <__umoddi3+0x134>
  800c95:	0f 84 91 00 00 00    	je     800d2c <__umoddi3+0x144>
  800c9b:	8b 54 24 04          	mov    0x4(%esp),%edx
  800c9f:	29 f2                	sub    %esi,%edx
  800ca1:	19 cb                	sbb    %ecx,%ebx
  800ca3:	89 d8                	mov    %ebx,%eax
  800ca5:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800ca9:	d3 e0                	shl    %cl,%eax
  800cab:	89 e9                	mov    %ebp,%ecx
  800cad:	d3 ea                	shr    %cl,%edx
  800caf:	09 d0                	or     %edx,%eax
  800cb1:	89 e9                	mov    %ebp,%ecx
  800cb3:	d3 eb                	shr    %cl,%ebx
  800cb5:	89 da                	mov    %ebx,%edx
  800cb7:	83 c4 1c             	add    $0x1c,%esp
  800cba:	5b                   	pop    %ebx
  800cbb:	5e                   	pop    %esi
  800cbc:	5f                   	pop    %edi
  800cbd:	5d                   	pop    %ebp
  800cbe:	c3                   	ret    
  800cbf:	90                   	nop
  800cc0:	89 fd                	mov    %edi,%ebp
  800cc2:	85 ff                	test   %edi,%edi
  800cc4:	75 0b                	jne    800cd1 <__umoddi3+0xe9>
  800cc6:	b8 01 00 00 00       	mov    $0x1,%eax
  800ccb:	31 d2                	xor    %edx,%edx
  800ccd:	f7 f7                	div    %edi
  800ccf:	89 c5                	mov    %eax,%ebp
  800cd1:	89 f0                	mov    %esi,%eax
  800cd3:	31 d2                	xor    %edx,%edx
  800cd5:	f7 f5                	div    %ebp
  800cd7:	89 c8                	mov    %ecx,%eax
  800cd9:	f7 f5                	div    %ebp
  800cdb:	89 d0                	mov    %edx,%eax
  800cdd:	e9 44 ff ff ff       	jmp    800c26 <__umoddi3+0x3e>
  800ce2:	66 90                	xchg   %ax,%ax
  800ce4:	89 c8                	mov    %ecx,%eax
  800ce6:	89 f2                	mov    %esi,%edx
  800ce8:	83 c4 1c             	add    $0x1c,%esp
  800ceb:	5b                   	pop    %ebx
  800cec:	5e                   	pop    %esi
  800ced:	5f                   	pop    %edi
  800cee:	5d                   	pop    %ebp
  800cef:	c3                   	ret    
  800cf0:	3b 04 24             	cmp    (%esp),%eax
  800cf3:	72 06                	jb     800cfb <__umoddi3+0x113>
  800cf5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800cf9:	77 0f                	ja     800d0a <__umoddi3+0x122>
  800cfb:	89 f2                	mov    %esi,%edx
  800cfd:	29 f9                	sub    %edi,%ecx
  800cff:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800d03:	89 14 24             	mov    %edx,(%esp)
  800d06:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800d0a:	8b 44 24 04          	mov    0x4(%esp),%eax
  800d0e:	8b 14 24             	mov    (%esp),%edx
  800d11:	83 c4 1c             	add    $0x1c,%esp
  800d14:	5b                   	pop    %ebx
  800d15:	5e                   	pop    %esi
  800d16:	5f                   	pop    %edi
  800d17:	5d                   	pop    %ebp
  800d18:	c3                   	ret    
  800d19:	8d 76 00             	lea    0x0(%esi),%esi
  800d1c:	2b 04 24             	sub    (%esp),%eax
  800d1f:	19 fa                	sbb    %edi,%edx
  800d21:	89 d1                	mov    %edx,%ecx
  800d23:	89 c6                	mov    %eax,%esi
  800d25:	e9 71 ff ff ff       	jmp    800c9b <__umoddi3+0xb3>
  800d2a:	66 90                	xchg   %ax,%ax
  800d2c:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d30:	72 ea                	jb     800d1c <__umoddi3+0x134>
  800d32:	89 d9                	mov    %ebx,%ecx
  800d34:	e9 62 ff ff ff       	jmp    800c9b <__umoddi3+0xb3>
