
obj/user/buggyhello:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 16 00 00 00       	call   800047 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	sys_cputs((char*)1, 1);
  800039:	6a 01                	push   $0x1
  80003b:	6a 01                	push   $0x1
  80003d:	e8 bf 09 00 00       	call   800a01 <sys_cputs>
}
  800042:	83 c4 10             	add    $0x10,%esp
  800045:	c9                   	leave  
  800046:	c3                   	ret    

00800047 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800047:	55                   	push   %ebp
  800048:	89 e5                	mov    %esp,%ebp
  80004a:	57                   	push   %edi
  80004b:	56                   	push   %esi
  80004c:	53                   	push   %ebx
  80004d:	83 ec 0c             	sub    $0xc,%esp
  800050:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800053:	8b 75 0c             	mov    0xc(%ebp),%esi
	// set thisenv to point at our Env structure in envs[].
	// LAB 3: Your code here.
	//thisenv = 0;
	int32_t env_Index1 = (int32_t)sys_getenvid();
  800056:	e8 24 0a 00 00       	call   800a7f <sys_getenvid>
  80005b:	89 c7                	mov    %eax,%edi
	cprintf("printing env_Index1: %d\n", env_Index1);
  80005d:	83 ec 08             	sub    $0x8,%esp
  800060:	50                   	push   %eax
  800061:	68 48 0d 80 00       	push   $0x800d48
  800066:	e8 0a 01 00 00       	call   800175 <cprintf>
	int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	cprintf("printing env_Index2: %d\n", env_Index2);
  80006b:	83 c4 08             	add    $0x8,%esp
  80006e:	81 e7 ff 03 00 00    	and    $0x3ff,%edi
  800074:	57                   	push   %edi
  800075:	68 61 0d 80 00       	push   $0x800d61
  80007a:	e8 f6 00 00 00       	call   800175 <cprintf>

	thisenv = &envs[ENVX(sys_getenvid())];
  80007f:	e8 fb 09 00 00       	call   800a7f <sys_getenvid>
  800084:	25 ff 03 00 00       	and    $0x3ff,%eax
  800089:	8d 14 00             	lea    (%eax,%eax,1),%edx
  80008c:	01 d0                	add    %edx,%eax
  80008e:	c1 e0 05             	shl    $0x5,%eax
  800091:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800096:	a3 04 10 80 00       	mov    %eax,0x801004
	cprintf("Addrs of thisenv (envs[0]) is: %p\n", thisenv);
  80009b:	83 c4 08             	add    $0x8,%esp
  80009e:	50                   	push   %eax
  80009f:	68 84 0d 80 00       	push   $0x800d84
  8000a4:	e8 cc 00 00 00       	call   800175 <cprintf>
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000a9:	83 c4 10             	add    $0x10,%esp
  8000ac:	85 db                	test   %ebx,%ebx
  8000ae:	7e 07                	jle    8000b7 <libmain+0x70>
		binaryname = argv[0];
  8000b0:	8b 06                	mov    (%esi),%eax
  8000b2:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  8000b7:	83 ec 08             	sub    $0x8,%esp
  8000ba:	56                   	push   %esi
  8000bb:	53                   	push   %ebx
  8000bc:	e8 72 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000c1:	e8 0b 00 00 00       	call   8000d1 <exit>
}
  8000c6:	83 c4 10             	add    $0x10,%esp
  8000c9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8000cc:	5b                   	pop    %ebx
  8000cd:	5e                   	pop    %esi
  8000ce:	5f                   	pop    %edi
  8000cf:	5d                   	pop    %ebp
  8000d0:	c3                   	ret    

008000d1 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000d1:	55                   	push   %ebp
  8000d2:	89 e5                	mov    %esp,%ebp
  8000d4:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000d7:	6a 00                	push   $0x0
  8000d9:	e8 60 09 00 00       	call   800a3e <sys_env_destroy>
}
  8000de:	83 c4 10             	add    $0x10,%esp
  8000e1:	c9                   	leave  
  8000e2:	c3                   	ret    

008000e3 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000e3:	55                   	push   %ebp
  8000e4:	89 e5                	mov    %esp,%ebp
  8000e6:	53                   	push   %ebx
  8000e7:	83 ec 04             	sub    $0x4,%esp
  8000ea:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000ed:	8b 13                	mov    (%ebx),%edx
  8000ef:	8d 42 01             	lea    0x1(%edx),%eax
  8000f2:	89 03                	mov    %eax,(%ebx)
  8000f4:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000f7:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8000fb:	3d ff 00 00 00       	cmp    $0xff,%eax
  800100:	75 1a                	jne    80011c <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800102:	83 ec 08             	sub    $0x8,%esp
  800105:	68 ff 00 00 00       	push   $0xff
  80010a:	8d 43 08             	lea    0x8(%ebx),%eax
  80010d:	50                   	push   %eax
  80010e:	e8 ee 08 00 00       	call   800a01 <sys_cputs>
		b->idx = 0;
  800113:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800119:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  80011c:	ff 43 04             	incl   0x4(%ebx)
}
  80011f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800122:	c9                   	leave  
  800123:	c3                   	ret    

00800124 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800124:	55                   	push   %ebp
  800125:	89 e5                	mov    %esp,%ebp
  800127:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  80012d:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800134:	00 00 00 
	b.cnt = 0;
  800137:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80013e:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800141:	ff 75 0c             	pushl  0xc(%ebp)
  800144:	ff 75 08             	pushl  0x8(%ebp)
  800147:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80014d:	50                   	push   %eax
  80014e:	68 e3 00 80 00       	push   $0x8000e3
  800153:	e8 51 01 00 00       	call   8002a9 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800158:	83 c4 08             	add    $0x8,%esp
  80015b:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800161:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800167:	50                   	push   %eax
  800168:	e8 94 08 00 00       	call   800a01 <sys_cputs>

	return b.cnt;
}
  80016d:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800173:	c9                   	leave  
  800174:	c3                   	ret    

00800175 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800175:	55                   	push   %ebp
  800176:	89 e5                	mov    %esp,%ebp
  800178:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80017b:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80017e:	50                   	push   %eax
  80017f:	ff 75 08             	pushl  0x8(%ebp)
  800182:	e8 9d ff ff ff       	call   800124 <vcprintf>
	va_end(ap);

	return cnt;
}
  800187:	c9                   	leave  
  800188:	c3                   	ret    

00800189 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800189:	55                   	push   %ebp
  80018a:	89 e5                	mov    %esp,%ebp
  80018c:	57                   	push   %edi
  80018d:	56                   	push   %esi
  80018e:	53                   	push   %ebx
  80018f:	83 ec 1c             	sub    $0x1c,%esp
  800192:	89 c7                	mov    %eax,%edi
  800194:	89 d6                	mov    %edx,%esi
  800196:	8b 45 08             	mov    0x8(%ebp),%eax
  800199:	8b 55 0c             	mov    0xc(%ebp),%edx
  80019c:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80019f:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001a2:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8001a5:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001aa:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001ad:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001b0:	39 d3                	cmp    %edx,%ebx
  8001b2:	72 05                	jb     8001b9 <printnum+0x30>
  8001b4:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001b7:	77 45                	ja     8001fe <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001b9:	83 ec 0c             	sub    $0xc,%esp
  8001bc:	ff 75 18             	pushl  0x18(%ebp)
  8001bf:	8b 45 14             	mov    0x14(%ebp),%eax
  8001c2:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001c5:	53                   	push   %ebx
  8001c6:	ff 75 10             	pushl  0x10(%ebp)
  8001c9:	83 ec 08             	sub    $0x8,%esp
  8001cc:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001cf:	ff 75 e0             	pushl  -0x20(%ebp)
  8001d2:	ff 75 dc             	pushl  -0x24(%ebp)
  8001d5:	ff 75 d8             	pushl  -0x28(%ebp)
  8001d8:	e8 07 09 00 00       	call   800ae4 <__udivdi3>
  8001dd:	83 c4 18             	add    $0x18,%esp
  8001e0:	52                   	push   %edx
  8001e1:	50                   	push   %eax
  8001e2:	89 f2                	mov    %esi,%edx
  8001e4:	89 f8                	mov    %edi,%eax
  8001e6:	e8 9e ff ff ff       	call   800189 <printnum>
  8001eb:	83 c4 20             	add    $0x20,%esp
  8001ee:	eb 16                	jmp    800206 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001f0:	83 ec 08             	sub    $0x8,%esp
  8001f3:	56                   	push   %esi
  8001f4:	ff 75 18             	pushl  0x18(%ebp)
  8001f7:	ff d7                	call   *%edi
  8001f9:	83 c4 10             	add    $0x10,%esp
  8001fc:	eb 03                	jmp    800201 <printnum+0x78>
  8001fe:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800201:	4b                   	dec    %ebx
  800202:	85 db                	test   %ebx,%ebx
  800204:	7f ea                	jg     8001f0 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800206:	83 ec 08             	sub    $0x8,%esp
  800209:	56                   	push   %esi
  80020a:	83 ec 04             	sub    $0x4,%esp
  80020d:	ff 75 e4             	pushl  -0x1c(%ebp)
  800210:	ff 75 e0             	pushl  -0x20(%ebp)
  800213:	ff 75 dc             	pushl  -0x24(%ebp)
  800216:	ff 75 d8             	pushl  -0x28(%ebp)
  800219:	e8 d6 09 00 00       	call   800bf4 <__umoddi3>
  80021e:	83 c4 14             	add    $0x14,%esp
  800221:	0f be 80 a7 0d 80 00 	movsbl 0x800da7(%eax),%eax
  800228:	50                   	push   %eax
  800229:	ff d7                	call   *%edi
}
  80022b:	83 c4 10             	add    $0x10,%esp
  80022e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800231:	5b                   	pop    %ebx
  800232:	5e                   	pop    %esi
  800233:	5f                   	pop    %edi
  800234:	5d                   	pop    %ebp
  800235:	c3                   	ret    

00800236 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800236:	55                   	push   %ebp
  800237:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800239:	83 fa 01             	cmp    $0x1,%edx
  80023c:	7e 0e                	jle    80024c <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  80023e:	8b 10                	mov    (%eax),%edx
  800240:	8d 4a 08             	lea    0x8(%edx),%ecx
  800243:	89 08                	mov    %ecx,(%eax)
  800245:	8b 02                	mov    (%edx),%eax
  800247:	8b 52 04             	mov    0x4(%edx),%edx
  80024a:	eb 22                	jmp    80026e <getuint+0x38>
	else if (lflag)
  80024c:	85 d2                	test   %edx,%edx
  80024e:	74 10                	je     800260 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800250:	8b 10                	mov    (%eax),%edx
  800252:	8d 4a 04             	lea    0x4(%edx),%ecx
  800255:	89 08                	mov    %ecx,(%eax)
  800257:	8b 02                	mov    (%edx),%eax
  800259:	ba 00 00 00 00       	mov    $0x0,%edx
  80025e:	eb 0e                	jmp    80026e <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800260:	8b 10                	mov    (%eax),%edx
  800262:	8d 4a 04             	lea    0x4(%edx),%ecx
  800265:	89 08                	mov    %ecx,(%eax)
  800267:	8b 02                	mov    (%edx),%eax
  800269:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80026e:	5d                   	pop    %ebp
  80026f:	c3                   	ret    

00800270 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800270:	55                   	push   %ebp
  800271:	89 e5                	mov    %esp,%ebp
  800273:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800276:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800279:	8b 10                	mov    (%eax),%edx
  80027b:	3b 50 04             	cmp    0x4(%eax),%edx
  80027e:	73 0a                	jae    80028a <sprintputch+0x1a>
		*b->buf++ = ch;
  800280:	8d 4a 01             	lea    0x1(%edx),%ecx
  800283:	89 08                	mov    %ecx,(%eax)
  800285:	8b 45 08             	mov    0x8(%ebp),%eax
  800288:	88 02                	mov    %al,(%edx)
}
  80028a:	5d                   	pop    %ebp
  80028b:	c3                   	ret    

0080028c <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80028c:	55                   	push   %ebp
  80028d:	89 e5                	mov    %esp,%ebp
  80028f:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800292:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800295:	50                   	push   %eax
  800296:	ff 75 10             	pushl  0x10(%ebp)
  800299:	ff 75 0c             	pushl  0xc(%ebp)
  80029c:	ff 75 08             	pushl  0x8(%ebp)
  80029f:	e8 05 00 00 00       	call   8002a9 <vprintfmt>
	va_end(ap);
}
  8002a4:	83 c4 10             	add    $0x10,%esp
  8002a7:	c9                   	leave  
  8002a8:	c3                   	ret    

008002a9 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8002a9:	55                   	push   %ebp
  8002aa:	89 e5                	mov    %esp,%ebp
  8002ac:	57                   	push   %edi
  8002ad:	56                   	push   %esi
  8002ae:	53                   	push   %ebx
  8002af:	83 ec 2c             	sub    $0x2c,%esp
  8002b2:	8b 75 08             	mov    0x8(%ebp),%esi
  8002b5:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002b8:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002bb:	eb 12                	jmp    8002cf <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002bd:	85 c0                	test   %eax,%eax
  8002bf:	0f 84 68 03 00 00    	je     80062d <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002c5:	83 ec 08             	sub    $0x8,%esp
  8002c8:	53                   	push   %ebx
  8002c9:	50                   	push   %eax
  8002ca:	ff d6                	call   *%esi
  8002cc:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002cf:	47                   	inc    %edi
  8002d0:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002d4:	83 f8 25             	cmp    $0x25,%eax
  8002d7:	75 e4                	jne    8002bd <vprintfmt+0x14>
  8002d9:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002dd:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002e4:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002eb:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002f2:	ba 00 00 00 00       	mov    $0x0,%edx
  8002f7:	eb 07                	jmp    800300 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002f9:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8002fc:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800300:	8d 47 01             	lea    0x1(%edi),%eax
  800303:	89 45 e0             	mov    %eax,-0x20(%ebp)
  800306:	0f b6 0f             	movzbl (%edi),%ecx
  800309:	8a 07                	mov    (%edi),%al
  80030b:	83 e8 23             	sub    $0x23,%eax
  80030e:	3c 55                	cmp    $0x55,%al
  800310:	0f 87 fe 02 00 00    	ja     800614 <vprintfmt+0x36b>
  800316:	0f b6 c0             	movzbl %al,%eax
  800319:	ff 24 85 34 0e 80 00 	jmp    *0x800e34(,%eax,4)
  800320:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800323:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800327:	eb d7                	jmp    800300 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800329:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80032c:	b8 00 00 00 00       	mov    $0x0,%eax
  800331:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800334:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800337:	01 c0                	add    %eax,%eax
  800339:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  80033d:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800340:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800343:	83 fa 09             	cmp    $0x9,%edx
  800346:	77 34                	ja     80037c <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800348:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800349:	eb e9                	jmp    800334 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80034b:	8b 45 14             	mov    0x14(%ebp),%eax
  80034e:	8d 48 04             	lea    0x4(%eax),%ecx
  800351:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800354:	8b 00                	mov    (%eax),%eax
  800356:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800359:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  80035c:	eb 24                	jmp    800382 <vprintfmt+0xd9>
  80035e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800362:	79 07                	jns    80036b <vprintfmt+0xc2>
  800364:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80036b:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80036e:	eb 90                	jmp    800300 <vprintfmt+0x57>
  800370:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800373:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80037a:	eb 84                	jmp    800300 <vprintfmt+0x57>
  80037c:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80037f:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800382:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800386:	0f 89 74 ff ff ff    	jns    800300 <vprintfmt+0x57>
				width = precision, precision = -1;
  80038c:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80038f:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800392:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800399:	e9 62 ff ff ff       	jmp    800300 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80039e:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80039f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8003a2:	e9 59 ff ff ff       	jmp    800300 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8003a7:	8b 45 14             	mov    0x14(%ebp),%eax
  8003aa:	8d 50 04             	lea    0x4(%eax),%edx
  8003ad:	89 55 14             	mov    %edx,0x14(%ebp)
  8003b0:	83 ec 08             	sub    $0x8,%esp
  8003b3:	53                   	push   %ebx
  8003b4:	ff 30                	pushl  (%eax)
  8003b6:	ff d6                	call   *%esi
			break;
  8003b8:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003bb:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003be:	e9 0c ff ff ff       	jmp    8002cf <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003c3:	8b 45 14             	mov    0x14(%ebp),%eax
  8003c6:	8d 50 04             	lea    0x4(%eax),%edx
  8003c9:	89 55 14             	mov    %edx,0x14(%ebp)
  8003cc:	8b 00                	mov    (%eax),%eax
  8003ce:	85 c0                	test   %eax,%eax
  8003d0:	79 02                	jns    8003d4 <vprintfmt+0x12b>
  8003d2:	f7 d8                	neg    %eax
  8003d4:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003d6:	83 f8 06             	cmp    $0x6,%eax
  8003d9:	7f 0b                	jg     8003e6 <vprintfmt+0x13d>
  8003db:	8b 04 85 8c 0f 80 00 	mov    0x800f8c(,%eax,4),%eax
  8003e2:	85 c0                	test   %eax,%eax
  8003e4:	75 18                	jne    8003fe <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003e6:	52                   	push   %edx
  8003e7:	68 bf 0d 80 00       	push   $0x800dbf
  8003ec:	53                   	push   %ebx
  8003ed:	56                   	push   %esi
  8003ee:	e8 99 fe ff ff       	call   80028c <printfmt>
  8003f3:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f6:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8003f9:	e9 d1 fe ff ff       	jmp    8002cf <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8003fe:	50                   	push   %eax
  8003ff:	68 c8 0d 80 00       	push   $0x800dc8
  800404:	53                   	push   %ebx
  800405:	56                   	push   %esi
  800406:	e8 81 fe ff ff       	call   80028c <printfmt>
  80040b:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80040e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800411:	e9 b9 fe ff ff       	jmp    8002cf <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800416:	8b 45 14             	mov    0x14(%ebp),%eax
  800419:	8d 50 04             	lea    0x4(%eax),%edx
  80041c:	89 55 14             	mov    %edx,0x14(%ebp)
  80041f:	8b 38                	mov    (%eax),%edi
  800421:	85 ff                	test   %edi,%edi
  800423:	75 05                	jne    80042a <vprintfmt+0x181>
				p = "(null)";
  800425:	bf b8 0d 80 00       	mov    $0x800db8,%edi
			if (width > 0 && padc != '-')
  80042a:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80042e:	0f 8e 90 00 00 00    	jle    8004c4 <vprintfmt+0x21b>
  800434:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800438:	0f 84 8e 00 00 00    	je     8004cc <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  80043e:	83 ec 08             	sub    $0x8,%esp
  800441:	ff 75 d0             	pushl  -0x30(%ebp)
  800444:	57                   	push   %edi
  800445:	e8 70 02 00 00       	call   8006ba <strnlen>
  80044a:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  80044d:	29 c1                	sub    %eax,%ecx
  80044f:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800452:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800455:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800459:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80045c:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  80045f:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800461:	eb 0d                	jmp    800470 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800463:	83 ec 08             	sub    $0x8,%esp
  800466:	53                   	push   %ebx
  800467:	ff 75 e4             	pushl  -0x1c(%ebp)
  80046a:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80046c:	4f                   	dec    %edi
  80046d:	83 c4 10             	add    $0x10,%esp
  800470:	85 ff                	test   %edi,%edi
  800472:	7f ef                	jg     800463 <vprintfmt+0x1ba>
  800474:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800477:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80047a:	89 c8                	mov    %ecx,%eax
  80047c:	85 c9                	test   %ecx,%ecx
  80047e:	79 05                	jns    800485 <vprintfmt+0x1dc>
  800480:	b8 00 00 00 00       	mov    $0x0,%eax
  800485:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800488:	29 c1                	sub    %eax,%ecx
  80048a:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  80048d:	89 75 08             	mov    %esi,0x8(%ebp)
  800490:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800493:	eb 3d                	jmp    8004d2 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800495:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800499:	74 19                	je     8004b4 <vprintfmt+0x20b>
  80049b:	0f be c0             	movsbl %al,%eax
  80049e:	83 e8 20             	sub    $0x20,%eax
  8004a1:	83 f8 5e             	cmp    $0x5e,%eax
  8004a4:	76 0e                	jbe    8004b4 <vprintfmt+0x20b>
					putch('?', putdat);
  8004a6:	83 ec 08             	sub    $0x8,%esp
  8004a9:	53                   	push   %ebx
  8004aa:	6a 3f                	push   $0x3f
  8004ac:	ff 55 08             	call   *0x8(%ebp)
  8004af:	83 c4 10             	add    $0x10,%esp
  8004b2:	eb 0b                	jmp    8004bf <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004b4:	83 ec 08             	sub    $0x8,%esp
  8004b7:	53                   	push   %ebx
  8004b8:	52                   	push   %edx
  8004b9:	ff 55 08             	call   *0x8(%ebp)
  8004bc:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004bf:	ff 4d e4             	decl   -0x1c(%ebp)
  8004c2:	eb 0e                	jmp    8004d2 <vprintfmt+0x229>
  8004c4:	89 75 08             	mov    %esi,0x8(%ebp)
  8004c7:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004ca:	eb 06                	jmp    8004d2 <vprintfmt+0x229>
  8004cc:	89 75 08             	mov    %esi,0x8(%ebp)
  8004cf:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004d2:	47                   	inc    %edi
  8004d3:	8a 47 ff             	mov    -0x1(%edi),%al
  8004d6:	0f be d0             	movsbl %al,%edx
  8004d9:	85 d2                	test   %edx,%edx
  8004db:	74 1d                	je     8004fa <vprintfmt+0x251>
  8004dd:	85 f6                	test   %esi,%esi
  8004df:	78 b4                	js     800495 <vprintfmt+0x1ec>
  8004e1:	4e                   	dec    %esi
  8004e2:	79 b1                	jns    800495 <vprintfmt+0x1ec>
  8004e4:	8b 75 08             	mov    0x8(%ebp),%esi
  8004e7:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004ea:	eb 14                	jmp    800500 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004ec:	83 ec 08             	sub    $0x8,%esp
  8004ef:	53                   	push   %ebx
  8004f0:	6a 20                	push   $0x20
  8004f2:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004f4:	4f                   	dec    %edi
  8004f5:	83 c4 10             	add    $0x10,%esp
  8004f8:	eb 06                	jmp    800500 <vprintfmt+0x257>
  8004fa:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004fd:	8b 75 08             	mov    0x8(%ebp),%esi
  800500:	85 ff                	test   %edi,%edi
  800502:	7f e8                	jg     8004ec <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800504:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800507:	e9 c3 fd ff ff       	jmp    8002cf <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  80050c:	83 fa 01             	cmp    $0x1,%edx
  80050f:	7e 16                	jle    800527 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800511:	8b 45 14             	mov    0x14(%ebp),%eax
  800514:	8d 50 08             	lea    0x8(%eax),%edx
  800517:	89 55 14             	mov    %edx,0x14(%ebp)
  80051a:	8b 50 04             	mov    0x4(%eax),%edx
  80051d:	8b 00                	mov    (%eax),%eax
  80051f:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800522:	89 55 dc             	mov    %edx,-0x24(%ebp)
  800525:	eb 32                	jmp    800559 <vprintfmt+0x2b0>
	else if (lflag)
  800527:	85 d2                	test   %edx,%edx
  800529:	74 18                	je     800543 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  80052b:	8b 45 14             	mov    0x14(%ebp),%eax
  80052e:	8d 50 04             	lea    0x4(%eax),%edx
  800531:	89 55 14             	mov    %edx,0x14(%ebp)
  800534:	8b 00                	mov    (%eax),%eax
  800536:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800539:	89 c1                	mov    %eax,%ecx
  80053b:	c1 f9 1f             	sar    $0x1f,%ecx
  80053e:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800541:	eb 16                	jmp    800559 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800543:	8b 45 14             	mov    0x14(%ebp),%eax
  800546:	8d 50 04             	lea    0x4(%eax),%edx
  800549:	89 55 14             	mov    %edx,0x14(%ebp)
  80054c:	8b 00                	mov    (%eax),%eax
  80054e:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800551:	89 c1                	mov    %eax,%ecx
  800553:	c1 f9 1f             	sar    $0x1f,%ecx
  800556:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800559:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80055c:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  80055f:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800563:	79 76                	jns    8005db <vprintfmt+0x332>
				putch('-', putdat);
  800565:	83 ec 08             	sub    $0x8,%esp
  800568:	53                   	push   %ebx
  800569:	6a 2d                	push   $0x2d
  80056b:	ff d6                	call   *%esi
				num = -(long long) num;
  80056d:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800570:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800573:	f7 d8                	neg    %eax
  800575:	83 d2 00             	adc    $0x0,%edx
  800578:	f7 da                	neg    %edx
  80057a:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80057d:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800582:	eb 5c                	jmp    8005e0 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800584:	8d 45 14             	lea    0x14(%ebp),%eax
  800587:	e8 aa fc ff ff       	call   800236 <getuint>
			base = 10;
  80058c:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800591:	eb 4d                	jmp    8005e0 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800593:	8d 45 14             	lea    0x14(%ebp),%eax
  800596:	e8 9b fc ff ff       	call   800236 <getuint>
			base = 8;
  80059b:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8005a0:	eb 3e                	jmp    8005e0 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8005a2:	83 ec 08             	sub    $0x8,%esp
  8005a5:	53                   	push   %ebx
  8005a6:	6a 30                	push   $0x30
  8005a8:	ff d6                	call   *%esi
			putch('x', putdat);
  8005aa:	83 c4 08             	add    $0x8,%esp
  8005ad:	53                   	push   %ebx
  8005ae:	6a 78                	push   $0x78
  8005b0:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005b2:	8b 45 14             	mov    0x14(%ebp),%eax
  8005b5:	8d 50 04             	lea    0x4(%eax),%edx
  8005b8:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005bb:	8b 00                	mov    (%eax),%eax
  8005bd:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005c2:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005c5:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005ca:	eb 14                	jmp    8005e0 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005cc:	8d 45 14             	lea    0x14(%ebp),%eax
  8005cf:	e8 62 fc ff ff       	call   800236 <getuint>
			base = 16;
  8005d4:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005d9:	eb 05                	jmp    8005e0 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005db:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005e0:	83 ec 0c             	sub    $0xc,%esp
  8005e3:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005e7:	57                   	push   %edi
  8005e8:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005eb:	51                   	push   %ecx
  8005ec:	52                   	push   %edx
  8005ed:	50                   	push   %eax
  8005ee:	89 da                	mov    %ebx,%edx
  8005f0:	89 f0                	mov    %esi,%eax
  8005f2:	e8 92 fb ff ff       	call   800189 <printnum>
			break;
  8005f7:	83 c4 20             	add    $0x20,%esp
  8005fa:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005fd:	e9 cd fc ff ff       	jmp    8002cf <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800602:	83 ec 08             	sub    $0x8,%esp
  800605:	53                   	push   %ebx
  800606:	51                   	push   %ecx
  800607:	ff d6                	call   *%esi
			break;
  800609:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80060c:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  80060f:	e9 bb fc ff ff       	jmp    8002cf <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800614:	83 ec 08             	sub    $0x8,%esp
  800617:	53                   	push   %ebx
  800618:	6a 25                	push   $0x25
  80061a:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  80061c:	83 c4 10             	add    $0x10,%esp
  80061f:	eb 01                	jmp    800622 <vprintfmt+0x379>
  800621:	4f                   	dec    %edi
  800622:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800626:	75 f9                	jne    800621 <vprintfmt+0x378>
  800628:	e9 a2 fc ff ff       	jmp    8002cf <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  80062d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800630:	5b                   	pop    %ebx
  800631:	5e                   	pop    %esi
  800632:	5f                   	pop    %edi
  800633:	5d                   	pop    %ebp
  800634:	c3                   	ret    

00800635 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800635:	55                   	push   %ebp
  800636:	89 e5                	mov    %esp,%ebp
  800638:	83 ec 18             	sub    $0x18,%esp
  80063b:	8b 45 08             	mov    0x8(%ebp),%eax
  80063e:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800641:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800644:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800648:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  80064b:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800652:	85 c0                	test   %eax,%eax
  800654:	74 26                	je     80067c <vsnprintf+0x47>
  800656:	85 d2                	test   %edx,%edx
  800658:	7e 29                	jle    800683 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80065a:	ff 75 14             	pushl  0x14(%ebp)
  80065d:	ff 75 10             	pushl  0x10(%ebp)
  800660:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800663:	50                   	push   %eax
  800664:	68 70 02 80 00       	push   $0x800270
  800669:	e8 3b fc ff ff       	call   8002a9 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80066e:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800671:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800674:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800677:	83 c4 10             	add    $0x10,%esp
  80067a:	eb 0c                	jmp    800688 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80067c:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800681:	eb 05                	jmp    800688 <vsnprintf+0x53>
  800683:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800688:	c9                   	leave  
  800689:	c3                   	ret    

0080068a <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80068a:	55                   	push   %ebp
  80068b:	89 e5                	mov    %esp,%ebp
  80068d:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800690:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800693:	50                   	push   %eax
  800694:	ff 75 10             	pushl  0x10(%ebp)
  800697:	ff 75 0c             	pushl  0xc(%ebp)
  80069a:	ff 75 08             	pushl  0x8(%ebp)
  80069d:	e8 93 ff ff ff       	call   800635 <vsnprintf>
	va_end(ap);

	return rc;
}
  8006a2:	c9                   	leave  
  8006a3:	c3                   	ret    

008006a4 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8006a4:	55                   	push   %ebp
  8006a5:	89 e5                	mov    %esp,%ebp
  8006a7:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8006aa:	b8 00 00 00 00       	mov    $0x0,%eax
  8006af:	eb 01                	jmp    8006b2 <strlen+0xe>
		n++;
  8006b1:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006b2:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006b6:	75 f9                	jne    8006b1 <strlen+0xd>
		n++;
	return n;
}
  8006b8:	5d                   	pop    %ebp
  8006b9:	c3                   	ret    

008006ba <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006ba:	55                   	push   %ebp
  8006bb:	89 e5                	mov    %esp,%ebp
  8006bd:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006c0:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006c3:	ba 00 00 00 00       	mov    $0x0,%edx
  8006c8:	eb 01                	jmp    8006cb <strnlen+0x11>
		n++;
  8006ca:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006cb:	39 c2                	cmp    %eax,%edx
  8006cd:	74 08                	je     8006d7 <strnlen+0x1d>
  8006cf:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006d3:	75 f5                	jne    8006ca <strnlen+0x10>
  8006d5:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006d7:	5d                   	pop    %ebp
  8006d8:	c3                   	ret    

008006d9 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006d9:	55                   	push   %ebp
  8006da:	89 e5                	mov    %esp,%ebp
  8006dc:	53                   	push   %ebx
  8006dd:	8b 45 08             	mov    0x8(%ebp),%eax
  8006e0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006e3:	89 c2                	mov    %eax,%edx
  8006e5:	42                   	inc    %edx
  8006e6:	41                   	inc    %ecx
  8006e7:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006ea:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006ed:	84 db                	test   %bl,%bl
  8006ef:	75 f4                	jne    8006e5 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006f1:	5b                   	pop    %ebx
  8006f2:	5d                   	pop    %ebp
  8006f3:	c3                   	ret    

008006f4 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006f4:	55                   	push   %ebp
  8006f5:	89 e5                	mov    %esp,%ebp
  8006f7:	53                   	push   %ebx
  8006f8:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8006fb:	53                   	push   %ebx
  8006fc:	e8 a3 ff ff ff       	call   8006a4 <strlen>
  800701:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  800704:	ff 75 0c             	pushl  0xc(%ebp)
  800707:	01 d8                	add    %ebx,%eax
  800709:	50                   	push   %eax
  80070a:	e8 ca ff ff ff       	call   8006d9 <strcpy>
	return dst;
}
  80070f:	89 d8                	mov    %ebx,%eax
  800711:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800714:	c9                   	leave  
  800715:	c3                   	ret    

00800716 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  800716:	55                   	push   %ebp
  800717:	89 e5                	mov    %esp,%ebp
  800719:	56                   	push   %esi
  80071a:	53                   	push   %ebx
  80071b:	8b 75 08             	mov    0x8(%ebp),%esi
  80071e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800721:	89 f3                	mov    %esi,%ebx
  800723:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800726:	89 f2                	mov    %esi,%edx
  800728:	eb 0c                	jmp    800736 <strncpy+0x20>
		*dst++ = *src;
  80072a:	42                   	inc    %edx
  80072b:	8a 01                	mov    (%ecx),%al
  80072d:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800730:	80 39 01             	cmpb   $0x1,(%ecx)
  800733:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800736:	39 da                	cmp    %ebx,%edx
  800738:	75 f0                	jne    80072a <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  80073a:	89 f0                	mov    %esi,%eax
  80073c:	5b                   	pop    %ebx
  80073d:	5e                   	pop    %esi
  80073e:	5d                   	pop    %ebp
  80073f:	c3                   	ret    

00800740 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800740:	55                   	push   %ebp
  800741:	89 e5                	mov    %esp,%ebp
  800743:	56                   	push   %esi
  800744:	53                   	push   %ebx
  800745:	8b 75 08             	mov    0x8(%ebp),%esi
  800748:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  80074b:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  80074e:	85 c0                	test   %eax,%eax
  800750:	74 1e                	je     800770 <strlcpy+0x30>
  800752:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800756:	89 f2                	mov    %esi,%edx
  800758:	eb 05                	jmp    80075f <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  80075a:	42                   	inc    %edx
  80075b:	41                   	inc    %ecx
  80075c:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  80075f:	39 c2                	cmp    %eax,%edx
  800761:	74 08                	je     80076b <strlcpy+0x2b>
  800763:	8a 19                	mov    (%ecx),%bl
  800765:	84 db                	test   %bl,%bl
  800767:	75 f1                	jne    80075a <strlcpy+0x1a>
  800769:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  80076b:	c6 00 00             	movb   $0x0,(%eax)
  80076e:	eb 02                	jmp    800772 <strlcpy+0x32>
  800770:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800772:	29 f0                	sub    %esi,%eax
}
  800774:	5b                   	pop    %ebx
  800775:	5e                   	pop    %esi
  800776:	5d                   	pop    %ebp
  800777:	c3                   	ret    

00800778 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800778:	55                   	push   %ebp
  800779:	89 e5                	mov    %esp,%ebp
  80077b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80077e:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800781:	eb 02                	jmp    800785 <strcmp+0xd>
		p++, q++;
  800783:	41                   	inc    %ecx
  800784:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800785:	8a 01                	mov    (%ecx),%al
  800787:	84 c0                	test   %al,%al
  800789:	74 04                	je     80078f <strcmp+0x17>
  80078b:	3a 02                	cmp    (%edx),%al
  80078d:	74 f4                	je     800783 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80078f:	0f b6 c0             	movzbl %al,%eax
  800792:	0f b6 12             	movzbl (%edx),%edx
  800795:	29 d0                	sub    %edx,%eax
}
  800797:	5d                   	pop    %ebp
  800798:	c3                   	ret    

00800799 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800799:	55                   	push   %ebp
  80079a:	89 e5                	mov    %esp,%ebp
  80079c:	53                   	push   %ebx
  80079d:	8b 45 08             	mov    0x8(%ebp),%eax
  8007a0:	8b 55 0c             	mov    0xc(%ebp),%edx
  8007a3:	89 c3                	mov    %eax,%ebx
  8007a5:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8007a8:	eb 02                	jmp    8007ac <strncmp+0x13>
		n--, p++, q++;
  8007aa:	40                   	inc    %eax
  8007ab:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8007ac:	39 d8                	cmp    %ebx,%eax
  8007ae:	74 14                	je     8007c4 <strncmp+0x2b>
  8007b0:	8a 08                	mov    (%eax),%cl
  8007b2:	84 c9                	test   %cl,%cl
  8007b4:	74 04                	je     8007ba <strncmp+0x21>
  8007b6:	3a 0a                	cmp    (%edx),%cl
  8007b8:	74 f0                	je     8007aa <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007ba:	0f b6 00             	movzbl (%eax),%eax
  8007bd:	0f b6 12             	movzbl (%edx),%edx
  8007c0:	29 d0                	sub    %edx,%eax
  8007c2:	eb 05                	jmp    8007c9 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007c4:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007c9:	5b                   	pop    %ebx
  8007ca:	5d                   	pop    %ebp
  8007cb:	c3                   	ret    

008007cc <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007cc:	55                   	push   %ebp
  8007cd:	89 e5                	mov    %esp,%ebp
  8007cf:	8b 45 08             	mov    0x8(%ebp),%eax
  8007d2:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007d5:	eb 05                	jmp    8007dc <strchr+0x10>
		if (*s == c)
  8007d7:	38 ca                	cmp    %cl,%dl
  8007d9:	74 0c                	je     8007e7 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007db:	40                   	inc    %eax
  8007dc:	8a 10                	mov    (%eax),%dl
  8007de:	84 d2                	test   %dl,%dl
  8007e0:	75 f5                	jne    8007d7 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007e2:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007e7:	5d                   	pop    %ebp
  8007e8:	c3                   	ret    

008007e9 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007e9:	55                   	push   %ebp
  8007ea:	89 e5                	mov    %esp,%ebp
  8007ec:	8b 45 08             	mov    0x8(%ebp),%eax
  8007ef:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007f2:	eb 05                	jmp    8007f9 <strfind+0x10>
		if (*s == c)
  8007f4:	38 ca                	cmp    %cl,%dl
  8007f6:	74 07                	je     8007ff <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007f8:	40                   	inc    %eax
  8007f9:	8a 10                	mov    (%eax),%dl
  8007fb:	84 d2                	test   %dl,%dl
  8007fd:	75 f5                	jne    8007f4 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8007ff:	5d                   	pop    %ebp
  800800:	c3                   	ret    

00800801 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800801:	55                   	push   %ebp
  800802:	89 e5                	mov    %esp,%ebp
  800804:	57                   	push   %edi
  800805:	56                   	push   %esi
  800806:	53                   	push   %ebx
  800807:	8b 7d 08             	mov    0x8(%ebp),%edi
  80080a:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  80080d:	85 c9                	test   %ecx,%ecx
  80080f:	74 36                	je     800847 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800811:	f7 c7 03 00 00 00    	test   $0x3,%edi
  800817:	75 28                	jne    800841 <memset+0x40>
  800819:	f6 c1 03             	test   $0x3,%cl
  80081c:	75 23                	jne    800841 <memset+0x40>
		c &= 0xFF;
  80081e:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800822:	89 d3                	mov    %edx,%ebx
  800824:	c1 e3 08             	shl    $0x8,%ebx
  800827:	89 d6                	mov    %edx,%esi
  800829:	c1 e6 18             	shl    $0x18,%esi
  80082c:	89 d0                	mov    %edx,%eax
  80082e:	c1 e0 10             	shl    $0x10,%eax
  800831:	09 f0                	or     %esi,%eax
  800833:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  800835:	89 d8                	mov    %ebx,%eax
  800837:	09 d0                	or     %edx,%eax
  800839:	c1 e9 02             	shr    $0x2,%ecx
  80083c:	fc                   	cld    
  80083d:	f3 ab                	rep stos %eax,%es:(%edi)
  80083f:	eb 06                	jmp    800847 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800841:	8b 45 0c             	mov    0xc(%ebp),%eax
  800844:	fc                   	cld    
  800845:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800847:	89 f8                	mov    %edi,%eax
  800849:	5b                   	pop    %ebx
  80084a:	5e                   	pop    %esi
  80084b:	5f                   	pop    %edi
  80084c:	5d                   	pop    %ebp
  80084d:	c3                   	ret    

0080084e <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  80084e:	55                   	push   %ebp
  80084f:	89 e5                	mov    %esp,%ebp
  800851:	57                   	push   %edi
  800852:	56                   	push   %esi
  800853:	8b 45 08             	mov    0x8(%ebp),%eax
  800856:	8b 75 0c             	mov    0xc(%ebp),%esi
  800859:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  80085c:	39 c6                	cmp    %eax,%esi
  80085e:	73 33                	jae    800893 <memmove+0x45>
  800860:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800863:	39 d0                	cmp    %edx,%eax
  800865:	73 2c                	jae    800893 <memmove+0x45>
		s += n;
		d += n;
  800867:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80086a:	89 d6                	mov    %edx,%esi
  80086c:	09 fe                	or     %edi,%esi
  80086e:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800874:	75 13                	jne    800889 <memmove+0x3b>
  800876:	f6 c1 03             	test   $0x3,%cl
  800879:	75 0e                	jne    800889 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  80087b:	83 ef 04             	sub    $0x4,%edi
  80087e:	8d 72 fc             	lea    -0x4(%edx),%esi
  800881:	c1 e9 02             	shr    $0x2,%ecx
  800884:	fd                   	std    
  800885:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800887:	eb 07                	jmp    800890 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800889:	4f                   	dec    %edi
  80088a:	8d 72 ff             	lea    -0x1(%edx),%esi
  80088d:	fd                   	std    
  80088e:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800890:	fc                   	cld    
  800891:	eb 1d                	jmp    8008b0 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800893:	89 f2                	mov    %esi,%edx
  800895:	09 c2                	or     %eax,%edx
  800897:	f6 c2 03             	test   $0x3,%dl
  80089a:	75 0f                	jne    8008ab <memmove+0x5d>
  80089c:	f6 c1 03             	test   $0x3,%cl
  80089f:	75 0a                	jne    8008ab <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  8008a1:	c1 e9 02             	shr    $0x2,%ecx
  8008a4:	89 c7                	mov    %eax,%edi
  8008a6:	fc                   	cld    
  8008a7:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008a9:	eb 05                	jmp    8008b0 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8008ab:	89 c7                	mov    %eax,%edi
  8008ad:	fc                   	cld    
  8008ae:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008b0:	5e                   	pop    %esi
  8008b1:	5f                   	pop    %edi
  8008b2:	5d                   	pop    %ebp
  8008b3:	c3                   	ret    

008008b4 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008b4:	55                   	push   %ebp
  8008b5:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008b7:	ff 75 10             	pushl  0x10(%ebp)
  8008ba:	ff 75 0c             	pushl  0xc(%ebp)
  8008bd:	ff 75 08             	pushl  0x8(%ebp)
  8008c0:	e8 89 ff ff ff       	call   80084e <memmove>
}
  8008c5:	c9                   	leave  
  8008c6:	c3                   	ret    

008008c7 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008c7:	55                   	push   %ebp
  8008c8:	89 e5                	mov    %esp,%ebp
  8008ca:	56                   	push   %esi
  8008cb:	53                   	push   %ebx
  8008cc:	8b 45 08             	mov    0x8(%ebp),%eax
  8008cf:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008d2:	89 c6                	mov    %eax,%esi
  8008d4:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008d7:	eb 14                	jmp    8008ed <memcmp+0x26>
		if (*s1 != *s2)
  8008d9:	8a 08                	mov    (%eax),%cl
  8008db:	8a 1a                	mov    (%edx),%bl
  8008dd:	38 d9                	cmp    %bl,%cl
  8008df:	74 0a                	je     8008eb <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008e1:	0f b6 c1             	movzbl %cl,%eax
  8008e4:	0f b6 db             	movzbl %bl,%ebx
  8008e7:	29 d8                	sub    %ebx,%eax
  8008e9:	eb 0b                	jmp    8008f6 <memcmp+0x2f>
		s1++, s2++;
  8008eb:	40                   	inc    %eax
  8008ec:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008ed:	39 f0                	cmp    %esi,%eax
  8008ef:	75 e8                	jne    8008d9 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008f1:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008f6:	5b                   	pop    %ebx
  8008f7:	5e                   	pop    %esi
  8008f8:	5d                   	pop    %ebp
  8008f9:	c3                   	ret    

008008fa <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8008fa:	55                   	push   %ebp
  8008fb:	89 e5                	mov    %esp,%ebp
  8008fd:	53                   	push   %ebx
  8008fe:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800901:	89 c1                	mov    %eax,%ecx
  800903:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  800906:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  80090a:	eb 08                	jmp    800914 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  80090c:	0f b6 10             	movzbl (%eax),%edx
  80090f:	39 da                	cmp    %ebx,%edx
  800911:	74 05                	je     800918 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800913:	40                   	inc    %eax
  800914:	39 c8                	cmp    %ecx,%eax
  800916:	72 f4                	jb     80090c <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  800918:	5b                   	pop    %ebx
  800919:	5d                   	pop    %ebp
  80091a:	c3                   	ret    

0080091b <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  80091b:	55                   	push   %ebp
  80091c:	89 e5                	mov    %esp,%ebp
  80091e:	57                   	push   %edi
  80091f:	56                   	push   %esi
  800920:	53                   	push   %ebx
  800921:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800924:	eb 01                	jmp    800927 <strtol+0xc>
		s++;
  800926:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800927:	8a 01                	mov    (%ecx),%al
  800929:	3c 20                	cmp    $0x20,%al
  80092b:	74 f9                	je     800926 <strtol+0xb>
  80092d:	3c 09                	cmp    $0x9,%al
  80092f:	74 f5                	je     800926 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800931:	3c 2b                	cmp    $0x2b,%al
  800933:	75 08                	jne    80093d <strtol+0x22>
		s++;
  800935:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800936:	bf 00 00 00 00       	mov    $0x0,%edi
  80093b:	eb 11                	jmp    80094e <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  80093d:	3c 2d                	cmp    $0x2d,%al
  80093f:	75 08                	jne    800949 <strtol+0x2e>
		s++, neg = 1;
  800941:	41                   	inc    %ecx
  800942:	bf 01 00 00 00       	mov    $0x1,%edi
  800947:	eb 05                	jmp    80094e <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800949:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  80094e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800952:	0f 84 87 00 00 00    	je     8009df <strtol+0xc4>
  800958:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  80095c:	75 27                	jne    800985 <strtol+0x6a>
  80095e:	80 39 30             	cmpb   $0x30,(%ecx)
  800961:	75 22                	jne    800985 <strtol+0x6a>
  800963:	e9 88 00 00 00       	jmp    8009f0 <strtol+0xd5>
		s += 2, base = 16;
  800968:	83 c1 02             	add    $0x2,%ecx
  80096b:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800972:	eb 11                	jmp    800985 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800974:	41                   	inc    %ecx
  800975:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  80097c:	eb 07                	jmp    800985 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  80097e:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800985:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  80098a:	8a 11                	mov    (%ecx),%dl
  80098c:	8d 5a d0             	lea    -0x30(%edx),%ebx
  80098f:	80 fb 09             	cmp    $0x9,%bl
  800992:	77 08                	ja     80099c <strtol+0x81>
			dig = *s - '0';
  800994:	0f be d2             	movsbl %dl,%edx
  800997:	83 ea 30             	sub    $0x30,%edx
  80099a:	eb 22                	jmp    8009be <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  80099c:	8d 72 9f             	lea    -0x61(%edx),%esi
  80099f:	89 f3                	mov    %esi,%ebx
  8009a1:	80 fb 19             	cmp    $0x19,%bl
  8009a4:	77 08                	ja     8009ae <strtol+0x93>
			dig = *s - 'a' + 10;
  8009a6:	0f be d2             	movsbl %dl,%edx
  8009a9:	83 ea 57             	sub    $0x57,%edx
  8009ac:	eb 10                	jmp    8009be <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009ae:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009b1:	89 f3                	mov    %esi,%ebx
  8009b3:	80 fb 19             	cmp    $0x19,%bl
  8009b6:	77 14                	ja     8009cc <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009b8:	0f be d2             	movsbl %dl,%edx
  8009bb:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009be:	3b 55 10             	cmp    0x10(%ebp),%edx
  8009c1:	7d 09                	jge    8009cc <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009c3:	41                   	inc    %ecx
  8009c4:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009c8:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009ca:	eb be                	jmp    80098a <strtol+0x6f>

	if (endptr)
  8009cc:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009d0:	74 05                	je     8009d7 <strtol+0xbc>
		*endptr = (char *) s;
  8009d2:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009d5:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009d7:	85 ff                	test   %edi,%edi
  8009d9:	74 21                	je     8009fc <strtol+0xe1>
  8009db:	f7 d8                	neg    %eax
  8009dd:	eb 1d                	jmp    8009fc <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009df:	80 39 30             	cmpb   $0x30,(%ecx)
  8009e2:	75 9a                	jne    80097e <strtol+0x63>
  8009e4:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009e8:	0f 84 7a ff ff ff    	je     800968 <strtol+0x4d>
  8009ee:	eb 84                	jmp    800974 <strtol+0x59>
  8009f0:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009f4:	0f 84 6e ff ff ff    	je     800968 <strtol+0x4d>
  8009fa:	eb 89                	jmp    800985 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  8009fc:	5b                   	pop    %ebx
  8009fd:	5e                   	pop    %esi
  8009fe:	5f                   	pop    %edi
  8009ff:	5d                   	pop    %ebp
  800a00:	c3                   	ret    

00800a01 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a01:	55                   	push   %ebp
  800a02:	89 e5                	mov    %esp,%ebp
  800a04:	57                   	push   %edi
  800a05:	56                   	push   %esi
  800a06:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a07:	b8 00 00 00 00       	mov    $0x0,%eax
  800a0c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a0f:	8b 55 08             	mov    0x8(%ebp),%edx
  800a12:	89 c3                	mov    %eax,%ebx
  800a14:	89 c7                	mov    %eax,%edi
  800a16:	89 c6                	mov    %eax,%esi
  800a18:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a1a:	5b                   	pop    %ebx
  800a1b:	5e                   	pop    %esi
  800a1c:	5f                   	pop    %edi
  800a1d:	5d                   	pop    %ebp
  800a1e:	c3                   	ret    

00800a1f <sys_cgetc>:

int
sys_cgetc(void)
{
  800a1f:	55                   	push   %ebp
  800a20:	89 e5                	mov    %esp,%ebp
  800a22:	57                   	push   %edi
  800a23:	56                   	push   %esi
  800a24:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a25:	ba 00 00 00 00       	mov    $0x0,%edx
  800a2a:	b8 01 00 00 00       	mov    $0x1,%eax
  800a2f:	89 d1                	mov    %edx,%ecx
  800a31:	89 d3                	mov    %edx,%ebx
  800a33:	89 d7                	mov    %edx,%edi
  800a35:	89 d6                	mov    %edx,%esi
  800a37:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a39:	5b                   	pop    %ebx
  800a3a:	5e                   	pop    %esi
  800a3b:	5f                   	pop    %edi
  800a3c:	5d                   	pop    %ebp
  800a3d:	c3                   	ret    

00800a3e <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a3e:	55                   	push   %ebp
  800a3f:	89 e5                	mov    %esp,%ebp
  800a41:	57                   	push   %edi
  800a42:	56                   	push   %esi
  800a43:	53                   	push   %ebx
  800a44:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a47:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a4c:	b8 03 00 00 00       	mov    $0x3,%eax
  800a51:	8b 55 08             	mov    0x8(%ebp),%edx
  800a54:	89 cb                	mov    %ecx,%ebx
  800a56:	89 cf                	mov    %ecx,%edi
  800a58:	89 ce                	mov    %ecx,%esi
  800a5a:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a5c:	85 c0                	test   %eax,%eax
  800a5e:	7e 17                	jle    800a77 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a60:	83 ec 0c             	sub    $0xc,%esp
  800a63:	50                   	push   %eax
  800a64:	6a 03                	push   $0x3
  800a66:	68 a8 0f 80 00       	push   $0x800fa8
  800a6b:	6a 23                	push   $0x23
  800a6d:	68 c5 0f 80 00       	push   $0x800fc5
  800a72:	e8 27 00 00 00       	call   800a9e <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a77:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a7a:	5b                   	pop    %ebx
  800a7b:	5e                   	pop    %esi
  800a7c:	5f                   	pop    %edi
  800a7d:	5d                   	pop    %ebp
  800a7e:	c3                   	ret    

00800a7f <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a7f:	55                   	push   %ebp
  800a80:	89 e5                	mov    %esp,%ebp
  800a82:	57                   	push   %edi
  800a83:	56                   	push   %esi
  800a84:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a85:	ba 00 00 00 00       	mov    $0x0,%edx
  800a8a:	b8 02 00 00 00       	mov    $0x2,%eax
  800a8f:	89 d1                	mov    %edx,%ecx
  800a91:	89 d3                	mov    %edx,%ebx
  800a93:	89 d7                	mov    %edx,%edi
  800a95:	89 d6                	mov    %edx,%esi
  800a97:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800a99:	5b                   	pop    %ebx
  800a9a:	5e                   	pop    %esi
  800a9b:	5f                   	pop    %edi
  800a9c:	5d                   	pop    %ebp
  800a9d:	c3                   	ret    

00800a9e <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800a9e:	55                   	push   %ebp
  800a9f:	89 e5                	mov    %esp,%ebp
  800aa1:	56                   	push   %esi
  800aa2:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800aa3:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800aa6:	8b 35 00 10 80 00    	mov    0x801000,%esi
  800aac:	e8 ce ff ff ff       	call   800a7f <sys_getenvid>
  800ab1:	83 ec 0c             	sub    $0xc,%esp
  800ab4:	ff 75 0c             	pushl  0xc(%ebp)
  800ab7:	ff 75 08             	pushl  0x8(%ebp)
  800aba:	56                   	push   %esi
  800abb:	50                   	push   %eax
  800abc:	68 d4 0f 80 00       	push   $0x800fd4
  800ac1:	e8 af f6 ff ff       	call   800175 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800ac6:	83 c4 18             	add    $0x18,%esp
  800ac9:	53                   	push   %ebx
  800aca:	ff 75 10             	pushl  0x10(%ebp)
  800acd:	e8 52 f6 ff ff       	call   800124 <vcprintf>
	cprintf("\n");
  800ad2:	c7 04 24 5f 0d 80 00 	movl   $0x800d5f,(%esp)
  800ad9:	e8 97 f6 ff ff       	call   800175 <cprintf>
  800ade:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800ae1:	cc                   	int3   
  800ae2:	eb fd                	jmp    800ae1 <_panic+0x43>

00800ae4 <__udivdi3>:
  800ae4:	55                   	push   %ebp
  800ae5:	57                   	push   %edi
  800ae6:	56                   	push   %esi
  800ae7:	53                   	push   %ebx
  800ae8:	83 ec 1c             	sub    $0x1c,%esp
  800aeb:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800aef:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800af3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800af7:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800afb:	89 ca                	mov    %ecx,%edx
  800afd:	89 f8                	mov    %edi,%eax
  800aff:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800b03:	85 f6                	test   %esi,%esi
  800b05:	75 2d                	jne    800b34 <__udivdi3+0x50>
  800b07:	39 cf                	cmp    %ecx,%edi
  800b09:	77 65                	ja     800b70 <__udivdi3+0x8c>
  800b0b:	89 fd                	mov    %edi,%ebp
  800b0d:	85 ff                	test   %edi,%edi
  800b0f:	75 0b                	jne    800b1c <__udivdi3+0x38>
  800b11:	b8 01 00 00 00       	mov    $0x1,%eax
  800b16:	31 d2                	xor    %edx,%edx
  800b18:	f7 f7                	div    %edi
  800b1a:	89 c5                	mov    %eax,%ebp
  800b1c:	31 d2                	xor    %edx,%edx
  800b1e:	89 c8                	mov    %ecx,%eax
  800b20:	f7 f5                	div    %ebp
  800b22:	89 c1                	mov    %eax,%ecx
  800b24:	89 d8                	mov    %ebx,%eax
  800b26:	f7 f5                	div    %ebp
  800b28:	89 cf                	mov    %ecx,%edi
  800b2a:	89 fa                	mov    %edi,%edx
  800b2c:	83 c4 1c             	add    $0x1c,%esp
  800b2f:	5b                   	pop    %ebx
  800b30:	5e                   	pop    %esi
  800b31:	5f                   	pop    %edi
  800b32:	5d                   	pop    %ebp
  800b33:	c3                   	ret    
  800b34:	39 ce                	cmp    %ecx,%esi
  800b36:	77 28                	ja     800b60 <__udivdi3+0x7c>
  800b38:	0f bd fe             	bsr    %esi,%edi
  800b3b:	83 f7 1f             	xor    $0x1f,%edi
  800b3e:	75 40                	jne    800b80 <__udivdi3+0x9c>
  800b40:	39 ce                	cmp    %ecx,%esi
  800b42:	72 0a                	jb     800b4e <__udivdi3+0x6a>
  800b44:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b48:	0f 87 9e 00 00 00    	ja     800bec <__udivdi3+0x108>
  800b4e:	b8 01 00 00 00       	mov    $0x1,%eax
  800b53:	89 fa                	mov    %edi,%edx
  800b55:	83 c4 1c             	add    $0x1c,%esp
  800b58:	5b                   	pop    %ebx
  800b59:	5e                   	pop    %esi
  800b5a:	5f                   	pop    %edi
  800b5b:	5d                   	pop    %ebp
  800b5c:	c3                   	ret    
  800b5d:	8d 76 00             	lea    0x0(%esi),%esi
  800b60:	31 ff                	xor    %edi,%edi
  800b62:	31 c0                	xor    %eax,%eax
  800b64:	89 fa                	mov    %edi,%edx
  800b66:	83 c4 1c             	add    $0x1c,%esp
  800b69:	5b                   	pop    %ebx
  800b6a:	5e                   	pop    %esi
  800b6b:	5f                   	pop    %edi
  800b6c:	5d                   	pop    %ebp
  800b6d:	c3                   	ret    
  800b6e:	66 90                	xchg   %ax,%ax
  800b70:	89 d8                	mov    %ebx,%eax
  800b72:	f7 f7                	div    %edi
  800b74:	31 ff                	xor    %edi,%edi
  800b76:	89 fa                	mov    %edi,%edx
  800b78:	83 c4 1c             	add    $0x1c,%esp
  800b7b:	5b                   	pop    %ebx
  800b7c:	5e                   	pop    %esi
  800b7d:	5f                   	pop    %edi
  800b7e:	5d                   	pop    %ebp
  800b7f:	c3                   	ret    
  800b80:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b85:	89 eb                	mov    %ebp,%ebx
  800b87:	29 fb                	sub    %edi,%ebx
  800b89:	89 f9                	mov    %edi,%ecx
  800b8b:	d3 e6                	shl    %cl,%esi
  800b8d:	89 c5                	mov    %eax,%ebp
  800b8f:	88 d9                	mov    %bl,%cl
  800b91:	d3 ed                	shr    %cl,%ebp
  800b93:	89 e9                	mov    %ebp,%ecx
  800b95:	09 f1                	or     %esi,%ecx
  800b97:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b9b:	89 f9                	mov    %edi,%ecx
  800b9d:	d3 e0                	shl    %cl,%eax
  800b9f:	89 c5                	mov    %eax,%ebp
  800ba1:	89 d6                	mov    %edx,%esi
  800ba3:	88 d9                	mov    %bl,%cl
  800ba5:	d3 ee                	shr    %cl,%esi
  800ba7:	89 f9                	mov    %edi,%ecx
  800ba9:	d3 e2                	shl    %cl,%edx
  800bab:	8b 44 24 08          	mov    0x8(%esp),%eax
  800baf:	88 d9                	mov    %bl,%cl
  800bb1:	d3 e8                	shr    %cl,%eax
  800bb3:	09 c2                	or     %eax,%edx
  800bb5:	89 d0                	mov    %edx,%eax
  800bb7:	89 f2                	mov    %esi,%edx
  800bb9:	f7 74 24 0c          	divl   0xc(%esp)
  800bbd:	89 d6                	mov    %edx,%esi
  800bbf:	89 c3                	mov    %eax,%ebx
  800bc1:	f7 e5                	mul    %ebp
  800bc3:	39 d6                	cmp    %edx,%esi
  800bc5:	72 19                	jb     800be0 <__udivdi3+0xfc>
  800bc7:	74 0b                	je     800bd4 <__udivdi3+0xf0>
  800bc9:	89 d8                	mov    %ebx,%eax
  800bcb:	31 ff                	xor    %edi,%edi
  800bcd:	e9 58 ff ff ff       	jmp    800b2a <__udivdi3+0x46>
  800bd2:	66 90                	xchg   %ax,%ax
  800bd4:	8b 54 24 08          	mov    0x8(%esp),%edx
  800bd8:	89 f9                	mov    %edi,%ecx
  800bda:	d3 e2                	shl    %cl,%edx
  800bdc:	39 c2                	cmp    %eax,%edx
  800bde:	73 e9                	jae    800bc9 <__udivdi3+0xe5>
  800be0:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800be3:	31 ff                	xor    %edi,%edi
  800be5:	e9 40 ff ff ff       	jmp    800b2a <__udivdi3+0x46>
  800bea:	66 90                	xchg   %ax,%ax
  800bec:	31 c0                	xor    %eax,%eax
  800bee:	e9 37 ff ff ff       	jmp    800b2a <__udivdi3+0x46>
  800bf3:	90                   	nop

00800bf4 <__umoddi3>:
  800bf4:	55                   	push   %ebp
  800bf5:	57                   	push   %edi
  800bf6:	56                   	push   %esi
  800bf7:	53                   	push   %ebx
  800bf8:	83 ec 1c             	sub    $0x1c,%esp
  800bfb:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bff:	8b 74 24 34          	mov    0x34(%esp),%esi
  800c03:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800c07:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800c0b:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800c0f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800c13:	89 f3                	mov    %esi,%ebx
  800c15:	89 fa                	mov    %edi,%edx
  800c17:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800c1b:	89 34 24             	mov    %esi,(%esp)
  800c1e:	85 c0                	test   %eax,%eax
  800c20:	75 1a                	jne    800c3c <__umoddi3+0x48>
  800c22:	39 f7                	cmp    %esi,%edi
  800c24:	0f 86 a2 00 00 00    	jbe    800ccc <__umoddi3+0xd8>
  800c2a:	89 c8                	mov    %ecx,%eax
  800c2c:	89 f2                	mov    %esi,%edx
  800c2e:	f7 f7                	div    %edi
  800c30:	89 d0                	mov    %edx,%eax
  800c32:	31 d2                	xor    %edx,%edx
  800c34:	83 c4 1c             	add    $0x1c,%esp
  800c37:	5b                   	pop    %ebx
  800c38:	5e                   	pop    %esi
  800c39:	5f                   	pop    %edi
  800c3a:	5d                   	pop    %ebp
  800c3b:	c3                   	ret    
  800c3c:	39 f0                	cmp    %esi,%eax
  800c3e:	0f 87 ac 00 00 00    	ja     800cf0 <__umoddi3+0xfc>
  800c44:	0f bd e8             	bsr    %eax,%ebp
  800c47:	83 f5 1f             	xor    $0x1f,%ebp
  800c4a:	0f 84 ac 00 00 00    	je     800cfc <__umoddi3+0x108>
  800c50:	bf 20 00 00 00       	mov    $0x20,%edi
  800c55:	29 ef                	sub    %ebp,%edi
  800c57:	89 fe                	mov    %edi,%esi
  800c59:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c5d:	89 e9                	mov    %ebp,%ecx
  800c5f:	d3 e0                	shl    %cl,%eax
  800c61:	89 d7                	mov    %edx,%edi
  800c63:	89 f1                	mov    %esi,%ecx
  800c65:	d3 ef                	shr    %cl,%edi
  800c67:	09 c7                	or     %eax,%edi
  800c69:	89 e9                	mov    %ebp,%ecx
  800c6b:	d3 e2                	shl    %cl,%edx
  800c6d:	89 14 24             	mov    %edx,(%esp)
  800c70:	89 d8                	mov    %ebx,%eax
  800c72:	d3 e0                	shl    %cl,%eax
  800c74:	89 c2                	mov    %eax,%edx
  800c76:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c7a:	d3 e0                	shl    %cl,%eax
  800c7c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c80:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c84:	89 f1                	mov    %esi,%ecx
  800c86:	d3 e8                	shr    %cl,%eax
  800c88:	09 d0                	or     %edx,%eax
  800c8a:	d3 eb                	shr    %cl,%ebx
  800c8c:	89 da                	mov    %ebx,%edx
  800c8e:	f7 f7                	div    %edi
  800c90:	89 d3                	mov    %edx,%ebx
  800c92:	f7 24 24             	mull   (%esp)
  800c95:	89 c6                	mov    %eax,%esi
  800c97:	89 d1                	mov    %edx,%ecx
  800c99:	39 d3                	cmp    %edx,%ebx
  800c9b:	0f 82 87 00 00 00    	jb     800d28 <__umoddi3+0x134>
  800ca1:	0f 84 91 00 00 00    	je     800d38 <__umoddi3+0x144>
  800ca7:	8b 54 24 04          	mov    0x4(%esp),%edx
  800cab:	29 f2                	sub    %esi,%edx
  800cad:	19 cb                	sbb    %ecx,%ebx
  800caf:	89 d8                	mov    %ebx,%eax
  800cb1:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800cb5:	d3 e0                	shl    %cl,%eax
  800cb7:	89 e9                	mov    %ebp,%ecx
  800cb9:	d3 ea                	shr    %cl,%edx
  800cbb:	09 d0                	or     %edx,%eax
  800cbd:	89 e9                	mov    %ebp,%ecx
  800cbf:	d3 eb                	shr    %cl,%ebx
  800cc1:	89 da                	mov    %ebx,%edx
  800cc3:	83 c4 1c             	add    $0x1c,%esp
  800cc6:	5b                   	pop    %ebx
  800cc7:	5e                   	pop    %esi
  800cc8:	5f                   	pop    %edi
  800cc9:	5d                   	pop    %ebp
  800cca:	c3                   	ret    
  800ccb:	90                   	nop
  800ccc:	89 fd                	mov    %edi,%ebp
  800cce:	85 ff                	test   %edi,%edi
  800cd0:	75 0b                	jne    800cdd <__umoddi3+0xe9>
  800cd2:	b8 01 00 00 00       	mov    $0x1,%eax
  800cd7:	31 d2                	xor    %edx,%edx
  800cd9:	f7 f7                	div    %edi
  800cdb:	89 c5                	mov    %eax,%ebp
  800cdd:	89 f0                	mov    %esi,%eax
  800cdf:	31 d2                	xor    %edx,%edx
  800ce1:	f7 f5                	div    %ebp
  800ce3:	89 c8                	mov    %ecx,%eax
  800ce5:	f7 f5                	div    %ebp
  800ce7:	89 d0                	mov    %edx,%eax
  800ce9:	e9 44 ff ff ff       	jmp    800c32 <__umoddi3+0x3e>
  800cee:	66 90                	xchg   %ax,%ax
  800cf0:	89 c8                	mov    %ecx,%eax
  800cf2:	89 f2                	mov    %esi,%edx
  800cf4:	83 c4 1c             	add    $0x1c,%esp
  800cf7:	5b                   	pop    %ebx
  800cf8:	5e                   	pop    %esi
  800cf9:	5f                   	pop    %edi
  800cfa:	5d                   	pop    %ebp
  800cfb:	c3                   	ret    
  800cfc:	3b 04 24             	cmp    (%esp),%eax
  800cff:	72 06                	jb     800d07 <__umoddi3+0x113>
  800d01:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800d05:	77 0f                	ja     800d16 <__umoddi3+0x122>
  800d07:	89 f2                	mov    %esi,%edx
  800d09:	29 f9                	sub    %edi,%ecx
  800d0b:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800d0f:	89 14 24             	mov    %edx,(%esp)
  800d12:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800d16:	8b 44 24 04          	mov    0x4(%esp),%eax
  800d1a:	8b 14 24             	mov    (%esp),%edx
  800d1d:	83 c4 1c             	add    $0x1c,%esp
  800d20:	5b                   	pop    %ebx
  800d21:	5e                   	pop    %esi
  800d22:	5f                   	pop    %edi
  800d23:	5d                   	pop    %ebp
  800d24:	c3                   	ret    
  800d25:	8d 76 00             	lea    0x0(%esi),%esi
  800d28:	2b 04 24             	sub    (%esp),%eax
  800d2b:	19 fa                	sbb    %edi,%edx
  800d2d:	89 d1                	mov    %edx,%ecx
  800d2f:	89 c6                	mov    %eax,%esi
  800d31:	e9 71 ff ff ff       	jmp    800ca7 <__umoddi3+0xb3>
  800d36:	66 90                	xchg   %ax,%ax
  800d38:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d3c:	72 ea                	jb     800d28 <__umoddi3+0x134>
  800d3e:	89 d9                	mov    %ebx,%ecx
  800d40:	e9 62 ff ff ff       	jmp    800ca7 <__umoddi3+0xb3>
