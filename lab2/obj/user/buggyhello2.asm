
obj/user/buggyhello2:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 1d 00 00 00       	call   80004e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

const char *hello = "hello, world\n";

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	sys_cputs(hello, 1024*1024);
  800039:	68 00 00 10 00       	push   $0x100000
  80003e:	ff 35 00 20 80 00    	pushl  0x802000
  800044:	e8 bf 09 00 00       	call   800a08 <sys_cputs>
}
  800049:	83 c4 10             	add    $0x10,%esp
  80004c:	c9                   	leave  
  80004d:	c3                   	ret    

0080004e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80004e:	55                   	push   %ebp
  80004f:	89 e5                	mov    %esp,%ebp
  800051:	57                   	push   %edi
  800052:	56                   	push   %esi
  800053:	53                   	push   %ebx
  800054:	83 ec 0c             	sub    $0xc,%esp
  800057:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80005a:	8b 75 0c             	mov    0xc(%ebp),%esi
	// set thisenv to point at our Env structure in envs[].
	// LAB 3: Your code here.
	//thisenv = 0;
	int32_t env_Index1 = (int32_t)sys_getenvid();
  80005d:	e8 24 0a 00 00       	call   800a86 <sys_getenvid>
  800062:	89 c7                	mov    %eax,%edi
	cprintf("printing env_Index1: %d\n", env_Index1);
  800064:	83 ec 08             	sub    $0x8,%esp
  800067:	50                   	push   %eax
  800068:	68 5e 0d 80 00       	push   $0x800d5e
  80006d:	e8 0a 01 00 00       	call   80017c <cprintf>
	int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	cprintf("printing env_Index2: %d\n", env_Index2);
  800072:	83 c4 08             	add    $0x8,%esp
  800075:	81 e7 ff 03 00 00    	and    $0x3ff,%edi
  80007b:	57                   	push   %edi
  80007c:	68 77 0d 80 00       	push   $0x800d77
  800081:	e8 f6 00 00 00       	call   80017c <cprintf>

	thisenv = &envs[ENVX(sys_getenvid())];
  800086:	e8 fb 09 00 00       	call   800a86 <sys_getenvid>
  80008b:	25 ff 03 00 00       	and    $0x3ff,%eax
  800090:	8d 14 00             	lea    (%eax,%eax,1),%edx
  800093:	01 d0                	add    %edx,%eax
  800095:	c1 e0 05             	shl    $0x5,%eax
  800098:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80009d:	a3 08 20 80 00       	mov    %eax,0x802008
	cprintf("Addrs of thisenv (envs[0]) is: %p\n", thisenv);
  8000a2:	83 c4 08             	add    $0x8,%esp
  8000a5:	50                   	push   %eax
  8000a6:	68 9c 0d 80 00       	push   $0x800d9c
  8000ab:	e8 cc 00 00 00       	call   80017c <cprintf>
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000b0:	83 c4 10             	add    $0x10,%esp
  8000b3:	85 db                	test   %ebx,%ebx
  8000b5:	7e 07                	jle    8000be <libmain+0x70>
		binaryname = argv[0];
  8000b7:	8b 06                	mov    (%esi),%eax
  8000b9:	a3 04 20 80 00       	mov    %eax,0x802004

	// call user main routine
	umain(argc, argv);
  8000be:	83 ec 08             	sub    $0x8,%esp
  8000c1:	56                   	push   %esi
  8000c2:	53                   	push   %ebx
  8000c3:	e8 6b ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  8000c8:	e8 0b 00 00 00       	call   8000d8 <exit>
}
  8000cd:	83 c4 10             	add    $0x10,%esp
  8000d0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8000d3:	5b                   	pop    %ebx
  8000d4:	5e                   	pop    %esi
  8000d5:	5f                   	pop    %edi
  8000d6:	5d                   	pop    %ebp
  8000d7:	c3                   	ret    

008000d8 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000d8:	55                   	push   %ebp
  8000d9:	89 e5                	mov    %esp,%ebp
  8000db:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000de:	6a 00                	push   $0x0
  8000e0:	e8 60 09 00 00       	call   800a45 <sys_env_destroy>
}
  8000e5:	83 c4 10             	add    $0x10,%esp
  8000e8:	c9                   	leave  
  8000e9:	c3                   	ret    

008000ea <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000ea:	55                   	push   %ebp
  8000eb:	89 e5                	mov    %esp,%ebp
  8000ed:	53                   	push   %ebx
  8000ee:	83 ec 04             	sub    $0x4,%esp
  8000f1:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000f4:	8b 13                	mov    (%ebx),%edx
  8000f6:	8d 42 01             	lea    0x1(%edx),%eax
  8000f9:	89 03                	mov    %eax,(%ebx)
  8000fb:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000fe:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800102:	3d ff 00 00 00       	cmp    $0xff,%eax
  800107:	75 1a                	jne    800123 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800109:	83 ec 08             	sub    $0x8,%esp
  80010c:	68 ff 00 00 00       	push   $0xff
  800111:	8d 43 08             	lea    0x8(%ebx),%eax
  800114:	50                   	push   %eax
  800115:	e8 ee 08 00 00       	call   800a08 <sys_cputs>
		b->idx = 0;
  80011a:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  800120:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  800123:	ff 43 04             	incl   0x4(%ebx)
}
  800126:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800129:	c9                   	leave  
  80012a:	c3                   	ret    

0080012b <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80012b:	55                   	push   %ebp
  80012c:	89 e5                	mov    %esp,%ebp
  80012e:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800134:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80013b:	00 00 00 
	b.cnt = 0;
  80013e:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800145:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800148:	ff 75 0c             	pushl  0xc(%ebp)
  80014b:	ff 75 08             	pushl  0x8(%ebp)
  80014e:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800154:	50                   	push   %eax
  800155:	68 ea 00 80 00       	push   $0x8000ea
  80015a:	e8 51 01 00 00       	call   8002b0 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80015f:	83 c4 08             	add    $0x8,%esp
  800162:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800168:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80016e:	50                   	push   %eax
  80016f:	e8 94 08 00 00       	call   800a08 <sys_cputs>

	return b.cnt;
}
  800174:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80017a:	c9                   	leave  
  80017b:	c3                   	ret    

0080017c <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80017c:	55                   	push   %ebp
  80017d:	89 e5                	mov    %esp,%ebp
  80017f:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800182:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800185:	50                   	push   %eax
  800186:	ff 75 08             	pushl  0x8(%ebp)
  800189:	e8 9d ff ff ff       	call   80012b <vcprintf>
	va_end(ap);

	return cnt;
}
  80018e:	c9                   	leave  
  80018f:	c3                   	ret    

00800190 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800190:	55                   	push   %ebp
  800191:	89 e5                	mov    %esp,%ebp
  800193:	57                   	push   %edi
  800194:	56                   	push   %esi
  800195:	53                   	push   %ebx
  800196:	83 ec 1c             	sub    $0x1c,%esp
  800199:	89 c7                	mov    %eax,%edi
  80019b:	89 d6                	mov    %edx,%esi
  80019d:	8b 45 08             	mov    0x8(%ebp),%eax
  8001a0:	8b 55 0c             	mov    0xc(%ebp),%edx
  8001a3:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8001a6:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001a9:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8001ac:	bb 00 00 00 00       	mov    $0x0,%ebx
  8001b1:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  8001b4:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  8001b7:	39 d3                	cmp    %edx,%ebx
  8001b9:	72 05                	jb     8001c0 <printnum+0x30>
  8001bb:	39 45 10             	cmp    %eax,0x10(%ebp)
  8001be:	77 45                	ja     800205 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001c0:	83 ec 0c             	sub    $0xc,%esp
  8001c3:	ff 75 18             	pushl  0x18(%ebp)
  8001c6:	8b 45 14             	mov    0x14(%ebp),%eax
  8001c9:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001cc:	53                   	push   %ebx
  8001cd:	ff 75 10             	pushl  0x10(%ebp)
  8001d0:	83 ec 08             	sub    $0x8,%esp
  8001d3:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001d6:	ff 75 e0             	pushl  -0x20(%ebp)
  8001d9:	ff 75 dc             	pushl  -0x24(%ebp)
  8001dc:	ff 75 d8             	pushl  -0x28(%ebp)
  8001df:	e8 08 09 00 00       	call   800aec <__udivdi3>
  8001e4:	83 c4 18             	add    $0x18,%esp
  8001e7:	52                   	push   %edx
  8001e8:	50                   	push   %eax
  8001e9:	89 f2                	mov    %esi,%edx
  8001eb:	89 f8                	mov    %edi,%eax
  8001ed:	e8 9e ff ff ff       	call   800190 <printnum>
  8001f2:	83 c4 20             	add    $0x20,%esp
  8001f5:	eb 16                	jmp    80020d <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001f7:	83 ec 08             	sub    $0x8,%esp
  8001fa:	56                   	push   %esi
  8001fb:	ff 75 18             	pushl  0x18(%ebp)
  8001fe:	ff d7                	call   *%edi
  800200:	83 c4 10             	add    $0x10,%esp
  800203:	eb 03                	jmp    800208 <printnum+0x78>
  800205:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800208:	4b                   	dec    %ebx
  800209:	85 db                	test   %ebx,%ebx
  80020b:	7f ea                	jg     8001f7 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80020d:	83 ec 08             	sub    $0x8,%esp
  800210:	56                   	push   %esi
  800211:	83 ec 04             	sub    $0x4,%esp
  800214:	ff 75 e4             	pushl  -0x1c(%ebp)
  800217:	ff 75 e0             	pushl  -0x20(%ebp)
  80021a:	ff 75 dc             	pushl  -0x24(%ebp)
  80021d:	ff 75 d8             	pushl  -0x28(%ebp)
  800220:	e8 d7 09 00 00       	call   800bfc <__umoddi3>
  800225:	83 c4 14             	add    $0x14,%esp
  800228:	0f be 80 bf 0d 80 00 	movsbl 0x800dbf(%eax),%eax
  80022f:	50                   	push   %eax
  800230:	ff d7                	call   *%edi
}
  800232:	83 c4 10             	add    $0x10,%esp
  800235:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800238:	5b                   	pop    %ebx
  800239:	5e                   	pop    %esi
  80023a:	5f                   	pop    %edi
  80023b:	5d                   	pop    %ebp
  80023c:	c3                   	ret    

0080023d <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80023d:	55                   	push   %ebp
  80023e:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800240:	83 fa 01             	cmp    $0x1,%edx
  800243:	7e 0e                	jle    800253 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800245:	8b 10                	mov    (%eax),%edx
  800247:	8d 4a 08             	lea    0x8(%edx),%ecx
  80024a:	89 08                	mov    %ecx,(%eax)
  80024c:	8b 02                	mov    (%edx),%eax
  80024e:	8b 52 04             	mov    0x4(%edx),%edx
  800251:	eb 22                	jmp    800275 <getuint+0x38>
	else if (lflag)
  800253:	85 d2                	test   %edx,%edx
  800255:	74 10                	je     800267 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800257:	8b 10                	mov    (%eax),%edx
  800259:	8d 4a 04             	lea    0x4(%edx),%ecx
  80025c:	89 08                	mov    %ecx,(%eax)
  80025e:	8b 02                	mov    (%edx),%eax
  800260:	ba 00 00 00 00       	mov    $0x0,%edx
  800265:	eb 0e                	jmp    800275 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800267:	8b 10                	mov    (%eax),%edx
  800269:	8d 4a 04             	lea    0x4(%edx),%ecx
  80026c:	89 08                	mov    %ecx,(%eax)
  80026e:	8b 02                	mov    (%edx),%eax
  800270:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800275:	5d                   	pop    %ebp
  800276:	c3                   	ret    

00800277 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800277:	55                   	push   %ebp
  800278:	89 e5                	mov    %esp,%ebp
  80027a:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80027d:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800280:	8b 10                	mov    (%eax),%edx
  800282:	3b 50 04             	cmp    0x4(%eax),%edx
  800285:	73 0a                	jae    800291 <sprintputch+0x1a>
		*b->buf++ = ch;
  800287:	8d 4a 01             	lea    0x1(%edx),%ecx
  80028a:	89 08                	mov    %ecx,(%eax)
  80028c:	8b 45 08             	mov    0x8(%ebp),%eax
  80028f:	88 02                	mov    %al,(%edx)
}
  800291:	5d                   	pop    %ebp
  800292:	c3                   	ret    

00800293 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800293:	55                   	push   %ebp
  800294:	89 e5                	mov    %esp,%ebp
  800296:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800299:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80029c:	50                   	push   %eax
  80029d:	ff 75 10             	pushl  0x10(%ebp)
  8002a0:	ff 75 0c             	pushl  0xc(%ebp)
  8002a3:	ff 75 08             	pushl  0x8(%ebp)
  8002a6:	e8 05 00 00 00       	call   8002b0 <vprintfmt>
	va_end(ap);
}
  8002ab:	83 c4 10             	add    $0x10,%esp
  8002ae:	c9                   	leave  
  8002af:	c3                   	ret    

008002b0 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8002b0:	55                   	push   %ebp
  8002b1:	89 e5                	mov    %esp,%ebp
  8002b3:	57                   	push   %edi
  8002b4:	56                   	push   %esi
  8002b5:	53                   	push   %ebx
  8002b6:	83 ec 2c             	sub    $0x2c,%esp
  8002b9:	8b 75 08             	mov    0x8(%ebp),%esi
  8002bc:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  8002bf:	8b 7d 10             	mov    0x10(%ebp),%edi
  8002c2:	eb 12                	jmp    8002d6 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  8002c4:	85 c0                	test   %eax,%eax
  8002c6:	0f 84 68 03 00 00    	je     800634 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  8002cc:	83 ec 08             	sub    $0x8,%esp
  8002cf:	53                   	push   %ebx
  8002d0:	50                   	push   %eax
  8002d1:	ff d6                	call   *%esi
  8002d3:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002d6:	47                   	inc    %edi
  8002d7:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002db:	83 f8 25             	cmp    $0x25,%eax
  8002de:	75 e4                	jne    8002c4 <vprintfmt+0x14>
  8002e0:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002e4:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002eb:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002f2:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002f9:	ba 00 00 00 00       	mov    $0x0,%edx
  8002fe:	eb 07                	jmp    800307 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800300:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800303:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800307:	8d 47 01             	lea    0x1(%edi),%eax
  80030a:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80030d:	0f b6 0f             	movzbl (%edi),%ecx
  800310:	8a 07                	mov    (%edi),%al
  800312:	83 e8 23             	sub    $0x23,%eax
  800315:	3c 55                	cmp    $0x55,%al
  800317:	0f 87 fe 02 00 00    	ja     80061b <vprintfmt+0x36b>
  80031d:	0f b6 c0             	movzbl %al,%eax
  800320:	ff 24 85 4c 0e 80 00 	jmp    *0x800e4c(,%eax,4)
  800327:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80032a:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  80032e:	eb d7                	jmp    800307 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800330:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800333:	b8 00 00 00 00       	mov    $0x0,%eax
  800338:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80033b:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80033e:	01 c0                	add    %eax,%eax
  800340:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800344:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800347:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80034a:	83 fa 09             	cmp    $0x9,%edx
  80034d:	77 34                	ja     800383 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80034f:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800350:	eb e9                	jmp    80033b <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800352:	8b 45 14             	mov    0x14(%ebp),%eax
  800355:	8d 48 04             	lea    0x4(%eax),%ecx
  800358:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80035b:	8b 00                	mov    (%eax),%eax
  80035d:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800360:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800363:	eb 24                	jmp    800389 <vprintfmt+0xd9>
  800365:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800369:	79 07                	jns    800372 <vprintfmt+0xc2>
  80036b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800372:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800375:	eb 90                	jmp    800307 <vprintfmt+0x57>
  800377:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80037a:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800381:	eb 84                	jmp    800307 <vprintfmt+0x57>
  800383:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800386:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800389:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80038d:	0f 89 74 ff ff ff    	jns    800307 <vprintfmt+0x57>
				width = precision, precision = -1;
  800393:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800396:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800399:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003a0:	e9 62 ff ff ff       	jmp    800307 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8003a5:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003a6:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  8003a9:	e9 59 ff ff ff       	jmp    800307 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8003ae:	8b 45 14             	mov    0x14(%ebp),%eax
  8003b1:	8d 50 04             	lea    0x4(%eax),%edx
  8003b4:	89 55 14             	mov    %edx,0x14(%ebp)
  8003b7:	83 ec 08             	sub    $0x8,%esp
  8003ba:	53                   	push   %ebx
  8003bb:	ff 30                	pushl  (%eax)
  8003bd:	ff d6                	call   *%esi
			break;
  8003bf:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003c2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  8003c5:	e9 0c ff ff ff       	jmp    8002d6 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8003ca:	8b 45 14             	mov    0x14(%ebp),%eax
  8003cd:	8d 50 04             	lea    0x4(%eax),%edx
  8003d0:	89 55 14             	mov    %edx,0x14(%ebp)
  8003d3:	8b 00                	mov    (%eax),%eax
  8003d5:	85 c0                	test   %eax,%eax
  8003d7:	79 02                	jns    8003db <vprintfmt+0x12b>
  8003d9:	f7 d8                	neg    %eax
  8003db:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003dd:	83 f8 06             	cmp    $0x6,%eax
  8003e0:	7f 0b                	jg     8003ed <vprintfmt+0x13d>
  8003e2:	8b 04 85 a4 0f 80 00 	mov    0x800fa4(,%eax,4),%eax
  8003e9:	85 c0                	test   %eax,%eax
  8003eb:	75 18                	jne    800405 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003ed:	52                   	push   %edx
  8003ee:	68 d7 0d 80 00       	push   $0x800dd7
  8003f3:	53                   	push   %ebx
  8003f4:	56                   	push   %esi
  8003f5:	e8 99 fe ff ff       	call   800293 <printfmt>
  8003fa:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003fd:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800400:	e9 d1 fe ff ff       	jmp    8002d6 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800405:	50                   	push   %eax
  800406:	68 e0 0d 80 00       	push   $0x800de0
  80040b:	53                   	push   %ebx
  80040c:	56                   	push   %esi
  80040d:	e8 81 fe ff ff       	call   800293 <printfmt>
  800412:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800415:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800418:	e9 b9 fe ff ff       	jmp    8002d6 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80041d:	8b 45 14             	mov    0x14(%ebp),%eax
  800420:	8d 50 04             	lea    0x4(%eax),%edx
  800423:	89 55 14             	mov    %edx,0x14(%ebp)
  800426:	8b 38                	mov    (%eax),%edi
  800428:	85 ff                	test   %edi,%edi
  80042a:	75 05                	jne    800431 <vprintfmt+0x181>
				p = "(null)";
  80042c:	bf d0 0d 80 00       	mov    $0x800dd0,%edi
			if (width > 0 && padc != '-')
  800431:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800435:	0f 8e 90 00 00 00    	jle    8004cb <vprintfmt+0x21b>
  80043b:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80043f:	0f 84 8e 00 00 00    	je     8004d3 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800445:	83 ec 08             	sub    $0x8,%esp
  800448:	ff 75 d0             	pushl  -0x30(%ebp)
  80044b:	57                   	push   %edi
  80044c:	e8 70 02 00 00       	call   8006c1 <strnlen>
  800451:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800454:	29 c1                	sub    %eax,%ecx
  800456:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800459:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80045c:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800460:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800463:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800466:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800468:	eb 0d                	jmp    800477 <vprintfmt+0x1c7>
					putch(padc, putdat);
  80046a:	83 ec 08             	sub    $0x8,%esp
  80046d:	53                   	push   %ebx
  80046e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800471:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800473:	4f                   	dec    %edi
  800474:	83 c4 10             	add    $0x10,%esp
  800477:	85 ff                	test   %edi,%edi
  800479:	7f ef                	jg     80046a <vprintfmt+0x1ba>
  80047b:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80047e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800481:	89 c8                	mov    %ecx,%eax
  800483:	85 c9                	test   %ecx,%ecx
  800485:	79 05                	jns    80048c <vprintfmt+0x1dc>
  800487:	b8 00 00 00 00       	mov    $0x0,%eax
  80048c:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80048f:	29 c1                	sub    %eax,%ecx
  800491:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800494:	89 75 08             	mov    %esi,0x8(%ebp)
  800497:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80049a:	eb 3d                	jmp    8004d9 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80049c:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  8004a0:	74 19                	je     8004bb <vprintfmt+0x20b>
  8004a2:	0f be c0             	movsbl %al,%eax
  8004a5:	83 e8 20             	sub    $0x20,%eax
  8004a8:	83 f8 5e             	cmp    $0x5e,%eax
  8004ab:	76 0e                	jbe    8004bb <vprintfmt+0x20b>
					putch('?', putdat);
  8004ad:	83 ec 08             	sub    $0x8,%esp
  8004b0:	53                   	push   %ebx
  8004b1:	6a 3f                	push   $0x3f
  8004b3:	ff 55 08             	call   *0x8(%ebp)
  8004b6:	83 c4 10             	add    $0x10,%esp
  8004b9:	eb 0b                	jmp    8004c6 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  8004bb:	83 ec 08             	sub    $0x8,%esp
  8004be:	53                   	push   %ebx
  8004bf:	52                   	push   %edx
  8004c0:	ff 55 08             	call   *0x8(%ebp)
  8004c3:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004c6:	ff 4d e4             	decl   -0x1c(%ebp)
  8004c9:	eb 0e                	jmp    8004d9 <vprintfmt+0x229>
  8004cb:	89 75 08             	mov    %esi,0x8(%ebp)
  8004ce:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004d1:	eb 06                	jmp    8004d9 <vprintfmt+0x229>
  8004d3:	89 75 08             	mov    %esi,0x8(%ebp)
  8004d6:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004d9:	47                   	inc    %edi
  8004da:	8a 47 ff             	mov    -0x1(%edi),%al
  8004dd:	0f be d0             	movsbl %al,%edx
  8004e0:	85 d2                	test   %edx,%edx
  8004e2:	74 1d                	je     800501 <vprintfmt+0x251>
  8004e4:	85 f6                	test   %esi,%esi
  8004e6:	78 b4                	js     80049c <vprintfmt+0x1ec>
  8004e8:	4e                   	dec    %esi
  8004e9:	79 b1                	jns    80049c <vprintfmt+0x1ec>
  8004eb:	8b 75 08             	mov    0x8(%ebp),%esi
  8004ee:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004f1:	eb 14                	jmp    800507 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004f3:	83 ec 08             	sub    $0x8,%esp
  8004f6:	53                   	push   %ebx
  8004f7:	6a 20                	push   $0x20
  8004f9:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004fb:	4f                   	dec    %edi
  8004fc:	83 c4 10             	add    $0x10,%esp
  8004ff:	eb 06                	jmp    800507 <vprintfmt+0x257>
  800501:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800504:	8b 75 08             	mov    0x8(%ebp),%esi
  800507:	85 ff                	test   %edi,%edi
  800509:	7f e8                	jg     8004f3 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80050b:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80050e:	e9 c3 fd ff ff       	jmp    8002d6 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  800513:	83 fa 01             	cmp    $0x1,%edx
  800516:	7e 16                	jle    80052e <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  800518:	8b 45 14             	mov    0x14(%ebp),%eax
  80051b:	8d 50 08             	lea    0x8(%eax),%edx
  80051e:	89 55 14             	mov    %edx,0x14(%ebp)
  800521:	8b 50 04             	mov    0x4(%eax),%edx
  800524:	8b 00                	mov    (%eax),%eax
  800526:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800529:	89 55 dc             	mov    %edx,-0x24(%ebp)
  80052c:	eb 32                	jmp    800560 <vprintfmt+0x2b0>
	else if (lflag)
  80052e:	85 d2                	test   %edx,%edx
  800530:	74 18                	je     80054a <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800532:	8b 45 14             	mov    0x14(%ebp),%eax
  800535:	8d 50 04             	lea    0x4(%eax),%edx
  800538:	89 55 14             	mov    %edx,0x14(%ebp)
  80053b:	8b 00                	mov    (%eax),%eax
  80053d:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800540:	89 c1                	mov    %eax,%ecx
  800542:	c1 f9 1f             	sar    $0x1f,%ecx
  800545:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800548:	eb 16                	jmp    800560 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80054a:	8b 45 14             	mov    0x14(%ebp),%eax
  80054d:	8d 50 04             	lea    0x4(%eax),%edx
  800550:	89 55 14             	mov    %edx,0x14(%ebp)
  800553:	8b 00                	mov    (%eax),%eax
  800555:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800558:	89 c1                	mov    %eax,%ecx
  80055a:	c1 f9 1f             	sar    $0x1f,%ecx
  80055d:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800560:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800563:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800566:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80056a:	79 76                	jns    8005e2 <vprintfmt+0x332>
				putch('-', putdat);
  80056c:	83 ec 08             	sub    $0x8,%esp
  80056f:	53                   	push   %ebx
  800570:	6a 2d                	push   $0x2d
  800572:	ff d6                	call   *%esi
				num = -(long long) num;
  800574:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800577:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80057a:	f7 d8                	neg    %eax
  80057c:	83 d2 00             	adc    $0x0,%edx
  80057f:	f7 da                	neg    %edx
  800581:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800584:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800589:	eb 5c                	jmp    8005e7 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80058b:	8d 45 14             	lea    0x14(%ebp),%eax
  80058e:	e8 aa fc ff ff       	call   80023d <getuint>
			base = 10;
  800593:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800598:	eb 4d                	jmp    8005e7 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80059a:	8d 45 14             	lea    0x14(%ebp),%eax
  80059d:	e8 9b fc ff ff       	call   80023d <getuint>
			base = 8;
  8005a2:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  8005a7:	eb 3e                	jmp    8005e7 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  8005a9:	83 ec 08             	sub    $0x8,%esp
  8005ac:	53                   	push   %ebx
  8005ad:	6a 30                	push   $0x30
  8005af:	ff d6                	call   *%esi
			putch('x', putdat);
  8005b1:	83 c4 08             	add    $0x8,%esp
  8005b4:	53                   	push   %ebx
  8005b5:	6a 78                	push   $0x78
  8005b7:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  8005b9:	8b 45 14             	mov    0x14(%ebp),%eax
  8005bc:	8d 50 04             	lea    0x4(%eax),%edx
  8005bf:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8005c2:	8b 00                	mov    (%eax),%eax
  8005c4:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  8005c9:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  8005cc:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005d1:	eb 14                	jmp    8005e7 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005d3:	8d 45 14             	lea    0x14(%ebp),%eax
  8005d6:	e8 62 fc ff ff       	call   80023d <getuint>
			base = 16;
  8005db:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005e0:	eb 05                	jmp    8005e7 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005e2:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005e7:	83 ec 0c             	sub    $0xc,%esp
  8005ea:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005ee:	57                   	push   %edi
  8005ef:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005f2:	51                   	push   %ecx
  8005f3:	52                   	push   %edx
  8005f4:	50                   	push   %eax
  8005f5:	89 da                	mov    %ebx,%edx
  8005f7:	89 f0                	mov    %esi,%eax
  8005f9:	e8 92 fb ff ff       	call   800190 <printnum>
			break;
  8005fe:	83 c4 20             	add    $0x20,%esp
  800601:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800604:	e9 cd fc ff ff       	jmp    8002d6 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800609:	83 ec 08             	sub    $0x8,%esp
  80060c:	53                   	push   %ebx
  80060d:	51                   	push   %ecx
  80060e:	ff d6                	call   *%esi
			break;
  800610:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800613:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  800616:	e9 bb fc ff ff       	jmp    8002d6 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80061b:	83 ec 08             	sub    $0x8,%esp
  80061e:	53                   	push   %ebx
  80061f:	6a 25                	push   $0x25
  800621:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  800623:	83 c4 10             	add    $0x10,%esp
  800626:	eb 01                	jmp    800629 <vprintfmt+0x379>
  800628:	4f                   	dec    %edi
  800629:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  80062d:	75 f9                	jne    800628 <vprintfmt+0x378>
  80062f:	e9 a2 fc ff ff       	jmp    8002d6 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800634:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800637:	5b                   	pop    %ebx
  800638:	5e                   	pop    %esi
  800639:	5f                   	pop    %edi
  80063a:	5d                   	pop    %ebp
  80063b:	c3                   	ret    

0080063c <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80063c:	55                   	push   %ebp
  80063d:	89 e5                	mov    %esp,%ebp
  80063f:	83 ec 18             	sub    $0x18,%esp
  800642:	8b 45 08             	mov    0x8(%ebp),%eax
  800645:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800648:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80064b:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80064f:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800652:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800659:	85 c0                	test   %eax,%eax
  80065b:	74 26                	je     800683 <vsnprintf+0x47>
  80065d:	85 d2                	test   %edx,%edx
  80065f:	7e 29                	jle    80068a <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800661:	ff 75 14             	pushl  0x14(%ebp)
  800664:	ff 75 10             	pushl  0x10(%ebp)
  800667:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80066a:	50                   	push   %eax
  80066b:	68 77 02 80 00       	push   $0x800277
  800670:	e8 3b fc ff ff       	call   8002b0 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800675:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800678:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80067b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80067e:	83 c4 10             	add    $0x10,%esp
  800681:	eb 0c                	jmp    80068f <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800683:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800688:	eb 05                	jmp    80068f <vsnprintf+0x53>
  80068a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80068f:	c9                   	leave  
  800690:	c3                   	ret    

00800691 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800691:	55                   	push   %ebp
  800692:	89 e5                	mov    %esp,%ebp
  800694:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800697:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80069a:	50                   	push   %eax
  80069b:	ff 75 10             	pushl  0x10(%ebp)
  80069e:	ff 75 0c             	pushl  0xc(%ebp)
  8006a1:	ff 75 08             	pushl  0x8(%ebp)
  8006a4:	e8 93 ff ff ff       	call   80063c <vsnprintf>
	va_end(ap);

	return rc;
}
  8006a9:	c9                   	leave  
  8006aa:	c3                   	ret    

008006ab <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  8006ab:	55                   	push   %ebp
  8006ac:	89 e5                	mov    %esp,%ebp
  8006ae:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  8006b1:	b8 00 00 00 00       	mov    $0x0,%eax
  8006b6:	eb 01                	jmp    8006b9 <strlen+0xe>
		n++;
  8006b8:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8006b9:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  8006bd:	75 f9                	jne    8006b8 <strlen+0xd>
		n++;
	return n;
}
  8006bf:	5d                   	pop    %ebp
  8006c0:	c3                   	ret    

008006c1 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  8006c1:	55                   	push   %ebp
  8006c2:	89 e5                	mov    %esp,%ebp
  8006c4:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8006c7:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006ca:	ba 00 00 00 00       	mov    $0x0,%edx
  8006cf:	eb 01                	jmp    8006d2 <strnlen+0x11>
		n++;
  8006d1:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006d2:	39 c2                	cmp    %eax,%edx
  8006d4:	74 08                	je     8006de <strnlen+0x1d>
  8006d6:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006da:	75 f5                	jne    8006d1 <strnlen+0x10>
  8006dc:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006de:	5d                   	pop    %ebp
  8006df:	c3                   	ret    

008006e0 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006e0:	55                   	push   %ebp
  8006e1:	89 e5                	mov    %esp,%ebp
  8006e3:	53                   	push   %ebx
  8006e4:	8b 45 08             	mov    0x8(%ebp),%eax
  8006e7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006ea:	89 c2                	mov    %eax,%edx
  8006ec:	42                   	inc    %edx
  8006ed:	41                   	inc    %ecx
  8006ee:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006f1:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006f4:	84 db                	test   %bl,%bl
  8006f6:	75 f4                	jne    8006ec <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006f8:	5b                   	pop    %ebx
  8006f9:	5d                   	pop    %ebp
  8006fa:	c3                   	ret    

008006fb <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006fb:	55                   	push   %ebp
  8006fc:	89 e5                	mov    %esp,%ebp
  8006fe:	53                   	push   %ebx
  8006ff:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800702:	53                   	push   %ebx
  800703:	e8 a3 ff ff ff       	call   8006ab <strlen>
  800708:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80070b:	ff 75 0c             	pushl  0xc(%ebp)
  80070e:	01 d8                	add    %ebx,%eax
  800710:	50                   	push   %eax
  800711:	e8 ca ff ff ff       	call   8006e0 <strcpy>
	return dst;
}
  800716:	89 d8                	mov    %ebx,%eax
  800718:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80071b:	c9                   	leave  
  80071c:	c3                   	ret    

0080071d <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  80071d:	55                   	push   %ebp
  80071e:	89 e5                	mov    %esp,%ebp
  800720:	56                   	push   %esi
  800721:	53                   	push   %ebx
  800722:	8b 75 08             	mov    0x8(%ebp),%esi
  800725:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800728:	89 f3                	mov    %esi,%ebx
  80072a:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80072d:	89 f2                	mov    %esi,%edx
  80072f:	eb 0c                	jmp    80073d <strncpy+0x20>
		*dst++ = *src;
  800731:	42                   	inc    %edx
  800732:	8a 01                	mov    (%ecx),%al
  800734:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800737:	80 39 01             	cmpb   $0x1,(%ecx)
  80073a:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80073d:	39 da                	cmp    %ebx,%edx
  80073f:	75 f0                	jne    800731 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800741:	89 f0                	mov    %esi,%eax
  800743:	5b                   	pop    %ebx
  800744:	5e                   	pop    %esi
  800745:	5d                   	pop    %ebp
  800746:	c3                   	ret    

00800747 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800747:	55                   	push   %ebp
  800748:	89 e5                	mov    %esp,%ebp
  80074a:	56                   	push   %esi
  80074b:	53                   	push   %ebx
  80074c:	8b 75 08             	mov    0x8(%ebp),%esi
  80074f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800752:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800755:	85 c0                	test   %eax,%eax
  800757:	74 1e                	je     800777 <strlcpy+0x30>
  800759:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  80075d:	89 f2                	mov    %esi,%edx
  80075f:	eb 05                	jmp    800766 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800761:	42                   	inc    %edx
  800762:	41                   	inc    %ecx
  800763:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800766:	39 c2                	cmp    %eax,%edx
  800768:	74 08                	je     800772 <strlcpy+0x2b>
  80076a:	8a 19                	mov    (%ecx),%bl
  80076c:	84 db                	test   %bl,%bl
  80076e:	75 f1                	jne    800761 <strlcpy+0x1a>
  800770:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800772:	c6 00 00             	movb   $0x0,(%eax)
  800775:	eb 02                	jmp    800779 <strlcpy+0x32>
  800777:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800779:	29 f0                	sub    %esi,%eax
}
  80077b:	5b                   	pop    %ebx
  80077c:	5e                   	pop    %esi
  80077d:	5d                   	pop    %ebp
  80077e:	c3                   	ret    

0080077f <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80077f:	55                   	push   %ebp
  800780:	89 e5                	mov    %esp,%ebp
  800782:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800785:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800788:	eb 02                	jmp    80078c <strcmp+0xd>
		p++, q++;
  80078a:	41                   	inc    %ecx
  80078b:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80078c:	8a 01                	mov    (%ecx),%al
  80078e:	84 c0                	test   %al,%al
  800790:	74 04                	je     800796 <strcmp+0x17>
  800792:	3a 02                	cmp    (%edx),%al
  800794:	74 f4                	je     80078a <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800796:	0f b6 c0             	movzbl %al,%eax
  800799:	0f b6 12             	movzbl (%edx),%edx
  80079c:	29 d0                	sub    %edx,%eax
}
  80079e:	5d                   	pop    %ebp
  80079f:	c3                   	ret    

008007a0 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  8007a0:	55                   	push   %ebp
  8007a1:	89 e5                	mov    %esp,%ebp
  8007a3:	53                   	push   %ebx
  8007a4:	8b 45 08             	mov    0x8(%ebp),%eax
  8007a7:	8b 55 0c             	mov    0xc(%ebp),%edx
  8007aa:	89 c3                	mov    %eax,%ebx
  8007ac:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  8007af:	eb 02                	jmp    8007b3 <strncmp+0x13>
		n--, p++, q++;
  8007b1:	40                   	inc    %eax
  8007b2:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  8007b3:	39 d8                	cmp    %ebx,%eax
  8007b5:	74 14                	je     8007cb <strncmp+0x2b>
  8007b7:	8a 08                	mov    (%eax),%cl
  8007b9:	84 c9                	test   %cl,%cl
  8007bb:	74 04                	je     8007c1 <strncmp+0x21>
  8007bd:	3a 0a                	cmp    (%edx),%cl
  8007bf:	74 f0                	je     8007b1 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8007c1:	0f b6 00             	movzbl (%eax),%eax
  8007c4:	0f b6 12             	movzbl (%edx),%edx
  8007c7:	29 d0                	sub    %edx,%eax
  8007c9:	eb 05                	jmp    8007d0 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  8007cb:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007d0:	5b                   	pop    %ebx
  8007d1:	5d                   	pop    %ebp
  8007d2:	c3                   	ret    

008007d3 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007d3:	55                   	push   %ebp
  8007d4:	89 e5                	mov    %esp,%ebp
  8007d6:	8b 45 08             	mov    0x8(%ebp),%eax
  8007d9:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007dc:	eb 05                	jmp    8007e3 <strchr+0x10>
		if (*s == c)
  8007de:	38 ca                	cmp    %cl,%dl
  8007e0:	74 0c                	je     8007ee <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007e2:	40                   	inc    %eax
  8007e3:	8a 10                	mov    (%eax),%dl
  8007e5:	84 d2                	test   %dl,%dl
  8007e7:	75 f5                	jne    8007de <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007e9:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007ee:	5d                   	pop    %ebp
  8007ef:	c3                   	ret    

008007f0 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007f0:	55                   	push   %ebp
  8007f1:	89 e5                	mov    %esp,%ebp
  8007f3:	8b 45 08             	mov    0x8(%ebp),%eax
  8007f6:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007f9:	eb 05                	jmp    800800 <strfind+0x10>
		if (*s == c)
  8007fb:	38 ca                	cmp    %cl,%dl
  8007fd:	74 07                	je     800806 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007ff:	40                   	inc    %eax
  800800:	8a 10                	mov    (%eax),%dl
  800802:	84 d2                	test   %dl,%dl
  800804:	75 f5                	jne    8007fb <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800806:	5d                   	pop    %ebp
  800807:	c3                   	ret    

00800808 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800808:	55                   	push   %ebp
  800809:	89 e5                	mov    %esp,%ebp
  80080b:	57                   	push   %edi
  80080c:	56                   	push   %esi
  80080d:	53                   	push   %ebx
  80080e:	8b 7d 08             	mov    0x8(%ebp),%edi
  800811:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  800814:	85 c9                	test   %ecx,%ecx
  800816:	74 36                	je     80084e <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  800818:	f7 c7 03 00 00 00    	test   $0x3,%edi
  80081e:	75 28                	jne    800848 <memset+0x40>
  800820:	f6 c1 03             	test   $0x3,%cl
  800823:	75 23                	jne    800848 <memset+0x40>
		c &= 0xFF;
  800825:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  800829:	89 d3                	mov    %edx,%ebx
  80082b:	c1 e3 08             	shl    $0x8,%ebx
  80082e:	89 d6                	mov    %edx,%esi
  800830:	c1 e6 18             	shl    $0x18,%esi
  800833:	89 d0                	mov    %edx,%eax
  800835:	c1 e0 10             	shl    $0x10,%eax
  800838:	09 f0                	or     %esi,%eax
  80083a:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  80083c:	89 d8                	mov    %ebx,%eax
  80083e:	09 d0                	or     %edx,%eax
  800840:	c1 e9 02             	shr    $0x2,%ecx
  800843:	fc                   	cld    
  800844:	f3 ab                	rep stos %eax,%es:(%edi)
  800846:	eb 06                	jmp    80084e <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800848:	8b 45 0c             	mov    0xc(%ebp),%eax
  80084b:	fc                   	cld    
  80084c:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  80084e:	89 f8                	mov    %edi,%eax
  800850:	5b                   	pop    %ebx
  800851:	5e                   	pop    %esi
  800852:	5f                   	pop    %edi
  800853:	5d                   	pop    %ebp
  800854:	c3                   	ret    

00800855 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800855:	55                   	push   %ebp
  800856:	89 e5                	mov    %esp,%ebp
  800858:	57                   	push   %edi
  800859:	56                   	push   %esi
  80085a:	8b 45 08             	mov    0x8(%ebp),%eax
  80085d:	8b 75 0c             	mov    0xc(%ebp),%esi
  800860:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800863:	39 c6                	cmp    %eax,%esi
  800865:	73 33                	jae    80089a <memmove+0x45>
  800867:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80086a:	39 d0                	cmp    %edx,%eax
  80086c:	73 2c                	jae    80089a <memmove+0x45>
		s += n;
		d += n;
  80086e:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800871:	89 d6                	mov    %edx,%esi
  800873:	09 fe                	or     %edi,%esi
  800875:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80087b:	75 13                	jne    800890 <memmove+0x3b>
  80087d:	f6 c1 03             	test   $0x3,%cl
  800880:	75 0e                	jne    800890 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800882:	83 ef 04             	sub    $0x4,%edi
  800885:	8d 72 fc             	lea    -0x4(%edx),%esi
  800888:	c1 e9 02             	shr    $0x2,%ecx
  80088b:	fd                   	std    
  80088c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80088e:	eb 07                	jmp    800897 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800890:	4f                   	dec    %edi
  800891:	8d 72 ff             	lea    -0x1(%edx),%esi
  800894:	fd                   	std    
  800895:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800897:	fc                   	cld    
  800898:	eb 1d                	jmp    8008b7 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80089a:	89 f2                	mov    %esi,%edx
  80089c:	09 c2                	or     %eax,%edx
  80089e:	f6 c2 03             	test   $0x3,%dl
  8008a1:	75 0f                	jne    8008b2 <memmove+0x5d>
  8008a3:	f6 c1 03             	test   $0x3,%cl
  8008a6:	75 0a                	jne    8008b2 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  8008a8:	c1 e9 02             	shr    $0x2,%ecx
  8008ab:	89 c7                	mov    %eax,%edi
  8008ad:	fc                   	cld    
  8008ae:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  8008b0:	eb 05                	jmp    8008b7 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  8008b2:	89 c7                	mov    %eax,%edi
  8008b4:	fc                   	cld    
  8008b5:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  8008b7:	5e                   	pop    %esi
  8008b8:	5f                   	pop    %edi
  8008b9:	5d                   	pop    %ebp
  8008ba:	c3                   	ret    

008008bb <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  8008bb:	55                   	push   %ebp
  8008bc:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  8008be:	ff 75 10             	pushl  0x10(%ebp)
  8008c1:	ff 75 0c             	pushl  0xc(%ebp)
  8008c4:	ff 75 08             	pushl  0x8(%ebp)
  8008c7:	e8 89 ff ff ff       	call   800855 <memmove>
}
  8008cc:	c9                   	leave  
  8008cd:	c3                   	ret    

008008ce <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008ce:	55                   	push   %ebp
  8008cf:	89 e5                	mov    %esp,%ebp
  8008d1:	56                   	push   %esi
  8008d2:	53                   	push   %ebx
  8008d3:	8b 45 08             	mov    0x8(%ebp),%eax
  8008d6:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008d9:	89 c6                	mov    %eax,%esi
  8008db:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008de:	eb 14                	jmp    8008f4 <memcmp+0x26>
		if (*s1 != *s2)
  8008e0:	8a 08                	mov    (%eax),%cl
  8008e2:	8a 1a                	mov    (%edx),%bl
  8008e4:	38 d9                	cmp    %bl,%cl
  8008e6:	74 0a                	je     8008f2 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008e8:	0f b6 c1             	movzbl %cl,%eax
  8008eb:	0f b6 db             	movzbl %bl,%ebx
  8008ee:	29 d8                	sub    %ebx,%eax
  8008f0:	eb 0b                	jmp    8008fd <memcmp+0x2f>
		s1++, s2++;
  8008f2:	40                   	inc    %eax
  8008f3:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008f4:	39 f0                	cmp    %esi,%eax
  8008f6:	75 e8                	jne    8008e0 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008f8:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008fd:	5b                   	pop    %ebx
  8008fe:	5e                   	pop    %esi
  8008ff:	5d                   	pop    %ebp
  800900:	c3                   	ret    

00800901 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800901:	55                   	push   %ebp
  800902:	89 e5                	mov    %esp,%ebp
  800904:	53                   	push   %ebx
  800905:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800908:	89 c1                	mov    %eax,%ecx
  80090a:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  80090d:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800911:	eb 08                	jmp    80091b <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800913:	0f b6 10             	movzbl (%eax),%edx
  800916:	39 da                	cmp    %ebx,%edx
  800918:	74 05                	je     80091f <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  80091a:	40                   	inc    %eax
  80091b:	39 c8                	cmp    %ecx,%eax
  80091d:	72 f4                	jb     800913 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  80091f:	5b                   	pop    %ebx
  800920:	5d                   	pop    %ebp
  800921:	c3                   	ret    

00800922 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800922:	55                   	push   %ebp
  800923:	89 e5                	mov    %esp,%ebp
  800925:	57                   	push   %edi
  800926:	56                   	push   %esi
  800927:	53                   	push   %ebx
  800928:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80092b:	eb 01                	jmp    80092e <strtol+0xc>
		s++;
  80092d:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  80092e:	8a 01                	mov    (%ecx),%al
  800930:	3c 20                	cmp    $0x20,%al
  800932:	74 f9                	je     80092d <strtol+0xb>
  800934:	3c 09                	cmp    $0x9,%al
  800936:	74 f5                	je     80092d <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800938:	3c 2b                	cmp    $0x2b,%al
  80093a:	75 08                	jne    800944 <strtol+0x22>
		s++;
  80093c:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  80093d:	bf 00 00 00 00       	mov    $0x0,%edi
  800942:	eb 11                	jmp    800955 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800944:	3c 2d                	cmp    $0x2d,%al
  800946:	75 08                	jne    800950 <strtol+0x2e>
		s++, neg = 1;
  800948:	41                   	inc    %ecx
  800949:	bf 01 00 00 00       	mov    $0x1,%edi
  80094e:	eb 05                	jmp    800955 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800950:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800955:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800959:	0f 84 87 00 00 00    	je     8009e6 <strtol+0xc4>
  80095f:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800963:	75 27                	jne    80098c <strtol+0x6a>
  800965:	80 39 30             	cmpb   $0x30,(%ecx)
  800968:	75 22                	jne    80098c <strtol+0x6a>
  80096a:	e9 88 00 00 00       	jmp    8009f7 <strtol+0xd5>
		s += 2, base = 16;
  80096f:	83 c1 02             	add    $0x2,%ecx
  800972:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800979:	eb 11                	jmp    80098c <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  80097b:	41                   	inc    %ecx
  80097c:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800983:	eb 07                	jmp    80098c <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800985:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  80098c:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800991:	8a 11                	mov    (%ecx),%dl
  800993:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800996:	80 fb 09             	cmp    $0x9,%bl
  800999:	77 08                	ja     8009a3 <strtol+0x81>
			dig = *s - '0';
  80099b:	0f be d2             	movsbl %dl,%edx
  80099e:	83 ea 30             	sub    $0x30,%edx
  8009a1:	eb 22                	jmp    8009c5 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  8009a3:	8d 72 9f             	lea    -0x61(%edx),%esi
  8009a6:	89 f3                	mov    %esi,%ebx
  8009a8:	80 fb 19             	cmp    $0x19,%bl
  8009ab:	77 08                	ja     8009b5 <strtol+0x93>
			dig = *s - 'a' + 10;
  8009ad:	0f be d2             	movsbl %dl,%edx
  8009b0:	83 ea 57             	sub    $0x57,%edx
  8009b3:	eb 10                	jmp    8009c5 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  8009b5:	8d 72 bf             	lea    -0x41(%edx),%esi
  8009b8:	89 f3                	mov    %esi,%ebx
  8009ba:	80 fb 19             	cmp    $0x19,%bl
  8009bd:	77 14                	ja     8009d3 <strtol+0xb1>
			dig = *s - 'A' + 10;
  8009bf:	0f be d2             	movsbl %dl,%edx
  8009c2:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  8009c5:	3b 55 10             	cmp    0x10(%ebp),%edx
  8009c8:	7d 09                	jge    8009d3 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  8009ca:	41                   	inc    %ecx
  8009cb:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009cf:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009d1:	eb be                	jmp    800991 <strtol+0x6f>

	if (endptr)
  8009d3:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009d7:	74 05                	je     8009de <strtol+0xbc>
		*endptr = (char *) s;
  8009d9:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009dc:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009de:	85 ff                	test   %edi,%edi
  8009e0:	74 21                	je     800a03 <strtol+0xe1>
  8009e2:	f7 d8                	neg    %eax
  8009e4:	eb 1d                	jmp    800a03 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009e6:	80 39 30             	cmpb   $0x30,(%ecx)
  8009e9:	75 9a                	jne    800985 <strtol+0x63>
  8009eb:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009ef:	0f 84 7a ff ff ff    	je     80096f <strtol+0x4d>
  8009f5:	eb 84                	jmp    80097b <strtol+0x59>
  8009f7:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009fb:	0f 84 6e ff ff ff    	je     80096f <strtol+0x4d>
  800a01:	eb 89                	jmp    80098c <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a03:	5b                   	pop    %ebx
  800a04:	5e                   	pop    %esi
  800a05:	5f                   	pop    %edi
  800a06:	5d                   	pop    %ebp
  800a07:	c3                   	ret    

00800a08 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800a08:	55                   	push   %ebp
  800a09:	89 e5                	mov    %esp,%ebp
  800a0b:	57                   	push   %edi
  800a0c:	56                   	push   %esi
  800a0d:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a0e:	b8 00 00 00 00       	mov    $0x0,%eax
  800a13:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800a16:	8b 55 08             	mov    0x8(%ebp),%edx
  800a19:	89 c3                	mov    %eax,%ebx
  800a1b:	89 c7                	mov    %eax,%edi
  800a1d:	89 c6                	mov    %eax,%esi
  800a1f:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800a21:	5b                   	pop    %ebx
  800a22:	5e                   	pop    %esi
  800a23:	5f                   	pop    %edi
  800a24:	5d                   	pop    %ebp
  800a25:	c3                   	ret    

00800a26 <sys_cgetc>:

int
sys_cgetc(void)
{
  800a26:	55                   	push   %ebp
  800a27:	89 e5                	mov    %esp,%ebp
  800a29:	57                   	push   %edi
  800a2a:	56                   	push   %esi
  800a2b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a2c:	ba 00 00 00 00       	mov    $0x0,%edx
  800a31:	b8 01 00 00 00       	mov    $0x1,%eax
  800a36:	89 d1                	mov    %edx,%ecx
  800a38:	89 d3                	mov    %edx,%ebx
  800a3a:	89 d7                	mov    %edx,%edi
  800a3c:	89 d6                	mov    %edx,%esi
  800a3e:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a40:	5b                   	pop    %ebx
  800a41:	5e                   	pop    %esi
  800a42:	5f                   	pop    %edi
  800a43:	5d                   	pop    %ebp
  800a44:	c3                   	ret    

00800a45 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a45:	55                   	push   %ebp
  800a46:	89 e5                	mov    %esp,%ebp
  800a48:	57                   	push   %edi
  800a49:	56                   	push   %esi
  800a4a:	53                   	push   %ebx
  800a4b:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a4e:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a53:	b8 03 00 00 00       	mov    $0x3,%eax
  800a58:	8b 55 08             	mov    0x8(%ebp),%edx
  800a5b:	89 cb                	mov    %ecx,%ebx
  800a5d:	89 cf                	mov    %ecx,%edi
  800a5f:	89 ce                	mov    %ecx,%esi
  800a61:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a63:	85 c0                	test   %eax,%eax
  800a65:	7e 17                	jle    800a7e <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a67:	83 ec 0c             	sub    $0xc,%esp
  800a6a:	50                   	push   %eax
  800a6b:	6a 03                	push   $0x3
  800a6d:	68 c0 0f 80 00       	push   $0x800fc0
  800a72:	6a 23                	push   $0x23
  800a74:	68 dd 0f 80 00       	push   $0x800fdd
  800a79:	e8 27 00 00 00       	call   800aa5 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a7e:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a81:	5b                   	pop    %ebx
  800a82:	5e                   	pop    %esi
  800a83:	5f                   	pop    %edi
  800a84:	5d                   	pop    %ebp
  800a85:	c3                   	ret    

00800a86 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a86:	55                   	push   %ebp
  800a87:	89 e5                	mov    %esp,%ebp
  800a89:	57                   	push   %edi
  800a8a:	56                   	push   %esi
  800a8b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a8c:	ba 00 00 00 00       	mov    $0x0,%edx
  800a91:	b8 02 00 00 00       	mov    $0x2,%eax
  800a96:	89 d1                	mov    %edx,%ecx
  800a98:	89 d3                	mov    %edx,%ebx
  800a9a:	89 d7                	mov    %edx,%edi
  800a9c:	89 d6                	mov    %edx,%esi
  800a9e:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800aa0:	5b                   	pop    %ebx
  800aa1:	5e                   	pop    %esi
  800aa2:	5f                   	pop    %edi
  800aa3:	5d                   	pop    %ebp
  800aa4:	c3                   	ret    

00800aa5 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800aa5:	55                   	push   %ebp
  800aa6:	89 e5                	mov    %esp,%ebp
  800aa8:	56                   	push   %esi
  800aa9:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800aaa:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800aad:	8b 35 04 20 80 00    	mov    0x802004,%esi
  800ab3:	e8 ce ff ff ff       	call   800a86 <sys_getenvid>
  800ab8:	83 ec 0c             	sub    $0xc,%esp
  800abb:	ff 75 0c             	pushl  0xc(%ebp)
  800abe:	ff 75 08             	pushl  0x8(%ebp)
  800ac1:	56                   	push   %esi
  800ac2:	50                   	push   %eax
  800ac3:	68 ec 0f 80 00       	push   $0x800fec
  800ac8:	e8 af f6 ff ff       	call   80017c <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800acd:	83 c4 18             	add    $0x18,%esp
  800ad0:	53                   	push   %ebx
  800ad1:	ff 75 10             	pushl  0x10(%ebp)
  800ad4:	e8 52 f6 ff ff       	call   80012b <vcprintf>
	cprintf("\n");
  800ad9:	c7 04 24 75 0d 80 00 	movl   $0x800d75,(%esp)
  800ae0:	e8 97 f6 ff ff       	call   80017c <cprintf>
  800ae5:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800ae8:	cc                   	int3   
  800ae9:	eb fd                	jmp    800ae8 <_panic+0x43>
  800aeb:	90                   	nop

00800aec <__udivdi3>:
  800aec:	55                   	push   %ebp
  800aed:	57                   	push   %edi
  800aee:	56                   	push   %esi
  800aef:	53                   	push   %ebx
  800af0:	83 ec 1c             	sub    $0x1c,%esp
  800af3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800af7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800afb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800aff:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800b03:	89 ca                	mov    %ecx,%edx
  800b05:	89 f8                	mov    %edi,%eax
  800b07:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800b0b:	85 f6                	test   %esi,%esi
  800b0d:	75 2d                	jne    800b3c <__udivdi3+0x50>
  800b0f:	39 cf                	cmp    %ecx,%edi
  800b11:	77 65                	ja     800b78 <__udivdi3+0x8c>
  800b13:	89 fd                	mov    %edi,%ebp
  800b15:	85 ff                	test   %edi,%edi
  800b17:	75 0b                	jne    800b24 <__udivdi3+0x38>
  800b19:	b8 01 00 00 00       	mov    $0x1,%eax
  800b1e:	31 d2                	xor    %edx,%edx
  800b20:	f7 f7                	div    %edi
  800b22:	89 c5                	mov    %eax,%ebp
  800b24:	31 d2                	xor    %edx,%edx
  800b26:	89 c8                	mov    %ecx,%eax
  800b28:	f7 f5                	div    %ebp
  800b2a:	89 c1                	mov    %eax,%ecx
  800b2c:	89 d8                	mov    %ebx,%eax
  800b2e:	f7 f5                	div    %ebp
  800b30:	89 cf                	mov    %ecx,%edi
  800b32:	89 fa                	mov    %edi,%edx
  800b34:	83 c4 1c             	add    $0x1c,%esp
  800b37:	5b                   	pop    %ebx
  800b38:	5e                   	pop    %esi
  800b39:	5f                   	pop    %edi
  800b3a:	5d                   	pop    %ebp
  800b3b:	c3                   	ret    
  800b3c:	39 ce                	cmp    %ecx,%esi
  800b3e:	77 28                	ja     800b68 <__udivdi3+0x7c>
  800b40:	0f bd fe             	bsr    %esi,%edi
  800b43:	83 f7 1f             	xor    $0x1f,%edi
  800b46:	75 40                	jne    800b88 <__udivdi3+0x9c>
  800b48:	39 ce                	cmp    %ecx,%esi
  800b4a:	72 0a                	jb     800b56 <__udivdi3+0x6a>
  800b4c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b50:	0f 87 9e 00 00 00    	ja     800bf4 <__udivdi3+0x108>
  800b56:	b8 01 00 00 00       	mov    $0x1,%eax
  800b5b:	89 fa                	mov    %edi,%edx
  800b5d:	83 c4 1c             	add    $0x1c,%esp
  800b60:	5b                   	pop    %ebx
  800b61:	5e                   	pop    %esi
  800b62:	5f                   	pop    %edi
  800b63:	5d                   	pop    %ebp
  800b64:	c3                   	ret    
  800b65:	8d 76 00             	lea    0x0(%esi),%esi
  800b68:	31 ff                	xor    %edi,%edi
  800b6a:	31 c0                	xor    %eax,%eax
  800b6c:	89 fa                	mov    %edi,%edx
  800b6e:	83 c4 1c             	add    $0x1c,%esp
  800b71:	5b                   	pop    %ebx
  800b72:	5e                   	pop    %esi
  800b73:	5f                   	pop    %edi
  800b74:	5d                   	pop    %ebp
  800b75:	c3                   	ret    
  800b76:	66 90                	xchg   %ax,%ax
  800b78:	89 d8                	mov    %ebx,%eax
  800b7a:	f7 f7                	div    %edi
  800b7c:	31 ff                	xor    %edi,%edi
  800b7e:	89 fa                	mov    %edi,%edx
  800b80:	83 c4 1c             	add    $0x1c,%esp
  800b83:	5b                   	pop    %ebx
  800b84:	5e                   	pop    %esi
  800b85:	5f                   	pop    %edi
  800b86:	5d                   	pop    %ebp
  800b87:	c3                   	ret    
  800b88:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b8d:	89 eb                	mov    %ebp,%ebx
  800b8f:	29 fb                	sub    %edi,%ebx
  800b91:	89 f9                	mov    %edi,%ecx
  800b93:	d3 e6                	shl    %cl,%esi
  800b95:	89 c5                	mov    %eax,%ebp
  800b97:	88 d9                	mov    %bl,%cl
  800b99:	d3 ed                	shr    %cl,%ebp
  800b9b:	89 e9                	mov    %ebp,%ecx
  800b9d:	09 f1                	or     %esi,%ecx
  800b9f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800ba3:	89 f9                	mov    %edi,%ecx
  800ba5:	d3 e0                	shl    %cl,%eax
  800ba7:	89 c5                	mov    %eax,%ebp
  800ba9:	89 d6                	mov    %edx,%esi
  800bab:	88 d9                	mov    %bl,%cl
  800bad:	d3 ee                	shr    %cl,%esi
  800baf:	89 f9                	mov    %edi,%ecx
  800bb1:	d3 e2                	shl    %cl,%edx
  800bb3:	8b 44 24 08          	mov    0x8(%esp),%eax
  800bb7:	88 d9                	mov    %bl,%cl
  800bb9:	d3 e8                	shr    %cl,%eax
  800bbb:	09 c2                	or     %eax,%edx
  800bbd:	89 d0                	mov    %edx,%eax
  800bbf:	89 f2                	mov    %esi,%edx
  800bc1:	f7 74 24 0c          	divl   0xc(%esp)
  800bc5:	89 d6                	mov    %edx,%esi
  800bc7:	89 c3                	mov    %eax,%ebx
  800bc9:	f7 e5                	mul    %ebp
  800bcb:	39 d6                	cmp    %edx,%esi
  800bcd:	72 19                	jb     800be8 <__udivdi3+0xfc>
  800bcf:	74 0b                	je     800bdc <__udivdi3+0xf0>
  800bd1:	89 d8                	mov    %ebx,%eax
  800bd3:	31 ff                	xor    %edi,%edi
  800bd5:	e9 58 ff ff ff       	jmp    800b32 <__udivdi3+0x46>
  800bda:	66 90                	xchg   %ax,%ax
  800bdc:	8b 54 24 08          	mov    0x8(%esp),%edx
  800be0:	89 f9                	mov    %edi,%ecx
  800be2:	d3 e2                	shl    %cl,%edx
  800be4:	39 c2                	cmp    %eax,%edx
  800be6:	73 e9                	jae    800bd1 <__udivdi3+0xe5>
  800be8:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800beb:	31 ff                	xor    %edi,%edi
  800bed:	e9 40 ff ff ff       	jmp    800b32 <__udivdi3+0x46>
  800bf2:	66 90                	xchg   %ax,%ax
  800bf4:	31 c0                	xor    %eax,%eax
  800bf6:	e9 37 ff ff ff       	jmp    800b32 <__udivdi3+0x46>
  800bfb:	90                   	nop

00800bfc <__umoddi3>:
  800bfc:	55                   	push   %ebp
  800bfd:	57                   	push   %edi
  800bfe:	56                   	push   %esi
  800bff:	53                   	push   %ebx
  800c00:	83 ec 1c             	sub    $0x1c,%esp
  800c03:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800c07:	8b 74 24 34          	mov    0x34(%esp),%esi
  800c0b:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800c0f:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800c13:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800c17:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800c1b:	89 f3                	mov    %esi,%ebx
  800c1d:	89 fa                	mov    %edi,%edx
  800c1f:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800c23:	89 34 24             	mov    %esi,(%esp)
  800c26:	85 c0                	test   %eax,%eax
  800c28:	75 1a                	jne    800c44 <__umoddi3+0x48>
  800c2a:	39 f7                	cmp    %esi,%edi
  800c2c:	0f 86 a2 00 00 00    	jbe    800cd4 <__umoddi3+0xd8>
  800c32:	89 c8                	mov    %ecx,%eax
  800c34:	89 f2                	mov    %esi,%edx
  800c36:	f7 f7                	div    %edi
  800c38:	89 d0                	mov    %edx,%eax
  800c3a:	31 d2                	xor    %edx,%edx
  800c3c:	83 c4 1c             	add    $0x1c,%esp
  800c3f:	5b                   	pop    %ebx
  800c40:	5e                   	pop    %esi
  800c41:	5f                   	pop    %edi
  800c42:	5d                   	pop    %ebp
  800c43:	c3                   	ret    
  800c44:	39 f0                	cmp    %esi,%eax
  800c46:	0f 87 ac 00 00 00    	ja     800cf8 <__umoddi3+0xfc>
  800c4c:	0f bd e8             	bsr    %eax,%ebp
  800c4f:	83 f5 1f             	xor    $0x1f,%ebp
  800c52:	0f 84 ac 00 00 00    	je     800d04 <__umoddi3+0x108>
  800c58:	bf 20 00 00 00       	mov    $0x20,%edi
  800c5d:	29 ef                	sub    %ebp,%edi
  800c5f:	89 fe                	mov    %edi,%esi
  800c61:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c65:	89 e9                	mov    %ebp,%ecx
  800c67:	d3 e0                	shl    %cl,%eax
  800c69:	89 d7                	mov    %edx,%edi
  800c6b:	89 f1                	mov    %esi,%ecx
  800c6d:	d3 ef                	shr    %cl,%edi
  800c6f:	09 c7                	or     %eax,%edi
  800c71:	89 e9                	mov    %ebp,%ecx
  800c73:	d3 e2                	shl    %cl,%edx
  800c75:	89 14 24             	mov    %edx,(%esp)
  800c78:	89 d8                	mov    %ebx,%eax
  800c7a:	d3 e0                	shl    %cl,%eax
  800c7c:	89 c2                	mov    %eax,%edx
  800c7e:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c82:	d3 e0                	shl    %cl,%eax
  800c84:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c88:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c8c:	89 f1                	mov    %esi,%ecx
  800c8e:	d3 e8                	shr    %cl,%eax
  800c90:	09 d0                	or     %edx,%eax
  800c92:	d3 eb                	shr    %cl,%ebx
  800c94:	89 da                	mov    %ebx,%edx
  800c96:	f7 f7                	div    %edi
  800c98:	89 d3                	mov    %edx,%ebx
  800c9a:	f7 24 24             	mull   (%esp)
  800c9d:	89 c6                	mov    %eax,%esi
  800c9f:	89 d1                	mov    %edx,%ecx
  800ca1:	39 d3                	cmp    %edx,%ebx
  800ca3:	0f 82 87 00 00 00    	jb     800d30 <__umoddi3+0x134>
  800ca9:	0f 84 91 00 00 00    	je     800d40 <__umoddi3+0x144>
  800caf:	8b 54 24 04          	mov    0x4(%esp),%edx
  800cb3:	29 f2                	sub    %esi,%edx
  800cb5:	19 cb                	sbb    %ecx,%ebx
  800cb7:	89 d8                	mov    %ebx,%eax
  800cb9:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800cbd:	d3 e0                	shl    %cl,%eax
  800cbf:	89 e9                	mov    %ebp,%ecx
  800cc1:	d3 ea                	shr    %cl,%edx
  800cc3:	09 d0                	or     %edx,%eax
  800cc5:	89 e9                	mov    %ebp,%ecx
  800cc7:	d3 eb                	shr    %cl,%ebx
  800cc9:	89 da                	mov    %ebx,%edx
  800ccb:	83 c4 1c             	add    $0x1c,%esp
  800cce:	5b                   	pop    %ebx
  800ccf:	5e                   	pop    %esi
  800cd0:	5f                   	pop    %edi
  800cd1:	5d                   	pop    %ebp
  800cd2:	c3                   	ret    
  800cd3:	90                   	nop
  800cd4:	89 fd                	mov    %edi,%ebp
  800cd6:	85 ff                	test   %edi,%edi
  800cd8:	75 0b                	jne    800ce5 <__umoddi3+0xe9>
  800cda:	b8 01 00 00 00       	mov    $0x1,%eax
  800cdf:	31 d2                	xor    %edx,%edx
  800ce1:	f7 f7                	div    %edi
  800ce3:	89 c5                	mov    %eax,%ebp
  800ce5:	89 f0                	mov    %esi,%eax
  800ce7:	31 d2                	xor    %edx,%edx
  800ce9:	f7 f5                	div    %ebp
  800ceb:	89 c8                	mov    %ecx,%eax
  800ced:	f7 f5                	div    %ebp
  800cef:	89 d0                	mov    %edx,%eax
  800cf1:	e9 44 ff ff ff       	jmp    800c3a <__umoddi3+0x3e>
  800cf6:	66 90                	xchg   %ax,%ax
  800cf8:	89 c8                	mov    %ecx,%eax
  800cfa:	89 f2                	mov    %esi,%edx
  800cfc:	83 c4 1c             	add    $0x1c,%esp
  800cff:	5b                   	pop    %ebx
  800d00:	5e                   	pop    %esi
  800d01:	5f                   	pop    %edi
  800d02:	5d                   	pop    %ebp
  800d03:	c3                   	ret    
  800d04:	3b 04 24             	cmp    (%esp),%eax
  800d07:	72 06                	jb     800d0f <__umoddi3+0x113>
  800d09:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800d0d:	77 0f                	ja     800d1e <__umoddi3+0x122>
  800d0f:	89 f2                	mov    %esi,%edx
  800d11:	29 f9                	sub    %edi,%ecx
  800d13:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800d17:	89 14 24             	mov    %edx,(%esp)
  800d1a:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800d1e:	8b 44 24 04          	mov    0x4(%esp),%eax
  800d22:	8b 14 24             	mov    (%esp),%edx
  800d25:	83 c4 1c             	add    $0x1c,%esp
  800d28:	5b                   	pop    %ebx
  800d29:	5e                   	pop    %esi
  800d2a:	5f                   	pop    %edi
  800d2b:	5d                   	pop    %ebp
  800d2c:	c3                   	ret    
  800d2d:	8d 76 00             	lea    0x0(%esi),%esi
  800d30:	2b 04 24             	sub    (%esp),%eax
  800d33:	19 fa                	sbb    %edi,%edx
  800d35:	89 d1                	mov    %edx,%ecx
  800d37:	89 c6                	mov    %eax,%esi
  800d39:	e9 71 ff ff ff       	jmp    800caf <__umoddi3+0xb3>
  800d3e:	66 90                	xchg   %ax,%ax
  800d40:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d44:	72 ea                	jb     800d30 <__umoddi3+0x134>
  800d46:	89 d9                	mov    %ebx,%ecx
  800d48:	e9 62 ff ff ff       	jmp    800caf <__umoddi3+0xb3>
