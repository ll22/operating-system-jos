
obj/user/testbss:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 a5 00 00 00       	call   8000d6 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

uint32_t bigarray[ARRAYSIZE];

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 14             	sub    $0x14,%esp
	int i;

	cprintf("Making sure bss works right...\n");
  800039:	68 d8 0d 80 00       	push   $0x800dd8
  80003e:	e8 07 02 00 00       	call   80024a <cprintf>
  800043:	83 c4 10             	add    $0x10,%esp
	for (i = 0; i < ARRAYSIZE; i++)
  800046:	b8 00 00 00 00       	mov    $0x0,%eax
		if (bigarray[i] != 0)
  80004b:	83 3c 85 20 20 80 00 	cmpl   $0x0,0x802020(,%eax,4)
  800052:	00 
  800053:	74 12                	je     800067 <umain+0x34>
			panic("bigarray[%d] isn't cleared!\n", i);
  800055:	50                   	push   %eax
  800056:	68 53 0e 80 00       	push   $0x800e53
  80005b:	6a 11                	push   $0x11
  80005d:	68 70 0e 80 00       	push   $0x800e70
  800062:	e8 0b 01 00 00       	call   800172 <_panic>
umain(int argc, char **argv)
{
	int i;

	cprintf("Making sure bss works right...\n");
	for (i = 0; i < ARRAYSIZE; i++)
  800067:	40                   	inc    %eax
  800068:	3d 00 00 10 00       	cmp    $0x100000,%eax
  80006d:	75 dc                	jne    80004b <umain+0x18>
  80006f:	b8 00 00 00 00       	mov    $0x0,%eax
		if (bigarray[i] != 0)
			panic("bigarray[%d] isn't cleared!\n", i);
	for (i = 0; i < ARRAYSIZE; i++)
		bigarray[i] = i;
  800074:	89 04 85 20 20 80 00 	mov    %eax,0x802020(,%eax,4)

	cprintf("Making sure bss works right...\n");
	for (i = 0; i < ARRAYSIZE; i++)
		if (bigarray[i] != 0)
			panic("bigarray[%d] isn't cleared!\n", i);
	for (i = 0; i < ARRAYSIZE; i++)
  80007b:	40                   	inc    %eax
  80007c:	3d 00 00 10 00       	cmp    $0x100000,%eax
  800081:	75 f1                	jne    800074 <umain+0x41>
  800083:	b8 00 00 00 00       	mov    $0x0,%eax
		bigarray[i] = i;
	for (i = 0; i < ARRAYSIZE; i++)
		if (bigarray[i] != i)
  800088:	3b 04 85 20 20 80 00 	cmp    0x802020(,%eax,4),%eax
  80008f:	74 12                	je     8000a3 <umain+0x70>
			panic("bigarray[%d] didn't hold its value!\n", i);
  800091:	50                   	push   %eax
  800092:	68 f8 0d 80 00       	push   $0x800df8
  800097:	6a 16                	push   $0x16
  800099:	68 70 0e 80 00       	push   $0x800e70
  80009e:	e8 cf 00 00 00       	call   800172 <_panic>
	for (i = 0; i < ARRAYSIZE; i++)
		if (bigarray[i] != 0)
			panic("bigarray[%d] isn't cleared!\n", i);
	for (i = 0; i < ARRAYSIZE; i++)
		bigarray[i] = i;
	for (i = 0; i < ARRAYSIZE; i++)
  8000a3:	40                   	inc    %eax
  8000a4:	3d 00 00 10 00       	cmp    $0x100000,%eax
  8000a9:	75 dd                	jne    800088 <umain+0x55>
		if (bigarray[i] != i)
			panic("bigarray[%d] didn't hold its value!\n", i);

	cprintf("Yes, good.  Now doing a wild write off the end...\n");
  8000ab:	83 ec 0c             	sub    $0xc,%esp
  8000ae:	68 20 0e 80 00       	push   $0x800e20
  8000b3:	e8 92 01 00 00       	call   80024a <cprintf>
	bigarray[ARRAYSIZE+1024] = 0;
  8000b8:	c7 05 20 30 c0 00 00 	movl   $0x0,0xc03020
  8000bf:	00 00 00 
	panic("SHOULD HAVE TRAPPED!!!");
  8000c2:	83 c4 0c             	add    $0xc,%esp
  8000c5:	68 7f 0e 80 00       	push   $0x800e7f
  8000ca:	6a 1a                	push   $0x1a
  8000cc:	68 70 0e 80 00       	push   $0x800e70
  8000d1:	e8 9c 00 00 00       	call   800172 <_panic>

008000d6 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  8000d6:	55                   	push   %ebp
  8000d7:	89 e5                	mov    %esp,%ebp
  8000d9:	57                   	push   %edi
  8000da:	56                   	push   %esi
  8000db:	53                   	push   %ebx
  8000dc:	83 ec 0c             	sub    $0xc,%esp
  8000df:	8b 5d 08             	mov    0x8(%ebp),%ebx
  8000e2:	8b 75 0c             	mov    0xc(%ebp),%esi
	// set thisenv to point at our Env structure in envs[].
	// LAB 3: Your code here.
	//thisenv = 0;
	int32_t env_Index1 = (int32_t)sys_getenvid();
  8000e5:	e8 6a 0a 00 00       	call   800b54 <sys_getenvid>
  8000ea:	89 c7                	mov    %eax,%edi
	cprintf("printing env_Index1: %d\n", env_Index1);
  8000ec:	83 ec 08             	sub    $0x8,%esp
  8000ef:	50                   	push   %eax
  8000f0:	68 96 0e 80 00       	push   $0x800e96
  8000f5:	e8 50 01 00 00       	call   80024a <cprintf>
	int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	cprintf("printing env_Index2: %d\n", env_Index2);
  8000fa:	83 c4 08             	add    $0x8,%esp
  8000fd:	81 e7 ff 03 00 00    	and    $0x3ff,%edi
  800103:	57                   	push   %edi
  800104:	68 af 0e 80 00       	push   $0x800eaf
  800109:	e8 3c 01 00 00       	call   80024a <cprintf>

	thisenv = &envs[ENVX(sys_getenvid())];
  80010e:	e8 41 0a 00 00       	call   800b54 <sys_getenvid>
  800113:	25 ff 03 00 00       	and    $0x3ff,%eax
  800118:	8d 14 00             	lea    (%eax,%eax,1),%edx
  80011b:	01 d0                	add    %edx,%eax
  80011d:	c1 e0 05             	shl    $0x5,%eax
  800120:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800125:	a3 20 20 c0 00       	mov    %eax,0xc02020
	cprintf("Addrs of thisenv (envs[0]) is: %p\n", thisenv);
  80012a:	83 c4 08             	add    $0x8,%esp
  80012d:	50                   	push   %eax
  80012e:	68 d4 0e 80 00       	push   $0x800ed4
  800133:	e8 12 01 00 00       	call   80024a <cprintf>
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800138:	83 c4 10             	add    $0x10,%esp
  80013b:	85 db                	test   %ebx,%ebx
  80013d:	7e 07                	jle    800146 <libmain+0x70>
		binaryname = argv[0];
  80013f:	8b 06                	mov    (%esi),%eax
  800141:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	umain(argc, argv);
  800146:	83 ec 08             	sub    $0x8,%esp
  800149:	56                   	push   %esi
  80014a:	53                   	push   %ebx
  80014b:	e8 e3 fe ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800150:	e8 0b 00 00 00       	call   800160 <exit>
}
  800155:	83 c4 10             	add    $0x10,%esp
  800158:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80015b:	5b                   	pop    %ebx
  80015c:	5e                   	pop    %esi
  80015d:	5f                   	pop    %edi
  80015e:	5d                   	pop    %ebp
  80015f:	c3                   	ret    

00800160 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800160:	55                   	push   %ebp
  800161:	89 e5                	mov    %esp,%ebp
  800163:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  800166:	6a 00                	push   $0x0
  800168:	e8 a6 09 00 00       	call   800b13 <sys_env_destroy>
}
  80016d:	83 c4 10             	add    $0x10,%esp
  800170:	c9                   	leave  
  800171:	c3                   	ret    

00800172 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800172:	55                   	push   %ebp
  800173:	89 e5                	mov    %esp,%ebp
  800175:	56                   	push   %esi
  800176:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800177:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80017a:	8b 35 00 20 80 00    	mov    0x802000,%esi
  800180:	e8 cf 09 00 00       	call   800b54 <sys_getenvid>
  800185:	83 ec 0c             	sub    $0xc,%esp
  800188:	ff 75 0c             	pushl  0xc(%ebp)
  80018b:	ff 75 08             	pushl  0x8(%ebp)
  80018e:	56                   	push   %esi
  80018f:	50                   	push   %eax
  800190:	68 f8 0e 80 00       	push   $0x800ef8
  800195:	e8 b0 00 00 00       	call   80024a <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80019a:	83 c4 18             	add    $0x18,%esp
  80019d:	53                   	push   %ebx
  80019e:	ff 75 10             	pushl  0x10(%ebp)
  8001a1:	e8 53 00 00 00       	call   8001f9 <vcprintf>
	cprintf("\n");
  8001a6:	c7 04 24 6e 0e 80 00 	movl   $0x800e6e,(%esp)
  8001ad:	e8 98 00 00 00       	call   80024a <cprintf>
  8001b2:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  8001b5:	cc                   	int3   
  8001b6:	eb fd                	jmp    8001b5 <_panic+0x43>

008001b8 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8001b8:	55                   	push   %ebp
  8001b9:	89 e5                	mov    %esp,%ebp
  8001bb:	53                   	push   %ebx
  8001bc:	83 ec 04             	sub    $0x4,%esp
  8001bf:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8001c2:	8b 13                	mov    (%ebx),%edx
  8001c4:	8d 42 01             	lea    0x1(%edx),%eax
  8001c7:	89 03                	mov    %eax,(%ebx)
  8001c9:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8001cc:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8001d0:	3d ff 00 00 00       	cmp    $0xff,%eax
  8001d5:	75 1a                	jne    8001f1 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8001d7:	83 ec 08             	sub    $0x8,%esp
  8001da:	68 ff 00 00 00       	push   $0xff
  8001df:	8d 43 08             	lea    0x8(%ebx),%eax
  8001e2:	50                   	push   %eax
  8001e3:	e8 ee 08 00 00       	call   800ad6 <sys_cputs>
		b->idx = 0;
  8001e8:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8001ee:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8001f1:	ff 43 04             	incl   0x4(%ebx)
}
  8001f4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001f7:	c9                   	leave  
  8001f8:	c3                   	ret    

008001f9 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8001f9:	55                   	push   %ebp
  8001fa:	89 e5                	mov    %esp,%ebp
  8001fc:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800202:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800209:	00 00 00 
	b.cnt = 0;
  80020c:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800213:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800216:	ff 75 0c             	pushl  0xc(%ebp)
  800219:	ff 75 08             	pushl  0x8(%ebp)
  80021c:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800222:	50                   	push   %eax
  800223:	68 b8 01 80 00       	push   $0x8001b8
  800228:	e8 51 01 00 00       	call   80037e <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80022d:	83 c4 08             	add    $0x8,%esp
  800230:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800236:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80023c:	50                   	push   %eax
  80023d:	e8 94 08 00 00       	call   800ad6 <sys_cputs>

	return b.cnt;
}
  800242:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800248:	c9                   	leave  
  800249:	c3                   	ret    

0080024a <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80024a:	55                   	push   %ebp
  80024b:	89 e5                	mov    %esp,%ebp
  80024d:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800250:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800253:	50                   	push   %eax
  800254:	ff 75 08             	pushl  0x8(%ebp)
  800257:	e8 9d ff ff ff       	call   8001f9 <vcprintf>
	va_end(ap);

	return cnt;
}
  80025c:	c9                   	leave  
  80025d:	c3                   	ret    

0080025e <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80025e:	55                   	push   %ebp
  80025f:	89 e5                	mov    %esp,%ebp
  800261:	57                   	push   %edi
  800262:	56                   	push   %esi
  800263:	53                   	push   %ebx
  800264:	83 ec 1c             	sub    $0x1c,%esp
  800267:	89 c7                	mov    %eax,%edi
  800269:	89 d6                	mov    %edx,%esi
  80026b:	8b 45 08             	mov    0x8(%ebp),%eax
  80026e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800271:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800274:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800277:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80027a:	bb 00 00 00 00       	mov    $0x0,%ebx
  80027f:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800282:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800285:	39 d3                	cmp    %edx,%ebx
  800287:	72 05                	jb     80028e <printnum+0x30>
  800289:	39 45 10             	cmp    %eax,0x10(%ebp)
  80028c:	77 45                	ja     8002d3 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  80028e:	83 ec 0c             	sub    $0xc,%esp
  800291:	ff 75 18             	pushl  0x18(%ebp)
  800294:	8b 45 14             	mov    0x14(%ebp),%eax
  800297:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80029a:	53                   	push   %ebx
  80029b:	ff 75 10             	pushl  0x10(%ebp)
  80029e:	83 ec 08             	sub    $0x8,%esp
  8002a1:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002a4:	ff 75 e0             	pushl  -0x20(%ebp)
  8002a7:	ff 75 dc             	pushl  -0x24(%ebp)
  8002aa:	ff 75 d8             	pushl  -0x28(%ebp)
  8002ad:	e8 c2 08 00 00       	call   800b74 <__udivdi3>
  8002b2:	83 c4 18             	add    $0x18,%esp
  8002b5:	52                   	push   %edx
  8002b6:	50                   	push   %eax
  8002b7:	89 f2                	mov    %esi,%edx
  8002b9:	89 f8                	mov    %edi,%eax
  8002bb:	e8 9e ff ff ff       	call   80025e <printnum>
  8002c0:	83 c4 20             	add    $0x20,%esp
  8002c3:	eb 16                	jmp    8002db <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8002c5:	83 ec 08             	sub    $0x8,%esp
  8002c8:	56                   	push   %esi
  8002c9:	ff 75 18             	pushl  0x18(%ebp)
  8002cc:	ff d7                	call   *%edi
  8002ce:	83 c4 10             	add    $0x10,%esp
  8002d1:	eb 03                	jmp    8002d6 <printnum+0x78>
  8002d3:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8002d6:	4b                   	dec    %ebx
  8002d7:	85 db                	test   %ebx,%ebx
  8002d9:	7f ea                	jg     8002c5 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8002db:	83 ec 08             	sub    $0x8,%esp
  8002de:	56                   	push   %esi
  8002df:	83 ec 04             	sub    $0x4,%esp
  8002e2:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002e5:	ff 75 e0             	pushl  -0x20(%ebp)
  8002e8:	ff 75 dc             	pushl  -0x24(%ebp)
  8002eb:	ff 75 d8             	pushl  -0x28(%ebp)
  8002ee:	e8 91 09 00 00       	call   800c84 <__umoddi3>
  8002f3:	83 c4 14             	add    $0x14,%esp
  8002f6:	0f be 80 1c 0f 80 00 	movsbl 0x800f1c(%eax),%eax
  8002fd:	50                   	push   %eax
  8002fe:	ff d7                	call   *%edi
}
  800300:	83 c4 10             	add    $0x10,%esp
  800303:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800306:	5b                   	pop    %ebx
  800307:	5e                   	pop    %esi
  800308:	5f                   	pop    %edi
  800309:	5d                   	pop    %ebp
  80030a:	c3                   	ret    

0080030b <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80030b:	55                   	push   %ebp
  80030c:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80030e:	83 fa 01             	cmp    $0x1,%edx
  800311:	7e 0e                	jle    800321 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800313:	8b 10                	mov    (%eax),%edx
  800315:	8d 4a 08             	lea    0x8(%edx),%ecx
  800318:	89 08                	mov    %ecx,(%eax)
  80031a:	8b 02                	mov    (%edx),%eax
  80031c:	8b 52 04             	mov    0x4(%edx),%edx
  80031f:	eb 22                	jmp    800343 <getuint+0x38>
	else if (lflag)
  800321:	85 d2                	test   %edx,%edx
  800323:	74 10                	je     800335 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800325:	8b 10                	mov    (%eax),%edx
  800327:	8d 4a 04             	lea    0x4(%edx),%ecx
  80032a:	89 08                	mov    %ecx,(%eax)
  80032c:	8b 02                	mov    (%edx),%eax
  80032e:	ba 00 00 00 00       	mov    $0x0,%edx
  800333:	eb 0e                	jmp    800343 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800335:	8b 10                	mov    (%eax),%edx
  800337:	8d 4a 04             	lea    0x4(%edx),%ecx
  80033a:	89 08                	mov    %ecx,(%eax)
  80033c:	8b 02                	mov    (%edx),%eax
  80033e:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800343:	5d                   	pop    %ebp
  800344:	c3                   	ret    

00800345 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800345:	55                   	push   %ebp
  800346:	89 e5                	mov    %esp,%ebp
  800348:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80034b:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80034e:	8b 10                	mov    (%eax),%edx
  800350:	3b 50 04             	cmp    0x4(%eax),%edx
  800353:	73 0a                	jae    80035f <sprintputch+0x1a>
		*b->buf++ = ch;
  800355:	8d 4a 01             	lea    0x1(%edx),%ecx
  800358:	89 08                	mov    %ecx,(%eax)
  80035a:	8b 45 08             	mov    0x8(%ebp),%eax
  80035d:	88 02                	mov    %al,(%edx)
}
  80035f:	5d                   	pop    %ebp
  800360:	c3                   	ret    

00800361 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800361:	55                   	push   %ebp
  800362:	89 e5                	mov    %esp,%ebp
  800364:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800367:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80036a:	50                   	push   %eax
  80036b:	ff 75 10             	pushl  0x10(%ebp)
  80036e:	ff 75 0c             	pushl  0xc(%ebp)
  800371:	ff 75 08             	pushl  0x8(%ebp)
  800374:	e8 05 00 00 00       	call   80037e <vprintfmt>
	va_end(ap);
}
  800379:	83 c4 10             	add    $0x10,%esp
  80037c:	c9                   	leave  
  80037d:	c3                   	ret    

0080037e <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80037e:	55                   	push   %ebp
  80037f:	89 e5                	mov    %esp,%ebp
  800381:	57                   	push   %edi
  800382:	56                   	push   %esi
  800383:	53                   	push   %ebx
  800384:	83 ec 2c             	sub    $0x2c,%esp
  800387:	8b 75 08             	mov    0x8(%ebp),%esi
  80038a:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80038d:	8b 7d 10             	mov    0x10(%ebp),%edi
  800390:	eb 12                	jmp    8003a4 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800392:	85 c0                	test   %eax,%eax
  800394:	0f 84 68 03 00 00    	je     800702 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80039a:	83 ec 08             	sub    $0x8,%esp
  80039d:	53                   	push   %ebx
  80039e:	50                   	push   %eax
  80039f:	ff d6                	call   *%esi
  8003a1:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8003a4:	47                   	inc    %edi
  8003a5:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8003a9:	83 f8 25             	cmp    $0x25,%eax
  8003ac:	75 e4                	jne    800392 <vprintfmt+0x14>
  8003ae:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8003b2:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8003b9:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8003c0:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8003c7:	ba 00 00 00 00       	mov    $0x0,%edx
  8003cc:	eb 07                	jmp    8003d5 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ce:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8003d1:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003d5:	8d 47 01             	lea    0x1(%edi),%eax
  8003d8:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8003db:	0f b6 0f             	movzbl (%edi),%ecx
  8003de:	8a 07                	mov    (%edi),%al
  8003e0:	83 e8 23             	sub    $0x23,%eax
  8003e3:	3c 55                	cmp    $0x55,%al
  8003e5:	0f 87 fe 02 00 00    	ja     8006e9 <vprintfmt+0x36b>
  8003eb:	0f b6 c0             	movzbl %al,%eax
  8003ee:	ff 24 85 ac 0f 80 00 	jmp    *0x800fac(,%eax,4)
  8003f5:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003f8:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003fc:	eb d7                	jmp    8003d5 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003fe:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800401:	b8 00 00 00 00       	mov    $0x0,%eax
  800406:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  800409:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80040c:	01 c0                	add    %eax,%eax
  80040e:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800412:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800415:	8d 51 d0             	lea    -0x30(%ecx),%edx
  800418:	83 fa 09             	cmp    $0x9,%edx
  80041b:	77 34                	ja     800451 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80041d:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80041e:	eb e9                	jmp    800409 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800420:	8b 45 14             	mov    0x14(%ebp),%eax
  800423:	8d 48 04             	lea    0x4(%eax),%ecx
  800426:	89 4d 14             	mov    %ecx,0x14(%ebp)
  800429:	8b 00                	mov    (%eax),%eax
  80042b:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80042e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800431:	eb 24                	jmp    800457 <vprintfmt+0xd9>
  800433:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800437:	79 07                	jns    800440 <vprintfmt+0xc2>
  800439:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800440:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800443:	eb 90                	jmp    8003d5 <vprintfmt+0x57>
  800445:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800448:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80044f:	eb 84                	jmp    8003d5 <vprintfmt+0x57>
  800451:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800454:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800457:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80045b:	0f 89 74 ff ff ff    	jns    8003d5 <vprintfmt+0x57>
				width = precision, precision = -1;
  800461:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800464:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800467:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80046e:	e9 62 ff ff ff       	jmp    8003d5 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800473:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800474:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800477:	e9 59 ff ff ff       	jmp    8003d5 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80047c:	8b 45 14             	mov    0x14(%ebp),%eax
  80047f:	8d 50 04             	lea    0x4(%eax),%edx
  800482:	89 55 14             	mov    %edx,0x14(%ebp)
  800485:	83 ec 08             	sub    $0x8,%esp
  800488:	53                   	push   %ebx
  800489:	ff 30                	pushl  (%eax)
  80048b:	ff d6                	call   *%esi
			break;
  80048d:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800490:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800493:	e9 0c ff ff ff       	jmp    8003a4 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800498:	8b 45 14             	mov    0x14(%ebp),%eax
  80049b:	8d 50 04             	lea    0x4(%eax),%edx
  80049e:	89 55 14             	mov    %edx,0x14(%ebp)
  8004a1:	8b 00                	mov    (%eax),%eax
  8004a3:	85 c0                	test   %eax,%eax
  8004a5:	79 02                	jns    8004a9 <vprintfmt+0x12b>
  8004a7:	f7 d8                	neg    %eax
  8004a9:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8004ab:	83 f8 06             	cmp    $0x6,%eax
  8004ae:	7f 0b                	jg     8004bb <vprintfmt+0x13d>
  8004b0:	8b 04 85 04 11 80 00 	mov    0x801104(,%eax,4),%eax
  8004b7:	85 c0                	test   %eax,%eax
  8004b9:	75 18                	jne    8004d3 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8004bb:	52                   	push   %edx
  8004bc:	68 34 0f 80 00       	push   $0x800f34
  8004c1:	53                   	push   %ebx
  8004c2:	56                   	push   %esi
  8004c3:	e8 99 fe ff ff       	call   800361 <printfmt>
  8004c8:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004cb:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8004ce:	e9 d1 fe ff ff       	jmp    8003a4 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8004d3:	50                   	push   %eax
  8004d4:	68 3d 0f 80 00       	push   $0x800f3d
  8004d9:	53                   	push   %ebx
  8004da:	56                   	push   %esi
  8004db:	e8 81 fe ff ff       	call   800361 <printfmt>
  8004e0:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004e3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004e6:	e9 b9 fe ff ff       	jmp    8003a4 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004eb:	8b 45 14             	mov    0x14(%ebp),%eax
  8004ee:	8d 50 04             	lea    0x4(%eax),%edx
  8004f1:	89 55 14             	mov    %edx,0x14(%ebp)
  8004f4:	8b 38                	mov    (%eax),%edi
  8004f6:	85 ff                	test   %edi,%edi
  8004f8:	75 05                	jne    8004ff <vprintfmt+0x181>
				p = "(null)";
  8004fa:	bf 2d 0f 80 00       	mov    $0x800f2d,%edi
			if (width > 0 && padc != '-')
  8004ff:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800503:	0f 8e 90 00 00 00    	jle    800599 <vprintfmt+0x21b>
  800509:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  80050d:	0f 84 8e 00 00 00    	je     8005a1 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800513:	83 ec 08             	sub    $0x8,%esp
  800516:	ff 75 d0             	pushl  -0x30(%ebp)
  800519:	57                   	push   %edi
  80051a:	e8 70 02 00 00       	call   80078f <strnlen>
  80051f:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800522:	29 c1                	sub    %eax,%ecx
  800524:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  800527:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80052a:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  80052e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800531:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800534:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800536:	eb 0d                	jmp    800545 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800538:	83 ec 08             	sub    $0x8,%esp
  80053b:	53                   	push   %ebx
  80053c:	ff 75 e4             	pushl  -0x1c(%ebp)
  80053f:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800541:	4f                   	dec    %edi
  800542:	83 c4 10             	add    $0x10,%esp
  800545:	85 ff                	test   %edi,%edi
  800547:	7f ef                	jg     800538 <vprintfmt+0x1ba>
  800549:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80054c:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80054f:	89 c8                	mov    %ecx,%eax
  800551:	85 c9                	test   %ecx,%ecx
  800553:	79 05                	jns    80055a <vprintfmt+0x1dc>
  800555:	b8 00 00 00 00       	mov    $0x0,%eax
  80055a:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80055d:	29 c1                	sub    %eax,%ecx
  80055f:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800562:	89 75 08             	mov    %esi,0x8(%ebp)
  800565:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800568:	eb 3d                	jmp    8005a7 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80056a:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80056e:	74 19                	je     800589 <vprintfmt+0x20b>
  800570:	0f be c0             	movsbl %al,%eax
  800573:	83 e8 20             	sub    $0x20,%eax
  800576:	83 f8 5e             	cmp    $0x5e,%eax
  800579:	76 0e                	jbe    800589 <vprintfmt+0x20b>
					putch('?', putdat);
  80057b:	83 ec 08             	sub    $0x8,%esp
  80057e:	53                   	push   %ebx
  80057f:	6a 3f                	push   $0x3f
  800581:	ff 55 08             	call   *0x8(%ebp)
  800584:	83 c4 10             	add    $0x10,%esp
  800587:	eb 0b                	jmp    800594 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800589:	83 ec 08             	sub    $0x8,%esp
  80058c:	53                   	push   %ebx
  80058d:	52                   	push   %edx
  80058e:	ff 55 08             	call   *0x8(%ebp)
  800591:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800594:	ff 4d e4             	decl   -0x1c(%ebp)
  800597:	eb 0e                	jmp    8005a7 <vprintfmt+0x229>
  800599:	89 75 08             	mov    %esi,0x8(%ebp)
  80059c:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80059f:	eb 06                	jmp    8005a7 <vprintfmt+0x229>
  8005a1:	89 75 08             	mov    %esi,0x8(%ebp)
  8005a4:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8005a7:	47                   	inc    %edi
  8005a8:	8a 47 ff             	mov    -0x1(%edi),%al
  8005ab:	0f be d0             	movsbl %al,%edx
  8005ae:	85 d2                	test   %edx,%edx
  8005b0:	74 1d                	je     8005cf <vprintfmt+0x251>
  8005b2:	85 f6                	test   %esi,%esi
  8005b4:	78 b4                	js     80056a <vprintfmt+0x1ec>
  8005b6:	4e                   	dec    %esi
  8005b7:	79 b1                	jns    80056a <vprintfmt+0x1ec>
  8005b9:	8b 75 08             	mov    0x8(%ebp),%esi
  8005bc:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8005bf:	eb 14                	jmp    8005d5 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8005c1:	83 ec 08             	sub    $0x8,%esp
  8005c4:	53                   	push   %ebx
  8005c5:	6a 20                	push   $0x20
  8005c7:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005c9:	4f                   	dec    %edi
  8005ca:	83 c4 10             	add    $0x10,%esp
  8005cd:	eb 06                	jmp    8005d5 <vprintfmt+0x257>
  8005cf:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8005d2:	8b 75 08             	mov    0x8(%ebp),%esi
  8005d5:	85 ff                	test   %edi,%edi
  8005d7:	7f e8                	jg     8005c1 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005d9:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005dc:	e9 c3 fd ff ff       	jmp    8003a4 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8005e1:	83 fa 01             	cmp    $0x1,%edx
  8005e4:	7e 16                	jle    8005fc <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8005e6:	8b 45 14             	mov    0x14(%ebp),%eax
  8005e9:	8d 50 08             	lea    0x8(%eax),%edx
  8005ec:	89 55 14             	mov    %edx,0x14(%ebp)
  8005ef:	8b 50 04             	mov    0x4(%eax),%edx
  8005f2:	8b 00                	mov    (%eax),%eax
  8005f4:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005f7:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8005fa:	eb 32                	jmp    80062e <vprintfmt+0x2b0>
	else if (lflag)
  8005fc:	85 d2                	test   %edx,%edx
  8005fe:	74 18                	je     800618 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800600:	8b 45 14             	mov    0x14(%ebp),%eax
  800603:	8d 50 04             	lea    0x4(%eax),%edx
  800606:	89 55 14             	mov    %edx,0x14(%ebp)
  800609:	8b 00                	mov    (%eax),%eax
  80060b:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80060e:	89 c1                	mov    %eax,%ecx
  800610:	c1 f9 1f             	sar    $0x1f,%ecx
  800613:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800616:	eb 16                	jmp    80062e <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  800618:	8b 45 14             	mov    0x14(%ebp),%eax
  80061b:	8d 50 04             	lea    0x4(%eax),%edx
  80061e:	89 55 14             	mov    %edx,0x14(%ebp)
  800621:	8b 00                	mov    (%eax),%eax
  800623:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800626:	89 c1                	mov    %eax,%ecx
  800628:	c1 f9 1f             	sar    $0x1f,%ecx
  80062b:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80062e:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800631:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800634:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800638:	79 76                	jns    8006b0 <vprintfmt+0x332>
				putch('-', putdat);
  80063a:	83 ec 08             	sub    $0x8,%esp
  80063d:	53                   	push   %ebx
  80063e:	6a 2d                	push   $0x2d
  800640:	ff d6                	call   *%esi
				num = -(long long) num;
  800642:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800645:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800648:	f7 d8                	neg    %eax
  80064a:	83 d2 00             	adc    $0x0,%edx
  80064d:	f7 da                	neg    %edx
  80064f:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800652:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800657:	eb 5c                	jmp    8006b5 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800659:	8d 45 14             	lea    0x14(%ebp),%eax
  80065c:	e8 aa fc ff ff       	call   80030b <getuint>
			base = 10;
  800661:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800666:	eb 4d                	jmp    8006b5 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800668:	8d 45 14             	lea    0x14(%ebp),%eax
  80066b:	e8 9b fc ff ff       	call   80030b <getuint>
			base = 8;
  800670:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800675:	eb 3e                	jmp    8006b5 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800677:	83 ec 08             	sub    $0x8,%esp
  80067a:	53                   	push   %ebx
  80067b:	6a 30                	push   $0x30
  80067d:	ff d6                	call   *%esi
			putch('x', putdat);
  80067f:	83 c4 08             	add    $0x8,%esp
  800682:	53                   	push   %ebx
  800683:	6a 78                	push   $0x78
  800685:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800687:	8b 45 14             	mov    0x14(%ebp),%eax
  80068a:	8d 50 04             	lea    0x4(%eax),%edx
  80068d:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800690:	8b 00                	mov    (%eax),%eax
  800692:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800697:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80069a:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  80069f:	eb 14                	jmp    8006b5 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8006a1:	8d 45 14             	lea    0x14(%ebp),%eax
  8006a4:	e8 62 fc ff ff       	call   80030b <getuint>
			base = 16;
  8006a9:	b9 10 00 00 00       	mov    $0x10,%ecx
  8006ae:	eb 05                	jmp    8006b5 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8006b0:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8006b5:	83 ec 0c             	sub    $0xc,%esp
  8006b8:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8006bc:	57                   	push   %edi
  8006bd:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006c0:	51                   	push   %ecx
  8006c1:	52                   	push   %edx
  8006c2:	50                   	push   %eax
  8006c3:	89 da                	mov    %ebx,%edx
  8006c5:	89 f0                	mov    %esi,%eax
  8006c7:	e8 92 fb ff ff       	call   80025e <printnum>
			break;
  8006cc:	83 c4 20             	add    $0x20,%esp
  8006cf:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006d2:	e9 cd fc ff ff       	jmp    8003a4 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8006d7:	83 ec 08             	sub    $0x8,%esp
  8006da:	53                   	push   %ebx
  8006db:	51                   	push   %ecx
  8006dc:	ff d6                	call   *%esi
			break;
  8006de:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006e1:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8006e4:	e9 bb fc ff ff       	jmp    8003a4 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006e9:	83 ec 08             	sub    $0x8,%esp
  8006ec:	53                   	push   %ebx
  8006ed:	6a 25                	push   $0x25
  8006ef:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006f1:	83 c4 10             	add    $0x10,%esp
  8006f4:	eb 01                	jmp    8006f7 <vprintfmt+0x379>
  8006f6:	4f                   	dec    %edi
  8006f7:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006fb:	75 f9                	jne    8006f6 <vprintfmt+0x378>
  8006fd:	e9 a2 fc ff ff       	jmp    8003a4 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800702:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800705:	5b                   	pop    %ebx
  800706:	5e                   	pop    %esi
  800707:	5f                   	pop    %edi
  800708:	5d                   	pop    %ebp
  800709:	c3                   	ret    

0080070a <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80070a:	55                   	push   %ebp
  80070b:	89 e5                	mov    %esp,%ebp
  80070d:	83 ec 18             	sub    $0x18,%esp
  800710:	8b 45 08             	mov    0x8(%ebp),%eax
  800713:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800716:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800719:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  80071d:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800720:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800727:	85 c0                	test   %eax,%eax
  800729:	74 26                	je     800751 <vsnprintf+0x47>
  80072b:	85 d2                	test   %edx,%edx
  80072d:	7e 29                	jle    800758 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80072f:	ff 75 14             	pushl  0x14(%ebp)
  800732:	ff 75 10             	pushl  0x10(%ebp)
  800735:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800738:	50                   	push   %eax
  800739:	68 45 03 80 00       	push   $0x800345
  80073e:	e8 3b fc ff ff       	call   80037e <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800743:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800746:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800749:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80074c:	83 c4 10             	add    $0x10,%esp
  80074f:	eb 0c                	jmp    80075d <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800751:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800756:	eb 05                	jmp    80075d <vsnprintf+0x53>
  800758:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80075d:	c9                   	leave  
  80075e:	c3                   	ret    

0080075f <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80075f:	55                   	push   %ebp
  800760:	89 e5                	mov    %esp,%ebp
  800762:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800765:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800768:	50                   	push   %eax
  800769:	ff 75 10             	pushl  0x10(%ebp)
  80076c:	ff 75 0c             	pushl  0xc(%ebp)
  80076f:	ff 75 08             	pushl  0x8(%ebp)
  800772:	e8 93 ff ff ff       	call   80070a <vsnprintf>
	va_end(ap);

	return rc;
}
  800777:	c9                   	leave  
  800778:	c3                   	ret    

00800779 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800779:	55                   	push   %ebp
  80077a:	89 e5                	mov    %esp,%ebp
  80077c:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  80077f:	b8 00 00 00 00       	mov    $0x0,%eax
  800784:	eb 01                	jmp    800787 <strlen+0xe>
		n++;
  800786:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800787:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80078b:	75 f9                	jne    800786 <strlen+0xd>
		n++;
	return n;
}
  80078d:	5d                   	pop    %ebp
  80078e:	c3                   	ret    

0080078f <strnlen>:

int
strnlen(const char *s, size_t size)
{
  80078f:	55                   	push   %ebp
  800790:	89 e5                	mov    %esp,%ebp
  800792:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800795:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800798:	ba 00 00 00 00       	mov    $0x0,%edx
  80079d:	eb 01                	jmp    8007a0 <strnlen+0x11>
		n++;
  80079f:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8007a0:	39 c2                	cmp    %eax,%edx
  8007a2:	74 08                	je     8007ac <strnlen+0x1d>
  8007a4:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8007a8:	75 f5                	jne    80079f <strnlen+0x10>
  8007aa:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8007ac:	5d                   	pop    %ebp
  8007ad:	c3                   	ret    

008007ae <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8007ae:	55                   	push   %ebp
  8007af:	89 e5                	mov    %esp,%ebp
  8007b1:	53                   	push   %ebx
  8007b2:	8b 45 08             	mov    0x8(%ebp),%eax
  8007b5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8007b8:	89 c2                	mov    %eax,%edx
  8007ba:	42                   	inc    %edx
  8007bb:	41                   	inc    %ecx
  8007bc:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8007bf:	88 5a ff             	mov    %bl,-0x1(%edx)
  8007c2:	84 db                	test   %bl,%bl
  8007c4:	75 f4                	jne    8007ba <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8007c6:	5b                   	pop    %ebx
  8007c7:	5d                   	pop    %ebp
  8007c8:	c3                   	ret    

008007c9 <strcat>:

char *
strcat(char *dst, const char *src)
{
  8007c9:	55                   	push   %ebp
  8007ca:	89 e5                	mov    %esp,%ebp
  8007cc:	53                   	push   %ebx
  8007cd:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8007d0:	53                   	push   %ebx
  8007d1:	e8 a3 ff ff ff       	call   800779 <strlen>
  8007d6:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8007d9:	ff 75 0c             	pushl  0xc(%ebp)
  8007dc:	01 d8                	add    %ebx,%eax
  8007de:	50                   	push   %eax
  8007df:	e8 ca ff ff ff       	call   8007ae <strcpy>
	return dst;
}
  8007e4:	89 d8                	mov    %ebx,%eax
  8007e6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8007e9:	c9                   	leave  
  8007ea:	c3                   	ret    

008007eb <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8007eb:	55                   	push   %ebp
  8007ec:	89 e5                	mov    %esp,%ebp
  8007ee:	56                   	push   %esi
  8007ef:	53                   	push   %ebx
  8007f0:	8b 75 08             	mov    0x8(%ebp),%esi
  8007f3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007f6:	89 f3                	mov    %esi,%ebx
  8007f8:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007fb:	89 f2                	mov    %esi,%edx
  8007fd:	eb 0c                	jmp    80080b <strncpy+0x20>
		*dst++ = *src;
  8007ff:	42                   	inc    %edx
  800800:	8a 01                	mov    (%ecx),%al
  800802:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800805:	80 39 01             	cmpb   $0x1,(%ecx)
  800808:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80080b:	39 da                	cmp    %ebx,%edx
  80080d:	75 f0                	jne    8007ff <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  80080f:	89 f0                	mov    %esi,%eax
  800811:	5b                   	pop    %ebx
  800812:	5e                   	pop    %esi
  800813:	5d                   	pop    %ebp
  800814:	c3                   	ret    

00800815 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800815:	55                   	push   %ebp
  800816:	89 e5                	mov    %esp,%ebp
  800818:	56                   	push   %esi
  800819:	53                   	push   %ebx
  80081a:	8b 75 08             	mov    0x8(%ebp),%esi
  80081d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800820:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800823:	85 c0                	test   %eax,%eax
  800825:	74 1e                	je     800845 <strlcpy+0x30>
  800827:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  80082b:	89 f2                	mov    %esi,%edx
  80082d:	eb 05                	jmp    800834 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  80082f:	42                   	inc    %edx
  800830:	41                   	inc    %ecx
  800831:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800834:	39 c2                	cmp    %eax,%edx
  800836:	74 08                	je     800840 <strlcpy+0x2b>
  800838:	8a 19                	mov    (%ecx),%bl
  80083a:	84 db                	test   %bl,%bl
  80083c:	75 f1                	jne    80082f <strlcpy+0x1a>
  80083e:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800840:	c6 00 00             	movb   $0x0,(%eax)
  800843:	eb 02                	jmp    800847 <strlcpy+0x32>
  800845:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800847:	29 f0                	sub    %esi,%eax
}
  800849:	5b                   	pop    %ebx
  80084a:	5e                   	pop    %esi
  80084b:	5d                   	pop    %ebp
  80084c:	c3                   	ret    

0080084d <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80084d:	55                   	push   %ebp
  80084e:	89 e5                	mov    %esp,%ebp
  800850:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800853:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800856:	eb 02                	jmp    80085a <strcmp+0xd>
		p++, q++;
  800858:	41                   	inc    %ecx
  800859:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80085a:	8a 01                	mov    (%ecx),%al
  80085c:	84 c0                	test   %al,%al
  80085e:	74 04                	je     800864 <strcmp+0x17>
  800860:	3a 02                	cmp    (%edx),%al
  800862:	74 f4                	je     800858 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800864:	0f b6 c0             	movzbl %al,%eax
  800867:	0f b6 12             	movzbl (%edx),%edx
  80086a:	29 d0                	sub    %edx,%eax
}
  80086c:	5d                   	pop    %ebp
  80086d:	c3                   	ret    

0080086e <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  80086e:	55                   	push   %ebp
  80086f:	89 e5                	mov    %esp,%ebp
  800871:	53                   	push   %ebx
  800872:	8b 45 08             	mov    0x8(%ebp),%eax
  800875:	8b 55 0c             	mov    0xc(%ebp),%edx
  800878:	89 c3                	mov    %eax,%ebx
  80087a:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  80087d:	eb 02                	jmp    800881 <strncmp+0x13>
		n--, p++, q++;
  80087f:	40                   	inc    %eax
  800880:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800881:	39 d8                	cmp    %ebx,%eax
  800883:	74 14                	je     800899 <strncmp+0x2b>
  800885:	8a 08                	mov    (%eax),%cl
  800887:	84 c9                	test   %cl,%cl
  800889:	74 04                	je     80088f <strncmp+0x21>
  80088b:	3a 0a                	cmp    (%edx),%cl
  80088d:	74 f0                	je     80087f <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  80088f:	0f b6 00             	movzbl (%eax),%eax
  800892:	0f b6 12             	movzbl (%edx),%edx
  800895:	29 d0                	sub    %edx,%eax
  800897:	eb 05                	jmp    80089e <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800899:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  80089e:	5b                   	pop    %ebx
  80089f:	5d                   	pop    %ebp
  8008a0:	c3                   	ret    

008008a1 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8008a1:	55                   	push   %ebp
  8008a2:	89 e5                	mov    %esp,%ebp
  8008a4:	8b 45 08             	mov    0x8(%ebp),%eax
  8008a7:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8008aa:	eb 05                	jmp    8008b1 <strchr+0x10>
		if (*s == c)
  8008ac:	38 ca                	cmp    %cl,%dl
  8008ae:	74 0c                	je     8008bc <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8008b0:	40                   	inc    %eax
  8008b1:	8a 10                	mov    (%eax),%dl
  8008b3:	84 d2                	test   %dl,%dl
  8008b5:	75 f5                	jne    8008ac <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8008b7:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008bc:	5d                   	pop    %ebp
  8008bd:	c3                   	ret    

008008be <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8008be:	55                   	push   %ebp
  8008bf:	89 e5                	mov    %esp,%ebp
  8008c1:	8b 45 08             	mov    0x8(%ebp),%eax
  8008c4:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8008c7:	eb 05                	jmp    8008ce <strfind+0x10>
		if (*s == c)
  8008c9:	38 ca                	cmp    %cl,%dl
  8008cb:	74 07                	je     8008d4 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8008cd:	40                   	inc    %eax
  8008ce:	8a 10                	mov    (%eax),%dl
  8008d0:	84 d2                	test   %dl,%dl
  8008d2:	75 f5                	jne    8008c9 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8008d4:	5d                   	pop    %ebp
  8008d5:	c3                   	ret    

008008d6 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8008d6:	55                   	push   %ebp
  8008d7:	89 e5                	mov    %esp,%ebp
  8008d9:	57                   	push   %edi
  8008da:	56                   	push   %esi
  8008db:	53                   	push   %ebx
  8008dc:	8b 7d 08             	mov    0x8(%ebp),%edi
  8008df:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8008e2:	85 c9                	test   %ecx,%ecx
  8008e4:	74 36                	je     80091c <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8008e6:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8008ec:	75 28                	jne    800916 <memset+0x40>
  8008ee:	f6 c1 03             	test   $0x3,%cl
  8008f1:	75 23                	jne    800916 <memset+0x40>
		c &= 0xFF;
  8008f3:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8008f7:	89 d3                	mov    %edx,%ebx
  8008f9:	c1 e3 08             	shl    $0x8,%ebx
  8008fc:	89 d6                	mov    %edx,%esi
  8008fe:	c1 e6 18             	shl    $0x18,%esi
  800901:	89 d0                	mov    %edx,%eax
  800903:	c1 e0 10             	shl    $0x10,%eax
  800906:	09 f0                	or     %esi,%eax
  800908:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  80090a:	89 d8                	mov    %ebx,%eax
  80090c:	09 d0                	or     %edx,%eax
  80090e:	c1 e9 02             	shr    $0x2,%ecx
  800911:	fc                   	cld    
  800912:	f3 ab                	rep stos %eax,%es:(%edi)
  800914:	eb 06                	jmp    80091c <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800916:	8b 45 0c             	mov    0xc(%ebp),%eax
  800919:	fc                   	cld    
  80091a:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  80091c:	89 f8                	mov    %edi,%eax
  80091e:	5b                   	pop    %ebx
  80091f:	5e                   	pop    %esi
  800920:	5f                   	pop    %edi
  800921:	5d                   	pop    %ebp
  800922:	c3                   	ret    

00800923 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800923:	55                   	push   %ebp
  800924:	89 e5                	mov    %esp,%ebp
  800926:	57                   	push   %edi
  800927:	56                   	push   %esi
  800928:	8b 45 08             	mov    0x8(%ebp),%eax
  80092b:	8b 75 0c             	mov    0xc(%ebp),%esi
  80092e:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800931:	39 c6                	cmp    %eax,%esi
  800933:	73 33                	jae    800968 <memmove+0x45>
  800935:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800938:	39 d0                	cmp    %edx,%eax
  80093a:	73 2c                	jae    800968 <memmove+0x45>
		s += n;
		d += n;
  80093c:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80093f:	89 d6                	mov    %edx,%esi
  800941:	09 fe                	or     %edi,%esi
  800943:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800949:	75 13                	jne    80095e <memmove+0x3b>
  80094b:	f6 c1 03             	test   $0x3,%cl
  80094e:	75 0e                	jne    80095e <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800950:	83 ef 04             	sub    $0x4,%edi
  800953:	8d 72 fc             	lea    -0x4(%edx),%esi
  800956:	c1 e9 02             	shr    $0x2,%ecx
  800959:	fd                   	std    
  80095a:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80095c:	eb 07                	jmp    800965 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  80095e:	4f                   	dec    %edi
  80095f:	8d 72 ff             	lea    -0x1(%edx),%esi
  800962:	fd                   	std    
  800963:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800965:	fc                   	cld    
  800966:	eb 1d                	jmp    800985 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800968:	89 f2                	mov    %esi,%edx
  80096a:	09 c2                	or     %eax,%edx
  80096c:	f6 c2 03             	test   $0x3,%dl
  80096f:	75 0f                	jne    800980 <memmove+0x5d>
  800971:	f6 c1 03             	test   $0x3,%cl
  800974:	75 0a                	jne    800980 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800976:	c1 e9 02             	shr    $0x2,%ecx
  800979:	89 c7                	mov    %eax,%edi
  80097b:	fc                   	cld    
  80097c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80097e:	eb 05                	jmp    800985 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800980:	89 c7                	mov    %eax,%edi
  800982:	fc                   	cld    
  800983:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800985:	5e                   	pop    %esi
  800986:	5f                   	pop    %edi
  800987:	5d                   	pop    %ebp
  800988:	c3                   	ret    

00800989 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800989:	55                   	push   %ebp
  80098a:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  80098c:	ff 75 10             	pushl  0x10(%ebp)
  80098f:	ff 75 0c             	pushl  0xc(%ebp)
  800992:	ff 75 08             	pushl  0x8(%ebp)
  800995:	e8 89 ff ff ff       	call   800923 <memmove>
}
  80099a:	c9                   	leave  
  80099b:	c3                   	ret    

0080099c <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  80099c:	55                   	push   %ebp
  80099d:	89 e5                	mov    %esp,%ebp
  80099f:	56                   	push   %esi
  8009a0:	53                   	push   %ebx
  8009a1:	8b 45 08             	mov    0x8(%ebp),%eax
  8009a4:	8b 55 0c             	mov    0xc(%ebp),%edx
  8009a7:	89 c6                	mov    %eax,%esi
  8009a9:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8009ac:	eb 14                	jmp    8009c2 <memcmp+0x26>
		if (*s1 != *s2)
  8009ae:	8a 08                	mov    (%eax),%cl
  8009b0:	8a 1a                	mov    (%edx),%bl
  8009b2:	38 d9                	cmp    %bl,%cl
  8009b4:	74 0a                	je     8009c0 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8009b6:	0f b6 c1             	movzbl %cl,%eax
  8009b9:	0f b6 db             	movzbl %bl,%ebx
  8009bc:	29 d8                	sub    %ebx,%eax
  8009be:	eb 0b                	jmp    8009cb <memcmp+0x2f>
		s1++, s2++;
  8009c0:	40                   	inc    %eax
  8009c1:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8009c2:	39 f0                	cmp    %esi,%eax
  8009c4:	75 e8                	jne    8009ae <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8009c6:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8009cb:	5b                   	pop    %ebx
  8009cc:	5e                   	pop    %esi
  8009cd:	5d                   	pop    %ebp
  8009ce:	c3                   	ret    

008009cf <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8009cf:	55                   	push   %ebp
  8009d0:	89 e5                	mov    %esp,%ebp
  8009d2:	53                   	push   %ebx
  8009d3:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8009d6:	89 c1                	mov    %eax,%ecx
  8009d8:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8009db:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009df:	eb 08                	jmp    8009e9 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8009e1:	0f b6 10             	movzbl (%eax),%edx
  8009e4:	39 da                	cmp    %ebx,%edx
  8009e6:	74 05                	je     8009ed <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009e8:	40                   	inc    %eax
  8009e9:	39 c8                	cmp    %ecx,%eax
  8009eb:	72 f4                	jb     8009e1 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8009ed:	5b                   	pop    %ebx
  8009ee:	5d                   	pop    %ebp
  8009ef:	c3                   	ret    

008009f0 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8009f0:	55                   	push   %ebp
  8009f1:	89 e5                	mov    %esp,%ebp
  8009f3:	57                   	push   %edi
  8009f4:	56                   	push   %esi
  8009f5:	53                   	push   %ebx
  8009f6:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009f9:	eb 01                	jmp    8009fc <strtol+0xc>
		s++;
  8009fb:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009fc:	8a 01                	mov    (%ecx),%al
  8009fe:	3c 20                	cmp    $0x20,%al
  800a00:	74 f9                	je     8009fb <strtol+0xb>
  800a02:	3c 09                	cmp    $0x9,%al
  800a04:	74 f5                	je     8009fb <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800a06:	3c 2b                	cmp    $0x2b,%al
  800a08:	75 08                	jne    800a12 <strtol+0x22>
		s++;
  800a0a:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800a0b:	bf 00 00 00 00       	mov    $0x0,%edi
  800a10:	eb 11                	jmp    800a23 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800a12:	3c 2d                	cmp    $0x2d,%al
  800a14:	75 08                	jne    800a1e <strtol+0x2e>
		s++, neg = 1;
  800a16:	41                   	inc    %ecx
  800a17:	bf 01 00 00 00       	mov    $0x1,%edi
  800a1c:	eb 05                	jmp    800a23 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800a1e:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a23:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a27:	0f 84 87 00 00 00    	je     800ab4 <strtol+0xc4>
  800a2d:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800a31:	75 27                	jne    800a5a <strtol+0x6a>
  800a33:	80 39 30             	cmpb   $0x30,(%ecx)
  800a36:	75 22                	jne    800a5a <strtol+0x6a>
  800a38:	e9 88 00 00 00       	jmp    800ac5 <strtol+0xd5>
		s += 2, base = 16;
  800a3d:	83 c1 02             	add    $0x2,%ecx
  800a40:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a47:	eb 11                	jmp    800a5a <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a49:	41                   	inc    %ecx
  800a4a:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a51:	eb 07                	jmp    800a5a <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a53:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a5a:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a5f:	8a 11                	mov    (%ecx),%dl
  800a61:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a64:	80 fb 09             	cmp    $0x9,%bl
  800a67:	77 08                	ja     800a71 <strtol+0x81>
			dig = *s - '0';
  800a69:	0f be d2             	movsbl %dl,%edx
  800a6c:	83 ea 30             	sub    $0x30,%edx
  800a6f:	eb 22                	jmp    800a93 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a71:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a74:	89 f3                	mov    %esi,%ebx
  800a76:	80 fb 19             	cmp    $0x19,%bl
  800a79:	77 08                	ja     800a83 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a7b:	0f be d2             	movsbl %dl,%edx
  800a7e:	83 ea 57             	sub    $0x57,%edx
  800a81:	eb 10                	jmp    800a93 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a83:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a86:	89 f3                	mov    %esi,%ebx
  800a88:	80 fb 19             	cmp    $0x19,%bl
  800a8b:	77 14                	ja     800aa1 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a8d:	0f be d2             	movsbl %dl,%edx
  800a90:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a93:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a96:	7d 09                	jge    800aa1 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a98:	41                   	inc    %ecx
  800a99:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a9d:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a9f:	eb be                	jmp    800a5f <strtol+0x6f>

	if (endptr)
  800aa1:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800aa5:	74 05                	je     800aac <strtol+0xbc>
		*endptr = (char *) s;
  800aa7:	8b 75 0c             	mov    0xc(%ebp),%esi
  800aaa:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800aac:	85 ff                	test   %edi,%edi
  800aae:	74 21                	je     800ad1 <strtol+0xe1>
  800ab0:	f7 d8                	neg    %eax
  800ab2:	eb 1d                	jmp    800ad1 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800ab4:	80 39 30             	cmpb   $0x30,(%ecx)
  800ab7:	75 9a                	jne    800a53 <strtol+0x63>
  800ab9:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800abd:	0f 84 7a ff ff ff    	je     800a3d <strtol+0x4d>
  800ac3:	eb 84                	jmp    800a49 <strtol+0x59>
  800ac5:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800ac9:	0f 84 6e ff ff ff    	je     800a3d <strtol+0x4d>
  800acf:	eb 89                	jmp    800a5a <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800ad1:	5b                   	pop    %ebx
  800ad2:	5e                   	pop    %esi
  800ad3:	5f                   	pop    %edi
  800ad4:	5d                   	pop    %ebp
  800ad5:	c3                   	ret    

00800ad6 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800ad6:	55                   	push   %ebp
  800ad7:	89 e5                	mov    %esp,%ebp
  800ad9:	57                   	push   %edi
  800ada:	56                   	push   %esi
  800adb:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800adc:	b8 00 00 00 00       	mov    $0x0,%eax
  800ae1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800ae4:	8b 55 08             	mov    0x8(%ebp),%edx
  800ae7:	89 c3                	mov    %eax,%ebx
  800ae9:	89 c7                	mov    %eax,%edi
  800aeb:	89 c6                	mov    %eax,%esi
  800aed:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  800aef:	5b                   	pop    %ebx
  800af0:	5e                   	pop    %esi
  800af1:	5f                   	pop    %edi
  800af2:	5d                   	pop    %ebp
  800af3:	c3                   	ret    

00800af4 <sys_cgetc>:

int
sys_cgetc(void)
{
  800af4:	55                   	push   %ebp
  800af5:	89 e5                	mov    %esp,%ebp
  800af7:	57                   	push   %edi
  800af8:	56                   	push   %esi
  800af9:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800afa:	ba 00 00 00 00       	mov    $0x0,%edx
  800aff:	b8 01 00 00 00       	mov    $0x1,%eax
  800b04:	89 d1                	mov    %edx,%ecx
  800b06:	89 d3                	mov    %edx,%ebx
  800b08:	89 d7                	mov    %edx,%edi
  800b0a:	89 d6                	mov    %edx,%esi
  800b0c:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800b0e:	5b                   	pop    %ebx
  800b0f:	5e                   	pop    %esi
  800b10:	5f                   	pop    %edi
  800b11:	5d                   	pop    %ebp
  800b12:	c3                   	ret    

00800b13 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800b13:	55                   	push   %ebp
  800b14:	89 e5                	mov    %esp,%ebp
  800b16:	57                   	push   %edi
  800b17:	56                   	push   %esi
  800b18:	53                   	push   %ebx
  800b19:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b1c:	b9 00 00 00 00       	mov    $0x0,%ecx
  800b21:	b8 03 00 00 00       	mov    $0x3,%eax
  800b26:	8b 55 08             	mov    0x8(%ebp),%edx
  800b29:	89 cb                	mov    %ecx,%ebx
  800b2b:	89 cf                	mov    %ecx,%edi
  800b2d:	89 ce                	mov    %ecx,%esi
  800b2f:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800b31:	85 c0                	test   %eax,%eax
  800b33:	7e 17                	jle    800b4c <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800b35:	83 ec 0c             	sub    $0xc,%esp
  800b38:	50                   	push   %eax
  800b39:	6a 03                	push   $0x3
  800b3b:	68 20 11 80 00       	push   $0x801120
  800b40:	6a 23                	push   $0x23
  800b42:	68 3d 11 80 00       	push   $0x80113d
  800b47:	e8 26 f6 ff ff       	call   800172 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800b4c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800b4f:	5b                   	pop    %ebx
  800b50:	5e                   	pop    %esi
  800b51:	5f                   	pop    %edi
  800b52:	5d                   	pop    %ebp
  800b53:	c3                   	ret    

00800b54 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800b54:	55                   	push   %ebp
  800b55:	89 e5                	mov    %esp,%ebp
  800b57:	57                   	push   %edi
  800b58:	56                   	push   %esi
  800b59:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800b5a:	ba 00 00 00 00       	mov    $0x0,%edx
  800b5f:	b8 02 00 00 00       	mov    $0x2,%eax
  800b64:	89 d1                	mov    %edx,%ecx
  800b66:	89 d3                	mov    %edx,%ebx
  800b68:	89 d7                	mov    %edx,%edi
  800b6a:	89 d6                	mov    %edx,%esi
  800b6c:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800b6e:	5b                   	pop    %ebx
  800b6f:	5e                   	pop    %esi
  800b70:	5f                   	pop    %edi
  800b71:	5d                   	pop    %ebp
  800b72:	c3                   	ret    
  800b73:	90                   	nop

00800b74 <__udivdi3>:
  800b74:	55                   	push   %ebp
  800b75:	57                   	push   %edi
  800b76:	56                   	push   %esi
  800b77:	53                   	push   %ebx
  800b78:	83 ec 1c             	sub    $0x1c,%esp
  800b7b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800b7f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800b83:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800b87:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800b8b:	89 ca                	mov    %ecx,%edx
  800b8d:	89 f8                	mov    %edi,%eax
  800b8f:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800b93:	85 f6                	test   %esi,%esi
  800b95:	75 2d                	jne    800bc4 <__udivdi3+0x50>
  800b97:	39 cf                	cmp    %ecx,%edi
  800b99:	77 65                	ja     800c00 <__udivdi3+0x8c>
  800b9b:	89 fd                	mov    %edi,%ebp
  800b9d:	85 ff                	test   %edi,%edi
  800b9f:	75 0b                	jne    800bac <__udivdi3+0x38>
  800ba1:	b8 01 00 00 00       	mov    $0x1,%eax
  800ba6:	31 d2                	xor    %edx,%edx
  800ba8:	f7 f7                	div    %edi
  800baa:	89 c5                	mov    %eax,%ebp
  800bac:	31 d2                	xor    %edx,%edx
  800bae:	89 c8                	mov    %ecx,%eax
  800bb0:	f7 f5                	div    %ebp
  800bb2:	89 c1                	mov    %eax,%ecx
  800bb4:	89 d8                	mov    %ebx,%eax
  800bb6:	f7 f5                	div    %ebp
  800bb8:	89 cf                	mov    %ecx,%edi
  800bba:	89 fa                	mov    %edi,%edx
  800bbc:	83 c4 1c             	add    $0x1c,%esp
  800bbf:	5b                   	pop    %ebx
  800bc0:	5e                   	pop    %esi
  800bc1:	5f                   	pop    %edi
  800bc2:	5d                   	pop    %ebp
  800bc3:	c3                   	ret    
  800bc4:	39 ce                	cmp    %ecx,%esi
  800bc6:	77 28                	ja     800bf0 <__udivdi3+0x7c>
  800bc8:	0f bd fe             	bsr    %esi,%edi
  800bcb:	83 f7 1f             	xor    $0x1f,%edi
  800bce:	75 40                	jne    800c10 <__udivdi3+0x9c>
  800bd0:	39 ce                	cmp    %ecx,%esi
  800bd2:	72 0a                	jb     800bde <__udivdi3+0x6a>
  800bd4:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800bd8:	0f 87 9e 00 00 00    	ja     800c7c <__udivdi3+0x108>
  800bde:	b8 01 00 00 00       	mov    $0x1,%eax
  800be3:	89 fa                	mov    %edi,%edx
  800be5:	83 c4 1c             	add    $0x1c,%esp
  800be8:	5b                   	pop    %ebx
  800be9:	5e                   	pop    %esi
  800bea:	5f                   	pop    %edi
  800beb:	5d                   	pop    %ebp
  800bec:	c3                   	ret    
  800bed:	8d 76 00             	lea    0x0(%esi),%esi
  800bf0:	31 ff                	xor    %edi,%edi
  800bf2:	31 c0                	xor    %eax,%eax
  800bf4:	89 fa                	mov    %edi,%edx
  800bf6:	83 c4 1c             	add    $0x1c,%esp
  800bf9:	5b                   	pop    %ebx
  800bfa:	5e                   	pop    %esi
  800bfb:	5f                   	pop    %edi
  800bfc:	5d                   	pop    %ebp
  800bfd:	c3                   	ret    
  800bfe:	66 90                	xchg   %ax,%ax
  800c00:	89 d8                	mov    %ebx,%eax
  800c02:	f7 f7                	div    %edi
  800c04:	31 ff                	xor    %edi,%edi
  800c06:	89 fa                	mov    %edi,%edx
  800c08:	83 c4 1c             	add    $0x1c,%esp
  800c0b:	5b                   	pop    %ebx
  800c0c:	5e                   	pop    %esi
  800c0d:	5f                   	pop    %edi
  800c0e:	5d                   	pop    %ebp
  800c0f:	c3                   	ret    
  800c10:	bd 20 00 00 00       	mov    $0x20,%ebp
  800c15:	89 eb                	mov    %ebp,%ebx
  800c17:	29 fb                	sub    %edi,%ebx
  800c19:	89 f9                	mov    %edi,%ecx
  800c1b:	d3 e6                	shl    %cl,%esi
  800c1d:	89 c5                	mov    %eax,%ebp
  800c1f:	88 d9                	mov    %bl,%cl
  800c21:	d3 ed                	shr    %cl,%ebp
  800c23:	89 e9                	mov    %ebp,%ecx
  800c25:	09 f1                	or     %esi,%ecx
  800c27:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800c2b:	89 f9                	mov    %edi,%ecx
  800c2d:	d3 e0                	shl    %cl,%eax
  800c2f:	89 c5                	mov    %eax,%ebp
  800c31:	89 d6                	mov    %edx,%esi
  800c33:	88 d9                	mov    %bl,%cl
  800c35:	d3 ee                	shr    %cl,%esi
  800c37:	89 f9                	mov    %edi,%ecx
  800c39:	d3 e2                	shl    %cl,%edx
  800c3b:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c3f:	88 d9                	mov    %bl,%cl
  800c41:	d3 e8                	shr    %cl,%eax
  800c43:	09 c2                	or     %eax,%edx
  800c45:	89 d0                	mov    %edx,%eax
  800c47:	89 f2                	mov    %esi,%edx
  800c49:	f7 74 24 0c          	divl   0xc(%esp)
  800c4d:	89 d6                	mov    %edx,%esi
  800c4f:	89 c3                	mov    %eax,%ebx
  800c51:	f7 e5                	mul    %ebp
  800c53:	39 d6                	cmp    %edx,%esi
  800c55:	72 19                	jb     800c70 <__udivdi3+0xfc>
  800c57:	74 0b                	je     800c64 <__udivdi3+0xf0>
  800c59:	89 d8                	mov    %ebx,%eax
  800c5b:	31 ff                	xor    %edi,%edi
  800c5d:	e9 58 ff ff ff       	jmp    800bba <__udivdi3+0x46>
  800c62:	66 90                	xchg   %ax,%ax
  800c64:	8b 54 24 08          	mov    0x8(%esp),%edx
  800c68:	89 f9                	mov    %edi,%ecx
  800c6a:	d3 e2                	shl    %cl,%edx
  800c6c:	39 c2                	cmp    %eax,%edx
  800c6e:	73 e9                	jae    800c59 <__udivdi3+0xe5>
  800c70:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800c73:	31 ff                	xor    %edi,%edi
  800c75:	e9 40 ff ff ff       	jmp    800bba <__udivdi3+0x46>
  800c7a:	66 90                	xchg   %ax,%ax
  800c7c:	31 c0                	xor    %eax,%eax
  800c7e:	e9 37 ff ff ff       	jmp    800bba <__udivdi3+0x46>
  800c83:	90                   	nop

00800c84 <__umoddi3>:
  800c84:	55                   	push   %ebp
  800c85:	57                   	push   %edi
  800c86:	56                   	push   %esi
  800c87:	53                   	push   %ebx
  800c88:	83 ec 1c             	sub    $0x1c,%esp
  800c8b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800c8f:	8b 74 24 34          	mov    0x34(%esp),%esi
  800c93:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800c97:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800c9b:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800c9f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800ca3:	89 f3                	mov    %esi,%ebx
  800ca5:	89 fa                	mov    %edi,%edx
  800ca7:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800cab:	89 34 24             	mov    %esi,(%esp)
  800cae:	85 c0                	test   %eax,%eax
  800cb0:	75 1a                	jne    800ccc <__umoddi3+0x48>
  800cb2:	39 f7                	cmp    %esi,%edi
  800cb4:	0f 86 a2 00 00 00    	jbe    800d5c <__umoddi3+0xd8>
  800cba:	89 c8                	mov    %ecx,%eax
  800cbc:	89 f2                	mov    %esi,%edx
  800cbe:	f7 f7                	div    %edi
  800cc0:	89 d0                	mov    %edx,%eax
  800cc2:	31 d2                	xor    %edx,%edx
  800cc4:	83 c4 1c             	add    $0x1c,%esp
  800cc7:	5b                   	pop    %ebx
  800cc8:	5e                   	pop    %esi
  800cc9:	5f                   	pop    %edi
  800cca:	5d                   	pop    %ebp
  800ccb:	c3                   	ret    
  800ccc:	39 f0                	cmp    %esi,%eax
  800cce:	0f 87 ac 00 00 00    	ja     800d80 <__umoddi3+0xfc>
  800cd4:	0f bd e8             	bsr    %eax,%ebp
  800cd7:	83 f5 1f             	xor    $0x1f,%ebp
  800cda:	0f 84 ac 00 00 00    	je     800d8c <__umoddi3+0x108>
  800ce0:	bf 20 00 00 00       	mov    $0x20,%edi
  800ce5:	29 ef                	sub    %ebp,%edi
  800ce7:	89 fe                	mov    %edi,%esi
  800ce9:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800ced:	89 e9                	mov    %ebp,%ecx
  800cef:	d3 e0                	shl    %cl,%eax
  800cf1:	89 d7                	mov    %edx,%edi
  800cf3:	89 f1                	mov    %esi,%ecx
  800cf5:	d3 ef                	shr    %cl,%edi
  800cf7:	09 c7                	or     %eax,%edi
  800cf9:	89 e9                	mov    %ebp,%ecx
  800cfb:	d3 e2                	shl    %cl,%edx
  800cfd:	89 14 24             	mov    %edx,(%esp)
  800d00:	89 d8                	mov    %ebx,%eax
  800d02:	d3 e0                	shl    %cl,%eax
  800d04:	89 c2                	mov    %eax,%edx
  800d06:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d0a:	d3 e0                	shl    %cl,%eax
  800d0c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800d10:	8b 44 24 08          	mov    0x8(%esp),%eax
  800d14:	89 f1                	mov    %esi,%ecx
  800d16:	d3 e8                	shr    %cl,%eax
  800d18:	09 d0                	or     %edx,%eax
  800d1a:	d3 eb                	shr    %cl,%ebx
  800d1c:	89 da                	mov    %ebx,%edx
  800d1e:	f7 f7                	div    %edi
  800d20:	89 d3                	mov    %edx,%ebx
  800d22:	f7 24 24             	mull   (%esp)
  800d25:	89 c6                	mov    %eax,%esi
  800d27:	89 d1                	mov    %edx,%ecx
  800d29:	39 d3                	cmp    %edx,%ebx
  800d2b:	0f 82 87 00 00 00    	jb     800db8 <__umoddi3+0x134>
  800d31:	0f 84 91 00 00 00    	je     800dc8 <__umoddi3+0x144>
  800d37:	8b 54 24 04          	mov    0x4(%esp),%edx
  800d3b:	29 f2                	sub    %esi,%edx
  800d3d:	19 cb                	sbb    %ecx,%ebx
  800d3f:	89 d8                	mov    %ebx,%eax
  800d41:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800d45:	d3 e0                	shl    %cl,%eax
  800d47:	89 e9                	mov    %ebp,%ecx
  800d49:	d3 ea                	shr    %cl,%edx
  800d4b:	09 d0                	or     %edx,%eax
  800d4d:	89 e9                	mov    %ebp,%ecx
  800d4f:	d3 eb                	shr    %cl,%ebx
  800d51:	89 da                	mov    %ebx,%edx
  800d53:	83 c4 1c             	add    $0x1c,%esp
  800d56:	5b                   	pop    %ebx
  800d57:	5e                   	pop    %esi
  800d58:	5f                   	pop    %edi
  800d59:	5d                   	pop    %ebp
  800d5a:	c3                   	ret    
  800d5b:	90                   	nop
  800d5c:	89 fd                	mov    %edi,%ebp
  800d5e:	85 ff                	test   %edi,%edi
  800d60:	75 0b                	jne    800d6d <__umoddi3+0xe9>
  800d62:	b8 01 00 00 00       	mov    $0x1,%eax
  800d67:	31 d2                	xor    %edx,%edx
  800d69:	f7 f7                	div    %edi
  800d6b:	89 c5                	mov    %eax,%ebp
  800d6d:	89 f0                	mov    %esi,%eax
  800d6f:	31 d2                	xor    %edx,%edx
  800d71:	f7 f5                	div    %ebp
  800d73:	89 c8                	mov    %ecx,%eax
  800d75:	f7 f5                	div    %ebp
  800d77:	89 d0                	mov    %edx,%eax
  800d79:	e9 44 ff ff ff       	jmp    800cc2 <__umoddi3+0x3e>
  800d7e:	66 90                	xchg   %ax,%ax
  800d80:	89 c8                	mov    %ecx,%eax
  800d82:	89 f2                	mov    %esi,%edx
  800d84:	83 c4 1c             	add    $0x1c,%esp
  800d87:	5b                   	pop    %ebx
  800d88:	5e                   	pop    %esi
  800d89:	5f                   	pop    %edi
  800d8a:	5d                   	pop    %ebp
  800d8b:	c3                   	ret    
  800d8c:	3b 04 24             	cmp    (%esp),%eax
  800d8f:	72 06                	jb     800d97 <__umoddi3+0x113>
  800d91:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800d95:	77 0f                	ja     800da6 <__umoddi3+0x122>
  800d97:	89 f2                	mov    %esi,%edx
  800d99:	29 f9                	sub    %edi,%ecx
  800d9b:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800d9f:	89 14 24             	mov    %edx,(%esp)
  800da2:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800da6:	8b 44 24 04          	mov    0x4(%esp),%eax
  800daa:	8b 14 24             	mov    (%esp),%edx
  800dad:	83 c4 1c             	add    $0x1c,%esp
  800db0:	5b                   	pop    %ebx
  800db1:	5e                   	pop    %esi
  800db2:	5f                   	pop    %edi
  800db3:	5d                   	pop    %ebp
  800db4:	c3                   	ret    
  800db5:	8d 76 00             	lea    0x0(%esi),%esi
  800db8:	2b 04 24             	sub    (%esp),%eax
  800dbb:	19 fa                	sbb    %edi,%edx
  800dbd:	89 d1                	mov    %edx,%ecx
  800dbf:	89 c6                	mov    %eax,%esi
  800dc1:	e9 71 ff ff ff       	jmp    800d37 <__umoddi3+0xb3>
  800dc6:	66 90                	xchg   %ax,%ax
  800dc8:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800dcc:	72 ea                	jb     800db8 <__umoddi3+0x134>
  800dce:	89 d9                	mov    %ebx,%ecx
  800dd0:	e9 62 ff ff ff       	jmp    800d37 <__umoddi3+0xb3>
