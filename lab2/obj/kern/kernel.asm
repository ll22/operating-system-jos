
obj/kern/kernel:     file format elf32-i386


Disassembly of section .text:

f0100000 <_start+0xeffffff4>:
.globl _start
_start = RELOC(entry)

.globl entry
entry:
	movw	$0x1234, 0x472			# warm boot
f0100000:	02 b0 ad 1b 02 00    	add    0x21bad(%eax),%dh
f0100006:	00 00                	add    %al,(%eax)
f0100008:	fc                   	cld    
f0100009:	4f                   	dec    %edi
f010000a:	52                   	push   %edx
f010000b:	e4 66                	in     $0x66,%al

f010000c <entry>:
f010000c:	66 c7 05 72 04 00 00 	movw   $0x1234,0x472
f0100013:	34 12 
	# sufficient until we set up our real page table in mem_init
	# in lab 2.

	# Load the physical address of entry_pgdir into cr3.  entry_pgdir
	# is defined in entrypgdir.c.
	movl	$(RELOC(entry_pgdir)), %ecx
f0100015:	b9 00 b0 11 00       	mov    $0x11b000,%ecx
	movl	%ecx, %cr3
f010001a:	0f 22 d9             	mov    %ecx,%cr3
	# Enable PSE for 4MB pages.
	movl	%cr4, %ecx
f010001d:	0f 20 e1             	mov    %cr4,%ecx
	orl	$(CR4_PSE), %ecx
f0100020:	83 c9 10             	or     $0x10,%ecx
	movl	%ecx, %cr4
f0100023:	0f 22 e1             	mov    %ecx,%cr4
	# Turn on paging.
	movl	%cr0, %ecx
f0100026:	0f 20 c1             	mov    %cr0,%ecx
	orl	$(CR0_PE|CR0_PG|CR0_WP), %ecx
f0100029:	81 c9 01 00 01 80    	or     $0x80010001,%ecx
	movl	%ecx, %cr0
f010002f:	0f 22 c1             	mov    %ecx,%cr0

	# Now paging is enabled, but we're still running at a low EIP
	# (why is this okay?).  Jump up above KERNBASE before entering
	# C code.
	mov	$relocated, %ecx
f0100032:	b9 39 00 10 f0       	mov    $0xf0100039,%ecx
	jmp	*%ecx
f0100037:	ff e1                	jmp    *%ecx

f0100039 <relocated>:
relocated:

	# Clear the frame pointer register (EBP)
	# so that once we get into debugging C code,
	# stack backtraces will be terminated properly.
	movl	$0x0, %ebp			# nuke frame pointer
f0100039:	bd 00 00 00 00       	mov    $0x0,%ebp

	# Set the stack pointer
	movl	$(bootstacktop),%esp
f010003e:	bc 00 b0 11 f0       	mov    $0xf011b000,%esp

	# pointer to struct multiboot_info
	pushl	%ebx
f0100043:	53                   	push   %ebx
	# saved magic value
	pushl	%eax
f0100044:	50                   	push   %eax

	# now to C code
	call	i386_init
f0100045:	e8 59 00 00 00       	call   f01000a3 <i386_init>

f010004a <spin>:

	# Should never get here, but in case we do, just spin.
spin:	jmp	spin
f010004a:	eb fe                	jmp    f010004a <spin>

f010004c <_panic>:
 * Panic is called on unresolvable fatal errors.
 * It prints "panic: mesg", and then enters the kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt,...)
{
f010004c:	55                   	push   %ebp
f010004d:	89 e5                	mov    %esp,%ebp
f010004f:	56                   	push   %esi
f0100050:	53                   	push   %ebx
f0100051:	8b 75 10             	mov    0x10(%ebp),%esi
	va_list ap;

	if (panicstr)
f0100054:	83 3d 20 df 18 f0 00 	cmpl   $0x0,0xf018df20
f010005b:	75 37                	jne    f0100094 <_panic+0x48>
		goto dead;
	panicstr = fmt;
f010005d:	89 35 20 df 18 f0    	mov    %esi,0xf018df20

	// Be extra sure that the machine is in as reasonable state
	asm volatile("cli; cld");
f0100063:	fa                   	cli    
f0100064:	fc                   	cld    

	va_start(ap, fmt);
f0100065:	8d 5d 14             	lea    0x14(%ebp),%ebx
	cprintf("kernel panic at %s:%d: ", file, line);
f0100068:	83 ec 04             	sub    $0x4,%esp
f010006b:	ff 75 0c             	pushl  0xc(%ebp)
f010006e:	ff 75 08             	pushl  0x8(%ebp)
f0100071:	68 80 4f 10 f0       	push   $0xf0104f80
f0100076:	e8 4e 32 00 00       	call   f01032c9 <cprintf>
	vcprintf(fmt, ap);
f010007b:	83 c4 08             	add    $0x8,%esp
f010007e:	53                   	push   %ebx
f010007f:	56                   	push   %esi
f0100080:	e8 1e 32 00 00       	call   f01032a3 <vcprintf>
	cprintf("\n");
f0100085:	c7 04 24 85 61 10 f0 	movl   $0xf0106185,(%esp)
f010008c:	e8 38 32 00 00       	call   f01032c9 <cprintf>
	va_end(ap);
f0100091:	83 c4 10             	add    $0x10,%esp

dead:
	/* break into the kernel monitor */
	while (1)
		monitor(NULL);
f0100094:	83 ec 0c             	sub    $0xc,%esp
f0100097:	6a 00                	push   $0x0
f0100099:	e8 01 08 00 00       	call   f010089f <monitor>
f010009e:	83 c4 10             	add    $0x10,%esp
f01000a1:	eb f1                	jmp    f0100094 <_panic+0x48>

f01000a3 <i386_init>:
#include <kern/trap.h>


void
i386_init(uint32_t magic, uint32_t addr)
{
f01000a3:	55                   	push   %ebp
f01000a4:	89 e5                	mov    %esp,%ebp
f01000a6:	83 ec 0c             	sub    $0xc,%esp
	extern char edata[], end[];

	// Before doing anything else, complete the ELF loading process.
	// Clear the uninitialized global data (BSS) section of our program.
	// This ensures that all static/global variables start out zero.
	memset(edata, 0, end - edata);
f01000a9:	b8 50 e4 18 f0       	mov    $0xf018e450,%eax
f01000ae:	2d 02 d0 18 f0       	sub    $0xf018d002,%eax
f01000b3:	50                   	push   %eax
f01000b4:	6a 00                	push   $0x0
f01000b6:	68 02 d0 18 f0       	push   $0xf018d002
f01000bb:	e8 58 4a 00 00       	call   f0104b18 <memset>

	// Initialize the console.
	// Can't call cprintf until after we do this!
	cons_init();
f01000c0:	e8 74 04 00 00       	call   f0100539 <cons_init>

	// Must boot from Multiboot.
	assert(magic == MULTIBOOT_BOOTLOADER_MAGIC);
f01000c5:	83 c4 10             	add    $0x10,%esp
f01000c8:	81 7d 08 02 b0 ad 2b 	cmpl   $0x2badb002,0x8(%ebp)
f01000cf:	74 16                	je     f01000e7 <i386_init+0x44>
f01000d1:	68 00 50 10 f0       	push   $0xf0105000
f01000d6:	68 98 4f 10 f0       	push   $0xf0104f98
f01000db:	6a 20                	push   $0x20
f01000dd:	68 ad 4f 10 f0       	push   $0xf0104fad
f01000e2:	e8 65 ff ff ff       	call   f010004c <_panic>

	cprintf("451 decimal is %o octal!\n", 451);
f01000e7:	83 ec 08             	sub    $0x8,%esp
f01000ea:	68 c3 01 00 00       	push   $0x1c3
f01000ef:	68 b9 4f 10 f0       	push   $0xf0104fb9
f01000f4:	e8 d0 31 00 00       	call   f01032c9 <cprintf>

	// Print CPU information.
	cpuid_print();
f01000f9:	e8 4b 41 00 00       	call   f0104249 <cpuid_print>

	// Initialize e820 memory map.
	e820_init(addr);
f01000fe:	83 c4 04             	add    $0x4,%esp
f0100101:	ff 75 0c             	pushl  0xc(%ebp)
f0100104:	e8 da 08 00 00       	call   f01009e3 <e820_init>

	// Lab 2 memory management initialization functions
	mem_init();
f0100109:	e8 2e 13 00 00       	call   f010143c <mem_init>

	// Lab 3 user environment initialization functions
	env_init();
f010010e:	e8 08 2c 00 00       	call   f0102d1b <env_init>
	cprintf("After env_init\n");
f0100113:	c7 04 24 d3 4f 10 f0 	movl   $0xf0104fd3,(%esp)
f010011a:	e8 aa 31 00 00       	call   f01032c9 <cprintf>
	trap_init();
f010011f:	e8 16 32 00 00       	call   f010333a <trap_init>
	//cprintf("H3\n");
	ENV_CREATE(TEST, ENV_TYPE_USER);
	//cprintf("H4\n");
#else
	// Touch all you want.
	ENV_CREATE(user_hello, ENV_TYPE_USER);
f0100124:	83 c4 08             	add    $0x8,%esp
f0100127:	6a 00                	push   $0x0
f0100129:	68 56 c3 11 f0       	push   $0xf011c356
f010012e:	e8 ad 2d 00 00       	call   f0102ee0 <env_create>
	cprintf("After ENV_CREATE(user_hello, ENV_TYPE_USER)\n");
f0100133:	c7 04 24 24 50 10 f0 	movl   $0xf0105024,(%esp)
f010013a:	e8 8a 31 00 00       	call   f01032c9 <cprintf>
#endif // TEST*
	// We only have one user environment for now, so just run it.
	env_run(&envs[0]);
f010013f:	83 c4 04             	add    $0x4,%esp
f0100142:	ff 35 64 d2 18 f0    	pushl  0xf018d264
f0100148:	e8 d3 30 00 00       	call   f0103220 <env_run>

f010014d <_warn>:
}

/* like panic, but don't */
void
_warn(const char *file, int line, const char *fmt,...)
{
f010014d:	55                   	push   %ebp
f010014e:	89 e5                	mov    %esp,%ebp
f0100150:	53                   	push   %ebx
f0100151:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
f0100154:	8d 5d 14             	lea    0x14(%ebp),%ebx
	cprintf("kernel warning at %s:%d: ", file, line);
f0100157:	ff 75 0c             	pushl  0xc(%ebp)
f010015a:	ff 75 08             	pushl  0x8(%ebp)
f010015d:	68 e3 4f 10 f0       	push   $0xf0104fe3
f0100162:	e8 62 31 00 00       	call   f01032c9 <cprintf>
	vcprintf(fmt, ap);
f0100167:	83 c4 08             	add    $0x8,%esp
f010016a:	53                   	push   %ebx
f010016b:	ff 75 10             	pushl  0x10(%ebp)
f010016e:	e8 30 31 00 00       	call   f01032a3 <vcprintf>
	cprintf("\n");
f0100173:	c7 04 24 85 61 10 f0 	movl   $0xf0106185,(%esp)
f010017a:	e8 4a 31 00 00       	call   f01032c9 <cprintf>
	va_end(ap);
}
f010017f:	83 c4 10             	add    $0x10,%esp
f0100182:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0100185:	c9                   	leave  
f0100186:	c3                   	ret    

f0100187 <serial_proc_data>:

static bool serial_exists;

static int
serial_proc_data(void)
{
f0100187:	55                   	push   %ebp
f0100188:	89 e5                	mov    %esp,%ebp

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010018a:	ba fd 03 00 00       	mov    $0x3fd,%edx
f010018f:	ec                   	in     (%dx),%al
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
f0100190:	a8 01                	test   $0x1,%al
f0100192:	74 0b                	je     f010019f <serial_proc_data+0x18>
f0100194:	ba f8 03 00 00       	mov    $0x3f8,%edx
f0100199:	ec                   	in     (%dx),%al
		return -1;
	return inb(COM1+COM_RX);
f010019a:	0f b6 c0             	movzbl %al,%eax
f010019d:	eb 05                	jmp    f01001a4 <serial_proc_data+0x1d>

static int
serial_proc_data(void)
{
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
		return -1;
f010019f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
	return inb(COM1+COM_RX);
}
f01001a4:	5d                   	pop    %ebp
f01001a5:	c3                   	ret    

f01001a6 <cons_intr>:

// called by device interrupt routines to feed input characters
// into the circular console input buffer.
static void
cons_intr(int (*proc)(void))
{
f01001a6:	55                   	push   %ebp
f01001a7:	89 e5                	mov    %esp,%ebp
f01001a9:	53                   	push   %ebx
f01001aa:	83 ec 04             	sub    $0x4,%esp
f01001ad:	89 c3                	mov    %eax,%ebx
	int c;

	while ((c = (*proc)()) != -1) {
f01001af:	eb 2b                	jmp    f01001dc <cons_intr+0x36>
		if (c == 0)
f01001b1:	85 c0                	test   %eax,%eax
f01001b3:	74 27                	je     f01001dc <cons_intr+0x36>
			continue;
		cons.buf[cons.wpos++] = c;
f01001b5:	8b 0d 44 d2 18 f0    	mov    0xf018d244,%ecx
f01001bb:	8d 51 01             	lea    0x1(%ecx),%edx
f01001be:	89 15 44 d2 18 f0    	mov    %edx,0xf018d244
f01001c4:	88 81 40 d0 18 f0    	mov    %al,-0xfe72fc0(%ecx)
		if (cons.wpos == CONSBUFSIZE)
f01001ca:	81 fa 00 02 00 00    	cmp    $0x200,%edx
f01001d0:	75 0a                	jne    f01001dc <cons_intr+0x36>
			cons.wpos = 0;
f01001d2:	c7 05 44 d2 18 f0 00 	movl   $0x0,0xf018d244
f01001d9:	00 00 00 
static void
cons_intr(int (*proc)(void))
{
	int c;

	while ((c = (*proc)()) != -1) {
f01001dc:	ff d3                	call   *%ebx
f01001de:	83 f8 ff             	cmp    $0xffffffff,%eax
f01001e1:	75 ce                	jne    f01001b1 <cons_intr+0xb>
			continue;
		cons.buf[cons.wpos++] = c;
		if (cons.wpos == CONSBUFSIZE)
			cons.wpos = 0;
	}
}
f01001e3:	83 c4 04             	add    $0x4,%esp
f01001e6:	5b                   	pop    %ebx
f01001e7:	5d                   	pop    %ebp
f01001e8:	c3                   	ret    

f01001e9 <kbd_proc_data>:
f01001e9:	ba 64 00 00 00       	mov    $0x64,%edx
f01001ee:	ec                   	in     (%dx),%al
	int c;
	uint8_t stat, data;
	static uint32_t shift;

	stat = inb(KBSTATP);
	if ((stat & KBS_DIB) == 0)
f01001ef:	a8 01                	test   $0x1,%al
f01001f1:	0f 84 ee 00 00 00    	je     f01002e5 <kbd_proc_data+0xfc>
		return -1;
	// Ignore data from mouse.
	if (stat & KBS_TERR)
f01001f7:	a8 20                	test   $0x20,%al
f01001f9:	0f 85 ec 00 00 00    	jne    f01002eb <kbd_proc_data+0x102>
f01001ff:	ba 60 00 00 00       	mov    $0x60,%edx
f0100204:	ec                   	in     (%dx),%al
f0100205:	88 c2                	mov    %al,%dl
		return -1;

	data = inb(KBDATAP);

	if (data == 0xE0) {
f0100207:	3c e0                	cmp    $0xe0,%al
f0100209:	75 0d                	jne    f0100218 <kbd_proc_data+0x2f>
		// E0 escape character
		shift |= E0ESC;
f010020b:	83 0d 20 d0 18 f0 40 	orl    $0x40,0xf018d020
		return 0;
f0100212:	b8 00 00 00 00       	mov    $0x0,%eax
f0100217:	c3                   	ret    
	} else if (data & 0x80) {
f0100218:	84 c0                	test   %al,%al
f010021a:	79 2e                	jns    f010024a <kbd_proc_data+0x61>
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
f010021c:	8b 0d 20 d0 18 f0    	mov    0xf018d020,%ecx
f0100222:	f6 c1 40             	test   $0x40,%cl
f0100225:	75 05                	jne    f010022c <kbd_proc_data+0x43>
f0100227:	83 e0 7f             	and    $0x7f,%eax
f010022a:	88 c2                	mov    %al,%dl
		shift &= ~(shiftcode[data] | E0ESC);
f010022c:	0f b6 c2             	movzbl %dl,%eax
f010022f:	8a 80 a0 51 10 f0    	mov    -0xfefae60(%eax),%al
f0100235:	83 c8 40             	or     $0x40,%eax
f0100238:	0f b6 c0             	movzbl %al,%eax
f010023b:	f7 d0                	not    %eax
f010023d:	21 c8                	and    %ecx,%eax
f010023f:	a3 20 d0 18 f0       	mov    %eax,0xf018d020
		return 0;
f0100244:	b8 00 00 00 00       	mov    $0x0,%eax
		cprintf("Rebooting!\n");
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
}
f0100249:	c3                   	ret    
 * Get data from the keyboard.  If we finish a character, return it.  Else 0.
 * Return -1 if no data.
 */
static int
kbd_proc_data(void)
{
f010024a:	55                   	push   %ebp
f010024b:	89 e5                	mov    %esp,%ebp
f010024d:	53                   	push   %ebx
f010024e:	83 ec 04             	sub    $0x4,%esp
	} else if (data & 0x80) {
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
		shift &= ~(shiftcode[data] | E0ESC);
		return 0;
	} else if (shift & E0ESC) {
f0100251:	8b 0d 20 d0 18 f0    	mov    0xf018d020,%ecx
f0100257:	f6 c1 40             	test   $0x40,%cl
f010025a:	74 0e                	je     f010026a <kbd_proc_data+0x81>
		// Last character was an E0 escape; or with 0x80
		data |= 0x80;
f010025c:	83 c8 80             	or     $0xffffff80,%eax
f010025f:	88 c2                	mov    %al,%dl
		shift &= ~E0ESC;
f0100261:	83 e1 bf             	and    $0xffffffbf,%ecx
f0100264:	89 0d 20 d0 18 f0    	mov    %ecx,0xf018d020
	}

	shift |= shiftcode[data];
f010026a:	0f b6 c2             	movzbl %dl,%eax
	shift ^= togglecode[data];
f010026d:	0f b6 90 a0 51 10 f0 	movzbl -0xfefae60(%eax),%edx
f0100274:	0b 15 20 d0 18 f0    	or     0xf018d020,%edx
f010027a:	0f b6 88 a0 50 10 f0 	movzbl -0xfefaf60(%eax),%ecx
f0100281:	31 ca                	xor    %ecx,%edx
f0100283:	89 15 20 d0 18 f0    	mov    %edx,0xf018d020

	c = charcode[shift & (CTL | SHIFT)][data];
f0100289:	89 d1                	mov    %edx,%ecx
f010028b:	83 e1 03             	and    $0x3,%ecx
f010028e:	8b 0c 8d 80 50 10 f0 	mov    -0xfefaf80(,%ecx,4),%ecx
f0100295:	8a 04 01             	mov    (%ecx,%eax,1),%al
f0100298:	0f b6 d8             	movzbl %al,%ebx
	if (shift & CAPSLOCK) {
f010029b:	f6 c2 08             	test   $0x8,%dl
f010029e:	74 1a                	je     f01002ba <kbd_proc_data+0xd1>
		if ('a' <= c && c <= 'z')
f01002a0:	89 d8                	mov    %ebx,%eax
f01002a2:	8d 4b 9f             	lea    -0x61(%ebx),%ecx
f01002a5:	83 f9 19             	cmp    $0x19,%ecx
f01002a8:	77 05                	ja     f01002af <kbd_proc_data+0xc6>
			c += 'A' - 'a';
f01002aa:	83 eb 20             	sub    $0x20,%ebx
f01002ad:	eb 0b                	jmp    f01002ba <kbd_proc_data+0xd1>
		else if ('A' <= c && c <= 'Z')
f01002af:	83 e8 41             	sub    $0x41,%eax
f01002b2:	83 f8 19             	cmp    $0x19,%eax
f01002b5:	77 03                	ja     f01002ba <kbd_proc_data+0xd1>
			c += 'a' - 'A';
f01002b7:	83 c3 20             	add    $0x20,%ebx
	}

	// Process special keys
	// Ctrl-Alt-Del: reboot
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
f01002ba:	f7 d2                	not    %edx
f01002bc:	f6 c2 06             	test   $0x6,%dl
f01002bf:	75 30                	jne    f01002f1 <kbd_proc_data+0x108>
f01002c1:	81 fb e9 00 00 00    	cmp    $0xe9,%ebx
f01002c7:	75 2c                	jne    f01002f5 <kbd_proc_data+0x10c>
		cprintf("Rebooting!\n");
f01002c9:	83 ec 0c             	sub    $0xc,%esp
f01002cc:	68 51 50 10 f0       	push   $0xf0105051
f01002d1:	e8 f3 2f 00 00       	call   f01032c9 <cprintf>
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01002d6:	ba 92 00 00 00       	mov    $0x92,%edx
f01002db:	b0 03                	mov    $0x3,%al
f01002dd:	ee                   	out    %al,(%dx)
f01002de:	83 c4 10             	add    $0x10,%esp
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f01002e1:	89 d8                	mov    %ebx,%eax
f01002e3:	eb 12                	jmp    f01002f7 <kbd_proc_data+0x10e>
	uint8_t stat, data;
	static uint32_t shift;

	stat = inb(KBSTATP);
	if ((stat & KBS_DIB) == 0)
		return -1;
f01002e5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f01002ea:	c3                   	ret    
	// Ignore data from mouse.
	if (stat & KBS_TERR)
		return -1;
f01002eb:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f01002f0:	c3                   	ret    
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
		cprintf("Rebooting!\n");
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f01002f1:	89 d8                	mov    %ebx,%eax
f01002f3:	eb 02                	jmp    f01002f7 <kbd_proc_data+0x10e>
f01002f5:	89 d8                	mov    %ebx,%eax
}
f01002f7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01002fa:	c9                   	leave  
f01002fb:	c3                   	ret    

f01002fc <cons_putc>:
}

// output a character to the console
static void
cons_putc(int c)
{
f01002fc:	55                   	push   %ebp
f01002fd:	89 e5                	mov    %esp,%ebp
f01002ff:	57                   	push   %edi
f0100300:	56                   	push   %esi
f0100301:	53                   	push   %ebx
f0100302:	83 ec 1c             	sub    $0x1c,%esp
f0100305:	89 c7                	mov    %eax,%edi
f0100307:	bb 01 32 00 00       	mov    $0x3201,%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010030c:	be fd 03 00 00       	mov    $0x3fd,%esi
f0100311:	b9 84 00 00 00       	mov    $0x84,%ecx
f0100316:	eb 06                	jmp    f010031e <cons_putc+0x22>
f0100318:	89 ca                	mov    %ecx,%edx
f010031a:	ec                   	in     (%dx),%al
f010031b:	ec                   	in     (%dx),%al
f010031c:	ec                   	in     (%dx),%al
f010031d:	ec                   	in     (%dx),%al
f010031e:	89 f2                	mov    %esi,%edx
f0100320:	ec                   	in     (%dx),%al
static void
serial_putc(int c)
{
	int i;

	for (i = 0;
f0100321:	a8 20                	test   $0x20,%al
f0100323:	75 03                	jne    f0100328 <cons_putc+0x2c>
	     !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800;
f0100325:	4b                   	dec    %ebx
f0100326:	75 f0                	jne    f0100318 <cons_putc+0x1c>
f0100328:	89 f8                	mov    %edi,%eax
f010032a:	88 45 e7             	mov    %al,-0x19(%ebp)
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f010032d:	ba f8 03 00 00       	mov    $0x3f8,%edx
f0100332:	ee                   	out    %al,(%dx)
f0100333:	bb 01 32 00 00       	mov    $0x3201,%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100338:	be 79 03 00 00       	mov    $0x379,%esi
f010033d:	b9 84 00 00 00       	mov    $0x84,%ecx
f0100342:	eb 06                	jmp    f010034a <cons_putc+0x4e>
f0100344:	89 ca                	mov    %ecx,%edx
f0100346:	ec                   	in     (%dx),%al
f0100347:	ec                   	in     (%dx),%al
f0100348:	ec                   	in     (%dx),%al
f0100349:	ec                   	in     (%dx),%al
f010034a:	89 f2                	mov    %esi,%edx
f010034c:	ec                   	in     (%dx),%al
static void
lpt_putc(int c)
{
	int i;

	for (i = 0; !(inb(0x378+1) & 0x80) && i < 12800; i++)
f010034d:	84 c0                	test   %al,%al
f010034f:	78 03                	js     f0100354 <cons_putc+0x58>
f0100351:	4b                   	dec    %ebx
f0100352:	75 f0                	jne    f0100344 <cons_putc+0x48>
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100354:	ba 78 03 00 00       	mov    $0x378,%edx
f0100359:	8a 45 e7             	mov    -0x19(%ebp),%al
f010035c:	ee                   	out    %al,(%dx)
f010035d:	ba 7a 03 00 00       	mov    $0x37a,%edx
f0100362:	b0 0d                	mov    $0xd,%al
f0100364:	ee                   	out    %al,(%dx)
f0100365:	b0 08                	mov    $0x8,%al
f0100367:	ee                   	out    %al,(%dx)

static void
cga_putc(int c)
{
	// if no attribute given, then use black on white
	if (!(c & ~0xFF))
f0100368:	f7 c7 00 ff ff ff    	test   $0xffffff00,%edi
f010036e:	75 06                	jne    f0100376 <cons_putc+0x7a>
		c |= 0x0500;
f0100370:	81 cf 00 05 00 00    	or     $0x500,%edi

	switch (c & 0xff) {
f0100376:	89 f8                	mov    %edi,%eax
f0100378:	0f b6 c0             	movzbl %al,%eax
f010037b:	83 f8 09             	cmp    $0x9,%eax
f010037e:	74 75                	je     f01003f5 <cons_putc+0xf9>
f0100380:	83 f8 09             	cmp    $0x9,%eax
f0100383:	7f 0a                	jg     f010038f <cons_putc+0x93>
f0100385:	83 f8 08             	cmp    $0x8,%eax
f0100388:	74 14                	je     f010039e <cons_putc+0xa2>
f010038a:	e9 9a 00 00 00       	jmp    f0100429 <cons_putc+0x12d>
f010038f:	83 f8 0a             	cmp    $0xa,%eax
f0100392:	74 38                	je     f01003cc <cons_putc+0xd0>
f0100394:	83 f8 0d             	cmp    $0xd,%eax
f0100397:	74 3b                	je     f01003d4 <cons_putc+0xd8>
f0100399:	e9 8b 00 00 00       	jmp    f0100429 <cons_putc+0x12d>
	case '\b':
		if (crt_pos > 0) {
f010039e:	66 a1 48 d2 18 f0    	mov    0xf018d248,%ax
f01003a4:	66 85 c0             	test   %ax,%ax
f01003a7:	0f 84 e7 00 00 00    	je     f0100494 <cons_putc+0x198>
			crt_pos--;
f01003ad:	48                   	dec    %eax
f01003ae:	66 a3 48 d2 18 f0    	mov    %ax,0xf018d248
			crt_buf[crt_pos] = (c & ~0xff) | ' ';
f01003b4:	0f b7 c0             	movzwl %ax,%eax
f01003b7:	81 e7 00 ff ff ff    	and    $0xffffff00,%edi
f01003bd:	83 cf 20             	or     $0x20,%edi
f01003c0:	8b 15 4c d2 18 f0    	mov    0xf018d24c,%edx
f01003c6:	66 89 3c 42          	mov    %di,(%edx,%eax,2)
f01003ca:	eb 7a                	jmp    f0100446 <cons_putc+0x14a>
		}
		break;
	case '\n':
		crt_pos += CRT_COLS;
f01003cc:	66 83 05 48 d2 18 f0 	addw   $0x50,0xf018d248
f01003d3:	50 
		/* fallthru */
	case '\r':
		crt_pos -= (crt_pos % CRT_COLS);
f01003d4:	66 8b 0d 48 d2 18 f0 	mov    0xf018d248,%cx
f01003db:	bb 50 00 00 00       	mov    $0x50,%ebx
f01003e0:	89 c8                	mov    %ecx,%eax
f01003e2:	ba 00 00 00 00       	mov    $0x0,%edx
f01003e7:	66 f7 f3             	div    %bx
f01003ea:	29 d1                	sub    %edx,%ecx
f01003ec:	66 89 0d 48 d2 18 f0 	mov    %cx,0xf018d248
f01003f3:	eb 51                	jmp    f0100446 <cons_putc+0x14a>
		break;
	case '\t':
		cons_putc(' ');
f01003f5:	b8 20 00 00 00       	mov    $0x20,%eax
f01003fa:	e8 fd fe ff ff       	call   f01002fc <cons_putc>
		cons_putc(' ');
f01003ff:	b8 20 00 00 00       	mov    $0x20,%eax
f0100404:	e8 f3 fe ff ff       	call   f01002fc <cons_putc>
		cons_putc(' ');
f0100409:	b8 20 00 00 00       	mov    $0x20,%eax
f010040e:	e8 e9 fe ff ff       	call   f01002fc <cons_putc>
		cons_putc(' ');
f0100413:	b8 20 00 00 00       	mov    $0x20,%eax
f0100418:	e8 df fe ff ff       	call   f01002fc <cons_putc>
		cons_putc(' ');
f010041d:	b8 20 00 00 00       	mov    $0x20,%eax
f0100422:	e8 d5 fe ff ff       	call   f01002fc <cons_putc>
f0100427:	eb 1d                	jmp    f0100446 <cons_putc+0x14a>
		break;
	default:
		crt_buf[crt_pos++] = c;		/* write the character */
f0100429:	66 a1 48 d2 18 f0    	mov    0xf018d248,%ax
f010042f:	8d 50 01             	lea    0x1(%eax),%edx
f0100432:	66 89 15 48 d2 18 f0 	mov    %dx,0xf018d248
f0100439:	0f b7 c0             	movzwl %ax,%eax
f010043c:	8b 15 4c d2 18 f0    	mov    0xf018d24c,%edx
f0100442:	66 89 3c 42          	mov    %di,(%edx,%eax,2)
		break;
	}

	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
f0100446:	66 81 3d 48 d2 18 f0 	cmpw   $0x7cf,0xf018d248
f010044d:	cf 07 
f010044f:	76 43                	jbe    f0100494 <cons_putc+0x198>
		int i;

		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
f0100451:	a1 4c d2 18 f0       	mov    0xf018d24c,%eax
f0100456:	83 ec 04             	sub    $0x4,%esp
f0100459:	68 00 0f 00 00       	push   $0xf00
f010045e:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
f0100464:	52                   	push   %edx
f0100465:	50                   	push   %eax
f0100466:	e8 fa 46 00 00       	call   f0104b65 <memmove>
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
			crt_buf[i] = 0x0700 | ' ';
f010046b:	8b 15 4c d2 18 f0    	mov    0xf018d24c,%edx
f0100471:	8d 82 00 0f 00 00    	lea    0xf00(%edx),%eax
f0100477:	81 c2 a0 0f 00 00    	add    $0xfa0,%edx
f010047d:	83 c4 10             	add    $0x10,%esp
f0100480:	66 c7 00 20 07       	movw   $0x720,(%eax)
f0100485:	83 c0 02             	add    $0x2,%eax
	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
		int i;

		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
f0100488:	39 d0                	cmp    %edx,%eax
f010048a:	75 f4                	jne    f0100480 <cons_putc+0x184>
			crt_buf[i] = 0x0700 | ' ';
		crt_pos -= CRT_COLS;
f010048c:	66 83 2d 48 d2 18 f0 	subw   $0x50,0xf018d248
f0100493:	50 
	}

	/* move that little blinky thing */
	outb(addr_6845, 14);
f0100494:	8b 0d 50 d2 18 f0    	mov    0xf018d250,%ecx
f010049a:	b0 0e                	mov    $0xe,%al
f010049c:	89 ca                	mov    %ecx,%edx
f010049e:	ee                   	out    %al,(%dx)
	outb(addr_6845 + 1, crt_pos >> 8);
f010049f:	8d 59 01             	lea    0x1(%ecx),%ebx
f01004a2:	66 a1 48 d2 18 f0    	mov    0xf018d248,%ax
f01004a8:	66 c1 e8 08          	shr    $0x8,%ax
f01004ac:	89 da                	mov    %ebx,%edx
f01004ae:	ee                   	out    %al,(%dx)
f01004af:	b0 0f                	mov    $0xf,%al
f01004b1:	89 ca                	mov    %ecx,%edx
f01004b3:	ee                   	out    %al,(%dx)
f01004b4:	a0 48 d2 18 f0       	mov    0xf018d248,%al
f01004b9:	89 da                	mov    %ebx,%edx
f01004bb:	ee                   	out    %al,(%dx)
cons_putc(int c)
{
	serial_putc(c);
	lpt_putc(c);
	cga_putc(c);
}
f01004bc:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01004bf:	5b                   	pop    %ebx
f01004c0:	5e                   	pop    %esi
f01004c1:	5f                   	pop    %edi
f01004c2:	5d                   	pop    %ebp
f01004c3:	c3                   	ret    

f01004c4 <serial_intr>:
}

void
serial_intr(void)
{
	if (serial_exists)
f01004c4:	80 3d 54 d2 18 f0 00 	cmpb   $0x0,0xf018d254
f01004cb:	74 11                	je     f01004de <serial_intr+0x1a>
	return inb(COM1+COM_RX);
}

void
serial_intr(void)
{
f01004cd:	55                   	push   %ebp
f01004ce:	89 e5                	mov    %esp,%ebp
f01004d0:	83 ec 08             	sub    $0x8,%esp
	if (serial_exists)
		cons_intr(serial_proc_data);
f01004d3:	b8 87 01 10 f0       	mov    $0xf0100187,%eax
f01004d8:	e8 c9 fc ff ff       	call   f01001a6 <cons_intr>
}
f01004dd:	c9                   	leave  
f01004de:	c3                   	ret    

f01004df <kbd_intr>:
	return c;
}

void
kbd_intr(void)
{
f01004df:	55                   	push   %ebp
f01004e0:	89 e5                	mov    %esp,%ebp
f01004e2:	83 ec 08             	sub    $0x8,%esp
	cons_intr(kbd_proc_data);
f01004e5:	b8 e9 01 10 f0       	mov    $0xf01001e9,%eax
f01004ea:	e8 b7 fc ff ff       	call   f01001a6 <cons_intr>
}
f01004ef:	c9                   	leave  
f01004f0:	c3                   	ret    

f01004f1 <cons_getc>:
}

// return the next input character from the console, or 0 if none waiting
int
cons_getc(void)
{
f01004f1:	55                   	push   %ebp
f01004f2:	89 e5                	mov    %esp,%ebp
f01004f4:	83 ec 08             	sub    $0x8,%esp
	int c;

	// poll for any pending input characters,
	// so that this function works even when interrupts are disabled
	// (e.g., when called from the kernel monitor).
	serial_intr();
f01004f7:	e8 c8 ff ff ff       	call   f01004c4 <serial_intr>
	kbd_intr();
f01004fc:	e8 de ff ff ff       	call   f01004df <kbd_intr>

	// grab the next character from the input buffer.
	if (cons.rpos != cons.wpos) {
f0100501:	a1 40 d2 18 f0       	mov    0xf018d240,%eax
f0100506:	3b 05 44 d2 18 f0    	cmp    0xf018d244,%eax
f010050c:	74 24                	je     f0100532 <cons_getc+0x41>
		c = cons.buf[cons.rpos++];
f010050e:	8d 50 01             	lea    0x1(%eax),%edx
f0100511:	89 15 40 d2 18 f0    	mov    %edx,0xf018d240
f0100517:	0f b6 80 40 d0 18 f0 	movzbl -0xfe72fc0(%eax),%eax
		if (cons.rpos == CONSBUFSIZE)
f010051e:	81 fa 00 02 00 00    	cmp    $0x200,%edx
f0100524:	75 11                	jne    f0100537 <cons_getc+0x46>
			cons.rpos = 0;
f0100526:	c7 05 40 d2 18 f0 00 	movl   $0x0,0xf018d240
f010052d:	00 00 00 
f0100530:	eb 05                	jmp    f0100537 <cons_getc+0x46>
		return c;
	}
	return 0;
f0100532:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100537:	c9                   	leave  
f0100538:	c3                   	ret    

f0100539 <cons_init>:
}

// initialize the console devices
void
cons_init(void)
{
f0100539:	55                   	push   %ebp
f010053a:	89 e5                	mov    %esp,%ebp
f010053c:	56                   	push   %esi
f010053d:	53                   	push   %ebx
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
f010053e:	66 8b 15 00 80 0b f0 	mov    0xf00b8000,%dx
	*cp = (uint16_t) 0xA55A;
f0100545:	66 c7 05 00 80 0b f0 	movw   $0xa55a,0xf00b8000
f010054c:	5a a5 
	if (*cp != 0xA55A) {
f010054e:	66 a1 00 80 0b f0    	mov    0xf00b8000,%ax
f0100554:	66 3d 5a a5          	cmp    $0xa55a,%ax
f0100558:	74 11                	je     f010056b <cons_init+0x32>
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
		addr_6845 = MONO_BASE;
f010055a:	c7 05 50 d2 18 f0 b4 	movl   $0x3b4,0xf018d250
f0100561:	03 00 00 

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
	*cp = (uint16_t) 0xA55A;
	if (*cp != 0xA55A) {
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
f0100564:	be 00 00 0b f0       	mov    $0xf00b0000,%esi
f0100569:	eb 16                	jmp    f0100581 <cons_init+0x48>
		addr_6845 = MONO_BASE;
	} else {
		*cp = was;
f010056b:	66 89 15 00 80 0b f0 	mov    %dx,0xf00b8000
		addr_6845 = CGA_BASE;
f0100572:	c7 05 50 d2 18 f0 d4 	movl   $0x3d4,0xf018d250
f0100579:	03 00 00 
{
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
f010057c:	be 00 80 0b f0       	mov    $0xf00b8000,%esi
f0100581:	b0 0e                	mov    $0xe,%al
f0100583:	8b 15 50 d2 18 f0    	mov    0xf018d250,%edx
f0100589:	ee                   	out    %al,(%dx)
		addr_6845 = CGA_BASE;
	}

	/* Extract cursor location */
	outb(addr_6845, 14);
	pos = inb(addr_6845 + 1) << 8;
f010058a:	8d 5a 01             	lea    0x1(%edx),%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010058d:	89 da                	mov    %ebx,%edx
f010058f:	ec                   	in     (%dx),%al
f0100590:	0f b6 c8             	movzbl %al,%ecx
f0100593:	c1 e1 08             	shl    $0x8,%ecx
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100596:	b0 0f                	mov    $0xf,%al
f0100598:	8b 15 50 d2 18 f0    	mov    0xf018d250,%edx
f010059e:	ee                   	out    %al,(%dx)

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010059f:	89 da                	mov    %ebx,%edx
f01005a1:	ec                   	in     (%dx),%al
	outb(addr_6845, 15);
	pos |= inb(addr_6845 + 1);

	crt_buf = (uint16_t*) cp;
f01005a2:	89 35 4c d2 18 f0    	mov    %esi,0xf018d24c
	crt_pos = pos;
f01005a8:	0f b6 c0             	movzbl %al,%eax
f01005ab:	09 c8                	or     %ecx,%eax
f01005ad:	66 a3 48 d2 18 f0    	mov    %ax,0xf018d248
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01005b3:	be fa 03 00 00       	mov    $0x3fa,%esi
f01005b8:	b0 00                	mov    $0x0,%al
f01005ba:	89 f2                	mov    %esi,%edx
f01005bc:	ee                   	out    %al,(%dx)
f01005bd:	ba fb 03 00 00       	mov    $0x3fb,%edx
f01005c2:	b0 80                	mov    $0x80,%al
f01005c4:	ee                   	out    %al,(%dx)
f01005c5:	bb f8 03 00 00       	mov    $0x3f8,%ebx
f01005ca:	b0 0c                	mov    $0xc,%al
f01005cc:	89 da                	mov    %ebx,%edx
f01005ce:	ee                   	out    %al,(%dx)
f01005cf:	ba f9 03 00 00       	mov    $0x3f9,%edx
f01005d4:	b0 00                	mov    $0x0,%al
f01005d6:	ee                   	out    %al,(%dx)
f01005d7:	ba fb 03 00 00       	mov    $0x3fb,%edx
f01005dc:	b0 03                	mov    $0x3,%al
f01005de:	ee                   	out    %al,(%dx)
f01005df:	ba fc 03 00 00       	mov    $0x3fc,%edx
f01005e4:	b0 00                	mov    $0x0,%al
f01005e6:	ee                   	out    %al,(%dx)
f01005e7:	ba f9 03 00 00       	mov    $0x3f9,%edx
f01005ec:	b0 01                	mov    $0x1,%al
f01005ee:	ee                   	out    %al,(%dx)

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01005ef:	ba fd 03 00 00       	mov    $0x3fd,%edx
f01005f4:	ec                   	in     (%dx),%al
f01005f5:	88 c1                	mov    %al,%cl
	// Enable rcv interrupts
	outb(COM1+COM_IER, COM_IER_RDI);

	// Clear any preexisting overrun indications and interrupts
	// Serial port doesn't exist if COM_LSR returns 0xFF
	serial_exists = (inb(COM1+COM_LSR) != 0xFF);
f01005f7:	3c ff                	cmp    $0xff,%al
f01005f9:	0f 95 05 54 d2 18 f0 	setne  0xf018d254
f0100600:	89 f2                	mov    %esi,%edx
f0100602:	ec                   	in     (%dx),%al
f0100603:	89 da                	mov    %ebx,%edx
f0100605:	ec                   	in     (%dx),%al
{
	cga_init();
	kbd_init();
	serial_init();

	if (!serial_exists)
f0100606:	80 f9 ff             	cmp    $0xff,%cl
f0100609:	75 10                	jne    f010061b <cons_init+0xe2>
		cprintf("Serial port does not exist!\n");
f010060b:	83 ec 0c             	sub    $0xc,%esp
f010060e:	68 5d 50 10 f0       	push   $0xf010505d
f0100613:	e8 b1 2c 00 00       	call   f01032c9 <cprintf>
f0100618:	83 c4 10             	add    $0x10,%esp
}
f010061b:	8d 65 f8             	lea    -0x8(%ebp),%esp
f010061e:	5b                   	pop    %ebx
f010061f:	5e                   	pop    %esi
f0100620:	5d                   	pop    %ebp
f0100621:	c3                   	ret    

f0100622 <cputchar>:

// `High'-level console I/O.  Used by readline and cprintf.

void
cputchar(int c)
{
f0100622:	55                   	push   %ebp
f0100623:	89 e5                	mov    %esp,%ebp
f0100625:	83 ec 08             	sub    $0x8,%esp
	cons_putc(c);
f0100628:	8b 45 08             	mov    0x8(%ebp),%eax
f010062b:	e8 cc fc ff ff       	call   f01002fc <cons_putc>
}
f0100630:	c9                   	leave  
f0100631:	c3                   	ret    

f0100632 <getchar>:

int
getchar(void)
{
f0100632:	55                   	push   %ebp
f0100633:	89 e5                	mov    %esp,%ebp
f0100635:	83 ec 08             	sub    $0x8,%esp
	int c;

	while ((c = cons_getc()) == 0)
f0100638:	e8 b4 fe ff ff       	call   f01004f1 <cons_getc>
f010063d:	85 c0                	test   %eax,%eax
f010063f:	74 f7                	je     f0100638 <getchar+0x6>
		/* do nothing */;
	return c;
}
f0100641:	c9                   	leave  
f0100642:	c3                   	ret    

f0100643 <iscons>:

int
iscons(int fdnum)
{
f0100643:	55                   	push   %ebp
f0100644:	89 e5                	mov    %esp,%ebp
	// used by readline
	return 1;
}
f0100646:	b8 01 00 00 00       	mov    $0x1,%eax
f010064b:	5d                   	pop    %ebp
f010064c:	c3                   	ret    

f010064d <mon_help>:

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
f010064d:	55                   	push   %ebp
f010064e:	89 e5                	mov    %esp,%ebp
f0100650:	83 ec 0c             	sub    $0xc,%esp
	int i;

	for (i = 0; i < ARRAY_SIZE(commands); i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
f0100653:	68 a0 52 10 f0       	push   $0xf01052a0
f0100658:	68 be 52 10 f0       	push   $0xf01052be
f010065d:	68 c3 52 10 f0       	push   $0xf01052c3
f0100662:	e8 62 2c 00 00       	call   f01032c9 <cprintf>
f0100667:	83 c4 0c             	add    $0xc,%esp
f010066a:	68 84 53 10 f0       	push   $0xf0105384
f010066f:	68 cc 52 10 f0       	push   $0xf01052cc
f0100674:	68 c3 52 10 f0       	push   $0xf01052c3
f0100679:	e8 4b 2c 00 00       	call   f01032c9 <cprintf>
f010067e:	83 c4 0c             	add    $0xc,%esp
f0100681:	68 86 61 10 f0       	push   $0xf0106186
f0100686:	68 d5 52 10 f0       	push   $0xf01052d5
f010068b:	68 c3 52 10 f0       	push   $0xf01052c3
f0100690:	e8 34 2c 00 00       	call   f01032c9 <cprintf>
	return 0;
}
f0100695:	b8 00 00 00 00       	mov    $0x0,%eax
f010069a:	c9                   	leave  
f010069b:	c3                   	ret    

f010069c <mon_kerninfo>:

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
f010069c:	55                   	push   %ebp
f010069d:	89 e5                	mov    %esp,%ebp
f010069f:	83 ec 14             	sub    $0x14,%esp
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
f01006a2:	68 df 52 10 f0       	push   $0xf01052df
f01006a7:	e8 1d 2c 00 00       	call   f01032c9 <cprintf>
	cprintf("  _start                  %08x (phys)\n", _start);
f01006ac:	83 c4 08             	add    $0x8,%esp
f01006af:	68 0c 00 10 00       	push   $0x10000c
f01006b4:	68 ac 53 10 f0       	push   $0xf01053ac
f01006b9:	e8 0b 2c 00 00       	call   f01032c9 <cprintf>
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
f01006be:	83 c4 0c             	add    $0xc,%esp
f01006c1:	68 0c 00 10 00       	push   $0x10000c
f01006c6:	68 0c 00 10 f0       	push   $0xf010000c
f01006cb:	68 d4 53 10 f0       	push   $0xf01053d4
f01006d0:	e8 f4 2b 00 00       	call   f01032c9 <cprintf>
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
f01006d5:	83 c4 0c             	add    $0xc,%esp
f01006d8:	68 79 4f 10 00       	push   $0x104f79
f01006dd:	68 79 4f 10 f0       	push   $0xf0104f79
f01006e2:	68 f8 53 10 f0       	push   $0xf01053f8
f01006e7:	e8 dd 2b 00 00       	call   f01032c9 <cprintf>
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
f01006ec:	83 c4 0c             	add    $0xc,%esp
f01006ef:	68 02 d0 18 00       	push   $0x18d002
f01006f4:	68 02 d0 18 f0       	push   $0xf018d002
f01006f9:	68 1c 54 10 f0       	push   $0xf010541c
f01006fe:	e8 c6 2b 00 00       	call   f01032c9 <cprintf>
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
f0100703:	83 c4 0c             	add    $0xc,%esp
f0100706:	68 50 e4 18 00       	push   $0x18e450
f010070b:	68 50 e4 18 f0       	push   $0xf018e450
f0100710:	68 40 54 10 f0       	push   $0xf0105440
f0100715:	e8 af 2b 00 00       	call   f01032c9 <cprintf>
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
f010071a:	b8 4f e8 18 f0       	mov    $0xf018e84f,%eax
f010071f:	2d 0c 00 10 f0       	sub    $0xf010000c,%eax
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
f0100724:	83 c4 08             	add    $0x8,%esp
f0100727:	25 00 fc ff ff       	and    $0xfffffc00,%eax
f010072c:	89 c2                	mov    %eax,%edx
f010072e:	85 c0                	test   %eax,%eax
f0100730:	79 06                	jns    f0100738 <mon_kerninfo+0x9c>
f0100732:	8d 90 ff 03 00 00    	lea    0x3ff(%eax),%edx
f0100738:	c1 fa 0a             	sar    $0xa,%edx
f010073b:	52                   	push   %edx
f010073c:	68 64 54 10 f0       	push   $0xf0105464
f0100741:	e8 83 2b 00 00       	call   f01032c9 <cprintf>
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}
f0100746:	b8 00 00 00 00       	mov    $0x0,%eax
f010074b:	c9                   	leave  
f010074c:	c3                   	ret    

f010074d <print_stack_Info>:
ebp f010ff78  eip f01008ae  args 00000001 f010ff8c 00000000 f0110580 00000000
       kern/monitor.c:143: monitor+106
       (filename, line#, function name+offset)
*/
void print_stack_Info(uint32_t esp, uint32_t* return_Addr)
{
f010074d:	55                   	push   %ebp
f010074e:	89 e5                	mov    %esp,%ebp
f0100750:	57                   	push   %edi
f0100751:	56                   	push   %esi
f0100752:	53                   	push   %ebx
f0100753:	83 ec 30             	sub    $0x30,%esp
f0100756:	8b 75 0c             	mov    0xc(%ebp),%esi

	//if(0==debuginfo_eip((uintptr_t)(*return_Addr), info)){

	//}

	cprintf("ebp %08x  eip %08x", esp, *return_Addr);  //the current ebp == esp
f0100759:	ff 36                	pushl  (%esi)
f010075b:	ff 75 08             	pushl  0x8(%ebp)
f010075e:	68 f8 52 10 f0       	push   $0xf01052f8
f0100763:	e8 61 2b 00 00       	call   f01032c9 <cprintf>
	//print 5 arguments
	uint32_t arg;
	cprintf("  args");
f0100768:	c7 04 24 0b 53 10 f0 	movl   $0xf010530b,(%esp)
f010076f:	e8 55 2b 00 00       	call   f01032c9 <cprintf>
f0100774:	8d 5e 04             	lea    0x4(%esi),%ebx
f0100777:	8d 7e 18             	lea    0x18(%esi),%edi
f010077a:	83 c4 10             	add    $0x10,%esp
	for(int i=1; i<6; i++){
		arg = *( (uint32_t*)(return_Addr+i) );
		cprintf(" %08x", arg);
f010077d:	83 ec 08             	sub    $0x8,%esp
f0100780:	ff 33                	pushl  (%ebx)
f0100782:	68 05 53 10 f0       	push   $0xf0105305
f0100787:	e8 3d 2b 00 00       	call   f01032c9 <cprintf>
f010078c:	83 c3 04             	add    $0x4,%ebx

	cprintf("ebp %08x  eip %08x", esp, *return_Addr);  //the current ebp == esp
	//print 5 arguments
	uint32_t arg;
	cprintf("  args");
	for(int i=1; i<6; i++){
f010078f:	83 c4 10             	add    $0x10,%esp
f0100792:	39 fb                	cmp    %edi,%ebx
f0100794:	75 e7                	jne    f010077d <print_stack_Info+0x30>
		arg = *( (uint32_t*)(return_Addr+i) );
		cprintf(" %08x", arg);
	}
	cprintf("\n");
f0100796:	83 ec 0c             	sub    $0xc,%esp
f0100799:	68 85 61 10 f0       	push   $0xf0106185
f010079e:	e8 26 2b 00 00       	call   f01032c9 <cprintf>

	//print more readable info
	//struct Eipdebuginfo* info_Ptr;
	debuginfo_eip((uintptr_t)(*return_Addr), info_Ptr); //get the info anyway
f01007a3:	83 c4 08             	add    $0x8,%esp
f01007a6:	8d 45 d0             	lea    -0x30(%ebp),%eax
f01007a9:	50                   	push   %eax
f01007aa:	ff 36                	pushl  (%esi)
f01007ac:	e8 4f 38 00 00       	call   f0104000 <debuginfo_eip>
	//print file name
	cprintf("%s", (info_Ptr->eip_file) );
f01007b1:	83 c4 08             	add    $0x8,%esp
f01007b4:	ff 75 d0             	pushl  -0x30(%ebp)
f01007b7:	68 aa 4f 10 f0       	push   $0xf0104faa
f01007bc:	e8 08 2b 00 00       	call   f01032c9 <cprintf>
	cprintf(":");
f01007c1:	c7 04 24 12 53 10 f0 	movl   $0xf0105312,(%esp)
f01007c8:	e8 fc 2a 00 00       	call   f01032c9 <cprintf>

	//print line number
	//cprintf("%d", (*info_Ptr).eip_line ); //eip_line
	cprintf("%d", info_Ptr->eip_line ); //eip_line
f01007cd:	83 c4 08             	add    $0x8,%esp
f01007d0:	ff 75 d4             	pushl  -0x2c(%ebp)
f01007d3:	68 fe 6b 10 f0       	push   $0xf0106bfe
f01007d8:	e8 ec 2a 00 00       	call   f01032c9 <cprintf>
	cprintf(": ");
f01007dd:	c7 04 24 95 4f 10 f0 	movl   $0xf0104f95,(%esp)
f01007e4:	e8 e0 2a 00 00       	call   f01032c9 <cprintf>
	//print function name
	for(int i=0; i<(info_Ptr->eip_fn_namelen); i++){
f01007e9:	83 c4 10             	add    $0x10,%esp
f01007ec:	bb 00 00 00 00       	mov    $0x0,%ebx
f01007f1:	eb 19                	jmp    f010080c <print_stack_Info+0xbf>
		cprintf("%c", (info_Ptr->eip_fn_name)[i]);
f01007f3:	83 ec 08             	sub    $0x8,%esp
f01007f6:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01007f9:	0f be 04 18          	movsbl (%eax,%ebx,1),%eax
f01007fd:	50                   	push   %eax
f01007fe:	68 14 53 10 f0       	push   $0xf0105314
f0100803:	e8 c1 2a 00 00       	call   f01032c9 <cprintf>
	//print line number
	//cprintf("%d", (*info_Ptr).eip_line ); //eip_line
	cprintf("%d", info_Ptr->eip_line ); //eip_line
	cprintf(": ");
	//print function name
	for(int i=0; i<(info_Ptr->eip_fn_namelen); i++){
f0100808:	43                   	inc    %ebx
f0100809:	83 c4 10             	add    $0x10,%esp
f010080c:	3b 5d dc             	cmp    -0x24(%ebp),%ebx
f010080f:	7c e2                	jl     f01007f3 <print_stack_Info+0xa6>
		cprintf("%c", (info_Ptr->eip_fn_name)[i]);
	}
	//print offset number
	cprintf("+");
f0100811:	83 ec 0c             	sub    $0xc,%esp
f0100814:	68 17 53 10 f0       	push   $0xf0105317
f0100819:	e8 ab 2a 00 00       	call   f01032c9 <cprintf>
	cprintf("%d", ( (int)(*return_Addr) - (int)(info_Ptr->eip_fn_addr) ) );
f010081e:	83 c4 08             	add    $0x8,%esp
f0100821:	8b 06                	mov    (%esi),%eax
f0100823:	2b 45 e0             	sub    -0x20(%ebp),%eax
f0100826:	50                   	push   %eax
f0100827:	68 fe 6b 10 f0       	push   $0xf0106bfe
f010082c:	e8 98 2a 00 00       	call   f01032c9 <cprintf>
	cprintf("\n");
f0100831:	c7 04 24 85 61 10 f0 	movl   $0xf0106185,(%esp)
f0100838:	e8 8c 2a 00 00       	call   f01032c9 <cprintf>
}
f010083d:	83 c4 10             	add    $0x10,%esp
f0100840:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100843:	5b                   	pop    %ebx
f0100844:	5e                   	pop    %esi
f0100845:	5f                   	pop    %edi
f0100846:	5d                   	pop    %ebp
f0100847:	c3                   	ret    

f0100848 <mon_backtrace>:


*/
int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
f0100848:	55                   	push   %ebp
f0100849:	89 e5                	mov    %esp,%ebp
f010084b:	56                   	push   %esi
f010084c:	53                   	push   %ebx

static inline uint32_t
read_ebp(void)
{
	uint32_t ebp;
	asm volatile("movl %%ebp,%0" : "=r" (ebp));
f010084d:	89 ee                	mov    %ebp,%esi
	//function prologue
	//f0100751:	55                   	push   %ebp
	//f0100752:	89 e5                	mov    %esp,%ebp
	// Your code here. -.-
	uint32_t esp = read_ebp(); //since mov %esp,%ebp
	uint32_t ebp = *( (uint32_t*)esp );
f010084f:	8b 1e                	mov    (%esi),%ebx
	uint32_t* return_Addr = (uint32_t*)(esp+4);
	//start print information
	cprintf("Stack backtrace:\n");
f0100851:	83 ec 0c             	sub    $0xc,%esp
f0100854:	68 19 53 10 f0       	push   $0xf0105319
f0100859:	e8 6b 2a 00 00       	call   f01032c9 <cprintf>
	//cprintf("ebp %08x eip %08x\n", esp, return_Addr);  //the current ebp == esp
	print_stack_Info(esp, return_Addr);
f010085e:	83 c4 08             	add    $0x8,%esp
f0100861:	8d 46 04             	lea    0x4(%esi),%eax
f0100864:	50                   	push   %eax
f0100865:	56                   	push   %esi
f0100866:	e8 e2 fe ff ff       	call   f010074d <print_stack_Info>
	//ebp = *((uint32_t*)ebp); //trace back to the last ebp

	//while(ebp != 0xf0111000){ //while not at the 1st stack address
	while(ebp != 0x0){
f010086b:	83 c4 10             	add    $0x10,%esp
f010086e:	eb 12                	jmp    f0100882 <mon_backtrace+0x3a>
		return_Addr = ((uint32_t*)ebp+1);
		//ebp = *((uint32_t*)ebp); //trace back to the last ebp
		//cprintf("ebp %08x eip %08x\n", ebp, return_Addr);  //the current ebp == esp
		print_stack_Info(ebp, return_Addr);
f0100870:	83 ec 08             	sub    $0x8,%esp
f0100873:	8d 43 04             	lea    0x4(%ebx),%eax
f0100876:	50                   	push   %eax
f0100877:	53                   	push   %ebx
f0100878:	e8 d0 fe ff ff       	call   f010074d <print_stack_Info>
		ebp = *((uint32_t*)ebp); //trace back to the last ebp
f010087d:	8b 1b                	mov    (%ebx),%ebx
f010087f:	83 c4 10             	add    $0x10,%esp
	//cprintf("ebp %08x eip %08x\n", esp, return_Addr);  //the current ebp == esp
	print_stack_Info(esp, return_Addr);
	//ebp = *((uint32_t*)ebp); //trace back to the last ebp

	//while(ebp != 0xf0111000){ //while not at the 1st stack address
	while(ebp != 0x0){
f0100882:	85 db                	test   %ebx,%ebx
f0100884:	75 ea                	jne    f0100870 <mon_backtrace+0x28>
		print_stack_Info(ebp, return_Addr);
		ebp = *((uint32_t*)ebp); //trace back to the last ebp
	}

	//Not print the 1st stack address since it is the kernel allocated the beginning of the stack
	cprintf("Stoppp Stack backtrace:\n ");
f0100886:	83 ec 0c             	sub    $0xc,%esp
f0100889:	68 2b 53 10 f0       	push   $0xf010532b
f010088e:	e8 36 2a 00 00       	call   f01032c9 <cprintf>

	//cprintf("argc is %08x\n", argc);
	return 0;
}
f0100893:	b8 00 00 00 00       	mov    $0x0,%eax
f0100898:	8d 65 f8             	lea    -0x8(%ebp),%esp
f010089b:	5b                   	pop    %ebx
f010089c:	5e                   	pop    %esi
f010089d:	5d                   	pop    %ebp
f010089e:	c3                   	ret    

f010089f <monitor>:
	return 0;
}

void
monitor(struct Trapframe *tf)
{
f010089f:	55                   	push   %ebp
f01008a0:	89 e5                	mov    %esp,%ebp
f01008a2:	57                   	push   %edi
f01008a3:	56                   	push   %esi
f01008a4:	53                   	push   %ebx
f01008a5:	83 ec 58             	sub    $0x58,%esp
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
f01008a8:	68 90 54 10 f0       	push   $0xf0105490
f01008ad:	e8 17 2a 00 00       	call   f01032c9 <cprintf>
	cprintf("Type 'help' for a list of commands.\n");
f01008b2:	c7 04 24 b4 54 10 f0 	movl   $0xf01054b4,(%esp)
f01008b9:	e8 0b 2a 00 00       	call   f01032c9 <cprintf>

	if (tf != NULL)
f01008be:	83 c4 10             	add    $0x10,%esp
f01008c1:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
f01008c5:	74 0e                	je     f01008d5 <monitor+0x36>
		print_trapframe(tf);
f01008c7:	83 ec 0c             	sub    $0xc,%esp
f01008ca:	ff 75 08             	pushl  0x8(%ebp)
f01008cd:	e8 e1 30 00 00       	call   f01039b3 <print_trapframe>
f01008d2:	83 c4 10             	add    $0x10,%esp

	while (1) {
		buf = readline("K> ");
f01008d5:	83 ec 0c             	sub    $0xc,%esp
f01008d8:	68 45 53 10 f0       	push   $0xf0105345
f01008dd:	e8 e9 3f 00 00       	call   f01048cb <readline>
f01008e2:	89 c3                	mov    %eax,%ebx
		if (buf != NULL)
f01008e4:	83 c4 10             	add    $0x10,%esp
f01008e7:	85 c0                	test   %eax,%eax
f01008e9:	74 ea                	je     f01008d5 <monitor+0x36>
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
f01008eb:	c7 45 a8 00 00 00 00 	movl   $0x0,-0x58(%ebp)
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
f01008f2:	be 00 00 00 00       	mov    $0x0,%esi
f01008f7:	eb 0a                	jmp    f0100903 <monitor+0x64>
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
f01008f9:	c6 03 00             	movb   $0x0,(%ebx)
f01008fc:	89 f7                	mov    %esi,%edi
f01008fe:	8d 5b 01             	lea    0x1(%ebx),%ebx
f0100901:	89 fe                	mov    %edi,%esi
	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
f0100903:	8a 03                	mov    (%ebx),%al
f0100905:	84 c0                	test   %al,%al
f0100907:	74 60                	je     f0100969 <monitor+0xca>
f0100909:	83 ec 08             	sub    $0x8,%esp
f010090c:	0f be c0             	movsbl %al,%eax
f010090f:	50                   	push   %eax
f0100910:	68 49 53 10 f0       	push   $0xf0105349
f0100915:	e8 c9 41 00 00       	call   f0104ae3 <strchr>
f010091a:	83 c4 10             	add    $0x10,%esp
f010091d:	85 c0                	test   %eax,%eax
f010091f:	75 d8                	jne    f01008f9 <monitor+0x5a>
			*buf++ = 0;
		if (*buf == 0)
f0100921:	80 3b 00             	cmpb   $0x0,(%ebx)
f0100924:	74 43                	je     f0100969 <monitor+0xca>
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
f0100926:	83 fe 0f             	cmp    $0xf,%esi
f0100929:	75 14                	jne    f010093f <monitor+0xa0>
			cprintf("Too many arguments (max %d)\n", MAXARGS);
f010092b:	83 ec 08             	sub    $0x8,%esp
f010092e:	6a 10                	push   $0x10
f0100930:	68 4e 53 10 f0       	push   $0xf010534e
f0100935:	e8 8f 29 00 00       	call   f01032c9 <cprintf>
f010093a:	83 c4 10             	add    $0x10,%esp
f010093d:	eb 96                	jmp    f01008d5 <monitor+0x36>
			return 0;
		}
		argv[argc++] = buf;
f010093f:	8d 7e 01             	lea    0x1(%esi),%edi
f0100942:	89 5c b5 a8          	mov    %ebx,-0x58(%ebp,%esi,4)
f0100946:	eb 01                	jmp    f0100949 <monitor+0xaa>
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
f0100948:	43                   	inc    %ebx
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
f0100949:	8a 03                	mov    (%ebx),%al
f010094b:	84 c0                	test   %al,%al
f010094d:	74 b2                	je     f0100901 <monitor+0x62>
f010094f:	83 ec 08             	sub    $0x8,%esp
f0100952:	0f be c0             	movsbl %al,%eax
f0100955:	50                   	push   %eax
f0100956:	68 49 53 10 f0       	push   $0xf0105349
f010095b:	e8 83 41 00 00       	call   f0104ae3 <strchr>
f0100960:	83 c4 10             	add    $0x10,%esp
f0100963:	85 c0                	test   %eax,%eax
f0100965:	74 e1                	je     f0100948 <monitor+0xa9>
f0100967:	eb 98                	jmp    f0100901 <monitor+0x62>
			buf++;
	}
	argv[argc] = 0;
f0100969:	c7 44 b5 a8 00 00 00 	movl   $0x0,-0x58(%ebp,%esi,4)
f0100970:	00 

	// Lookup and invoke the command
	if (argc == 0)
f0100971:	85 f6                	test   %esi,%esi
f0100973:	0f 84 5c ff ff ff    	je     f01008d5 <monitor+0x36>
f0100979:	bf e0 54 10 f0       	mov    $0xf01054e0,%edi
f010097e:	bb 00 00 00 00       	mov    $0x0,%ebx
		return 0;
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
f0100983:	83 ec 08             	sub    $0x8,%esp
f0100986:	ff 37                	pushl  (%edi)
f0100988:	ff 75 a8             	pushl  -0x58(%ebp)
f010098b:	e8 ff 40 00 00       	call   f0104a8f <strcmp>
f0100990:	83 c4 10             	add    $0x10,%esp
f0100993:	85 c0                	test   %eax,%eax
f0100995:	75 23                	jne    f01009ba <monitor+0x11b>
			return commands[i].func(argc, argv, tf);
f0100997:	83 ec 04             	sub    $0x4,%esp
f010099a:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
f010099d:	01 c3                	add    %eax,%ebx
f010099f:	ff 75 08             	pushl  0x8(%ebp)
f01009a2:	8d 45 a8             	lea    -0x58(%ebp),%eax
f01009a5:	50                   	push   %eax
f01009a6:	56                   	push   %esi
f01009a7:	ff 14 9d e8 54 10 f0 	call   *-0xfefab18(,%ebx,4)
		print_trapframe(tf);

	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
f01009ae:	83 c4 10             	add    $0x10,%esp
f01009b1:	85 c0                	test   %eax,%eax
f01009b3:	78 26                	js     f01009db <monitor+0x13c>
f01009b5:	e9 1b ff ff ff       	jmp    f01008d5 <monitor+0x36>
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
f01009ba:	43                   	inc    %ebx
f01009bb:	83 c7 0c             	add    $0xc,%edi
f01009be:	83 fb 03             	cmp    $0x3,%ebx
f01009c1:	75 c0                	jne    f0100983 <monitor+0xe4>
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
f01009c3:	83 ec 08             	sub    $0x8,%esp
f01009c6:	ff 75 a8             	pushl  -0x58(%ebp)
f01009c9:	68 6b 53 10 f0       	push   $0xf010536b
f01009ce:	e8 f6 28 00 00       	call   f01032c9 <cprintf>
f01009d3:	83 c4 10             	add    $0x10,%esp
f01009d6:	e9 fa fe ff ff       	jmp    f01008d5 <monitor+0x36>
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
f01009db:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01009de:	5b                   	pop    %ebx
f01009df:	5e                   	pop    %esi
f01009e0:	5f                   	pop    %edi
f01009e1:	5d                   	pop    %ebp
f01009e2:	c3                   	ret    

f01009e3 <e820_init>:

// This function may ONLY be used during initialization,
// before page_init().
void
e820_init(physaddr_t mbi_pa)
{
f01009e3:	55                   	push   %ebp
f01009e4:	89 e5                	mov    %esp,%ebp
f01009e6:	57                   	push   %edi
f01009e7:	56                   	push   %esi
f01009e8:	53                   	push   %ebx
f01009e9:	83 ec 1c             	sub    $0x1c,%esp
f01009ec:	8b 75 08             	mov    0x8(%ebp),%esi
	struct multiboot_info *mbi;
	uint32_t addr, addr_end, i;

	mbi = (struct multiboot_info *)mbi_pa;
	assert(mbi->flags & MULTIBOOT_INFO_MEM_MAP);
f01009ef:	f6 06 40             	testb  $0x40,(%esi)
f01009f2:	75 16                	jne    f0100a0a <e820_init+0x27>
f01009f4:	68 04 55 10 f0       	push   $0xf0105504
f01009f9:	68 98 4f 10 f0       	push   $0xf0104f98
f01009fe:	6a 26                	push   $0x26
f0100a00:	68 57 55 10 f0       	push   $0xf0105557
f0100a05:	e8 42 f6 ff ff       	call   f010004c <_panic>
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);
f0100a0a:	8b 56 30             	mov    0x30(%esi),%edx
	struct multiboot_info *mbi;
	uint32_t addr, addr_end, i;

	mbi = (struct multiboot_info *)mbi_pa;
	assert(mbi->flags & MULTIBOOT_INFO_MEM_MAP);
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
f0100a0d:	83 ec 04             	sub    $0x4,%esp
f0100a10:	89 d0                	mov    %edx,%eax
f0100a12:	03 46 2c             	add    0x2c(%esi),%eax
f0100a15:	48                   	dec    %eax
f0100a16:	50                   	push   %eax
f0100a17:	52                   	push   %edx
f0100a18:	68 28 55 10 f0       	push   $0xf0105528
f0100a1d:	e8 a7 28 00 00       	call   f01032c9 <cprintf>
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);

	addr = mbi->mmap_addr;
f0100a22:	8b 5e 30             	mov    0x30(%esi),%ebx
	addr_end = mbi->mmap_addr + mbi->mmap_length;
f0100a25:	89 d8                	mov    %ebx,%eax
f0100a27:	03 46 2c             	add    0x2c(%esi),%eax
f0100a2a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0100a2d:	c7 45 dc 44 df 18 f0 	movl   $0xf018df44,-0x24(%ebp)
	for (i = 0; addr < addr_end; ++i) {
f0100a34:	83 c4 10             	add    $0x10,%esp
f0100a37:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
f0100a3e:	e9 b5 00 00 00       	jmp    f0100af8 <e820_init+0x115>
		struct multiboot_mmap_entry *e;

		// Print memory mapping.
		assert(addr_end - addr >= sizeof(*e));
f0100a43:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100a46:	29 d8                	sub    %ebx,%eax
f0100a48:	83 f8 17             	cmp    $0x17,%eax
f0100a4b:	77 16                	ja     f0100a63 <e820_init+0x80>
f0100a4d:	68 63 55 10 f0       	push   $0xf0105563
f0100a52:	68 98 4f 10 f0       	push   $0xf0104f98
f0100a57:	6a 30                	push   $0x30
f0100a59:	68 57 55 10 f0       	push   $0xf0105557
f0100a5e:	e8 e9 f5 ff ff       	call   f010004c <_panic>
		e = (struct multiboot_mmap_entry *)addr;
f0100a63:	89 de                	mov    %ebx,%esi
		cprintf("  [mem %08p-%08p] ",
f0100a65:	8b 53 04             	mov    0x4(%ebx),%edx
f0100a68:	83 ec 04             	sub    $0x4,%esp
f0100a6b:	89 d0                	mov    %edx,%eax
f0100a6d:	03 43 0c             	add    0xc(%ebx),%eax
f0100a70:	48                   	dec    %eax
f0100a71:	50                   	push   %eax
f0100a72:	52                   	push   %edx
f0100a73:	68 81 55 10 f0       	push   $0xf0105581
f0100a78:	e8 4c 28 00 00       	call   f01032c9 <cprintf>
			(uintptr_t)e->e820.addr,
			(uintptr_t)(e->e820.addr + e->e820.len - 1));
		print_e820_map_type(e->e820.type);
f0100a7d:	8b 43 14             	mov    0x14(%ebx),%eax
	"unusable",
};

static void
print_e820_map_type(uint32_t type) {
	switch (type) {
f0100a80:	83 c4 10             	add    $0x10,%esp
f0100a83:	8d 50 ff             	lea    -0x1(%eax),%edx
f0100a86:	83 fa 04             	cmp    $0x4,%edx
f0100a89:	77 14                	ja     f0100a9f <e820_init+0xbc>
	case 1 ... 5:
		cprintf(e820_map_types[type - 1]);
f0100a8b:	83 ec 0c             	sub    $0xc,%esp
f0100a8e:	ff 34 85 e0 55 10 f0 	pushl  -0xfefaa20(,%eax,4)
f0100a95:	e8 2f 28 00 00       	call   f01032c9 <cprintf>
f0100a9a:	83 c4 10             	add    $0x10,%esp
f0100a9d:	eb 11                	jmp    f0100ab0 <e820_init+0xcd>
		break;
	default:
		cprintf("type %u", type);
f0100a9f:	83 ec 08             	sub    $0x8,%esp
f0100aa2:	50                   	push   %eax
f0100aa3:	68 94 55 10 f0       	push   $0xf0105594
f0100aa8:	e8 1c 28 00 00       	call   f01032c9 <cprintf>
f0100aad:	83 c4 10             	add    $0x10,%esp
		e = (struct multiboot_mmap_entry *)addr;
		cprintf("  [mem %08p-%08p] ",
			(uintptr_t)e->e820.addr,
			(uintptr_t)(e->e820.addr + e->e820.len - 1));
		print_e820_map_type(e->e820.type);
		cprintf("\n");
f0100ab0:	83 ec 0c             	sub    $0xc,%esp
f0100ab3:	68 85 61 10 f0       	push   $0xf0106185
f0100ab8:	e8 0c 28 00 00       	call   f01032c9 <cprintf>

		// Save a copy.
		assert(i < E820_NR_MAX);
f0100abd:	83 c4 10             	add    $0x10,%esp
f0100ac0:	83 7d e0 40          	cmpl   $0x40,-0x20(%ebp)
f0100ac4:	75 16                	jne    f0100adc <e820_init+0xf9>
f0100ac6:	68 9c 55 10 f0       	push   $0xf010559c
f0100acb:	68 98 4f 10 f0       	push   $0xf0104f98
f0100ad0:	6a 39                	push   $0x39
f0100ad2:	68 57 55 10 f0       	push   $0xf0105557
f0100ad7:	e8 70 f5 ff ff       	call   f010004c <_panic>
		e820_map.entries[i] = e->e820;
f0100adc:	89 f0                	mov    %esi,%eax
f0100ade:	8d 76 04             	lea    0x4(%esi),%esi
f0100ae1:	b9 05 00 00 00       	mov    $0x5,%ecx
f0100ae6:	8b 7d dc             	mov    -0x24(%ebp),%edi
f0100ae9:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
		addr += (e->size + 4);
f0100aeb:	8b 00                	mov    (%eax),%eax
f0100aed:	8d 5c 03 04          	lea    0x4(%ebx,%eax,1),%ebx
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);

	addr = mbi->mmap_addr;
	addr_end = mbi->mmap_addr + mbi->mmap_length;
	for (i = 0; addr < addr_end; ++i) {
f0100af1:	ff 45 e0             	incl   -0x20(%ebp)
f0100af4:	83 45 dc 14          	addl   $0x14,-0x24(%ebp)
f0100af8:	3b 5d e4             	cmp    -0x1c(%ebp),%ebx
f0100afb:	0f 82 42 ff ff ff    	jb     f0100a43 <e820_init+0x60>
		// Save a copy.
		assert(i < E820_NR_MAX);
		e820_map.entries[i] = e->e820;
		addr += (e->size + 4);
	}
	e820_map.nr = i;
f0100b01:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100b04:	a3 40 df 18 f0       	mov    %eax,0xf018df40
}
f0100b09:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100b0c:	5b                   	pop    %ebx
f0100b0d:	5e                   	pop    %esi
f0100b0e:	5f                   	pop    %edi
f0100b0f:	5d                   	pop    %ebp
f0100b10:	c3                   	ret    

f0100b11 <boot_alloc>:
// If we're out of memory, boot_alloc should panic.
// This function may ONLY be used during initialization,
// before the page_free_list list has been set up.
static void *
boot_alloc(uint32_t n)
{
f0100b11:	89 c1                	mov    %eax,%ecx
	// Initialize nextfree if this is the first time.
	// 'end' is a magic symbol automatically generated by the linker,
	// which points to the end of the kernel's bss segment:
	// the first virtual address that the linker did *not* assign
	// to any kernel code or global variables.
	if (!nextfree) {
f0100b13:	83 3d 58 d2 18 f0 00 	cmpl   $0x0,0xf018d258
f0100b1a:	75 0f                	jne    f0100b2b <boot_alloc+0x1a>
		extern char end[]; //points to the end of the kernel
		nextfree = ROUNDUP((char *) end, PGSIZE); //set the nextfree pointer
f0100b1c:	b8 4f f4 18 f0       	mov    $0xf018f44f,%eax
f0100b21:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0100b26:	a3 58 d2 18 f0       	mov    %eax,0xf018d258
	//
	// LAB 2: Your code here.
	//check if there's current enough space to allocate
	//(npages * PGSIZE) is the total memory space
	//if( ((npages * PGSIZE) - nextfree ) <  ROUNDUP((char *) n, PGSIZE) ){
	if( ( (char*)(npages * PGSIZE) - nextfree ) <  n ){
f0100b2b:	a1 58 d2 18 f0       	mov    0xf018d258,%eax
f0100b30:	8b 15 44 e4 18 f0    	mov    0xf018e444,%edx
f0100b36:	c1 e2 0c             	shl    $0xc,%edx
f0100b39:	29 c2                	sub    %eax,%edx
f0100b3b:	39 ca                	cmp    %ecx,%edx
f0100b3d:	73 17                	jae    f0100b56 <boot_alloc+0x45>
// If we're out of memory, boot_alloc should panic.
// This function may ONLY be used during initialization,
// before the page_free_list list has been set up.
static void *
boot_alloc(uint32_t n)
{
f0100b3f:	55                   	push   %ebp
f0100b40:	89 e5                	mov    %esp,%ebp
f0100b42:	83 ec 0c             	sub    $0xc,%esp
	// LAB 2: Your code here.
	//check if there's current enough space to allocate
	//(npages * PGSIZE) is the total memory space
	//if( ((npages * PGSIZE) - nextfree ) <  ROUNDUP((char *) n, PGSIZE) ){
	if( ( (char*)(npages * PGSIZE) - nextfree ) <  n ){
		panic("boot_alloc: not enough physical address to allocate\n");
f0100b45:	68 f8 55 10 f0       	push   $0xf01055f8
f0100b4a:	6a 62                	push   $0x62
f0100b4c:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100b51:	e8 f6 f4 ff ff       	call   f010004c <_panic>
	}
	//nextfree = ROUNDUP( ((char *)n + nextfree), PGSIZE); //update the nextfree pointer
	result = nextfree;
	nextfree = n + nextfree; //update the nextfree pointer
f0100b56:	01 c1                	add    %eax,%ecx
f0100b58:	89 0d 58 d2 18 f0    	mov    %ecx,0xf018d258
	return result;
	//return NULL;
}
f0100b5e:	c3                   	ret    

f0100b5f <check_va2pa>:
check_va2pa(pde_t *pgdir, uintptr_t va)
{
	pte_t *p;
	//cprintf("VA check_va2pa looks at is: %x\n", va);
	pgdir = &pgdir[PDX(va)];
	if (!(*pgdir & PTE_P)){ //no page directory entry there
f0100b5f:	89 d1                	mov    %edx,%ecx
f0100b61:	c1 e9 16             	shr    $0x16,%ecx
f0100b64:	8b 04 88             	mov    (%eax,%ecx,4),%eax
f0100b67:	a8 01                	test   $0x1,%al
f0100b69:	74 47                	je     f0100bb2 <check_va2pa+0x53>
		//cprintf("check_va2pa no pde, return fff...\n");
		return ~0;
	}

	p = (pte_t*) KADDR(PTE_ADDR(*pgdir)); //convert PA to VA so that we can deref later to get pte
f0100b6b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100b70:	89 c1                	mov    %eax,%ecx
f0100b72:	c1 e9 0c             	shr    $0xc,%ecx
f0100b75:	3b 0d 44 e4 18 f0    	cmp    0xf018e444,%ecx
f0100b7b:	72 1b                	jb     f0100b98 <check_va2pa+0x39>
// this functionality for us!  We define our own version to help check
// the check_kern_pgdir() function; it shouldn't be used elsewhere.

static physaddr_t
check_va2pa(pde_t *pgdir, uintptr_t va)
{
f0100b7d:	55                   	push   %ebp
f0100b7e:	89 e5                	mov    %esp,%ebp
f0100b80:	83 ec 08             	sub    $0x8,%esp
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0100b83:	50                   	push   %eax
f0100b84:	68 30 56 10 f0       	push   $0xf0105630
f0100b89:	68 f0 03 00 00       	push   $0x3f0
f0100b8e:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100b93:	e8 b4 f4 ff ff       	call   f010004c <_panic>
		return ~0;
	}

	p = (pte_t*) KADDR(PTE_ADDR(*pgdir)); //convert PA to VA so that we can deref later to get pte
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	if (!(p[PTX(va)] & PTE_P)){ //no page table entry there
f0100b98:	c1 ea 0c             	shr    $0xc,%edx
f0100b9b:	81 e2 ff 03 00 00    	and    $0x3ff,%edx
f0100ba1:	8b 84 90 00 00 00 f0 	mov    -0x10000000(%eax,%edx,4),%eax
f0100ba8:	a8 01                	test   $0x1,%al
f0100baa:	74 0c                	je     f0100bb8 <check_va2pa+0x59>
	}

	//cprintf("in check_va2pa, the pte* is: %x\n", &(p[PTX(va)]) );
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	//cprintf("check_va2pa returns: %x\n", PTE_ADDR(p[PTX(va)]));
	return PTE_ADDR(p[PTX(va)]); //return physical address of the page containing 'va'
f0100bac:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0100bb1:	c3                   	ret    
	pte_t *p;
	//cprintf("VA check_va2pa looks at is: %x\n", va);
	pgdir = &pgdir[PDX(va)];
	if (!(*pgdir & PTE_P)){ //no page directory entry there
		//cprintf("check_va2pa no pde, return fff...\n");
		return ~0;
f0100bb2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100bb7:	c3                   	ret    
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	if (!(p[PTX(va)] & PTE_P)){ //no page table entry there
		//cprintf("in check_va2pa, the pte* is: %x\n", &(p[PTX(va)]) );
		//cprintf("check_va2pa no pte, return fff...\n");
		//cprintf("shit happens\n");
		return ~0;
f0100bb8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax

	//cprintf("in check_va2pa, the pte* is: %x\n", &(p[PTX(va)]) );
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	//cprintf("check_va2pa returns: %x\n", PTE_ADDR(p[PTX(va)]));
	return PTE_ADDR(p[PTX(va)]); //return physical address of the page containing 'va'
}
f0100bbd:	c3                   	ret    

f0100bbe <check_page_free_list>:
//
// Check that the pages on the page_free_list are reasonable.
//
static void
check_page_free_list(bool only_low_memory)
{
f0100bbe:	55                   	push   %ebp
f0100bbf:	89 e5                	mov    %esp,%ebp
f0100bc1:	57                   	push   %edi
f0100bc2:	56                   	push   %esi
f0100bc3:	53                   	push   %ebx
f0100bc4:	83 ec 2c             	sub    $0x2c,%esp
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
f0100bc7:	84 c0                	test   %al,%al
f0100bc9:	0f 85 6d 02 00 00    	jne    f0100e3c <check_page_free_list+0x27e>
f0100bcf:	e9 7a 02 00 00       	jmp    f0100e4e <check_page_free_list+0x290>
	int nfree_basemem = 0, nfree_extmem = 0;
	char *first_free_page;

	if (!page_free_list)
		panic("'page_free_list' is a null pointer!");
f0100bd4:	83 ec 04             	sub    $0x4,%esp
f0100bd7:	68 54 56 10 f0       	push   $0xf0105654
f0100bdc:	68 27 03 00 00       	push   $0x327
f0100be1:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100be6:	e8 61 f4 ff ff       	call   f010004c <_panic>

	if (only_low_memory) {
		// Move pages with lower addresses first in the free
		// list, since entry_pgdir does not map all pages.
		struct PageInfo *pp1, *pp2;
		struct PageInfo **tp[2] = { &pp1, &pp2 };
f0100beb:	8d 55 d8             	lea    -0x28(%ebp),%edx
f0100bee:	89 55 e0             	mov    %edx,-0x20(%ebp)
f0100bf1:	8d 55 dc             	lea    -0x24(%ebp),%edx
f0100bf4:	89 55 e4             	mov    %edx,-0x1c(%ebp)
		for (pp = page_free_list; pp; pp = pp->pp_link) {
			int pagetype = PDX(page2pa(pp)) >= pdx_limit;
f0100bf7:	89 c2                	mov    %eax,%edx
f0100bf9:	2b 15 4c e4 18 f0    	sub    0xf018e44c,%edx
f0100bff:	f7 c2 00 e0 7f 00    	test   $0x7fe000,%edx
f0100c05:	0f 95 c2             	setne  %dl
f0100c08:	0f b6 d2             	movzbl %dl,%edx
			*tp[pagetype] = pp;
f0100c0b:	8b 4c 95 e0          	mov    -0x20(%ebp,%edx,4),%ecx
f0100c0f:	89 01                	mov    %eax,(%ecx)
			tp[pagetype] = &pp->pp_link;
f0100c11:	89 44 95 e0          	mov    %eax,-0x20(%ebp,%edx,4)
	if (only_low_memory) {
		// Move pages with lower addresses first in the free
		// list, since entry_pgdir does not map all pages.
		struct PageInfo *pp1, *pp2;
		struct PageInfo **tp[2] = { &pp1, &pp2 };
		for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100c15:	8b 00                	mov    (%eax),%eax
f0100c17:	85 c0                	test   %eax,%eax
f0100c19:	75 dc                	jne    f0100bf7 <check_page_free_list+0x39>
			int pagetype = PDX(page2pa(pp)) >= pdx_limit;
			*tp[pagetype] = pp;
			tp[pagetype] = &pp->pp_link;
		}
		*tp[1] = 0;
f0100c1b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100c1e:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		*tp[0] = pp2;
f0100c24:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100c27:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0100c2a:	89 10                	mov    %edx,(%eax)
		page_free_list = pp1;
f0100c2c:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0100c2f:	a3 5c d2 18 f0       	mov    %eax,0xf018d25c
//
static void
check_page_free_list(bool only_low_memory)
{
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
f0100c34:	be 01 00 00 00       	mov    $0x1,%esi
		page_free_list = pp1;
	}

	// if there's a page that shouldn't be on the free list,
	// try to make sure it eventually causes trouble.
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0100c39:	8b 1d 5c d2 18 f0    	mov    0xf018d25c,%ebx
f0100c3f:	eb 53                	jmp    f0100c94 <check_page_free_list+0xd6>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0100c41:	89 d8                	mov    %ebx,%eax
f0100c43:	2b 05 4c e4 18 f0    	sub    0xf018e44c,%eax
f0100c49:	c1 f8 03             	sar    $0x3,%eax
f0100c4c:	c1 e0 0c             	shl    $0xc,%eax
		if (PDX(page2pa(pp)) < pdx_limit)
f0100c4f:	89 c2                	mov    %eax,%edx
f0100c51:	c1 ea 16             	shr    $0x16,%edx
f0100c54:	39 f2                	cmp    %esi,%edx
f0100c56:	73 3a                	jae    f0100c92 <check_page_free_list+0xd4>
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100c58:	89 c2                	mov    %eax,%edx
f0100c5a:	c1 ea 0c             	shr    $0xc,%edx
f0100c5d:	3b 15 44 e4 18 f0    	cmp    0xf018e444,%edx
f0100c63:	72 12                	jb     f0100c77 <check_page_free_list+0xb9>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0100c65:	50                   	push   %eax
f0100c66:	68 30 56 10 f0       	push   $0xf0105630
f0100c6b:	6a 56                	push   $0x56
f0100c6d:	68 f5 5d 10 f0       	push   $0xf0105df5
f0100c72:	e8 d5 f3 ff ff       	call   f010004c <_panic>
			memset(page2kva(pp), 0x97, 128);
f0100c77:	83 ec 04             	sub    $0x4,%esp
f0100c7a:	68 80 00 00 00       	push   $0x80
f0100c7f:	68 97 00 00 00       	push   $0x97
f0100c84:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0100c89:	50                   	push   %eax
f0100c8a:	e8 89 3e 00 00       	call   f0104b18 <memset>
f0100c8f:	83 c4 10             	add    $0x10,%esp
		page_free_list = pp1;
	}

	// if there's a page that shouldn't be on the free list,
	// try to make sure it eventually causes trouble.
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0100c92:	8b 1b                	mov    (%ebx),%ebx
f0100c94:	85 db                	test   %ebx,%ebx
f0100c96:	75 a9                	jne    f0100c41 <check_page_free_list+0x83>
		if (PDX(page2pa(pp)) < pdx_limit)
			memset(page2kva(pp), 0x97, 128);

	first_free_page = (char *) boot_alloc(0);
f0100c98:	b8 00 00 00 00       	mov    $0x0,%eax
f0100c9d:	e8 6f fe ff ff       	call   f0100b11 <boot_alloc>
f0100ca2:	89 45 cc             	mov    %eax,-0x34(%ebp)
	for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100ca5:	8b 15 5c d2 18 f0    	mov    0xf018d25c,%edx
		// check that we didn't corrupt the free list itself
		assert(pp >= pages);
f0100cab:	8b 0d 4c e4 18 f0    	mov    0xf018e44c,%ecx
		assert(pp < pages + npages);
f0100cb1:	a1 44 e4 18 f0       	mov    0xf018e444,%eax
f0100cb6:	89 45 c8             	mov    %eax,-0x38(%ebp)
f0100cb9:	8d 3c c1             	lea    (%ecx,%eax,8),%edi
		assert(((char *) pp - (char *) pages) % sizeof(*pp) == 0);
f0100cbc:	89 4d d4             	mov    %ecx,-0x2c(%ebp)
static void
check_page_free_list(bool only_low_memory)
{
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
	int nfree_basemem = 0, nfree_extmem = 0;
f0100cbf:	be 00 00 00 00       	mov    $0x0,%esi
f0100cc4:	89 5d d0             	mov    %ebx,-0x30(%ebp)
	for (pp = page_free_list; pp; pp = pp->pp_link)
		if (PDX(page2pa(pp)) < pdx_limit)
			memset(page2kva(pp), 0x97, 128);

	first_free_page = (char *) boot_alloc(0);
	for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100cc7:	e9 2b 01 00 00       	jmp    f0100df7 <check_page_free_list+0x239>
		// check that we didn't corrupt the free list itself
		assert(pp >= pages);
f0100ccc:	39 ca                	cmp    %ecx,%edx
f0100cce:	73 19                	jae    f0100ce9 <check_page_free_list+0x12b>
f0100cd0:	68 03 5e 10 f0       	push   $0xf0105e03
f0100cd5:	68 98 4f 10 f0       	push   $0xf0104f98
f0100cda:	68 41 03 00 00       	push   $0x341
f0100cdf:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100ce4:	e8 63 f3 ff ff       	call   f010004c <_panic>
		assert(pp < pages + npages);
f0100ce9:	39 fa                	cmp    %edi,%edx
f0100ceb:	72 19                	jb     f0100d06 <check_page_free_list+0x148>
f0100ced:	68 0f 5e 10 f0       	push   $0xf0105e0f
f0100cf2:	68 98 4f 10 f0       	push   $0xf0104f98
f0100cf7:	68 42 03 00 00       	push   $0x342
f0100cfc:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100d01:	e8 46 f3 ff ff       	call   f010004c <_panic>
		assert(((char *) pp - (char *) pages) % sizeof(*pp) == 0);
f0100d06:	89 d0                	mov    %edx,%eax
f0100d08:	2b 45 d4             	sub    -0x2c(%ebp),%eax
f0100d0b:	a8 07                	test   $0x7,%al
f0100d0d:	74 19                	je     f0100d28 <check_page_free_list+0x16a>
f0100d0f:	68 78 56 10 f0       	push   $0xf0105678
f0100d14:	68 98 4f 10 f0       	push   $0xf0104f98
f0100d19:	68 43 03 00 00       	push   $0x343
f0100d1e:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100d23:	e8 24 f3 ff ff       	call   f010004c <_panic>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0100d28:	c1 f8 03             	sar    $0x3,%eax

		// check a few pages that shouldn't be on the free list
		assert(page2pa(pp) != 0);
f0100d2b:	c1 e0 0c             	shl    $0xc,%eax
f0100d2e:	75 19                	jne    f0100d49 <check_page_free_list+0x18b>
f0100d30:	68 23 5e 10 f0       	push   $0xf0105e23
f0100d35:	68 98 4f 10 f0       	push   $0xf0104f98
f0100d3a:	68 46 03 00 00       	push   $0x346
f0100d3f:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100d44:	e8 03 f3 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) != IOPHYSMEM);
f0100d49:	3d 00 00 0a 00       	cmp    $0xa0000,%eax
f0100d4e:	75 19                	jne    f0100d69 <check_page_free_list+0x1ab>
f0100d50:	68 34 5e 10 f0       	push   $0xf0105e34
f0100d55:	68 98 4f 10 f0       	push   $0xf0104f98
f0100d5a:	68 47 03 00 00       	push   $0x347
f0100d5f:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100d64:	e8 e3 f2 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) != EXTPHYSMEM - PGSIZE);
f0100d69:	3d 00 f0 0f 00       	cmp    $0xff000,%eax
f0100d6e:	75 19                	jne    f0100d89 <check_page_free_list+0x1cb>
f0100d70:	68 ac 56 10 f0       	push   $0xf01056ac
f0100d75:	68 98 4f 10 f0       	push   $0xf0104f98
f0100d7a:	68 48 03 00 00       	push   $0x348
f0100d7f:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100d84:	e8 c3 f2 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) != EXTPHYSMEM);
f0100d89:	3d 00 00 10 00       	cmp    $0x100000,%eax
f0100d8e:	75 19                	jne    f0100da9 <check_page_free_list+0x1eb>
f0100d90:	68 4d 5e 10 f0       	push   $0xf0105e4d
f0100d95:	68 98 4f 10 f0       	push   $0xf0104f98
f0100d9a:	68 49 03 00 00       	push   $0x349
f0100d9f:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100da4:	e8 a3 f2 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) < EXTPHYSMEM || (char *) page2kva(pp) >= first_free_page);
f0100da9:	3d ff ff 0f 00       	cmp    $0xfffff,%eax
f0100dae:	76 3f                	jbe    f0100def <check_page_free_list+0x231>
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100db0:	89 c3                	mov    %eax,%ebx
f0100db2:	c1 eb 0c             	shr    $0xc,%ebx
f0100db5:	39 5d c8             	cmp    %ebx,-0x38(%ebp)
f0100db8:	77 12                	ja     f0100dcc <check_page_free_list+0x20e>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0100dba:	50                   	push   %eax
f0100dbb:	68 30 56 10 f0       	push   $0xf0105630
f0100dc0:	6a 56                	push   $0x56
f0100dc2:	68 f5 5d 10 f0       	push   $0xf0105df5
f0100dc7:	e8 80 f2 ff ff       	call   f010004c <_panic>
f0100dcc:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0100dd1:	39 45 cc             	cmp    %eax,-0x34(%ebp)
f0100dd4:	76 1c                	jbe    f0100df2 <check_page_free_list+0x234>
f0100dd6:	68 d0 56 10 f0       	push   $0xf01056d0
f0100ddb:	68 98 4f 10 f0       	push   $0xf0104f98
f0100de0:	68 4a 03 00 00       	push   $0x34a
f0100de5:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100dea:	e8 5d f2 ff ff       	call   f010004c <_panic>

		if (page2pa(pp) < EXTPHYSMEM)
			++nfree_basemem;
f0100def:	46                   	inc    %esi
f0100df0:	eb 03                	jmp    f0100df5 <check_page_free_list+0x237>
		else
			++nfree_extmem;
f0100df2:	ff 45 d0             	incl   -0x30(%ebp)
	for (pp = page_free_list; pp; pp = pp->pp_link)
		if (PDX(page2pa(pp)) < pdx_limit)
			memset(page2kva(pp), 0x97, 128);

	first_free_page = (char *) boot_alloc(0);
	for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100df5:	8b 12                	mov    (%edx),%edx
f0100df7:	85 d2                	test   %edx,%edx
f0100df9:	0f 85 cd fe ff ff    	jne    f0100ccc <check_page_free_list+0x10e>
f0100dff:	8b 5d d0             	mov    -0x30(%ebp),%ebx
			++nfree_basemem;
		else
			++nfree_extmem;
	}

	assert(nfree_basemem > 0);
f0100e02:	85 f6                	test   %esi,%esi
f0100e04:	7f 19                	jg     f0100e1f <check_page_free_list+0x261>
f0100e06:	68 67 5e 10 f0       	push   $0xf0105e67
f0100e0b:	68 98 4f 10 f0       	push   $0xf0104f98
f0100e10:	68 52 03 00 00       	push   $0x352
f0100e15:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100e1a:	e8 2d f2 ff ff       	call   f010004c <_panic>
	assert(nfree_extmem > 0);
f0100e1f:	85 db                	test   %ebx,%ebx
f0100e21:	7f 42                	jg     f0100e65 <check_page_free_list+0x2a7>
f0100e23:	68 79 5e 10 f0       	push   $0xf0105e79
f0100e28:	68 98 4f 10 f0       	push   $0xf0104f98
f0100e2d:	68 53 03 00 00       	push   $0x353
f0100e32:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100e37:	e8 10 f2 ff ff       	call   f010004c <_panic>
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
	int nfree_basemem = 0, nfree_extmem = 0;
	char *first_free_page;

	if (!page_free_list)
f0100e3c:	a1 5c d2 18 f0       	mov    0xf018d25c,%eax
f0100e41:	85 c0                	test   %eax,%eax
f0100e43:	0f 85 a2 fd ff ff    	jne    f0100beb <check_page_free_list+0x2d>
f0100e49:	e9 86 fd ff ff       	jmp    f0100bd4 <check_page_free_list+0x16>
f0100e4e:	83 3d 5c d2 18 f0 00 	cmpl   $0x0,0xf018d25c
f0100e55:	0f 84 79 fd ff ff    	je     f0100bd4 <check_page_free_list+0x16>
//
static void
check_page_free_list(bool only_low_memory)
{
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
f0100e5b:	be 00 04 00 00       	mov    $0x400,%esi
f0100e60:	e9 d4 fd ff ff       	jmp    f0100c39 <check_page_free_list+0x7b>
			++nfree_extmem;
	}

	assert(nfree_basemem > 0);
	assert(nfree_extmem > 0);
}
f0100e65:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100e68:	5b                   	pop    %ebx
f0100e69:	5e                   	pop    %esi
f0100e6a:	5f                   	pop    %edi
f0100e6b:	5d                   	pop    %ebp
f0100e6c:	c3                   	ret    

f0100e6d <page_init>:
// allocator functions below to allocate and deallocate physical
// memory via the page_free_list.
//
void
page_init(void)
{
f0100e6d:	55                   	push   %ebp
f0100e6e:	89 e5                	mov    %esp,%ebp
f0100e70:	56                   	push   %esi
f0100e71:	53                   	push   %ebx

	//page_free_list -> Pn -> Pn-1 -> ... -> P2 -> P1
	//physical page 0 is in use.
	//[mem 0x00001000-0x0009fbff] available
	int i;
	cprintf("npages: %d\n", npages);
f0100e72:	83 ec 08             	sub    $0x8,%esp
f0100e75:	ff 35 44 e4 18 f0    	pushl  0xf018e444
f0100e7b:	68 8a 5e 10 f0       	push   $0xf0105e8a
f0100e80:	e8 44 24 00 00       	call   f01032c9 <cprintf>
	assert(PGNUM(0x0009fbff) <= npages);
f0100e85:	83 c4 10             	add    $0x10,%esp
f0100e88:	81 3d 44 e4 18 f0 9e 	cmpl   $0x9e,0xf018e444
f0100e8f:	00 00 00 
f0100e92:	76 0f                	jbe    f0100ea3 <page_init+0x36>
f0100e94:	8b 0d 5c d2 18 f0    	mov    0xf018d25c,%ecx
f0100e9a:	b8 00 00 00 00       	mov    $0x0,%eax
f0100e9f:	b3 00                	mov    $0x0,%bl
f0100ea1:	eb 2d                	jmp    f0100ed0 <page_init+0x63>
f0100ea3:	68 96 5e 10 f0       	push   $0xf0105e96
f0100ea8:	68 98 4f 10 f0       	push   $0xf0104f98
f0100ead:	68 4a 01 00 00       	push   $0x14a
f0100eb2:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100eb7:	e8 90 f1 ff ff       	call   f010004c <_panic>
	for(i=1; i<(pa2page(0x0009fbff) - pages); i++){
		pages[i].pp_ref = 0;
f0100ebc:	01 c2                	add    %eax,%edx
f0100ebe:	66 c7 42 04 00 00    	movw   $0x0,0x4(%edx)
		pages[i].pp_link = page_free_list;
f0100ec4:	89 0a                	mov    %ecx,(%edx)
		page_free_list = &pages[i];
f0100ec6:	89 c1                	mov    %eax,%ecx
f0100ec8:	03 0d 4c e4 18 f0    	add    0xf018e44c,%ecx
f0100ece:	b3 01                	mov    $0x1,%bl
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100ed0:	81 3d 44 e4 18 f0 9f 	cmpl   $0x9f,0xf018e444
f0100ed7:	00 00 00 
f0100eda:	77 1e                	ja     f0100efa <page_init+0x8d>
f0100edc:	84 db                	test   %bl,%bl
f0100ede:	74 06                	je     f0100ee6 <page_init+0x79>
f0100ee0:	89 0d 5c d2 18 f0    	mov    %ecx,0xf018d25c
		panic("pa2page called with invalid pa");
f0100ee6:	83 ec 04             	sub    $0x4,%esp
f0100ee9:	68 18 57 10 f0       	push   $0xf0105718
f0100eee:	6a 4f                	push   $0x4f
f0100ef0:	68 f5 5d 10 f0       	push   $0xf0105df5
f0100ef5:	e8 52 f1 ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f0100efa:	8b 15 4c e4 18 f0    	mov    0xf018e44c,%edx
f0100f00:	83 c0 08             	add    $0x8,%eax
	//physical page 0 is in use.
	//[mem 0x00001000-0x0009fbff] available
	int i;
	cprintf("npages: %d\n", npages);
	assert(PGNUM(0x0009fbff) <= npages);
	for(i=1; i<(pa2page(0x0009fbff) - pages); i++){
f0100f03:	3d f8 04 00 00       	cmp    $0x4f8,%eax
f0100f08:	75 b2                	jne    f0100ebc <page_init+0x4f>
f0100f0a:	84 db                	test   %bl,%bl
f0100f0c:	74 06                	je     f0100f14 <page_init+0xa7>
f0100f0e:	89 0d 5c d2 18 f0    	mov    %ecx,0xf018d25c
	//     0x00100000-0x????????  in use for kernel
    //[mem 0x00100000-0x07fdffff] available
	//physaddr_t start_PA = PADDR(boot_alloc(0)); //physical address of the end of the kernel plus previously allocated stuff
	//physaddr_t end_PA = PADDR(0x07fdffff);

	physaddr_t start_PA = (physaddr_t)boot_alloc(0) - KERNBASE;
f0100f14:	b8 00 00 00 00       	mov    $0x0,%eax
f0100f19:	e8 f3 fb ff ff       	call   f0100b11 <boot_alloc>
	physaddr_t end_PA = (physaddr_t)0x07fdffff;
	assert(PGNUM(start_PA) <= npages);
f0100f1e:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f0100f24:	c1 ea 0c             	shr    $0xc,%edx
f0100f27:	a1 44 e4 18 f0       	mov    0xf018e444,%eax
f0100f2c:	39 c2                	cmp    %eax,%edx
f0100f2e:	76 19                	jbe    f0100f49 <page_init+0xdc>
f0100f30:	68 b2 5e 10 f0       	push   $0xf0105eb2
f0100f35:	68 98 4f 10 f0       	push   $0xf0104f98
f0100f3a:	68 58 01 00 00       	push   $0x158
f0100f3f:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100f44:	e8 03 f1 ff ff       	call   f010004c <_panic>
	assert(PGNUM(end_PA) <= npages);
f0100f49:	3d de 7f 00 00       	cmp    $0x7fde,%eax
f0100f4e:	77 19                	ja     f0100f69 <page_init+0xfc>
f0100f50:	68 cc 5e 10 f0       	push   $0xf0105ecc
f0100f55:	68 98 4f 10 f0       	push   $0xf0104f98
f0100f5a:	68 59 01 00 00       	push   $0x159
f0100f5f:	68 e9 5d 10 f0       	push   $0xf0105de9
f0100f64:	e8 e3 f0 ff ff       	call   f010004c <_panic>
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100f69:	39 c2                	cmp    %eax,%edx
f0100f6b:	72 14                	jb     f0100f81 <page_init+0x114>
		panic("pa2page called with invalid pa");
f0100f6d:	83 ec 04             	sub    $0x4,%esp
f0100f70:	68 18 57 10 f0       	push   $0xf0105718
f0100f75:	6a 4f                	push   $0x4f
f0100f77:	68 f5 5d 10 f0       	push   $0xf0105df5
f0100f7c:	e8 cb f0 ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f0100f81:	c1 e2 03             	shl    $0x3,%edx
	for(i= (pa2page(start_PA) - pages); i<(pa2page(end_PA) - pages); i++){
f0100f84:	89 d1                	mov    %edx,%ecx
f0100f86:	c1 f9 03             	sar    $0x3,%ecx
f0100f89:	8b 1d 5c d2 18 f0    	mov    0xf018d25c,%ebx
f0100f8f:	be 00 00 00 00       	mov    $0x0,%esi
f0100f94:	eb 1b                	jmp    f0100fb1 <page_init+0x144>
		pages[i].pp_ref = 0;
f0100f96:	01 d0                	add    %edx,%eax
f0100f98:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)
		pages[i].pp_link = page_free_list;
f0100f9e:	89 18                	mov    %ebx,(%eax)
		page_free_list = &pages[i];
f0100fa0:	89 d3                	mov    %edx,%ebx
f0100fa2:	03 1d 4c e4 18 f0    	add    0xf018e44c,%ebx

	physaddr_t start_PA = (physaddr_t)boot_alloc(0) - KERNBASE;
	physaddr_t end_PA = (physaddr_t)0x07fdffff;
	assert(PGNUM(start_PA) <= npages);
	assert(PGNUM(end_PA) <= npages);
	for(i= (pa2page(start_PA) - pages); i<(pa2page(end_PA) - pages); i++){
f0100fa8:	41                   	inc    %ecx
f0100fa9:	83 c2 08             	add    $0x8,%edx
f0100fac:	be 01 00 00 00       	mov    $0x1,%esi
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100fb1:	81 3d 44 e4 18 f0 df 	cmpl   $0x7fdf,0xf018e444
f0100fb8:	7f 00 00 
f0100fbb:	77 20                	ja     f0100fdd <page_init+0x170>
f0100fbd:	89 f0                	mov    %esi,%eax
f0100fbf:	84 c0                	test   %al,%al
f0100fc1:	74 06                	je     f0100fc9 <page_init+0x15c>
f0100fc3:	89 1d 5c d2 18 f0    	mov    %ebx,0xf018d25c
		panic("pa2page called with invalid pa");
f0100fc9:	83 ec 04             	sub    $0x4,%esp
f0100fcc:	68 18 57 10 f0       	push   $0xf0105718
f0100fd1:	6a 4f                	push   $0x4f
f0100fd3:	68 f5 5d 10 f0       	push   $0xf0105df5
f0100fd8:	e8 6f f0 ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f0100fdd:	a1 4c e4 18 f0       	mov    0xf018e44c,%eax
f0100fe2:	81 f9 de 7f 00 00    	cmp    $0x7fde,%ecx
f0100fe8:	7e ac                	jle    f0100f96 <page_init+0x129>
f0100fea:	89 f0                	mov    %esi,%eax
f0100fec:	84 c0                	test   %al,%al
f0100fee:	74 06                	je     f0100ff6 <page_init+0x189>
f0100ff0:	89 1d 5c d2 18 f0    	mov    %ebx,0xf018d25c
		page_free_list = &pages[i];
	}
	//page_free_list->pp_ref = 1;
	//page_free_list->pp_ref = 0;
	//cprintf("Got here1\n");
}
f0100ff6:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0100ff9:	5b                   	pop    %ebx
f0100ffa:	5e                   	pop    %esi
f0100ffb:	5d                   	pop    %ebp
f0100ffc:	c3                   	ret    

f0100ffd <page_alloc>:
// Returns NULL if out of free memory.
//
// Hint: use page2kva and memset
struct PageInfo *
page_alloc(int alloc_flags)
{
f0100ffd:	55                   	push   %ebp
f0100ffe:	89 e5                	mov    %esp,%ebp
f0101000:	53                   	push   %ebx
f0101001:	83 ec 04             	sub    $0x4,%esp
	
	// Fill this function in
	if(page_free_list == NULL){ //check if out of memory
f0101004:	8b 1d 5c d2 18 f0    	mov    0xf018d25c,%ebx
f010100a:	85 db                	test   %ebx,%ebx
f010100c:	74 5e                	je     f010106c <page_alloc+0x6f>
	}
	//page_free_list->pp_ref = 1;
	//page_free_list->pp_ref = 0;
	//page_free_list->pp_link = NULL;
	struct PageInfo *page = page_free_list;
	page_free_list = page_free_list->pp_link;
f010100e:	8b 03                	mov    (%ebx),%eax
f0101010:	a3 5c d2 18 f0       	mov    %eax,0xf018d25c
	page->pp_link = NULL;
f0101015:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
	page->pp_ref = 0;
f010101b:	66 c7 43 04 00 00    	movw   $0x0,0x4(%ebx)
	if(alloc_flags & ALLOC_ZERO){  //fills the entire returned physical page with '\0' bytes
f0101021:	f6 45 08 01          	testb  $0x1,0x8(%ebp)
f0101025:	74 45                	je     f010106c <page_alloc+0x6f>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101027:	89 d8                	mov    %ebx,%eax
f0101029:	2b 05 4c e4 18 f0    	sub    0xf018e44c,%eax
f010102f:	c1 f8 03             	sar    $0x3,%eax
f0101032:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101035:	89 c2                	mov    %eax,%edx
f0101037:	c1 ea 0c             	shr    $0xc,%edx
f010103a:	3b 15 44 e4 18 f0    	cmp    0xf018e444,%edx
f0101040:	72 12                	jb     f0101054 <page_alloc+0x57>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101042:	50                   	push   %eax
f0101043:	68 30 56 10 f0       	push   $0xf0105630
f0101048:	6a 56                	push   $0x56
f010104a:	68 f5 5d 10 f0       	push   $0xf0105df5
f010104f:	e8 f8 ef ff ff       	call   f010004c <_panic>
		memset(page2kva(page), '\0', PGSIZE);
f0101054:	83 ec 04             	sub    $0x4,%esp
f0101057:	68 00 10 00 00       	push   $0x1000
f010105c:	6a 00                	push   $0x0
f010105e:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0101063:	50                   	push   %eax
f0101064:	e8 af 3a 00 00       	call   f0104b18 <memset>
f0101069:	83 c4 10             	add    $0x10,%esp
		char *kva = page2kva(result);
		memset(kva, '\0', PGSIZE);
	}
	
	return result;*/
}
f010106c:	89 d8                	mov    %ebx,%eax
f010106e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0101071:	c9                   	leave  
f0101072:	c3                   	ret    

f0101073 <page_free>:
// Return a page to the free list.
// (This function should only be called when pp->pp_ref reaches 0.)
//
void
page_free(struct PageInfo *pp)
{
f0101073:	55                   	push   %ebp
f0101074:	89 e5                	mov    %esp,%ebp
f0101076:	83 ec 08             	sub    $0x8,%esp
f0101079:	8b 45 08             	mov    0x8(%ebp),%eax
	// Fill this function in
	// Hint: You may want to panic if pp->pp_ref is nonzero or
	// pp->pp_link is not NULL.
	if(pp->pp_ref != 0){
f010107c:	66 83 78 04 00       	cmpw   $0x0,0x4(%eax)
f0101081:	74 17                	je     f010109a <page_free+0x27>
		panic("pp->pp_ref is nonzero");
f0101083:	83 ec 04             	sub    $0x4,%esp
f0101086:	68 e4 5e 10 f0       	push   $0xf0105ee4
f010108b:	68 a5 01 00 00       	push   $0x1a5
f0101090:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101095:	e8 b2 ef ff ff       	call   f010004c <_panic>
	}

	if(pp->pp_link != NULL){
f010109a:	83 38 00             	cmpl   $0x0,(%eax)
f010109d:	74 17                	je     f01010b6 <page_free+0x43>
		panic("pp->pp_link is not NULL");
f010109f:	83 ec 04             	sub    $0x4,%esp
f01010a2:	68 fa 5e 10 f0       	push   $0xf0105efa
f01010a7:	68 a9 01 00 00       	push   $0x1a9
f01010ac:	68 e9 5d 10 f0       	push   $0xf0105de9
f01010b1:	e8 96 ef ff ff       	call   f010004c <_panic>
	}

	if(page_free_list == NULL){
f01010b6:	8b 15 5c d2 18 f0    	mov    0xf018d25c,%edx
f01010bc:	85 d2                	test   %edx,%edx
f01010be:	75 07                	jne    f01010c7 <page_free+0x54>
		page_free_list = pp;
f01010c0:	a3 5c d2 18 f0       	mov    %eax,0xf018d25c
f01010c5:	eb 07                	jmp    f01010ce <page_free+0x5b>
	}else{
		pp->pp_link = page_free_list;
f01010c7:	89 10                	mov    %edx,(%eax)
		page_free_list = pp;
f01010c9:	a3 5c d2 18 f0       	mov    %eax,0xf018d25c
	}
	//cprintf("Free-ed %x\n", pp);
}
f01010ce:	c9                   	leave  
f01010cf:	c3                   	ret    

f01010d0 <page_decref>:
// Decrement the reference count on a page,
// freeing it if there are no more refs.
//
void
page_decref(struct PageInfo* pp)
{
f01010d0:	55                   	push   %ebp
f01010d1:	89 e5                	mov    %esp,%ebp
f01010d3:	83 ec 08             	sub    $0x8,%esp
f01010d6:	8b 55 08             	mov    0x8(%ebp),%edx
	if (--pp->pp_ref == 0)
f01010d9:	8b 42 04             	mov    0x4(%edx),%eax
f01010dc:	48                   	dec    %eax
f01010dd:	66 89 42 04          	mov    %ax,0x4(%edx)
f01010e1:	66 85 c0             	test   %ax,%ax
f01010e4:	75 0c                	jne    f01010f2 <page_decref+0x22>
		page_free(pp);
f01010e6:	83 ec 0c             	sub    $0xc,%esp
f01010e9:	52                   	push   %edx
f01010ea:	e8 84 ff ff ff       	call   f0101073 <page_free>
f01010ef:	83 c4 10             	add    $0x10,%esp
}
f01010f2:	c9                   	leave  
f01010f3:	c3                   	ret    

f01010f4 <pgdir_walk>:
// Hint 3: look at inc/mmu.h for useful macros that mainipulate page
// table and page directory entries.
//
pte_t *
pgdir_walk(pde_t *pgdir, const void *va, int create)
{
f01010f4:	55                   	push   %ebp
f01010f5:	89 e5                	mov    %esp,%ebp
f01010f7:	53                   	push   %ebx
f01010f8:	83 ec 04             	sub    $0x4,%esp
f01010fb:	8b 45 08             	mov    0x8(%ebp),%eax
	//pte_t* pte_Ptr = (pte_t*)(KADDR(pde) + PTX(va));
	return pte_Ptr;
	*/

	//reference code:
	assert(pgdir);
f01010fe:	85 c0                	test   %eax,%eax
f0101100:	75 19                	jne    f010111b <pgdir_walk+0x27>
f0101102:	68 12 5f 10 f0       	push   $0xf0105f12
f0101107:	68 98 4f 10 f0       	push   $0xf0104f98
f010110c:	68 fe 01 00 00       	push   $0x1fe
f0101111:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101116:	e8 31 ef ff ff       	call   f010004c <_panic>

	// Page directory index.
	size_t pdx = PDX(va);

	// Page directory entry.
	pde_t pde = pgdir[pdx];
f010111b:	8b 55 0c             	mov    0xc(%ebp),%edx
f010111e:	c1 ea 16             	shr    $0x16,%edx
f0101121:	8d 1c 90             	lea    (%eax,%edx,4),%ebx
f0101124:	8b 03                	mov    (%ebx),%eax

	// Page table present?
	// pt is a (kernel) virtual address.
	if (pde & PTE_P) {
f0101126:	a8 01                	test   $0x1,%al
f0101128:	74 2f                	je     f0101159 <pgdir_walk+0x65>
		// Get page table's kva from physical address.
		pt = KADDR(PTE_ADDR(pde));
f010112a:	25 00 f0 ff ff       	and    $0xfffff000,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010112f:	89 c2                	mov    %eax,%edx
f0101131:	c1 ea 0c             	shr    $0xc,%edx
f0101134:	39 15 44 e4 18 f0    	cmp    %edx,0xf018e444
f010113a:	77 15                	ja     f0101151 <pgdir_walk+0x5d>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010113c:	50                   	push   %eax
f010113d:	68 30 56 10 f0       	push   $0xf0105630
f0101142:	68 10 02 00 00       	push   $0x210
f0101147:	68 e9 5d 10 f0       	push   $0xf0105de9
f010114c:	e8 fb ee ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f0101151:	8d 90 00 00 00 f0    	lea    -0x10000000(%eax),%edx
f0101157:	eb 5f                	jmp    f01011b8 <pgdir_walk+0xc4>
	} else {
		if (!create)
f0101159:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f010115d:	74 68                	je     f01011c7 <pgdir_walk+0xd3>
			return NULL;
		
		// Create a new page for the page table.
		struct PageInfo * page = page_alloc(ALLOC_ZERO);
f010115f:	83 ec 0c             	sub    $0xc,%esp
f0101162:	6a 01                	push   $0x1
f0101164:	e8 94 fe ff ff       	call   f0100ffd <page_alloc>
		if (!page)
f0101169:	83 c4 10             	add    $0x10,%esp
f010116c:	85 c0                	test   %eax,%eax
f010116e:	74 5e                	je     f01011ce <pgdir_walk+0xda>
			return NULL;

		// The new page has these permissions for convenience.
		pgdir[pdx] = page2pa(page) | PTE_U | PTE_W | PTE_P;
f0101170:	89 c2                	mov    %eax,%edx
f0101172:	2b 15 4c e4 18 f0    	sub    0xf018e44c,%edx
f0101178:	c1 fa 03             	sar    $0x3,%edx
f010117b:	c1 e2 0c             	shl    $0xc,%edx
f010117e:	83 ca 07             	or     $0x7,%edx
f0101181:	89 13                	mov    %edx,(%ebx)

		// It appeared in page directory, so increment refcount.
		page->pp_ref++;
f0101183:	66 ff 40 04          	incw   0x4(%eax)
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101187:	2b 05 4c e4 18 f0    	sub    0xf018e44c,%eax
f010118d:	c1 f8 03             	sar    $0x3,%eax
f0101190:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101193:	89 c2                	mov    %eax,%edx
f0101195:	c1 ea 0c             	shr    $0xc,%edx
f0101198:	3b 15 44 e4 18 f0    	cmp    0xf018e444,%edx
f010119e:	72 12                	jb     f01011b2 <pgdir_walk+0xbe>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01011a0:	50                   	push   %eax
f01011a1:	68 30 56 10 f0       	push   $0xf0105630
f01011a6:	6a 56                	push   $0x56
f01011a8:	68 f5 5d 10 f0       	push   $0xf0105df5
f01011ad:	e8 9a ee ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f01011b2:	8d 90 00 00 00 f0    	lea    -0x10000000(%eax),%edx

	// Page table index.
	size_t ptx = PTX(va);

	// Return page table entry pointer.
	return &pt[ptx];
f01011b8:	8b 45 0c             	mov    0xc(%ebp),%eax
f01011bb:	c1 e8 0a             	shr    $0xa,%eax
f01011be:	25 fc 0f 00 00       	and    $0xffc,%eax
f01011c3:	01 d0                	add    %edx,%eax
f01011c5:	eb 0c                	jmp    f01011d3 <pgdir_walk+0xdf>
	if (pde & PTE_P) {
		// Get page table's kva from physical address.
		pt = KADDR(PTE_ADDR(pde));
	} else {
		if (!create)
			return NULL;
f01011c7:	b8 00 00 00 00       	mov    $0x0,%eax
f01011cc:	eb 05                	jmp    f01011d3 <pgdir_walk+0xdf>
		
		// Create a new page for the page table.
		struct PageInfo * page = page_alloc(ALLOC_ZERO);
		if (!page)
			return NULL;
f01011ce:	b8 00 00 00 00       	mov    $0x0,%eax
	// Page table index.
	size_t ptx = PTX(va);

	// Return page table entry pointer.
	return &pt[ptx];
}
f01011d3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01011d6:	c9                   	leave  
f01011d7:	c3                   	ret    

f01011d8 <boot_map_region>:
// mapped pages.
//
// Hint: the TA solution uses pgdir_walk
static void
boot_map_region(pde_t *pgdir, uintptr_t va, size_t size, physaddr_t pa, int perm)
{
f01011d8:	55                   	push   %ebp
f01011d9:	89 e5                	mov    %esp,%ebp
f01011db:	57                   	push   %edi
f01011dc:	56                   	push   %esi
f01011dd:	53                   	push   %ebx
f01011de:	83 ec 1c             	sub    $0x1c,%esp
f01011e1:	89 c3                	mov    %eax,%ebx
f01011e3:	89 45 e0             	mov    %eax,-0x20(%ebp)
f01011e6:	89 cf                	mov    %ecx,%edi
f01011e8:	8b 45 08             	mov    0x8(%ebp),%eax
		//cprintf("pte boot_map_region get is: %x\n", (*pte) );
		//page_insert(pgdir, pa2page(pa+i*PGSIZE), (void*)(va+i*PGSIZE), perm);
//	}
	
	//reference code:
	assert(pgdir);
f01011eb:	85 db                	test   %ebx,%ebx
f01011ed:	75 19                	jne    f0101208 <boot_map_region+0x30>
f01011ef:	68 12 5f 10 f0       	push   $0xf0105f12
f01011f4:	68 98 4f 10 f0       	push   $0xf0104f98
f01011f9:	68 4f 02 00 00       	push   $0x24f
f01011fe:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101203:	e8 44 ee ff ff       	call   f010004c <_panic>
	assert(ROUNDDOWN(va, PGSIZE) == va);
f0101208:	f7 c2 ff 0f 00 00    	test   $0xfff,%edx
f010120e:	74 19                	je     f0101229 <boot_map_region+0x51>
f0101210:	68 18 5f 10 f0       	push   $0xf0105f18
f0101215:	68 98 4f 10 f0       	push   $0xf0104f98
f010121a:	68 50 02 00 00       	push   $0x250
f010121f:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101224:	e8 23 ee ff ff       	call   f010004c <_panic>
	assert(ROUNDDOWN(size, PGSIZE) == size);
f0101229:	f7 c7 ff 0f 00 00    	test   $0xfff,%edi
f010122f:	74 19                	je     f010124a <boot_map_region+0x72>
f0101231:	68 38 57 10 f0       	push   $0xf0105738
f0101236:	68 98 4f 10 f0       	push   $0xf0104f98
f010123b:	68 51 02 00 00       	push   $0x251
f0101240:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101245:	e8 02 ee ff ff       	call   f010004c <_panic>
	assert(ROUNDDOWN(pa, PGSIZE) == pa);
f010124a:	a9 ff 0f 00 00       	test   $0xfff,%eax
f010124f:	74 3d                	je     f010128e <boot_map_region+0xb6>
f0101251:	68 34 5f 10 f0       	push   $0xf0105f34
f0101256:	68 98 4f 10 f0       	push   $0xf0104f98
f010125b:	68 52 02 00 00       	push   $0x252
f0101260:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101265:	e8 e2 ed ff ff       	call   f010004c <_panic>

	while (size >= PGSIZE) {
		// Find PTE of va.
		pte_t *pte = pgdir_walk(pgdir, (void *)va, 1);
f010126a:	83 ec 04             	sub    $0x4,%esp
f010126d:	6a 01                	push   $0x1
f010126f:	53                   	push   %ebx
f0101270:	ff 75 e0             	pushl  -0x20(%ebp)
f0101273:	e8 7c fe ff ff       	call   f01010f4 <pgdir_walk>

		// Make it present and map to pa.
		*pte = pa | perm | PTE_P;
f0101278:	0b 75 dc             	or     -0x24(%ebp),%esi
f010127b:	89 30                	mov    %esi,(%eax)

		// Next page.
		va += PGSIZE;
f010127d:	81 c3 00 10 00 00    	add    $0x1000,%ebx
		pa += PGSIZE;
		size -= PGSIZE;
f0101283:	81 ef 00 10 00 00    	sub    $0x1000,%edi
f0101289:	83 c4 10             	add    $0x10,%esp
f010128c:	eb 10                	jmp    f010129e <boot_map_region+0xc6>
f010128e:	89 d3                	mov    %edx,%ebx
f0101290:	29 d0                	sub    %edx,%eax
f0101292:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	while (size >= PGSIZE) {
		// Find PTE of va.
		pte_t *pte = pgdir_walk(pgdir, (void *)va, 1);

		// Make it present and map to pa.
		*pte = pa | perm | PTE_P;
f0101295:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101298:	83 c8 01             	or     $0x1,%eax
f010129b:	89 45 dc             	mov    %eax,-0x24(%ebp)
f010129e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01012a1:	8d 34 18             	lea    (%eax,%ebx,1),%esi
	assert(pgdir);
	assert(ROUNDDOWN(va, PGSIZE) == va);
	assert(ROUNDDOWN(size, PGSIZE) == size);
	assert(ROUNDDOWN(pa, PGSIZE) == pa);

	while (size >= PGSIZE) {
f01012a4:	81 ff ff 0f 00 00    	cmp    $0xfff,%edi
f01012aa:	77 be                	ja     f010126a <boot_map_region+0x92>
		// Next page.
		va += PGSIZE;
		pa += PGSIZE;
		size -= PGSIZE;
	}
}
f01012ac:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01012af:	5b                   	pop    %ebx
f01012b0:	5e                   	pop    %esi
f01012b1:	5f                   	pop    %edi
f01012b2:	5d                   	pop    %ebp
f01012b3:	c3                   	ret    

f01012b4 <page_lookup>:
//
// Hint: the TA solution uses pgdir_walk and pa2page.
//
struct PageInfo *
page_lookup(pde_t *pgdir, void *va, pte_t **pte_store)
{
f01012b4:	55                   	push   %ebp
f01012b5:	89 e5                	mov    %esp,%ebp
f01012b7:	53                   	push   %ebx
f01012b8:	83 ec 08             	sub    $0x8,%esp
f01012bb:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
f01012be:	6a 00                	push   $0x0
f01012c0:	ff 75 0c             	pushl  0xc(%ebp)
f01012c3:	ff 75 08             	pushl  0x8(%ebp)
f01012c6:	e8 29 fe ff ff       	call   f01010f4 <pgdir_walk>
	//cprintf("H3\n");
	while(pte_Ptr == NULL){
f01012cb:	83 c4 10             	add    $0x10,%esp
f01012ce:	85 c0                	test   %eax,%eax
f01012d0:	74 5a                	je     f010132c <page_lookup+0x78>
		return NULL; //no page mapped at va (not even a table)
	}

	if(pte_store != 0){
f01012d2:	85 db                	test   %ebx,%ebx
f01012d4:	74 02                	je     f01012d8 <page_lookup+0x24>
		*pte_store = pte_Ptr;
f01012d6:	89 03                	mov    %eax,(%ebx)
	}

	pte_t pte = *pte_Ptr;
f01012d8:	8b 00                	mov    (%eax),%eax
	if(pte & PTE_P){ //if present
f01012da:	a8 01                	test   $0x1,%al
f01012dc:	74 55                	je     f0101333 <page_lookup+0x7f>
		assert(PGNUM( PTE_ADDR(pte) ) < npages);
f01012de:	8b 15 44 e4 18 f0    	mov    0xf018e444,%edx
f01012e4:	89 c1                	mov    %eax,%ecx
f01012e6:	c1 e9 0c             	shr    $0xc,%ecx
f01012e9:	39 d1                	cmp    %edx,%ecx
f01012eb:	72 19                	jb     f0101306 <page_lookup+0x52>
f01012ed:	68 58 57 10 f0       	push   $0xf0105758
f01012f2:	68 98 4f 10 f0       	push   $0xf0104f98
f01012f7:	68 aa 02 00 00       	push   $0x2aa
f01012fc:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101301:	e8 46 ed ff ff       	call   f010004c <_panic>
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101306:	c1 e8 0c             	shr    $0xc,%eax
f0101309:	39 c2                	cmp    %eax,%edx
f010130b:	77 14                	ja     f0101321 <page_lookup+0x6d>
		panic("pa2page called with invalid pa");
f010130d:	83 ec 04             	sub    $0x4,%esp
f0101310:	68 18 57 10 f0       	push   $0xf0105718
f0101315:	6a 4f                	push   $0x4f
f0101317:	68 f5 5d 10 f0       	push   $0xf0105df5
f010131c:	e8 2b ed ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f0101321:	8b 15 4c e4 18 f0    	mov    0xf018e44c,%edx
f0101327:	8d 04 c2             	lea    (%edx,%eax,8),%eax
		return (struct PageInfo*)pa2page(PTE_ADDR(pte));
f010132a:	eb 0c                	jmp    f0101338 <page_lookup+0x84>
{
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
	//cprintf("H3\n");
	while(pte_Ptr == NULL){
		return NULL; //no page mapped at va (not even a table)
f010132c:	b8 00 00 00 00       	mov    $0x0,%eax
f0101331:	eb 05                	jmp    f0101338 <page_lookup+0x84>
	pte_t pte = *pte_Ptr;
	if(pte & PTE_P){ //if present
		assert(PGNUM( PTE_ADDR(pte) ) < npages);
		return (struct PageInfo*)pa2page(PTE_ADDR(pte));
	}else{
		return NULL;
f0101333:	b8 00 00 00 00       	mov    $0x0,%eax
	}
}
f0101338:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f010133b:	c9                   	leave  
f010133c:	c3                   	ret    

f010133d <page_remove>:
// Hint: The TA solution is implemented using page_lookup,
// 	tlb_invalidate, and page_decref.
//
void
page_remove(pde_t *pgdir, void *va)
{
f010133d:	55                   	push   %ebp
f010133e:	89 e5                	mov    %esp,%ebp
f0101340:	56                   	push   %esi
f0101341:	53                   	push   %ebx
f0101342:	8b 75 08             	mov    0x8(%ebp),%esi
f0101345:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	// Fill this function in
	//pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
	if( page_lookup(pgdir, va, 0) == NULL){
f0101348:	83 ec 04             	sub    $0x4,%esp
f010134b:	6a 00                	push   $0x0
f010134d:	53                   	push   %ebx
f010134e:	56                   	push   %esi
f010134f:	e8 60 ff ff ff       	call   f01012b4 <page_lookup>
f0101354:	83 c4 10             	add    $0x10,%esp
f0101357:	85 c0                	test   %eax,%eax
f0101359:	74 74                	je     f01013cf <page_remove+0x92>
}

static inline void
invlpg(void *addr)
{
	asm volatile("invlpg (%0)" : : "r" (addr) : "memory");
f010135b:	0f 01 3b             	invlpg (%ebx)
		return;
	}
	tlb_invalidate(pgdir, va);
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
f010135e:	83 ec 04             	sub    $0x4,%esp
f0101361:	6a 00                	push   $0x0
f0101363:	53                   	push   %ebx
f0101364:	56                   	push   %esi
f0101365:	e8 8a fd ff ff       	call   f01010f4 <pgdir_walk>
	pte_t pte = *pte_Ptr;
f010136a:	8b 10                	mov    (%eax),%edx
	*pte_Ptr = 0;
f010136c:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	assert(PGNUM(PTE_ADDR(pte)) < npages);
f0101372:	8b 0d 44 e4 18 f0    	mov    0xf018e444,%ecx
f0101378:	89 d0                	mov    %edx,%eax
f010137a:	c1 e8 0c             	shr    $0xc,%eax
f010137d:	83 c4 10             	add    $0x10,%esp
f0101380:	39 c8                	cmp    %ecx,%eax
f0101382:	72 19                	jb     f010139d <page_remove+0x60>
f0101384:	68 50 5f 10 f0       	push   $0xf0105f50
f0101389:	68 98 4f 10 f0       	push   $0xf0104f98
f010138e:	68 cc 02 00 00       	push   $0x2cc
f0101393:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101398:	e8 af ec ff ff       	call   f010004c <_panic>
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010139d:	89 d0                	mov    %edx,%eax
f010139f:	c1 e8 0c             	shr    $0xc,%eax
f01013a2:	39 c8                	cmp    %ecx,%eax
f01013a4:	72 14                	jb     f01013ba <page_remove+0x7d>
		panic("pa2page called with invalid pa");
f01013a6:	83 ec 04             	sub    $0x4,%esp
f01013a9:	68 18 57 10 f0       	push   $0xf0105718
f01013ae:	6a 4f                	push   $0x4f
f01013b0:	68 f5 5d 10 f0       	push   $0xf0105df5
f01013b5:	e8 92 ec ff ff       	call   f010004c <_panic>
	struct PageInfo* pp = pa2page( PTE_ADDR(pte));
	page_decref(pp);
f01013ba:	83 ec 0c             	sub    $0xc,%esp
f01013bd:	8b 15 4c e4 18 f0    	mov    0xf018e44c,%edx
f01013c3:	8d 04 c2             	lea    (%edx,%eax,8),%eax
f01013c6:	50                   	push   %eax
f01013c7:	e8 04 fd ff ff       	call   f01010d0 <page_decref>
f01013cc:	83 c4 10             	add    $0x10,%esp

}
f01013cf:	8d 65 f8             	lea    -0x8(%ebp),%esp
f01013d2:	5b                   	pop    %ebx
f01013d3:	5e                   	pop    %esi
f01013d4:	5d                   	pop    %ebp
f01013d5:	c3                   	ret    

f01013d6 <page_insert>:
// Hint: The TA solution is implemented using pgdir_walk, page_remove,
// and page2pa.
//
int
page_insert(pde_t *pgdir, struct PageInfo *pp, void *va, int perm)
{
f01013d6:	55                   	push   %ebp
f01013d7:	89 e5                	mov    %esp,%ebp
f01013d9:	57                   	push   %edi
f01013da:	56                   	push   %esi
f01013db:	53                   	push   %ebx
f01013dc:	83 ec 10             	sub    $0x10,%esp
f01013df:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f01013e2:	8b 7d 10             	mov    0x10(%ebp),%edi
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 1); //find the pointer to the page table entry
f01013e5:	6a 01                	push   $0x1
f01013e7:	57                   	push   %edi
f01013e8:	ff 75 08             	pushl  0x8(%ebp)
f01013eb:	e8 04 fd ff ff       	call   f01010f4 <pgdir_walk>
	//cprintf("Addr from pgdir_walk: %x\n", pte_Ptr);
	if(pte_Ptr == NULL){
f01013f0:	83 c4 10             	add    $0x10,%esp
f01013f3:	85 c0                	test   %eax,%eax
f01013f5:	74 38                	je     f010142f <page_insert+0x59>
f01013f7:	89 c6                	mov    %eax,%esi
		return -E_NO_MEM; //page table couldn't be allocated
	}
	pte_t pte = *pte_Ptr;
f01013f9:	8b 00                	mov    (%eax),%eax
	pp->pp_ref++;
f01013fb:	66 ff 43 04          	incw   0x4(%ebx)
	if(pte & PTE_P){ //check if there is already a page mapped at 'va'
f01013ff:	a8 01                	test   $0x1,%al
f0101401:	74 0f                	je     f0101412 <page_insert+0x3c>
		page_remove(pgdir, va); //remove the exising page
f0101403:	83 ec 08             	sub    $0x8,%esp
f0101406:	57                   	push   %edi
f0101407:	ff 75 08             	pushl  0x8(%ebp)
f010140a:	e8 2e ff ff ff       	call   f010133d <page_remove>
f010140f:	83 c4 10             	add    $0x10,%esp
	}
	*pte_Ptr = PTE_ADDR(page2pa(pp)) | PTE_P | perm; //insert the page
f0101412:	2b 1d 4c e4 18 f0    	sub    0xf018e44c,%ebx
f0101418:	c1 fb 03             	sar    $0x3,%ebx
f010141b:	c1 e3 0c             	shl    $0xc,%ebx
f010141e:	8b 45 14             	mov    0x14(%ebp),%eax
f0101421:	83 c8 01             	or     $0x1,%eax
f0101424:	09 c3                	or     %eax,%ebx
f0101426:	89 1e                	mov    %ebx,(%esi)
	//pgdir[PDX(va)] |= perm;
	//pp->pp_ref++;
	return 0; //success
f0101428:	b8 00 00 00 00       	mov    $0x0,%eax
f010142d:	eb 05                	jmp    f0101434 <page_insert+0x5e>
{
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 1); //find the pointer to the page table entry
	//cprintf("Addr from pgdir_walk: %x\n", pte_Ptr);
	if(pte_Ptr == NULL){
		return -E_NO_MEM; //page table couldn't be allocated
f010142f:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
	}
	*pte_Ptr = PTE_ADDR(page2pa(pp)) | PTE_P | perm; //insert the page
	//pgdir[PDX(va)] |= perm;
	//pp->pp_ref++;
	return 0; //success
}
f0101434:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0101437:	5b                   	pop    %ebx
f0101438:	5e                   	pop    %esi
f0101439:	5f                   	pop    %edi
f010143a:	5d                   	pop    %ebp
f010143b:	c3                   	ret    

f010143c <mem_init>:
//
// From UTOP to ULIM, the user is allowed to read but not write.
// Above ULIM the user cannot read or write.
void
mem_init(void)
{
f010143c:	55                   	push   %ebp
f010143d:	89 e5                	mov    %esp,%ebp
f010143f:	57                   	push   %edi
f0101440:	56                   	push   %esi
f0101441:	53                   	push   %ebx
f0101442:	83 ec 3c             	sub    $0x3c,%esp
	uint32_t i;
	struct e820_entry *e;
	size_t mem = 0, mem_max = -KERNBASE;

	e = e820_map.entries;
	for (i = 0; i != e820_map.nr; ++i, ++e) {
f0101445:	8b 1d 40 df 18 f0    	mov    0xf018df40,%ebx
static void
detect_memory(void)
{
	uint32_t i;
	struct e820_entry *e;
	size_t mem = 0, mem_max = -KERNBASE;
f010144b:	be 00 00 00 00       	mov    $0x0,%esi

	e = e820_map.entries;
f0101450:	b8 44 df 18 f0       	mov    $0xf018df44,%eax
	for (i = 0; i != e820_map.nr; ++i, ++e) {
f0101455:	b9 00 00 00 00       	mov    $0x0,%ecx
f010145a:	eb 1d                	jmp    f0101479 <mem_init+0x3d>
		if (e->addr >= mem_max)
f010145c:	8b 10                	mov    (%eax),%edx
f010145e:	83 78 04 00          	cmpl   $0x0,0x4(%eax)
f0101462:	75 11                	jne    f0101475 <mem_init+0x39>
f0101464:	81 fa ff ff ff 0f    	cmp    $0xfffffff,%edx
f010146a:	77 09                	ja     f0101475 <mem_init+0x39>
			continue;
		mem = MAX(mem, (size_t)(e->addr + e->len));
f010146c:	03 50 08             	add    0x8(%eax),%edx
f010146f:	39 d6                	cmp    %edx,%esi
f0101471:	73 02                	jae    f0101475 <mem_init+0x39>
f0101473:	89 d6                	mov    %edx,%esi
	uint32_t i;
	struct e820_entry *e;
	size_t mem = 0, mem_max = -KERNBASE;

	e = e820_map.entries;
	for (i = 0; i != e820_map.nr; ++i, ++e) {
f0101475:	41                   	inc    %ecx
f0101476:	83 c0 14             	add    $0x14,%eax
f0101479:	39 d9                	cmp    %ebx,%ecx
f010147b:	75 df                	jne    f010145c <mem_init+0x20>
			continue;
		mem = MAX(mem, (size_t)(e->addr + e->len));
	}

	// Limit memory to 256MB.
	mem = MIN(mem, mem_max);
f010147d:	89 f0                	mov    %esi,%eax
f010147f:	81 fe 00 00 00 10    	cmp    $0x10000000,%esi
f0101485:	76 05                	jbe    f010148c <mem_init+0x50>
f0101487:	b8 00 00 00 10       	mov    $0x10000000,%eax
	npages = mem / PGSIZE;
f010148c:	89 c2                	mov    %eax,%edx
f010148e:	c1 ea 0c             	shr    $0xc,%edx
f0101491:	89 15 44 e4 18 f0    	mov    %edx,0xf018e444
	cprintf("E820: physical memory %uMB\n", mem / 1024 / 1024);
f0101497:	83 ec 08             	sub    $0x8,%esp
f010149a:	c1 e8 14             	shr    $0x14,%eax
f010149d:	50                   	push   %eax
f010149e:	68 6e 5f 10 f0       	push   $0xf0105f6e
f01014a3:	e8 21 1e 00 00       	call   f01032c9 <cprintf>
	// Remove this line when you're ready to test this function.
	//panic("mem_init: This function is not finished\n");

	//////////////////////////////////////////////////////////////////////
	// create initial page directory.
	kern_pgdir = (pde_t *) boot_alloc(PGSIZE);
f01014a8:	b8 00 10 00 00       	mov    $0x1000,%eax
f01014ad:	e8 5f f6 ff ff       	call   f0100b11 <boot_alloc>
f01014b2:	a3 48 e4 18 f0       	mov    %eax,0xf018e448
	memset(kern_pgdir, 0, PGSIZE);
f01014b7:	83 c4 0c             	add    $0xc,%esp
f01014ba:	68 00 10 00 00       	push   $0x1000
f01014bf:	6a 00                	push   $0x0
f01014c1:	50                   	push   %eax
f01014c2:	e8 51 36 00 00       	call   f0104b18 <memset>
	// a virtual page table at virtual address UVPT.
	// (For now, you don't have understand the greater purpose of the
	// following line.)

	// Permissions: kernel R, user R
	kern_pgdir[PDX(UVPT)] = PADDR(kern_pgdir) | PTE_U | PTE_P;
f01014c7:	a1 48 e4 18 f0       	mov    0xf018e448,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01014cc:	83 c4 10             	add    $0x10,%esp
f01014cf:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01014d4:	77 15                	ja     f01014eb <mem_init+0xaf>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01014d6:	50                   	push   %eax
f01014d7:	68 78 57 10 f0       	push   $0xf0105778
f01014dc:	68 8c 00 00 00       	push   $0x8c
f01014e1:	68 e9 5d 10 f0       	push   $0xf0105de9
f01014e6:	e8 61 eb ff ff       	call   f010004c <_panic>
f01014eb:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f01014f1:	83 ca 05             	or     $0x5,%edx
f01014f4:	89 90 f4 0e 00 00    	mov    %edx,0xef4(%eax)
	// The kernel uses this array to keep track of physical pages: for
	// each physical page, there is a corresponding struct PageInfo in this
	// array.  'npages' is the number of physical pages in memory.  Use memset
	// to initialize all fields of each struct PageInfo to 0.
	// Your code goes here:
	pages = boot_alloc(npages * sizeof(struct PageInfo)); 
f01014fa:	a1 44 e4 18 f0       	mov    0xf018e444,%eax
f01014ff:	c1 e0 03             	shl    $0x3,%eax
f0101502:	e8 0a f6 ff ff       	call   f0100b11 <boot_alloc>
f0101507:	a3 4c e4 18 f0       	mov    %eax,0xf018e44c

	//Lab3 Exercise 1: allocate and map the envs array
	//pages = boot_alloc(npages * sizeof(struct PageInfo)); 
	envs = (struct Env *)boot_alloc(NENV * sizeof(struct Env));
f010150c:	b8 00 80 01 00       	mov    $0x18000,%eax
f0101511:	e8 fb f5 ff ff       	call   f0100b11 <boot_alloc>
f0101516:	a3 64 d2 18 f0       	mov    %eax,0xf018d264

	//initialize all fields of each struct PageInfo to 0.
	memset(pages, 0, npages * sizeof(struct PageInfo)); //npages * 8 is the total number of bytes
f010151b:	83 ec 04             	sub    $0x4,%esp
f010151e:	a1 44 e4 18 f0       	mov    0xf018e444,%eax
f0101523:	c1 e0 03             	shl    $0x3,%eax
f0101526:	50                   	push   %eax
f0101527:	6a 00                	push   $0x0
f0101529:	ff 35 4c e4 18 f0    	pushl  0xf018e44c
f010152f:	e8 e4 35 00 00       	call   f0104b18 <memset>
	// Now that we've allocated the initial kernel data structures, we set
	// up the list of free physical pages. Once we've done so, all further
	// memory management will go through the page_* functions. In
	// particular, we can now map memory using boot_map_region
	// or page_insert
	page_init();
f0101534:	e8 34 f9 ff ff       	call   f0100e6d <page_init>

	check_page_free_list(1);
f0101539:	b8 01 00 00 00       	mov    $0x1,%eax
f010153e:	e8 7b f6 ff ff       	call   f0100bbe <check_page_free_list>
	int nfree;
	struct PageInfo *fl;
	char *c;
	int i;

	if (!pages)
f0101543:	83 c4 10             	add    $0x10,%esp
f0101546:	83 3d 4c e4 18 f0 00 	cmpl   $0x0,0xf018e44c
f010154d:	75 17                	jne    f0101566 <mem_init+0x12a>
		panic("'pages' is a null pointer!");
f010154f:	83 ec 04             	sub    $0x4,%esp
f0101552:	68 8a 5f 10 f0       	push   $0xf0105f8a
f0101557:	68 64 03 00 00       	push   $0x364
f010155c:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101561:	e8 e6 ea ff ff       	call   f010004c <_panic>

	// check number of free pages
	for (pp = page_free_list, nfree = 0; pp; pp = pp->pp_link)
f0101566:	a1 5c d2 18 f0       	mov    0xf018d25c,%eax
f010156b:	bb 00 00 00 00       	mov    $0x0,%ebx
f0101570:	eb 03                	jmp    f0101575 <mem_init+0x139>
		++nfree;
f0101572:	43                   	inc    %ebx

	if (!pages)
		panic("'pages' is a null pointer!");

	// check number of free pages
	for (pp = page_free_list, nfree = 0; pp; pp = pp->pp_link)
f0101573:	8b 00                	mov    (%eax),%eax
f0101575:	85 c0                	test   %eax,%eax
f0101577:	75 f9                	jne    f0101572 <mem_init+0x136>
		++nfree;

	// should be able to allocate three pages
	pp0 = pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f0101579:	83 ec 0c             	sub    $0xc,%esp
f010157c:	6a 00                	push   $0x0
f010157e:	e8 7a fa ff ff       	call   f0100ffd <page_alloc>
f0101583:	89 c7                	mov    %eax,%edi
f0101585:	83 c4 10             	add    $0x10,%esp
f0101588:	85 c0                	test   %eax,%eax
f010158a:	75 19                	jne    f01015a5 <mem_init+0x169>
f010158c:	68 a5 5f 10 f0       	push   $0xf0105fa5
f0101591:	68 98 4f 10 f0       	push   $0xf0104f98
f0101596:	68 6c 03 00 00       	push   $0x36c
f010159b:	68 e9 5d 10 f0       	push   $0xf0105de9
f01015a0:	e8 a7 ea ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f01015a5:	83 ec 0c             	sub    $0xc,%esp
f01015a8:	6a 00                	push   $0x0
f01015aa:	e8 4e fa ff ff       	call   f0100ffd <page_alloc>
f01015af:	89 c6                	mov    %eax,%esi
f01015b1:	83 c4 10             	add    $0x10,%esp
f01015b4:	85 c0                	test   %eax,%eax
f01015b6:	75 19                	jne    f01015d1 <mem_init+0x195>
f01015b8:	68 bb 5f 10 f0       	push   $0xf0105fbb
f01015bd:	68 98 4f 10 f0       	push   $0xf0104f98
f01015c2:	68 6d 03 00 00       	push   $0x36d
f01015c7:	68 e9 5d 10 f0       	push   $0xf0105de9
f01015cc:	e8 7b ea ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f01015d1:	83 ec 0c             	sub    $0xc,%esp
f01015d4:	6a 00                	push   $0x0
f01015d6:	e8 22 fa ff ff       	call   f0100ffd <page_alloc>
f01015db:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f01015de:	83 c4 10             	add    $0x10,%esp
f01015e1:	85 c0                	test   %eax,%eax
f01015e3:	75 19                	jne    f01015fe <mem_init+0x1c2>
f01015e5:	68 d1 5f 10 f0       	push   $0xf0105fd1
f01015ea:	68 98 4f 10 f0       	push   $0xf0104f98
f01015ef:	68 6e 03 00 00       	push   $0x36e
f01015f4:	68 e9 5d 10 f0       	push   $0xf0105de9
f01015f9:	e8 4e ea ff ff       	call   f010004c <_panic>

	assert(pp0);
	assert(pp1 && pp1 != pp0);
f01015fe:	39 f7                	cmp    %esi,%edi
f0101600:	75 19                	jne    f010161b <mem_init+0x1df>
f0101602:	68 e7 5f 10 f0       	push   $0xf0105fe7
f0101607:	68 98 4f 10 f0       	push   $0xf0104f98
f010160c:	68 71 03 00 00       	push   $0x371
f0101611:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101616:	e8 31 ea ff ff       	call   f010004c <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f010161b:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f010161e:	39 c6                	cmp    %eax,%esi
f0101620:	74 04                	je     f0101626 <mem_init+0x1ea>
f0101622:	39 c7                	cmp    %eax,%edi
f0101624:	75 19                	jne    f010163f <mem_init+0x203>
f0101626:	68 9c 57 10 f0       	push   $0xf010579c
f010162b:	68 98 4f 10 f0       	push   $0xf0104f98
f0101630:	68 72 03 00 00       	push   $0x372
f0101635:	68 e9 5d 10 f0       	push   $0xf0105de9
f010163a:	e8 0d ea ff ff       	call   f010004c <_panic>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f010163f:	8b 0d 4c e4 18 f0    	mov    0xf018e44c,%ecx
	assert(page2pa(pp0) < npages*PGSIZE);
f0101645:	8b 15 44 e4 18 f0    	mov    0xf018e444,%edx
f010164b:	c1 e2 0c             	shl    $0xc,%edx
f010164e:	89 f8                	mov    %edi,%eax
f0101650:	29 c8                	sub    %ecx,%eax
f0101652:	c1 f8 03             	sar    $0x3,%eax
f0101655:	c1 e0 0c             	shl    $0xc,%eax
f0101658:	39 d0                	cmp    %edx,%eax
f010165a:	72 19                	jb     f0101675 <mem_init+0x239>
f010165c:	68 f9 5f 10 f0       	push   $0xf0105ff9
f0101661:	68 98 4f 10 f0       	push   $0xf0104f98
f0101666:	68 73 03 00 00       	push   $0x373
f010166b:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101670:	e8 d7 e9 ff ff       	call   f010004c <_panic>
	assert(page2pa(pp1) < npages*PGSIZE);
f0101675:	89 f0                	mov    %esi,%eax
f0101677:	29 c8                	sub    %ecx,%eax
f0101679:	c1 f8 03             	sar    $0x3,%eax
f010167c:	c1 e0 0c             	shl    $0xc,%eax
f010167f:	39 c2                	cmp    %eax,%edx
f0101681:	77 19                	ja     f010169c <mem_init+0x260>
f0101683:	68 16 60 10 f0       	push   $0xf0106016
f0101688:	68 98 4f 10 f0       	push   $0xf0104f98
f010168d:	68 74 03 00 00       	push   $0x374
f0101692:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101697:	e8 b0 e9 ff ff       	call   f010004c <_panic>
	assert(page2pa(pp2) < npages*PGSIZE);
f010169c:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f010169f:	29 c8                	sub    %ecx,%eax
f01016a1:	c1 f8 03             	sar    $0x3,%eax
f01016a4:	c1 e0 0c             	shl    $0xc,%eax
f01016a7:	39 c2                	cmp    %eax,%edx
f01016a9:	77 19                	ja     f01016c4 <mem_init+0x288>
f01016ab:	68 33 60 10 f0       	push   $0xf0106033
f01016b0:	68 98 4f 10 f0       	push   $0xf0104f98
f01016b5:	68 75 03 00 00       	push   $0x375
f01016ba:	68 e9 5d 10 f0       	push   $0xf0105de9
f01016bf:	e8 88 e9 ff ff       	call   f010004c <_panic>

	// temporarily steal the rest of the free pages
	fl = page_free_list;
f01016c4:	a1 5c d2 18 f0       	mov    0xf018d25c,%eax
f01016c9:	89 45 d0             	mov    %eax,-0x30(%ebp)
	page_free_list = 0;
f01016cc:	c7 05 5c d2 18 f0 00 	movl   $0x0,0xf018d25c
f01016d3:	00 00 00 

	// should be no free memory
	assert(!page_alloc(0));
f01016d6:	83 ec 0c             	sub    $0xc,%esp
f01016d9:	6a 00                	push   $0x0
f01016db:	e8 1d f9 ff ff       	call   f0100ffd <page_alloc>
f01016e0:	83 c4 10             	add    $0x10,%esp
f01016e3:	85 c0                	test   %eax,%eax
f01016e5:	74 19                	je     f0101700 <mem_init+0x2c4>
f01016e7:	68 50 60 10 f0       	push   $0xf0106050
f01016ec:	68 98 4f 10 f0       	push   $0xf0104f98
f01016f1:	68 7c 03 00 00       	push   $0x37c
f01016f6:	68 e9 5d 10 f0       	push   $0xf0105de9
f01016fb:	e8 4c e9 ff ff       	call   f010004c <_panic>

	// free and re-allocate?
	page_free(pp0);
f0101700:	83 ec 0c             	sub    $0xc,%esp
f0101703:	57                   	push   %edi
f0101704:	e8 6a f9 ff ff       	call   f0101073 <page_free>
	page_free(pp1);
f0101709:	89 34 24             	mov    %esi,(%esp)
f010170c:	e8 62 f9 ff ff       	call   f0101073 <page_free>
	page_free(pp2);
f0101711:	83 c4 04             	add    $0x4,%esp
f0101714:	ff 75 d4             	pushl  -0x2c(%ebp)
f0101717:	e8 57 f9 ff ff       	call   f0101073 <page_free>
	pp0 = pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f010171c:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
f0101723:	e8 d5 f8 ff ff       	call   f0100ffd <page_alloc>
f0101728:	89 c6                	mov    %eax,%esi
f010172a:	83 c4 10             	add    $0x10,%esp
f010172d:	85 c0                	test   %eax,%eax
f010172f:	75 19                	jne    f010174a <mem_init+0x30e>
f0101731:	68 a5 5f 10 f0       	push   $0xf0105fa5
f0101736:	68 98 4f 10 f0       	push   $0xf0104f98
f010173b:	68 83 03 00 00       	push   $0x383
f0101740:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101745:	e8 02 e9 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f010174a:	83 ec 0c             	sub    $0xc,%esp
f010174d:	6a 00                	push   $0x0
f010174f:	e8 a9 f8 ff ff       	call   f0100ffd <page_alloc>
f0101754:	89 c7                	mov    %eax,%edi
f0101756:	83 c4 10             	add    $0x10,%esp
f0101759:	85 c0                	test   %eax,%eax
f010175b:	75 19                	jne    f0101776 <mem_init+0x33a>
f010175d:	68 bb 5f 10 f0       	push   $0xf0105fbb
f0101762:	68 98 4f 10 f0       	push   $0xf0104f98
f0101767:	68 84 03 00 00       	push   $0x384
f010176c:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101771:	e8 d6 e8 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f0101776:	83 ec 0c             	sub    $0xc,%esp
f0101779:	6a 00                	push   $0x0
f010177b:	e8 7d f8 ff ff       	call   f0100ffd <page_alloc>
f0101780:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f0101783:	83 c4 10             	add    $0x10,%esp
f0101786:	85 c0                	test   %eax,%eax
f0101788:	75 19                	jne    f01017a3 <mem_init+0x367>
f010178a:	68 d1 5f 10 f0       	push   $0xf0105fd1
f010178f:	68 98 4f 10 f0       	push   $0xf0104f98
f0101794:	68 85 03 00 00       	push   $0x385
f0101799:	68 e9 5d 10 f0       	push   $0xf0105de9
f010179e:	e8 a9 e8 ff ff       	call   f010004c <_panic>
	assert(pp0);
	assert(pp1 && pp1 != pp0);
f01017a3:	39 fe                	cmp    %edi,%esi
f01017a5:	75 19                	jne    f01017c0 <mem_init+0x384>
f01017a7:	68 e7 5f 10 f0       	push   $0xf0105fe7
f01017ac:	68 98 4f 10 f0       	push   $0xf0104f98
f01017b1:	68 87 03 00 00       	push   $0x387
f01017b6:	68 e9 5d 10 f0       	push   $0xf0105de9
f01017bb:	e8 8c e8 ff ff       	call   f010004c <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f01017c0:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01017c3:	39 c7                	cmp    %eax,%edi
f01017c5:	74 04                	je     f01017cb <mem_init+0x38f>
f01017c7:	39 c6                	cmp    %eax,%esi
f01017c9:	75 19                	jne    f01017e4 <mem_init+0x3a8>
f01017cb:	68 9c 57 10 f0       	push   $0xf010579c
f01017d0:	68 98 4f 10 f0       	push   $0xf0104f98
f01017d5:	68 88 03 00 00       	push   $0x388
f01017da:	68 e9 5d 10 f0       	push   $0xf0105de9
f01017df:	e8 68 e8 ff ff       	call   f010004c <_panic>
	assert(!page_alloc(0));
f01017e4:	83 ec 0c             	sub    $0xc,%esp
f01017e7:	6a 00                	push   $0x0
f01017e9:	e8 0f f8 ff ff       	call   f0100ffd <page_alloc>
f01017ee:	83 c4 10             	add    $0x10,%esp
f01017f1:	85 c0                	test   %eax,%eax
f01017f3:	74 19                	je     f010180e <mem_init+0x3d2>
f01017f5:	68 50 60 10 f0       	push   $0xf0106050
f01017fa:	68 98 4f 10 f0       	push   $0xf0104f98
f01017ff:	68 89 03 00 00       	push   $0x389
f0101804:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101809:	e8 3e e8 ff ff       	call   f010004c <_panic>
f010180e:	89 f0                	mov    %esi,%eax
f0101810:	2b 05 4c e4 18 f0    	sub    0xf018e44c,%eax
f0101816:	c1 f8 03             	sar    $0x3,%eax
f0101819:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010181c:	89 c2                	mov    %eax,%edx
f010181e:	c1 ea 0c             	shr    $0xc,%edx
f0101821:	3b 15 44 e4 18 f0    	cmp    0xf018e444,%edx
f0101827:	72 12                	jb     f010183b <mem_init+0x3ff>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101829:	50                   	push   %eax
f010182a:	68 30 56 10 f0       	push   $0xf0105630
f010182f:	6a 56                	push   $0x56
f0101831:	68 f5 5d 10 f0       	push   $0xf0105df5
f0101836:	e8 11 e8 ff ff       	call   f010004c <_panic>

	// test flags
	memset(page2kva(pp0), 1, PGSIZE);
f010183b:	83 ec 04             	sub    $0x4,%esp
f010183e:	68 00 10 00 00       	push   $0x1000
f0101843:	6a 01                	push   $0x1
f0101845:	2d 00 00 00 10       	sub    $0x10000000,%eax
f010184a:	50                   	push   %eax
f010184b:	e8 c8 32 00 00       	call   f0104b18 <memset>
	page_free(pp0);
f0101850:	89 34 24             	mov    %esi,(%esp)
f0101853:	e8 1b f8 ff ff       	call   f0101073 <page_free>
	assert((pp = page_alloc(ALLOC_ZERO)));
f0101858:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
f010185f:	e8 99 f7 ff ff       	call   f0100ffd <page_alloc>
f0101864:	83 c4 10             	add    $0x10,%esp
f0101867:	85 c0                	test   %eax,%eax
f0101869:	75 19                	jne    f0101884 <mem_init+0x448>
f010186b:	68 5f 60 10 f0       	push   $0xf010605f
f0101870:	68 98 4f 10 f0       	push   $0xf0104f98
f0101875:	68 8e 03 00 00       	push   $0x38e
f010187a:	68 e9 5d 10 f0       	push   $0xf0105de9
f010187f:	e8 c8 e7 ff ff       	call   f010004c <_panic>
	assert(pp && pp0 == pp);
f0101884:	39 c6                	cmp    %eax,%esi
f0101886:	74 19                	je     f01018a1 <mem_init+0x465>
f0101888:	68 7d 60 10 f0       	push   $0xf010607d
f010188d:	68 98 4f 10 f0       	push   $0xf0104f98
f0101892:	68 8f 03 00 00       	push   $0x38f
f0101897:	68 e9 5d 10 f0       	push   $0xf0105de9
f010189c:	e8 ab e7 ff ff       	call   f010004c <_panic>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f01018a1:	89 f0                	mov    %esi,%eax
f01018a3:	2b 05 4c e4 18 f0    	sub    0xf018e44c,%eax
f01018a9:	c1 f8 03             	sar    $0x3,%eax
f01018ac:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01018af:	89 c2                	mov    %eax,%edx
f01018b1:	c1 ea 0c             	shr    $0xc,%edx
f01018b4:	3b 15 44 e4 18 f0    	cmp    0xf018e444,%edx
f01018ba:	72 12                	jb     f01018ce <mem_init+0x492>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01018bc:	50                   	push   %eax
f01018bd:	68 30 56 10 f0       	push   $0xf0105630
f01018c2:	6a 56                	push   $0x56
f01018c4:	68 f5 5d 10 f0       	push   $0xf0105df5
f01018c9:	e8 7e e7 ff ff       	call   f010004c <_panic>
f01018ce:	8d 90 00 10 00 f0    	lea    -0xffff000(%eax),%edx
	return (void *)(pa + KERNBASE);
f01018d4:	8d 80 00 00 00 f0    	lea    -0x10000000(%eax),%eax
	c = page2kva(pp);
	for (i = 0; i < PGSIZE; i++)
		assert(c[i] == 0);
f01018da:	80 38 00             	cmpb   $0x0,(%eax)
f01018dd:	74 19                	je     f01018f8 <mem_init+0x4bc>
f01018df:	68 8d 60 10 f0       	push   $0xf010608d
f01018e4:	68 98 4f 10 f0       	push   $0xf0104f98
f01018e9:	68 92 03 00 00       	push   $0x392
f01018ee:	68 e9 5d 10 f0       	push   $0xf0105de9
f01018f3:	e8 54 e7 ff ff       	call   f010004c <_panic>
f01018f8:	40                   	inc    %eax
	memset(page2kva(pp0), 1, PGSIZE);
	page_free(pp0);
	assert((pp = page_alloc(ALLOC_ZERO)));
	assert(pp && pp0 == pp);
	c = page2kva(pp);
	for (i = 0; i < PGSIZE; i++)
f01018f9:	39 d0                	cmp    %edx,%eax
f01018fb:	75 dd                	jne    f01018da <mem_init+0x49e>
		assert(c[i] == 0);

	// give free list back
	page_free_list = fl;
f01018fd:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0101900:	a3 5c d2 18 f0       	mov    %eax,0xf018d25c

	// free the pages we took
	page_free(pp0);
f0101905:	83 ec 0c             	sub    $0xc,%esp
f0101908:	56                   	push   %esi
f0101909:	e8 65 f7 ff ff       	call   f0101073 <page_free>
	page_free(pp1);
f010190e:	89 3c 24             	mov    %edi,(%esp)
f0101911:	e8 5d f7 ff ff       	call   f0101073 <page_free>
	page_free(pp2);
f0101916:	83 c4 04             	add    $0x4,%esp
f0101919:	ff 75 d4             	pushl  -0x2c(%ebp)
f010191c:	e8 52 f7 ff ff       	call   f0101073 <page_free>

	// number of free pages should be the same
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0101921:	a1 5c d2 18 f0       	mov    0xf018d25c,%eax
f0101926:	83 c4 10             	add    $0x10,%esp
f0101929:	eb 03                	jmp    f010192e <mem_init+0x4f2>
		--nfree;
f010192b:	4b                   	dec    %ebx
	page_free(pp0);
	page_free(pp1);
	page_free(pp2);

	// number of free pages should be the same
	for (pp = page_free_list; pp; pp = pp->pp_link)
f010192c:	8b 00                	mov    (%eax),%eax
f010192e:	85 c0                	test   %eax,%eax
f0101930:	75 f9                	jne    f010192b <mem_init+0x4ef>
		--nfree;
	assert(nfree == 0);
f0101932:	85 db                	test   %ebx,%ebx
f0101934:	74 19                	je     f010194f <mem_init+0x513>
f0101936:	68 97 60 10 f0       	push   $0xf0106097
f010193b:	68 98 4f 10 f0       	push   $0xf0104f98
f0101940:	68 9f 03 00 00       	push   $0x39f
f0101945:	68 e9 5d 10 f0       	push   $0xf0105de9
f010194a:	e8 fd e6 ff ff       	call   f010004c <_panic>

	cprintf("check_page_alloc() succeeded!\n");
f010194f:	83 ec 0c             	sub    $0xc,%esp
f0101952:	68 bc 57 10 f0       	push   $0xf01057bc
f0101957:	e8 6d 19 00 00       	call   f01032c9 <cprintf>
	int i;
	extern pde_t entry_pgdir[];

	// should be able to allocate three pages
	pp0 = pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f010195c:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
f0101963:	e8 95 f6 ff ff       	call   f0100ffd <page_alloc>
f0101968:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f010196b:	83 c4 10             	add    $0x10,%esp
f010196e:	85 c0                	test   %eax,%eax
f0101970:	75 19                	jne    f010198b <mem_init+0x54f>
f0101972:	68 a5 5f 10 f0       	push   $0xf0105fa5
f0101977:	68 98 4f 10 f0       	push   $0xf0104f98
f010197c:	68 0e 04 00 00       	push   $0x40e
f0101981:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101986:	e8 c1 e6 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f010198b:	83 ec 0c             	sub    $0xc,%esp
f010198e:	6a 00                	push   $0x0
f0101990:	e8 68 f6 ff ff       	call   f0100ffd <page_alloc>
f0101995:	89 c6                	mov    %eax,%esi
f0101997:	83 c4 10             	add    $0x10,%esp
f010199a:	85 c0                	test   %eax,%eax
f010199c:	75 19                	jne    f01019b7 <mem_init+0x57b>
f010199e:	68 bb 5f 10 f0       	push   $0xf0105fbb
f01019a3:	68 98 4f 10 f0       	push   $0xf0104f98
f01019a8:	68 0f 04 00 00       	push   $0x40f
f01019ad:	68 e9 5d 10 f0       	push   $0xf0105de9
f01019b2:	e8 95 e6 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f01019b7:	83 ec 0c             	sub    $0xc,%esp
f01019ba:	6a 00                	push   $0x0
f01019bc:	e8 3c f6 ff ff       	call   f0100ffd <page_alloc>
f01019c1:	89 c3                	mov    %eax,%ebx
f01019c3:	83 c4 10             	add    $0x10,%esp
f01019c6:	85 c0                	test   %eax,%eax
f01019c8:	75 19                	jne    f01019e3 <mem_init+0x5a7>
f01019ca:	68 d1 5f 10 f0       	push   $0xf0105fd1
f01019cf:	68 98 4f 10 f0       	push   $0xf0104f98
f01019d4:	68 10 04 00 00       	push   $0x410
f01019d9:	68 e9 5d 10 f0       	push   $0xf0105de9
f01019de:	e8 69 e6 ff ff       	call   f010004c <_panic>

	assert(pp0);
	assert(pp1 && pp1 != pp0);
f01019e3:	39 75 d4             	cmp    %esi,-0x2c(%ebp)
f01019e6:	75 19                	jne    f0101a01 <mem_init+0x5c5>
f01019e8:	68 e7 5f 10 f0       	push   $0xf0105fe7
f01019ed:	68 98 4f 10 f0       	push   $0xf0104f98
f01019f2:	68 13 04 00 00       	push   $0x413
f01019f7:	68 e9 5d 10 f0       	push   $0xf0105de9
f01019fc:	e8 4b e6 ff ff       	call   f010004c <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f0101a01:	39 c6                	cmp    %eax,%esi
f0101a03:	74 05                	je     f0101a0a <mem_init+0x5ce>
f0101a05:	39 45 d4             	cmp    %eax,-0x2c(%ebp)
f0101a08:	75 19                	jne    f0101a23 <mem_init+0x5e7>
f0101a0a:	68 9c 57 10 f0       	push   $0xf010579c
f0101a0f:	68 98 4f 10 f0       	push   $0xf0104f98
f0101a14:	68 14 04 00 00       	push   $0x414
f0101a19:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101a1e:	e8 29 e6 ff ff       	call   f010004c <_panic>

	// temporarily steal the rest of the free pages
	fl = page_free_list;
f0101a23:	a1 5c d2 18 f0       	mov    0xf018d25c,%eax
f0101a28:	89 45 d0             	mov    %eax,-0x30(%ebp)

	page_free_list = 0;
f0101a2b:	c7 05 5c d2 18 f0 00 	movl   $0x0,0xf018d25c
f0101a32:	00 00 00 
	//print_fl();
	// should be no free memory
	assert(!page_alloc(0));
f0101a35:	83 ec 0c             	sub    $0xc,%esp
f0101a38:	6a 00                	push   $0x0
f0101a3a:	e8 be f5 ff ff       	call   f0100ffd <page_alloc>
f0101a3f:	83 c4 10             	add    $0x10,%esp
f0101a42:	85 c0                	test   %eax,%eax
f0101a44:	74 19                	je     f0101a5f <mem_init+0x623>
f0101a46:	68 50 60 10 f0       	push   $0xf0106050
f0101a4b:	68 98 4f 10 f0       	push   $0xf0104f98
f0101a50:	68 1c 04 00 00       	push   $0x41c
f0101a55:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101a5a:	e8 ed e5 ff ff       	call   f010004c <_panic>
	//print_fl();
	// there is no page allocated at address 0
	assert(page_lookup(kern_pgdir, (void *) 0x0, &ptep) == NULL);
f0101a5f:	83 ec 04             	sub    $0x4,%esp
f0101a62:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0101a65:	50                   	push   %eax
f0101a66:	6a 00                	push   $0x0
f0101a68:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0101a6e:	e8 41 f8 ff ff       	call   f01012b4 <page_lookup>
f0101a73:	83 c4 10             	add    $0x10,%esp
f0101a76:	85 c0                	test   %eax,%eax
f0101a78:	74 19                	je     f0101a93 <mem_init+0x657>
f0101a7a:	68 dc 57 10 f0       	push   $0xf01057dc
f0101a7f:	68 98 4f 10 f0       	push   $0xf0104f98
f0101a84:	68 1f 04 00 00       	push   $0x41f
f0101a89:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101a8e:	e8 b9 e5 ff ff       	call   f010004c <_panic>
	//cprintf("H1\n");
	// there is no free memory, so we can't allocate a page table
	assert(page_insert(kern_pgdir, pp1, 0x0, PTE_W) < 0); //insert fail
f0101a93:	6a 02                	push   $0x2
f0101a95:	6a 00                	push   $0x0
f0101a97:	56                   	push   %esi
f0101a98:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0101a9e:	e8 33 f9 ff ff       	call   f01013d6 <page_insert>
f0101aa3:	83 c4 10             	add    $0x10,%esp
f0101aa6:	85 c0                	test   %eax,%eax
f0101aa8:	78 19                	js     f0101ac3 <mem_init+0x687>
f0101aaa:	68 14 58 10 f0       	push   $0xf0105814
f0101aaf:	68 98 4f 10 f0       	push   $0xf0104f98
f0101ab4:	68 22 04 00 00       	push   $0x422
f0101ab9:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101abe:	e8 89 e5 ff ff       	call   f010004c <_panic>
	//print_fl();
	//cprintf("H2\n");
	// free pp0 and try again: pp0 should be used for page table
	page_free(pp0);
f0101ac3:	83 ec 0c             	sub    $0xc,%esp
f0101ac6:	ff 75 d4             	pushl  -0x2c(%ebp)
f0101ac9:	e8 a5 f5 ff ff       	call   f0101073 <page_free>
	//cprintf("H3\n");
	
	assert(page_insert(kern_pgdir, pp1, 0x0, PTE_W) == 0); //insert success
f0101ace:	6a 02                	push   $0x2
f0101ad0:	6a 00                	push   $0x0
f0101ad2:	56                   	push   %esi
f0101ad3:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0101ad9:	e8 f8 f8 ff ff       	call   f01013d6 <page_insert>
f0101ade:	83 c4 20             	add    $0x20,%esp
f0101ae1:	85 c0                	test   %eax,%eax
f0101ae3:	74 19                	je     f0101afe <mem_init+0x6c2>
f0101ae5:	68 44 58 10 f0       	push   $0xf0105844
f0101aea:	68 98 4f 10 f0       	push   $0xf0104f98
f0101aef:	68 29 04 00 00       	push   $0x429
f0101af4:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101af9:	e8 4e e5 ff ff       	call   f010004c <_panic>
	//print_fl();
	//cprintf("H3\n");
	assert(PTE_ADDR(kern_pgdir[0]) == page2pa(pp0)); //problem
f0101afe:	8b 3d 48 e4 18 f0    	mov    0xf018e448,%edi
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101b04:	a1 4c e4 18 f0       	mov    0xf018e44c,%eax
f0101b09:	89 45 cc             	mov    %eax,-0x34(%ebp)
f0101b0c:	8b 17                	mov    (%edi),%edx
f0101b0e:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f0101b14:	8b 4d d4             	mov    -0x2c(%ebp),%ecx
f0101b17:	29 c1                	sub    %eax,%ecx
f0101b19:	89 c8                	mov    %ecx,%eax
f0101b1b:	c1 f8 03             	sar    $0x3,%eax
f0101b1e:	c1 e0 0c             	shl    $0xc,%eax
f0101b21:	39 c2                	cmp    %eax,%edx
f0101b23:	74 19                	je     f0101b3e <mem_init+0x702>
f0101b25:	68 74 58 10 f0       	push   $0xf0105874
f0101b2a:	68 98 4f 10 f0       	push   $0xf0104f98
f0101b2f:	68 2c 04 00 00       	push   $0x42c
f0101b34:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101b39:	e8 0e e5 ff ff       	call   f010004c <_panic>
	//cprintf("H4\n");
	//cprintf("Address returned by check_va2pa is: %x\n", check_va2pa(kern_pgdir, 0x0));
	assert(check_va2pa(kern_pgdir, 0x0) == page2pa(pp1)); //problem
f0101b3e:	ba 00 00 00 00       	mov    $0x0,%edx
f0101b43:	89 f8                	mov    %edi,%eax
f0101b45:	e8 15 f0 ff ff       	call   f0100b5f <check_va2pa>
f0101b4a:	89 f2                	mov    %esi,%edx
f0101b4c:	2b 55 cc             	sub    -0x34(%ebp),%edx
f0101b4f:	c1 fa 03             	sar    $0x3,%edx
f0101b52:	c1 e2 0c             	shl    $0xc,%edx
f0101b55:	39 d0                	cmp    %edx,%eax
f0101b57:	74 19                	je     f0101b72 <mem_init+0x736>
f0101b59:	68 9c 58 10 f0       	push   $0xf010589c
f0101b5e:	68 98 4f 10 f0       	push   $0xf0104f98
f0101b63:	68 2f 04 00 00       	push   $0x42f
f0101b68:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101b6d:	e8 da e4 ff ff       	call   f010004c <_panic>
	//cprintf("H5\n");
	assert(pp1->pp_ref == 1);
f0101b72:	66 83 7e 04 01       	cmpw   $0x1,0x4(%esi)
f0101b77:	74 19                	je     f0101b92 <mem_init+0x756>
f0101b79:	68 a2 60 10 f0       	push   $0xf01060a2
f0101b7e:	68 98 4f 10 f0       	push   $0xf0104f98
f0101b83:	68 31 04 00 00       	push   $0x431
f0101b88:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101b8d:	e8 ba e4 ff ff       	call   f010004c <_panic>
	assert(pp0->pp_ref == 1);
f0101b92:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0101b95:	66 83 78 04 01       	cmpw   $0x1,0x4(%eax)
f0101b9a:	74 19                	je     f0101bb5 <mem_init+0x779>
f0101b9c:	68 b3 60 10 f0       	push   $0xf01060b3
f0101ba1:	68 98 4f 10 f0       	push   $0xf0104f98
f0101ba6:	68 32 04 00 00       	push   $0x432
f0101bab:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101bb0:	e8 97 e4 ff ff       	call   f010004c <_panic>

	//print_fl();
	// should be able to map pp2 at PGSIZE because pp0 is already allocated for page table
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W) == 0);
f0101bb5:	6a 02                	push   $0x2
f0101bb7:	68 00 10 00 00       	push   $0x1000
f0101bbc:	53                   	push   %ebx
f0101bbd:	57                   	push   %edi
f0101bbe:	e8 13 f8 ff ff       	call   f01013d6 <page_insert>
f0101bc3:	83 c4 10             	add    $0x10,%esp
f0101bc6:	85 c0                	test   %eax,%eax
f0101bc8:	74 19                	je     f0101be3 <mem_init+0x7a7>
f0101bca:	68 cc 58 10 f0       	push   $0xf01058cc
f0101bcf:	68 98 4f 10 f0       	push   $0xf0104f98
f0101bd4:	68 36 04 00 00       	push   $0x436
f0101bd9:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101bde:	e8 69 e4 ff ff       	call   f010004c <_panic>
	//print_fl();
	//cprintf("Address returned by check_va2pa is: %x\n", check_va2pa(kern_pgdir, PGSIZE));
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp2));
f0101be3:	ba 00 10 00 00       	mov    $0x1000,%edx
f0101be8:	a1 48 e4 18 f0       	mov    0xf018e448,%eax
f0101bed:	e8 6d ef ff ff       	call   f0100b5f <check_va2pa>
f0101bf2:	89 da                	mov    %ebx,%edx
f0101bf4:	2b 15 4c e4 18 f0    	sub    0xf018e44c,%edx
f0101bfa:	c1 fa 03             	sar    $0x3,%edx
f0101bfd:	c1 e2 0c             	shl    $0xc,%edx
f0101c00:	39 d0                	cmp    %edx,%eax
f0101c02:	74 19                	je     f0101c1d <mem_init+0x7e1>
f0101c04:	68 08 59 10 f0       	push   $0xf0105908
f0101c09:	68 98 4f 10 f0       	push   $0xf0104f98
f0101c0e:	68 39 04 00 00       	push   $0x439
f0101c13:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101c18:	e8 2f e4 ff ff       	call   f010004c <_panic>
	//cprintf("H6\n");
	assert(pp2->pp_ref == 1);
f0101c1d:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f0101c22:	74 19                	je     f0101c3d <mem_init+0x801>
f0101c24:	68 c4 60 10 f0       	push   $0xf01060c4
f0101c29:	68 98 4f 10 f0       	push   $0xf0104f98
f0101c2e:	68 3b 04 00 00       	push   $0x43b
f0101c33:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101c38:	e8 0f e4 ff ff       	call   f010004c <_panic>

	// should be no free memory
	//print_fl();
	assert(!page_alloc(0));
f0101c3d:	83 ec 0c             	sub    $0xc,%esp
f0101c40:	6a 00                	push   $0x0
f0101c42:	e8 b6 f3 ff ff       	call   f0100ffd <page_alloc>
f0101c47:	83 c4 10             	add    $0x10,%esp
f0101c4a:	85 c0                	test   %eax,%eax
f0101c4c:	74 19                	je     f0101c67 <mem_init+0x82b>
f0101c4e:	68 50 60 10 f0       	push   $0xf0106050
f0101c53:	68 98 4f 10 f0       	push   $0xf0104f98
f0101c58:	68 3f 04 00 00       	push   $0x43f
f0101c5d:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101c62:	e8 e5 e3 ff ff       	call   f010004c <_panic>
	//print_fl();
	// should be able to map pp2 at PGSIZE because it's already there
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W) == 0);
f0101c67:	6a 02                	push   $0x2
f0101c69:	68 00 10 00 00       	push   $0x1000
f0101c6e:	53                   	push   %ebx
f0101c6f:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0101c75:	e8 5c f7 ff ff       	call   f01013d6 <page_insert>
f0101c7a:	83 c4 10             	add    $0x10,%esp
f0101c7d:	85 c0                	test   %eax,%eax
f0101c7f:	74 19                	je     f0101c9a <mem_init+0x85e>
f0101c81:	68 cc 58 10 f0       	push   $0xf01058cc
f0101c86:	68 98 4f 10 f0       	push   $0xf0104f98
f0101c8b:	68 42 04 00 00       	push   $0x442
f0101c90:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101c95:	e8 b2 e3 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp2));
f0101c9a:	ba 00 10 00 00       	mov    $0x1000,%edx
f0101c9f:	a1 48 e4 18 f0       	mov    0xf018e448,%eax
f0101ca4:	e8 b6 ee ff ff       	call   f0100b5f <check_va2pa>
f0101ca9:	89 da                	mov    %ebx,%edx
f0101cab:	2b 15 4c e4 18 f0    	sub    0xf018e44c,%edx
f0101cb1:	c1 fa 03             	sar    $0x3,%edx
f0101cb4:	c1 e2 0c             	shl    $0xc,%edx
f0101cb7:	39 d0                	cmp    %edx,%eax
f0101cb9:	74 19                	je     f0101cd4 <mem_init+0x898>
f0101cbb:	68 08 59 10 f0       	push   $0xf0105908
f0101cc0:	68 98 4f 10 f0       	push   $0xf0104f98
f0101cc5:	68 43 04 00 00       	push   $0x443
f0101cca:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101ccf:	e8 78 e3 ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 1);
f0101cd4:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f0101cd9:	74 19                	je     f0101cf4 <mem_init+0x8b8>
f0101cdb:	68 c4 60 10 f0       	push   $0xf01060c4
f0101ce0:	68 98 4f 10 f0       	push   $0xf0104f98
f0101ce5:	68 44 04 00 00       	push   $0x444
f0101cea:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101cef:	e8 58 e3 ff ff       	call   f010004c <_panic>
	//print_fl();
	// pp2 should NOT be on the free list
	// could happen in ref counts are handled sloppily in page_insert
	assert(!page_alloc(0));
f0101cf4:	83 ec 0c             	sub    $0xc,%esp
f0101cf7:	6a 00                	push   $0x0
f0101cf9:	e8 ff f2 ff ff       	call   f0100ffd <page_alloc>
f0101cfe:	83 c4 10             	add    $0x10,%esp
f0101d01:	85 c0                	test   %eax,%eax
f0101d03:	74 19                	je     f0101d1e <mem_init+0x8e2>
f0101d05:	68 50 60 10 f0       	push   $0xf0106050
f0101d0a:	68 98 4f 10 f0       	push   $0xf0104f98
f0101d0f:	68 48 04 00 00       	push   $0x448
f0101d14:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101d19:	e8 2e e3 ff ff       	call   f010004c <_panic>

	// check that pgdir_walk returns a pointer to the pte
	ptep = (pte_t *) KADDR(PTE_ADDR(kern_pgdir[PDX(PGSIZE)]));
f0101d1e:	8b 15 48 e4 18 f0    	mov    0xf018e448,%edx
f0101d24:	8b 02                	mov    (%edx),%eax
f0101d26:	25 00 f0 ff ff       	and    $0xfffff000,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101d2b:	89 c1                	mov    %eax,%ecx
f0101d2d:	c1 e9 0c             	shr    $0xc,%ecx
f0101d30:	3b 0d 44 e4 18 f0    	cmp    0xf018e444,%ecx
f0101d36:	72 15                	jb     f0101d4d <mem_init+0x911>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101d38:	50                   	push   %eax
f0101d39:	68 30 56 10 f0       	push   $0xf0105630
f0101d3e:	68 4b 04 00 00       	push   $0x44b
f0101d43:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101d48:	e8 ff e2 ff ff       	call   f010004c <_panic>
f0101d4d:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0101d52:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	assert(pgdir_walk(kern_pgdir, (void*)PGSIZE, 0) == ptep+PTX(PGSIZE));
f0101d55:	83 ec 04             	sub    $0x4,%esp
f0101d58:	6a 00                	push   $0x0
f0101d5a:	68 00 10 00 00       	push   $0x1000
f0101d5f:	52                   	push   %edx
f0101d60:	e8 8f f3 ff ff       	call   f01010f4 <pgdir_walk>
f0101d65:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0101d68:	8d 57 04             	lea    0x4(%edi),%edx
f0101d6b:	83 c4 10             	add    $0x10,%esp
f0101d6e:	39 d0                	cmp    %edx,%eax
f0101d70:	74 19                	je     f0101d8b <mem_init+0x94f>
f0101d72:	68 38 59 10 f0       	push   $0xf0105938
f0101d77:	68 98 4f 10 f0       	push   $0xf0104f98
f0101d7c:	68 4c 04 00 00       	push   $0x44c
f0101d81:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101d86:	e8 c1 e2 ff ff       	call   f010004c <_panic>

	// should be able to change permissions too.
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W|PTE_U) == 0);
f0101d8b:	6a 06                	push   $0x6
f0101d8d:	68 00 10 00 00       	push   $0x1000
f0101d92:	53                   	push   %ebx
f0101d93:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0101d99:	e8 38 f6 ff ff       	call   f01013d6 <page_insert>
f0101d9e:	83 c4 10             	add    $0x10,%esp
f0101da1:	85 c0                	test   %eax,%eax
f0101da3:	74 19                	je     f0101dbe <mem_init+0x982>
f0101da5:	68 78 59 10 f0       	push   $0xf0105978
f0101daa:	68 98 4f 10 f0       	push   $0xf0104f98
f0101daf:	68 4f 04 00 00       	push   $0x44f
f0101db4:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101db9:	e8 8e e2 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp2));
f0101dbe:	8b 3d 48 e4 18 f0    	mov    0xf018e448,%edi
f0101dc4:	ba 00 10 00 00       	mov    $0x1000,%edx
f0101dc9:	89 f8                	mov    %edi,%eax
f0101dcb:	e8 8f ed ff ff       	call   f0100b5f <check_va2pa>
f0101dd0:	89 da                	mov    %ebx,%edx
f0101dd2:	2b 15 4c e4 18 f0    	sub    0xf018e44c,%edx
f0101dd8:	c1 fa 03             	sar    $0x3,%edx
f0101ddb:	c1 e2 0c             	shl    $0xc,%edx
f0101dde:	39 d0                	cmp    %edx,%eax
f0101de0:	74 19                	je     f0101dfb <mem_init+0x9bf>
f0101de2:	68 08 59 10 f0       	push   $0xf0105908
f0101de7:	68 98 4f 10 f0       	push   $0xf0104f98
f0101dec:	68 50 04 00 00       	push   $0x450
f0101df1:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101df6:	e8 51 e2 ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 1);
f0101dfb:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f0101e00:	74 19                	je     f0101e1b <mem_init+0x9df>
f0101e02:	68 c4 60 10 f0       	push   $0xf01060c4
f0101e07:	68 98 4f 10 f0       	push   $0xf0104f98
f0101e0c:	68 51 04 00 00       	push   $0x451
f0101e11:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101e16:	e8 31 e2 ff ff       	call   f010004c <_panic>
	assert(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_U);
f0101e1b:	83 ec 04             	sub    $0x4,%esp
f0101e1e:	6a 00                	push   $0x0
f0101e20:	68 00 10 00 00       	push   $0x1000
f0101e25:	57                   	push   %edi
f0101e26:	e8 c9 f2 ff ff       	call   f01010f4 <pgdir_walk>
f0101e2b:	83 c4 10             	add    $0x10,%esp
f0101e2e:	f6 00 04             	testb  $0x4,(%eax)
f0101e31:	75 19                	jne    f0101e4c <mem_init+0xa10>
f0101e33:	68 b8 59 10 f0       	push   $0xf01059b8
f0101e38:	68 98 4f 10 f0       	push   $0xf0104f98
f0101e3d:	68 52 04 00 00       	push   $0x452
f0101e42:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101e47:	e8 00 e2 ff ff       	call   f010004c <_panic>
	assert(kern_pgdir[0] & PTE_U);
f0101e4c:	a1 48 e4 18 f0       	mov    0xf018e448,%eax
f0101e51:	f6 00 04             	testb  $0x4,(%eax)
f0101e54:	75 19                	jne    f0101e6f <mem_init+0xa33>
f0101e56:	68 d5 60 10 f0       	push   $0xf01060d5
f0101e5b:	68 98 4f 10 f0       	push   $0xf0104f98
f0101e60:	68 53 04 00 00       	push   $0x453
f0101e65:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101e6a:	e8 dd e1 ff ff       	call   f010004c <_panic>

	// should be able to remap with fewer permissions
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W) == 0);
f0101e6f:	6a 02                	push   $0x2
f0101e71:	68 00 10 00 00       	push   $0x1000
f0101e76:	53                   	push   %ebx
f0101e77:	50                   	push   %eax
f0101e78:	e8 59 f5 ff ff       	call   f01013d6 <page_insert>
f0101e7d:	83 c4 10             	add    $0x10,%esp
f0101e80:	85 c0                	test   %eax,%eax
f0101e82:	74 19                	je     f0101e9d <mem_init+0xa61>
f0101e84:	68 cc 58 10 f0       	push   $0xf01058cc
f0101e89:	68 98 4f 10 f0       	push   $0xf0104f98
f0101e8e:	68 56 04 00 00       	push   $0x456
f0101e93:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101e98:	e8 af e1 ff ff       	call   f010004c <_panic>
	assert(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_W);
f0101e9d:	83 ec 04             	sub    $0x4,%esp
f0101ea0:	6a 00                	push   $0x0
f0101ea2:	68 00 10 00 00       	push   $0x1000
f0101ea7:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0101ead:	e8 42 f2 ff ff       	call   f01010f4 <pgdir_walk>
f0101eb2:	83 c4 10             	add    $0x10,%esp
f0101eb5:	f6 00 02             	testb  $0x2,(%eax)
f0101eb8:	75 19                	jne    f0101ed3 <mem_init+0xa97>
f0101eba:	68 ec 59 10 f0       	push   $0xf01059ec
f0101ebf:	68 98 4f 10 f0       	push   $0xf0104f98
f0101ec4:	68 57 04 00 00       	push   $0x457
f0101ec9:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101ece:	e8 79 e1 ff ff       	call   f010004c <_panic>
	assert(!(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_U));
f0101ed3:	83 ec 04             	sub    $0x4,%esp
f0101ed6:	6a 00                	push   $0x0
f0101ed8:	68 00 10 00 00       	push   $0x1000
f0101edd:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0101ee3:	e8 0c f2 ff ff       	call   f01010f4 <pgdir_walk>
f0101ee8:	83 c4 10             	add    $0x10,%esp
f0101eeb:	f6 00 04             	testb  $0x4,(%eax)
f0101eee:	74 19                	je     f0101f09 <mem_init+0xacd>
f0101ef0:	68 20 5a 10 f0       	push   $0xf0105a20
f0101ef5:	68 98 4f 10 f0       	push   $0xf0104f98
f0101efa:	68 58 04 00 00       	push   $0x458
f0101eff:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101f04:	e8 43 e1 ff ff       	call   f010004c <_panic>

	// should not be able to map at PTSIZE because need free page for page table
	assert(page_insert(kern_pgdir, pp0, (void*) PTSIZE, PTE_W) < 0);
f0101f09:	6a 02                	push   $0x2
f0101f0b:	68 00 00 40 00       	push   $0x400000
f0101f10:	ff 75 d4             	pushl  -0x2c(%ebp)
f0101f13:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0101f19:	e8 b8 f4 ff ff       	call   f01013d6 <page_insert>
f0101f1e:	83 c4 10             	add    $0x10,%esp
f0101f21:	85 c0                	test   %eax,%eax
f0101f23:	78 19                	js     f0101f3e <mem_init+0xb02>
f0101f25:	68 58 5a 10 f0       	push   $0xf0105a58
f0101f2a:	68 98 4f 10 f0       	push   $0xf0104f98
f0101f2f:	68 5b 04 00 00       	push   $0x45b
f0101f34:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101f39:	e8 0e e1 ff ff       	call   f010004c <_panic>

	// insert pp1 at PGSIZE (replacing pp2)
	assert(page_insert(kern_pgdir, pp1, (void*) PGSIZE, PTE_W) == 0);
f0101f3e:	6a 02                	push   $0x2
f0101f40:	68 00 10 00 00       	push   $0x1000
f0101f45:	56                   	push   %esi
f0101f46:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0101f4c:	e8 85 f4 ff ff       	call   f01013d6 <page_insert>
f0101f51:	83 c4 10             	add    $0x10,%esp
f0101f54:	85 c0                	test   %eax,%eax
f0101f56:	74 19                	je     f0101f71 <mem_init+0xb35>
f0101f58:	68 90 5a 10 f0       	push   $0xf0105a90
f0101f5d:	68 98 4f 10 f0       	push   $0xf0104f98
f0101f62:	68 5e 04 00 00       	push   $0x45e
f0101f67:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101f6c:	e8 db e0 ff ff       	call   f010004c <_panic>
	assert(!(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_U));
f0101f71:	83 ec 04             	sub    $0x4,%esp
f0101f74:	6a 00                	push   $0x0
f0101f76:	68 00 10 00 00       	push   $0x1000
f0101f7b:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0101f81:	e8 6e f1 ff ff       	call   f01010f4 <pgdir_walk>
f0101f86:	83 c4 10             	add    $0x10,%esp
f0101f89:	f6 00 04             	testb  $0x4,(%eax)
f0101f8c:	74 19                	je     f0101fa7 <mem_init+0xb6b>
f0101f8e:	68 20 5a 10 f0       	push   $0xf0105a20
f0101f93:	68 98 4f 10 f0       	push   $0xf0104f98
f0101f98:	68 5f 04 00 00       	push   $0x45f
f0101f9d:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101fa2:	e8 a5 e0 ff ff       	call   f010004c <_panic>

	// should have pp1 at both 0 and PGSIZE, pp2 nowhere, ...
	assert(check_va2pa(kern_pgdir, 0) == page2pa(pp1));
f0101fa7:	8b 3d 48 e4 18 f0    	mov    0xf018e448,%edi
f0101fad:	ba 00 00 00 00       	mov    $0x0,%edx
f0101fb2:	89 f8                	mov    %edi,%eax
f0101fb4:	e8 a6 eb ff ff       	call   f0100b5f <check_va2pa>
f0101fb9:	89 c1                	mov    %eax,%ecx
f0101fbb:	89 45 cc             	mov    %eax,-0x34(%ebp)
f0101fbe:	89 f0                	mov    %esi,%eax
f0101fc0:	2b 05 4c e4 18 f0    	sub    0xf018e44c,%eax
f0101fc6:	c1 f8 03             	sar    $0x3,%eax
f0101fc9:	c1 e0 0c             	shl    $0xc,%eax
f0101fcc:	39 c1                	cmp    %eax,%ecx
f0101fce:	74 19                	je     f0101fe9 <mem_init+0xbad>
f0101fd0:	68 cc 5a 10 f0       	push   $0xf0105acc
f0101fd5:	68 98 4f 10 f0       	push   $0xf0104f98
f0101fda:	68 62 04 00 00       	push   $0x462
f0101fdf:	68 e9 5d 10 f0       	push   $0xf0105de9
f0101fe4:	e8 63 e0 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp1));
f0101fe9:	ba 00 10 00 00       	mov    $0x1000,%edx
f0101fee:	89 f8                	mov    %edi,%eax
f0101ff0:	e8 6a eb ff ff       	call   f0100b5f <check_va2pa>
f0101ff5:	39 45 cc             	cmp    %eax,-0x34(%ebp)
f0101ff8:	74 19                	je     f0102013 <mem_init+0xbd7>
f0101ffa:	68 f8 5a 10 f0       	push   $0xf0105af8
f0101fff:	68 98 4f 10 f0       	push   $0xf0104f98
f0102004:	68 63 04 00 00       	push   $0x463
f0102009:	68 e9 5d 10 f0       	push   $0xf0105de9
f010200e:	e8 39 e0 ff ff       	call   f010004c <_panic>
	// ... and ref counts should reflect this
	assert(pp1->pp_ref == 2);
f0102013:	66 83 7e 04 02       	cmpw   $0x2,0x4(%esi)
f0102018:	74 19                	je     f0102033 <mem_init+0xbf7>
f010201a:	68 eb 60 10 f0       	push   $0xf01060eb
f010201f:	68 98 4f 10 f0       	push   $0xf0104f98
f0102024:	68 65 04 00 00       	push   $0x465
f0102029:	68 e9 5d 10 f0       	push   $0xf0105de9
f010202e:	e8 19 e0 ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 0);
f0102033:	66 83 7b 04 00       	cmpw   $0x0,0x4(%ebx)
f0102038:	74 19                	je     f0102053 <mem_init+0xc17>
f010203a:	68 fc 60 10 f0       	push   $0xf01060fc
f010203f:	68 98 4f 10 f0       	push   $0xf0104f98
f0102044:	68 66 04 00 00       	push   $0x466
f0102049:	68 e9 5d 10 f0       	push   $0xf0105de9
f010204e:	e8 f9 df ff ff       	call   f010004c <_panic>

	// pp2 should be returned by page_alloc
	assert((pp = page_alloc(0)) && pp == pp2);
f0102053:	83 ec 0c             	sub    $0xc,%esp
f0102056:	6a 00                	push   $0x0
f0102058:	e8 a0 ef ff ff       	call   f0100ffd <page_alloc>
f010205d:	83 c4 10             	add    $0x10,%esp
f0102060:	85 c0                	test   %eax,%eax
f0102062:	74 04                	je     f0102068 <mem_init+0xc2c>
f0102064:	39 c3                	cmp    %eax,%ebx
f0102066:	74 19                	je     f0102081 <mem_init+0xc45>
f0102068:	68 28 5b 10 f0       	push   $0xf0105b28
f010206d:	68 98 4f 10 f0       	push   $0xf0104f98
f0102072:	68 69 04 00 00       	push   $0x469
f0102077:	68 e9 5d 10 f0       	push   $0xf0105de9
f010207c:	e8 cb df ff ff       	call   f010004c <_panic>

	// unmapping pp1 at 0 should keep pp1 at PGSIZE
	page_remove(kern_pgdir, 0x0);
f0102081:	83 ec 08             	sub    $0x8,%esp
f0102084:	6a 00                	push   $0x0
f0102086:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f010208c:	e8 ac f2 ff ff       	call   f010133d <page_remove>
	assert(check_va2pa(kern_pgdir, 0x0) == ~0);
f0102091:	8b 3d 48 e4 18 f0    	mov    0xf018e448,%edi
f0102097:	ba 00 00 00 00       	mov    $0x0,%edx
f010209c:	89 f8                	mov    %edi,%eax
f010209e:	e8 bc ea ff ff       	call   f0100b5f <check_va2pa>
f01020a3:	83 c4 10             	add    $0x10,%esp
f01020a6:	83 f8 ff             	cmp    $0xffffffff,%eax
f01020a9:	74 19                	je     f01020c4 <mem_init+0xc88>
f01020ab:	68 4c 5b 10 f0       	push   $0xf0105b4c
f01020b0:	68 98 4f 10 f0       	push   $0xf0104f98
f01020b5:	68 6d 04 00 00       	push   $0x46d
f01020ba:	68 e9 5d 10 f0       	push   $0xf0105de9
f01020bf:	e8 88 df ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp1));
f01020c4:	ba 00 10 00 00       	mov    $0x1000,%edx
f01020c9:	89 f8                	mov    %edi,%eax
f01020cb:	e8 8f ea ff ff       	call   f0100b5f <check_va2pa>
f01020d0:	89 f2                	mov    %esi,%edx
f01020d2:	2b 15 4c e4 18 f0    	sub    0xf018e44c,%edx
f01020d8:	c1 fa 03             	sar    $0x3,%edx
f01020db:	c1 e2 0c             	shl    $0xc,%edx
f01020de:	39 d0                	cmp    %edx,%eax
f01020e0:	74 19                	je     f01020fb <mem_init+0xcbf>
f01020e2:	68 f8 5a 10 f0       	push   $0xf0105af8
f01020e7:	68 98 4f 10 f0       	push   $0xf0104f98
f01020ec:	68 6e 04 00 00       	push   $0x46e
f01020f1:	68 e9 5d 10 f0       	push   $0xf0105de9
f01020f6:	e8 51 df ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref == 1);
f01020fb:	66 83 7e 04 01       	cmpw   $0x1,0x4(%esi)
f0102100:	74 19                	je     f010211b <mem_init+0xcdf>
f0102102:	68 a2 60 10 f0       	push   $0xf01060a2
f0102107:	68 98 4f 10 f0       	push   $0xf0104f98
f010210c:	68 6f 04 00 00       	push   $0x46f
f0102111:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102116:	e8 31 df ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 0);
f010211b:	66 83 7b 04 00       	cmpw   $0x0,0x4(%ebx)
f0102120:	74 19                	je     f010213b <mem_init+0xcff>
f0102122:	68 fc 60 10 f0       	push   $0xf01060fc
f0102127:	68 98 4f 10 f0       	push   $0xf0104f98
f010212c:	68 70 04 00 00       	push   $0x470
f0102131:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102136:	e8 11 df ff ff       	call   f010004c <_panic>

	// test re-inserting pp1 at PGSIZE
	assert(page_insert(kern_pgdir, pp1, (void*) PGSIZE, 0) == 0);
f010213b:	6a 00                	push   $0x0
f010213d:	68 00 10 00 00       	push   $0x1000
f0102142:	56                   	push   %esi
f0102143:	57                   	push   %edi
f0102144:	e8 8d f2 ff ff       	call   f01013d6 <page_insert>
f0102149:	83 c4 10             	add    $0x10,%esp
f010214c:	85 c0                	test   %eax,%eax
f010214e:	74 19                	je     f0102169 <mem_init+0xd2d>
f0102150:	68 70 5b 10 f0       	push   $0xf0105b70
f0102155:	68 98 4f 10 f0       	push   $0xf0104f98
f010215a:	68 73 04 00 00       	push   $0x473
f010215f:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102164:	e8 e3 de ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref);
f0102169:	66 83 7e 04 00       	cmpw   $0x0,0x4(%esi)
f010216e:	75 19                	jne    f0102189 <mem_init+0xd4d>
f0102170:	68 0d 61 10 f0       	push   $0xf010610d
f0102175:	68 98 4f 10 f0       	push   $0xf0104f98
f010217a:	68 74 04 00 00       	push   $0x474
f010217f:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102184:	e8 c3 de ff ff       	call   f010004c <_panic>
	assert(pp1->pp_link == NULL);
f0102189:	83 3e 00             	cmpl   $0x0,(%esi)
f010218c:	74 19                	je     f01021a7 <mem_init+0xd6b>
f010218e:	68 19 61 10 f0       	push   $0xf0106119
f0102193:	68 98 4f 10 f0       	push   $0xf0104f98
f0102198:	68 75 04 00 00       	push   $0x475
f010219d:	68 e9 5d 10 f0       	push   $0xf0105de9
f01021a2:	e8 a5 de ff ff       	call   f010004c <_panic>

	// unmapping pp1 at PGSIZE should free it
	page_remove(kern_pgdir, (void*) PGSIZE);
f01021a7:	83 ec 08             	sub    $0x8,%esp
f01021aa:	68 00 10 00 00       	push   $0x1000
f01021af:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f01021b5:	e8 83 f1 ff ff       	call   f010133d <page_remove>
	assert(check_va2pa(kern_pgdir, 0x0) == ~0);
f01021ba:	8b 3d 48 e4 18 f0    	mov    0xf018e448,%edi
f01021c0:	ba 00 00 00 00       	mov    $0x0,%edx
f01021c5:	89 f8                	mov    %edi,%eax
f01021c7:	e8 93 e9 ff ff       	call   f0100b5f <check_va2pa>
f01021cc:	83 c4 10             	add    $0x10,%esp
f01021cf:	83 f8 ff             	cmp    $0xffffffff,%eax
f01021d2:	74 19                	je     f01021ed <mem_init+0xdb1>
f01021d4:	68 4c 5b 10 f0       	push   $0xf0105b4c
f01021d9:	68 98 4f 10 f0       	push   $0xf0104f98
f01021de:	68 79 04 00 00       	push   $0x479
f01021e3:	68 e9 5d 10 f0       	push   $0xf0105de9
f01021e8:	e8 5f de ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == ~0);
f01021ed:	ba 00 10 00 00       	mov    $0x1000,%edx
f01021f2:	89 f8                	mov    %edi,%eax
f01021f4:	e8 66 e9 ff ff       	call   f0100b5f <check_va2pa>
f01021f9:	83 f8 ff             	cmp    $0xffffffff,%eax
f01021fc:	74 19                	je     f0102217 <mem_init+0xddb>
f01021fe:	68 a8 5b 10 f0       	push   $0xf0105ba8
f0102203:	68 98 4f 10 f0       	push   $0xf0104f98
f0102208:	68 7a 04 00 00       	push   $0x47a
f010220d:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102212:	e8 35 de ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref == 0);
f0102217:	66 83 7e 04 00       	cmpw   $0x0,0x4(%esi)
f010221c:	74 19                	je     f0102237 <mem_init+0xdfb>
f010221e:	68 2e 61 10 f0       	push   $0xf010612e
f0102223:	68 98 4f 10 f0       	push   $0xf0104f98
f0102228:	68 7b 04 00 00       	push   $0x47b
f010222d:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102232:	e8 15 de ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 0);
f0102237:	66 83 7b 04 00       	cmpw   $0x0,0x4(%ebx)
f010223c:	74 19                	je     f0102257 <mem_init+0xe1b>
f010223e:	68 fc 60 10 f0       	push   $0xf01060fc
f0102243:	68 98 4f 10 f0       	push   $0xf0104f98
f0102248:	68 7c 04 00 00       	push   $0x47c
f010224d:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102252:	e8 f5 dd ff ff       	call   f010004c <_panic>

	// so it should be returned by page_alloc
	assert((pp = page_alloc(0)) && pp == pp1);
f0102257:	83 ec 0c             	sub    $0xc,%esp
f010225a:	6a 00                	push   $0x0
f010225c:	e8 9c ed ff ff       	call   f0100ffd <page_alloc>
f0102261:	83 c4 10             	add    $0x10,%esp
f0102264:	85 c0                	test   %eax,%eax
f0102266:	74 04                	je     f010226c <mem_init+0xe30>
f0102268:	39 c6                	cmp    %eax,%esi
f010226a:	74 19                	je     f0102285 <mem_init+0xe49>
f010226c:	68 d0 5b 10 f0       	push   $0xf0105bd0
f0102271:	68 98 4f 10 f0       	push   $0xf0104f98
f0102276:	68 7f 04 00 00       	push   $0x47f
f010227b:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102280:	e8 c7 dd ff ff       	call   f010004c <_panic>

	// should be no free memory
	assert(!page_alloc(0));
f0102285:	83 ec 0c             	sub    $0xc,%esp
f0102288:	6a 00                	push   $0x0
f010228a:	e8 6e ed ff ff       	call   f0100ffd <page_alloc>
f010228f:	83 c4 10             	add    $0x10,%esp
f0102292:	85 c0                	test   %eax,%eax
f0102294:	74 19                	je     f01022af <mem_init+0xe73>
f0102296:	68 50 60 10 f0       	push   $0xf0106050
f010229b:	68 98 4f 10 f0       	push   $0xf0104f98
f01022a0:	68 82 04 00 00       	push   $0x482
f01022a5:	68 e9 5d 10 f0       	push   $0xf0105de9
f01022aa:	e8 9d dd ff ff       	call   f010004c <_panic>

	// forcibly take pp0 back
	assert(PTE_ADDR(kern_pgdir[0]) == page2pa(pp0));
f01022af:	8b 0d 48 e4 18 f0    	mov    0xf018e448,%ecx
f01022b5:	8b 11                	mov    (%ecx),%edx
f01022b7:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f01022bd:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01022c0:	2b 05 4c e4 18 f0    	sub    0xf018e44c,%eax
f01022c6:	c1 f8 03             	sar    $0x3,%eax
f01022c9:	c1 e0 0c             	shl    $0xc,%eax
f01022cc:	39 c2                	cmp    %eax,%edx
f01022ce:	74 19                	je     f01022e9 <mem_init+0xead>
f01022d0:	68 74 58 10 f0       	push   $0xf0105874
f01022d5:	68 98 4f 10 f0       	push   $0xf0104f98
f01022da:	68 85 04 00 00       	push   $0x485
f01022df:	68 e9 5d 10 f0       	push   $0xf0105de9
f01022e4:	e8 63 dd ff ff       	call   f010004c <_panic>
	kern_pgdir[0] = 0;
f01022e9:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	assert(pp0->pp_ref == 1);
f01022ef:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01022f2:	66 83 78 04 01       	cmpw   $0x1,0x4(%eax)
f01022f7:	74 19                	je     f0102312 <mem_init+0xed6>
f01022f9:	68 b3 60 10 f0       	push   $0xf01060b3
f01022fe:	68 98 4f 10 f0       	push   $0xf0104f98
f0102303:	68 87 04 00 00       	push   $0x487
f0102308:	68 e9 5d 10 f0       	push   $0xf0105de9
f010230d:	e8 3a dd ff ff       	call   f010004c <_panic>
	pp0->pp_ref = 0;
f0102312:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0102315:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)

	// check pointer arithmetic in pgdir_walk
	page_free(pp0);
f010231b:	83 ec 0c             	sub    $0xc,%esp
f010231e:	50                   	push   %eax
f010231f:	e8 4f ed ff ff       	call   f0101073 <page_free>
	va = (void*)(PGSIZE * NPDENTRIES + PGSIZE);
	ptep = pgdir_walk(kern_pgdir, va, 1);
f0102324:	83 c4 0c             	add    $0xc,%esp
f0102327:	6a 01                	push   $0x1
f0102329:	68 00 10 40 00       	push   $0x401000
f010232e:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0102334:	e8 bb ed ff ff       	call   f01010f4 <pgdir_walk>
f0102339:	89 45 cc             	mov    %eax,-0x34(%ebp)
f010233c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	ptep1 = (pte_t *) KADDR(PTE_ADDR(kern_pgdir[PDX(va)]));
f010233f:	8b 0d 48 e4 18 f0    	mov    0xf018e448,%ecx
f0102345:	8b 51 04             	mov    0x4(%ecx),%edx
f0102348:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010234e:	8b 3d 44 e4 18 f0    	mov    0xf018e444,%edi
f0102354:	89 d0                	mov    %edx,%eax
f0102356:	c1 e8 0c             	shr    $0xc,%eax
f0102359:	83 c4 10             	add    $0x10,%esp
f010235c:	39 f8                	cmp    %edi,%eax
f010235e:	72 15                	jb     f0102375 <mem_init+0xf39>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102360:	52                   	push   %edx
f0102361:	68 30 56 10 f0       	push   $0xf0105630
f0102366:	68 8e 04 00 00       	push   $0x48e
f010236b:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102370:	e8 d7 dc ff ff       	call   f010004c <_panic>
	assert(ptep == ptep1 + PTX(va));
f0102375:	81 ea fc ff ff 0f    	sub    $0xffffffc,%edx
f010237b:	39 55 cc             	cmp    %edx,-0x34(%ebp)
f010237e:	74 19                	je     f0102399 <mem_init+0xf5d>
f0102380:	68 3f 61 10 f0       	push   $0xf010613f
f0102385:	68 98 4f 10 f0       	push   $0xf0104f98
f010238a:	68 8f 04 00 00       	push   $0x48f
f010238f:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102394:	e8 b3 dc ff ff       	call   f010004c <_panic>
	kern_pgdir[PDX(va)] = 0;
f0102399:	c7 41 04 00 00 00 00 	movl   $0x0,0x4(%ecx)
	pp0->pp_ref = 0;
f01023a0:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01023a3:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f01023a9:	2b 05 4c e4 18 f0    	sub    0xf018e44c,%eax
f01023af:	c1 f8 03             	sar    $0x3,%eax
f01023b2:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01023b5:	89 c2                	mov    %eax,%edx
f01023b7:	c1 ea 0c             	shr    $0xc,%edx
f01023ba:	39 d7                	cmp    %edx,%edi
f01023bc:	77 12                	ja     f01023d0 <mem_init+0xf94>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01023be:	50                   	push   %eax
f01023bf:	68 30 56 10 f0       	push   $0xf0105630
f01023c4:	6a 56                	push   $0x56
f01023c6:	68 f5 5d 10 f0       	push   $0xf0105df5
f01023cb:	e8 7c dc ff ff       	call   f010004c <_panic>

	// check that new page tables get cleared
	memset(page2kva(pp0), 0xFF, PGSIZE);
f01023d0:	83 ec 04             	sub    $0x4,%esp
f01023d3:	68 00 10 00 00       	push   $0x1000
f01023d8:	68 ff 00 00 00       	push   $0xff
f01023dd:	2d 00 00 00 10       	sub    $0x10000000,%eax
f01023e2:	50                   	push   %eax
f01023e3:	e8 30 27 00 00       	call   f0104b18 <memset>
	page_free(pp0);
f01023e8:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f01023eb:	89 3c 24             	mov    %edi,(%esp)
f01023ee:	e8 80 ec ff ff       	call   f0101073 <page_free>
	pgdir_walk(kern_pgdir, 0x0, 1);
f01023f3:	83 c4 0c             	add    $0xc,%esp
f01023f6:	6a 01                	push   $0x1
f01023f8:	6a 00                	push   $0x0
f01023fa:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0102400:	e8 ef ec ff ff       	call   f01010f4 <pgdir_walk>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0102405:	89 fa                	mov    %edi,%edx
f0102407:	2b 15 4c e4 18 f0    	sub    0xf018e44c,%edx
f010240d:	c1 fa 03             	sar    $0x3,%edx
f0102410:	c1 e2 0c             	shl    $0xc,%edx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102413:	89 d0                	mov    %edx,%eax
f0102415:	c1 e8 0c             	shr    $0xc,%eax
f0102418:	83 c4 10             	add    $0x10,%esp
f010241b:	3b 05 44 e4 18 f0    	cmp    0xf018e444,%eax
f0102421:	72 12                	jb     f0102435 <mem_init+0xff9>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102423:	52                   	push   %edx
f0102424:	68 30 56 10 f0       	push   $0xf0105630
f0102429:	6a 56                	push   $0x56
f010242b:	68 f5 5d 10 f0       	push   $0xf0105df5
f0102430:	e8 17 dc ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f0102435:	8d 82 00 00 00 f0    	lea    -0x10000000(%edx),%eax
	ptep = (pte_t *) page2kva(pp0);
f010243b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f010243e:	81 ea 00 f0 ff 0f    	sub    $0xffff000,%edx
	for(i=0; i<NPTENTRIES; i++)
		assert((ptep[i] & PTE_P) == 0);
f0102444:	f6 00 01             	testb  $0x1,(%eax)
f0102447:	74 19                	je     f0102462 <mem_init+0x1026>
f0102449:	68 57 61 10 f0       	push   $0xf0106157
f010244e:	68 98 4f 10 f0       	push   $0xf0104f98
f0102453:	68 99 04 00 00       	push   $0x499
f0102458:	68 e9 5d 10 f0       	push   $0xf0105de9
f010245d:	e8 ea db ff ff       	call   f010004c <_panic>
f0102462:	83 c0 04             	add    $0x4,%eax
	// check that new page tables get cleared
	memset(page2kva(pp0), 0xFF, PGSIZE);
	page_free(pp0);
	pgdir_walk(kern_pgdir, 0x0, 1);
	ptep = (pte_t *) page2kva(pp0);
	for(i=0; i<NPTENTRIES; i++)
f0102465:	39 d0                	cmp    %edx,%eax
f0102467:	75 db                	jne    f0102444 <mem_init+0x1008>
		assert((ptep[i] & PTE_P) == 0);
	kern_pgdir[0] = 0;
f0102469:	a1 48 e4 18 f0       	mov    0xf018e448,%eax
f010246e:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	pp0->pp_ref = 0;
f0102474:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0102477:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)

	// give free list back
	page_free_list = fl;
f010247d:	8b 7d d0             	mov    -0x30(%ebp),%edi
f0102480:	89 3d 5c d2 18 f0    	mov    %edi,0xf018d25c

	// free the pages we took
	page_free(pp0);
f0102486:	83 ec 0c             	sub    $0xc,%esp
f0102489:	50                   	push   %eax
f010248a:	e8 e4 eb ff ff       	call   f0101073 <page_free>
	page_free(pp1);
f010248f:	89 34 24             	mov    %esi,(%esp)
f0102492:	e8 dc eb ff ff       	call   f0101073 <page_free>
	page_free(pp2);
f0102497:	89 1c 24             	mov    %ebx,(%esp)
f010249a:	e8 d4 eb ff ff       	call   f0101073 <page_free>

	cprintf("check_page() succeeded!\n");
f010249f:	c7 04 24 6e 61 10 f0 	movl   $0xf010616e,(%esp)
f01024a6:	e8 1e 0e 00 00       	call   f01032c9 <cprintf>
	//    - the new image at UPAGES -- kernel R, user R
	//      (ie. perm = PTE_U | PTE_P)
	//    - pages itself -- kernel RW, user NONE
	// Your code goes here:
	size_t pags_Size = npages * sizeof(struct PageInfo);
	physaddr_t pages_PA = PADDR(pages);//page2pa(pages);
f01024ab:	a1 4c e4 18 f0       	mov    0xf018e44c,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01024b0:	83 c4 10             	add    $0x10,%esp
f01024b3:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01024b8:	77 15                	ja     f01024cf <mem_init+0x1093>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01024ba:	50                   	push   %eax
f01024bb:	68 78 57 10 f0       	push   $0xf0105778
f01024c0:	68 cb 00 00 00       	push   $0xcb
f01024c5:	68 e9 5d 10 f0       	push   $0xf0105de9
f01024ca:	e8 7d db ff ff       	call   f010004c <_panic>
//boot_map_region(kern_pgdir, UPAGES, PTSIZE, pages_PA, PTE_U | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, UPAGES, PTSIZE, PADDR(pages), PTE_U);
f01024cf:	83 ec 08             	sub    $0x8,%esp
f01024d2:	6a 04                	push   $0x4
f01024d4:	05 00 00 00 10       	add    $0x10000000,%eax
f01024d9:	50                   	push   %eax
f01024da:	b9 00 00 40 00       	mov    $0x400000,%ecx
f01024df:	ba 00 00 00 ef       	mov    $0xef000000,%edx
f01024e4:	a1 48 e4 18 f0       	mov    0xf018e448,%eax
f01024e9:	e8 ea ec ff ff       	call   f01011d8 <boot_map_region>

	size_t envs_Size = NENV * sizeof(struct Env);
	physaddr_t envs_PA = PADDR(envs);
f01024ee:	a1 64 d2 18 f0       	mov    0xf018d264,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01024f3:	83 c4 10             	add    $0x10,%esp
f01024f6:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01024fb:	77 15                	ja     f0102512 <mem_init+0x10d6>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01024fd:	50                   	push   %eax
f01024fe:	68 78 57 10 f0       	push   $0xf0105778
f0102503:	68 d1 00 00 00       	push   $0xd1
f0102508:	68 e9 5d 10 f0       	push   $0xf0105de9
f010250d:	e8 3a db ff ff       	call   f010004c <_panic>
	//the memory backing envs should also be mapped user read-only at UENVS
	//boot_map_region(kern_pgdir, UENVS, envs_Size, envs_PA, PTE_U | PTE_P);
//PTSIZE
//boot_map_region(kern_pgdir, UENVS, PTSIZE, envs_PA, PTE_U | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, UENVS, PTSIZE, PADDR(envs), PTE_U);
f0102512:	83 ec 08             	sub    $0x8,%esp
f0102515:	6a 04                	push   $0x4
f0102517:	05 00 00 00 10       	add    $0x10000000,%eax
f010251c:	50                   	push   %eax
f010251d:	b9 00 00 40 00       	mov    $0x400000,%ecx
f0102522:	ba 00 00 c0 ee       	mov    $0xeec00000,%edx
f0102527:	a1 48 e4 18 f0       	mov    0xf018e448,%eax
f010252c:	e8 a7 ec ff ff       	call   f01011d8 <boot_map_region>
	cprintf("In mem_init(), UENVS is %p, and envs is: %p\n\n\n\n", UENVS, envs);
f0102531:	83 c4 0c             	add    $0xc,%esp
f0102534:	ff 35 64 d2 18 f0    	pushl  0xf018d264
f010253a:	68 00 00 c0 ee       	push   $0xeec00000
f010253f:	68 f4 5b 10 f0       	push   $0xf0105bf4
f0102544:	e8 80 0d 00 00       	call   f01032c9 <cprintf>
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102549:	83 c4 10             	add    $0x10,%esp
f010254c:	b8 00 30 11 f0       	mov    $0xf0113000,%eax
f0102551:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0102556:	77 15                	ja     f010256d <mem_init+0x1131>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102558:	50                   	push   %eax
f0102559:	68 78 57 10 f0       	push   $0xf0105778
f010255e:	68 f6 00 00 00       	push   $0xf6
f0102563:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102568:	e8 df da ff ff       	call   f010004c <_panic>
	  #### (unmapped)
	  KSTACKTOP - PTSIZE
	*/
//boot_map_region(kern_pgdir, (KSTACKTOP-KSTKSIZE), KSTKSIZE, ((physaddr_t)PADDR(bootstack)), PTE_W | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, KSTACKTOP - KSTKSIZE, KSTKSIZE, PADDR(bootstack), PTE_W);  
f010256d:	83 ec 08             	sub    $0x8,%esp
f0102570:	6a 02                	push   $0x2
f0102572:	68 00 30 11 00       	push   $0x113000
f0102577:	b9 00 80 00 00       	mov    $0x8000,%ecx
f010257c:	ba 00 80 ff ef       	mov    $0xefff8000,%edx
f0102581:	a1 48 e4 18 f0       	mov    0xf018e448,%eax
f0102586:	e8 4d ec ff ff       	call   f01011d8 <boot_map_region>
	// we just set up the mapping anyway.
	// Permissions: kernel RW, user NONE
	// Your code goes here:
//boot_map_region(kern_pgdir, KERNBASE, (0xFFFFFFFF - KERNBASE), 0, PTE_W | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, KERNBASE, -KERNBASE/* ~KERNBASE + 1 */, 0, PTE_W);
f010258b:	83 c4 08             	add    $0x8,%esp
f010258e:	6a 02                	push   $0x2
f0102590:	6a 00                	push   $0x0
f0102592:	b9 00 00 00 10       	mov    $0x10000000,%ecx
f0102597:	ba 00 00 00 f0       	mov    $0xf0000000,%edx
f010259c:	a1 48 e4 18 f0       	mov    0xf018e448,%eax
f01025a1:	e8 32 ec ff ff       	call   f01011d8 <boot_map_region>
check_kern_pgdir(void)
{
	uint32_t i, n;
	pde_t *pgdir;

	pgdir = kern_pgdir;
f01025a6:	8b 35 48 e4 18 f0    	mov    0xf018e448,%esi

	// check pages array
	n = ROUNDUP(npages*sizeof(struct PageInfo), PGSIZE);
f01025ac:	a1 44 e4 18 f0       	mov    0xf018e444,%eax
f01025b1:	89 45 c8             	mov    %eax,-0x38(%ebp)
f01025b4:	8d 04 c5 ff 0f 00 00 	lea    0xfff(,%eax,8),%eax
f01025bb:	25 00 f0 ff ff       	and    $0xfffff000,%eax
	for (i = 0; i < n; i += PGSIZE){
		assert(check_va2pa(pgdir, UPAGES + i) == PADDR(pages) + i);
f01025c0:	8b 3d 4c e4 18 f0    	mov    0xf018e44c,%edi
f01025c6:	89 7d c4             	mov    %edi,-0x3c(%ebp)
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01025c9:	89 7d d0             	mov    %edi,-0x30(%ebp)
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
	return (physaddr_t)kva - KERNBASE;
f01025cc:	81 c7 00 00 00 10    	add    $0x10000000,%edi
f01025d2:	89 7d cc             	mov    %edi,-0x34(%ebp)
//<<<<<<< HEAD

	// check envs array (new test for lab 3)
	n = ROUNDUP(NENV*sizeof(struct Env), PGSIZE);
	for (i = 0; i < n; i += PGSIZE)
		assert(check_va2pa(pgdir, UENVS + i) == PADDR(envs) + i);
f01025d5:	8b 3d 64 d2 18 f0    	mov    0xf018d264,%edi
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01025db:	89 7d d4             	mov    %edi,-0x2c(%ebp)
f01025de:	83 c4 10             	add    $0x10,%esp

	pgdir = kern_pgdir;

	// check pages array
	n = ROUNDUP(npages*sizeof(struct PageInfo), PGSIZE);
	for (i = 0; i < n; i += PGSIZE){
f01025e1:	bb 00 00 00 00       	mov    $0x0,%ebx
f01025e6:	e9 b8 00 00 00       	jmp    f01026a3 <mem_init+0x1267>
		assert(check_va2pa(pgdir, UPAGES + i) == PADDR(pages) + i);
f01025eb:	8d 93 00 00 00 ef    	lea    -0x11000000(%ebx),%edx
f01025f1:	89 f0                	mov    %esi,%eax
f01025f3:	e8 67 e5 ff ff       	call   f0100b5f <check_va2pa>
f01025f8:	81 7d d0 ff ff ff ef 	cmpl   $0xefffffff,-0x30(%ebp)
f01025ff:	77 17                	ja     f0102618 <mem_init+0x11dc>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102601:	ff 75 c4             	pushl  -0x3c(%ebp)
f0102604:	68 78 57 10 f0       	push   $0xf0105778
f0102609:	68 b7 03 00 00       	push   $0x3b7
f010260e:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102613:	e8 34 da ff ff       	call   f010004c <_panic>
f0102618:	03 5d cc             	add    -0x34(%ebp),%ebx
f010261b:	39 d8                	cmp    %ebx,%eax
f010261d:	74 19                	je     f0102638 <mem_init+0x11fc>
f010261f:	68 24 5c 10 f0       	push   $0xf0105c24
f0102624:	68 98 4f 10 f0       	push   $0xf0104f98
f0102629:	68 b7 03 00 00       	push   $0x3b7
f010262e:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102633:	e8 14 da ff ff       	call   f010004c <_panic>
f0102638:	bb 00 00 00 00       	mov    $0x0,%ebx
f010263d:	eb 02                	jmp    f0102641 <mem_init+0x1205>
//<<<<<<< HEAD

	// check envs array (new test for lab 3)
	n = ROUNDUP(NENV*sizeof(struct Env), PGSIZE);
	for (i = 0; i < n; i += PGSIZE)
f010263f:	89 c3                	mov    %eax,%ebx
		assert(check_va2pa(pgdir, UENVS + i) == PADDR(envs) + i);
f0102641:	8d 93 00 00 c0 ee    	lea    -0x11400000(%ebx),%edx
f0102647:	89 f0                	mov    %esi,%eax
f0102649:	e8 11 e5 ff ff       	call   f0100b5f <check_va2pa>
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f010264e:	81 7d d4 ff ff ff ef 	cmpl   $0xefffffff,-0x2c(%ebp)
f0102655:	77 15                	ja     f010266c <mem_init+0x1230>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102657:	57                   	push   %edi
f0102658:	68 78 57 10 f0       	push   $0xf0105778
f010265d:	68 bd 03 00 00       	push   $0x3bd
f0102662:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102667:	e8 e0 d9 ff ff       	call   f010004c <_panic>
f010266c:	8d 94 1f 00 00 00 10 	lea    0x10000000(%edi,%ebx,1),%edx
f0102673:	39 d0                	cmp    %edx,%eax
f0102675:	74 19                	je     f0102690 <mem_init+0x1254>
f0102677:	68 58 5c 10 f0       	push   $0xf0105c58
f010267c:	68 98 4f 10 f0       	push   $0xf0104f98
f0102681:	68 bd 03 00 00       	push   $0x3bd
f0102686:	68 e9 5d 10 f0       	push   $0xf0105de9
f010268b:	e8 bc d9 ff ff       	call   f010004c <_panic>
		assert(check_va2pa(pgdir, UPAGES + i) == PADDR(pages) + i);
//<<<<<<< HEAD

	// check envs array (new test for lab 3)
	n = ROUNDUP(NENV*sizeof(struct Env), PGSIZE);
	for (i = 0; i < n; i += PGSIZE)
f0102690:	8d 83 00 10 00 00    	lea    0x1000(%ebx),%eax
f0102696:	3d 00 80 01 00       	cmp    $0x18000,%eax
f010269b:	75 a2                	jne    f010263f <mem_init+0x1203>

	pgdir = kern_pgdir;

	// check pages array
	n = ROUNDUP(npages*sizeof(struct PageInfo), PGSIZE);
	for (i = 0; i < n; i += PGSIZE){
f010269d:	81 c3 00 20 00 00    	add    $0x2000,%ebx
f01026a3:	39 c3                	cmp    %eax,%ebx
f01026a5:	0f 82 40 ff ff ff    	jb     f01025eb <mem_init+0x11af>
//=======
	}
//>>>>>>> lab2

	// check phys mem
	for (i = 0; i < npages * PGSIZE; i += PGSIZE)
f01026ab:	8b 7d c8             	mov    -0x38(%ebp),%edi
f01026ae:	c1 e7 0c             	shl    $0xc,%edi
f01026b1:	bb 00 00 00 00       	mov    $0x0,%ebx
f01026b6:	eb 30                	jmp    f01026e8 <mem_init+0x12ac>
		assert(check_va2pa(pgdir, KERNBASE + i) == i);
f01026b8:	8d 93 00 00 00 f0    	lea    -0x10000000(%ebx),%edx
f01026be:	89 f0                	mov    %esi,%eax
f01026c0:	e8 9a e4 ff ff       	call   f0100b5f <check_va2pa>
f01026c5:	39 c3                	cmp    %eax,%ebx
f01026c7:	74 19                	je     f01026e2 <mem_init+0x12a6>
f01026c9:	68 8c 5c 10 f0       	push   $0xf0105c8c
f01026ce:	68 98 4f 10 f0       	push   $0xf0104f98
f01026d3:	68 c4 03 00 00       	push   $0x3c4
f01026d8:	68 e9 5d 10 f0       	push   $0xf0105de9
f01026dd:	e8 6a d9 ff ff       	call   f010004c <_panic>
//=======
	}
//>>>>>>> lab2

	// check phys mem
	for (i = 0; i < npages * PGSIZE; i += PGSIZE)
f01026e2:	81 c3 00 10 00 00    	add    $0x1000,%ebx
f01026e8:	39 fb                	cmp    %edi,%ebx
f01026ea:	72 cc                	jb     f01026b8 <mem_init+0x127c>
f01026ec:	bb 00 80 ff ef       	mov    $0xefff8000,%ebx
		assert(check_va2pa(pgdir, KERNBASE + i) == i);

	// check kernel stack
	for (i = 0; i < KSTKSIZE; i += PGSIZE)
		assert(check_va2pa(pgdir, KSTACKTOP - KSTKSIZE + i) == PADDR(bootstack) + i);
f01026f1:	89 da                	mov    %ebx,%edx
f01026f3:	89 f0                	mov    %esi,%eax
f01026f5:	e8 65 e4 ff ff       	call   f0100b5f <check_va2pa>
f01026fa:	8d 93 00 b0 11 10    	lea    0x1011b000(%ebx),%edx
f0102700:	39 c2                	cmp    %eax,%edx
f0102702:	74 19                	je     f010271d <mem_init+0x12e1>
f0102704:	68 b4 5c 10 f0       	push   $0xf0105cb4
f0102709:	68 98 4f 10 f0       	push   $0xf0104f98
f010270e:	68 c8 03 00 00       	push   $0x3c8
f0102713:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102718:	e8 2f d9 ff ff       	call   f010004c <_panic>
f010271d:	81 c3 00 10 00 00    	add    $0x1000,%ebx
	// check phys mem
	for (i = 0; i < npages * PGSIZE; i += PGSIZE)
		assert(check_va2pa(pgdir, KERNBASE + i) == i);

	// check kernel stack
	for (i = 0; i < KSTKSIZE; i += PGSIZE)
f0102723:	81 fb 00 00 00 f0    	cmp    $0xf0000000,%ebx
f0102729:	75 c6                	jne    f01026f1 <mem_init+0x12b5>
		assert(check_va2pa(pgdir, KSTACKTOP - KSTKSIZE + i) == PADDR(bootstack) + i);
	assert(check_va2pa(pgdir, KSTACKTOP - PTSIZE) == ~0);
f010272b:	ba 00 00 c0 ef       	mov    $0xefc00000,%edx
f0102730:	89 f0                	mov    %esi,%eax
f0102732:	e8 28 e4 ff ff       	call   f0100b5f <check_va2pa>
f0102737:	83 f8 ff             	cmp    $0xffffffff,%eax
f010273a:	74 51                	je     f010278d <mem_init+0x1351>
f010273c:	68 fc 5c 10 f0       	push   $0xf0105cfc
f0102741:	68 98 4f 10 f0       	push   $0xf0104f98
f0102746:	68 c9 03 00 00       	push   $0x3c9
f010274b:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102750:	e8 f7 d8 ff ff       	call   f010004c <_panic>

	// check PDE permissions
	for (i = 0; i < NPDENTRIES; i++) {
		switch (i) {
f0102755:	3d bb 03 00 00       	cmp    $0x3bb,%eax
f010275a:	72 36                	jb     f0102792 <mem_init+0x1356>
f010275c:	3d bd 03 00 00       	cmp    $0x3bd,%eax
f0102761:	76 07                	jbe    f010276a <mem_init+0x132e>
f0102763:	3d bf 03 00 00       	cmp    $0x3bf,%eax
f0102768:	75 28                	jne    f0102792 <mem_init+0x1356>
		case PDX(UVPT):
		case PDX(KSTACKTOP-1):
		case PDX(UPAGES):
		case PDX(UENVS):
			assert(pgdir[i] & PTE_P);
f010276a:	f6 04 86 01          	testb  $0x1,(%esi,%eax,4)
f010276e:	0f 85 83 00 00 00    	jne    f01027f7 <mem_init+0x13bb>
f0102774:	68 87 61 10 f0       	push   $0xf0106187
f0102779:	68 98 4f 10 f0       	push   $0xf0104f98
f010277e:	68 d2 03 00 00       	push   $0x3d2
f0102783:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102788:	e8 bf d8 ff ff       	call   f010004c <_panic>
		assert(check_va2pa(pgdir, KERNBASE + i) == i);

	// check kernel stack
	for (i = 0; i < KSTKSIZE; i += PGSIZE)
		assert(check_va2pa(pgdir, KSTACKTOP - KSTKSIZE + i) == PADDR(bootstack) + i);
	assert(check_va2pa(pgdir, KSTACKTOP - PTSIZE) == ~0);
f010278d:	b8 00 00 00 00       	mov    $0x0,%eax
		case PDX(UPAGES):
		case PDX(UENVS):
			assert(pgdir[i] & PTE_P);
			break;
		default:
			if (i >= PDX(KERNBASE)) {
f0102792:	3d bf 03 00 00       	cmp    $0x3bf,%eax
f0102797:	76 3f                	jbe    f01027d8 <mem_init+0x139c>
				assert(pgdir[i] & PTE_P);
f0102799:	8b 14 86             	mov    (%esi,%eax,4),%edx
f010279c:	f6 c2 01             	test   $0x1,%dl
f010279f:	75 19                	jne    f01027ba <mem_init+0x137e>
f01027a1:	68 87 61 10 f0       	push   $0xf0106187
f01027a6:	68 98 4f 10 f0       	push   $0xf0104f98
f01027ab:	68 d6 03 00 00       	push   $0x3d6
f01027b0:	68 e9 5d 10 f0       	push   $0xf0105de9
f01027b5:	e8 92 d8 ff ff       	call   f010004c <_panic>
				assert(pgdir[i] & PTE_W);
f01027ba:	f6 c2 02             	test   $0x2,%dl
f01027bd:	75 38                	jne    f01027f7 <mem_init+0x13bb>
f01027bf:	68 98 61 10 f0       	push   $0xf0106198
f01027c4:	68 98 4f 10 f0       	push   $0xf0104f98
f01027c9:	68 d7 03 00 00       	push   $0x3d7
f01027ce:	68 e9 5d 10 f0       	push   $0xf0105de9
f01027d3:	e8 74 d8 ff ff       	call   f010004c <_panic>
			} else
				assert(pgdir[i] == 0);
f01027d8:	83 3c 86 00          	cmpl   $0x0,(%esi,%eax,4)
f01027dc:	74 19                	je     f01027f7 <mem_init+0x13bb>
f01027de:	68 a9 61 10 f0       	push   $0xf01061a9
f01027e3:	68 98 4f 10 f0       	push   $0xf0104f98
f01027e8:	68 d9 03 00 00       	push   $0x3d9
f01027ed:	68 e9 5d 10 f0       	push   $0xf0105de9
f01027f2:	e8 55 d8 ff ff       	call   f010004c <_panic>
	for (i = 0; i < KSTKSIZE; i += PGSIZE)
		assert(check_va2pa(pgdir, KSTACKTOP - KSTKSIZE + i) == PADDR(bootstack) + i);
	assert(check_va2pa(pgdir, KSTACKTOP - PTSIZE) == ~0);

	// check PDE permissions
	for (i = 0; i < NPDENTRIES; i++) {
f01027f7:	40                   	inc    %eax
f01027f8:	3d ff 03 00 00       	cmp    $0x3ff,%eax
f01027fd:	0f 86 52 ff ff ff    	jbe    f0102755 <mem_init+0x1319>
			} else
				assert(pgdir[i] == 0);
			break;
		}
	}
	cprintf("check_kern_pgdir() succeeded!\n");
f0102803:	83 ec 0c             	sub    $0xc,%esp
f0102806:	68 2c 5d 10 f0       	push   $0xf0105d2c
f010280b:	e8 b9 0a 00 00       	call   f01032c9 <cprintf>
	// somewhere between KERNBASE and KERNBASE+4MB right now, which is
	// mapped the same way by both page tables.
	//
	// If the machine reboots at this point, you've probably set up your
	// kern_pgdir wrong.
	lcr3(PADDR(kern_pgdir));
f0102810:	a1 48 e4 18 f0       	mov    0xf018e448,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102815:	83 c4 10             	add    $0x10,%esp
f0102818:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f010281d:	77 15                	ja     f0102834 <mem_init+0x13f8>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f010281f:	50                   	push   %eax
f0102820:	68 78 57 10 f0       	push   $0xf0105778
f0102825:	68 0f 01 00 00       	push   $0x10f
f010282a:	68 e9 5d 10 f0       	push   $0xf0105de9
f010282f:	e8 18 d8 ff ff       	call   f010004c <_panic>
}

static inline void
lcr3(uint32_t val)
{
	asm volatile("movl %0,%%cr3" : : "r" (val));
f0102834:	05 00 00 00 10       	add    $0x10000000,%eax
f0102839:	0f 22 d8             	mov    %eax,%cr3

	check_page_free_list(0);
f010283c:	b8 00 00 00 00       	mov    $0x0,%eax
f0102841:	e8 78 e3 ff ff       	call   f0100bbe <check_page_free_list>

static inline uint32_t
rcr0(void)
{
	uint32_t val;
	asm volatile("movl %%cr0,%0" : "=r" (val));
f0102846:	0f 20 c0             	mov    %cr0,%eax
f0102849:	83 e0 f3             	and    $0xfffffff3,%eax
}

static inline void
lcr0(uint32_t val)
{
	asm volatile("movl %0,%%cr0" : : "r" (val));
f010284c:	0d 23 00 05 80       	or     $0x80050023,%eax
f0102851:	0f 22 c0             	mov    %eax,%cr0
	uintptr_t va;
	int i;

	// check that we can read and write installed pages
	pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f0102854:	83 ec 0c             	sub    $0xc,%esp
f0102857:	6a 00                	push   $0x0
f0102859:	e8 9f e7 ff ff       	call   f0100ffd <page_alloc>
f010285e:	89 c3                	mov    %eax,%ebx
f0102860:	83 c4 10             	add    $0x10,%esp
f0102863:	85 c0                	test   %eax,%eax
f0102865:	75 19                	jne    f0102880 <mem_init+0x1444>
f0102867:	68 a5 5f 10 f0       	push   $0xf0105fa5
f010286c:	68 98 4f 10 f0       	push   $0xf0104f98
f0102871:	68 b4 04 00 00       	push   $0x4b4
f0102876:	68 e9 5d 10 f0       	push   $0xf0105de9
f010287b:	e8 cc d7 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f0102880:	83 ec 0c             	sub    $0xc,%esp
f0102883:	6a 00                	push   $0x0
f0102885:	e8 73 e7 ff ff       	call   f0100ffd <page_alloc>
f010288a:	89 c7                	mov    %eax,%edi
f010288c:	83 c4 10             	add    $0x10,%esp
f010288f:	85 c0                	test   %eax,%eax
f0102891:	75 19                	jne    f01028ac <mem_init+0x1470>
f0102893:	68 bb 5f 10 f0       	push   $0xf0105fbb
f0102898:	68 98 4f 10 f0       	push   $0xf0104f98
f010289d:	68 b5 04 00 00       	push   $0x4b5
f01028a2:	68 e9 5d 10 f0       	push   $0xf0105de9
f01028a7:	e8 a0 d7 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f01028ac:	83 ec 0c             	sub    $0xc,%esp
f01028af:	6a 00                	push   $0x0
f01028b1:	e8 47 e7 ff ff       	call   f0100ffd <page_alloc>
f01028b6:	89 c6                	mov    %eax,%esi
f01028b8:	83 c4 10             	add    $0x10,%esp
f01028bb:	85 c0                	test   %eax,%eax
f01028bd:	75 19                	jne    f01028d8 <mem_init+0x149c>
f01028bf:	68 d1 5f 10 f0       	push   $0xf0105fd1
f01028c4:	68 98 4f 10 f0       	push   $0xf0104f98
f01028c9:	68 b6 04 00 00       	push   $0x4b6
f01028ce:	68 e9 5d 10 f0       	push   $0xf0105de9
f01028d3:	e8 74 d7 ff ff       	call   f010004c <_panic>
	page_free(pp0);
f01028d8:	83 ec 0c             	sub    $0xc,%esp
f01028db:	53                   	push   %ebx
f01028dc:	e8 92 e7 ff ff       	call   f0101073 <page_free>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f01028e1:	89 f8                	mov    %edi,%eax
f01028e3:	2b 05 4c e4 18 f0    	sub    0xf018e44c,%eax
f01028e9:	c1 f8 03             	sar    $0x3,%eax
f01028ec:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01028ef:	89 c2                	mov    %eax,%edx
f01028f1:	c1 ea 0c             	shr    $0xc,%edx
f01028f4:	83 c4 10             	add    $0x10,%esp
f01028f7:	3b 15 44 e4 18 f0    	cmp    0xf018e444,%edx
f01028fd:	72 12                	jb     f0102911 <mem_init+0x14d5>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01028ff:	50                   	push   %eax
f0102900:	68 30 56 10 f0       	push   $0xf0105630
f0102905:	6a 56                	push   $0x56
f0102907:	68 f5 5d 10 f0       	push   $0xf0105df5
f010290c:	e8 3b d7 ff ff       	call   f010004c <_panic>
	memset(page2kva(pp1), 1, PGSIZE);
f0102911:	83 ec 04             	sub    $0x4,%esp
f0102914:	68 00 10 00 00       	push   $0x1000
f0102919:	6a 01                	push   $0x1
f010291b:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102920:	50                   	push   %eax
f0102921:	e8 f2 21 00 00       	call   f0104b18 <memset>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0102926:	89 f0                	mov    %esi,%eax
f0102928:	2b 05 4c e4 18 f0    	sub    0xf018e44c,%eax
f010292e:	c1 f8 03             	sar    $0x3,%eax
f0102931:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102934:	89 c2                	mov    %eax,%edx
f0102936:	c1 ea 0c             	shr    $0xc,%edx
f0102939:	83 c4 10             	add    $0x10,%esp
f010293c:	3b 15 44 e4 18 f0    	cmp    0xf018e444,%edx
f0102942:	72 12                	jb     f0102956 <mem_init+0x151a>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102944:	50                   	push   %eax
f0102945:	68 30 56 10 f0       	push   $0xf0105630
f010294a:	6a 56                	push   $0x56
f010294c:	68 f5 5d 10 f0       	push   $0xf0105df5
f0102951:	e8 f6 d6 ff ff       	call   f010004c <_panic>
	memset(page2kva(pp2), 2, PGSIZE);
f0102956:	83 ec 04             	sub    $0x4,%esp
f0102959:	68 00 10 00 00       	push   $0x1000
f010295e:	6a 02                	push   $0x2
f0102960:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102965:	50                   	push   %eax
f0102966:	e8 ad 21 00 00       	call   f0104b18 <memset>
	page_insert(kern_pgdir, pp1, (void*) PGSIZE, PTE_W);
f010296b:	6a 02                	push   $0x2
f010296d:	68 00 10 00 00       	push   $0x1000
f0102972:	57                   	push   %edi
f0102973:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0102979:	e8 58 ea ff ff       	call   f01013d6 <page_insert>
	assert(pp1->pp_ref == 1);
f010297e:	83 c4 20             	add    $0x20,%esp
f0102981:	66 83 7f 04 01       	cmpw   $0x1,0x4(%edi)
f0102986:	74 19                	je     f01029a1 <mem_init+0x1565>
f0102988:	68 a2 60 10 f0       	push   $0xf01060a2
f010298d:	68 98 4f 10 f0       	push   $0xf0104f98
f0102992:	68 bb 04 00 00       	push   $0x4bb
f0102997:	68 e9 5d 10 f0       	push   $0xf0105de9
f010299c:	e8 ab d6 ff ff       	call   f010004c <_panic>
	assert(*(uint32_t *)PGSIZE == 0x01010101U);
f01029a1:	81 3d 00 10 00 00 01 	cmpl   $0x1010101,0x1000
f01029a8:	01 01 01 
f01029ab:	74 19                	je     f01029c6 <mem_init+0x158a>
f01029ad:	68 4c 5d 10 f0       	push   $0xf0105d4c
f01029b2:	68 98 4f 10 f0       	push   $0xf0104f98
f01029b7:	68 bc 04 00 00       	push   $0x4bc
f01029bc:	68 e9 5d 10 f0       	push   $0xf0105de9
f01029c1:	e8 86 d6 ff ff       	call   f010004c <_panic>
	page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W);
f01029c6:	6a 02                	push   $0x2
f01029c8:	68 00 10 00 00       	push   $0x1000
f01029cd:	56                   	push   %esi
f01029ce:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f01029d4:	e8 fd e9 ff ff       	call   f01013d6 <page_insert>
	assert(*(uint32_t *)PGSIZE == 0x02020202U);
f01029d9:	83 c4 10             	add    $0x10,%esp
f01029dc:	81 3d 00 10 00 00 02 	cmpl   $0x2020202,0x1000
f01029e3:	02 02 02 
f01029e6:	74 19                	je     f0102a01 <mem_init+0x15c5>
f01029e8:	68 70 5d 10 f0       	push   $0xf0105d70
f01029ed:	68 98 4f 10 f0       	push   $0xf0104f98
f01029f2:	68 be 04 00 00       	push   $0x4be
f01029f7:	68 e9 5d 10 f0       	push   $0xf0105de9
f01029fc:	e8 4b d6 ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 1);
f0102a01:	66 83 7e 04 01       	cmpw   $0x1,0x4(%esi)
f0102a06:	74 19                	je     f0102a21 <mem_init+0x15e5>
f0102a08:	68 c4 60 10 f0       	push   $0xf01060c4
f0102a0d:	68 98 4f 10 f0       	push   $0xf0104f98
f0102a12:	68 bf 04 00 00       	push   $0x4bf
f0102a17:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102a1c:	e8 2b d6 ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref == 0);
f0102a21:	66 83 7f 04 00       	cmpw   $0x0,0x4(%edi)
f0102a26:	74 19                	je     f0102a41 <mem_init+0x1605>
f0102a28:	68 2e 61 10 f0       	push   $0xf010612e
f0102a2d:	68 98 4f 10 f0       	push   $0xf0104f98
f0102a32:	68 c0 04 00 00       	push   $0x4c0
f0102a37:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102a3c:	e8 0b d6 ff ff       	call   f010004c <_panic>
	*(uint32_t *)PGSIZE = 0x03030303U;
f0102a41:	c7 05 00 10 00 00 03 	movl   $0x3030303,0x1000
f0102a48:	03 03 03 
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0102a4b:	89 f0                	mov    %esi,%eax
f0102a4d:	2b 05 4c e4 18 f0    	sub    0xf018e44c,%eax
f0102a53:	c1 f8 03             	sar    $0x3,%eax
f0102a56:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102a59:	89 c2                	mov    %eax,%edx
f0102a5b:	c1 ea 0c             	shr    $0xc,%edx
f0102a5e:	3b 15 44 e4 18 f0    	cmp    0xf018e444,%edx
f0102a64:	72 12                	jb     f0102a78 <mem_init+0x163c>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102a66:	50                   	push   %eax
f0102a67:	68 30 56 10 f0       	push   $0xf0105630
f0102a6c:	6a 56                	push   $0x56
f0102a6e:	68 f5 5d 10 f0       	push   $0xf0105df5
f0102a73:	e8 d4 d5 ff ff       	call   f010004c <_panic>
	assert(*(uint32_t *)page2kva(pp2) == 0x03030303U);
f0102a78:	81 b8 00 00 00 f0 03 	cmpl   $0x3030303,-0x10000000(%eax)
f0102a7f:	03 03 03 
f0102a82:	74 19                	je     f0102a9d <mem_init+0x1661>
f0102a84:	68 94 5d 10 f0       	push   $0xf0105d94
f0102a89:	68 98 4f 10 f0       	push   $0xf0104f98
f0102a8e:	68 c2 04 00 00       	push   $0x4c2
f0102a93:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102a98:	e8 af d5 ff ff       	call   f010004c <_panic>
	page_remove(kern_pgdir, (void*) PGSIZE);
f0102a9d:	83 ec 08             	sub    $0x8,%esp
f0102aa0:	68 00 10 00 00       	push   $0x1000
f0102aa5:	ff 35 48 e4 18 f0    	pushl  0xf018e448
f0102aab:	e8 8d e8 ff ff       	call   f010133d <page_remove>
	assert(pp2->pp_ref == 0);
f0102ab0:	83 c4 10             	add    $0x10,%esp
f0102ab3:	66 83 7e 04 00       	cmpw   $0x0,0x4(%esi)
f0102ab8:	74 19                	je     f0102ad3 <mem_init+0x1697>
f0102aba:	68 fc 60 10 f0       	push   $0xf01060fc
f0102abf:	68 98 4f 10 f0       	push   $0xf0104f98
f0102ac4:	68 c4 04 00 00       	push   $0x4c4
f0102ac9:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102ace:	e8 79 d5 ff ff       	call   f010004c <_panic>

	// forcibly take pp0 back
	assert(PTE_ADDR(kern_pgdir[0]) == page2pa(pp0));
f0102ad3:	8b 0d 48 e4 18 f0    	mov    0xf018e448,%ecx
f0102ad9:	8b 11                	mov    (%ecx),%edx
f0102adb:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f0102ae1:	89 d8                	mov    %ebx,%eax
f0102ae3:	2b 05 4c e4 18 f0    	sub    0xf018e44c,%eax
f0102ae9:	c1 f8 03             	sar    $0x3,%eax
f0102aec:	c1 e0 0c             	shl    $0xc,%eax
f0102aef:	39 c2                	cmp    %eax,%edx
f0102af1:	74 19                	je     f0102b0c <mem_init+0x16d0>
f0102af3:	68 74 58 10 f0       	push   $0xf0105874
f0102af8:	68 98 4f 10 f0       	push   $0xf0104f98
f0102afd:	68 c7 04 00 00       	push   $0x4c7
f0102b02:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102b07:	e8 40 d5 ff ff       	call   f010004c <_panic>
	kern_pgdir[0] = 0;
f0102b0c:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	assert(pp0->pp_ref == 1);
f0102b12:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f0102b17:	74 19                	je     f0102b32 <mem_init+0x16f6>
f0102b19:	68 b3 60 10 f0       	push   $0xf01060b3
f0102b1e:	68 98 4f 10 f0       	push   $0xf0104f98
f0102b23:	68 c9 04 00 00       	push   $0x4c9
f0102b28:	68 e9 5d 10 f0       	push   $0xf0105de9
f0102b2d:	e8 1a d5 ff ff       	call   f010004c <_panic>
	pp0->pp_ref = 0;
f0102b32:	66 c7 43 04 00 00    	movw   $0x0,0x4(%ebx)

	// free the pages we took
	page_free(pp0);
f0102b38:	83 ec 0c             	sub    $0xc,%esp
f0102b3b:	53                   	push   %ebx
f0102b3c:	e8 32 e5 ff ff       	call   f0101073 <page_free>

	cprintf("check_page_installed_pgdir() succeeded!\n");
f0102b41:	c7 04 24 c0 5d 10 f0 	movl   $0xf0105dc0,(%esp)
f0102b48:	e8 7c 07 00 00       	call   f01032c9 <cprintf>
	cr0 &= ~(CR0_TS|CR0_EM);
	lcr0(cr0);

	// Some more checks, only possible after kern_pgdir is installed.
	check_page_installed_pgdir();
}
f0102b4d:	83 c4 10             	add    $0x10,%esp
f0102b50:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0102b53:	5b                   	pop    %ebx
f0102b54:	5e                   	pop    %esi
f0102b55:	5f                   	pop    %edi
f0102b56:	5d                   	pop    %ebp
f0102b57:	c3                   	ret    

f0102b58 <print_fl>:

}

void
print_fl()
{	
f0102b58:	55                   	push   %ebp
f0102b59:	89 e5                	mov    %esp,%ebp
f0102b5b:	53                   	push   %ebx
f0102b5c:	83 ec 10             	sub    $0x10,%esp
	struct PageInfo *pp = page_free_list;
f0102b5f:	8b 1d 5c d2 18 f0    	mov    0xf018d25c,%ebx
	cprintf("Freelist is: ");
f0102b65:	68 b7 61 10 f0       	push   $0xf01061b7
f0102b6a:	e8 5a 07 00 00       	call   f01032c9 <cprintf>
	while(pp != NULL){
f0102b6f:	83 c4 10             	add    $0x10,%esp
f0102b72:	eb 1e                	jmp    f0102b92 <print_fl+0x3a>
		cprintf("-> p%x ", (pp-pages));
f0102b74:	83 ec 08             	sub    $0x8,%esp
f0102b77:	89 d8                	mov    %ebx,%eax
f0102b79:	2b 05 4c e4 18 f0    	sub    0xf018e44c,%eax
f0102b7f:	c1 f8 03             	sar    $0x3,%eax
f0102b82:	50                   	push   %eax
f0102b83:	68 c5 61 10 f0       	push   $0xf01061c5
f0102b88:	e8 3c 07 00 00       	call   f01032c9 <cprintf>
		pp = pp->pp_link;
f0102b8d:	8b 1b                	mov    (%ebx),%ebx
f0102b8f:	83 c4 10             	add    $0x10,%esp
void
print_fl()
{	
	struct PageInfo *pp = page_free_list;
	cprintf("Freelist is: ");
	while(pp != NULL){
f0102b92:	85 db                	test   %ebx,%ebx
f0102b94:	75 de                	jne    f0102b74 <print_fl+0x1c>
		cprintf("-> p%x ", (pp-pages));
		pp = pp->pp_link;
	}
	cprintf("->end\n");
f0102b96:	83 ec 0c             	sub    $0xc,%esp
f0102b99:	68 cd 61 10 f0       	push   $0xf01061cd
f0102b9e:	e8 26 07 00 00       	call   f01032c9 <cprintf>
}
f0102ba3:	83 c4 10             	add    $0x10,%esp
f0102ba6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0102ba9:	c9                   	leave  
f0102baa:	c3                   	ret    

f0102bab <tlb_invalidate>:
// Invalidate a TLB entry, but only if the page tables being
// edited are the ones currently in use by the processor.
//
void
tlb_invalidate(pde_t *pgdir, void *va)
{
f0102bab:	55                   	push   %ebp
f0102bac:	89 e5                	mov    %esp,%ebp
}

static inline void
invlpg(void *addr)
{
	asm volatile("invlpg (%0)" : : "r" (addr) : "memory");
f0102bae:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102bb1:	0f 01 38             	invlpg (%eax)
	// Flush the entry only if we're modifying the current address space.
	// For now, there is only one address space, so always invalidate.
	invlpg(va);
}
f0102bb4:	5d                   	pop    %ebp
f0102bb5:	c3                   	ret    

f0102bb6 <user_mem_check>:
// Returns 0 if the user program can access this range of addresses,
// and -E_FAULT otherwise.
//
int
user_mem_check(struct Env *env, const void *va, size_t len, int perm)
{
f0102bb6:	55                   	push   %ebp
f0102bb7:	89 e5                	mov    %esp,%ebp
	// LAB 3: Your code here.

	return 0;
}
f0102bb9:	b8 00 00 00 00       	mov    $0x0,%eax
f0102bbe:	5d                   	pop    %ebp
f0102bbf:	c3                   	ret    

f0102bc0 <user_mem_assert>:
// If it cannot, 'env' is destroyed and, if env is the current
// environment, this function will not return.
//
void
user_mem_assert(struct Env *env, const void *va, size_t len, int perm)
{
f0102bc0:	55                   	push   %ebp
f0102bc1:	89 e5                	mov    %esp,%ebp
	if (user_mem_check(env, va, len, perm | PTE_U) < 0) {
		cprintf("[%08x] user_mem_check assertion failure for "
			"va %08x\n", env->env_id, user_mem_check_addr);
		env_destroy(env);	// may not return
	}
}
f0102bc3:	5d                   	pop    %ebp
f0102bc4:	c3                   	ret    

f0102bc5 <region_alloc>:
{
	// LAB 3: Your code here.
	// (But only if you need it for load_icode.)
	//
	int num_Page;
	if(len == 0){
f0102bc5:	85 c9                	test   %ecx,%ecx
f0102bc7:	0f 84 9b 00 00 00    	je     f0102c68 <region_alloc+0xa3>
// Pages should be writable by user and kernel.
// Panic if any allocation attempt fails.
//
static void
region_alloc(struct Env *e, void *va, size_t len)
{
f0102bcd:	55                   	push   %ebp
f0102bce:	89 e5                	mov    %esp,%ebp
f0102bd0:	57                   	push   %edi
f0102bd1:	56                   	push   %esi
f0102bd2:	53                   	push   %ebx
f0102bd3:	83 ec 1c             	sub    $0x1c,%esp
	//
	int num_Page;
	if(len == 0){
		return;
	}
	void* end_Va = va + len;
f0102bd6:	01 d1                	add    %edx,%ecx
	void* start_Va = (void*)PTE_ADDR(va); //va & ~0xFFF; //round down va
f0102bd8:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f0102bde:	89 d3                	mov    %edx,%ebx
	//figure out how many pages needs to be allocated
	if(((int)end_Va % PGSIZE) == 0){ //already aligned 
f0102be0:	f7 c1 ff 0f 00 00    	test   $0xfff,%ecx
f0102be6:	75 16                	jne    f0102bfe <region_alloc+0x39>
		num_Page = ((end_Va - start_Va) / PGSIZE);
f0102be8:	29 d1                	sub    %edx,%ecx
f0102bea:	89 ca                	mov    %ecx,%edx
f0102bec:	85 c9                	test   %ecx,%ecx
f0102bee:	79 06                	jns    f0102bf6 <region_alloc+0x31>
f0102bf0:	8d 91 ff 0f 00 00    	lea    0xfff(%ecx),%edx
f0102bf6:	c1 fa 0c             	sar    $0xc,%edx
f0102bf9:	89 55 e4             	mov    %edx,-0x1c(%ebp)
f0102bfc:	eb 17                	jmp    f0102c15 <region_alloc+0x50>
	}else{
		num_Page = ((end_Va - start_Va) / PGSIZE) + 1;
f0102bfe:	29 d1                	sub    %edx,%ecx
f0102c00:	89 ca                	mov    %ecx,%edx
f0102c02:	85 c9                	test   %ecx,%ecx
f0102c04:	79 06                	jns    f0102c0c <region_alloc+0x47>
f0102c06:	8d 91 ff 0f 00 00    	lea    0xfff(%ecx),%edx
f0102c0c:	c1 fa 0c             	sar    $0xc,%edx
f0102c0f:	8d 4a 01             	lea    0x1(%edx),%ecx
f0102c12:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
f0102c15:	89 c7                	mov    %eax,%edi
	}
	//boot_map_region(e->pg_dir, );
	for(int i=0; i<num_Page; i++){
f0102c17:	be 00 00 00 00       	mov    $0x0,%esi
f0102c1c:	eb 3e                	jmp    f0102c5c <region_alloc+0x97>
		struct PageInfo* pp = page_alloc((!ALLOC_ZERO)); //Does not zero
f0102c1e:	83 ec 0c             	sub    $0xc,%esp
f0102c21:	6a 00                	push   $0x0
f0102c23:	e8 d5 e3 ff ff       	call   f0100ffd <page_alloc>
		if(pp == NULL){
f0102c28:	83 c4 10             	add    $0x10,%esp
f0102c2b:	85 c0                	test   %eax,%eax
f0102c2d:	75 17                	jne    f0102c46 <region_alloc+0x81>
			panic("allocation attempt fails in region_alloc");
f0102c2f:	83 ec 04             	sub    $0x4,%esp
f0102c32:	68 d4 61 10 f0       	push   $0xf01061d4
f0102c37:	68 30 01 00 00       	push   $0x130
f0102c3c:	68 56 62 10 f0       	push   $0xf0106256
f0102c41:	e8 06 d4 ff ff       	call   f010004c <_panic>
			//return -E_NO_MEM; //page table couldn't be allocated
		}
		page_insert(e->env_pgdir, pp, start_Va+i*PGSIZE, PTE_W|PTE_U|PTE_P); //user read and write
f0102c46:	6a 07                	push   $0x7
f0102c48:	53                   	push   %ebx
f0102c49:	50                   	push   %eax
f0102c4a:	ff 77 5c             	pushl  0x5c(%edi)
f0102c4d:	e8 84 e7 ff ff       	call   f01013d6 <page_insert>
		num_Page = ((end_Va - start_Va) / PGSIZE);
	}else{
		num_Page = ((end_Va - start_Va) / PGSIZE) + 1;
	}
	//boot_map_region(e->pg_dir, );
	for(int i=0; i<num_Page; i++){
f0102c52:	46                   	inc    %esi
f0102c53:	81 c3 00 10 00 00    	add    $0x1000,%ebx
f0102c59:	83 c4 10             	add    $0x10,%esp
f0102c5c:	39 75 e4             	cmp    %esi,-0x1c(%ebp)
f0102c5f:	7f bd                	jg     f0102c1e <region_alloc+0x59>

	// Hint: It is easier to use region_alloc if the caller can pass
	//   'va' and 'len' values that are not page-aligned.
	//   You should round va down, and round (va + len) up.
	//   (Watch out for corner-cases!)
}
f0102c61:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0102c64:	5b                   	pop    %ebx
f0102c65:	5e                   	pop    %esi
f0102c66:	5f                   	pop    %edi
f0102c67:	5d                   	pop    %ebp
f0102c68:	c3                   	ret    

f0102c69 <envid2env>:
//   On success, sets *env_store to the environment.
//   On error, sets *env_store to NULL.
//
int
envid2env(envid_t envid, struct Env **env_store, bool checkperm)
{
f0102c69:	55                   	push   %ebp
f0102c6a:	89 e5                	mov    %esp,%ebp
f0102c6c:	53                   	push   %ebx
f0102c6d:	8b 55 08             	mov    0x8(%ebp),%edx
f0102c70:	8b 4d 10             	mov    0x10(%ebp),%ecx
	struct Env *e;

	// If envid is zero, return the current environment.
	if (envid == 0) {
f0102c73:	85 d2                	test   %edx,%edx
f0102c75:	75 11                	jne    f0102c88 <envid2env+0x1f>
		*env_store = curenv;
f0102c77:	a1 60 d2 18 f0       	mov    0xf018d260,%eax
f0102c7c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0102c7f:	89 01                	mov    %eax,(%ecx)
		return 0;
f0102c81:	b8 00 00 00 00       	mov    $0x0,%eax
f0102c86:	eb 60                	jmp    f0102ce8 <envid2env+0x7f>
	// Look up the Env structure via the index part of the envid,
	// then check the env_id field in that struct Env
	// to ensure that the envid is not stale
	// (i.e., does not refer to a _previous_ environment
	// that used the same slot in the envs[] array).
	e = &envs[ENVX(envid)];
f0102c88:	89 d0                	mov    %edx,%eax
f0102c8a:	25 ff 03 00 00       	and    $0x3ff,%eax
f0102c8f:	8d 1c 00             	lea    (%eax,%eax,1),%ebx
f0102c92:	01 d8                	add    %ebx,%eax
f0102c94:	c1 e0 05             	shl    $0x5,%eax
f0102c97:	03 05 64 d2 18 f0    	add    0xf018d264,%eax
	if (e->env_status == ENV_FREE || e->env_id != envid) {
f0102c9d:	83 78 54 00          	cmpl   $0x0,0x54(%eax)
f0102ca1:	74 05                	je     f0102ca8 <envid2env+0x3f>
f0102ca3:	3b 50 48             	cmp    0x48(%eax),%edx
f0102ca6:	74 10                	je     f0102cb8 <envid2env+0x4f>
		*env_store = 0;
f0102ca8:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102cab:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		return -E_BAD_ENV;
f0102cb1:	b8 fe ff ff ff       	mov    $0xfffffffe,%eax
f0102cb6:	eb 30                	jmp    f0102ce8 <envid2env+0x7f>
	// Check that the calling environment has legitimate permission
	// to manipulate the specified environment.
	// If checkperm is set, the specified environment
	// must be either the current environment
	// or an immediate child of the current environment.
	if (checkperm && e != curenv && e->env_parent_id != curenv->env_id) {
f0102cb8:	84 c9                	test   %cl,%cl
f0102cba:	74 22                	je     f0102cde <envid2env+0x75>
f0102cbc:	8b 15 60 d2 18 f0    	mov    0xf018d260,%edx
f0102cc2:	39 d0                	cmp    %edx,%eax
f0102cc4:	74 18                	je     f0102cde <envid2env+0x75>
f0102cc6:	8b 5a 48             	mov    0x48(%edx),%ebx
f0102cc9:	39 58 4c             	cmp    %ebx,0x4c(%eax)
f0102ccc:	74 10                	je     f0102cde <envid2env+0x75>
		*env_store = 0;
f0102cce:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102cd1:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		return -E_BAD_ENV;
f0102cd7:	b8 fe ff ff ff       	mov    $0xfffffffe,%eax
f0102cdc:	eb 0a                	jmp    f0102ce8 <envid2env+0x7f>
	}

	*env_store = e;
f0102cde:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f0102ce1:	89 03                	mov    %eax,(%ebx)
	return 0;
f0102ce3:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0102ce8:	5b                   	pop    %ebx
f0102ce9:	5d                   	pop    %ebp
f0102cea:	c3                   	ret    

f0102ceb <env_init_percpu>:
}

// Load GDT and segment descriptors.
void
env_init_percpu(void)
{
f0102ceb:	55                   	push   %ebp
f0102cec:	89 e5                	mov    %esp,%ebp
}

static inline void
lgdt(void *p)
{
	asm volatile("lgdt (%0)" : : "r" (p));
f0102cee:	b8 00 c3 11 f0       	mov    $0xf011c300,%eax
f0102cf3:	0f 01 10             	lgdtl  (%eax)
	lgdt(&gdt_pd);
	// The kernel never uses GS or FS, so we leave those set to
	// the user data segment.
	asm volatile("movw %%ax,%%gs" : : "a" (GD_UD|3));
f0102cf6:	b8 23 00 00 00       	mov    $0x23,%eax
f0102cfb:	8e e8                	mov    %eax,%gs
	asm volatile("movw %%ax,%%fs" : : "a" (GD_UD|3));
f0102cfd:	8e e0                	mov    %eax,%fs
	// The kernel does use ES, DS, and SS.  We'll change between
	// the kernel and user data segments as needed.
	asm volatile("movw %%ax,%%es" : : "a" (GD_KD));
f0102cff:	b8 10 00 00 00       	mov    $0x10,%eax
f0102d04:	8e c0                	mov    %eax,%es
	asm volatile("movw %%ax,%%ds" : : "a" (GD_KD));
f0102d06:	8e d8                	mov    %eax,%ds
	asm volatile("movw %%ax,%%ss" : : "a" (GD_KD));
f0102d08:	8e d0                	mov    %eax,%ss
	// Load the kernel text segment into CS.
	asm volatile("ljmp %0,$1f\n 1:\n" : : "i" (GD_KT));
f0102d0a:	ea 11 2d 10 f0 08 00 	ljmp   $0x8,$0xf0102d11
}

static inline void
lldt(uint16_t sel)
{
	asm volatile("lldt %0" : : "r" (sel));
f0102d11:	b8 00 00 00 00       	mov    $0x0,%eax
f0102d16:	0f 00 d0             	lldt   %ax
	// For good measure, clear the local descriptor table (LDT),
	// since we don't use it.
	lldt(0);
}
f0102d19:	5d                   	pop    %ebp
f0102d1a:	c3                   	ret    

f0102d1b <env_init>:
	env_init_percpu();*/

	//reference code:
	int i;
	for (i = 0; i != NENV - 1; ++i) {
		envs[i].env_id = 0;
f0102d1b:	8b 15 64 d2 18 f0    	mov    0xf018d264,%edx
f0102d21:	8d 42 60             	lea    0x60(%edx),%eax
f0102d24:	81 c2 00 80 01 00    	add    $0x18000,%edx
f0102d2a:	c7 40 e8 00 00 00 00 	movl   $0x0,-0x18(%eax)
		envs[i].env_link = &envs[i + 1];
f0102d31:	89 40 e4             	mov    %eax,-0x1c(%eax)
f0102d34:	83 c0 60             	add    $0x60,%eax
	// Per-CPU part of the initialization
	env_init_percpu();*/

	//reference code:
	int i;
	for (i = 0; i != NENV - 1; ++i) {
f0102d37:	39 d0                	cmp    %edx,%eax
f0102d39:	75 ef                	jne    f0102d2a <env_init+0xf>
// they are in the envs array (i.e., so that the first call to
// env_alloc() returns envs[0]).
//
void
env_init(void)
{
f0102d3b:	55                   	push   %ebp
f0102d3c:	89 e5                	mov    %esp,%ebp
	int i;
	for (i = 0; i != NENV - 1; ++i) {
		envs[i].env_id = 0;
		envs[i].env_link = &envs[i + 1];
	}
	envs[NENV - 1].env_link = NULL;
f0102d3e:	a1 64 d2 18 f0       	mov    0xf018d264,%eax
f0102d43:	c7 80 e4 7f 01 00 00 	movl   $0x0,0x17fe4(%eax)
f0102d4a:	00 00 00 
	env_free_list = &envs[0];
f0102d4d:	a3 68 d2 18 f0       	mov    %eax,0xf018d268

	// Per-CPU part of the initialization
	env_init_percpu();
f0102d52:	e8 94 ff ff ff       	call   f0102ceb <env_init_percpu>
}
f0102d57:	5d                   	pop    %ebp
f0102d58:	c3                   	ret    

f0102d59 <env_alloc>:
//	-E_NO_FREE_ENV if all NENVS environments are allocated
//	-E_NO_MEM on memory exhaustion
//
int
env_alloc(struct Env **newenv_store, envid_t parent_id)
{
f0102d59:	55                   	push   %ebp
f0102d5a:	89 e5                	mov    %esp,%ebp
f0102d5c:	56                   	push   %esi
f0102d5d:	53                   	push   %ebx
	int32_t generation;
	int r;
	struct Env *e;

	if (!(e = env_free_list))
f0102d5e:	8b 1d 68 d2 18 f0    	mov    0xf018d268,%ebx
f0102d64:	85 db                	test   %ebx,%ebx
f0102d66:	0f 84 61 01 00 00    	je     f0102ecd <env_alloc+0x174>
{
	int i;
	struct PageInfo *p = NULL;

	// Allocate a page for the page directory
	if (!(p = page_alloc(ALLOC_ZERO))){
f0102d6c:	83 ec 0c             	sub    $0xc,%esp
f0102d6f:	6a 01                	push   $0x1
f0102d71:	e8 87 e2 ff ff       	call   f0100ffd <page_alloc>
f0102d76:	83 c4 10             	add    $0x10,%esp
f0102d79:	85 c0                	test   %eax,%eax
f0102d7b:	0f 84 53 01 00 00    	je     f0102ed4 <env_alloc+0x17b>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0102d81:	89 c2                	mov    %eax,%edx
f0102d83:	2b 15 4c e4 18 f0    	sub    0xf018e44c,%edx
f0102d89:	c1 fa 03             	sar    $0x3,%edx
f0102d8c:	c1 e2 0c             	shl    $0xc,%edx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102d8f:	89 d1                	mov    %edx,%ecx
f0102d91:	c1 e9 0c             	shr    $0xc,%ecx
f0102d94:	3b 0d 44 e4 18 f0    	cmp    0xf018e444,%ecx
f0102d9a:	72 12                	jb     f0102dae <env_alloc+0x55>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102d9c:	52                   	push   %edx
f0102d9d:	68 30 56 10 f0       	push   $0xf0105630
f0102da2:	6a 56                	push   $0x56
f0102da4:	68 f5 5d 10 f0       	push   $0xf0105df5
f0102da9:	e8 9e d2 ff ff       	call   f010004c <_panic>
	//	is an exception -- you need to increment env_pgdir's
	//	pp_ref for env_free to work correctly.
	//    - The functions in kern/pmap.h are handy.

	// LAB 3: Your code here.
	e->env_pgdir = (pde_t *)page2kva(p); //page2kva() ??
f0102dae:	81 ea 00 00 00 10    	sub    $0x10000000,%edx
f0102db4:	89 53 5c             	mov    %edx,0x5c(%ebx)
f0102db7:	ba ec 0e 00 00       	mov    $0xeec,%edx
	//for(int i=PDX(ULIM); i<1024; i++){ //everything above ULIM is kernel!
	for(int i=PDX(UTOP); i<1024; i++){
		e->env_pgdir[i] = kern_pgdir[i];
f0102dbc:	8b 0d 48 e4 18 f0    	mov    0xf018e448,%ecx
f0102dc2:	8b 34 11             	mov    (%ecx,%edx,1),%esi
f0102dc5:	8b 4b 5c             	mov    0x5c(%ebx),%ecx
f0102dc8:	89 34 11             	mov    %esi,(%ecx,%edx,1)
f0102dcb:	83 c2 04             	add    $0x4,%edx
	//    - The functions in kern/pmap.h are handy.

	// LAB 3: Your code here.
	e->env_pgdir = (pde_t *)page2kva(p); //page2kva() ??
	//for(int i=PDX(ULIM); i<1024; i++){ //everything above ULIM is kernel!
	for(int i=PDX(UTOP); i<1024; i++){
f0102dce:	81 fa 00 10 00 00    	cmp    $0x1000,%edx
f0102dd4:	75 e6                	jne    f0102dbc <env_alloc+0x63>
		e->env_pgdir[i] = kern_pgdir[i];
	}
	p->pp_ref++;
f0102dd6:	66 ff 40 04          	incw   0x4(%eax)
	// UVPT maps the env's own page table read-only.
	// Permissions: kernel R, user R
	e->env_pgdir[PDX(UVPT)] = PADDR(e->env_pgdir) | PTE_P | PTE_U;
f0102dda:	8b 43 5c             	mov    0x5c(%ebx),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102ddd:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0102de2:	77 15                	ja     f0102df9 <env_alloc+0xa0>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102de4:	50                   	push   %eax
f0102de5:	68 78 57 10 f0       	push   $0xf0105778
f0102dea:	68 d0 00 00 00       	push   $0xd0
f0102def:	68 56 62 10 f0       	push   $0xf0106256
f0102df4:	e8 53 d2 ff ff       	call   f010004c <_panic>
f0102df9:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f0102dff:	83 ca 05             	or     $0x5,%edx
f0102e02:	89 90 f4 0e 00 00    	mov    %edx,0xef4(%eax)

	// Allocate and set up the page directory for this environment.
	if ((r = env_setup_vm(e)) < 0)
		return r;
	// Generate an env_id for this environment.
	generation = (e->env_id + (1 << ENVGENSHIFT)) & ~(NENV - 1);
f0102e08:	8b 43 48             	mov    0x48(%ebx),%eax
f0102e0b:	05 00 10 00 00       	add    $0x1000,%eax
	if (generation <= 0)	// Don't create a negative env_id.
f0102e10:	25 00 fc ff ff       	and    $0xfffffc00,%eax
f0102e15:	7f 05                	jg     f0102e1c <env_alloc+0xc3>
		generation = 1 << ENVGENSHIFT;
f0102e17:	b8 00 10 00 00       	mov    $0x1000,%eax
	e->env_id = generation | (e - envs);
f0102e1c:	89 da                	mov    %ebx,%edx
f0102e1e:	2b 15 64 d2 18 f0    	sub    0xf018d264,%edx
f0102e24:	c1 fa 05             	sar    $0x5,%edx
f0102e27:	8d 0c 92             	lea    (%edx,%edx,4),%ecx
f0102e2a:	8d 0c 8a             	lea    (%edx,%ecx,4),%ecx
f0102e2d:	8d 34 8a             	lea    (%edx,%ecx,4),%esi
f0102e30:	89 f1                	mov    %esi,%ecx
f0102e32:	c1 e1 08             	shl    $0x8,%ecx
f0102e35:	01 ce                	add    %ecx,%esi
f0102e37:	89 f1                	mov    %esi,%ecx
f0102e39:	c1 e1 10             	shl    $0x10,%ecx
f0102e3c:	01 f1                	add    %esi,%ecx
f0102e3e:	01 c9                	add    %ecx,%ecx
f0102e40:	01 ca                	add    %ecx,%edx
f0102e42:	09 c2                	or     %eax,%edx
f0102e44:	89 53 48             	mov    %edx,0x48(%ebx)

	// Set the basic status variables.
	e->env_parent_id = parent_id;
f0102e47:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102e4a:	89 43 4c             	mov    %eax,0x4c(%ebx)
	e->env_type = ENV_TYPE_USER;
f0102e4d:	c7 43 50 00 00 00 00 	movl   $0x0,0x50(%ebx)
	e->env_status = ENV_RUNNABLE;
f0102e54:	c7 43 54 02 00 00 00 	movl   $0x2,0x54(%ebx)
	e->env_runs = 0;
f0102e5b:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)

	// Clear out all the saved register state,
	// to prevent the register values
	// of a prior environment inhabiting this Env structure
	// from "leaking" into our new environment.
	memset(&e->env_tf, 0, sizeof(e->env_tf));
f0102e62:	83 ec 04             	sub    $0x4,%esp
f0102e65:	6a 44                	push   $0x44
f0102e67:	6a 00                	push   $0x0
f0102e69:	53                   	push   %ebx
f0102e6a:	e8 a9 1c 00 00       	call   f0104b18 <memset>
	// The low 2 bits of each segment register contains the
	// Requestor Privilege Level (RPL); 3 means user mode.  When
	// we switch privilege levels, the hardware does various
	// checks involving the RPL and the Descriptor Privilege Level
	// (DPL) stored in the descriptors themselves.
	e->env_tf.tf_ds = GD_UD | 3;
f0102e6f:	66 c7 43 24 23 00    	movw   $0x23,0x24(%ebx)
	e->env_tf.tf_es = GD_UD | 3;
f0102e75:	66 c7 43 20 23 00    	movw   $0x23,0x20(%ebx)
	e->env_tf.tf_ss = GD_UD | 3;
f0102e7b:	66 c7 43 40 23 00    	movw   $0x23,0x40(%ebx)
	e->env_tf.tf_esp = USTACKTOP;
f0102e81:	c7 43 3c 00 e0 bf ee 	movl   $0xeebfe000,0x3c(%ebx)
	e->env_tf.tf_cs = GD_UT | 3;
f0102e88:	66 c7 43 34 1b 00    	movw   $0x1b,0x34(%ebx)
	// You will set e->env_tf.tf_eip later.

	// commit the allocation
	env_free_list = e->env_link;
f0102e8e:	8b 43 44             	mov    0x44(%ebx),%eax
f0102e91:	a3 68 d2 18 f0       	mov    %eax,0xf018d268
	*newenv_store = e;
f0102e96:	8b 45 08             	mov    0x8(%ebp),%eax
f0102e99:	89 18                	mov    %ebx,(%eax)

	cprintf("[%08x] new env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
f0102e9b:	8b 53 48             	mov    0x48(%ebx),%edx
f0102e9e:	a1 60 d2 18 f0       	mov    0xf018d260,%eax
f0102ea3:	83 c4 10             	add    $0x10,%esp
f0102ea6:	85 c0                	test   %eax,%eax
f0102ea8:	74 05                	je     f0102eaf <env_alloc+0x156>
f0102eaa:	8b 40 48             	mov    0x48(%eax),%eax
f0102ead:	eb 05                	jmp    f0102eb4 <env_alloc+0x15b>
f0102eaf:	b8 00 00 00 00       	mov    $0x0,%eax
f0102eb4:	83 ec 04             	sub    $0x4,%esp
f0102eb7:	52                   	push   %edx
f0102eb8:	50                   	push   %eax
f0102eb9:	68 61 62 10 f0       	push   $0xf0106261
f0102ebe:	e8 06 04 00 00       	call   f01032c9 <cprintf>
	return 0;
f0102ec3:	83 c4 10             	add    $0x10,%esp
f0102ec6:	b8 00 00 00 00       	mov    $0x0,%eax
f0102ecb:	eb 0c                	jmp    f0102ed9 <env_alloc+0x180>
	int32_t generation;
	int r;
	struct Env *e;

	if (!(e = env_free_list))
		return -E_NO_FREE_ENV;
f0102ecd:	b8 fb ff ff ff       	mov    $0xfffffffb,%eax
f0102ed2:	eb 05                	jmp    f0102ed9 <env_alloc+0x180>
	int i;
	struct PageInfo *p = NULL;

	// Allocate a page for the page directory
	if (!(p = page_alloc(ALLOC_ZERO))){
		return -E_NO_MEM;
f0102ed4:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
	env_free_list = e->env_link;
	*newenv_store = e;

	cprintf("[%08x] new env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
	return 0;
}
f0102ed9:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0102edc:	5b                   	pop    %ebx
f0102edd:	5e                   	pop    %esi
f0102ede:	5d                   	pop    %ebp
f0102edf:	c3                   	ret    

f0102ee0 <env_create>:
// before running the first user-mode environment.
// The new env's parent ID is set to 0.
//
void
env_create(uint8_t *binary, enum EnvType type)
{
f0102ee0:	55                   	push   %ebp
f0102ee1:	89 e5                	mov    %esp,%ebp
f0102ee3:	57                   	push   %edi
f0102ee4:	56                   	push   %esi
f0102ee5:	53                   	push   %ebx
f0102ee6:	83 ec 34             	sub    $0x34,%esp
f0102ee9:	8b 7d 08             	mov    0x8(%ebp),%edi
	// LAB 3: Your code here.
	struct Env *newenv;
	int env_alloc_Result;
	env_alloc_Result = env_alloc(&newenv, 0); //new env's parent ID is set to 0.
f0102eec:	6a 00                	push   $0x0
f0102eee:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0102ef1:	50                   	push   %eax
f0102ef2:	e8 62 fe ff ff       	call   f0102d59 <env_alloc>
	if(env_alloc_Result == -E_NO_FREE_ENV){
f0102ef7:	83 c4 10             	add    $0x10,%esp
f0102efa:	83 f8 fb             	cmp    $0xfffffffb,%eax
f0102efd:	75 16                	jne    f0102f15 <env_create+0x35>
		panic("env_create: %e", env_alloc_Result);
f0102eff:	6a fb                	push   $0xfffffffb
f0102f01:	68 76 62 10 f0       	push   $0xf0106276
f0102f06:	68 9e 01 00 00       	push   $0x19e
f0102f0b:	68 56 62 10 f0       	push   $0xf0106256
f0102f10:	e8 37 d1 ff ff       	call   f010004c <_panic>
	}
	if(env_alloc_Result == -E_NO_MEM){
f0102f15:	83 f8 fc             	cmp    $0xfffffffc,%eax
f0102f18:	75 16                	jne    f0102f30 <env_create+0x50>
		panic("env_create: %e", env_alloc_Result);
f0102f1a:	6a fc                	push   $0xfffffffc
f0102f1c:	68 76 62 10 f0       	push   $0xf0106276
f0102f21:	68 a1 01 00 00       	push   $0x1a1
f0102f26:	68 56 62 10 f0       	push   $0xf0106256
f0102f2b:	e8 1c d1 ff ff       	call   f010004c <_panic>
	}
	newenv->env_type = type;
f0102f30:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0102f33:	89 c1                	mov    %eax,%ecx
f0102f35:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f0102f38:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102f3b:	89 41 50             	mov    %eax,0x50(%ecx)
	//  You must also do something with the program's entry point,
	//  to make sure that the environment starts executing there.
	//  What?  (See env_run() and env_pop_tf() below.)

	// LAB 3: Your code here.
	cprintf("load_icode starts:\n");
f0102f3e:	83 ec 0c             	sub    $0xc,%esp
f0102f41:	68 85 62 10 f0       	push   $0xf0106285
f0102f46:	e8 7e 03 00 00       	call   f01032c9 <cprintf>

static inline uint32_t
rcr3(void)
{
	uint32_t val;
	asm volatile("movl %%cr3,%0" : "=r" (val));
f0102f4b:	0f 20 d8             	mov    %cr3,%eax
	physaddr_t  old_cr3 = rcr3(); 

	struct Proghdr *ph, *eph;
	if(((struct Elf *)binary)->e_magic != ELF_MAGIC) {
f0102f4e:	83 c4 10             	add    $0x10,%esp
f0102f51:	81 3f 7f 45 4c 46    	cmpl   $0x464c457f,(%edi)
f0102f57:	74 17                	je     f0102f70 <env_create+0x90>
		panic("Invalid binary in load_icode.\n");
f0102f59:	83 ec 04             	sub    $0x4,%esp
f0102f5c:	68 00 62 10 f0       	push   $0xf0106200
f0102f61:	68 77 01 00 00       	push   $0x177
f0102f66:	68 56 62 10 f0       	push   $0xf0106256
f0102f6b:	e8 dc d0 ff ff       	call   f010004c <_panic>
	}
	ph = (struct Proghdr *) ((uint8_t *) binary + ((struct Elf *)binary)->e_phoff);
f0102f70:	89 fb                	mov    %edi,%ebx
f0102f72:	03 5f 1c             	add    0x1c(%edi),%ebx
	eph = ph + ((struct Elf *)binary)->e_phnum;
f0102f75:	0f b7 77 2c          	movzwl 0x2c(%edi),%esi
f0102f79:	c1 e6 05             	shl    $0x5,%esi
f0102f7c:	01 de                	add    %ebx,%esi
	lcr3( PADDR(e->env_pgdir) );
f0102f7e:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0102f81:	8b 40 5c             	mov    0x5c(%eax),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102f84:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0102f89:	77 15                	ja     f0102fa0 <env_create+0xc0>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102f8b:	50                   	push   %eax
f0102f8c:	68 78 57 10 f0       	push   $0xf0105778
f0102f91:	68 7b 01 00 00       	push   $0x17b
f0102f96:	68 56 62 10 f0       	push   $0xf0106256
f0102f9b:	e8 ac d0 ff ff       	call   f010004c <_panic>
}

static inline void
lcr3(uint32_t val)
{
	asm volatile("movl %0,%%cr3" : : "r" (val));
f0102fa0:	05 00 00 00 10       	add    $0x10000000,%eax
f0102fa5:	0f 22 d8             	mov    %eax,%cr3
f0102fa8:	eb 44                	jmp    f0102fee <env_create+0x10e>

	for (; ph < eph; ph++){
		if(ph->p_type == ELF_PROG_LOAD){
f0102faa:	83 3b 01             	cmpl   $0x1,(%ebx)
f0102fad:	75 3c                	jne    f0102feb <env_create+0x10b>
			region_alloc(e, (void *)ph->p_va, ph->p_memsz);
f0102faf:	8b 4b 14             	mov    0x14(%ebx),%ecx
f0102fb2:	8b 53 08             	mov    0x8(%ebx),%edx
f0102fb5:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0102fb8:	e8 08 fc ff ff       	call   f0102bc5 <region_alloc>
			memmove((void *)ph->p_va,(binary  + ph->p_offset), ph->p_filesz);
f0102fbd:	83 ec 04             	sub    $0x4,%esp
f0102fc0:	ff 73 10             	pushl  0x10(%ebx)
f0102fc3:	89 f8                	mov    %edi,%eax
f0102fc5:	03 43 04             	add    0x4(%ebx),%eax
f0102fc8:	50                   	push   %eax
f0102fc9:	ff 73 08             	pushl  0x8(%ebx)
f0102fcc:	e8 94 1b 00 00       	call   f0104b65 <memmove>
			memset((void *)ph->p_va+ph->p_filesz, 0, ph->p_memsz-ph->p_filesz);
f0102fd1:	8b 43 10             	mov    0x10(%ebx),%eax
f0102fd4:	83 c4 0c             	add    $0xc,%esp
f0102fd7:	8b 53 14             	mov    0x14(%ebx),%edx
f0102fda:	29 c2                	sub    %eax,%edx
f0102fdc:	52                   	push   %edx
f0102fdd:	6a 00                	push   $0x0
f0102fdf:	03 43 08             	add    0x8(%ebx),%eax
f0102fe2:	50                   	push   %eax
f0102fe3:	e8 30 1b 00 00       	call   f0104b18 <memset>
f0102fe8:	83 c4 10             	add    $0x10,%esp
	}
	ph = (struct Proghdr *) ((uint8_t *) binary + ((struct Elf *)binary)->e_phoff);
	eph = ph + ((struct Elf *)binary)->e_phnum;
	lcr3( PADDR(e->env_pgdir) );

	for (; ph < eph; ph++){
f0102feb:	83 c3 20             	add    $0x20,%ebx
f0102fee:	39 de                	cmp    %ebx,%esi
f0102ff0:	77 b8                	ja     f0102faa <env_create+0xca>
	}

	//lcr3(old_cr3);
	// Now map one page for the program's initial stack
	// at virtual address USTACKTOP - PGSIZE.
	(e->env_tf).tf_eip = ((struct Elf *)binary)->e_entry;
f0102ff2:	8b 47 18             	mov    0x18(%edi),%eax
f0102ff5:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f0102ff8:	89 47 30             	mov    %eax,0x30(%edi)
	region_alloc(e, (void*)(USTACKTOP - PGSIZE), PGSIZE);
f0102ffb:	b9 00 10 00 00       	mov    $0x1000,%ecx
f0103000:	ba 00 d0 bf ee       	mov    $0xeebfd000,%edx
f0103005:	89 f8                	mov    %edi,%eax
f0103007:	e8 b9 fb ff ff       	call   f0102bc5 <region_alloc>
		panic("env_create: %e", env_alloc_Result);
	}
	newenv->env_type = type;
	load_icode(newenv, binary);

}
f010300c:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010300f:	5b                   	pop    %ebx
f0103010:	5e                   	pop    %esi
f0103011:	5f                   	pop    %edi
f0103012:	5d                   	pop    %ebp
f0103013:	c3                   	ret    

f0103014 <env_free>:
//
// Frees env e and all memory it uses.
//
void
env_free(struct Env *e)
{
f0103014:	55                   	push   %ebp
f0103015:	89 e5                	mov    %esp,%ebp
f0103017:	57                   	push   %edi
f0103018:	56                   	push   %esi
f0103019:	53                   	push   %ebx
f010301a:	83 ec 1c             	sub    $0x1c,%esp
f010301d:	8b 7d 08             	mov    0x8(%ebp),%edi
	physaddr_t pa;

	// If freeing the current environment, switch to kern_pgdir
	// before freeing the page directory, just in case the page
	// gets reused.
	if (e == curenv)
f0103020:	8b 15 60 d2 18 f0    	mov    0xf018d260,%edx
f0103026:	39 fa                	cmp    %edi,%edx
f0103028:	75 29                	jne    f0103053 <env_free+0x3f>
		lcr3(PADDR(kern_pgdir));
f010302a:	a1 48 e4 18 f0       	mov    0xf018e448,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f010302f:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0103034:	77 15                	ja     f010304b <env_free+0x37>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0103036:	50                   	push   %eax
f0103037:	68 78 57 10 f0       	push   $0xf0105778
f010303c:	68 b6 01 00 00       	push   $0x1b6
f0103041:	68 56 62 10 f0       	push   $0xf0106256
f0103046:	e8 01 d0 ff ff       	call   f010004c <_panic>
f010304b:	05 00 00 00 10       	add    $0x10000000,%eax
f0103050:	0f 22 d8             	mov    %eax,%cr3

	// Note the environment's demise.
	cprintf("[%08x] free env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
f0103053:	8b 4f 48             	mov    0x48(%edi),%ecx
f0103056:	85 d2                	test   %edx,%edx
f0103058:	74 05                	je     f010305f <env_free+0x4b>
f010305a:	8b 42 48             	mov    0x48(%edx),%eax
f010305d:	eb 05                	jmp    f0103064 <env_free+0x50>
f010305f:	b8 00 00 00 00       	mov    $0x0,%eax
f0103064:	83 ec 04             	sub    $0x4,%esp
f0103067:	51                   	push   %ecx
f0103068:	50                   	push   %eax
f0103069:	68 99 62 10 f0       	push   $0xf0106299
f010306e:	e8 56 02 00 00       	call   f01032c9 <cprintf>
f0103073:	83 c4 10             	add    $0x10,%esp
f0103076:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

	// Flush all mapped pages in the user portion of the address space
	static_assert(UTOP % PTSIZE == 0);
	for (pdeno = 0; pdeno < PDX(UTOP); pdeno++) {
f010307d:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)

		// only look at mapped page tables
		if (!(e->env_pgdir[pdeno] & PTE_P))
f0103084:	8b 47 5c             	mov    0x5c(%edi),%eax
f0103087:	8b 55 e0             	mov    -0x20(%ebp),%edx
f010308a:	8b 34 10             	mov    (%eax,%edx,1),%esi
f010308d:	f7 c6 01 00 00 00    	test   $0x1,%esi
f0103093:	0f 84 a6 00 00 00    	je     f010313f <env_free+0x12b>
			continue;

		// find the pa and va of the page table
		pa = PTE_ADDR(e->env_pgdir[pdeno]);
f0103099:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010309f:	89 f0                	mov    %esi,%eax
f01030a1:	c1 e8 0c             	shr    $0xc,%eax
f01030a4:	89 45 d8             	mov    %eax,-0x28(%ebp)
f01030a7:	39 05 44 e4 18 f0    	cmp    %eax,0xf018e444
f01030ad:	77 15                	ja     f01030c4 <env_free+0xb0>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01030af:	56                   	push   %esi
f01030b0:	68 30 56 10 f0       	push   $0xf0105630
f01030b5:	68 c5 01 00 00       	push   $0x1c5
f01030ba:	68 56 62 10 f0       	push   $0xf0106256
f01030bf:	e8 88 cf ff ff       	call   f010004c <_panic>
		pt = (pte_t*) KADDR(pa);

		// unmap all PTEs in this page table
		for (pteno = 0; pteno <= PTX(~0); pteno++) {
			if (pt[pteno] & PTE_P)
				page_remove(e->env_pgdir, PGADDR(pdeno, pteno, 0));
f01030c4:	8b 45 dc             	mov    -0x24(%ebp),%eax
f01030c7:	c1 e0 16             	shl    $0x16,%eax
f01030ca:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		// find the pa and va of the page table
		pa = PTE_ADDR(e->env_pgdir[pdeno]);
		pt = (pte_t*) KADDR(pa);

		// unmap all PTEs in this page table
		for (pteno = 0; pteno <= PTX(~0); pteno++) {
f01030cd:	bb 00 00 00 00       	mov    $0x0,%ebx
			if (pt[pteno] & PTE_P)
f01030d2:	f6 84 9e 00 00 00 f0 	testb  $0x1,-0x10000000(%esi,%ebx,4)
f01030d9:	01 
f01030da:	74 17                	je     f01030f3 <env_free+0xdf>
				page_remove(e->env_pgdir, PGADDR(pdeno, pteno, 0));
f01030dc:	83 ec 08             	sub    $0x8,%esp
f01030df:	89 d8                	mov    %ebx,%eax
f01030e1:	c1 e0 0c             	shl    $0xc,%eax
f01030e4:	0b 45 e4             	or     -0x1c(%ebp),%eax
f01030e7:	50                   	push   %eax
f01030e8:	ff 77 5c             	pushl  0x5c(%edi)
f01030eb:	e8 4d e2 ff ff       	call   f010133d <page_remove>
f01030f0:	83 c4 10             	add    $0x10,%esp
		// find the pa and va of the page table
		pa = PTE_ADDR(e->env_pgdir[pdeno]);
		pt = (pte_t*) KADDR(pa);

		// unmap all PTEs in this page table
		for (pteno = 0; pteno <= PTX(~0); pteno++) {
f01030f3:	43                   	inc    %ebx
f01030f4:	81 fb 00 04 00 00    	cmp    $0x400,%ebx
f01030fa:	75 d6                	jne    f01030d2 <env_free+0xbe>
			if (pt[pteno] & PTE_P)
				page_remove(e->env_pgdir, PGADDR(pdeno, pteno, 0));
		}

		// free the page table itself
		e->env_pgdir[pdeno] = 0;
f01030fc:	8b 47 5c             	mov    0x5c(%edi),%eax
f01030ff:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0103102:	c7 04 10 00 00 00 00 	movl   $0x0,(%eax,%edx,1)
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0103109:	8b 45 d8             	mov    -0x28(%ebp),%eax
f010310c:	3b 05 44 e4 18 f0    	cmp    0xf018e444,%eax
f0103112:	72 14                	jb     f0103128 <env_free+0x114>
		panic("pa2page called with invalid pa");
f0103114:	83 ec 04             	sub    $0x4,%esp
f0103117:	68 18 57 10 f0       	push   $0xf0105718
f010311c:	6a 4f                	push   $0x4f
f010311e:	68 f5 5d 10 f0       	push   $0xf0105df5
f0103123:	e8 24 cf ff ff       	call   f010004c <_panic>
		page_decref(pa2page(pa));
f0103128:	83 ec 0c             	sub    $0xc,%esp
f010312b:	a1 4c e4 18 f0       	mov    0xf018e44c,%eax
f0103130:	8b 55 d8             	mov    -0x28(%ebp),%edx
f0103133:	8d 04 d0             	lea    (%eax,%edx,8),%eax
f0103136:	50                   	push   %eax
f0103137:	e8 94 df ff ff       	call   f01010d0 <page_decref>
f010313c:	83 c4 10             	add    $0x10,%esp
	// Note the environment's demise.
	cprintf("[%08x] free env %08x\n", curenv ? curenv->env_id : 0, e->env_id);

	// Flush all mapped pages in the user portion of the address space
	static_assert(UTOP % PTSIZE == 0);
	for (pdeno = 0; pdeno < PDX(UTOP); pdeno++) {
f010313f:	ff 45 dc             	incl   -0x24(%ebp)
f0103142:	83 45 e0 04          	addl   $0x4,-0x20(%ebp)
f0103146:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0103149:	3d ec 0e 00 00       	cmp    $0xeec,%eax
f010314e:	0f 85 30 ff ff ff    	jne    f0103084 <env_free+0x70>
		e->env_pgdir[pdeno] = 0;
		page_decref(pa2page(pa));
	}

	// free the page directory
	pa = PADDR(e->env_pgdir);
f0103154:	8b 47 5c             	mov    0x5c(%edi),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0103157:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f010315c:	77 15                	ja     f0103173 <env_free+0x15f>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f010315e:	50                   	push   %eax
f010315f:	68 78 57 10 f0       	push   $0xf0105778
f0103164:	68 d3 01 00 00       	push   $0x1d3
f0103169:	68 56 62 10 f0       	push   $0xf0106256
f010316e:	e8 d9 ce ff ff       	call   f010004c <_panic>
	e->env_pgdir = 0;
f0103173:	c7 47 5c 00 00 00 00 	movl   $0x0,0x5c(%edi)
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f010317a:	05 00 00 00 10       	add    $0x10000000,%eax
f010317f:	c1 e8 0c             	shr    $0xc,%eax
f0103182:	3b 05 44 e4 18 f0    	cmp    0xf018e444,%eax
f0103188:	72 14                	jb     f010319e <env_free+0x18a>
		panic("pa2page called with invalid pa");
f010318a:	83 ec 04             	sub    $0x4,%esp
f010318d:	68 18 57 10 f0       	push   $0xf0105718
f0103192:	6a 4f                	push   $0x4f
f0103194:	68 f5 5d 10 f0       	push   $0xf0105df5
f0103199:	e8 ae ce ff ff       	call   f010004c <_panic>
	page_decref(pa2page(pa));
f010319e:	83 ec 0c             	sub    $0xc,%esp
f01031a1:	8b 15 4c e4 18 f0    	mov    0xf018e44c,%edx
f01031a7:	8d 04 c2             	lea    (%edx,%eax,8),%eax
f01031aa:	50                   	push   %eax
f01031ab:	e8 20 df ff ff       	call   f01010d0 <page_decref>

	// return the environment to the free list
	e->env_status = ENV_FREE;
f01031b0:	c7 47 54 00 00 00 00 	movl   $0x0,0x54(%edi)
	e->env_link = env_free_list;
f01031b7:	a1 68 d2 18 f0       	mov    0xf018d268,%eax
f01031bc:	89 47 44             	mov    %eax,0x44(%edi)
	env_free_list = e;
f01031bf:	89 3d 68 d2 18 f0    	mov    %edi,0xf018d268
}
f01031c5:	83 c4 10             	add    $0x10,%esp
f01031c8:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01031cb:	5b                   	pop    %ebx
f01031cc:	5e                   	pop    %esi
f01031cd:	5f                   	pop    %edi
f01031ce:	5d                   	pop    %ebp
f01031cf:	c3                   	ret    

f01031d0 <env_destroy>:
//
// Frees environment e.
//
void
env_destroy(struct Env *e)
{
f01031d0:	55                   	push   %ebp
f01031d1:	89 e5                	mov    %esp,%ebp
f01031d3:	83 ec 14             	sub    $0x14,%esp
	env_free(e);
f01031d6:	ff 75 08             	pushl  0x8(%ebp)
f01031d9:	e8 36 fe ff ff       	call   f0103014 <env_free>

	cprintf("Destroyed the only environment - nothing more to do!\n");
f01031de:	c7 04 24 20 62 10 f0 	movl   $0xf0106220,(%esp)
f01031e5:	e8 df 00 00 00       	call   f01032c9 <cprintf>
f01031ea:	83 c4 10             	add    $0x10,%esp
	while (1)
		monitor(NULL);
f01031ed:	83 ec 0c             	sub    $0xc,%esp
f01031f0:	6a 00                	push   $0x0
f01031f2:	e8 a8 d6 ff ff       	call   f010089f <monitor>
f01031f7:	83 c4 10             	add    $0x10,%esp
f01031fa:	eb f1                	jmp    f01031ed <env_destroy+0x1d>

f01031fc <env_pop_tf>:
//
// This function does not return.
//
void
env_pop_tf(struct Trapframe *tf)
{
f01031fc:	55                   	push   %ebp
f01031fd:	89 e5                	mov    %esp,%ebp
f01031ff:	83 ec 0c             	sub    $0xc,%esp
	asm volatile(
f0103202:	8b 65 08             	mov    0x8(%ebp),%esp
f0103205:	61                   	popa   
f0103206:	07                   	pop    %es
f0103207:	1f                   	pop    %ds
f0103208:	83 c4 08             	add    $0x8,%esp
f010320b:	cf                   	iret   
		"\tpopl %%es\n"
		"\tpopl %%ds\n"
		"\taddl $0x8,%%esp\n" /* skip tf_trapno and tf_errcode */
		"\tiret\n"
		: : "g" (tf) : "memory");
	panic("iret failed");  /* mostly to placate the compiler */
f010320c:	68 af 62 10 f0       	push   $0xf01062af
f0103211:	68 fc 01 00 00       	push   $0x1fc
f0103216:	68 56 62 10 f0       	push   $0xf0106256
f010321b:	e8 2c ce ff ff       	call   f010004c <_panic>

f0103220 <env_run>:
//
// This function does not return.
//
void
env_run(struct Env *e)
{
f0103220:	55                   	push   %ebp
f0103221:	89 e5                	mov    %esp,%ebp
f0103223:	83 ec 08             	sub    $0x8,%esp
f0103226:	8b 55 08             	mov    0x8(%ebp),%edx
	//if(curenv == NULL && (curenv->env_id == e->env_id) ){
	//	goto end;
	//}
	//if(curenv != NULL && (curenv->env_id != e->env_id)){ //check if it's a context switch
	
	if(curenv != NULL && (curenv->env_id != e->env_id)){
f0103229:	a1 60 d2 18 f0       	mov    0xf018d260,%eax
f010322e:	85 c0                	test   %eax,%eax
f0103230:	74 17                	je     f0103249 <env_run+0x29>
f0103232:	8b 4a 48             	mov    0x48(%edx),%ecx
f0103235:	39 48 48             	cmp    %ecx,0x48(%eax)
f0103238:	74 0f                	je     f0103249 <env_run+0x29>
		curenv->env_status = ENV_RUNNABLE;
f010323a:	c7 40 54 02 00 00 00 	movl   $0x2,0x54(%eax)
		curenv = e;
f0103241:	89 15 60 d2 18 f0    	mov    %edx,0xf018d260
f0103247:	eb 06                	jmp    f010324f <env_run+0x2f>
	}else{
		curenv = e;
f0103249:	89 15 60 d2 18 f0    	mov    %edx,0xf018d260
	}
		//cprintf("In env_run()\n");
		curenv->env_status = ENV_RUNNING;
f010324f:	a1 60 d2 18 f0       	mov    0xf018d260,%eax
f0103254:	c7 40 54 03 00 00 00 	movl   $0x3,0x54(%eax)
		(curenv->env_runs)++;
f010325b:	ff 40 58             	incl   0x58(%eax)
		lcr3(PADDR(curenv->env_pgdir));
f010325e:	8b 50 5c             	mov    0x5c(%eax),%edx
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0103261:	81 fa ff ff ff ef    	cmp    $0xefffffff,%edx
f0103267:	77 15                	ja     f010327e <env_run+0x5e>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0103269:	52                   	push   %edx
f010326a:	68 78 57 10 f0       	push   $0xf0105778
f010326f:	68 28 02 00 00       	push   $0x228
f0103274:	68 56 62 10 f0       	push   $0xf0106256
f0103279:	e8 ce cd ff ff       	call   f010004c <_panic>
f010327e:	81 c2 00 00 00 10    	add    $0x10000000,%edx
f0103284:	0f 22 da             	mov    %edx,%cr3
		env_pop_tf(&(curenv->env_tf));
f0103287:	83 ec 0c             	sub    $0xc,%esp
f010328a:	50                   	push   %eax
f010328b:	e8 6c ff ff ff       	call   f01031fc <env_pop_tf>

f0103290 <putch>:
#include <inc/stdarg.h>


static void
putch(int ch, int *cnt)
{
f0103290:	55                   	push   %ebp
f0103291:	89 e5                	mov    %esp,%ebp
f0103293:	83 ec 14             	sub    $0x14,%esp
	cputchar(ch);
f0103296:	ff 75 08             	pushl  0x8(%ebp)
f0103299:	e8 84 d3 ff ff       	call   f0100622 <cputchar>
	*cnt++;
}
f010329e:	83 c4 10             	add    $0x10,%esp
f01032a1:	c9                   	leave  
f01032a2:	c3                   	ret    

f01032a3 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
f01032a3:	55                   	push   %ebp
f01032a4:	89 e5                	mov    %esp,%ebp
f01032a6:	83 ec 18             	sub    $0x18,%esp
	int cnt = 0;
f01032a9:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	vprintfmt((void*)putch, &cnt, fmt, ap);
f01032b0:	ff 75 0c             	pushl  0xc(%ebp)
f01032b3:	ff 75 08             	pushl  0x8(%ebp)
f01032b6:	8d 45 f4             	lea    -0xc(%ebp),%eax
f01032b9:	50                   	push   %eax
f01032ba:	68 90 32 10 f0       	push   $0xf0103290
f01032bf:	e8 0c 12 00 00       	call   f01044d0 <vprintfmt>
	return cnt;
}
f01032c4:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01032c7:	c9                   	leave  
f01032c8:	c3                   	ret    

f01032c9 <cprintf>:

int
cprintf(const char *fmt, ...)
{
f01032c9:	55                   	push   %ebp
f01032ca:	89 e5                	mov    %esp,%ebp
f01032cc:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
f01032cf:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
f01032d2:	50                   	push   %eax
f01032d3:	ff 75 08             	pushl  0x8(%ebp)
f01032d6:	e8 c8 ff ff ff       	call   f01032a3 <vcprintf>
	va_end(ap);

	return cnt;
}
f01032db:	c9                   	leave  
f01032dc:	c3                   	ret    

f01032dd <trap_init_percpu>:
}

// Initialize and load the per-CPU TSS and IDT
void
trap_init_percpu(void)
{
f01032dd:	55                   	push   %ebp
f01032de:	89 e5                	mov    %esp,%ebp
	// Setup a TSS so that we get the right stack
	// when we trap to the kernel.
	ts.ts_esp0 = KSTACKTOP;
f01032e0:	b8 a0 da 18 f0       	mov    $0xf018daa0,%eax
f01032e5:	c7 05 a4 da 18 f0 00 	movl   $0xf0000000,0xf018daa4
f01032ec:	00 00 f0 
	ts.ts_ss0 = GD_KD;
f01032ef:	66 c7 05 a8 da 18 f0 	movw   $0x10,0xf018daa8
f01032f6:	10 00 

	// Initialize the TSS slot of the gdt.
	gdt[GD_TSS0 >> 3] = SEG16(STS_T32A, (uint32_t) (&ts),
f01032f8:	66 c7 05 48 c3 11 f0 	movw   $0x67,0xf011c348
f01032ff:	67 00 
f0103301:	66 a3 4a c3 11 f0    	mov    %ax,0xf011c34a
f0103307:	89 c2                	mov    %eax,%edx
f0103309:	c1 ea 10             	shr    $0x10,%edx
f010330c:	88 15 4c c3 11 f0    	mov    %dl,0xf011c34c
f0103312:	c6 05 4e c3 11 f0 40 	movb   $0x40,0xf011c34e
f0103319:	c1 e8 18             	shr    $0x18,%eax
f010331c:	a2 4f c3 11 f0       	mov    %al,0xf011c34f
					sizeof(struct Taskstate) - 1, 0);
	gdt[GD_TSS0 >> 3].sd_s = 0;
f0103321:	c6 05 4d c3 11 f0 89 	movb   $0x89,0xf011c34d
}

static inline void
ltr(uint16_t sel)
{
	asm volatile("ltr %0" : : "r" (sel));
f0103328:	b8 28 00 00 00       	mov    $0x28,%eax
f010332d:	0f 00 d8             	ltr    %ax
}

static inline void
lidt(void *p)
{
	asm volatile("lidt (%0)" : : "r" (p));
f0103330:	b8 50 c3 11 f0       	mov    $0xf011c350,%eax
f0103335:	0f 01 18             	lidtl  (%eax)
	// bottom three bits are special; we leave them 0)
	ltr(GD_TSS0);

	// Load the IDT
	lidt(&idt_pd);
}
f0103338:	5d                   	pop    %ebp
f0103339:	c3                   	ret    

f010333a <trap_init>:
    } while (0)


void
trap_init(void)
{
f010333a:	55                   	push   %ebp
f010333b:	89 e5                	mov    %esp,%ebp
	//SETGATE(idt[0], 0, GD_KT, );

	// Note that ISTRAP must be zero for all gates
    // because JOS says when it enters kernel,
    // all interrupts are masked.
	SG(0, 0, 0);
f010333d:	b8 ec 3c 10 f0       	mov    $0xf0103cec,%eax
f0103342:	66 a3 80 d2 18 f0    	mov    %ax,0xf018d280
f0103348:	66 c7 05 82 d2 18 f0 	movw   $0x8,0xf018d282
f010334f:	08 00 
f0103351:	c6 05 84 d2 18 f0 00 	movb   $0x0,0xf018d284
f0103358:	c6 05 85 d2 18 f0 8e 	movb   $0x8e,0xf018d285
f010335f:	c1 e8 10             	shr    $0x10,%eax
f0103362:	66 a3 86 d2 18 f0    	mov    %ax,0xf018d286
	SG(1, 0, 0);
f0103368:	b8 f6 3c 10 f0       	mov    $0xf0103cf6,%eax
f010336d:	66 a3 88 d2 18 f0    	mov    %ax,0xf018d288
f0103373:	66 c7 05 8a d2 18 f0 	movw   $0x8,0xf018d28a
f010337a:	08 00 
f010337c:	c6 05 8c d2 18 f0 00 	movb   $0x0,0xf018d28c
f0103383:	c6 05 8d d2 18 f0 8e 	movb   $0x8e,0xf018d28d
f010338a:	c1 e8 10             	shr    $0x10,%eax
f010338d:	66 a3 8e d2 18 f0    	mov    %ax,0xf018d28e
	SG(2, 0, 0);
f0103393:	b8 00 3d 10 f0       	mov    $0xf0103d00,%eax
f0103398:	66 a3 90 d2 18 f0    	mov    %ax,0xf018d290
f010339e:	66 c7 05 92 d2 18 f0 	movw   $0x8,0xf018d292
f01033a5:	08 00 
f01033a7:	c6 05 94 d2 18 f0 00 	movb   $0x0,0xf018d294
f01033ae:	c6 05 95 d2 18 f0 8e 	movb   $0x8e,0xf018d295
f01033b5:	c1 e8 10             	shr    $0x10,%eax
f01033b8:	66 a3 96 d2 18 f0    	mov    %ax,0xf018d296
	SG(3, 0, 3);
f01033be:	b8 0a 3d 10 f0       	mov    $0xf0103d0a,%eax
f01033c3:	66 a3 98 d2 18 f0    	mov    %ax,0xf018d298
f01033c9:	66 c7 05 9a d2 18 f0 	movw   $0x8,0xf018d29a
f01033d0:	08 00 
f01033d2:	c6 05 9c d2 18 f0 00 	movb   $0x0,0xf018d29c
f01033d9:	c6 05 9d d2 18 f0 ee 	movb   $0xee,0xf018d29d
f01033e0:	c1 e8 10             	shr    $0x10,%eax
f01033e3:	66 a3 9e d2 18 f0    	mov    %ax,0xf018d29e
	SG(4, 0, 0);
f01033e9:	b8 14 3d 10 f0       	mov    $0xf0103d14,%eax
f01033ee:	66 a3 a0 d2 18 f0    	mov    %ax,0xf018d2a0
f01033f4:	66 c7 05 a2 d2 18 f0 	movw   $0x8,0xf018d2a2
f01033fb:	08 00 
f01033fd:	c6 05 a4 d2 18 f0 00 	movb   $0x0,0xf018d2a4
f0103404:	c6 05 a5 d2 18 f0 8e 	movb   $0x8e,0xf018d2a5
f010340b:	c1 e8 10             	shr    $0x10,%eax
f010340e:	66 a3 a6 d2 18 f0    	mov    %ax,0xf018d2a6
	SG(5, 0, 0);
f0103414:	b8 1e 3d 10 f0       	mov    $0xf0103d1e,%eax
f0103419:	66 a3 a8 d2 18 f0    	mov    %ax,0xf018d2a8
f010341f:	66 c7 05 aa d2 18 f0 	movw   $0x8,0xf018d2aa
f0103426:	08 00 
f0103428:	c6 05 ac d2 18 f0 00 	movb   $0x0,0xf018d2ac
f010342f:	c6 05 ad d2 18 f0 8e 	movb   $0x8e,0xf018d2ad
f0103436:	c1 e8 10             	shr    $0x10,%eax
f0103439:	66 a3 ae d2 18 f0    	mov    %ax,0xf018d2ae
	SG(6, 0, 0);
f010343f:	b8 28 3d 10 f0       	mov    $0xf0103d28,%eax
f0103444:	66 a3 b0 d2 18 f0    	mov    %ax,0xf018d2b0
f010344a:	66 c7 05 b2 d2 18 f0 	movw   $0x8,0xf018d2b2
f0103451:	08 00 
f0103453:	c6 05 b4 d2 18 f0 00 	movb   $0x0,0xf018d2b4
f010345a:	c6 05 b5 d2 18 f0 8e 	movb   $0x8e,0xf018d2b5
f0103461:	c1 e8 10             	shr    $0x10,%eax
f0103464:	66 a3 b6 d2 18 f0    	mov    %ax,0xf018d2b6
	SG(7, 0, 0);
f010346a:	b8 32 3d 10 f0       	mov    $0xf0103d32,%eax
f010346f:	66 a3 b8 d2 18 f0    	mov    %ax,0xf018d2b8
f0103475:	66 c7 05 ba d2 18 f0 	movw   $0x8,0xf018d2ba
f010347c:	08 00 
f010347e:	c6 05 bc d2 18 f0 00 	movb   $0x0,0xf018d2bc
f0103485:	c6 05 bd d2 18 f0 8e 	movb   $0x8e,0xf018d2bd
f010348c:	c1 e8 10             	shr    $0x10,%eax
f010348f:	66 a3 be d2 18 f0    	mov    %ax,0xf018d2be
	SG(8, 0, 0);
f0103495:	b8 3c 3d 10 f0       	mov    $0xf0103d3c,%eax
f010349a:	66 a3 c0 d2 18 f0    	mov    %ax,0xf018d2c0
f01034a0:	66 c7 05 c2 d2 18 f0 	movw   $0x8,0xf018d2c2
f01034a7:	08 00 
f01034a9:	c6 05 c4 d2 18 f0 00 	movb   $0x0,0xf018d2c4
f01034b0:	c6 05 c5 d2 18 f0 8e 	movb   $0x8e,0xf018d2c5
f01034b7:	c1 e8 10             	shr    $0x10,%eax
f01034ba:	66 a3 c6 d2 18 f0    	mov    %ax,0xf018d2c6
	SG(10, 0, 0);
f01034c0:	b8 44 3d 10 f0       	mov    $0xf0103d44,%eax
f01034c5:	66 a3 d0 d2 18 f0    	mov    %ax,0xf018d2d0
f01034cb:	66 c7 05 d2 d2 18 f0 	movw   $0x8,0xf018d2d2
f01034d2:	08 00 
f01034d4:	c6 05 d4 d2 18 f0 00 	movb   $0x0,0xf018d2d4
f01034db:	c6 05 d5 d2 18 f0 8e 	movb   $0x8e,0xf018d2d5
f01034e2:	c1 e8 10             	shr    $0x10,%eax
f01034e5:	66 a3 d6 d2 18 f0    	mov    %ax,0xf018d2d6
	SG(11, 0, 0);
f01034eb:	b8 4c 3d 10 f0       	mov    $0xf0103d4c,%eax
f01034f0:	66 a3 d8 d2 18 f0    	mov    %ax,0xf018d2d8
f01034f6:	66 c7 05 da d2 18 f0 	movw   $0x8,0xf018d2da
f01034fd:	08 00 
f01034ff:	c6 05 dc d2 18 f0 00 	movb   $0x0,0xf018d2dc
f0103506:	c6 05 dd d2 18 f0 8e 	movb   $0x8e,0xf018d2dd
f010350d:	c1 e8 10             	shr    $0x10,%eax
f0103510:	66 a3 de d2 18 f0    	mov    %ax,0xf018d2de
	SG(12, 0, 0);
f0103516:	b8 54 3d 10 f0       	mov    $0xf0103d54,%eax
f010351b:	66 a3 e0 d2 18 f0    	mov    %ax,0xf018d2e0
f0103521:	66 c7 05 e2 d2 18 f0 	movw   $0x8,0xf018d2e2
f0103528:	08 00 
f010352a:	c6 05 e4 d2 18 f0 00 	movb   $0x0,0xf018d2e4
f0103531:	c6 05 e5 d2 18 f0 8e 	movb   $0x8e,0xf018d2e5
f0103538:	c1 e8 10             	shr    $0x10,%eax
f010353b:	66 a3 e6 d2 18 f0    	mov    %ax,0xf018d2e6
	SG(13, 0, 0);
f0103541:	b8 5c 3d 10 f0       	mov    $0xf0103d5c,%eax
f0103546:	66 a3 e8 d2 18 f0    	mov    %ax,0xf018d2e8
f010354c:	66 c7 05 ea d2 18 f0 	movw   $0x8,0xf018d2ea
f0103553:	08 00 
f0103555:	c6 05 ec d2 18 f0 00 	movb   $0x0,0xf018d2ec
f010355c:	c6 05 ed d2 18 f0 8e 	movb   $0x8e,0xf018d2ed
f0103563:	c1 e8 10             	shr    $0x10,%eax
f0103566:	66 a3 ee d2 18 f0    	mov    %ax,0xf018d2ee
	SG(14, 0, 0);
f010356c:	b8 64 3d 10 f0       	mov    $0xf0103d64,%eax
f0103571:	66 a3 f0 d2 18 f0    	mov    %ax,0xf018d2f0
f0103577:	66 c7 05 f2 d2 18 f0 	movw   $0x8,0xf018d2f2
f010357e:	08 00 
f0103580:	c6 05 f4 d2 18 f0 00 	movb   $0x0,0xf018d2f4
f0103587:	c6 05 f5 d2 18 f0 8e 	movb   $0x8e,0xf018d2f5
f010358e:	c1 e8 10             	shr    $0x10,%eax
f0103591:	66 a3 f6 d2 18 f0    	mov    %ax,0xf018d2f6
	SG(16, 0, 0);
f0103597:	b8 6c 3d 10 f0       	mov    $0xf0103d6c,%eax
f010359c:	66 a3 00 d3 18 f0    	mov    %ax,0xf018d300
f01035a2:	66 c7 05 02 d3 18 f0 	movw   $0x8,0xf018d302
f01035a9:	08 00 
f01035ab:	c6 05 04 d3 18 f0 00 	movb   $0x0,0xf018d304
f01035b2:	c6 05 05 d3 18 f0 8e 	movb   $0x8e,0xf018d305
f01035b9:	c1 e8 10             	shr    $0x10,%eax
f01035bc:	66 a3 06 d3 18 f0    	mov    %ax,0xf018d306
	SG(17, 0, 0);
f01035c2:	b8 76 3d 10 f0       	mov    $0xf0103d76,%eax
f01035c7:	66 a3 08 d3 18 f0    	mov    %ax,0xf018d308
f01035cd:	66 c7 05 0a d3 18 f0 	movw   $0x8,0xf018d30a
f01035d4:	08 00 
f01035d6:	c6 05 0c d3 18 f0 00 	movb   $0x0,0xf018d30c
f01035dd:	c6 05 0d d3 18 f0 8e 	movb   $0x8e,0xf018d30d
f01035e4:	c1 e8 10             	shr    $0x10,%eax
f01035e7:	66 a3 0e d3 18 f0    	mov    %ax,0xf018d30e
	SG(18, 0, 0);
f01035ed:	b8 7e 3d 10 f0       	mov    $0xf0103d7e,%eax
f01035f2:	66 a3 10 d3 18 f0    	mov    %ax,0xf018d310
f01035f8:	66 c7 05 12 d3 18 f0 	movw   $0x8,0xf018d312
f01035ff:	08 00 
f0103601:	c6 05 14 d3 18 f0 00 	movb   $0x0,0xf018d314
f0103608:	c6 05 15 d3 18 f0 8e 	movb   $0x8e,0xf018d315
f010360f:	c1 e8 10             	shr    $0x10,%eax
f0103612:	66 a3 16 d3 18 f0    	mov    %ax,0xf018d316
	SG(19, 0, 0);
f0103618:	b8 88 3d 10 f0       	mov    $0xf0103d88,%eax
f010361d:	66 a3 18 d3 18 f0    	mov    %ax,0xf018d318
f0103623:	66 c7 05 1a d3 18 f0 	movw   $0x8,0xf018d31a
f010362a:	08 00 
f010362c:	c6 05 1c d3 18 f0 00 	movb   $0x0,0xf018d31c
f0103633:	c6 05 1d d3 18 f0 8e 	movb   $0x8e,0xf018d31d
f010363a:	c1 e8 10             	shr    $0x10,%eax
f010363d:	66 a3 1e d3 18 f0    	mov    %ax,0xf018d31e

	// 16 IRQs
	SG(32, 0, 0);
f0103643:	b8 92 3d 10 f0       	mov    $0xf0103d92,%eax
f0103648:	66 a3 80 d3 18 f0    	mov    %ax,0xf018d380
f010364e:	66 c7 05 82 d3 18 f0 	movw   $0x8,0xf018d382
f0103655:	08 00 
f0103657:	c6 05 84 d3 18 f0 00 	movb   $0x0,0xf018d384
f010365e:	c6 05 85 d3 18 f0 8e 	movb   $0x8e,0xf018d385
f0103665:	c1 e8 10             	shr    $0x10,%eax
f0103668:	66 a3 86 d3 18 f0    	mov    %ax,0xf018d386
	SG(33, 0, 0);
f010366e:	b8 9c 3d 10 f0       	mov    $0xf0103d9c,%eax
f0103673:	66 a3 88 d3 18 f0    	mov    %ax,0xf018d388
f0103679:	66 c7 05 8a d3 18 f0 	movw   $0x8,0xf018d38a
f0103680:	08 00 
f0103682:	c6 05 8c d3 18 f0 00 	movb   $0x0,0xf018d38c
f0103689:	c6 05 8d d3 18 f0 8e 	movb   $0x8e,0xf018d38d
f0103690:	c1 e8 10             	shr    $0x10,%eax
f0103693:	66 a3 8e d3 18 f0    	mov    %ax,0xf018d38e
	SG(34, 0, 0);
f0103699:	b8 a6 3d 10 f0       	mov    $0xf0103da6,%eax
f010369e:	66 a3 90 d3 18 f0    	mov    %ax,0xf018d390
f01036a4:	66 c7 05 92 d3 18 f0 	movw   $0x8,0xf018d392
f01036ab:	08 00 
f01036ad:	c6 05 94 d3 18 f0 00 	movb   $0x0,0xf018d394
f01036b4:	c6 05 95 d3 18 f0 8e 	movb   $0x8e,0xf018d395
f01036bb:	c1 e8 10             	shr    $0x10,%eax
f01036be:	66 a3 96 d3 18 f0    	mov    %ax,0xf018d396
	SG(35, 0, 0);
f01036c4:	b8 b0 3d 10 f0       	mov    $0xf0103db0,%eax
f01036c9:	66 a3 98 d3 18 f0    	mov    %ax,0xf018d398
f01036cf:	66 c7 05 9a d3 18 f0 	movw   $0x8,0xf018d39a
f01036d6:	08 00 
f01036d8:	c6 05 9c d3 18 f0 00 	movb   $0x0,0xf018d39c
f01036df:	c6 05 9d d3 18 f0 8e 	movb   $0x8e,0xf018d39d
f01036e6:	c1 e8 10             	shr    $0x10,%eax
f01036e9:	66 a3 9e d3 18 f0    	mov    %ax,0xf018d39e
	SG(36, 0, 0);
f01036ef:	b8 ba 3d 10 f0       	mov    $0xf0103dba,%eax
f01036f4:	66 a3 a0 d3 18 f0    	mov    %ax,0xf018d3a0
f01036fa:	66 c7 05 a2 d3 18 f0 	movw   $0x8,0xf018d3a2
f0103701:	08 00 
f0103703:	c6 05 a4 d3 18 f0 00 	movb   $0x0,0xf018d3a4
f010370a:	c6 05 a5 d3 18 f0 8e 	movb   $0x8e,0xf018d3a5
f0103711:	c1 e8 10             	shr    $0x10,%eax
f0103714:	66 a3 a6 d3 18 f0    	mov    %ax,0xf018d3a6
	SG(37, 0, 0);
f010371a:	b8 c4 3d 10 f0       	mov    $0xf0103dc4,%eax
f010371f:	66 a3 a8 d3 18 f0    	mov    %ax,0xf018d3a8
f0103725:	66 c7 05 aa d3 18 f0 	movw   $0x8,0xf018d3aa
f010372c:	08 00 
f010372e:	c6 05 ac d3 18 f0 00 	movb   $0x0,0xf018d3ac
f0103735:	c6 05 ad d3 18 f0 8e 	movb   $0x8e,0xf018d3ad
f010373c:	c1 e8 10             	shr    $0x10,%eax
f010373f:	66 a3 ae d3 18 f0    	mov    %ax,0xf018d3ae
	SG(38, 0, 0);
f0103745:	b8 ce 3d 10 f0       	mov    $0xf0103dce,%eax
f010374a:	66 a3 b0 d3 18 f0    	mov    %ax,0xf018d3b0
f0103750:	66 c7 05 b2 d3 18 f0 	movw   $0x8,0xf018d3b2
f0103757:	08 00 
f0103759:	c6 05 b4 d3 18 f0 00 	movb   $0x0,0xf018d3b4
f0103760:	c6 05 b5 d3 18 f0 8e 	movb   $0x8e,0xf018d3b5
f0103767:	c1 e8 10             	shr    $0x10,%eax
f010376a:	66 a3 b6 d3 18 f0    	mov    %ax,0xf018d3b6
	SG(39, 0, 0);
f0103770:	b8 d8 3d 10 f0       	mov    $0xf0103dd8,%eax
f0103775:	66 a3 b8 d3 18 f0    	mov    %ax,0xf018d3b8
f010377b:	66 c7 05 ba d3 18 f0 	movw   $0x8,0xf018d3ba
f0103782:	08 00 
f0103784:	c6 05 bc d3 18 f0 00 	movb   $0x0,0xf018d3bc
f010378b:	c6 05 bd d3 18 f0 8e 	movb   $0x8e,0xf018d3bd
f0103792:	c1 e8 10             	shr    $0x10,%eax
f0103795:	66 a3 be d3 18 f0    	mov    %ax,0xf018d3be
	SG(40, 0, 0);
f010379b:	b8 e2 3d 10 f0       	mov    $0xf0103de2,%eax
f01037a0:	66 a3 c0 d3 18 f0    	mov    %ax,0xf018d3c0
f01037a6:	66 c7 05 c2 d3 18 f0 	movw   $0x8,0xf018d3c2
f01037ad:	08 00 
f01037af:	c6 05 c4 d3 18 f0 00 	movb   $0x0,0xf018d3c4
f01037b6:	c6 05 c5 d3 18 f0 8e 	movb   $0x8e,0xf018d3c5
f01037bd:	c1 e8 10             	shr    $0x10,%eax
f01037c0:	66 a3 c6 d3 18 f0    	mov    %ax,0xf018d3c6
	SG(41, 0, 0);
f01037c6:	b8 ec 3d 10 f0       	mov    $0xf0103dec,%eax
f01037cb:	66 a3 c8 d3 18 f0    	mov    %ax,0xf018d3c8
f01037d1:	66 c7 05 ca d3 18 f0 	movw   $0x8,0xf018d3ca
f01037d8:	08 00 
f01037da:	c6 05 cc d3 18 f0 00 	movb   $0x0,0xf018d3cc
f01037e1:	c6 05 cd d3 18 f0 8e 	movb   $0x8e,0xf018d3cd
f01037e8:	c1 e8 10             	shr    $0x10,%eax
f01037eb:	66 a3 ce d3 18 f0    	mov    %ax,0xf018d3ce
	SG(42, 0, 0);
f01037f1:	b8 f6 3d 10 f0       	mov    $0xf0103df6,%eax
f01037f6:	66 a3 d0 d3 18 f0    	mov    %ax,0xf018d3d0
f01037fc:	66 c7 05 d2 d3 18 f0 	movw   $0x8,0xf018d3d2
f0103803:	08 00 
f0103805:	c6 05 d4 d3 18 f0 00 	movb   $0x0,0xf018d3d4
f010380c:	c6 05 d5 d3 18 f0 8e 	movb   $0x8e,0xf018d3d5
f0103813:	c1 e8 10             	shr    $0x10,%eax
f0103816:	66 a3 d6 d3 18 f0    	mov    %ax,0xf018d3d6
	SG(43, 0, 0);
f010381c:	b8 00 3e 10 f0       	mov    $0xf0103e00,%eax
f0103821:	66 a3 d8 d3 18 f0    	mov    %ax,0xf018d3d8
f0103827:	66 c7 05 da d3 18 f0 	movw   $0x8,0xf018d3da
f010382e:	08 00 
f0103830:	c6 05 dc d3 18 f0 00 	movb   $0x0,0xf018d3dc
f0103837:	c6 05 dd d3 18 f0 8e 	movb   $0x8e,0xf018d3dd
f010383e:	c1 e8 10             	shr    $0x10,%eax
f0103841:	66 a3 de d3 18 f0    	mov    %ax,0xf018d3de
	SG(44, 0, 0);
f0103847:	b8 0a 3e 10 f0       	mov    $0xf0103e0a,%eax
f010384c:	66 a3 e0 d3 18 f0    	mov    %ax,0xf018d3e0
f0103852:	66 c7 05 e2 d3 18 f0 	movw   $0x8,0xf018d3e2
f0103859:	08 00 
f010385b:	c6 05 e4 d3 18 f0 00 	movb   $0x0,0xf018d3e4
f0103862:	c6 05 e5 d3 18 f0 8e 	movb   $0x8e,0xf018d3e5
f0103869:	c1 e8 10             	shr    $0x10,%eax
f010386c:	66 a3 e6 d3 18 f0    	mov    %ax,0xf018d3e6
	SG(45, 0, 0);
f0103872:	b8 14 3e 10 f0       	mov    $0xf0103e14,%eax
f0103877:	66 a3 e8 d3 18 f0    	mov    %ax,0xf018d3e8
f010387d:	66 c7 05 ea d3 18 f0 	movw   $0x8,0xf018d3ea
f0103884:	08 00 
f0103886:	c6 05 ec d3 18 f0 00 	movb   $0x0,0xf018d3ec
f010388d:	c6 05 ed d3 18 f0 8e 	movb   $0x8e,0xf018d3ed
f0103894:	c1 e8 10             	shr    $0x10,%eax
f0103897:	66 a3 ee d3 18 f0    	mov    %ax,0xf018d3ee
	SG(46, 0, 0);
f010389d:	b8 1e 3e 10 f0       	mov    $0xf0103e1e,%eax
f01038a2:	66 a3 f0 d3 18 f0    	mov    %ax,0xf018d3f0
f01038a8:	66 c7 05 f2 d3 18 f0 	movw   $0x8,0xf018d3f2
f01038af:	08 00 
f01038b1:	c6 05 f4 d3 18 f0 00 	movb   $0x0,0xf018d3f4
f01038b8:	c6 05 f5 d3 18 f0 8e 	movb   $0x8e,0xf018d3f5
f01038bf:	c1 e8 10             	shr    $0x10,%eax
f01038c2:	66 a3 f6 d3 18 f0    	mov    %ax,0xf018d3f6
	SG(47, 0, 0);
f01038c8:	b8 28 3e 10 f0       	mov    $0xf0103e28,%eax
f01038cd:	66 a3 f8 d3 18 f0    	mov    %ax,0xf018d3f8
f01038d3:	66 c7 05 fa d3 18 f0 	movw   $0x8,0xf018d3fa
f01038da:	08 00 
f01038dc:	c6 05 fc d3 18 f0 00 	movb   $0x0,0xf018d3fc
f01038e3:	c6 05 fd d3 18 f0 8e 	movb   $0x8e,0xf018d3fd
f01038ea:	c1 e8 10             	shr    $0x10,%eax
f01038ed:	66 a3 fe d3 18 f0    	mov    %ax,0xf018d3fe
 
	SG(48, 0, 3); //for interrupt
f01038f3:	b8 32 3e 10 f0       	mov    $0xf0103e32,%eax
f01038f8:	66 a3 00 d4 18 f0    	mov    %ax,0xf018d400
f01038fe:	66 c7 05 02 d4 18 f0 	movw   $0x8,0xf018d402
f0103905:	08 00 
f0103907:	c6 05 04 d4 18 f0 00 	movb   $0x0,0xf018d404
f010390e:	c6 05 05 d4 18 f0 ee 	movb   $0xee,0xf018d405
f0103915:	c1 e8 10             	shr    $0x10,%eax
f0103918:	66 a3 06 d4 18 f0    	mov    %ax,0xf018d406
	// warn("trap_init: treat system call as not trap");

	// Per-CPU setup 
	trap_init_percpu();
f010391e:	e8 ba f9 ff ff       	call   f01032dd <trap_init_percpu>
}
f0103923:	5d                   	pop    %ebp
f0103924:	c3                   	ret    

f0103925 <print_regs>:
	}
}

void
print_regs(struct PushRegs *regs)
{
f0103925:	55                   	push   %ebp
f0103926:	89 e5                	mov    %esp,%ebp
f0103928:	53                   	push   %ebx
f0103929:	83 ec 0c             	sub    $0xc,%esp
f010392c:	8b 5d 08             	mov    0x8(%ebp),%ebx
	cprintf("  edi  0x%08x\n", regs->reg_edi);
f010392f:	ff 33                	pushl  (%ebx)
f0103931:	68 bb 62 10 f0       	push   $0xf01062bb
f0103936:	e8 8e f9 ff ff       	call   f01032c9 <cprintf>
	cprintf("  esi  0x%08x\n", regs->reg_esi);
f010393b:	83 c4 08             	add    $0x8,%esp
f010393e:	ff 73 04             	pushl  0x4(%ebx)
f0103941:	68 ca 62 10 f0       	push   $0xf01062ca
f0103946:	e8 7e f9 ff ff       	call   f01032c9 <cprintf>
	cprintf("  ebp  0x%08x\n", regs->reg_ebp);
f010394b:	83 c4 08             	add    $0x8,%esp
f010394e:	ff 73 08             	pushl  0x8(%ebx)
f0103951:	68 d9 62 10 f0       	push   $0xf01062d9
f0103956:	e8 6e f9 ff ff       	call   f01032c9 <cprintf>
	cprintf("  oesp 0x%08x\n", regs->reg_oesp);
f010395b:	83 c4 08             	add    $0x8,%esp
f010395e:	ff 73 0c             	pushl  0xc(%ebx)
f0103961:	68 e8 62 10 f0       	push   $0xf01062e8
f0103966:	e8 5e f9 ff ff       	call   f01032c9 <cprintf>
	cprintf("  ebx  0x%08x\n", regs->reg_ebx);
f010396b:	83 c4 08             	add    $0x8,%esp
f010396e:	ff 73 10             	pushl  0x10(%ebx)
f0103971:	68 f7 62 10 f0       	push   $0xf01062f7
f0103976:	e8 4e f9 ff ff       	call   f01032c9 <cprintf>
	cprintf("  edx  0x%08x\n", regs->reg_edx);
f010397b:	83 c4 08             	add    $0x8,%esp
f010397e:	ff 73 14             	pushl  0x14(%ebx)
f0103981:	68 06 63 10 f0       	push   $0xf0106306
f0103986:	e8 3e f9 ff ff       	call   f01032c9 <cprintf>
	cprintf("  ecx  0x%08x\n", regs->reg_ecx);
f010398b:	83 c4 08             	add    $0x8,%esp
f010398e:	ff 73 18             	pushl  0x18(%ebx)
f0103991:	68 15 63 10 f0       	push   $0xf0106315
f0103996:	e8 2e f9 ff ff       	call   f01032c9 <cprintf>
	cprintf("  eax  0x%08x\n", regs->reg_eax);
f010399b:	83 c4 08             	add    $0x8,%esp
f010399e:	ff 73 1c             	pushl  0x1c(%ebx)
f01039a1:	68 24 63 10 f0       	push   $0xf0106324
f01039a6:	e8 1e f9 ff ff       	call   f01032c9 <cprintf>
}
f01039ab:	83 c4 10             	add    $0x10,%esp
f01039ae:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01039b1:	c9                   	leave  
f01039b2:	c3                   	ret    

f01039b3 <print_trapframe>:
	lidt(&idt_pd);
}

void
print_trapframe(struct Trapframe *tf)
{
f01039b3:	55                   	push   %ebp
f01039b4:	89 e5                	mov    %esp,%ebp
f01039b6:	53                   	push   %ebx
f01039b7:	83 ec 0c             	sub    $0xc,%esp
f01039ba:	8b 5d 08             	mov    0x8(%ebp),%ebx
	cprintf("TRAP frame at %p\n", tf);
f01039bd:	53                   	push   %ebx
f01039be:	68 75 64 10 f0       	push   $0xf0106475
f01039c3:	e8 01 f9 ff ff       	call   f01032c9 <cprintf>
	print_regs(&tf->tf_regs);
f01039c8:	89 1c 24             	mov    %ebx,(%esp)
f01039cb:	e8 55 ff ff ff       	call   f0103925 <print_regs>
	cprintf("  es   0x----%04x\n", tf->tf_es);
f01039d0:	83 c4 08             	add    $0x8,%esp
f01039d3:	0f b7 43 20          	movzwl 0x20(%ebx),%eax
f01039d7:	50                   	push   %eax
f01039d8:	68 75 63 10 f0       	push   $0xf0106375
f01039dd:	e8 e7 f8 ff ff       	call   f01032c9 <cprintf>
	cprintf("  ds   0x----%04x\n", tf->tf_ds);
f01039e2:	83 c4 08             	add    $0x8,%esp
f01039e5:	0f b7 43 24          	movzwl 0x24(%ebx),%eax
f01039e9:	50                   	push   %eax
f01039ea:	68 88 63 10 f0       	push   $0xf0106388
f01039ef:	e8 d5 f8 ff ff       	call   f01032c9 <cprintf>
	cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
f01039f4:	8b 43 28             	mov    0x28(%ebx),%eax
		"Alignment Check",
		"Machine-Check",
		"SIMD Floating-Point Exception"
	};

	if (trapno < ARRAY_SIZE(excnames))
f01039f7:	83 c4 10             	add    $0x10,%esp
f01039fa:	83 f8 13             	cmp    $0x13,%eax
f01039fd:	77 09                	ja     f0103a08 <print_trapframe+0x55>
		return excnames[trapno];
f01039ff:	8b 14 85 60 66 10 f0 	mov    -0xfef99a0(,%eax,4),%edx
f0103a06:	eb 11                	jmp    f0103a19 <print_trapframe+0x66>
	if (trapno == T_SYSCALL)
f0103a08:	83 f8 30             	cmp    $0x30,%eax
f0103a0b:	74 07                	je     f0103a14 <print_trapframe+0x61>
		return "System call";
	return "(unknown trap)";
f0103a0d:	ba 3f 63 10 f0       	mov    $0xf010633f,%edx
f0103a12:	eb 05                	jmp    f0103a19 <print_trapframe+0x66>
	};

	if (trapno < ARRAY_SIZE(excnames))
		return excnames[trapno];
	if (trapno == T_SYSCALL)
		return "System call";
f0103a14:	ba 33 63 10 f0       	mov    $0xf0106333,%edx
{
	cprintf("TRAP frame at %p\n", tf);
	print_regs(&tf->tf_regs);
	cprintf("  es   0x----%04x\n", tf->tf_es);
	cprintf("  ds   0x----%04x\n", tf->tf_ds);
	cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
f0103a19:	83 ec 04             	sub    $0x4,%esp
f0103a1c:	52                   	push   %edx
f0103a1d:	50                   	push   %eax
f0103a1e:	68 9b 63 10 f0       	push   $0xf010639b
f0103a23:	e8 a1 f8 ff ff       	call   f01032c9 <cprintf>
	// If this trap was a page fault that just happened
	// (so %cr2 is meaningful), print the faulting linear address.
	if (tf == last_tf && tf->tf_trapno == T_PGFLT)
f0103a28:	83 c4 10             	add    $0x10,%esp
f0103a2b:	3b 1d 80 da 18 f0    	cmp    0xf018da80,%ebx
f0103a31:	75 1a                	jne    f0103a4d <print_trapframe+0x9a>
f0103a33:	83 7b 28 0e          	cmpl   $0xe,0x28(%ebx)
f0103a37:	75 14                	jne    f0103a4d <print_trapframe+0x9a>

static inline uint32_t
rcr2(void)
{
	uint32_t val;
	asm volatile("movl %%cr2,%0" : "=r" (val));
f0103a39:	0f 20 d0             	mov    %cr2,%eax
		cprintf("  cr2  0x%08x\n", rcr2());
f0103a3c:	83 ec 08             	sub    $0x8,%esp
f0103a3f:	50                   	push   %eax
f0103a40:	68 ad 63 10 f0       	push   $0xf01063ad
f0103a45:	e8 7f f8 ff ff       	call   f01032c9 <cprintf>
f0103a4a:	83 c4 10             	add    $0x10,%esp
	cprintf("  err  0x%08x", tf->tf_err);
f0103a4d:	83 ec 08             	sub    $0x8,%esp
f0103a50:	ff 73 2c             	pushl  0x2c(%ebx)
f0103a53:	68 bc 63 10 f0       	push   $0xf01063bc
f0103a58:	e8 6c f8 ff ff       	call   f01032c9 <cprintf>
	// For page faults, print decoded fault error code:
	// U/K=fault occurred in user/kernel mode
	// W/R=a write/read caused the fault
	// PR=a protection violation caused the fault (NP=page not present).
	if (tf->tf_trapno == T_PGFLT)
f0103a5d:	83 c4 10             	add    $0x10,%esp
f0103a60:	83 7b 28 0e          	cmpl   $0xe,0x28(%ebx)
f0103a64:	75 45                	jne    f0103aab <print_trapframe+0xf8>
		cprintf(" [%s, %s, %s]\n",
			tf->tf_err & 4 ? "user" : "kernel",
			tf->tf_err & 2 ? "write" : "read",
			tf->tf_err & 1 ? "protection" : "not-present");
f0103a66:	8b 43 2c             	mov    0x2c(%ebx),%eax
	// For page faults, print decoded fault error code:
	// U/K=fault occurred in user/kernel mode
	// W/R=a write/read caused the fault
	// PR=a protection violation caused the fault (NP=page not present).
	if (tf->tf_trapno == T_PGFLT)
		cprintf(" [%s, %s, %s]\n",
f0103a69:	a8 01                	test   $0x1,%al
f0103a6b:	75 07                	jne    f0103a74 <print_trapframe+0xc1>
f0103a6d:	b9 59 63 10 f0       	mov    $0xf0106359,%ecx
f0103a72:	eb 05                	jmp    f0103a79 <print_trapframe+0xc6>
f0103a74:	b9 4e 63 10 f0       	mov    $0xf010634e,%ecx
f0103a79:	a8 02                	test   $0x2,%al
f0103a7b:	75 07                	jne    f0103a84 <print_trapframe+0xd1>
f0103a7d:	ba 6b 63 10 f0       	mov    $0xf010636b,%edx
f0103a82:	eb 05                	jmp    f0103a89 <print_trapframe+0xd6>
f0103a84:	ba 65 63 10 f0       	mov    $0xf0106365,%edx
f0103a89:	a8 04                	test   $0x4,%al
f0103a8b:	75 07                	jne    f0103a94 <print_trapframe+0xe1>
f0103a8d:	b8 bd 64 10 f0       	mov    $0xf01064bd,%eax
f0103a92:	eb 05                	jmp    f0103a99 <print_trapframe+0xe6>
f0103a94:	b8 70 63 10 f0       	mov    $0xf0106370,%eax
f0103a99:	51                   	push   %ecx
f0103a9a:	52                   	push   %edx
f0103a9b:	50                   	push   %eax
f0103a9c:	68 ca 63 10 f0       	push   $0xf01063ca
f0103aa1:	e8 23 f8 ff ff       	call   f01032c9 <cprintf>
f0103aa6:	83 c4 10             	add    $0x10,%esp
f0103aa9:	eb 10                	jmp    f0103abb <print_trapframe+0x108>
			tf->tf_err & 4 ? "user" : "kernel",
			tf->tf_err & 2 ? "write" : "read",
			tf->tf_err & 1 ? "protection" : "not-present");
	else
		cprintf("\n");
f0103aab:	83 ec 0c             	sub    $0xc,%esp
f0103aae:	68 85 61 10 f0       	push   $0xf0106185
f0103ab3:	e8 11 f8 ff ff       	call   f01032c9 <cprintf>
f0103ab8:	83 c4 10             	add    $0x10,%esp
	cprintf("  eip  0x%08x\n", tf->tf_eip);
f0103abb:	83 ec 08             	sub    $0x8,%esp
f0103abe:	ff 73 30             	pushl  0x30(%ebx)
f0103ac1:	68 d9 63 10 f0       	push   $0xf01063d9
f0103ac6:	e8 fe f7 ff ff       	call   f01032c9 <cprintf>
	cprintf("  cs   0x----%04x\n", tf->tf_cs);
f0103acb:	83 c4 08             	add    $0x8,%esp
f0103ace:	0f b7 43 34          	movzwl 0x34(%ebx),%eax
f0103ad2:	50                   	push   %eax
f0103ad3:	68 e8 63 10 f0       	push   $0xf01063e8
f0103ad8:	e8 ec f7 ff ff       	call   f01032c9 <cprintf>
	cprintf("  flag 0x%08x\n", tf->tf_eflags);
f0103add:	83 c4 08             	add    $0x8,%esp
f0103ae0:	ff 73 38             	pushl  0x38(%ebx)
f0103ae3:	68 fb 63 10 f0       	push   $0xf01063fb
f0103ae8:	e8 dc f7 ff ff       	call   f01032c9 <cprintf>
	if ((tf->tf_cs & 3) != 0) {
f0103aed:	83 c4 10             	add    $0x10,%esp
f0103af0:	f6 43 34 03          	testb  $0x3,0x34(%ebx)
f0103af4:	74 25                	je     f0103b1b <print_trapframe+0x168>
		cprintf("  esp  0x%08x\n", tf->tf_esp);
f0103af6:	83 ec 08             	sub    $0x8,%esp
f0103af9:	ff 73 3c             	pushl  0x3c(%ebx)
f0103afc:	68 0a 64 10 f0       	push   $0xf010640a
f0103b01:	e8 c3 f7 ff ff       	call   f01032c9 <cprintf>
		cprintf("  ss   0x----%04x\n", tf->tf_ss);
f0103b06:	83 c4 08             	add    $0x8,%esp
f0103b09:	0f b7 43 40          	movzwl 0x40(%ebx),%eax
f0103b0d:	50                   	push   %eax
f0103b0e:	68 19 64 10 f0       	push   $0xf0106419
f0103b13:	e8 b1 f7 ff ff       	call   f01032c9 <cprintf>
f0103b18:	83 c4 10             	add    $0x10,%esp
	}
}
f0103b1b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0103b1e:	c9                   	leave  
f0103b1f:	c3                   	ret    

f0103b20 <page_fault_handler>:
}

//special function to handle page fault
void
page_fault_handler(struct Trapframe *tf)
{
f0103b20:	55                   	push   %ebp
f0103b21:	89 e5                	mov    %esp,%ebp
f0103b23:	53                   	push   %ebx
f0103b24:	83 ec 04             	sub    $0x4,%esp
f0103b27:	8b 5d 08             	mov    0x8(%ebp),%ebx
f0103b2a:	0f 20 d0             	mov    %cr2,%eax

	// Handle kernel-mode page faults.

	// LAB 3: Your code here.
//------------
	if ((tf->tf_cs & 0x3) == 0) {
f0103b2d:	f6 43 34 03          	testb  $0x3,0x34(%ebx)
f0103b31:	75 20                	jne    f0103b53 <page_fault_handler+0x33>
        print_trapframe(tf);
f0103b33:	83 ec 0c             	sub    $0xc,%esp
f0103b36:	53                   	push   %ebx
f0103b37:	e8 77 fe ff ff       	call   f01039b3 <print_trapframe>
		panic("page fault in kernel space");
f0103b3c:	83 c4 0c             	add    $0xc,%esp
f0103b3f:	68 2c 64 10 f0       	push   $0xf010642c
f0103b44:	68 2d 01 00 00       	push   $0x12d
f0103b49:	68 47 64 10 f0       	push   $0xf0106447
f0103b4e:	e8 f9 c4 ff ff       	call   f010004c <_panic>
//------------
	// We've already handled kernel-mode exceptions, so if we get here,
	// the page fault happened in user mode.

	// Destroy the environment that caused the fault.
	cprintf("[%08x] user fault va %08x ip %08x\n",
f0103b53:	ff 73 30             	pushl  0x30(%ebx)
f0103b56:	50                   	push   %eax
f0103b57:	a1 60 d2 18 f0       	mov    0xf018d260,%eax
f0103b5c:	ff 70 48             	pushl  0x48(%eax)
f0103b5f:	68 08 66 10 f0       	push   $0xf0106608
f0103b64:	e8 60 f7 ff ff       	call   f01032c9 <cprintf>
		curenv->env_id, fault_va, tf->tf_eip);
	print_trapframe(tf);
f0103b69:	89 1c 24             	mov    %ebx,(%esp)
f0103b6c:	e8 42 fe ff ff       	call   f01039b3 <print_trapframe>
	env_destroy(curenv);
f0103b71:	83 c4 04             	add    $0x4,%esp
f0103b74:	ff 35 60 d2 18 f0    	pushl  0xf018d260
f0103b7a:	e8 51 f6 ff ff       	call   f01031d0 <env_destroy>
}
f0103b7f:	83 c4 10             	add    $0x10,%esp
f0103b82:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0103b85:	c9                   	leave  
f0103b86:	c3                   	ret    

f0103b87 <trap>:
	}*/
}

void
trap(struct Trapframe *tf)
{
f0103b87:	55                   	push   %ebp
f0103b88:	89 e5                	mov    %esp,%ebp
f0103b8a:	57                   	push   %edi
f0103b8b:	56                   	push   %esi
f0103b8c:	8b 75 08             	mov    0x8(%ebp),%esi
	// The environment may have set DF and some versions
	// of GCC rely on DF being clear
	asm volatile("cld" ::: "cc");
f0103b8f:	fc                   	cld    

static inline uint32_t
read_eflags(void)
{
	uint32_t eflags;
	asm volatile("pushfl; popl %0" : "=r" (eflags));
f0103b90:	9c                   	pushf  
f0103b91:	58                   	pop    %eax

	// Check that interrupts are disabled.  If this assertion
	// fails, DO NOT be tempted to fix it by inserting a "cli" in
	// the interrupt path.
	assert(!(read_eflags() & FL_IF));
f0103b92:	f6 c4 02             	test   $0x2,%ah
f0103b95:	74 19                	je     f0103bb0 <trap+0x29>
f0103b97:	68 53 64 10 f0       	push   $0xf0106453
f0103b9c:	68 98 4f 10 f0       	push   $0xf0104f98
f0103ba1:	68 01 01 00 00       	push   $0x101
f0103ba6:	68 47 64 10 f0       	push   $0xf0106447
f0103bab:	e8 9c c4 ff ff       	call   f010004c <_panic>

	cprintf("Incoming TRAP frame at %p\n", tf);
f0103bb0:	83 ec 08             	sub    $0x8,%esp
f0103bb3:	56                   	push   %esi
f0103bb4:	68 6c 64 10 f0       	push   $0xf010646c
f0103bb9:	e8 0b f7 ff ff       	call   f01032c9 <cprintf>

	if ((tf->tf_cs & 3) == 3) {
f0103bbe:	66 8b 46 34          	mov    0x34(%esi),%ax
f0103bc2:	83 e0 03             	and    $0x3,%eax
f0103bc5:	83 c4 10             	add    $0x10,%esp
f0103bc8:	66 83 f8 03          	cmp    $0x3,%ax
f0103bcc:	75 31                	jne    f0103bff <trap+0x78>
		// Trapped from user mode.
		assert(curenv);
f0103bce:	a1 60 d2 18 f0       	mov    0xf018d260,%eax
f0103bd3:	85 c0                	test   %eax,%eax
f0103bd5:	75 19                	jne    f0103bf0 <trap+0x69>
f0103bd7:	68 87 64 10 f0       	push   $0xf0106487
f0103bdc:	68 98 4f 10 f0       	push   $0xf0104f98
f0103be1:	68 07 01 00 00       	push   $0x107
f0103be6:	68 47 64 10 f0       	push   $0xf0106447
f0103beb:	e8 5c c4 ff ff       	call   f010004c <_panic>

		// Copy trap frame (which is currently on the stack)
		// into 'curenv->env_tf', so that running the environment
		// will restart at the trap point.
		curenv->env_tf = *tf;
f0103bf0:	b9 11 00 00 00       	mov    $0x11,%ecx
f0103bf5:	89 c7                	mov    %eax,%edi
f0103bf7:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
		// The trapframe on the stack should be ignored from here on.
		tf = &curenv->env_tf;
f0103bf9:	8b 35 60 d2 18 f0    	mov    0xf018d260,%esi
	}

	// Record that tf is the last real trapframe so
	// print_trapframe can print some additional information.
	last_tf = tf;
f0103bff:	89 35 80 da 18 f0    	mov    %esi,0xf018da80
	// LAB 3: Your code here.

	// Handle spurious interrupts
	// The hardware sometimes raises these because of noise on the
	// IRQ line or other reasons. We don't care.
	if (tf->tf_trapno == IRQ_OFFSET + IRQ_SPURIOUS) {
f0103c05:	8b 46 28             	mov    0x28(%esi),%eax
f0103c08:	83 f8 27             	cmp    $0x27,%eax
f0103c0b:	75 1d                	jne    f0103c2a <trap+0xa3>
		cprintf("Spurious interrupt on irq 7\n");
f0103c0d:	83 ec 0c             	sub    $0xc,%esp
f0103c10:	68 8e 64 10 f0       	push   $0xf010648e
f0103c15:	e8 af f6 ff ff       	call   f01032c9 <cprintf>
		print_trapframe(tf);
f0103c1a:	89 34 24             	mov    %esi,(%esp)
f0103c1d:	e8 91 fd ff ff       	call   f01039b3 <print_trapframe>
f0103c22:	83 c4 10             	add    $0x10,%esp
f0103c25:	e9 91 00 00 00       	jmp    f0103cbb <trap+0x134>
		return;
	}

	switch (tf->tf_trapno) {
f0103c2a:	83 f8 0e             	cmp    $0xe,%eax
f0103c2d:	74 0c                	je     f0103c3b <trap+0xb4>
f0103c2f:	83 f8 30             	cmp    $0x30,%eax
f0103c32:	74 2b                	je     f0103c5f <trap+0xd8>
f0103c34:	83 f8 03             	cmp    $0x3,%eax
f0103c37:	75 47                	jne    f0103c80 <trap+0xf9>
f0103c39:	eb 0e                	jmp    f0103c49 <trap+0xc2>
		case T_PGFLT:
			page_fault_handler(tf);
f0103c3b:	83 ec 0c             	sub    $0xc,%esp
f0103c3e:	56                   	push   %esi
f0103c3f:	e8 dc fe ff ff       	call   f0103b20 <page_fault_handler>
f0103c44:	83 c4 10             	add    $0x10,%esp
f0103c47:	eb 72                	jmp    f0103cbb <trap+0x134>
			break;
		case T_BRKPT:
			print_trapframe(tf);
f0103c49:	83 ec 0c             	sub    $0xc,%esp
f0103c4c:	56                   	push   %esi
f0103c4d:	e8 61 fd ff ff       	call   f01039b3 <print_trapframe>
			monitor(tf); //invoke the kernel monitor
f0103c52:	89 34 24             	mov    %esi,(%esp)
f0103c55:	e8 45 cc ff ff       	call   f010089f <monitor>
f0103c5a:	83 c4 10             	add    $0x10,%esp
f0103c5d:	eb 5c                	jmp    f0103cbb <trap+0x134>
			break;
		case T_SYSCALL:
			//syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5)
			tf->tf_regs.reg_eax = syscall(
f0103c5f:	83 ec 08             	sub    $0x8,%esp
f0103c62:	ff 76 04             	pushl  0x4(%esi)
f0103c65:	ff 36                	pushl  (%esi)
f0103c67:	ff 76 10             	pushl  0x10(%esi)
f0103c6a:	ff 76 18             	pushl  0x18(%esi)
f0103c6d:	ff 76 14             	pushl  0x14(%esi)
f0103c70:	ff 76 1c             	pushl  0x1c(%esi)
f0103c73:	e8 d5 01 00 00       	call   f0103e4d <syscall>
f0103c78:	89 46 1c             	mov    %eax,0x1c(%esi)
f0103c7b:	83 c4 20             	add    $0x20,%esp
f0103c7e:	eb 3b                	jmp    f0103cbb <trap+0x134>
            tf->tf_regs.reg_esi);
            break;
        //break;
		default:
			// Unexpected trap: The user process or the kernel has a bug.
			print_trapframe(tf);
f0103c80:	83 ec 0c             	sub    $0xc,%esp
f0103c83:	56                   	push   %esi
f0103c84:	e8 2a fd ff ff       	call   f01039b3 <print_trapframe>
			if (tf->tf_cs == GD_KT)
f0103c89:	83 c4 10             	add    $0x10,%esp
f0103c8c:	66 83 7e 34 08       	cmpw   $0x8,0x34(%esi)
f0103c91:	75 17                	jne    f0103caa <trap+0x123>
				panic("unhandled trap in kernel");
f0103c93:	83 ec 04             	sub    $0x4,%esp
f0103c96:	68 ab 64 10 f0       	push   $0xf01064ab
f0103c9b:	68 e7 00 00 00       	push   $0xe7
f0103ca0:	68 47 64 10 f0       	push   $0xf0106447
f0103ca5:	e8 a2 c3 ff ff       	call   f010004c <_panic>
			else {
				env_destroy(curenv);
f0103caa:	83 ec 0c             	sub    $0xc,%esp
f0103cad:	ff 35 60 d2 18 f0    	pushl  0xf018d260
f0103cb3:	e8 18 f5 ff ff       	call   f01031d0 <env_destroy>
f0103cb8:	83 c4 10             	add    $0x10,%esp

	// Dispatch based on what type of trap occurred
	trap_dispatch(tf);

	// Return to the current environment, which should be running.
	assert(curenv && curenv->env_status == ENV_RUNNING);
f0103cbb:	a1 60 d2 18 f0       	mov    0xf018d260,%eax
f0103cc0:	85 c0                	test   %eax,%eax
f0103cc2:	74 06                	je     f0103cca <trap+0x143>
f0103cc4:	83 78 54 03          	cmpl   $0x3,0x54(%eax)
f0103cc8:	74 19                	je     f0103ce3 <trap+0x15c>
f0103cca:	68 2c 66 10 f0       	push   $0xf010662c
f0103ccf:	68 98 4f 10 f0       	push   $0xf0104f98
f0103cd4:	68 19 01 00 00       	push   $0x119
f0103cd9:	68 47 64 10 f0       	push   $0xf0106447
f0103cde:	e8 69 c3 ff ff       	call   f010004c <_panic>
	env_run(curenv);
f0103ce3:	83 ec 0c             	sub    $0xc,%esp
f0103ce6:	50                   	push   %eax
f0103ce7:	e8 34 f5 ff ff       	call   f0103220 <env_run>

f0103cec <handler0>:
#define TY(N) TRAPHANDLER(H(N), N)
#define TN(N) TRAPHANDLER_NOEC(H(N), N)
#define L(N) .long H(N)
#define S(N) .long 0 /* skip */

TN(0)
f0103cec:	6a 00                	push   $0x0
f0103cee:	6a 00                	push   $0x0
f0103cf0:	e9 46 01 00 00       	jmp    f0103e3b <_alltraps>
f0103cf5:	90                   	nop

f0103cf6 <handler1>:
TN(1)
f0103cf6:	6a 00                	push   $0x0
f0103cf8:	6a 01                	push   $0x1
f0103cfa:	e9 3c 01 00 00       	jmp    f0103e3b <_alltraps>
f0103cff:	90                   	nop

f0103d00 <handler2>:
TN(2)
f0103d00:	6a 00                	push   $0x0
f0103d02:	6a 02                	push   $0x2
f0103d04:	e9 32 01 00 00       	jmp    f0103e3b <_alltraps>
f0103d09:	90                   	nop

f0103d0a <handler3>:
TN(3)
f0103d0a:	6a 00                	push   $0x0
f0103d0c:	6a 03                	push   $0x3
f0103d0e:	e9 28 01 00 00       	jmp    f0103e3b <_alltraps>
f0103d13:	90                   	nop

f0103d14 <handler4>:
TN(4)
f0103d14:	6a 00                	push   $0x0
f0103d16:	6a 04                	push   $0x4
f0103d18:	e9 1e 01 00 00       	jmp    f0103e3b <_alltraps>
f0103d1d:	90                   	nop

f0103d1e <handler5>:
TN(5)
f0103d1e:	6a 00                	push   $0x0
f0103d20:	6a 05                	push   $0x5
f0103d22:	e9 14 01 00 00       	jmp    f0103e3b <_alltraps>
f0103d27:	90                   	nop

f0103d28 <handler6>:
TN(6)
f0103d28:	6a 00                	push   $0x0
f0103d2a:	6a 06                	push   $0x6
f0103d2c:	e9 0a 01 00 00       	jmp    f0103e3b <_alltraps>
f0103d31:	90                   	nop

f0103d32 <handler7>:
TN(7)
f0103d32:	6a 00                	push   $0x0
f0103d34:	6a 07                	push   $0x7
f0103d36:	e9 00 01 00 00       	jmp    f0103e3b <_alltraps>
f0103d3b:	90                   	nop

f0103d3c <handler8>:
TY(8)
f0103d3c:	6a 08                	push   $0x8
f0103d3e:	e9 f8 00 00 00       	jmp    f0103e3b <_alltraps>
f0103d43:	90                   	nop

f0103d44 <handler10>:
// TN(9) /* reserved */
TY(10)
f0103d44:	6a 0a                	push   $0xa
f0103d46:	e9 f0 00 00 00       	jmp    f0103e3b <_alltraps>
f0103d4b:	90                   	nop

f0103d4c <handler11>:
TY(11)
f0103d4c:	6a 0b                	push   $0xb
f0103d4e:	e9 e8 00 00 00       	jmp    f0103e3b <_alltraps>
f0103d53:	90                   	nop

f0103d54 <handler12>:
TY(12)
f0103d54:	6a 0c                	push   $0xc
f0103d56:	e9 e0 00 00 00       	jmp    f0103e3b <_alltraps>
f0103d5b:	90                   	nop

f0103d5c <handler13>:
TY(13)
f0103d5c:	6a 0d                	push   $0xd
f0103d5e:	e9 d8 00 00 00       	jmp    f0103e3b <_alltraps>
f0103d63:	90                   	nop

f0103d64 <handler14>:
TY(14)
f0103d64:	6a 0e                	push   $0xe
f0103d66:	e9 d0 00 00 00       	jmp    f0103e3b <_alltraps>
f0103d6b:	90                   	nop

f0103d6c <handler16>:
// TN(15) /* reserved */
TN(16)
f0103d6c:	6a 00                	push   $0x0
f0103d6e:	6a 10                	push   $0x10
f0103d70:	e9 c6 00 00 00       	jmp    f0103e3b <_alltraps>
f0103d75:	90                   	nop

f0103d76 <handler17>:
TY(17)
f0103d76:	6a 11                	push   $0x11
f0103d78:	e9 be 00 00 00       	jmp    f0103e3b <_alltraps>
f0103d7d:	90                   	nop

f0103d7e <handler18>:
TN(18)
f0103d7e:	6a 00                	push   $0x0
f0103d80:	6a 12                	push   $0x12
f0103d82:	e9 b4 00 00 00       	jmp    f0103e3b <_alltraps>
f0103d87:	90                   	nop

f0103d88 <handler19>:
TN(19)
f0103d88:	6a 00                	push   $0x0
f0103d8a:	6a 13                	push   $0x13
f0103d8c:	e9 aa 00 00 00       	jmp    f0103e3b <_alltraps>
f0103d91:	90                   	nop

f0103d92 <handler32>:

/* 16 IRQs */
TN(32)
f0103d92:	6a 00                	push   $0x0
f0103d94:	6a 20                	push   $0x20
f0103d96:	e9 a0 00 00 00       	jmp    f0103e3b <_alltraps>
f0103d9b:	90                   	nop

f0103d9c <handler33>:
TN(33)
f0103d9c:	6a 00                	push   $0x0
f0103d9e:	6a 21                	push   $0x21
f0103da0:	e9 96 00 00 00       	jmp    f0103e3b <_alltraps>
f0103da5:	90                   	nop

f0103da6 <handler34>:
TN(34)
f0103da6:	6a 00                	push   $0x0
f0103da8:	6a 22                	push   $0x22
f0103daa:	e9 8c 00 00 00       	jmp    f0103e3b <_alltraps>
f0103daf:	90                   	nop

f0103db0 <handler35>:
TN(35)
f0103db0:	6a 00                	push   $0x0
f0103db2:	6a 23                	push   $0x23
f0103db4:	e9 82 00 00 00       	jmp    f0103e3b <_alltraps>
f0103db9:	90                   	nop

f0103dba <handler36>:
TN(36)
f0103dba:	6a 00                	push   $0x0
f0103dbc:	6a 24                	push   $0x24
f0103dbe:	e9 78 00 00 00       	jmp    f0103e3b <_alltraps>
f0103dc3:	90                   	nop

f0103dc4 <handler37>:
TN(37)
f0103dc4:	6a 00                	push   $0x0
f0103dc6:	6a 25                	push   $0x25
f0103dc8:	e9 6e 00 00 00       	jmp    f0103e3b <_alltraps>
f0103dcd:	90                   	nop

f0103dce <handler38>:
TN(38)
f0103dce:	6a 00                	push   $0x0
f0103dd0:	6a 26                	push   $0x26
f0103dd2:	e9 64 00 00 00       	jmp    f0103e3b <_alltraps>
f0103dd7:	90                   	nop

f0103dd8 <handler39>:
TN(39)
f0103dd8:	6a 00                	push   $0x0
f0103dda:	6a 27                	push   $0x27
f0103ddc:	e9 5a 00 00 00       	jmp    f0103e3b <_alltraps>
f0103de1:	90                   	nop

f0103de2 <handler40>:
TN(40)
f0103de2:	6a 00                	push   $0x0
f0103de4:	6a 28                	push   $0x28
f0103de6:	e9 50 00 00 00       	jmp    f0103e3b <_alltraps>
f0103deb:	90                   	nop

f0103dec <handler41>:
TN(41)
f0103dec:	6a 00                	push   $0x0
f0103dee:	6a 29                	push   $0x29
f0103df0:	e9 46 00 00 00       	jmp    f0103e3b <_alltraps>
f0103df5:	90                   	nop

f0103df6 <handler42>:
TN(42)
f0103df6:	6a 00                	push   $0x0
f0103df8:	6a 2a                	push   $0x2a
f0103dfa:	e9 3c 00 00 00       	jmp    f0103e3b <_alltraps>
f0103dff:	90                   	nop

f0103e00 <handler43>:
TN(43)
f0103e00:	6a 00                	push   $0x0
f0103e02:	6a 2b                	push   $0x2b
f0103e04:	e9 32 00 00 00       	jmp    f0103e3b <_alltraps>
f0103e09:	90                   	nop

f0103e0a <handler44>:
TN(44)
f0103e0a:	6a 00                	push   $0x0
f0103e0c:	6a 2c                	push   $0x2c
f0103e0e:	e9 28 00 00 00       	jmp    f0103e3b <_alltraps>
f0103e13:	90                   	nop

f0103e14 <handler45>:
TN(45)
f0103e14:	6a 00                	push   $0x0
f0103e16:	6a 2d                	push   $0x2d
f0103e18:	e9 1e 00 00 00       	jmp    f0103e3b <_alltraps>
f0103e1d:	90                   	nop

f0103e1e <handler46>:
TN(46)
f0103e1e:	6a 00                	push   $0x0
f0103e20:	6a 2e                	push   $0x2e
f0103e22:	e9 14 00 00 00       	jmp    f0103e3b <_alltraps>
f0103e27:	90                   	nop

f0103e28 <handler47>:
TN(47)
f0103e28:	6a 00                	push   $0x0
f0103e2a:	6a 2f                	push   $0x2f
f0103e2c:	e9 0a 00 00 00       	jmp    f0103e3b <_alltraps>
f0103e31:	90                   	nop

f0103e32 <handler48>:
	
TN(48)
f0103e32:	6a 00                	push   $0x0
f0103e34:	6a 30                	push   $0x30
f0103e36:	e9 00 00 00 00       	jmp    f0103e3b <_alltraps>

f0103e3b <_alltraps>:
 */

.global _alltraps
_alltraps:
	// Build struct Trapframe
	pushl %ds
f0103e3b:	1e                   	push   %ds
	pushl %es
f0103e3c:	06                   	push   %es
	pushal
f0103e3d:	60                   	pusha  

	movl $GD_KD, %eax
f0103e3e:	b8 10 00 00 00       	mov    $0x10,%eax
	movw %ax, %ds
f0103e43:	8e d8                	mov    %eax,%ds
	movw %ax, %es
f0103e45:	8e c0                	mov    %eax,%es

	pushl %esp /* struct Trapframe * as argument */
f0103e47:	54                   	push   %esp
	call trap
f0103e48:	e8 3a fd ff ff       	call   f0103b87 <trap>

f0103e4d <syscall>:
}

// Dispatches to the correct kernel function, passing the arguments.
int32_t
syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5)
{
f0103e4d:	55                   	push   %ebp
f0103e4e:	89 e5                	mov    %esp,%ebp
f0103e50:	83 ec 18             	sub    $0x18,%esp
f0103e53:	8b 45 08             	mov    0x8(%ebp),%eax
	// Return any appropriate return value.
	// LAB 3: Your code here.

	//panic("syscall not implemented");

	switch (syscallno) {
f0103e56:	83 f8 01             	cmp    $0x1,%eax
f0103e59:	74 35                	je     f0103e90 <syscall+0x43>
f0103e5b:	83 f8 01             	cmp    $0x1,%eax
f0103e5e:	72 13                	jb     f0103e73 <syscall+0x26>
f0103e60:	83 f8 02             	cmp    $0x2,%eax
f0103e63:	0f 84 93 00 00 00    	je     f0103efc <syscall+0xaf>
f0103e69:	83 f8 03             	cmp    $0x3,%eax
f0103e6c:	74 29                	je     f0103e97 <syscall+0x4a>
f0103e6e:	e9 93 00 00 00       	jmp    f0103f06 <syscall+0xb9>
	// Destroy the environment if not.

	// LAB 3: Your code here.

	// Print the string supplied by the user.
	cprintf("%.*s", len, s);
f0103e73:	83 ec 04             	sub    $0x4,%esp
f0103e76:	ff 75 0c             	pushl  0xc(%ebp)
f0103e79:	ff 75 10             	pushl  0x10(%ebp)
f0103e7c:	68 b0 66 10 f0       	push   $0xf01066b0
f0103e81:	e8 43 f4 ff ff       	call   f01032c9 <cprintf>
f0103e86:	83 c4 10             	add    $0x10,%esp
	//panic("syscall not implemented");

	switch (syscallno) {
		case SYS_cputs:
			sys_cputs((char*)a1, (size_t)a2);  //Print a string to the system console.
			return 0;
f0103e89:	b8 00 00 00 00       	mov    $0x0,%eax
f0103e8e:	eb 7b                	jmp    f0103f0b <syscall+0xbe>
// Read a character from the system console without blocking.
// Returns the character, or 0 if there is no input waiting.
static int
sys_cgetc(void)
{
	return cons_getc();
f0103e90:	e8 5c c6 ff ff       	call   f01004f1 <cons_getc>
		case SYS_cputs:
			sys_cputs((char*)a1, (size_t)a2);  //Print a string to the system console.
			return 0;

		case SYS_cgetc:
			return sys_cgetc(); //Read a character from the system console without blocking.
f0103e95:	eb 74                	jmp    f0103f0b <syscall+0xbe>
sys_env_destroy(envid_t envid)
{
	int r;
	struct Env *e;

	if ((r = envid2env(envid, &e, 1)) < 0)
f0103e97:	83 ec 04             	sub    $0x4,%esp
f0103e9a:	6a 01                	push   $0x1
f0103e9c:	8d 45 f4             	lea    -0xc(%ebp),%eax
f0103e9f:	50                   	push   %eax
f0103ea0:	ff 75 10             	pushl  0x10(%ebp)
f0103ea3:	e8 c1 ed ff ff       	call   f0102c69 <envid2env>
f0103ea8:	83 c4 10             	add    $0x10,%esp
f0103eab:	85 c0                	test   %eax,%eax
f0103ead:	78 5c                	js     f0103f0b <syscall+0xbe>
		return r;
	if (e == curenv)
f0103eaf:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103eb2:	8b 15 60 d2 18 f0    	mov    0xf018d260,%edx
f0103eb8:	39 d0                	cmp    %edx,%eax
f0103eba:	75 15                	jne    f0103ed1 <syscall+0x84>
		cprintf("[%08x] exiting gracefully\n", curenv->env_id);
f0103ebc:	83 ec 08             	sub    $0x8,%esp
f0103ebf:	ff 70 48             	pushl  0x48(%eax)
f0103ec2:	68 b5 66 10 f0       	push   $0xf01066b5
f0103ec7:	e8 fd f3 ff ff       	call   f01032c9 <cprintf>
f0103ecc:	83 c4 10             	add    $0x10,%esp
f0103ecf:	eb 16                	jmp    f0103ee7 <syscall+0x9a>
	else
		cprintf("[%08x] destroying %08x\n", curenv->env_id, e->env_id);
f0103ed1:	83 ec 04             	sub    $0x4,%esp
f0103ed4:	ff 70 48             	pushl  0x48(%eax)
f0103ed7:	ff 72 48             	pushl  0x48(%edx)
f0103eda:	68 d0 66 10 f0       	push   $0xf01066d0
f0103edf:	e8 e5 f3 ff ff       	call   f01032c9 <cprintf>
f0103ee4:	83 c4 10             	add    $0x10,%esp
	env_destroy(e);
f0103ee7:	83 ec 0c             	sub    $0xc,%esp
f0103eea:	ff 75 f4             	pushl  -0xc(%ebp)
f0103eed:	e8 de f2 ff ff       	call   f01031d0 <env_destroy>
f0103ef2:	83 c4 10             	add    $0x10,%esp
	return 0;
f0103ef5:	b8 00 00 00 00       	mov    $0x0,%eax
f0103efa:	eb 0f                	jmp    f0103f0b <syscall+0xbe>

// Returns the current environment's envid.
static envid_t
sys_getenvid(void)
{
	return curenv->env_id;
f0103efc:	a1 60 d2 18 f0       	mov    0xf018d260,%eax
f0103f01:	8b 40 48             	mov    0x48(%eax),%eax

		case SYS_env_destroy:
			return sys_env_destroy(a2); //Destroy a given environment (possibly the currently running environment).

		case SYS_getenvid:
			return sys_getenvid(); //get the current environment's envid.
f0103f04:	eb 05                	jmp    f0103f0b <syscall+0xbe>

		default:
			return -E_INVAL;
f0103f06:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
}
f0103f0b:	c9                   	leave  
f0103f0c:	c3                   	ret    

f0103f0d <stab_binsearch>:
//	will exit setting left = 118, right = 554.
//
static void
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
f0103f0d:	55                   	push   %ebp
f0103f0e:	89 e5                	mov    %esp,%ebp
f0103f10:	57                   	push   %edi
f0103f11:	56                   	push   %esi
f0103f12:	53                   	push   %ebx
f0103f13:	83 ec 14             	sub    $0x14,%esp
f0103f16:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0103f19:	89 55 e4             	mov    %edx,-0x1c(%ebp)
f0103f1c:	89 4d e0             	mov    %ecx,-0x20(%ebp)
f0103f1f:	8b 7d 08             	mov    0x8(%ebp),%edi
	int l = *region_left, r = *region_right, any_matches = 0;
f0103f22:	8b 1a                	mov    (%edx),%ebx
f0103f24:	8b 01                	mov    (%ecx),%eax
f0103f26:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0103f29:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)

	while (l <= r) {
f0103f30:	eb 7e                	jmp    f0103fb0 <stab_binsearch+0xa3>
		int true_m = (l + r) / 2, m = true_m;
f0103f32:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103f35:	01 d8                	add    %ebx,%eax
f0103f37:	89 c6                	mov    %eax,%esi
f0103f39:	c1 ee 1f             	shr    $0x1f,%esi
f0103f3c:	01 c6                	add    %eax,%esi
f0103f3e:	d1 fe                	sar    %esi
f0103f40:	8d 04 36             	lea    (%esi,%esi,1),%eax
f0103f43:	01 f0                	add    %esi,%eax
f0103f45:	8b 4d ec             	mov    -0x14(%ebp),%ecx
f0103f48:	8d 54 81 04          	lea    0x4(%ecx,%eax,4),%edx
f0103f4c:	89 f0                	mov    %esi,%eax

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f0103f4e:	eb 01                	jmp    f0103f51 <stab_binsearch+0x44>
			m--;
f0103f50:	48                   	dec    %eax

	while (l <= r) {
		int true_m = (l + r) / 2, m = true_m;

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f0103f51:	39 c3                	cmp    %eax,%ebx
f0103f53:	7f 0c                	jg     f0103f61 <stab_binsearch+0x54>
f0103f55:	0f b6 0a             	movzbl (%edx),%ecx
f0103f58:	83 ea 0c             	sub    $0xc,%edx
f0103f5b:	39 f9                	cmp    %edi,%ecx
f0103f5d:	75 f1                	jne    f0103f50 <stab_binsearch+0x43>
f0103f5f:	eb 05                	jmp    f0103f66 <stab_binsearch+0x59>
			m--;
		if (m < l) {	// no match in [l, m]
			l = true_m + 1;
f0103f61:	8d 5e 01             	lea    0x1(%esi),%ebx
			continue;
f0103f64:	eb 4a                	jmp    f0103fb0 <stab_binsearch+0xa3>
		}

		// actual binary search
		any_matches = 1;
		if (stabs[m].n_value < addr) {
f0103f66:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103f69:	01 c2                	add    %eax,%edx
f0103f6b:	8b 4d ec             	mov    -0x14(%ebp),%ecx
f0103f6e:	8b 54 91 08          	mov    0x8(%ecx,%edx,4),%edx
f0103f72:	39 55 0c             	cmp    %edx,0xc(%ebp)
f0103f75:	76 11                	jbe    f0103f88 <stab_binsearch+0x7b>
			*region_left = m;
f0103f77:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
f0103f7a:	89 03                	mov    %eax,(%ebx)
			l = true_m + 1;
f0103f7c:	8d 5e 01             	lea    0x1(%esi),%ebx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0103f7f:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
f0103f86:	eb 28                	jmp    f0103fb0 <stab_binsearch+0xa3>
		if (stabs[m].n_value < addr) {
			*region_left = m;
			l = true_m + 1;
		} else if (stabs[m].n_value > addr) {
f0103f88:	39 55 0c             	cmp    %edx,0xc(%ebp)
f0103f8b:	73 12                	jae    f0103f9f <stab_binsearch+0x92>
			*region_right = m - 1;
f0103f8d:	48                   	dec    %eax
f0103f8e:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0103f91:	8b 75 e0             	mov    -0x20(%ebp),%esi
f0103f94:	89 06                	mov    %eax,(%esi)
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0103f96:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
f0103f9d:	eb 11                	jmp    f0103fb0 <stab_binsearch+0xa3>
			*region_right = m - 1;
			r = m - 1;
		} else {
			// exact match for 'addr', but continue loop to find
			// *region_right
			*region_left = m;
f0103f9f:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f0103fa2:	89 06                	mov    %eax,(%esi)
			l = m;
			addr++;
f0103fa4:	ff 45 0c             	incl   0xc(%ebp)
f0103fa7:	89 c3                	mov    %eax,%ebx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0103fa9:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
	int l = *region_left, r = *region_right, any_matches = 0;

	while (l <= r) {
f0103fb0:	3b 5d f0             	cmp    -0x10(%ebp),%ebx
f0103fb3:	0f 8e 79 ff ff ff    	jle    f0103f32 <stab_binsearch+0x25>
			l = m;
			addr++;
		}
	}

	if (!any_matches)
f0103fb9:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
f0103fbd:	75 0d                	jne    f0103fcc <stab_binsearch+0xbf>
		*region_right = *region_left - 1;
f0103fbf:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0103fc2:	8b 00                	mov    (%eax),%eax
f0103fc4:	48                   	dec    %eax
f0103fc5:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0103fc8:	89 07                	mov    %eax,(%edi)
f0103fca:	eb 2c                	jmp    f0103ff8 <stab_binsearch+0xeb>
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0103fcc:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0103fcf:	8b 00                	mov    (%eax),%eax
		     l > *region_left && stabs[l].n_type != type;
f0103fd1:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f0103fd4:	8b 0e                	mov    (%esi),%ecx
f0103fd6:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0103fd9:	01 c2                	add    %eax,%edx
f0103fdb:	8b 75 ec             	mov    -0x14(%ebp),%esi
f0103fde:	8d 54 96 04          	lea    0x4(%esi,%edx,4),%edx

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0103fe2:	eb 01                	jmp    f0103fe5 <stab_binsearch+0xd8>
		     l > *region_left && stabs[l].n_type != type;
		     l--)
f0103fe4:	48                   	dec    %eax

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0103fe5:	39 c8                	cmp    %ecx,%eax
f0103fe7:	7e 0a                	jle    f0103ff3 <stab_binsearch+0xe6>
		     l > *region_left && stabs[l].n_type != type;
f0103fe9:	0f b6 1a             	movzbl (%edx),%ebx
f0103fec:	83 ea 0c             	sub    $0xc,%edx
f0103fef:	39 df                	cmp    %ebx,%edi
f0103ff1:	75 f1                	jne    f0103fe4 <stab_binsearch+0xd7>
		     l--)
			/* do nothing */;
		*region_left = l;
f0103ff3:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0103ff6:	89 07                	mov    %eax,(%edi)
	}
}
f0103ff8:	83 c4 14             	add    $0x14,%esp
f0103ffb:	5b                   	pop    %ebx
f0103ffc:	5e                   	pop    %esi
f0103ffd:	5f                   	pop    %edi
f0103ffe:	5d                   	pop    %ebp
f0103fff:	c3                   	ret    

f0104000 <debuginfo_eip>:
//	negative if not.  But even if it returns negative it has stored some
//	information into '*info'.
//
int
debuginfo_eip(uintptr_t addr, struct Eipdebuginfo *info)
{
f0104000:	55                   	push   %ebp
f0104001:	89 e5                	mov    %esp,%ebp
f0104003:	57                   	push   %edi
f0104004:	56                   	push   %esi
f0104005:	53                   	push   %ebx
f0104006:	83 ec 3c             	sub    $0x3c,%esp
f0104009:	8b 5d 08             	mov    0x8(%ebp),%ebx
f010400c:	8b 75 0c             	mov    0xc(%ebp),%esi
	const struct Stab *stabs, *stab_end;
	const char *stabstr, *stabstr_end;
	int lfile, rfile, lfun, rfun, lline, rline;

	// Initialize *info
	info->eip_file = "<unknown>";
f010400f:	c7 06 e8 66 10 f0    	movl   $0xf01066e8,(%esi)
	info->eip_line = 0;
f0104015:	c7 46 04 00 00 00 00 	movl   $0x0,0x4(%esi)
	info->eip_fn_name = "<unknown>";
f010401c:	c7 46 08 e8 66 10 f0 	movl   $0xf01066e8,0x8(%esi)
	info->eip_fn_namelen = 9;
f0104023:	c7 46 0c 09 00 00 00 	movl   $0x9,0xc(%esi)
	info->eip_fn_addr = addr;
f010402a:	89 5e 10             	mov    %ebx,0x10(%esi)
	info->eip_fn_narg = 0;
f010402d:	c7 46 14 00 00 00 00 	movl   $0x0,0x14(%esi)

	// Find the relevant set of stabs
	if (addr >= ULIM) {
f0104034:	81 fb ff ff 7f ef    	cmp    $0xef7fffff,%ebx
f010403a:	77 21                	ja     f010405d <debuginfo_eip+0x5d>

		// Make sure this memory is valid.
		// Return -1 if it is not.  Hint: Call user_mem_check.
		// LAB 3: Your code here.

		stabs = usd->stabs;
f010403c:	a1 00 00 20 00       	mov    0x200000,%eax
f0104041:	89 45 bc             	mov    %eax,-0x44(%ebp)
		stab_end = usd->stab_end;
f0104044:	a1 04 00 20 00       	mov    0x200004,%eax
		stabstr = usd->stabstr;
f0104049:	8b 3d 08 00 20 00    	mov    0x200008,%edi
f010404f:	89 7d b8             	mov    %edi,-0x48(%ebp)
		stabstr_end = usd->stabstr_end;
f0104052:	8b 3d 0c 00 20 00    	mov    0x20000c,%edi
f0104058:	89 7d c0             	mov    %edi,-0x40(%ebp)
f010405b:	eb 1a                	jmp    f0104077 <debuginfo_eip+0x77>
	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
		stab_end = __STAB_END__;
		stabstr = __STABSTR_BEGIN__;
		stabstr_end = __STABSTR_END__;
f010405d:	c7 45 c0 76 24 11 f0 	movl   $0xf0112476,-0x40(%ebp)

	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
		stab_end = __STAB_END__;
		stabstr = __STABSTR_BEGIN__;
f0104064:	c7 45 b8 55 ee 10 f0 	movl   $0xf010ee55,-0x48(%ebp)
	info->eip_fn_narg = 0;

	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
		stab_end = __STAB_END__;
f010406b:	b8 54 ee 10 f0       	mov    $0xf010ee54,%eax
	info->eip_fn_addr = addr;
	info->eip_fn_narg = 0;

	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
f0104070:	c7 45 bc f0 6d 10 f0 	movl   $0xf0106df0,-0x44(%ebp)
		// Make sure the STABS and string table memory is valid.
		// LAB 3: Your code here.
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
f0104077:	8b 7d c0             	mov    -0x40(%ebp),%edi
f010407a:	39 7d b8             	cmp    %edi,-0x48(%ebp)
f010407d:	0f 83 9d 01 00 00    	jae    f0104220 <debuginfo_eip+0x220>
f0104083:	80 7f ff 00          	cmpb   $0x0,-0x1(%edi)
f0104087:	0f 85 9a 01 00 00    	jne    f0104227 <debuginfo_eip+0x227>
	// 'eip'.  First, we find the basic source file containing 'eip'.
	// Then, we look in that source file for the function.  Then we look
	// for the line number.

	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
f010408d:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
	rfile = (stab_end - stabs) - 1;
f0104094:	8b 7d bc             	mov    -0x44(%ebp),%edi
f0104097:	29 f8                	sub    %edi,%eax
f0104099:	c1 f8 02             	sar    $0x2,%eax
f010409c:	8d 14 80             	lea    (%eax,%eax,4),%edx
f010409f:	8d 14 90             	lea    (%eax,%edx,4),%edx
f01040a2:	8d 0c 90             	lea    (%eax,%edx,4),%ecx
f01040a5:	89 ca                	mov    %ecx,%edx
f01040a7:	c1 e2 08             	shl    $0x8,%edx
f01040aa:	01 d1                	add    %edx,%ecx
f01040ac:	89 ca                	mov    %ecx,%edx
f01040ae:	c1 e2 10             	shl    $0x10,%edx
f01040b1:	01 ca                	add    %ecx,%edx
f01040b3:	01 d2                	add    %edx,%edx
f01040b5:	8d 44 10 ff          	lea    -0x1(%eax,%edx,1),%eax
f01040b9:	89 45 e0             	mov    %eax,-0x20(%ebp)
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
f01040bc:	53                   	push   %ebx
f01040bd:	6a 64                	push   $0x64
f01040bf:	8d 45 e0             	lea    -0x20(%ebp),%eax
f01040c2:	89 c1                	mov    %eax,%ecx
f01040c4:	8d 55 e4             	lea    -0x1c(%ebp),%edx
f01040c7:	89 f8                	mov    %edi,%eax
f01040c9:	e8 3f fe ff ff       	call   f0103f0d <stab_binsearch>
	if (lfile == 0)
f01040ce:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01040d1:	83 c4 08             	add    $0x8,%esp
f01040d4:	85 c0                	test   %eax,%eax
f01040d6:	0f 84 52 01 00 00    	je     f010422e <debuginfo_eip+0x22e>
		return -1;

	// Search within that file's stabs for the function definition
	// (N_FUN).
	lfun = lfile;
f01040dc:	89 45 dc             	mov    %eax,-0x24(%ebp)
	rfun = rfile;
f01040df:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01040e2:	89 45 d8             	mov    %eax,-0x28(%ebp)
	stab_binsearch(stabs, &lfun, &rfun, N_FUN, addr);
f01040e5:	53                   	push   %ebx
f01040e6:	6a 24                	push   $0x24
f01040e8:	8d 45 d8             	lea    -0x28(%ebp),%eax
f01040eb:	89 c1                	mov    %eax,%ecx
f01040ed:	8d 55 dc             	lea    -0x24(%ebp),%edx
f01040f0:	89 f8                	mov    %edi,%eax
f01040f2:	e8 16 fe ff ff       	call   f0103f0d <stab_binsearch>

	if (lfun <= rfun) {
f01040f7:	8b 45 dc             	mov    -0x24(%ebp),%eax
f01040fa:	8b 55 d8             	mov    -0x28(%ebp),%edx
f01040fd:	89 55 c4             	mov    %edx,-0x3c(%ebp)
f0104100:	83 c4 08             	add    $0x8,%esp
f0104103:	39 d0                	cmp    %edx,%eax
f0104105:	7f 2d                	jg     f0104134 <debuginfo_eip+0x134>
		// stabs[lfun] points to the function name
		// in the string table, but check bounds just in case.
		if (stabs[lfun].n_strx < stabstr_end - stabstr)
f0104107:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010410a:	01 c2                	add    %eax,%edx
f010410c:	8d 0c 97             	lea    (%edi,%edx,4),%ecx
f010410f:	8b 11                	mov    (%ecx),%edx
f0104111:	8b 7d c0             	mov    -0x40(%ebp),%edi
f0104114:	2b 7d b8             	sub    -0x48(%ebp),%edi
f0104117:	39 fa                	cmp    %edi,%edx
f0104119:	73 06                	jae    f0104121 <debuginfo_eip+0x121>
			info->eip_fn_name = stabstr + stabs[lfun].n_strx;
f010411b:	03 55 b8             	add    -0x48(%ebp),%edx
f010411e:	89 56 08             	mov    %edx,0x8(%esi)
		info->eip_fn_addr = stabs[lfun].n_value;
f0104121:	8b 51 08             	mov    0x8(%ecx),%edx
f0104124:	89 56 10             	mov    %edx,0x10(%esi)
		addr -= info->eip_fn_addr;
f0104127:	29 d3                	sub    %edx,%ebx
		// Search within the function definition for the line number.
		lline = lfun;
f0104129:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		rline = rfun;
f010412c:	8b 45 c4             	mov    -0x3c(%ebp),%eax
f010412f:	89 45 d0             	mov    %eax,-0x30(%ebp)
f0104132:	eb 0f                	jmp    f0104143 <debuginfo_eip+0x143>
	} else {
		// Couldn't find function stab!  Maybe we're in an assembly
		// file.  Search the whole file for the line number.
		info->eip_fn_addr = addr;
f0104134:	89 5e 10             	mov    %ebx,0x10(%esi)
		lline = lfile;
f0104137:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f010413a:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		rline = rfile;
f010413d:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0104140:	89 45 d0             	mov    %eax,-0x30(%ebp)
	}
	// Ignore stuff after the colon.
	info->eip_fn_namelen = strfind(info->eip_fn_name, ':') - info->eip_fn_name;
f0104143:	83 ec 08             	sub    $0x8,%esp
f0104146:	6a 3a                	push   $0x3a
f0104148:	ff 76 08             	pushl  0x8(%esi)
f010414b:	e8 b0 09 00 00       	call   f0104b00 <strfind>
f0104150:	2b 46 08             	sub    0x8(%esi),%eax
f0104153:	89 46 0c             	mov    %eax,0xc(%esi)
	// Hint:
	//	There's a particular stabs type used for line numbers.
	//	Look at the STABS documentation and <inc/stab.h> to find
	//	which one.
	// Your code here.
	stab_binsearch(stabs, &lline, &rline, N_SLINE, addr);
f0104156:	83 c4 08             	add    $0x8,%esp
f0104159:	53                   	push   %ebx
f010415a:	6a 44                	push   $0x44
f010415c:	8d 4d d0             	lea    -0x30(%ebp),%ecx
f010415f:	8d 55 d4             	lea    -0x2c(%ebp),%edx
f0104162:	8b 5d bc             	mov    -0x44(%ebp),%ebx
f0104165:	89 d8                	mov    %ebx,%eax
f0104167:	e8 a1 fd ff ff       	call   f0103f0d <stab_binsearch>
	info->eip_line = stabs[lline].n_value;
f010416c:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f010416f:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104172:	01 c2                	add    %eax,%edx
f0104174:	c1 e2 02             	shl    $0x2,%edx
f0104177:	8b 4c 13 08          	mov    0x8(%ebx,%edx,1),%ecx
f010417b:	89 4e 04             	mov    %ecx,0x4(%esi)
	// Search backwards from the line number for the relevant filename
	// stab.
	// We can't just use the "lfile" stab because inlined functions
	// can interpolate code from a different file!
	// Such included source files use the N_SOL stab type.
	while (lline >= lfile
f010417e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0104181:	8d 54 13 08          	lea    0x8(%ebx,%edx,1),%edx
f0104185:	83 c4 10             	add    $0x10,%esp
f0104188:	c6 45 c4 00          	movb   $0x0,-0x3c(%ebp)
f010418c:	89 75 0c             	mov    %esi,0xc(%ebp)
f010418f:	eb 08                	jmp    f0104199 <debuginfo_eip+0x199>
f0104191:	48                   	dec    %eax
f0104192:	83 ea 0c             	sub    $0xc,%edx
f0104195:	c6 45 c4 01          	movb   $0x1,-0x3c(%ebp)
f0104199:	39 c7                	cmp    %eax,%edi
f010419b:	7e 05                	jle    f01041a2 <debuginfo_eip+0x1a2>
f010419d:	8b 75 0c             	mov    0xc(%ebp),%esi
f01041a0:	eb 47                	jmp    f01041e9 <debuginfo_eip+0x1e9>
	       && stabs[lline].n_type != N_SOL
f01041a2:	8a 4a fc             	mov    -0x4(%edx),%cl
f01041a5:	80 f9 84             	cmp    $0x84,%cl
f01041a8:	75 0e                	jne    f01041b8 <debuginfo_eip+0x1b8>
f01041aa:	8b 75 0c             	mov    0xc(%ebp),%esi
f01041ad:	80 7d c4 00          	cmpb   $0x0,-0x3c(%ebp)
f01041b1:	74 1b                	je     f01041ce <debuginfo_eip+0x1ce>
f01041b3:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f01041b6:	eb 16                	jmp    f01041ce <debuginfo_eip+0x1ce>
	       && (stabs[lline].n_type != N_SO || !stabs[lline].n_value))
f01041b8:	80 f9 64             	cmp    $0x64,%cl
f01041bb:	75 d4                	jne    f0104191 <debuginfo_eip+0x191>
f01041bd:	83 3a 00             	cmpl   $0x0,(%edx)
f01041c0:	74 cf                	je     f0104191 <debuginfo_eip+0x191>
f01041c2:	8b 75 0c             	mov    0xc(%ebp),%esi
f01041c5:	80 7d c4 00          	cmpb   $0x0,-0x3c(%ebp)
f01041c9:	74 03                	je     f01041ce <debuginfo_eip+0x1ce>
f01041cb:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		lline--;
	if (lline >= lfile && stabs[lline].n_strx < stabstr_end - stabstr)
f01041ce:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01041d1:	01 d0                	add    %edx,%eax
f01041d3:	8b 7d bc             	mov    -0x44(%ebp),%edi
f01041d6:	8b 14 87             	mov    (%edi,%eax,4),%edx
f01041d9:	8b 45 c0             	mov    -0x40(%ebp),%eax
f01041dc:	8b 7d b8             	mov    -0x48(%ebp),%edi
f01041df:	29 f8                	sub    %edi,%eax
f01041e1:	39 c2                	cmp    %eax,%edx
f01041e3:	73 04                	jae    f01041e9 <debuginfo_eip+0x1e9>
		info->eip_file = stabstr + stabs[lline].n_strx;
f01041e5:	01 fa                	add    %edi,%edx
f01041e7:	89 16                	mov    %edx,(%esi)


	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	if (lfun < rfun)
f01041e9:	8b 55 dc             	mov    -0x24(%ebp),%edx
f01041ec:	8b 5d d8             	mov    -0x28(%ebp),%ebx
f01041ef:	39 da                	cmp    %ebx,%edx
f01041f1:	7d 42                	jge    f0104235 <debuginfo_eip+0x235>
		for (lline = lfun + 1;
f01041f3:	42                   	inc    %edx
f01041f4:	89 55 d4             	mov    %edx,-0x2c(%ebp)
f01041f7:	89 d0                	mov    %edx,%eax
f01041f9:	8d 0c 12             	lea    (%edx,%edx,1),%ecx
f01041fc:	01 ca                	add    %ecx,%edx
f01041fe:	8b 7d bc             	mov    -0x44(%ebp),%edi
f0104201:	8d 54 97 04          	lea    0x4(%edi,%edx,4),%edx
f0104205:	eb 03                	jmp    f010420a <debuginfo_eip+0x20a>
		     lline < rfun && stabs[lline].n_type == N_PSYM;
		     lline++)
			info->eip_fn_narg++;
f0104207:	ff 46 14             	incl   0x14(%esi)


	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	if (lfun < rfun)
		for (lline = lfun + 1;
f010420a:	39 c3                	cmp    %eax,%ebx
f010420c:	7e 2e                	jle    f010423c <debuginfo_eip+0x23c>
		     lline < rfun && stabs[lline].n_type == N_PSYM;
f010420e:	8a 0a                	mov    (%edx),%cl
f0104210:	40                   	inc    %eax
f0104211:	83 c2 0c             	add    $0xc,%edx
f0104214:	80 f9 a0             	cmp    $0xa0,%cl
f0104217:	74 ee                	je     f0104207 <debuginfo_eip+0x207>
		     lline++)
			info->eip_fn_narg++;

	return 0;
f0104219:	b8 00 00 00 00       	mov    $0x0,%eax
f010421e:	eb 21                	jmp    f0104241 <debuginfo_eip+0x241>
		// LAB 3: Your code here.
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
		return -1;
f0104220:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0104225:	eb 1a                	jmp    f0104241 <debuginfo_eip+0x241>
f0104227:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f010422c:	eb 13                	jmp    f0104241 <debuginfo_eip+0x241>
	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
	rfile = (stab_end - stabs) - 1;
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
	if (lfile == 0)
		return -1;
f010422e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0104233:	eb 0c                	jmp    f0104241 <debuginfo_eip+0x241>
		for (lline = lfun + 1;
		     lline < rfun && stabs[lline].n_type == N_PSYM;
		     lline++)
			info->eip_fn_narg++;

	return 0;
f0104235:	b8 00 00 00 00       	mov    $0x0,%eax
f010423a:	eb 05                	jmp    f0104241 <debuginfo_eip+0x241>
f010423c:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0104241:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0104244:	5b                   	pop    %ebx
f0104245:	5e                   	pop    %esi
f0104246:	5f                   	pop    %edi
f0104247:	5d                   	pop    %ebp
f0104248:	c3                   	ret    

f0104249 <cpuid_print>:
	return feature[bit / 32] & BIT(bit % 32);
}

void
cpuid_print(void)
{
f0104249:	55                   	push   %ebp
f010424a:	89 e5                	mov    %esp,%ebp
f010424c:	57                   	push   %edi
f010424d:	56                   	push   %esi
f010424e:	53                   	push   %ebx
f010424f:	83 ec 6c             	sub    $0x6c,%esp
	uint32_t eax, brand[12], feature[CPUID_NR_FLAGS] = {0};
f0104252:	8d 7d a4             	lea    -0x5c(%ebp),%edi
f0104255:	b9 05 00 00 00       	mov    $0x5,%ecx
f010425a:	b8 00 00 00 00       	mov    $0x0,%eax
f010425f:	f3 ab                	rep stos %eax,%es:(%edi)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f0104261:	b8 00 00 00 80       	mov    $0x80000000,%eax
f0104266:	0f a2                	cpuid  

	cpuid(0x80000000, &eax, NULL, NULL, NULL);
	if (eax < 0x80000004)
f0104268:	3d 03 00 00 80       	cmp    $0x80000003,%eax
f010426d:	77 17                	ja     f0104286 <cpuid_print+0x3d>
		panic("CPU too old!");
f010426f:	83 ec 04             	sub    $0x4,%esp
f0104272:	68 f2 66 10 f0       	push   $0xf01066f2
f0104277:	68 8d 00 00 00       	push   $0x8d
f010427c:	68 ff 66 10 f0       	push   $0xf01066ff
f0104281:	e8 c6 bd ff ff       	call   f010004c <_panic>
f0104286:	b8 02 00 00 80       	mov    $0x80000002,%eax
f010428b:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f010428d:	89 45 b8             	mov    %eax,-0x48(%ebp)
	if (ebxp)
		*ebxp = ebx;
f0104290:	89 5d bc             	mov    %ebx,-0x44(%ebp)
	if (ecxp)
		*ecxp = ecx;
f0104293:	89 4d c0             	mov    %ecx,-0x40(%ebp)
	if (edxp)
		*edxp = edx;
f0104296:	89 55 c4             	mov    %edx,-0x3c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f0104299:	b8 03 00 00 80       	mov    $0x80000003,%eax
f010429e:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f01042a0:	89 45 c8             	mov    %eax,-0x38(%ebp)
	if (ebxp)
		*ebxp = ebx;
f01042a3:	89 5d cc             	mov    %ebx,-0x34(%ebp)
	if (ecxp)
		*ecxp = ecx;
f01042a6:	89 4d d0             	mov    %ecx,-0x30(%ebp)
	if (edxp)
		*edxp = edx;
f01042a9:	89 55 d4             	mov    %edx,-0x2c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f01042ac:	b8 04 00 00 80       	mov    $0x80000004,%eax
f01042b1:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f01042b3:	89 45 d8             	mov    %eax,-0x28(%ebp)
	if (ebxp)
		*ebxp = ebx;
f01042b6:	89 5d dc             	mov    %ebx,-0x24(%ebp)
	if (ecxp)
		*ecxp = ecx;
f01042b9:	89 4d e0             	mov    %ecx,-0x20(%ebp)
	if (edxp)
		*edxp = edx;
f01042bc:	89 55 e4             	mov    %edx,-0x1c(%ebp)

	cpuid(0x80000002, &brand[0], &brand[1], &brand[2], &brand[3]);
	cpuid(0x80000003, &brand[4], &brand[5], &brand[6], &brand[7]);
	cpuid(0x80000004, &brand[8], &brand[9], &brand[10], &brand[11]);
	cprintf("CPU: %.48s\n", brand);
f01042bf:	83 ec 08             	sub    $0x8,%esp
f01042c2:	8d 45 b8             	lea    -0x48(%ebp),%eax
f01042c5:	50                   	push   %eax
f01042c6:	68 0b 67 10 f0       	push   $0xf010670b
f01042cb:	e8 f9 ef ff ff       	call   f01032c9 <cprintf>

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f01042d0:	b8 01 00 00 00       	mov    $0x1,%eax
f01042d5:	0f a2                	cpuid  
f01042d7:	89 55 90             	mov    %edx,-0x70(%ebp)
	if (eaxp)
		*eaxp = eax;
	if (ebxp)
		*ebxp = ebx;
	if (ecxp)
		*ecxp = ecx;
f01042da:	89 4d a8             	mov    %ecx,-0x58(%ebp)
	if (edxp)
		*edxp = edx;
f01042dd:	89 55 a4             	mov    %edx,-0x5c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f01042e0:	b8 01 00 00 80       	mov    $0x80000001,%eax
f01042e5:	0f a2                	cpuid  
	if (eaxp)
		*eaxp = eax;
	if (ebxp)
		*ebxp = ebx;
	if (ecxp)
		*ecxp = ecx;
f01042e7:	89 4d b4             	mov    %ecx,-0x4c(%ebp)
	if (edxp)
		*edxp = edx;
f01042ea:	89 55 b0             	mov    %edx,-0x50(%ebp)
f01042ed:	83 c4 10             	add    $0x10,%esp
static void
print_feature(uint32_t *feature)
{
	int i, j;

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
f01042f0:	c7 45 94 00 00 00 00 	movl   $0x0,-0x6c(%ebp)
		if (!feature[i])
f01042f7:	8b 45 94             	mov    -0x6c(%ebp),%eax
f01042fa:	8b 74 85 a4          	mov    -0x5c(%ebp,%eax,4),%esi
f01042fe:	85 f6                	test   %esi,%esi
f0104300:	74 5a                	je     f010435c <cpuid_print+0x113>
			continue;
		cprintf(" ");
f0104302:	83 ec 0c             	sub    $0xc,%esp
f0104305:	68 4c 53 10 f0       	push   $0xf010534c
f010430a:	e8 ba ef ff ff       	call   f01032c9 <cprintf>
f010430f:	8b 7d 94             	mov    -0x6c(%ebp),%edi
f0104312:	c1 e7 05             	shl    $0x5,%edi
f0104315:	83 c4 10             	add    $0x10,%esp
		for (j = 0; j < 32; ++j) {
f0104318:	bb 00 00 00 00       	mov    $0x0,%ebx
			const char *name = names[CPUID_BIT(i, j)];

			if ((feature[i] & BIT(j)) && name)
f010431d:	89 f0                	mov    %esi,%eax
f010431f:	88 d9                	mov    %bl,%cl
f0104321:	d3 e8                	shr    %cl,%eax
f0104323:	a8 01                	test   $0x1,%al
f0104325:	74 1f                	je     f0104346 <cpuid_print+0xfd>
	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
		if (!feature[i])
			continue;
		cprintf(" ");
		for (j = 0; j < 32; ++j) {
			const char *name = names[CPUID_BIT(i, j)];
f0104327:	8d 04 3b             	lea    (%ebx,%edi,1),%eax
f010432a:	8b 04 85 60 69 10 f0 	mov    -0xfef96a0(,%eax,4),%eax

			if ((feature[i] & BIT(j)) && name)
f0104331:	85 c0                	test   %eax,%eax
f0104333:	74 11                	je     f0104346 <cpuid_print+0xfd>
				cprintf(" %s", name);
f0104335:	83 ec 08             	sub    $0x8,%esp
f0104338:	50                   	push   %eax
f0104339:	68 a9 4f 10 f0       	push   $0xf0104fa9
f010433e:	e8 86 ef ff ff       	call   f01032c9 <cprintf>
f0104343:	83 c4 10             	add    $0x10,%esp

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
		if (!feature[i])
			continue;
		cprintf(" ");
		for (j = 0; j < 32; ++j) {
f0104346:	43                   	inc    %ebx
f0104347:	83 fb 20             	cmp    $0x20,%ebx
f010434a:	75 d1                	jne    f010431d <cpuid_print+0xd4>
			const char *name = names[CPUID_BIT(i, j)];

			if ((feature[i] & BIT(j)) && name)
				cprintf(" %s", name);
		}
		cprintf("\n");
f010434c:	83 ec 0c             	sub    $0xc,%esp
f010434f:	68 85 61 10 f0       	push   $0xf0106185
f0104354:	e8 70 ef ff ff       	call   f01032c9 <cprintf>
f0104359:	83 c4 10             	add    $0x10,%esp
static void
print_feature(uint32_t *feature)
{
	int i, j;

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
f010435c:	ff 45 94             	incl   -0x6c(%ebp)
f010435f:	8b 45 94             	mov    -0x6c(%ebp),%eax
f0104362:	83 f8 05             	cmp    $0x5,%eax
f0104365:	75 90                	jne    f01042f7 <cpuid_print+0xae>
	      &feature[CPUID_1_ECX], &feature[CPUID_1_EDX]);
	cpuid(0x80000001, NULL, NULL,
	      &feature[CPUID_80000001_ECX], &feature[CPUID_80000001_EDX]);
	print_feature(feature);
	// Check feature bits.
	assert(cpuid_has(feature, CPUID_FEATURE_PSE));
f0104367:	f6 45 90 08          	testb  $0x8,-0x70(%ebp)
f010436b:	75 19                	jne    f0104386 <cpuid_print+0x13d>
f010436d:	68 10 69 10 f0       	push   $0xf0106910
f0104372:	68 98 4f 10 f0       	push   $0xf0104f98
f0104377:	68 9a 00 00 00       	push   $0x9a
f010437c:	68 ff 66 10 f0       	push   $0xf01066ff
f0104381:	e8 c6 bc ff ff       	call   f010004c <_panic>
	assert(cpuid_has(feature, CPUID_FEATURE_APIC));
f0104386:	f7 45 90 00 02 00 00 	testl  $0x200,-0x70(%ebp)
f010438d:	75 19                	jne    f01043a8 <cpuid_print+0x15f>
f010438f:	68 38 69 10 f0       	push   $0xf0106938
f0104394:	68 98 4f 10 f0       	push   $0xf0104f98
f0104399:	68 9b 00 00 00       	push   $0x9b
f010439e:	68 ff 66 10 f0       	push   $0xf01066ff
f01043a3:	e8 a4 bc ff ff       	call   f010004c <_panic>
}
f01043a8:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01043ab:	5b                   	pop    %ebx
f01043ac:	5e                   	pop    %esi
f01043ad:	5f                   	pop    %edi
f01043ae:	5d                   	pop    %ebp
f01043af:	c3                   	ret    

f01043b0 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
f01043b0:	55                   	push   %ebp
f01043b1:	89 e5                	mov    %esp,%ebp
f01043b3:	57                   	push   %edi
f01043b4:	56                   	push   %esi
f01043b5:	53                   	push   %ebx
f01043b6:	83 ec 1c             	sub    $0x1c,%esp
f01043b9:	89 c7                	mov    %eax,%edi
f01043bb:	89 d6                	mov    %edx,%esi
f01043bd:	8b 45 08             	mov    0x8(%ebp),%eax
f01043c0:	8b 55 0c             	mov    0xc(%ebp),%edx
f01043c3:	89 45 d8             	mov    %eax,-0x28(%ebp)
f01043c6:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
f01043c9:	8b 4d 10             	mov    0x10(%ebp),%ecx
f01043cc:	bb 00 00 00 00       	mov    $0x0,%ebx
f01043d1:	89 4d e0             	mov    %ecx,-0x20(%ebp)
f01043d4:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
f01043d7:	39 d3                	cmp    %edx,%ebx
f01043d9:	72 05                	jb     f01043e0 <printnum+0x30>
f01043db:	39 45 10             	cmp    %eax,0x10(%ebp)
f01043de:	77 45                	ja     f0104425 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
f01043e0:	83 ec 0c             	sub    $0xc,%esp
f01043e3:	ff 75 18             	pushl  0x18(%ebp)
f01043e6:	8b 45 14             	mov    0x14(%ebp),%eax
f01043e9:	8d 58 ff             	lea    -0x1(%eax),%ebx
f01043ec:	53                   	push   %ebx
f01043ed:	ff 75 10             	pushl  0x10(%ebp)
f01043f0:	83 ec 08             	sub    $0x8,%esp
f01043f3:	ff 75 e4             	pushl  -0x1c(%ebp)
f01043f6:	ff 75 e0             	pushl  -0x20(%ebp)
f01043f9:	ff 75 dc             	pushl  -0x24(%ebp)
f01043fc:	ff 75 d8             	pushl  -0x28(%ebp)
f01043ff:	e8 14 09 00 00       	call   f0104d18 <__udivdi3>
f0104404:	83 c4 18             	add    $0x18,%esp
f0104407:	52                   	push   %edx
f0104408:	50                   	push   %eax
f0104409:	89 f2                	mov    %esi,%edx
f010440b:	89 f8                	mov    %edi,%eax
f010440d:	e8 9e ff ff ff       	call   f01043b0 <printnum>
f0104412:	83 c4 20             	add    $0x20,%esp
f0104415:	eb 16                	jmp    f010442d <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
f0104417:	83 ec 08             	sub    $0x8,%esp
f010441a:	56                   	push   %esi
f010441b:	ff 75 18             	pushl  0x18(%ebp)
f010441e:	ff d7                	call   *%edi
f0104420:	83 c4 10             	add    $0x10,%esp
f0104423:	eb 03                	jmp    f0104428 <printnum+0x78>
f0104425:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
f0104428:	4b                   	dec    %ebx
f0104429:	85 db                	test   %ebx,%ebx
f010442b:	7f ea                	jg     f0104417 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
f010442d:	83 ec 08             	sub    $0x8,%esp
f0104430:	56                   	push   %esi
f0104431:	83 ec 04             	sub    $0x4,%esp
f0104434:	ff 75 e4             	pushl  -0x1c(%ebp)
f0104437:	ff 75 e0             	pushl  -0x20(%ebp)
f010443a:	ff 75 dc             	pushl  -0x24(%ebp)
f010443d:	ff 75 d8             	pushl  -0x28(%ebp)
f0104440:	e8 e3 09 00 00       	call   f0104e28 <__umoddi3>
f0104445:	83 c4 14             	add    $0x14,%esp
f0104448:	0f be 80 e0 6b 10 f0 	movsbl -0xfef9420(%eax),%eax
f010444f:	50                   	push   %eax
f0104450:	ff d7                	call   *%edi
}
f0104452:	83 c4 10             	add    $0x10,%esp
f0104455:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0104458:	5b                   	pop    %ebx
f0104459:	5e                   	pop    %esi
f010445a:	5f                   	pop    %edi
f010445b:	5d                   	pop    %ebp
f010445c:	c3                   	ret    

f010445d <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
f010445d:	55                   	push   %ebp
f010445e:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
f0104460:	83 fa 01             	cmp    $0x1,%edx
f0104463:	7e 0e                	jle    f0104473 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
f0104465:	8b 10                	mov    (%eax),%edx
f0104467:	8d 4a 08             	lea    0x8(%edx),%ecx
f010446a:	89 08                	mov    %ecx,(%eax)
f010446c:	8b 02                	mov    (%edx),%eax
f010446e:	8b 52 04             	mov    0x4(%edx),%edx
f0104471:	eb 22                	jmp    f0104495 <getuint+0x38>
	else if (lflag)
f0104473:	85 d2                	test   %edx,%edx
f0104475:	74 10                	je     f0104487 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
f0104477:	8b 10                	mov    (%eax),%edx
f0104479:	8d 4a 04             	lea    0x4(%edx),%ecx
f010447c:	89 08                	mov    %ecx,(%eax)
f010447e:	8b 02                	mov    (%edx),%eax
f0104480:	ba 00 00 00 00       	mov    $0x0,%edx
f0104485:	eb 0e                	jmp    f0104495 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
f0104487:	8b 10                	mov    (%eax),%edx
f0104489:	8d 4a 04             	lea    0x4(%edx),%ecx
f010448c:	89 08                	mov    %ecx,(%eax)
f010448e:	8b 02                	mov    (%edx),%eax
f0104490:	ba 00 00 00 00       	mov    $0x0,%edx
}
f0104495:	5d                   	pop    %ebp
f0104496:	c3                   	ret    

f0104497 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
f0104497:	55                   	push   %ebp
f0104498:	89 e5                	mov    %esp,%ebp
f010449a:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
f010449d:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
f01044a0:	8b 10                	mov    (%eax),%edx
f01044a2:	3b 50 04             	cmp    0x4(%eax),%edx
f01044a5:	73 0a                	jae    f01044b1 <sprintputch+0x1a>
		*b->buf++ = ch;
f01044a7:	8d 4a 01             	lea    0x1(%edx),%ecx
f01044aa:	89 08                	mov    %ecx,(%eax)
f01044ac:	8b 45 08             	mov    0x8(%ebp),%eax
f01044af:	88 02                	mov    %al,(%edx)
}
f01044b1:	5d                   	pop    %ebp
f01044b2:	c3                   	ret    

f01044b3 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
f01044b3:	55                   	push   %ebp
f01044b4:	89 e5                	mov    %esp,%ebp
f01044b6:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
f01044b9:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
f01044bc:	50                   	push   %eax
f01044bd:	ff 75 10             	pushl  0x10(%ebp)
f01044c0:	ff 75 0c             	pushl  0xc(%ebp)
f01044c3:	ff 75 08             	pushl  0x8(%ebp)
f01044c6:	e8 05 00 00 00       	call   f01044d0 <vprintfmt>
	va_end(ap);
}
f01044cb:	83 c4 10             	add    $0x10,%esp
f01044ce:	c9                   	leave  
f01044cf:	c3                   	ret    

f01044d0 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
f01044d0:	55                   	push   %ebp
f01044d1:	89 e5                	mov    %esp,%ebp
f01044d3:	57                   	push   %edi
f01044d4:	56                   	push   %esi
f01044d5:	53                   	push   %ebx
f01044d6:	83 ec 2c             	sub    $0x2c,%esp
f01044d9:	8b 75 08             	mov    0x8(%ebp),%esi
f01044dc:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f01044df:	8b 7d 10             	mov    0x10(%ebp),%edi
f01044e2:	eb 12                	jmp    f01044f6 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
f01044e4:	85 c0                	test   %eax,%eax
f01044e6:	0f 84 68 03 00 00    	je     f0104854 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
f01044ec:	83 ec 08             	sub    $0x8,%esp
f01044ef:	53                   	push   %ebx
f01044f0:	50                   	push   %eax
f01044f1:	ff d6                	call   *%esi
f01044f3:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
f01044f6:	47                   	inc    %edi
f01044f7:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
f01044fb:	83 f8 25             	cmp    $0x25,%eax
f01044fe:	75 e4                	jne    f01044e4 <vprintfmt+0x14>
f0104500:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
f0104504:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
f010450b:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
f0104512:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
f0104519:	ba 00 00 00 00       	mov    $0x0,%edx
f010451e:	eb 07                	jmp    f0104527 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0104520:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
f0104523:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0104527:	8d 47 01             	lea    0x1(%edi),%eax
f010452a:	89 45 e0             	mov    %eax,-0x20(%ebp)
f010452d:	0f b6 0f             	movzbl (%edi),%ecx
f0104530:	8a 07                	mov    (%edi),%al
f0104532:	83 e8 23             	sub    $0x23,%eax
f0104535:	3c 55                	cmp    $0x55,%al
f0104537:	0f 87 fe 02 00 00    	ja     f010483b <vprintfmt+0x36b>
f010453d:	0f b6 c0             	movzbl %al,%eax
f0104540:	ff 24 85 6c 6c 10 f0 	jmp    *-0xfef9394(,%eax,4)
f0104547:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
f010454a:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
f010454e:	eb d7                	jmp    f0104527 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0104550:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0104553:	b8 00 00 00 00       	mov    $0x0,%eax
f0104558:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
f010455b:	8d 04 80             	lea    (%eax,%eax,4),%eax
f010455e:	01 c0                	add    %eax,%eax
f0104560:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
f0104564:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
f0104567:	8d 51 d0             	lea    -0x30(%ecx),%edx
f010456a:	83 fa 09             	cmp    $0x9,%edx
f010456d:	77 34                	ja     f01045a3 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
f010456f:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
f0104570:	eb e9                	jmp    f010455b <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
f0104572:	8b 45 14             	mov    0x14(%ebp),%eax
f0104575:	8d 48 04             	lea    0x4(%eax),%ecx
f0104578:	89 4d 14             	mov    %ecx,0x14(%ebp)
f010457b:	8b 00                	mov    (%eax),%eax
f010457d:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0104580:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
f0104583:	eb 24                	jmp    f01045a9 <vprintfmt+0xd9>
f0104585:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f0104589:	79 07                	jns    f0104592 <vprintfmt+0xc2>
f010458b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0104592:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0104595:	eb 90                	jmp    f0104527 <vprintfmt+0x57>
f0104597:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
f010459a:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
f01045a1:	eb 84                	jmp    f0104527 <vprintfmt+0x57>
f01045a3:	8b 55 e0             	mov    -0x20(%ebp),%edx
f01045a6:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
f01045a9:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f01045ad:	0f 89 74 ff ff ff    	jns    f0104527 <vprintfmt+0x57>
				width = precision, precision = -1;
f01045b3:	8b 45 d0             	mov    -0x30(%ebp),%eax
f01045b6:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f01045b9:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
f01045c0:	e9 62 ff ff ff       	jmp    f0104527 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
f01045c5:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f01045c6:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
f01045c9:	e9 59 ff ff ff       	jmp    f0104527 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
f01045ce:	8b 45 14             	mov    0x14(%ebp),%eax
f01045d1:	8d 50 04             	lea    0x4(%eax),%edx
f01045d4:	89 55 14             	mov    %edx,0x14(%ebp)
f01045d7:	83 ec 08             	sub    $0x8,%esp
f01045da:	53                   	push   %ebx
f01045db:	ff 30                	pushl  (%eax)
f01045dd:	ff d6                	call   *%esi
			break;
f01045df:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f01045e2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
f01045e5:	e9 0c ff ff ff       	jmp    f01044f6 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
f01045ea:	8b 45 14             	mov    0x14(%ebp),%eax
f01045ed:	8d 50 04             	lea    0x4(%eax),%edx
f01045f0:	89 55 14             	mov    %edx,0x14(%ebp)
f01045f3:	8b 00                	mov    (%eax),%eax
f01045f5:	85 c0                	test   %eax,%eax
f01045f7:	79 02                	jns    f01045fb <vprintfmt+0x12b>
f01045f9:	f7 d8                	neg    %eax
f01045fb:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
f01045fd:	83 f8 06             	cmp    $0x6,%eax
f0104600:	7f 0b                	jg     f010460d <vprintfmt+0x13d>
f0104602:	8b 04 85 c4 6d 10 f0 	mov    -0xfef923c(,%eax,4),%eax
f0104609:	85 c0                	test   %eax,%eax
f010460b:	75 18                	jne    f0104625 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
f010460d:	52                   	push   %edx
f010460e:	68 f8 6b 10 f0       	push   $0xf0106bf8
f0104613:	53                   	push   %ebx
f0104614:	56                   	push   %esi
f0104615:	e8 99 fe ff ff       	call   f01044b3 <printfmt>
f010461a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010461d:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
f0104620:	e9 d1 fe ff ff       	jmp    f01044f6 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
f0104625:	50                   	push   %eax
f0104626:	68 aa 4f 10 f0       	push   $0xf0104faa
f010462b:	53                   	push   %ebx
f010462c:	56                   	push   %esi
f010462d:	e8 81 fe ff ff       	call   f01044b3 <printfmt>
f0104632:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0104635:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0104638:	e9 b9 fe ff ff       	jmp    f01044f6 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
f010463d:	8b 45 14             	mov    0x14(%ebp),%eax
f0104640:	8d 50 04             	lea    0x4(%eax),%edx
f0104643:	89 55 14             	mov    %edx,0x14(%ebp)
f0104646:	8b 38                	mov    (%eax),%edi
f0104648:	85 ff                	test   %edi,%edi
f010464a:	75 05                	jne    f0104651 <vprintfmt+0x181>
				p = "(null)";
f010464c:	bf f1 6b 10 f0       	mov    $0xf0106bf1,%edi
			if (width > 0 && padc != '-')
f0104651:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f0104655:	0f 8e 90 00 00 00    	jle    f01046eb <vprintfmt+0x21b>
f010465b:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
f010465f:	0f 84 8e 00 00 00    	je     f01046f3 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
f0104665:	83 ec 08             	sub    $0x8,%esp
f0104668:	ff 75 d0             	pushl  -0x30(%ebp)
f010466b:	57                   	push   %edi
f010466c:	e8 60 03 00 00       	call   f01049d1 <strnlen>
f0104671:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
f0104674:	29 c1                	sub    %eax,%ecx
f0104676:	89 4d cc             	mov    %ecx,-0x34(%ebp)
f0104679:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
f010467c:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
f0104680:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0104683:	89 7d d4             	mov    %edi,-0x2c(%ebp)
f0104686:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f0104688:	eb 0d                	jmp    f0104697 <vprintfmt+0x1c7>
					putch(padc, putdat);
f010468a:	83 ec 08             	sub    $0x8,%esp
f010468d:	53                   	push   %ebx
f010468e:	ff 75 e4             	pushl  -0x1c(%ebp)
f0104691:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f0104693:	4f                   	dec    %edi
f0104694:	83 c4 10             	add    $0x10,%esp
f0104697:	85 ff                	test   %edi,%edi
f0104699:	7f ef                	jg     f010468a <vprintfmt+0x1ba>
f010469b:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f010469e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
f01046a1:	89 c8                	mov    %ecx,%eax
f01046a3:	85 c9                	test   %ecx,%ecx
f01046a5:	79 05                	jns    f01046ac <vprintfmt+0x1dc>
f01046a7:	b8 00 00 00 00       	mov    $0x0,%eax
f01046ac:	8b 4d cc             	mov    -0x34(%ebp),%ecx
f01046af:	29 c1                	sub    %eax,%ecx
f01046b1:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
f01046b4:	89 75 08             	mov    %esi,0x8(%ebp)
f01046b7:	8b 75 d0             	mov    -0x30(%ebp),%esi
f01046ba:	eb 3d                	jmp    f01046f9 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
f01046bc:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
f01046c0:	74 19                	je     f01046db <vprintfmt+0x20b>
f01046c2:	0f be c0             	movsbl %al,%eax
f01046c5:	83 e8 20             	sub    $0x20,%eax
f01046c8:	83 f8 5e             	cmp    $0x5e,%eax
f01046cb:	76 0e                	jbe    f01046db <vprintfmt+0x20b>
					putch('?', putdat);
f01046cd:	83 ec 08             	sub    $0x8,%esp
f01046d0:	53                   	push   %ebx
f01046d1:	6a 3f                	push   $0x3f
f01046d3:	ff 55 08             	call   *0x8(%ebp)
f01046d6:	83 c4 10             	add    $0x10,%esp
f01046d9:	eb 0b                	jmp    f01046e6 <vprintfmt+0x216>
				else
					putch(ch, putdat);
f01046db:	83 ec 08             	sub    $0x8,%esp
f01046de:	53                   	push   %ebx
f01046df:	52                   	push   %edx
f01046e0:	ff 55 08             	call   *0x8(%ebp)
f01046e3:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
f01046e6:	ff 4d e4             	decl   -0x1c(%ebp)
f01046e9:	eb 0e                	jmp    f01046f9 <vprintfmt+0x229>
f01046eb:	89 75 08             	mov    %esi,0x8(%ebp)
f01046ee:	8b 75 d0             	mov    -0x30(%ebp),%esi
f01046f1:	eb 06                	jmp    f01046f9 <vprintfmt+0x229>
f01046f3:	89 75 08             	mov    %esi,0x8(%ebp)
f01046f6:	8b 75 d0             	mov    -0x30(%ebp),%esi
f01046f9:	47                   	inc    %edi
f01046fa:	8a 47 ff             	mov    -0x1(%edi),%al
f01046fd:	0f be d0             	movsbl %al,%edx
f0104700:	85 d2                	test   %edx,%edx
f0104702:	74 1d                	je     f0104721 <vprintfmt+0x251>
f0104704:	85 f6                	test   %esi,%esi
f0104706:	78 b4                	js     f01046bc <vprintfmt+0x1ec>
f0104708:	4e                   	dec    %esi
f0104709:	79 b1                	jns    f01046bc <vprintfmt+0x1ec>
f010470b:	8b 75 08             	mov    0x8(%ebp),%esi
f010470e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0104711:	eb 14                	jmp    f0104727 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
f0104713:	83 ec 08             	sub    $0x8,%esp
f0104716:	53                   	push   %ebx
f0104717:	6a 20                	push   $0x20
f0104719:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
f010471b:	4f                   	dec    %edi
f010471c:	83 c4 10             	add    $0x10,%esp
f010471f:	eb 06                	jmp    f0104727 <vprintfmt+0x257>
f0104721:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0104724:	8b 75 08             	mov    0x8(%ebp),%esi
f0104727:	85 ff                	test   %edi,%edi
f0104729:	7f e8                	jg     f0104713 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010472b:	8b 7d e0             	mov    -0x20(%ebp),%edi
f010472e:	e9 c3 fd ff ff       	jmp    f01044f6 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
f0104733:	83 fa 01             	cmp    $0x1,%edx
f0104736:	7e 16                	jle    f010474e <vprintfmt+0x27e>
		return va_arg(*ap, long long);
f0104738:	8b 45 14             	mov    0x14(%ebp),%eax
f010473b:	8d 50 08             	lea    0x8(%eax),%edx
f010473e:	89 55 14             	mov    %edx,0x14(%ebp)
f0104741:	8b 50 04             	mov    0x4(%eax),%edx
f0104744:	8b 00                	mov    (%eax),%eax
f0104746:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0104749:	89 55 dc             	mov    %edx,-0x24(%ebp)
f010474c:	eb 32                	jmp    f0104780 <vprintfmt+0x2b0>
	else if (lflag)
f010474e:	85 d2                	test   %edx,%edx
f0104750:	74 18                	je     f010476a <vprintfmt+0x29a>
		return va_arg(*ap, long);
f0104752:	8b 45 14             	mov    0x14(%ebp),%eax
f0104755:	8d 50 04             	lea    0x4(%eax),%edx
f0104758:	89 55 14             	mov    %edx,0x14(%ebp)
f010475b:	8b 00                	mov    (%eax),%eax
f010475d:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0104760:	89 c1                	mov    %eax,%ecx
f0104762:	c1 f9 1f             	sar    $0x1f,%ecx
f0104765:	89 4d dc             	mov    %ecx,-0x24(%ebp)
f0104768:	eb 16                	jmp    f0104780 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
f010476a:	8b 45 14             	mov    0x14(%ebp),%eax
f010476d:	8d 50 04             	lea    0x4(%eax),%edx
f0104770:	89 55 14             	mov    %edx,0x14(%ebp)
f0104773:	8b 00                	mov    (%eax),%eax
f0104775:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0104778:	89 c1                	mov    %eax,%ecx
f010477a:	c1 f9 1f             	sar    $0x1f,%ecx
f010477d:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
f0104780:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0104783:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
f0104786:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
f010478a:	79 76                	jns    f0104802 <vprintfmt+0x332>
				putch('-', putdat);
f010478c:	83 ec 08             	sub    $0x8,%esp
f010478f:	53                   	push   %ebx
f0104790:	6a 2d                	push   $0x2d
f0104792:	ff d6                	call   *%esi
				num = -(long long) num;
f0104794:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0104797:	8b 55 dc             	mov    -0x24(%ebp),%edx
f010479a:	f7 d8                	neg    %eax
f010479c:	83 d2 00             	adc    $0x0,%edx
f010479f:	f7 da                	neg    %edx
f01047a1:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
f01047a4:	b9 0a 00 00 00       	mov    $0xa,%ecx
f01047a9:	eb 5c                	jmp    f0104807 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
f01047ab:	8d 45 14             	lea    0x14(%ebp),%eax
f01047ae:	e8 aa fc ff ff       	call   f010445d <getuint>
			base = 10;
f01047b3:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
f01047b8:	eb 4d                	jmp    f0104807 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
f01047ba:	8d 45 14             	lea    0x14(%ebp),%eax
f01047bd:	e8 9b fc ff ff       	call   f010445d <getuint>
			base = 8;
f01047c2:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
f01047c7:	eb 3e                	jmp    f0104807 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
f01047c9:	83 ec 08             	sub    $0x8,%esp
f01047cc:	53                   	push   %ebx
f01047cd:	6a 30                	push   $0x30
f01047cf:	ff d6                	call   *%esi
			putch('x', putdat);
f01047d1:	83 c4 08             	add    $0x8,%esp
f01047d4:	53                   	push   %ebx
f01047d5:	6a 78                	push   $0x78
f01047d7:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
f01047d9:	8b 45 14             	mov    0x14(%ebp),%eax
f01047dc:	8d 50 04             	lea    0x4(%eax),%edx
f01047df:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
f01047e2:	8b 00                	mov    (%eax),%eax
f01047e4:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
f01047e9:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
f01047ec:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
f01047f1:	eb 14                	jmp    f0104807 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
f01047f3:	8d 45 14             	lea    0x14(%ebp),%eax
f01047f6:	e8 62 fc ff ff       	call   f010445d <getuint>
			base = 16;
f01047fb:	b9 10 00 00 00       	mov    $0x10,%ecx
f0104800:	eb 05                	jmp    f0104807 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
f0104802:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
f0104807:	83 ec 0c             	sub    $0xc,%esp
f010480a:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
f010480e:	57                   	push   %edi
f010480f:	ff 75 e4             	pushl  -0x1c(%ebp)
f0104812:	51                   	push   %ecx
f0104813:	52                   	push   %edx
f0104814:	50                   	push   %eax
f0104815:	89 da                	mov    %ebx,%edx
f0104817:	89 f0                	mov    %esi,%eax
f0104819:	e8 92 fb ff ff       	call   f01043b0 <printnum>
			break;
f010481e:	83 c4 20             	add    $0x20,%esp
f0104821:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0104824:	e9 cd fc ff ff       	jmp    f01044f6 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
f0104829:	83 ec 08             	sub    $0x8,%esp
f010482c:	53                   	push   %ebx
f010482d:	51                   	push   %ecx
f010482e:	ff d6                	call   *%esi
			break;
f0104830:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0104833:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
f0104836:	e9 bb fc ff ff       	jmp    f01044f6 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
f010483b:	83 ec 08             	sub    $0x8,%esp
f010483e:	53                   	push   %ebx
f010483f:	6a 25                	push   $0x25
f0104841:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
f0104843:	83 c4 10             	add    $0x10,%esp
f0104846:	eb 01                	jmp    f0104849 <vprintfmt+0x379>
f0104848:	4f                   	dec    %edi
f0104849:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
f010484d:	75 f9                	jne    f0104848 <vprintfmt+0x378>
f010484f:	e9 a2 fc ff ff       	jmp    f01044f6 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
f0104854:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0104857:	5b                   	pop    %ebx
f0104858:	5e                   	pop    %esi
f0104859:	5f                   	pop    %edi
f010485a:	5d                   	pop    %ebp
f010485b:	c3                   	ret    

f010485c <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
f010485c:	55                   	push   %ebp
f010485d:	89 e5                	mov    %esp,%ebp
f010485f:	83 ec 18             	sub    $0x18,%esp
f0104862:	8b 45 08             	mov    0x8(%ebp),%eax
f0104865:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
f0104868:	89 45 ec             	mov    %eax,-0x14(%ebp)
f010486b:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
f010486f:	89 4d f0             	mov    %ecx,-0x10(%ebp)
f0104872:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
f0104879:	85 c0                	test   %eax,%eax
f010487b:	74 26                	je     f01048a3 <vsnprintf+0x47>
f010487d:	85 d2                	test   %edx,%edx
f010487f:	7e 29                	jle    f01048aa <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
f0104881:	ff 75 14             	pushl  0x14(%ebp)
f0104884:	ff 75 10             	pushl  0x10(%ebp)
f0104887:	8d 45 ec             	lea    -0x14(%ebp),%eax
f010488a:	50                   	push   %eax
f010488b:	68 97 44 10 f0       	push   $0xf0104497
f0104890:	e8 3b fc ff ff       	call   f01044d0 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
f0104895:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0104898:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
f010489b:	8b 45 f4             	mov    -0xc(%ebp),%eax
f010489e:	83 c4 10             	add    $0x10,%esp
f01048a1:	eb 0c                	jmp    f01048af <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
f01048a3:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
f01048a8:	eb 05                	jmp    f01048af <vsnprintf+0x53>
f01048aa:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
f01048af:	c9                   	leave  
f01048b0:	c3                   	ret    

f01048b1 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
f01048b1:	55                   	push   %ebp
f01048b2:	89 e5                	mov    %esp,%ebp
f01048b4:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
f01048b7:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
f01048ba:	50                   	push   %eax
f01048bb:	ff 75 10             	pushl  0x10(%ebp)
f01048be:	ff 75 0c             	pushl  0xc(%ebp)
f01048c1:	ff 75 08             	pushl  0x8(%ebp)
f01048c4:	e8 93 ff ff ff       	call   f010485c <vsnprintf>
	va_end(ap);

	return rc;
}
f01048c9:	c9                   	leave  
f01048ca:	c3                   	ret    

f01048cb <readline>:
#define BUFLEN 1024
static char buf[BUFLEN];

char *
readline(const char *prompt)
{
f01048cb:	55                   	push   %ebp
f01048cc:	89 e5                	mov    %esp,%ebp
f01048ce:	57                   	push   %edi
f01048cf:	56                   	push   %esi
f01048d0:	53                   	push   %ebx
f01048d1:	83 ec 0c             	sub    $0xc,%esp
f01048d4:	8b 45 08             	mov    0x8(%ebp),%eax
	int i, c, echoing;

	if (prompt != NULL)
f01048d7:	85 c0                	test   %eax,%eax
f01048d9:	74 11                	je     f01048ec <readline+0x21>
		cprintf("%s", prompt);
f01048db:	83 ec 08             	sub    $0x8,%esp
f01048de:	50                   	push   %eax
f01048df:	68 aa 4f 10 f0       	push   $0xf0104faa
f01048e4:	e8 e0 e9 ff ff       	call   f01032c9 <cprintf>
f01048e9:	83 c4 10             	add    $0x10,%esp

	i = 0;
	echoing = iscons(0);
f01048ec:	83 ec 0c             	sub    $0xc,%esp
f01048ef:	6a 00                	push   $0x0
f01048f1:	e8 4d bd ff ff       	call   f0100643 <iscons>
f01048f6:	89 c7                	mov    %eax,%edi
f01048f8:	83 c4 10             	add    $0x10,%esp
	int i, c, echoing;

	if (prompt != NULL)
		cprintf("%s", prompt);

	i = 0;
f01048fb:	be 00 00 00 00       	mov    $0x0,%esi
	echoing = iscons(0);
	while (1) {
		c = getchar();
f0104900:	e8 2d bd ff ff       	call   f0100632 <getchar>
f0104905:	89 c3                	mov    %eax,%ebx
		if (c < 0) {
f0104907:	85 c0                	test   %eax,%eax
f0104909:	79 1b                	jns    f0104926 <readline+0x5b>
			cprintf("read error: %e\n", c);
f010490b:	83 ec 08             	sub    $0x8,%esp
f010490e:	50                   	push   %eax
f010490f:	68 e0 6d 10 f0       	push   $0xf0106de0
f0104914:	e8 b0 e9 ff ff       	call   f01032c9 <cprintf>
			return NULL;
f0104919:	83 c4 10             	add    $0x10,%esp
f010491c:	b8 00 00 00 00       	mov    $0x0,%eax
f0104921:	e9 8d 00 00 00       	jmp    f01049b3 <readline+0xe8>
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
f0104926:	83 f8 08             	cmp    $0x8,%eax
f0104929:	74 72                	je     f010499d <readline+0xd2>
f010492b:	83 f8 7f             	cmp    $0x7f,%eax
f010492e:	75 16                	jne    f0104946 <readline+0x7b>
f0104930:	eb 65                	jmp    f0104997 <readline+0xcc>
			if (echoing)
f0104932:	85 ff                	test   %edi,%edi
f0104934:	74 0d                	je     f0104943 <readline+0x78>
				cputchar('\b');
f0104936:	83 ec 0c             	sub    $0xc,%esp
f0104939:	6a 08                	push   $0x8
f010493b:	e8 e2 bc ff ff       	call   f0100622 <cputchar>
f0104940:	83 c4 10             	add    $0x10,%esp
			i--;
f0104943:	4e                   	dec    %esi
f0104944:	eb ba                	jmp    f0104900 <readline+0x35>
		} else if (c >= ' ' && i < BUFLEN-1) {
f0104946:	83 f8 1f             	cmp    $0x1f,%eax
f0104949:	7e 23                	jle    f010496e <readline+0xa3>
f010494b:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
f0104951:	7f 1b                	jg     f010496e <readline+0xa3>
			if (echoing)
f0104953:	85 ff                	test   %edi,%edi
f0104955:	74 0c                	je     f0104963 <readline+0x98>
				cputchar(c);
f0104957:	83 ec 0c             	sub    $0xc,%esp
f010495a:	53                   	push   %ebx
f010495b:	e8 c2 bc ff ff       	call   f0100622 <cputchar>
f0104960:	83 c4 10             	add    $0x10,%esp
			buf[i++] = c;
f0104963:	88 9e 20 db 18 f0    	mov    %bl,-0xfe724e0(%esi)
f0104969:	8d 76 01             	lea    0x1(%esi),%esi
f010496c:	eb 92                	jmp    f0104900 <readline+0x35>
		} else if (c == '\n' || c == '\r') {
f010496e:	83 fb 0a             	cmp    $0xa,%ebx
f0104971:	74 05                	je     f0104978 <readline+0xad>
f0104973:	83 fb 0d             	cmp    $0xd,%ebx
f0104976:	75 88                	jne    f0104900 <readline+0x35>
			if (echoing)
f0104978:	85 ff                	test   %edi,%edi
f010497a:	74 0d                	je     f0104989 <readline+0xbe>
				cputchar('\n');
f010497c:	83 ec 0c             	sub    $0xc,%esp
f010497f:	6a 0a                	push   $0xa
f0104981:	e8 9c bc ff ff       	call   f0100622 <cputchar>
f0104986:	83 c4 10             	add    $0x10,%esp
			buf[i] = 0;
f0104989:	c6 86 20 db 18 f0 00 	movb   $0x0,-0xfe724e0(%esi)
			return buf;
f0104990:	b8 20 db 18 f0       	mov    $0xf018db20,%eax
f0104995:	eb 1c                	jmp    f01049b3 <readline+0xe8>
	while (1) {
		c = getchar();
		if (c < 0) {
			cprintf("read error: %e\n", c);
			return NULL;
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
f0104997:	85 f6                	test   %esi,%esi
f0104999:	7f 97                	jg     f0104932 <readline+0x67>
f010499b:	eb 09                	jmp    f01049a6 <readline+0xdb>
f010499d:	85 f6                	test   %esi,%esi
f010499f:	7f 91                	jg     f0104932 <readline+0x67>
f01049a1:	e9 5a ff ff ff       	jmp    f0104900 <readline+0x35>
			if (echoing)
				cputchar('\b');
			i--;
		} else if (c >= ' ' && i < BUFLEN-1) {
f01049a6:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
f01049ac:	7e a5                	jle    f0104953 <readline+0x88>
f01049ae:	e9 4d ff ff ff       	jmp    f0104900 <readline+0x35>
				cputchar('\n');
			buf[i] = 0;
			return buf;
		}
	}
}
f01049b3:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01049b6:	5b                   	pop    %ebx
f01049b7:	5e                   	pop    %esi
f01049b8:	5f                   	pop    %edi
f01049b9:	5d                   	pop    %ebp
f01049ba:	c3                   	ret    

f01049bb <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
f01049bb:	55                   	push   %ebp
f01049bc:	89 e5                	mov    %esp,%ebp
f01049be:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
f01049c1:	b8 00 00 00 00       	mov    $0x0,%eax
f01049c6:	eb 01                	jmp    f01049c9 <strlen+0xe>
		n++;
f01049c8:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
f01049c9:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
f01049cd:	75 f9                	jne    f01049c8 <strlen+0xd>
		n++;
	return n;
}
f01049cf:	5d                   	pop    %ebp
f01049d0:	c3                   	ret    

f01049d1 <strnlen>:

int
strnlen(const char *s, size_t size)
{
f01049d1:	55                   	push   %ebp
f01049d2:	89 e5                	mov    %esp,%ebp
f01049d4:	8b 4d 08             	mov    0x8(%ebp),%ecx
f01049d7:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f01049da:	ba 00 00 00 00       	mov    $0x0,%edx
f01049df:	eb 01                	jmp    f01049e2 <strnlen+0x11>
		n++;
f01049e1:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f01049e2:	39 c2                	cmp    %eax,%edx
f01049e4:	74 08                	je     f01049ee <strnlen+0x1d>
f01049e6:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
f01049ea:	75 f5                	jne    f01049e1 <strnlen+0x10>
f01049ec:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
f01049ee:	5d                   	pop    %ebp
f01049ef:	c3                   	ret    

f01049f0 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
f01049f0:	55                   	push   %ebp
f01049f1:	89 e5                	mov    %esp,%ebp
f01049f3:	53                   	push   %ebx
f01049f4:	8b 45 08             	mov    0x8(%ebp),%eax
f01049f7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
f01049fa:	89 c2                	mov    %eax,%edx
f01049fc:	42                   	inc    %edx
f01049fd:	41                   	inc    %ecx
f01049fe:	8a 59 ff             	mov    -0x1(%ecx),%bl
f0104a01:	88 5a ff             	mov    %bl,-0x1(%edx)
f0104a04:	84 db                	test   %bl,%bl
f0104a06:	75 f4                	jne    f01049fc <strcpy+0xc>
		/* do nothing */;
	return ret;
}
f0104a08:	5b                   	pop    %ebx
f0104a09:	5d                   	pop    %ebp
f0104a0a:	c3                   	ret    

f0104a0b <strcat>:

char *
strcat(char *dst, const char *src)
{
f0104a0b:	55                   	push   %ebp
f0104a0c:	89 e5                	mov    %esp,%ebp
f0104a0e:	53                   	push   %ebx
f0104a0f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
f0104a12:	53                   	push   %ebx
f0104a13:	e8 a3 ff ff ff       	call   f01049bb <strlen>
f0104a18:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
f0104a1b:	ff 75 0c             	pushl  0xc(%ebp)
f0104a1e:	01 d8                	add    %ebx,%eax
f0104a20:	50                   	push   %eax
f0104a21:	e8 ca ff ff ff       	call   f01049f0 <strcpy>
	return dst;
}
f0104a26:	89 d8                	mov    %ebx,%eax
f0104a28:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0104a2b:	c9                   	leave  
f0104a2c:	c3                   	ret    

f0104a2d <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
f0104a2d:	55                   	push   %ebp
f0104a2e:	89 e5                	mov    %esp,%ebp
f0104a30:	56                   	push   %esi
f0104a31:	53                   	push   %ebx
f0104a32:	8b 75 08             	mov    0x8(%ebp),%esi
f0104a35:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0104a38:	89 f3                	mov    %esi,%ebx
f0104a3a:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f0104a3d:	89 f2                	mov    %esi,%edx
f0104a3f:	eb 0c                	jmp    f0104a4d <strncpy+0x20>
		*dst++ = *src;
f0104a41:	42                   	inc    %edx
f0104a42:	8a 01                	mov    (%ecx),%al
f0104a44:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
f0104a47:	80 39 01             	cmpb   $0x1,(%ecx)
f0104a4a:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f0104a4d:	39 da                	cmp    %ebx,%edx
f0104a4f:	75 f0                	jne    f0104a41 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
f0104a51:	89 f0                	mov    %esi,%eax
f0104a53:	5b                   	pop    %ebx
f0104a54:	5e                   	pop    %esi
f0104a55:	5d                   	pop    %ebp
f0104a56:	c3                   	ret    

f0104a57 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
f0104a57:	55                   	push   %ebp
f0104a58:	89 e5                	mov    %esp,%ebp
f0104a5a:	56                   	push   %esi
f0104a5b:	53                   	push   %ebx
f0104a5c:	8b 75 08             	mov    0x8(%ebp),%esi
f0104a5f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0104a62:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
f0104a65:	85 c0                	test   %eax,%eax
f0104a67:	74 1e                	je     f0104a87 <strlcpy+0x30>
f0104a69:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
f0104a6d:	89 f2                	mov    %esi,%edx
f0104a6f:	eb 05                	jmp    f0104a76 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
f0104a71:	42                   	inc    %edx
f0104a72:	41                   	inc    %ecx
f0104a73:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
f0104a76:	39 c2                	cmp    %eax,%edx
f0104a78:	74 08                	je     f0104a82 <strlcpy+0x2b>
f0104a7a:	8a 19                	mov    (%ecx),%bl
f0104a7c:	84 db                	test   %bl,%bl
f0104a7e:	75 f1                	jne    f0104a71 <strlcpy+0x1a>
f0104a80:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
f0104a82:	c6 00 00             	movb   $0x0,(%eax)
f0104a85:	eb 02                	jmp    f0104a89 <strlcpy+0x32>
f0104a87:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
f0104a89:	29 f0                	sub    %esi,%eax
}
f0104a8b:	5b                   	pop    %ebx
f0104a8c:	5e                   	pop    %esi
f0104a8d:	5d                   	pop    %ebp
f0104a8e:	c3                   	ret    

f0104a8f <strcmp>:

int
strcmp(const char *p, const char *q)
{
f0104a8f:	55                   	push   %ebp
f0104a90:	89 e5                	mov    %esp,%ebp
f0104a92:	8b 4d 08             	mov    0x8(%ebp),%ecx
f0104a95:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
f0104a98:	eb 02                	jmp    f0104a9c <strcmp+0xd>
		p++, q++;
f0104a9a:	41                   	inc    %ecx
f0104a9b:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
f0104a9c:	8a 01                	mov    (%ecx),%al
f0104a9e:	84 c0                	test   %al,%al
f0104aa0:	74 04                	je     f0104aa6 <strcmp+0x17>
f0104aa2:	3a 02                	cmp    (%edx),%al
f0104aa4:	74 f4                	je     f0104a9a <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
f0104aa6:	0f b6 c0             	movzbl %al,%eax
f0104aa9:	0f b6 12             	movzbl (%edx),%edx
f0104aac:	29 d0                	sub    %edx,%eax
}
f0104aae:	5d                   	pop    %ebp
f0104aaf:	c3                   	ret    

f0104ab0 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
f0104ab0:	55                   	push   %ebp
f0104ab1:	89 e5                	mov    %esp,%ebp
f0104ab3:	53                   	push   %ebx
f0104ab4:	8b 45 08             	mov    0x8(%ebp),%eax
f0104ab7:	8b 55 0c             	mov    0xc(%ebp),%edx
f0104aba:	89 c3                	mov    %eax,%ebx
f0104abc:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
f0104abf:	eb 02                	jmp    f0104ac3 <strncmp+0x13>
		n--, p++, q++;
f0104ac1:	40                   	inc    %eax
f0104ac2:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
f0104ac3:	39 d8                	cmp    %ebx,%eax
f0104ac5:	74 14                	je     f0104adb <strncmp+0x2b>
f0104ac7:	8a 08                	mov    (%eax),%cl
f0104ac9:	84 c9                	test   %cl,%cl
f0104acb:	74 04                	je     f0104ad1 <strncmp+0x21>
f0104acd:	3a 0a                	cmp    (%edx),%cl
f0104acf:	74 f0                	je     f0104ac1 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
f0104ad1:	0f b6 00             	movzbl (%eax),%eax
f0104ad4:	0f b6 12             	movzbl (%edx),%edx
f0104ad7:	29 d0                	sub    %edx,%eax
f0104ad9:	eb 05                	jmp    f0104ae0 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
f0104adb:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
f0104ae0:	5b                   	pop    %ebx
f0104ae1:	5d                   	pop    %ebp
f0104ae2:	c3                   	ret    

f0104ae3 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
f0104ae3:	55                   	push   %ebp
f0104ae4:	89 e5                	mov    %esp,%ebp
f0104ae6:	8b 45 08             	mov    0x8(%ebp),%eax
f0104ae9:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
f0104aec:	eb 05                	jmp    f0104af3 <strchr+0x10>
		if (*s == c)
f0104aee:	38 ca                	cmp    %cl,%dl
f0104af0:	74 0c                	je     f0104afe <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
f0104af2:	40                   	inc    %eax
f0104af3:	8a 10                	mov    (%eax),%dl
f0104af5:	84 d2                	test   %dl,%dl
f0104af7:	75 f5                	jne    f0104aee <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
f0104af9:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0104afe:	5d                   	pop    %ebp
f0104aff:	c3                   	ret    

f0104b00 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
f0104b00:	55                   	push   %ebp
f0104b01:	89 e5                	mov    %esp,%ebp
f0104b03:	8b 45 08             	mov    0x8(%ebp),%eax
f0104b06:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
f0104b09:	eb 05                	jmp    f0104b10 <strfind+0x10>
		if (*s == c)
f0104b0b:	38 ca                	cmp    %cl,%dl
f0104b0d:	74 07                	je     f0104b16 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
f0104b0f:	40                   	inc    %eax
f0104b10:	8a 10                	mov    (%eax),%dl
f0104b12:	84 d2                	test   %dl,%dl
f0104b14:	75 f5                	jne    f0104b0b <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
f0104b16:	5d                   	pop    %ebp
f0104b17:	c3                   	ret    

f0104b18 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
f0104b18:	55                   	push   %ebp
f0104b19:	89 e5                	mov    %esp,%ebp
f0104b1b:	57                   	push   %edi
f0104b1c:	56                   	push   %esi
f0104b1d:	53                   	push   %ebx
f0104b1e:	8b 7d 08             	mov    0x8(%ebp),%edi
f0104b21:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
f0104b24:	85 c9                	test   %ecx,%ecx
f0104b26:	74 36                	je     f0104b5e <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
f0104b28:	f7 c7 03 00 00 00    	test   $0x3,%edi
f0104b2e:	75 28                	jne    f0104b58 <memset+0x40>
f0104b30:	f6 c1 03             	test   $0x3,%cl
f0104b33:	75 23                	jne    f0104b58 <memset+0x40>
		c &= 0xFF;
f0104b35:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
f0104b39:	89 d3                	mov    %edx,%ebx
f0104b3b:	c1 e3 08             	shl    $0x8,%ebx
f0104b3e:	89 d6                	mov    %edx,%esi
f0104b40:	c1 e6 18             	shl    $0x18,%esi
f0104b43:	89 d0                	mov    %edx,%eax
f0104b45:	c1 e0 10             	shl    $0x10,%eax
f0104b48:	09 f0                	or     %esi,%eax
f0104b4a:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
f0104b4c:	89 d8                	mov    %ebx,%eax
f0104b4e:	09 d0                	or     %edx,%eax
f0104b50:	c1 e9 02             	shr    $0x2,%ecx
f0104b53:	fc                   	cld    
f0104b54:	f3 ab                	rep stos %eax,%es:(%edi)
f0104b56:	eb 06                	jmp    f0104b5e <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
f0104b58:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104b5b:	fc                   	cld    
f0104b5c:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
f0104b5e:	89 f8                	mov    %edi,%eax
f0104b60:	5b                   	pop    %ebx
f0104b61:	5e                   	pop    %esi
f0104b62:	5f                   	pop    %edi
f0104b63:	5d                   	pop    %ebp
f0104b64:	c3                   	ret    

f0104b65 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
f0104b65:	55                   	push   %ebp
f0104b66:	89 e5                	mov    %esp,%ebp
f0104b68:	57                   	push   %edi
f0104b69:	56                   	push   %esi
f0104b6a:	8b 45 08             	mov    0x8(%ebp),%eax
f0104b6d:	8b 75 0c             	mov    0xc(%ebp),%esi
f0104b70:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
f0104b73:	39 c6                	cmp    %eax,%esi
f0104b75:	73 33                	jae    f0104baa <memmove+0x45>
f0104b77:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
f0104b7a:	39 d0                	cmp    %edx,%eax
f0104b7c:	73 2c                	jae    f0104baa <memmove+0x45>
		s += n;
		d += n;
f0104b7e:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
f0104b81:	89 d6                	mov    %edx,%esi
f0104b83:	09 fe                	or     %edi,%esi
f0104b85:	f7 c6 03 00 00 00    	test   $0x3,%esi
f0104b8b:	75 13                	jne    f0104ba0 <memmove+0x3b>
f0104b8d:	f6 c1 03             	test   $0x3,%cl
f0104b90:	75 0e                	jne    f0104ba0 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
f0104b92:	83 ef 04             	sub    $0x4,%edi
f0104b95:	8d 72 fc             	lea    -0x4(%edx),%esi
f0104b98:	c1 e9 02             	shr    $0x2,%ecx
f0104b9b:	fd                   	std    
f0104b9c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f0104b9e:	eb 07                	jmp    f0104ba7 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
f0104ba0:	4f                   	dec    %edi
f0104ba1:	8d 72 ff             	lea    -0x1(%edx),%esi
f0104ba4:	fd                   	std    
f0104ba5:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
f0104ba7:	fc                   	cld    
f0104ba8:	eb 1d                	jmp    f0104bc7 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
f0104baa:	89 f2                	mov    %esi,%edx
f0104bac:	09 c2                	or     %eax,%edx
f0104bae:	f6 c2 03             	test   $0x3,%dl
f0104bb1:	75 0f                	jne    f0104bc2 <memmove+0x5d>
f0104bb3:	f6 c1 03             	test   $0x3,%cl
f0104bb6:	75 0a                	jne    f0104bc2 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
f0104bb8:	c1 e9 02             	shr    $0x2,%ecx
f0104bbb:	89 c7                	mov    %eax,%edi
f0104bbd:	fc                   	cld    
f0104bbe:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f0104bc0:	eb 05                	jmp    f0104bc7 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
f0104bc2:	89 c7                	mov    %eax,%edi
f0104bc4:	fc                   	cld    
f0104bc5:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
f0104bc7:	5e                   	pop    %esi
f0104bc8:	5f                   	pop    %edi
f0104bc9:	5d                   	pop    %ebp
f0104bca:	c3                   	ret    

f0104bcb <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
f0104bcb:	55                   	push   %ebp
f0104bcc:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
f0104bce:	ff 75 10             	pushl  0x10(%ebp)
f0104bd1:	ff 75 0c             	pushl  0xc(%ebp)
f0104bd4:	ff 75 08             	pushl  0x8(%ebp)
f0104bd7:	e8 89 ff ff ff       	call   f0104b65 <memmove>
}
f0104bdc:	c9                   	leave  
f0104bdd:	c3                   	ret    

f0104bde <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
f0104bde:	55                   	push   %ebp
f0104bdf:	89 e5                	mov    %esp,%ebp
f0104be1:	56                   	push   %esi
f0104be2:	53                   	push   %ebx
f0104be3:	8b 45 08             	mov    0x8(%ebp),%eax
f0104be6:	8b 55 0c             	mov    0xc(%ebp),%edx
f0104be9:	89 c6                	mov    %eax,%esi
f0104beb:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
f0104bee:	eb 14                	jmp    f0104c04 <memcmp+0x26>
		if (*s1 != *s2)
f0104bf0:	8a 08                	mov    (%eax),%cl
f0104bf2:	8a 1a                	mov    (%edx),%bl
f0104bf4:	38 d9                	cmp    %bl,%cl
f0104bf6:	74 0a                	je     f0104c02 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
f0104bf8:	0f b6 c1             	movzbl %cl,%eax
f0104bfb:	0f b6 db             	movzbl %bl,%ebx
f0104bfe:	29 d8                	sub    %ebx,%eax
f0104c00:	eb 0b                	jmp    f0104c0d <memcmp+0x2f>
		s1++, s2++;
f0104c02:	40                   	inc    %eax
f0104c03:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
f0104c04:	39 f0                	cmp    %esi,%eax
f0104c06:	75 e8                	jne    f0104bf0 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
f0104c08:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0104c0d:	5b                   	pop    %ebx
f0104c0e:	5e                   	pop    %esi
f0104c0f:	5d                   	pop    %ebp
f0104c10:	c3                   	ret    

f0104c11 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
f0104c11:	55                   	push   %ebp
f0104c12:	89 e5                	mov    %esp,%ebp
f0104c14:	53                   	push   %ebx
f0104c15:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
f0104c18:	89 c1                	mov    %eax,%ecx
f0104c1a:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
f0104c1d:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f0104c21:	eb 08                	jmp    f0104c2b <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
f0104c23:	0f b6 10             	movzbl (%eax),%edx
f0104c26:	39 da                	cmp    %ebx,%edx
f0104c28:	74 05                	je     f0104c2f <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f0104c2a:	40                   	inc    %eax
f0104c2b:	39 c8                	cmp    %ecx,%eax
f0104c2d:	72 f4                	jb     f0104c23 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
f0104c2f:	5b                   	pop    %ebx
f0104c30:	5d                   	pop    %ebp
f0104c31:	c3                   	ret    

f0104c32 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
f0104c32:	55                   	push   %ebp
f0104c33:	89 e5                	mov    %esp,%ebp
f0104c35:	57                   	push   %edi
f0104c36:	56                   	push   %esi
f0104c37:	53                   	push   %ebx
f0104c38:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f0104c3b:	eb 01                	jmp    f0104c3e <strtol+0xc>
		s++;
f0104c3d:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f0104c3e:	8a 01                	mov    (%ecx),%al
f0104c40:	3c 20                	cmp    $0x20,%al
f0104c42:	74 f9                	je     f0104c3d <strtol+0xb>
f0104c44:	3c 09                	cmp    $0x9,%al
f0104c46:	74 f5                	je     f0104c3d <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
f0104c48:	3c 2b                	cmp    $0x2b,%al
f0104c4a:	75 08                	jne    f0104c54 <strtol+0x22>
		s++;
f0104c4c:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
f0104c4d:	bf 00 00 00 00       	mov    $0x0,%edi
f0104c52:	eb 11                	jmp    f0104c65 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
f0104c54:	3c 2d                	cmp    $0x2d,%al
f0104c56:	75 08                	jne    f0104c60 <strtol+0x2e>
		s++, neg = 1;
f0104c58:	41                   	inc    %ecx
f0104c59:	bf 01 00 00 00       	mov    $0x1,%edi
f0104c5e:	eb 05                	jmp    f0104c65 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
f0104c60:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f0104c65:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0104c69:	0f 84 87 00 00 00    	je     f0104cf6 <strtol+0xc4>
f0104c6f:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
f0104c73:	75 27                	jne    f0104c9c <strtol+0x6a>
f0104c75:	80 39 30             	cmpb   $0x30,(%ecx)
f0104c78:	75 22                	jne    f0104c9c <strtol+0x6a>
f0104c7a:	e9 88 00 00 00       	jmp    f0104d07 <strtol+0xd5>
		s += 2, base = 16;
f0104c7f:	83 c1 02             	add    $0x2,%ecx
f0104c82:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
f0104c89:	eb 11                	jmp    f0104c9c <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
f0104c8b:	41                   	inc    %ecx
f0104c8c:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
f0104c93:	eb 07                	jmp    f0104c9c <strtol+0x6a>
	else if (base == 0)
		base = 10;
f0104c95:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
f0104c9c:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
f0104ca1:	8a 11                	mov    (%ecx),%dl
f0104ca3:	8d 5a d0             	lea    -0x30(%edx),%ebx
f0104ca6:	80 fb 09             	cmp    $0x9,%bl
f0104ca9:	77 08                	ja     f0104cb3 <strtol+0x81>
			dig = *s - '0';
f0104cab:	0f be d2             	movsbl %dl,%edx
f0104cae:	83 ea 30             	sub    $0x30,%edx
f0104cb1:	eb 22                	jmp    f0104cd5 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
f0104cb3:	8d 72 9f             	lea    -0x61(%edx),%esi
f0104cb6:	89 f3                	mov    %esi,%ebx
f0104cb8:	80 fb 19             	cmp    $0x19,%bl
f0104cbb:	77 08                	ja     f0104cc5 <strtol+0x93>
			dig = *s - 'a' + 10;
f0104cbd:	0f be d2             	movsbl %dl,%edx
f0104cc0:	83 ea 57             	sub    $0x57,%edx
f0104cc3:	eb 10                	jmp    f0104cd5 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
f0104cc5:	8d 72 bf             	lea    -0x41(%edx),%esi
f0104cc8:	89 f3                	mov    %esi,%ebx
f0104cca:	80 fb 19             	cmp    $0x19,%bl
f0104ccd:	77 14                	ja     f0104ce3 <strtol+0xb1>
			dig = *s - 'A' + 10;
f0104ccf:	0f be d2             	movsbl %dl,%edx
f0104cd2:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
f0104cd5:	3b 55 10             	cmp    0x10(%ebp),%edx
f0104cd8:	7d 09                	jge    f0104ce3 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
f0104cda:	41                   	inc    %ecx
f0104cdb:	0f af 45 10          	imul   0x10(%ebp),%eax
f0104cdf:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
f0104ce1:	eb be                	jmp    f0104ca1 <strtol+0x6f>

	if (endptr)
f0104ce3:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f0104ce7:	74 05                	je     f0104cee <strtol+0xbc>
		*endptr = (char *) s;
f0104ce9:	8b 75 0c             	mov    0xc(%ebp),%esi
f0104cec:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
f0104cee:	85 ff                	test   %edi,%edi
f0104cf0:	74 21                	je     f0104d13 <strtol+0xe1>
f0104cf2:	f7 d8                	neg    %eax
f0104cf4:	eb 1d                	jmp    f0104d13 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f0104cf6:	80 39 30             	cmpb   $0x30,(%ecx)
f0104cf9:	75 9a                	jne    f0104c95 <strtol+0x63>
f0104cfb:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
f0104cff:	0f 84 7a ff ff ff    	je     f0104c7f <strtol+0x4d>
f0104d05:	eb 84                	jmp    f0104c8b <strtol+0x59>
f0104d07:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
f0104d0b:	0f 84 6e ff ff ff    	je     f0104c7f <strtol+0x4d>
f0104d11:	eb 89                	jmp    f0104c9c <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
f0104d13:	5b                   	pop    %ebx
f0104d14:	5e                   	pop    %esi
f0104d15:	5f                   	pop    %edi
f0104d16:	5d                   	pop    %ebp
f0104d17:	c3                   	ret    

f0104d18 <__udivdi3>:
f0104d18:	55                   	push   %ebp
f0104d19:	57                   	push   %edi
f0104d1a:	56                   	push   %esi
f0104d1b:	53                   	push   %ebx
f0104d1c:	83 ec 1c             	sub    $0x1c,%esp
f0104d1f:	8b 5c 24 30          	mov    0x30(%esp),%ebx
f0104d23:	8b 4c 24 34          	mov    0x34(%esp),%ecx
f0104d27:	8b 7c 24 38          	mov    0x38(%esp),%edi
f0104d2b:	89 5c 24 08          	mov    %ebx,0x8(%esp)
f0104d2f:	89 ca                	mov    %ecx,%edx
f0104d31:	89 f8                	mov    %edi,%eax
f0104d33:	8b 74 24 3c          	mov    0x3c(%esp),%esi
f0104d37:	85 f6                	test   %esi,%esi
f0104d39:	75 2d                	jne    f0104d68 <__udivdi3+0x50>
f0104d3b:	39 cf                	cmp    %ecx,%edi
f0104d3d:	77 65                	ja     f0104da4 <__udivdi3+0x8c>
f0104d3f:	89 fd                	mov    %edi,%ebp
f0104d41:	85 ff                	test   %edi,%edi
f0104d43:	75 0b                	jne    f0104d50 <__udivdi3+0x38>
f0104d45:	b8 01 00 00 00       	mov    $0x1,%eax
f0104d4a:	31 d2                	xor    %edx,%edx
f0104d4c:	f7 f7                	div    %edi
f0104d4e:	89 c5                	mov    %eax,%ebp
f0104d50:	31 d2                	xor    %edx,%edx
f0104d52:	89 c8                	mov    %ecx,%eax
f0104d54:	f7 f5                	div    %ebp
f0104d56:	89 c1                	mov    %eax,%ecx
f0104d58:	89 d8                	mov    %ebx,%eax
f0104d5a:	f7 f5                	div    %ebp
f0104d5c:	89 cf                	mov    %ecx,%edi
f0104d5e:	89 fa                	mov    %edi,%edx
f0104d60:	83 c4 1c             	add    $0x1c,%esp
f0104d63:	5b                   	pop    %ebx
f0104d64:	5e                   	pop    %esi
f0104d65:	5f                   	pop    %edi
f0104d66:	5d                   	pop    %ebp
f0104d67:	c3                   	ret    
f0104d68:	39 ce                	cmp    %ecx,%esi
f0104d6a:	77 28                	ja     f0104d94 <__udivdi3+0x7c>
f0104d6c:	0f bd fe             	bsr    %esi,%edi
f0104d6f:	83 f7 1f             	xor    $0x1f,%edi
f0104d72:	75 40                	jne    f0104db4 <__udivdi3+0x9c>
f0104d74:	39 ce                	cmp    %ecx,%esi
f0104d76:	72 0a                	jb     f0104d82 <__udivdi3+0x6a>
f0104d78:	3b 44 24 08          	cmp    0x8(%esp),%eax
f0104d7c:	0f 87 9e 00 00 00    	ja     f0104e20 <__udivdi3+0x108>
f0104d82:	b8 01 00 00 00       	mov    $0x1,%eax
f0104d87:	89 fa                	mov    %edi,%edx
f0104d89:	83 c4 1c             	add    $0x1c,%esp
f0104d8c:	5b                   	pop    %ebx
f0104d8d:	5e                   	pop    %esi
f0104d8e:	5f                   	pop    %edi
f0104d8f:	5d                   	pop    %ebp
f0104d90:	c3                   	ret    
f0104d91:	8d 76 00             	lea    0x0(%esi),%esi
f0104d94:	31 ff                	xor    %edi,%edi
f0104d96:	31 c0                	xor    %eax,%eax
f0104d98:	89 fa                	mov    %edi,%edx
f0104d9a:	83 c4 1c             	add    $0x1c,%esp
f0104d9d:	5b                   	pop    %ebx
f0104d9e:	5e                   	pop    %esi
f0104d9f:	5f                   	pop    %edi
f0104da0:	5d                   	pop    %ebp
f0104da1:	c3                   	ret    
f0104da2:	66 90                	xchg   %ax,%ax
f0104da4:	89 d8                	mov    %ebx,%eax
f0104da6:	f7 f7                	div    %edi
f0104da8:	31 ff                	xor    %edi,%edi
f0104daa:	89 fa                	mov    %edi,%edx
f0104dac:	83 c4 1c             	add    $0x1c,%esp
f0104daf:	5b                   	pop    %ebx
f0104db0:	5e                   	pop    %esi
f0104db1:	5f                   	pop    %edi
f0104db2:	5d                   	pop    %ebp
f0104db3:	c3                   	ret    
f0104db4:	bd 20 00 00 00       	mov    $0x20,%ebp
f0104db9:	89 eb                	mov    %ebp,%ebx
f0104dbb:	29 fb                	sub    %edi,%ebx
f0104dbd:	89 f9                	mov    %edi,%ecx
f0104dbf:	d3 e6                	shl    %cl,%esi
f0104dc1:	89 c5                	mov    %eax,%ebp
f0104dc3:	88 d9                	mov    %bl,%cl
f0104dc5:	d3 ed                	shr    %cl,%ebp
f0104dc7:	89 e9                	mov    %ebp,%ecx
f0104dc9:	09 f1                	or     %esi,%ecx
f0104dcb:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
f0104dcf:	89 f9                	mov    %edi,%ecx
f0104dd1:	d3 e0                	shl    %cl,%eax
f0104dd3:	89 c5                	mov    %eax,%ebp
f0104dd5:	89 d6                	mov    %edx,%esi
f0104dd7:	88 d9                	mov    %bl,%cl
f0104dd9:	d3 ee                	shr    %cl,%esi
f0104ddb:	89 f9                	mov    %edi,%ecx
f0104ddd:	d3 e2                	shl    %cl,%edx
f0104ddf:	8b 44 24 08          	mov    0x8(%esp),%eax
f0104de3:	88 d9                	mov    %bl,%cl
f0104de5:	d3 e8                	shr    %cl,%eax
f0104de7:	09 c2                	or     %eax,%edx
f0104de9:	89 d0                	mov    %edx,%eax
f0104deb:	89 f2                	mov    %esi,%edx
f0104ded:	f7 74 24 0c          	divl   0xc(%esp)
f0104df1:	89 d6                	mov    %edx,%esi
f0104df3:	89 c3                	mov    %eax,%ebx
f0104df5:	f7 e5                	mul    %ebp
f0104df7:	39 d6                	cmp    %edx,%esi
f0104df9:	72 19                	jb     f0104e14 <__udivdi3+0xfc>
f0104dfb:	74 0b                	je     f0104e08 <__udivdi3+0xf0>
f0104dfd:	89 d8                	mov    %ebx,%eax
f0104dff:	31 ff                	xor    %edi,%edi
f0104e01:	e9 58 ff ff ff       	jmp    f0104d5e <__udivdi3+0x46>
f0104e06:	66 90                	xchg   %ax,%ax
f0104e08:	8b 54 24 08          	mov    0x8(%esp),%edx
f0104e0c:	89 f9                	mov    %edi,%ecx
f0104e0e:	d3 e2                	shl    %cl,%edx
f0104e10:	39 c2                	cmp    %eax,%edx
f0104e12:	73 e9                	jae    f0104dfd <__udivdi3+0xe5>
f0104e14:	8d 43 ff             	lea    -0x1(%ebx),%eax
f0104e17:	31 ff                	xor    %edi,%edi
f0104e19:	e9 40 ff ff ff       	jmp    f0104d5e <__udivdi3+0x46>
f0104e1e:	66 90                	xchg   %ax,%ax
f0104e20:	31 c0                	xor    %eax,%eax
f0104e22:	e9 37 ff ff ff       	jmp    f0104d5e <__udivdi3+0x46>
f0104e27:	90                   	nop

f0104e28 <__umoddi3>:
f0104e28:	55                   	push   %ebp
f0104e29:	57                   	push   %edi
f0104e2a:	56                   	push   %esi
f0104e2b:	53                   	push   %ebx
f0104e2c:	83 ec 1c             	sub    $0x1c,%esp
f0104e2f:	8b 4c 24 30          	mov    0x30(%esp),%ecx
f0104e33:	8b 74 24 34          	mov    0x34(%esp),%esi
f0104e37:	8b 7c 24 38          	mov    0x38(%esp),%edi
f0104e3b:	8b 44 24 3c          	mov    0x3c(%esp),%eax
f0104e3f:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0104e43:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0104e47:	89 f3                	mov    %esi,%ebx
f0104e49:	89 fa                	mov    %edi,%edx
f0104e4b:	89 4c 24 04          	mov    %ecx,0x4(%esp)
f0104e4f:	89 34 24             	mov    %esi,(%esp)
f0104e52:	85 c0                	test   %eax,%eax
f0104e54:	75 1a                	jne    f0104e70 <__umoddi3+0x48>
f0104e56:	39 f7                	cmp    %esi,%edi
f0104e58:	0f 86 a2 00 00 00    	jbe    f0104f00 <__umoddi3+0xd8>
f0104e5e:	89 c8                	mov    %ecx,%eax
f0104e60:	89 f2                	mov    %esi,%edx
f0104e62:	f7 f7                	div    %edi
f0104e64:	89 d0                	mov    %edx,%eax
f0104e66:	31 d2                	xor    %edx,%edx
f0104e68:	83 c4 1c             	add    $0x1c,%esp
f0104e6b:	5b                   	pop    %ebx
f0104e6c:	5e                   	pop    %esi
f0104e6d:	5f                   	pop    %edi
f0104e6e:	5d                   	pop    %ebp
f0104e6f:	c3                   	ret    
f0104e70:	39 f0                	cmp    %esi,%eax
f0104e72:	0f 87 ac 00 00 00    	ja     f0104f24 <__umoddi3+0xfc>
f0104e78:	0f bd e8             	bsr    %eax,%ebp
f0104e7b:	83 f5 1f             	xor    $0x1f,%ebp
f0104e7e:	0f 84 ac 00 00 00    	je     f0104f30 <__umoddi3+0x108>
f0104e84:	bf 20 00 00 00       	mov    $0x20,%edi
f0104e89:	29 ef                	sub    %ebp,%edi
f0104e8b:	89 fe                	mov    %edi,%esi
f0104e8d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
f0104e91:	89 e9                	mov    %ebp,%ecx
f0104e93:	d3 e0                	shl    %cl,%eax
f0104e95:	89 d7                	mov    %edx,%edi
f0104e97:	89 f1                	mov    %esi,%ecx
f0104e99:	d3 ef                	shr    %cl,%edi
f0104e9b:	09 c7                	or     %eax,%edi
f0104e9d:	89 e9                	mov    %ebp,%ecx
f0104e9f:	d3 e2                	shl    %cl,%edx
f0104ea1:	89 14 24             	mov    %edx,(%esp)
f0104ea4:	89 d8                	mov    %ebx,%eax
f0104ea6:	d3 e0                	shl    %cl,%eax
f0104ea8:	89 c2                	mov    %eax,%edx
f0104eaa:	8b 44 24 08          	mov    0x8(%esp),%eax
f0104eae:	d3 e0                	shl    %cl,%eax
f0104eb0:	89 44 24 04          	mov    %eax,0x4(%esp)
f0104eb4:	8b 44 24 08          	mov    0x8(%esp),%eax
f0104eb8:	89 f1                	mov    %esi,%ecx
f0104eba:	d3 e8                	shr    %cl,%eax
f0104ebc:	09 d0                	or     %edx,%eax
f0104ebe:	d3 eb                	shr    %cl,%ebx
f0104ec0:	89 da                	mov    %ebx,%edx
f0104ec2:	f7 f7                	div    %edi
f0104ec4:	89 d3                	mov    %edx,%ebx
f0104ec6:	f7 24 24             	mull   (%esp)
f0104ec9:	89 c6                	mov    %eax,%esi
f0104ecb:	89 d1                	mov    %edx,%ecx
f0104ecd:	39 d3                	cmp    %edx,%ebx
f0104ecf:	0f 82 87 00 00 00    	jb     f0104f5c <__umoddi3+0x134>
f0104ed5:	0f 84 91 00 00 00    	je     f0104f6c <__umoddi3+0x144>
f0104edb:	8b 54 24 04          	mov    0x4(%esp),%edx
f0104edf:	29 f2                	sub    %esi,%edx
f0104ee1:	19 cb                	sbb    %ecx,%ebx
f0104ee3:	89 d8                	mov    %ebx,%eax
f0104ee5:	8a 4c 24 0c          	mov    0xc(%esp),%cl
f0104ee9:	d3 e0                	shl    %cl,%eax
f0104eeb:	89 e9                	mov    %ebp,%ecx
f0104eed:	d3 ea                	shr    %cl,%edx
f0104eef:	09 d0                	or     %edx,%eax
f0104ef1:	89 e9                	mov    %ebp,%ecx
f0104ef3:	d3 eb                	shr    %cl,%ebx
f0104ef5:	89 da                	mov    %ebx,%edx
f0104ef7:	83 c4 1c             	add    $0x1c,%esp
f0104efa:	5b                   	pop    %ebx
f0104efb:	5e                   	pop    %esi
f0104efc:	5f                   	pop    %edi
f0104efd:	5d                   	pop    %ebp
f0104efe:	c3                   	ret    
f0104eff:	90                   	nop
f0104f00:	89 fd                	mov    %edi,%ebp
f0104f02:	85 ff                	test   %edi,%edi
f0104f04:	75 0b                	jne    f0104f11 <__umoddi3+0xe9>
f0104f06:	b8 01 00 00 00       	mov    $0x1,%eax
f0104f0b:	31 d2                	xor    %edx,%edx
f0104f0d:	f7 f7                	div    %edi
f0104f0f:	89 c5                	mov    %eax,%ebp
f0104f11:	89 f0                	mov    %esi,%eax
f0104f13:	31 d2                	xor    %edx,%edx
f0104f15:	f7 f5                	div    %ebp
f0104f17:	89 c8                	mov    %ecx,%eax
f0104f19:	f7 f5                	div    %ebp
f0104f1b:	89 d0                	mov    %edx,%eax
f0104f1d:	e9 44 ff ff ff       	jmp    f0104e66 <__umoddi3+0x3e>
f0104f22:	66 90                	xchg   %ax,%ax
f0104f24:	89 c8                	mov    %ecx,%eax
f0104f26:	89 f2                	mov    %esi,%edx
f0104f28:	83 c4 1c             	add    $0x1c,%esp
f0104f2b:	5b                   	pop    %ebx
f0104f2c:	5e                   	pop    %esi
f0104f2d:	5f                   	pop    %edi
f0104f2e:	5d                   	pop    %ebp
f0104f2f:	c3                   	ret    
f0104f30:	3b 04 24             	cmp    (%esp),%eax
f0104f33:	72 06                	jb     f0104f3b <__umoddi3+0x113>
f0104f35:	3b 7c 24 04          	cmp    0x4(%esp),%edi
f0104f39:	77 0f                	ja     f0104f4a <__umoddi3+0x122>
f0104f3b:	89 f2                	mov    %esi,%edx
f0104f3d:	29 f9                	sub    %edi,%ecx
f0104f3f:	1b 54 24 0c          	sbb    0xc(%esp),%edx
f0104f43:	89 14 24             	mov    %edx,(%esp)
f0104f46:	89 4c 24 04          	mov    %ecx,0x4(%esp)
f0104f4a:	8b 44 24 04          	mov    0x4(%esp),%eax
f0104f4e:	8b 14 24             	mov    (%esp),%edx
f0104f51:	83 c4 1c             	add    $0x1c,%esp
f0104f54:	5b                   	pop    %ebx
f0104f55:	5e                   	pop    %esi
f0104f56:	5f                   	pop    %edi
f0104f57:	5d                   	pop    %ebp
f0104f58:	c3                   	ret    
f0104f59:	8d 76 00             	lea    0x0(%esi),%esi
f0104f5c:	2b 04 24             	sub    (%esp),%eax
f0104f5f:	19 fa                	sbb    %edi,%edx
f0104f61:	89 d1                	mov    %edx,%ecx
f0104f63:	89 c6                	mov    %eax,%esi
f0104f65:	e9 71 ff ff ff       	jmp    f0104edb <__umoddi3+0xb3>
f0104f6a:	66 90                	xchg   %ax,%ax
f0104f6c:	39 44 24 04          	cmp    %eax,0x4(%esp)
f0104f70:	72 ea                	jb     f0104f5c <__umoddi3+0x134>
f0104f72:	89 d9                	mov    %ebx,%ecx
f0104f74:	e9 62 ff ff ff       	jmp    f0104edb <__umoddi3+0xb3>
