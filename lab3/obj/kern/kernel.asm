
obj/kern/kernel:     file format elf32-i386


Disassembly of section .text:

f0100000 <_start+0xeffffff4>:
.globl _start
_start = RELOC(entry)

.globl entry
entry:
	movw	$0x1234, 0x472			# warm boot
f0100000:	02 b0 ad 1b 02 00    	add    0x21bad(%eax),%dh
f0100006:	00 00                	add    %al,(%eax)
f0100008:	fc                   	cld    
f0100009:	4f                   	dec    %edi
f010000a:	52                   	push   %edx
f010000b:	e4 66                	in     $0x66,%al

f010000c <entry>:
f010000c:	66 c7 05 72 04 00 00 	movw   $0x1234,0x472
f0100013:	34 12 
	# sufficient until we set up our real page table in mem_init
	# in lab 2.

	# Load the physical address of entry_pgdir into cr3.  entry_pgdir
	# is defined in entrypgdir.c.
	movl	$(RELOC(entry_pgdir)), %ecx
f0100015:	b9 00 b0 11 00       	mov    $0x11b000,%ecx
	movl	%ecx, %cr3
f010001a:	0f 22 d9             	mov    %ecx,%cr3
	# Enable PSE for 4MB pages.
	movl	%cr4, %ecx
f010001d:	0f 20 e1             	mov    %cr4,%ecx
	orl	$(CR4_PSE), %ecx
f0100020:	83 c9 10             	or     $0x10,%ecx
	movl	%ecx, %cr4
f0100023:	0f 22 e1             	mov    %ecx,%cr4
	# Turn on paging.
	movl	%cr0, %ecx
f0100026:	0f 20 c1             	mov    %cr0,%ecx
	orl	$(CR0_PE|CR0_PG|CR0_WP), %ecx
f0100029:	81 c9 01 00 01 80    	or     $0x80010001,%ecx
	movl	%ecx, %cr0
f010002f:	0f 22 c1             	mov    %ecx,%cr0

	# Now paging is enabled, but we're still running at a low EIP
	# (why is this okay?).  Jump up above KERNBASE before entering
	# C code.
	mov	$relocated, %ecx
f0100032:	b9 39 00 10 f0       	mov    $0xf0100039,%ecx
	jmp	*%ecx
f0100037:	ff e1                	jmp    *%ecx

f0100039 <relocated>:
relocated:

	# Clear the frame pointer register (EBP)
	# so that once we get into debugging C code,
	# stack backtraces will be terminated properly.
	movl	$0x0, %ebp			# nuke frame pointer
f0100039:	bd 00 00 00 00       	mov    $0x0,%ebp

	# Set the stack pointer
	movl	$(bootstacktop),%esp
f010003e:	bc 00 b0 11 f0       	mov    $0xf011b000,%esp

	# pointer to struct multiboot_info
	pushl	%ebx
f0100043:	53                   	push   %ebx
	# saved magic value
	pushl	%eax
f0100044:	50                   	push   %eax

	# now to C code
	call	i386_init
f0100045:	e8 59 00 00 00       	call   f01000a3 <i386_init>

f010004a <spin>:

	# Should never get here, but in case we do, just spin.
spin:	jmp	spin
f010004a:	eb fe                	jmp    f010004a <spin>

f010004c <_panic>:
 * Panic is called on unresolvable fatal errors.
 * It prints "panic: mesg", and then enters the kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt,...)
{
f010004c:	55                   	push   %ebp
f010004d:	89 e5                	mov    %esp,%ebp
f010004f:	56                   	push   %esi
f0100050:	53                   	push   %ebx
f0100051:	8b 75 10             	mov    0x10(%ebp),%esi
	va_list ap;

	if (panicstr)
f0100054:	83 3d 20 8f 18 f0 00 	cmpl   $0x0,0xf0188f20
f010005b:	75 37                	jne    f0100094 <_panic+0x48>
		goto dead;
	panicstr = fmt;
f010005d:	89 35 20 8f 18 f0    	mov    %esi,0xf0188f20

	// Be extra sure that the machine is in as reasonable state
	asm volatile("cli; cld");
f0100063:	fa                   	cli    
f0100064:	fc                   	cld    

	va_start(ap, fmt);
f0100065:	8d 5d 14             	lea    0x14(%ebp),%ebx
	cprintf("kernel panic at %s:%d: ", file, line);
f0100068:	83 ec 04             	sub    $0x4,%esp
f010006b:	ff 75 0c             	pushl  0xc(%ebp)
f010006e:	ff 75 08             	pushl  0x8(%ebp)
f0100071:	68 c0 50 10 f0       	push   $0xf01050c0
f0100076:	e8 16 33 00 00       	call   f0103391 <cprintf>
	vcprintf(fmt, ap);
f010007b:	83 c4 08             	add    $0x8,%esp
f010007e:	53                   	push   %ebx
f010007f:	56                   	push   %esi
f0100080:	e8 e6 32 00 00       	call   f010336b <vcprintf>
	cprintf("\n");
f0100085:	c7 04 24 dd 62 10 f0 	movl   $0xf01062dd,(%esp)
f010008c:	e8 00 33 00 00       	call   f0103391 <cprintf>
	va_end(ap);
f0100091:	83 c4 10             	add    $0x10,%esp

dead:
	/* break into the kernel monitor */
	while (1)
		monitor(NULL);
f0100094:	83 ec 0c             	sub    $0xc,%esp
f0100097:	6a 00                	push   $0x0
f0100099:	e8 f5 07 00 00       	call   f0100893 <monitor>
f010009e:	83 c4 10             	add    $0x10,%esp
f01000a1:	eb f1                	jmp    f0100094 <_panic+0x48>

f01000a3 <i386_init>:
#include <kern/trap.h>


void
i386_init(uint32_t magic, uint32_t addr)
{
f01000a3:	55                   	push   %ebp
f01000a4:	89 e5                	mov    %esp,%ebp
f01000a6:	83 ec 0c             	sub    $0xc,%esp
	extern char edata[], end[];

	// Before doing anything else, complete the ELF loading process.
	// Clear the uninitialized global data (BSS) section of our program.
	// This ensures that all static/global variables start out zero.
	memset(edata, 0, end - edata);
f01000a9:	b8 50 94 18 f0       	mov    $0xf0189450,%eax
f01000ae:	2d 02 80 18 f0       	sub    $0xf0188002,%eax
f01000b3:	50                   	push   %eax
f01000b4:	6a 00                	push   $0x0
f01000b6:	68 02 80 18 f0       	push   $0xf0188002
f01000bb:	e8 83 4b 00 00       	call   f0104c43 <memset>

	// Initialize the console.
	// Can't call cprintf until after we do this!
	cons_init();
f01000c0:	e8 68 04 00 00       	call   f010052d <cons_init>

	// Must boot from Multiboot.
	assert(magic == MULTIBOOT_BOOTLOADER_MAGIC);
f01000c5:	83 c4 10             	add    $0x10,%esp
f01000c8:	81 7d 08 02 b0 ad 2b 	cmpl   $0x2badb002,0x8(%ebp)
f01000cf:	74 16                	je     f01000e7 <i386_init+0x44>
f01000d1:	68 40 51 10 f0       	push   $0xf0105140
f01000d6:	68 d8 50 10 f0       	push   $0xf01050d8
f01000db:	6a 20                	push   $0x20
f01000dd:	68 ed 50 10 f0       	push   $0xf01050ed
f01000e2:	e8 65 ff ff ff       	call   f010004c <_panic>

	cprintf("451 decimal is %o octal!\n", 451);
f01000e7:	83 ec 08             	sub    $0x8,%esp
f01000ea:	68 c3 01 00 00       	push   $0x1c3
f01000ef:	68 f9 50 10 f0       	push   $0xf01050f9
f01000f4:	e8 98 32 00 00       	call   f0103391 <cprintf>

	// Print CPU information.
	cpuid_print();
f01000f9:	e8 76 42 00 00       	call   f0104374 <cpuid_print>

	// Initialize e820 memory map.
	e820_init(addr);
f01000fe:	83 c4 04             	add    $0x4,%esp
f0100101:	ff 75 0c             	pushl  0xc(%ebp)
f0100104:	e8 ce 08 00 00       	call   f01009d7 <e820_init>

	// Lab 2 memory management initialization functions
	mem_init();
f0100109:	e8 22 13 00 00       	call   f0101430 <mem_init>

	// Lab 3 user environment initialization functions
	env_init();
f010010e:	e8 c7 2c 00 00       	call   f0102dda <env_init>
	cprintf("After env_init\n");
f0100113:	c7 04 24 13 51 10 f0 	movl   $0xf0105113,(%esp)
f010011a:	e8 72 32 00 00       	call   f0103391 <cprintf>
	trap_init();
f010011f:	e8 de 32 00 00       	call   f0103402 <trap_init>
#if defined(TEST)
	// Don't touch -- used by grading script!
	//cprintf("H3\n");
	ENV_CREATE(TEST, ENV_TYPE_USER);
f0100124:	83 c4 08             	add    $0x8,%esp
f0100127:	6a 00                	push   $0x0
f0100129:	68 aa 4d 13 f0       	push   $0xf0134daa
f010012e:	e8 6c 2e 00 00       	call   f0102f9f <env_create>
	// Touch all you want.
	ENV_CREATE(user_hello, ENV_TYPE_USER);
	cprintf("After ENV_CREATE(user_hello, ENV_TYPE_USER)\n");
#endif // TEST*
	// We only have one user environment for now, so just run it.
	env_run(&envs[0]);
f0100133:	83 c4 04             	add    $0x4,%esp
f0100136:	ff 35 68 82 18 f0    	pushl  0xf0188268
f010013c:	e8 a7 31 00 00       	call   f01032e8 <env_run>

f0100141 <_warn>:
}

/* like panic, but don't */
void
_warn(const char *file, int line, const char *fmt,...)
{
f0100141:	55                   	push   %ebp
f0100142:	89 e5                	mov    %esp,%ebp
f0100144:	53                   	push   %ebx
f0100145:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
f0100148:	8d 5d 14             	lea    0x14(%ebp),%ebx
	cprintf("kernel warning at %s:%d: ", file, line);
f010014b:	ff 75 0c             	pushl  0xc(%ebp)
f010014e:	ff 75 08             	pushl  0x8(%ebp)
f0100151:	68 23 51 10 f0       	push   $0xf0105123
f0100156:	e8 36 32 00 00       	call   f0103391 <cprintf>
	vcprintf(fmt, ap);
f010015b:	83 c4 08             	add    $0x8,%esp
f010015e:	53                   	push   %ebx
f010015f:	ff 75 10             	pushl  0x10(%ebp)
f0100162:	e8 04 32 00 00       	call   f010336b <vcprintf>
	cprintf("\n");
f0100167:	c7 04 24 dd 62 10 f0 	movl   $0xf01062dd,(%esp)
f010016e:	e8 1e 32 00 00       	call   f0103391 <cprintf>
	va_end(ap);
}
f0100173:	83 c4 10             	add    $0x10,%esp
f0100176:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0100179:	c9                   	leave  
f010017a:	c3                   	ret    

f010017b <serial_proc_data>:

static bool serial_exists;

static int
serial_proc_data(void)
{
f010017b:	55                   	push   %ebp
f010017c:	89 e5                	mov    %esp,%ebp

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010017e:	ba fd 03 00 00       	mov    $0x3fd,%edx
f0100183:	ec                   	in     (%dx),%al
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
f0100184:	a8 01                	test   $0x1,%al
f0100186:	74 0b                	je     f0100193 <serial_proc_data+0x18>
f0100188:	ba f8 03 00 00       	mov    $0x3f8,%edx
f010018d:	ec                   	in     (%dx),%al
		return -1;
	return inb(COM1+COM_RX);
f010018e:	0f b6 c0             	movzbl %al,%eax
f0100191:	eb 05                	jmp    f0100198 <serial_proc_data+0x1d>

static int
serial_proc_data(void)
{
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
		return -1;
f0100193:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
	return inb(COM1+COM_RX);
}
f0100198:	5d                   	pop    %ebp
f0100199:	c3                   	ret    

f010019a <cons_intr>:

// called by device interrupt routines to feed input characters
// into the circular console input buffer.
static void
cons_intr(int (*proc)(void))
{
f010019a:	55                   	push   %ebp
f010019b:	89 e5                	mov    %esp,%ebp
f010019d:	53                   	push   %ebx
f010019e:	83 ec 04             	sub    $0x4,%esp
f01001a1:	89 c3                	mov    %eax,%ebx
	int c;

	while ((c = (*proc)()) != -1) {
f01001a3:	eb 2b                	jmp    f01001d0 <cons_intr+0x36>
		if (c == 0)
f01001a5:	85 c0                	test   %eax,%eax
f01001a7:	74 27                	je     f01001d0 <cons_intr+0x36>
			continue;
		cons.buf[cons.wpos++] = c;
f01001a9:	8b 0d 44 82 18 f0    	mov    0xf0188244,%ecx
f01001af:	8d 51 01             	lea    0x1(%ecx),%edx
f01001b2:	89 15 44 82 18 f0    	mov    %edx,0xf0188244
f01001b8:	88 81 40 80 18 f0    	mov    %al,-0xfe77fc0(%ecx)
		if (cons.wpos == CONSBUFSIZE)
f01001be:	81 fa 00 02 00 00    	cmp    $0x200,%edx
f01001c4:	75 0a                	jne    f01001d0 <cons_intr+0x36>
			cons.wpos = 0;
f01001c6:	c7 05 44 82 18 f0 00 	movl   $0x0,0xf0188244
f01001cd:	00 00 00 
static void
cons_intr(int (*proc)(void))
{
	int c;

	while ((c = (*proc)()) != -1) {
f01001d0:	ff d3                	call   *%ebx
f01001d2:	83 f8 ff             	cmp    $0xffffffff,%eax
f01001d5:	75 ce                	jne    f01001a5 <cons_intr+0xb>
			continue;
		cons.buf[cons.wpos++] = c;
		if (cons.wpos == CONSBUFSIZE)
			cons.wpos = 0;
	}
}
f01001d7:	83 c4 04             	add    $0x4,%esp
f01001da:	5b                   	pop    %ebx
f01001db:	5d                   	pop    %ebp
f01001dc:	c3                   	ret    

f01001dd <kbd_proc_data>:
f01001dd:	ba 64 00 00 00       	mov    $0x64,%edx
f01001e2:	ec                   	in     (%dx),%al
	int c;
	uint8_t stat, data;
	static uint32_t shift;

	stat = inb(KBSTATP);
	if ((stat & KBS_DIB) == 0)
f01001e3:	a8 01                	test   $0x1,%al
f01001e5:	0f 84 ee 00 00 00    	je     f01002d9 <kbd_proc_data+0xfc>
		return -1;
	// Ignore data from mouse.
	if (stat & KBS_TERR)
f01001eb:	a8 20                	test   $0x20,%al
f01001ed:	0f 85 ec 00 00 00    	jne    f01002df <kbd_proc_data+0x102>
f01001f3:	ba 60 00 00 00       	mov    $0x60,%edx
f01001f8:	ec                   	in     (%dx),%al
f01001f9:	88 c2                	mov    %al,%dl
		return -1;

	data = inb(KBDATAP);

	if (data == 0xE0) {
f01001fb:	3c e0                	cmp    $0xe0,%al
f01001fd:	75 0d                	jne    f010020c <kbd_proc_data+0x2f>
		// E0 escape character
		shift |= E0ESC;
f01001ff:	83 0d 20 80 18 f0 40 	orl    $0x40,0xf0188020
		return 0;
f0100206:	b8 00 00 00 00       	mov    $0x0,%eax
f010020b:	c3                   	ret    
	} else if (data & 0x80) {
f010020c:	84 c0                	test   %al,%al
f010020e:	79 2e                	jns    f010023e <kbd_proc_data+0x61>
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
f0100210:	8b 0d 20 80 18 f0    	mov    0xf0188020,%ecx
f0100216:	f6 c1 40             	test   $0x40,%cl
f0100219:	75 05                	jne    f0100220 <kbd_proc_data+0x43>
f010021b:	83 e0 7f             	and    $0x7f,%eax
f010021e:	88 c2                	mov    %al,%dl
		shift &= ~(shiftcode[data] | E0ESC);
f0100220:	0f b6 c2             	movzbl %dl,%eax
f0100223:	8a 80 c0 52 10 f0    	mov    -0xfefad40(%eax),%al
f0100229:	83 c8 40             	or     $0x40,%eax
f010022c:	0f b6 c0             	movzbl %al,%eax
f010022f:	f7 d0                	not    %eax
f0100231:	21 c8                	and    %ecx,%eax
f0100233:	a3 20 80 18 f0       	mov    %eax,0xf0188020
		return 0;
f0100238:	b8 00 00 00 00       	mov    $0x0,%eax
		cprintf("Rebooting!\n");
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
}
f010023d:	c3                   	ret    
 * Get data from the keyboard.  If we finish a character, return it.  Else 0.
 * Return -1 if no data.
 */
static int
kbd_proc_data(void)
{
f010023e:	55                   	push   %ebp
f010023f:	89 e5                	mov    %esp,%ebp
f0100241:	53                   	push   %ebx
f0100242:	83 ec 04             	sub    $0x4,%esp
	} else if (data & 0x80) {
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
		shift &= ~(shiftcode[data] | E0ESC);
		return 0;
	} else if (shift & E0ESC) {
f0100245:	8b 0d 20 80 18 f0    	mov    0xf0188020,%ecx
f010024b:	f6 c1 40             	test   $0x40,%cl
f010024e:	74 0e                	je     f010025e <kbd_proc_data+0x81>
		// Last character was an E0 escape; or with 0x80
		data |= 0x80;
f0100250:	83 c8 80             	or     $0xffffff80,%eax
f0100253:	88 c2                	mov    %al,%dl
		shift &= ~E0ESC;
f0100255:	83 e1 bf             	and    $0xffffffbf,%ecx
f0100258:	89 0d 20 80 18 f0    	mov    %ecx,0xf0188020
	}

	shift |= shiftcode[data];
f010025e:	0f b6 c2             	movzbl %dl,%eax
	shift ^= togglecode[data];
f0100261:	0f b6 90 c0 52 10 f0 	movzbl -0xfefad40(%eax),%edx
f0100268:	0b 15 20 80 18 f0    	or     0xf0188020,%edx
f010026e:	0f b6 88 c0 51 10 f0 	movzbl -0xfefae40(%eax),%ecx
f0100275:	31 ca                	xor    %ecx,%edx
f0100277:	89 15 20 80 18 f0    	mov    %edx,0xf0188020

	c = charcode[shift & (CTL | SHIFT)][data];
f010027d:	89 d1                	mov    %edx,%ecx
f010027f:	83 e1 03             	and    $0x3,%ecx
f0100282:	8b 0c 8d a0 51 10 f0 	mov    -0xfefae60(,%ecx,4),%ecx
f0100289:	8a 04 01             	mov    (%ecx,%eax,1),%al
f010028c:	0f b6 d8             	movzbl %al,%ebx
	if (shift & CAPSLOCK) {
f010028f:	f6 c2 08             	test   $0x8,%dl
f0100292:	74 1a                	je     f01002ae <kbd_proc_data+0xd1>
		if ('a' <= c && c <= 'z')
f0100294:	89 d8                	mov    %ebx,%eax
f0100296:	8d 4b 9f             	lea    -0x61(%ebx),%ecx
f0100299:	83 f9 19             	cmp    $0x19,%ecx
f010029c:	77 05                	ja     f01002a3 <kbd_proc_data+0xc6>
			c += 'A' - 'a';
f010029e:	83 eb 20             	sub    $0x20,%ebx
f01002a1:	eb 0b                	jmp    f01002ae <kbd_proc_data+0xd1>
		else if ('A' <= c && c <= 'Z')
f01002a3:	83 e8 41             	sub    $0x41,%eax
f01002a6:	83 f8 19             	cmp    $0x19,%eax
f01002a9:	77 03                	ja     f01002ae <kbd_proc_data+0xd1>
			c += 'a' - 'A';
f01002ab:	83 c3 20             	add    $0x20,%ebx
	}

	// Process special keys
	// Ctrl-Alt-Del: reboot
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
f01002ae:	f7 d2                	not    %edx
f01002b0:	f6 c2 06             	test   $0x6,%dl
f01002b3:	75 30                	jne    f01002e5 <kbd_proc_data+0x108>
f01002b5:	81 fb e9 00 00 00    	cmp    $0xe9,%ebx
f01002bb:	75 2c                	jne    f01002e9 <kbd_proc_data+0x10c>
		cprintf("Rebooting!\n");
f01002bd:	83 ec 0c             	sub    $0xc,%esp
f01002c0:	68 64 51 10 f0       	push   $0xf0105164
f01002c5:	e8 c7 30 00 00       	call   f0103391 <cprintf>
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01002ca:	ba 92 00 00 00       	mov    $0x92,%edx
f01002cf:	b0 03                	mov    $0x3,%al
f01002d1:	ee                   	out    %al,(%dx)
f01002d2:	83 c4 10             	add    $0x10,%esp
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f01002d5:	89 d8                	mov    %ebx,%eax
f01002d7:	eb 12                	jmp    f01002eb <kbd_proc_data+0x10e>
	uint8_t stat, data;
	static uint32_t shift;

	stat = inb(KBSTATP);
	if ((stat & KBS_DIB) == 0)
		return -1;
f01002d9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f01002de:	c3                   	ret    
	// Ignore data from mouse.
	if (stat & KBS_TERR)
		return -1;
f01002df:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f01002e4:	c3                   	ret    
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
		cprintf("Rebooting!\n");
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f01002e5:	89 d8                	mov    %ebx,%eax
f01002e7:	eb 02                	jmp    f01002eb <kbd_proc_data+0x10e>
f01002e9:	89 d8                	mov    %ebx,%eax
}
f01002eb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01002ee:	c9                   	leave  
f01002ef:	c3                   	ret    

f01002f0 <cons_putc>:
}

// output a character to the console
static void
cons_putc(int c)
{
f01002f0:	55                   	push   %ebp
f01002f1:	89 e5                	mov    %esp,%ebp
f01002f3:	57                   	push   %edi
f01002f4:	56                   	push   %esi
f01002f5:	53                   	push   %ebx
f01002f6:	83 ec 1c             	sub    $0x1c,%esp
f01002f9:	89 c7                	mov    %eax,%edi
f01002fb:	bb 01 32 00 00       	mov    $0x3201,%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100300:	be fd 03 00 00       	mov    $0x3fd,%esi
f0100305:	b9 84 00 00 00       	mov    $0x84,%ecx
f010030a:	eb 06                	jmp    f0100312 <cons_putc+0x22>
f010030c:	89 ca                	mov    %ecx,%edx
f010030e:	ec                   	in     (%dx),%al
f010030f:	ec                   	in     (%dx),%al
f0100310:	ec                   	in     (%dx),%al
f0100311:	ec                   	in     (%dx),%al
f0100312:	89 f2                	mov    %esi,%edx
f0100314:	ec                   	in     (%dx),%al
static void
serial_putc(int c)
{
	int i;

	for (i = 0;
f0100315:	a8 20                	test   $0x20,%al
f0100317:	75 03                	jne    f010031c <cons_putc+0x2c>
	     !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800;
f0100319:	4b                   	dec    %ebx
f010031a:	75 f0                	jne    f010030c <cons_putc+0x1c>
f010031c:	89 f8                	mov    %edi,%eax
f010031e:	88 45 e7             	mov    %al,-0x19(%ebp)
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100321:	ba f8 03 00 00       	mov    $0x3f8,%edx
f0100326:	ee                   	out    %al,(%dx)
f0100327:	bb 01 32 00 00       	mov    $0x3201,%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010032c:	be 79 03 00 00       	mov    $0x379,%esi
f0100331:	b9 84 00 00 00       	mov    $0x84,%ecx
f0100336:	eb 06                	jmp    f010033e <cons_putc+0x4e>
f0100338:	89 ca                	mov    %ecx,%edx
f010033a:	ec                   	in     (%dx),%al
f010033b:	ec                   	in     (%dx),%al
f010033c:	ec                   	in     (%dx),%al
f010033d:	ec                   	in     (%dx),%al
f010033e:	89 f2                	mov    %esi,%edx
f0100340:	ec                   	in     (%dx),%al
static void
lpt_putc(int c)
{
	int i;

	for (i = 0; !(inb(0x378+1) & 0x80) && i < 12800; i++)
f0100341:	84 c0                	test   %al,%al
f0100343:	78 03                	js     f0100348 <cons_putc+0x58>
f0100345:	4b                   	dec    %ebx
f0100346:	75 f0                	jne    f0100338 <cons_putc+0x48>
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100348:	ba 78 03 00 00       	mov    $0x378,%edx
f010034d:	8a 45 e7             	mov    -0x19(%ebp),%al
f0100350:	ee                   	out    %al,(%dx)
f0100351:	ba 7a 03 00 00       	mov    $0x37a,%edx
f0100356:	b0 0d                	mov    $0xd,%al
f0100358:	ee                   	out    %al,(%dx)
f0100359:	b0 08                	mov    $0x8,%al
f010035b:	ee                   	out    %al,(%dx)

static void
cga_putc(int c)
{
	// if no attribute given, then use black on white
	if (!(c & ~0xFF))
f010035c:	f7 c7 00 ff ff ff    	test   $0xffffff00,%edi
f0100362:	75 06                	jne    f010036a <cons_putc+0x7a>
		c |= 0x0500;
f0100364:	81 cf 00 05 00 00    	or     $0x500,%edi

	switch (c & 0xff) {
f010036a:	89 f8                	mov    %edi,%eax
f010036c:	0f b6 c0             	movzbl %al,%eax
f010036f:	83 f8 09             	cmp    $0x9,%eax
f0100372:	74 75                	je     f01003e9 <cons_putc+0xf9>
f0100374:	83 f8 09             	cmp    $0x9,%eax
f0100377:	7f 0a                	jg     f0100383 <cons_putc+0x93>
f0100379:	83 f8 08             	cmp    $0x8,%eax
f010037c:	74 14                	je     f0100392 <cons_putc+0xa2>
f010037e:	e9 9a 00 00 00       	jmp    f010041d <cons_putc+0x12d>
f0100383:	83 f8 0a             	cmp    $0xa,%eax
f0100386:	74 38                	je     f01003c0 <cons_putc+0xd0>
f0100388:	83 f8 0d             	cmp    $0xd,%eax
f010038b:	74 3b                	je     f01003c8 <cons_putc+0xd8>
f010038d:	e9 8b 00 00 00       	jmp    f010041d <cons_putc+0x12d>
	case '\b':
		if (crt_pos > 0) {
f0100392:	66 a1 48 82 18 f0    	mov    0xf0188248,%ax
f0100398:	66 85 c0             	test   %ax,%ax
f010039b:	0f 84 e7 00 00 00    	je     f0100488 <cons_putc+0x198>
			crt_pos--;
f01003a1:	48                   	dec    %eax
f01003a2:	66 a3 48 82 18 f0    	mov    %ax,0xf0188248
			crt_buf[crt_pos] = (c & ~0xff) | ' ';
f01003a8:	0f b7 c0             	movzwl %ax,%eax
f01003ab:	81 e7 00 ff ff ff    	and    $0xffffff00,%edi
f01003b1:	83 cf 20             	or     $0x20,%edi
f01003b4:	8b 15 4c 82 18 f0    	mov    0xf018824c,%edx
f01003ba:	66 89 3c 42          	mov    %di,(%edx,%eax,2)
f01003be:	eb 7a                	jmp    f010043a <cons_putc+0x14a>
		}
		break;
	case '\n':
		crt_pos += CRT_COLS;
f01003c0:	66 83 05 48 82 18 f0 	addw   $0x50,0xf0188248
f01003c7:	50 
		/* fallthru */
	case '\r':
		crt_pos -= (crt_pos % CRT_COLS);
f01003c8:	66 8b 0d 48 82 18 f0 	mov    0xf0188248,%cx
f01003cf:	bb 50 00 00 00       	mov    $0x50,%ebx
f01003d4:	89 c8                	mov    %ecx,%eax
f01003d6:	ba 00 00 00 00       	mov    $0x0,%edx
f01003db:	66 f7 f3             	div    %bx
f01003de:	29 d1                	sub    %edx,%ecx
f01003e0:	66 89 0d 48 82 18 f0 	mov    %cx,0xf0188248
f01003e7:	eb 51                	jmp    f010043a <cons_putc+0x14a>
		break;
	case '\t':
		cons_putc(' ');
f01003e9:	b8 20 00 00 00       	mov    $0x20,%eax
f01003ee:	e8 fd fe ff ff       	call   f01002f0 <cons_putc>
		cons_putc(' ');
f01003f3:	b8 20 00 00 00       	mov    $0x20,%eax
f01003f8:	e8 f3 fe ff ff       	call   f01002f0 <cons_putc>
		cons_putc(' ');
f01003fd:	b8 20 00 00 00       	mov    $0x20,%eax
f0100402:	e8 e9 fe ff ff       	call   f01002f0 <cons_putc>
		cons_putc(' ');
f0100407:	b8 20 00 00 00       	mov    $0x20,%eax
f010040c:	e8 df fe ff ff       	call   f01002f0 <cons_putc>
		cons_putc(' ');
f0100411:	b8 20 00 00 00       	mov    $0x20,%eax
f0100416:	e8 d5 fe ff ff       	call   f01002f0 <cons_putc>
f010041b:	eb 1d                	jmp    f010043a <cons_putc+0x14a>
		break;
	default:
		crt_buf[crt_pos++] = c;		/* write the character */
f010041d:	66 a1 48 82 18 f0    	mov    0xf0188248,%ax
f0100423:	8d 50 01             	lea    0x1(%eax),%edx
f0100426:	66 89 15 48 82 18 f0 	mov    %dx,0xf0188248
f010042d:	0f b7 c0             	movzwl %ax,%eax
f0100430:	8b 15 4c 82 18 f0    	mov    0xf018824c,%edx
f0100436:	66 89 3c 42          	mov    %di,(%edx,%eax,2)
		break;
	}

	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
f010043a:	66 81 3d 48 82 18 f0 	cmpw   $0x7cf,0xf0188248
f0100441:	cf 07 
f0100443:	76 43                	jbe    f0100488 <cons_putc+0x198>
		int i;

		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
f0100445:	a1 4c 82 18 f0       	mov    0xf018824c,%eax
f010044a:	83 ec 04             	sub    $0x4,%esp
f010044d:	68 00 0f 00 00       	push   $0xf00
f0100452:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
f0100458:	52                   	push   %edx
f0100459:	50                   	push   %eax
f010045a:	e8 31 48 00 00       	call   f0104c90 <memmove>
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
			crt_buf[i] = 0x0700 | ' ';
f010045f:	8b 15 4c 82 18 f0    	mov    0xf018824c,%edx
f0100465:	8d 82 00 0f 00 00    	lea    0xf00(%edx),%eax
f010046b:	81 c2 a0 0f 00 00    	add    $0xfa0,%edx
f0100471:	83 c4 10             	add    $0x10,%esp
f0100474:	66 c7 00 20 07       	movw   $0x720,(%eax)
f0100479:	83 c0 02             	add    $0x2,%eax
	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
		int i;

		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
f010047c:	39 d0                	cmp    %edx,%eax
f010047e:	75 f4                	jne    f0100474 <cons_putc+0x184>
			crt_buf[i] = 0x0700 | ' ';
		crt_pos -= CRT_COLS;
f0100480:	66 83 2d 48 82 18 f0 	subw   $0x50,0xf0188248
f0100487:	50 
	}

	/* move that little blinky thing */
	outb(addr_6845, 14);
f0100488:	8b 0d 50 82 18 f0    	mov    0xf0188250,%ecx
f010048e:	b0 0e                	mov    $0xe,%al
f0100490:	89 ca                	mov    %ecx,%edx
f0100492:	ee                   	out    %al,(%dx)
	outb(addr_6845 + 1, crt_pos >> 8);
f0100493:	8d 59 01             	lea    0x1(%ecx),%ebx
f0100496:	66 a1 48 82 18 f0    	mov    0xf0188248,%ax
f010049c:	66 c1 e8 08          	shr    $0x8,%ax
f01004a0:	89 da                	mov    %ebx,%edx
f01004a2:	ee                   	out    %al,(%dx)
f01004a3:	b0 0f                	mov    $0xf,%al
f01004a5:	89 ca                	mov    %ecx,%edx
f01004a7:	ee                   	out    %al,(%dx)
f01004a8:	a0 48 82 18 f0       	mov    0xf0188248,%al
f01004ad:	89 da                	mov    %ebx,%edx
f01004af:	ee                   	out    %al,(%dx)
cons_putc(int c)
{
	serial_putc(c);
	lpt_putc(c);
	cga_putc(c);
}
f01004b0:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01004b3:	5b                   	pop    %ebx
f01004b4:	5e                   	pop    %esi
f01004b5:	5f                   	pop    %edi
f01004b6:	5d                   	pop    %ebp
f01004b7:	c3                   	ret    

f01004b8 <serial_intr>:
}

void
serial_intr(void)
{
	if (serial_exists)
f01004b8:	80 3d 54 82 18 f0 00 	cmpb   $0x0,0xf0188254
f01004bf:	74 11                	je     f01004d2 <serial_intr+0x1a>
	return inb(COM1+COM_RX);
}

void
serial_intr(void)
{
f01004c1:	55                   	push   %ebp
f01004c2:	89 e5                	mov    %esp,%ebp
f01004c4:	83 ec 08             	sub    $0x8,%esp
	if (serial_exists)
		cons_intr(serial_proc_data);
f01004c7:	b8 7b 01 10 f0       	mov    $0xf010017b,%eax
f01004cc:	e8 c9 fc ff ff       	call   f010019a <cons_intr>
}
f01004d1:	c9                   	leave  
f01004d2:	c3                   	ret    

f01004d3 <kbd_intr>:
	return c;
}

void
kbd_intr(void)
{
f01004d3:	55                   	push   %ebp
f01004d4:	89 e5                	mov    %esp,%ebp
f01004d6:	83 ec 08             	sub    $0x8,%esp
	cons_intr(kbd_proc_data);
f01004d9:	b8 dd 01 10 f0       	mov    $0xf01001dd,%eax
f01004de:	e8 b7 fc ff ff       	call   f010019a <cons_intr>
}
f01004e3:	c9                   	leave  
f01004e4:	c3                   	ret    

f01004e5 <cons_getc>:
}

// return the next input character from the console, or 0 if none waiting
int
cons_getc(void)
{
f01004e5:	55                   	push   %ebp
f01004e6:	89 e5                	mov    %esp,%ebp
f01004e8:	83 ec 08             	sub    $0x8,%esp
	int c;

	// poll for any pending input characters,
	// so that this function works even when interrupts are disabled
	// (e.g., when called from the kernel monitor).
	serial_intr();
f01004eb:	e8 c8 ff ff ff       	call   f01004b8 <serial_intr>
	kbd_intr();
f01004f0:	e8 de ff ff ff       	call   f01004d3 <kbd_intr>

	// grab the next character from the input buffer.
	if (cons.rpos != cons.wpos) {
f01004f5:	a1 40 82 18 f0       	mov    0xf0188240,%eax
f01004fa:	3b 05 44 82 18 f0    	cmp    0xf0188244,%eax
f0100500:	74 24                	je     f0100526 <cons_getc+0x41>
		c = cons.buf[cons.rpos++];
f0100502:	8d 50 01             	lea    0x1(%eax),%edx
f0100505:	89 15 40 82 18 f0    	mov    %edx,0xf0188240
f010050b:	0f b6 80 40 80 18 f0 	movzbl -0xfe77fc0(%eax),%eax
		if (cons.rpos == CONSBUFSIZE)
f0100512:	81 fa 00 02 00 00    	cmp    $0x200,%edx
f0100518:	75 11                	jne    f010052b <cons_getc+0x46>
			cons.rpos = 0;
f010051a:	c7 05 40 82 18 f0 00 	movl   $0x0,0xf0188240
f0100521:	00 00 00 
f0100524:	eb 05                	jmp    f010052b <cons_getc+0x46>
		return c;
	}
	return 0;
f0100526:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010052b:	c9                   	leave  
f010052c:	c3                   	ret    

f010052d <cons_init>:
}

// initialize the console devices
void
cons_init(void)
{
f010052d:	55                   	push   %ebp
f010052e:	89 e5                	mov    %esp,%ebp
f0100530:	56                   	push   %esi
f0100531:	53                   	push   %ebx
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
f0100532:	66 8b 15 00 80 0b f0 	mov    0xf00b8000,%dx
	*cp = (uint16_t) 0xA55A;
f0100539:	66 c7 05 00 80 0b f0 	movw   $0xa55a,0xf00b8000
f0100540:	5a a5 
	if (*cp != 0xA55A) {
f0100542:	66 a1 00 80 0b f0    	mov    0xf00b8000,%ax
f0100548:	66 3d 5a a5          	cmp    $0xa55a,%ax
f010054c:	74 11                	je     f010055f <cons_init+0x32>
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
		addr_6845 = MONO_BASE;
f010054e:	c7 05 50 82 18 f0 b4 	movl   $0x3b4,0xf0188250
f0100555:	03 00 00 

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
	*cp = (uint16_t) 0xA55A;
	if (*cp != 0xA55A) {
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
f0100558:	be 00 00 0b f0       	mov    $0xf00b0000,%esi
f010055d:	eb 16                	jmp    f0100575 <cons_init+0x48>
		addr_6845 = MONO_BASE;
	} else {
		*cp = was;
f010055f:	66 89 15 00 80 0b f0 	mov    %dx,0xf00b8000
		addr_6845 = CGA_BASE;
f0100566:	c7 05 50 82 18 f0 d4 	movl   $0x3d4,0xf0188250
f010056d:	03 00 00 
{
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
f0100570:	be 00 80 0b f0       	mov    $0xf00b8000,%esi
f0100575:	b0 0e                	mov    $0xe,%al
f0100577:	8b 15 50 82 18 f0    	mov    0xf0188250,%edx
f010057d:	ee                   	out    %al,(%dx)
		addr_6845 = CGA_BASE;
	}

	/* Extract cursor location */
	outb(addr_6845, 14);
	pos = inb(addr_6845 + 1) << 8;
f010057e:	8d 5a 01             	lea    0x1(%edx),%ebx

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100581:	89 da                	mov    %ebx,%edx
f0100583:	ec                   	in     (%dx),%al
f0100584:	0f b6 c8             	movzbl %al,%ecx
f0100587:	c1 e1 08             	shl    $0x8,%ecx
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f010058a:	b0 0f                	mov    $0xf,%al
f010058c:	8b 15 50 82 18 f0    	mov    0xf0188250,%edx
f0100592:	ee                   	out    %al,(%dx)

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100593:	89 da                	mov    %ebx,%edx
f0100595:	ec                   	in     (%dx),%al
	outb(addr_6845, 15);
	pos |= inb(addr_6845 + 1);

	crt_buf = (uint16_t*) cp;
f0100596:	89 35 4c 82 18 f0    	mov    %esi,0xf018824c
	crt_pos = pos;
f010059c:	0f b6 c0             	movzbl %al,%eax
f010059f:	09 c8                	or     %ecx,%eax
f01005a1:	66 a3 48 82 18 f0    	mov    %ax,0xf0188248
}

static inline void
outb(int port, uint8_t data)
{
	asm volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01005a7:	be fa 03 00 00       	mov    $0x3fa,%esi
f01005ac:	b0 00                	mov    $0x0,%al
f01005ae:	89 f2                	mov    %esi,%edx
f01005b0:	ee                   	out    %al,(%dx)
f01005b1:	ba fb 03 00 00       	mov    $0x3fb,%edx
f01005b6:	b0 80                	mov    $0x80,%al
f01005b8:	ee                   	out    %al,(%dx)
f01005b9:	bb f8 03 00 00       	mov    $0x3f8,%ebx
f01005be:	b0 0c                	mov    $0xc,%al
f01005c0:	89 da                	mov    %ebx,%edx
f01005c2:	ee                   	out    %al,(%dx)
f01005c3:	ba f9 03 00 00       	mov    $0x3f9,%edx
f01005c8:	b0 00                	mov    $0x0,%al
f01005ca:	ee                   	out    %al,(%dx)
f01005cb:	ba fb 03 00 00       	mov    $0x3fb,%edx
f01005d0:	b0 03                	mov    $0x3,%al
f01005d2:	ee                   	out    %al,(%dx)
f01005d3:	ba fc 03 00 00       	mov    $0x3fc,%edx
f01005d8:	b0 00                	mov    $0x0,%al
f01005da:	ee                   	out    %al,(%dx)
f01005db:	ba f9 03 00 00       	mov    $0x3f9,%edx
f01005e0:	b0 01                	mov    $0x1,%al
f01005e2:	ee                   	out    %al,(%dx)

static inline uint8_t
inb(int port)
{
	uint8_t data;
	asm volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01005e3:	ba fd 03 00 00       	mov    $0x3fd,%edx
f01005e8:	ec                   	in     (%dx),%al
f01005e9:	88 c1                	mov    %al,%cl
	// Enable rcv interrupts
	outb(COM1+COM_IER, COM_IER_RDI);

	// Clear any preexisting overrun indications and interrupts
	// Serial port doesn't exist if COM_LSR returns 0xFF
	serial_exists = (inb(COM1+COM_LSR) != 0xFF);
f01005eb:	3c ff                	cmp    $0xff,%al
f01005ed:	0f 95 05 54 82 18 f0 	setne  0xf0188254
f01005f4:	89 f2                	mov    %esi,%edx
f01005f6:	ec                   	in     (%dx),%al
f01005f7:	89 da                	mov    %ebx,%edx
f01005f9:	ec                   	in     (%dx),%al
{
	cga_init();
	kbd_init();
	serial_init();

	if (!serial_exists)
f01005fa:	80 f9 ff             	cmp    $0xff,%cl
f01005fd:	75 10                	jne    f010060f <cons_init+0xe2>
		cprintf("Serial port does not exist!\n");
f01005ff:	83 ec 0c             	sub    $0xc,%esp
f0100602:	68 70 51 10 f0       	push   $0xf0105170
f0100607:	e8 85 2d 00 00       	call   f0103391 <cprintf>
f010060c:	83 c4 10             	add    $0x10,%esp
}
f010060f:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0100612:	5b                   	pop    %ebx
f0100613:	5e                   	pop    %esi
f0100614:	5d                   	pop    %ebp
f0100615:	c3                   	ret    

f0100616 <cputchar>:

// `High'-level console I/O.  Used by readline and cprintf.

void
cputchar(int c)
{
f0100616:	55                   	push   %ebp
f0100617:	89 e5                	mov    %esp,%ebp
f0100619:	83 ec 08             	sub    $0x8,%esp
	cons_putc(c);
f010061c:	8b 45 08             	mov    0x8(%ebp),%eax
f010061f:	e8 cc fc ff ff       	call   f01002f0 <cons_putc>
}
f0100624:	c9                   	leave  
f0100625:	c3                   	ret    

f0100626 <getchar>:

int
getchar(void)
{
f0100626:	55                   	push   %ebp
f0100627:	89 e5                	mov    %esp,%ebp
f0100629:	83 ec 08             	sub    $0x8,%esp
	int c;

	while ((c = cons_getc()) == 0)
f010062c:	e8 b4 fe ff ff       	call   f01004e5 <cons_getc>
f0100631:	85 c0                	test   %eax,%eax
f0100633:	74 f7                	je     f010062c <getchar+0x6>
		/* do nothing */;
	return c;
}
f0100635:	c9                   	leave  
f0100636:	c3                   	ret    

f0100637 <iscons>:

int
iscons(int fdnum)
{
f0100637:	55                   	push   %ebp
f0100638:	89 e5                	mov    %esp,%ebp
	// used by readline
	return 1;
}
f010063a:	b8 01 00 00 00       	mov    $0x1,%eax
f010063f:	5d                   	pop    %ebp
f0100640:	c3                   	ret    

f0100641 <mon_help>:

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
f0100641:	55                   	push   %ebp
f0100642:	89 e5                	mov    %esp,%ebp
f0100644:	83 ec 0c             	sub    $0xc,%esp
	int i;

	for (i = 0; i < ARRAY_SIZE(commands); i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
f0100647:	68 c0 53 10 f0       	push   $0xf01053c0
f010064c:	68 de 53 10 f0       	push   $0xf01053de
f0100651:	68 e3 53 10 f0       	push   $0xf01053e3
f0100656:	e8 36 2d 00 00       	call   f0103391 <cprintf>
f010065b:	83 c4 0c             	add    $0xc,%esp
f010065e:	68 a4 54 10 f0       	push   $0xf01054a4
f0100663:	68 ec 53 10 f0       	push   $0xf01053ec
f0100668:	68 e3 53 10 f0       	push   $0xf01053e3
f010066d:	e8 1f 2d 00 00       	call   f0103391 <cprintf>
f0100672:	83 c4 0c             	add    $0xc,%esp
f0100675:	68 de 62 10 f0       	push   $0xf01062de
f010067a:	68 f5 53 10 f0       	push   $0xf01053f5
f010067f:	68 e3 53 10 f0       	push   $0xf01053e3
f0100684:	e8 08 2d 00 00       	call   f0103391 <cprintf>
	return 0;
}
f0100689:	b8 00 00 00 00       	mov    $0x0,%eax
f010068e:	c9                   	leave  
f010068f:	c3                   	ret    

f0100690 <mon_kerninfo>:

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
f0100690:	55                   	push   %ebp
f0100691:	89 e5                	mov    %esp,%ebp
f0100693:	83 ec 14             	sub    $0x14,%esp
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
f0100696:	68 ff 53 10 f0       	push   $0xf01053ff
f010069b:	e8 f1 2c 00 00       	call   f0103391 <cprintf>
	cprintf("  _start                  %08x (phys)\n", _start);
f01006a0:	83 c4 08             	add    $0x8,%esp
f01006a3:	68 0c 00 10 00       	push   $0x10000c
f01006a8:	68 cc 54 10 f0       	push   $0xf01054cc
f01006ad:	e8 df 2c 00 00       	call   f0103391 <cprintf>
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
f01006b2:	83 c4 0c             	add    $0xc,%esp
f01006b5:	68 0c 00 10 00       	push   $0x10000c
f01006ba:	68 0c 00 10 f0       	push   $0xf010000c
f01006bf:	68 f4 54 10 f0       	push   $0xf01054f4
f01006c4:	e8 c8 2c 00 00       	call   f0103391 <cprintf>
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
f01006c9:	83 c4 0c             	add    $0xc,%esp
f01006cc:	68 a5 50 10 00       	push   $0x1050a5
f01006d1:	68 a5 50 10 f0       	push   $0xf01050a5
f01006d6:	68 18 55 10 f0       	push   $0xf0105518
f01006db:	e8 b1 2c 00 00       	call   f0103391 <cprintf>
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
f01006e0:	83 c4 0c             	add    $0xc,%esp
f01006e3:	68 02 80 18 00       	push   $0x188002
f01006e8:	68 02 80 18 f0       	push   $0xf0188002
f01006ed:	68 3c 55 10 f0       	push   $0xf010553c
f01006f2:	e8 9a 2c 00 00       	call   f0103391 <cprintf>
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
f01006f7:	83 c4 0c             	add    $0xc,%esp
f01006fa:	68 50 94 18 00       	push   $0x189450
f01006ff:	68 50 94 18 f0       	push   $0xf0189450
f0100704:	68 60 55 10 f0       	push   $0xf0105560
f0100709:	e8 83 2c 00 00       	call   f0103391 <cprintf>
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
f010070e:	b8 4f 98 18 f0       	mov    $0xf018984f,%eax
f0100713:	2d 0c 00 10 f0       	sub    $0xf010000c,%eax
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
f0100718:	83 c4 08             	add    $0x8,%esp
f010071b:	25 00 fc ff ff       	and    $0xfffffc00,%eax
f0100720:	89 c2                	mov    %eax,%edx
f0100722:	85 c0                	test   %eax,%eax
f0100724:	79 06                	jns    f010072c <mon_kerninfo+0x9c>
f0100726:	8d 90 ff 03 00 00    	lea    0x3ff(%eax),%edx
f010072c:	c1 fa 0a             	sar    $0xa,%edx
f010072f:	52                   	push   %edx
f0100730:	68 84 55 10 f0       	push   $0xf0105584
f0100735:	e8 57 2c 00 00       	call   f0103391 <cprintf>
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}
f010073a:	b8 00 00 00 00       	mov    $0x0,%eax
f010073f:	c9                   	leave  
f0100740:	c3                   	ret    

f0100741 <print_stack_Info>:
ebp f010ff78  eip f01008ae  args 00000001 f010ff8c 00000000 f0110580 00000000
       kern/monitor.c:143: monitor+106
       (filename, line#, function name+offset)
*/
void print_stack_Info(uint32_t esp, uint32_t* return_Addr)
{
f0100741:	55                   	push   %ebp
f0100742:	89 e5                	mov    %esp,%ebp
f0100744:	57                   	push   %edi
f0100745:	56                   	push   %esi
f0100746:	53                   	push   %ebx
f0100747:	83 ec 30             	sub    $0x30,%esp
f010074a:	8b 75 0c             	mov    0xc(%ebp),%esi

	//if(0==debuginfo_eip((uintptr_t)(*return_Addr), info)){

	//}

	cprintf("ebp %08x  eip %08x", esp, *return_Addr);  //the current ebp == esp
f010074d:	ff 36                	pushl  (%esi)
f010074f:	ff 75 08             	pushl  0x8(%ebp)
f0100752:	68 18 54 10 f0       	push   $0xf0105418
f0100757:	e8 35 2c 00 00       	call   f0103391 <cprintf>
	//print 5 arguments
	uint32_t arg;
	cprintf("  args");
f010075c:	c7 04 24 2b 54 10 f0 	movl   $0xf010542b,(%esp)
f0100763:	e8 29 2c 00 00       	call   f0103391 <cprintf>
f0100768:	8d 5e 04             	lea    0x4(%esi),%ebx
f010076b:	8d 7e 18             	lea    0x18(%esi),%edi
f010076e:	83 c4 10             	add    $0x10,%esp
	for(int i=1; i<6; i++){
		arg = *( (uint32_t*)(return_Addr+i) );
		cprintf(" %08x", arg);
f0100771:	83 ec 08             	sub    $0x8,%esp
f0100774:	ff 33                	pushl  (%ebx)
f0100776:	68 25 54 10 f0       	push   $0xf0105425
f010077b:	e8 11 2c 00 00       	call   f0103391 <cprintf>
f0100780:	83 c3 04             	add    $0x4,%ebx

	cprintf("ebp %08x  eip %08x", esp, *return_Addr);  //the current ebp == esp
	//print 5 arguments
	uint32_t arg;
	cprintf("  args");
	for(int i=1; i<6; i++){
f0100783:	83 c4 10             	add    $0x10,%esp
f0100786:	39 fb                	cmp    %edi,%ebx
f0100788:	75 e7                	jne    f0100771 <print_stack_Info+0x30>
		arg = *( (uint32_t*)(return_Addr+i) );
		cprintf(" %08x", arg);
	}
	cprintf("\n");
f010078a:	83 ec 0c             	sub    $0xc,%esp
f010078d:	68 dd 62 10 f0       	push   $0xf01062dd
f0100792:	e8 fa 2b 00 00       	call   f0103391 <cprintf>

	//print more readable info
	//struct Eipdebuginfo* info_Ptr;
	debuginfo_eip((uintptr_t)(*return_Addr), info_Ptr); //get the info anyway
f0100797:	83 c4 08             	add    $0x8,%esp
f010079a:	8d 45 d0             	lea    -0x30(%ebp),%eax
f010079d:	50                   	push   %eax
f010079e:	ff 36                	pushl  (%esi)
f01007a0:	e8 36 39 00 00       	call   f01040db <debuginfo_eip>
	//print file name
	cprintf("%s", (info_Ptr->eip_file) );
f01007a5:	83 c4 08             	add    $0x8,%esp
f01007a8:	ff 75 d0             	pushl  -0x30(%ebp)
f01007ab:	68 ea 50 10 f0       	push   $0xf01050ea
f01007b0:	e8 dc 2b 00 00       	call   f0103391 <cprintf>
	cprintf(":");
f01007b5:	c7 04 24 32 54 10 f0 	movl   $0xf0105432,(%esp)
f01007bc:	e8 d0 2b 00 00       	call   f0103391 <cprintf>

	//print line number
	//cprintf("%d", (*info_Ptr).eip_line ); //eip_line
	cprintf("%d", info_Ptr->eip_line ); //eip_line
f01007c1:	83 c4 08             	add    $0x8,%esp
f01007c4:	ff 75 d4             	pushl  -0x2c(%ebp)
f01007c7:	68 5e 6d 10 f0       	push   $0xf0106d5e
f01007cc:	e8 c0 2b 00 00       	call   f0103391 <cprintf>
	cprintf(": ");
f01007d1:	c7 04 24 d5 50 10 f0 	movl   $0xf01050d5,(%esp)
f01007d8:	e8 b4 2b 00 00       	call   f0103391 <cprintf>
	//print function name
	for(int i=0; i<(info_Ptr->eip_fn_namelen); i++){
f01007dd:	83 c4 10             	add    $0x10,%esp
f01007e0:	bb 00 00 00 00       	mov    $0x0,%ebx
f01007e5:	eb 19                	jmp    f0100800 <print_stack_Info+0xbf>
		cprintf("%c", (info_Ptr->eip_fn_name)[i]);
f01007e7:	83 ec 08             	sub    $0x8,%esp
f01007ea:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01007ed:	0f be 04 18          	movsbl (%eax,%ebx,1),%eax
f01007f1:	50                   	push   %eax
f01007f2:	68 34 54 10 f0       	push   $0xf0105434
f01007f7:	e8 95 2b 00 00       	call   f0103391 <cprintf>
	//print line number
	//cprintf("%d", (*info_Ptr).eip_line ); //eip_line
	cprintf("%d", info_Ptr->eip_line ); //eip_line
	cprintf(": ");
	//print function name
	for(int i=0; i<(info_Ptr->eip_fn_namelen); i++){
f01007fc:	43                   	inc    %ebx
f01007fd:	83 c4 10             	add    $0x10,%esp
f0100800:	3b 5d dc             	cmp    -0x24(%ebp),%ebx
f0100803:	7c e2                	jl     f01007e7 <print_stack_Info+0xa6>
		cprintf("%c", (info_Ptr->eip_fn_name)[i]);
	}
	//print offset number
	cprintf("+");
f0100805:	83 ec 0c             	sub    $0xc,%esp
f0100808:	68 37 54 10 f0       	push   $0xf0105437
f010080d:	e8 7f 2b 00 00       	call   f0103391 <cprintf>
	cprintf("%d", ( (int)(*return_Addr) - (int)(info_Ptr->eip_fn_addr) ) );
f0100812:	83 c4 08             	add    $0x8,%esp
f0100815:	8b 06                	mov    (%esi),%eax
f0100817:	2b 45 e0             	sub    -0x20(%ebp),%eax
f010081a:	50                   	push   %eax
f010081b:	68 5e 6d 10 f0       	push   $0xf0106d5e
f0100820:	e8 6c 2b 00 00       	call   f0103391 <cprintf>
	cprintf("\n");
f0100825:	c7 04 24 dd 62 10 f0 	movl   $0xf01062dd,(%esp)
f010082c:	e8 60 2b 00 00       	call   f0103391 <cprintf>
}
f0100831:	83 c4 10             	add    $0x10,%esp
f0100834:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100837:	5b                   	pop    %ebx
f0100838:	5e                   	pop    %esi
f0100839:	5f                   	pop    %edi
f010083a:	5d                   	pop    %ebp
f010083b:	c3                   	ret    

f010083c <mon_backtrace>:


*/
int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
f010083c:	55                   	push   %ebp
f010083d:	89 e5                	mov    %esp,%ebp
f010083f:	56                   	push   %esi
f0100840:	53                   	push   %ebx

static inline uint32_t
read_ebp(void)
{
	uint32_t ebp;
	asm volatile("movl %%ebp,%0" : "=r" (ebp));
f0100841:	89 ee                	mov    %ebp,%esi
	//function prologue
	//f0100751:	55                   	push   %ebp
	//f0100752:	89 e5                	mov    %esp,%ebp
	// Your code here. -.-
	uint32_t esp = read_ebp(); //since mov %esp,%ebp
	uint32_t ebp = *( (uint32_t*)esp );
f0100843:	8b 1e                	mov    (%esi),%ebx
	uint32_t* return_Addr = (uint32_t*)(esp+4);
	//start print information
	cprintf("Stack backtrace:\n");
f0100845:	83 ec 0c             	sub    $0xc,%esp
f0100848:	68 39 54 10 f0       	push   $0xf0105439
f010084d:	e8 3f 2b 00 00       	call   f0103391 <cprintf>
	//cprintf("ebp %08x eip %08x\n", esp, return_Addr);  //the current ebp == esp
	print_stack_Info(esp, return_Addr);
f0100852:	83 c4 08             	add    $0x8,%esp
f0100855:	8d 46 04             	lea    0x4(%esi),%eax
f0100858:	50                   	push   %eax
f0100859:	56                   	push   %esi
f010085a:	e8 e2 fe ff ff       	call   f0100741 <print_stack_Info>
	//ebp = *((uint32_t*)ebp); //trace back to the last ebp

	//while(ebp != 0xf0111000){ //while not at the 1st stack address
	while(ebp != 0x0){
f010085f:	83 c4 10             	add    $0x10,%esp
f0100862:	eb 12                	jmp    f0100876 <mon_backtrace+0x3a>
		return_Addr = ((uint32_t*)ebp+1);
		//ebp = *((uint32_t*)ebp); //trace back to the last ebp
		//cprintf("ebp %08x eip %08x\n", ebp, return_Addr);  //the current ebp == esp
		print_stack_Info(ebp, return_Addr);
f0100864:	83 ec 08             	sub    $0x8,%esp
f0100867:	8d 43 04             	lea    0x4(%ebx),%eax
f010086a:	50                   	push   %eax
f010086b:	53                   	push   %ebx
f010086c:	e8 d0 fe ff ff       	call   f0100741 <print_stack_Info>
		ebp = *((uint32_t*)ebp); //trace back to the last ebp
f0100871:	8b 1b                	mov    (%ebx),%ebx
f0100873:	83 c4 10             	add    $0x10,%esp
	//cprintf("ebp %08x eip %08x\n", esp, return_Addr);  //the current ebp == esp
	print_stack_Info(esp, return_Addr);
	//ebp = *((uint32_t*)ebp); //trace back to the last ebp

	//while(ebp != 0xf0111000){ //while not at the 1st stack address
	while(ebp != 0x0){
f0100876:	85 db                	test   %ebx,%ebx
f0100878:	75 ea                	jne    f0100864 <mon_backtrace+0x28>
		print_stack_Info(ebp, return_Addr);
		ebp = *((uint32_t*)ebp); //trace back to the last ebp
	}

	//Not print the 1st stack address since it is the kernel allocated the beginning of the stack
	cprintf("Stoppp Stack backtrace:\n ");
f010087a:	83 ec 0c             	sub    $0xc,%esp
f010087d:	68 4b 54 10 f0       	push   $0xf010544b
f0100882:	e8 0a 2b 00 00       	call   f0103391 <cprintf>

	//cprintf("argc is %08x\n", argc);
	return 0;
}
f0100887:	b8 00 00 00 00       	mov    $0x0,%eax
f010088c:	8d 65 f8             	lea    -0x8(%ebp),%esp
f010088f:	5b                   	pop    %ebx
f0100890:	5e                   	pop    %esi
f0100891:	5d                   	pop    %ebp
f0100892:	c3                   	ret    

f0100893 <monitor>:
	return 0;
}

void
monitor(struct Trapframe *tf)
{
f0100893:	55                   	push   %ebp
f0100894:	89 e5                	mov    %esp,%ebp
f0100896:	57                   	push   %edi
f0100897:	56                   	push   %esi
f0100898:	53                   	push   %ebx
f0100899:	83 ec 58             	sub    $0x58,%esp
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
f010089c:	68 b0 55 10 f0       	push   $0xf01055b0
f01008a1:	e8 eb 2a 00 00       	call   f0103391 <cprintf>
	cprintf("Type 'help' for a list of commands.\n");
f01008a6:	c7 04 24 d4 55 10 f0 	movl   $0xf01055d4,(%esp)
f01008ad:	e8 df 2a 00 00       	call   f0103391 <cprintf>

	if (tf != NULL)
f01008b2:	83 c4 10             	add    $0x10,%esp
f01008b5:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
f01008b9:	74 0e                	je     f01008c9 <monitor+0x36>
		print_trapframe(tf);
f01008bb:	83 ec 0c             	sub    $0xc,%esp
f01008be:	ff 75 08             	pushl  0x8(%ebp)
f01008c1:	e8 b5 31 00 00       	call   f0103a7b <print_trapframe>
f01008c6:	83 c4 10             	add    $0x10,%esp

	while (1) {
		buf = readline("K> ");
f01008c9:	83 ec 0c             	sub    $0xc,%esp
f01008cc:	68 65 54 10 f0       	push   $0xf0105465
f01008d1:	e8 20 41 00 00       	call   f01049f6 <readline>
f01008d6:	89 c3                	mov    %eax,%ebx
		if (buf != NULL)
f01008d8:	83 c4 10             	add    $0x10,%esp
f01008db:	85 c0                	test   %eax,%eax
f01008dd:	74 ea                	je     f01008c9 <monitor+0x36>
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
f01008df:	c7 45 a8 00 00 00 00 	movl   $0x0,-0x58(%ebp)
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
f01008e6:	be 00 00 00 00       	mov    $0x0,%esi
f01008eb:	eb 0a                	jmp    f01008f7 <monitor+0x64>
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
f01008ed:	c6 03 00             	movb   $0x0,(%ebx)
f01008f0:	89 f7                	mov    %esi,%edi
f01008f2:	8d 5b 01             	lea    0x1(%ebx),%ebx
f01008f5:	89 fe                	mov    %edi,%esi
	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
f01008f7:	8a 03                	mov    (%ebx),%al
f01008f9:	84 c0                	test   %al,%al
f01008fb:	74 60                	je     f010095d <monitor+0xca>
f01008fd:	83 ec 08             	sub    $0x8,%esp
f0100900:	0f be c0             	movsbl %al,%eax
f0100903:	50                   	push   %eax
f0100904:	68 69 54 10 f0       	push   $0xf0105469
f0100909:	e8 00 43 00 00       	call   f0104c0e <strchr>
f010090e:	83 c4 10             	add    $0x10,%esp
f0100911:	85 c0                	test   %eax,%eax
f0100913:	75 d8                	jne    f01008ed <monitor+0x5a>
			*buf++ = 0;
		if (*buf == 0)
f0100915:	80 3b 00             	cmpb   $0x0,(%ebx)
f0100918:	74 43                	je     f010095d <monitor+0xca>
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
f010091a:	83 fe 0f             	cmp    $0xf,%esi
f010091d:	75 14                	jne    f0100933 <monitor+0xa0>
			cprintf("Too many arguments (max %d)\n", MAXARGS);
f010091f:	83 ec 08             	sub    $0x8,%esp
f0100922:	6a 10                	push   $0x10
f0100924:	68 6e 54 10 f0       	push   $0xf010546e
f0100929:	e8 63 2a 00 00       	call   f0103391 <cprintf>
f010092e:	83 c4 10             	add    $0x10,%esp
f0100931:	eb 96                	jmp    f01008c9 <monitor+0x36>
			return 0;
		}
		argv[argc++] = buf;
f0100933:	8d 7e 01             	lea    0x1(%esi),%edi
f0100936:	89 5c b5 a8          	mov    %ebx,-0x58(%ebp,%esi,4)
f010093a:	eb 01                	jmp    f010093d <monitor+0xaa>
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
f010093c:	43                   	inc    %ebx
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
f010093d:	8a 03                	mov    (%ebx),%al
f010093f:	84 c0                	test   %al,%al
f0100941:	74 b2                	je     f01008f5 <monitor+0x62>
f0100943:	83 ec 08             	sub    $0x8,%esp
f0100946:	0f be c0             	movsbl %al,%eax
f0100949:	50                   	push   %eax
f010094a:	68 69 54 10 f0       	push   $0xf0105469
f010094f:	e8 ba 42 00 00       	call   f0104c0e <strchr>
f0100954:	83 c4 10             	add    $0x10,%esp
f0100957:	85 c0                	test   %eax,%eax
f0100959:	74 e1                	je     f010093c <monitor+0xa9>
f010095b:	eb 98                	jmp    f01008f5 <monitor+0x62>
			buf++;
	}
	argv[argc] = 0;
f010095d:	c7 44 b5 a8 00 00 00 	movl   $0x0,-0x58(%ebp,%esi,4)
f0100964:	00 

	// Lookup and invoke the command
	if (argc == 0)
f0100965:	85 f6                	test   %esi,%esi
f0100967:	0f 84 5c ff ff ff    	je     f01008c9 <monitor+0x36>
f010096d:	bf 00 56 10 f0       	mov    $0xf0105600,%edi
f0100972:	bb 00 00 00 00       	mov    $0x0,%ebx
		return 0;
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
f0100977:	83 ec 08             	sub    $0x8,%esp
f010097a:	ff 37                	pushl  (%edi)
f010097c:	ff 75 a8             	pushl  -0x58(%ebp)
f010097f:	e8 36 42 00 00       	call   f0104bba <strcmp>
f0100984:	83 c4 10             	add    $0x10,%esp
f0100987:	85 c0                	test   %eax,%eax
f0100989:	75 23                	jne    f01009ae <monitor+0x11b>
			return commands[i].func(argc, argv, tf);
f010098b:	83 ec 04             	sub    $0x4,%esp
f010098e:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
f0100991:	01 c3                	add    %eax,%ebx
f0100993:	ff 75 08             	pushl  0x8(%ebp)
f0100996:	8d 45 a8             	lea    -0x58(%ebp),%eax
f0100999:	50                   	push   %eax
f010099a:	56                   	push   %esi
f010099b:	ff 14 9d 08 56 10 f0 	call   *-0xfefa9f8(,%ebx,4)
		print_trapframe(tf);

	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
f01009a2:	83 c4 10             	add    $0x10,%esp
f01009a5:	85 c0                	test   %eax,%eax
f01009a7:	78 26                	js     f01009cf <monitor+0x13c>
f01009a9:	e9 1b ff ff ff       	jmp    f01008c9 <monitor+0x36>
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
f01009ae:	43                   	inc    %ebx
f01009af:	83 c7 0c             	add    $0xc,%edi
f01009b2:	83 fb 03             	cmp    $0x3,%ebx
f01009b5:	75 c0                	jne    f0100977 <monitor+0xe4>
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
f01009b7:	83 ec 08             	sub    $0x8,%esp
f01009ba:	ff 75 a8             	pushl  -0x58(%ebp)
f01009bd:	68 8b 54 10 f0       	push   $0xf010548b
f01009c2:	e8 ca 29 00 00       	call   f0103391 <cprintf>
f01009c7:	83 c4 10             	add    $0x10,%esp
f01009ca:	e9 fa fe ff ff       	jmp    f01008c9 <monitor+0x36>
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
f01009cf:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01009d2:	5b                   	pop    %ebx
f01009d3:	5e                   	pop    %esi
f01009d4:	5f                   	pop    %edi
f01009d5:	5d                   	pop    %ebp
f01009d6:	c3                   	ret    

f01009d7 <e820_init>:

// This function may ONLY be used during initialization,
// before page_init().
void
e820_init(physaddr_t mbi_pa)
{
f01009d7:	55                   	push   %ebp
f01009d8:	89 e5                	mov    %esp,%ebp
f01009da:	57                   	push   %edi
f01009db:	56                   	push   %esi
f01009dc:	53                   	push   %ebx
f01009dd:	83 ec 1c             	sub    $0x1c,%esp
f01009e0:	8b 75 08             	mov    0x8(%ebp),%esi
	struct multiboot_info *mbi;
	uint32_t addr, addr_end, i;

	mbi = (struct multiboot_info *)mbi_pa;
	assert(mbi->flags & MULTIBOOT_INFO_MEM_MAP);
f01009e3:	f6 06 40             	testb  $0x40,(%esi)
f01009e6:	75 16                	jne    f01009fe <e820_init+0x27>
f01009e8:	68 24 56 10 f0       	push   $0xf0105624
f01009ed:	68 d8 50 10 f0       	push   $0xf01050d8
f01009f2:	6a 26                	push   $0x26
f01009f4:	68 77 56 10 f0       	push   $0xf0105677
f01009f9:	e8 4e f6 ff ff       	call   f010004c <_panic>
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);
f01009fe:	8b 56 30             	mov    0x30(%esi),%edx
	struct multiboot_info *mbi;
	uint32_t addr, addr_end, i;

	mbi = (struct multiboot_info *)mbi_pa;
	assert(mbi->flags & MULTIBOOT_INFO_MEM_MAP);
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
f0100a01:	83 ec 04             	sub    $0x4,%esp
f0100a04:	89 d0                	mov    %edx,%eax
f0100a06:	03 46 2c             	add    0x2c(%esi),%eax
f0100a09:	48                   	dec    %eax
f0100a0a:	50                   	push   %eax
f0100a0b:	52                   	push   %edx
f0100a0c:	68 48 56 10 f0       	push   $0xf0105648
f0100a11:	e8 7b 29 00 00       	call   f0103391 <cprintf>
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);

	addr = mbi->mmap_addr;
f0100a16:	8b 5e 30             	mov    0x30(%esi),%ebx
	addr_end = mbi->mmap_addr + mbi->mmap_length;
f0100a19:	89 d8                	mov    %ebx,%eax
f0100a1b:	03 46 2c             	add    0x2c(%esi),%eax
f0100a1e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0100a21:	c7 45 dc 44 8f 18 f0 	movl   $0xf0188f44,-0x24(%ebp)
	for (i = 0; addr < addr_end; ++i) {
f0100a28:	83 c4 10             	add    $0x10,%esp
f0100a2b:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
f0100a32:	e9 b5 00 00 00       	jmp    f0100aec <e820_init+0x115>
		struct multiboot_mmap_entry *e;

		// Print memory mapping.
		assert(addr_end - addr >= sizeof(*e));
f0100a37:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100a3a:	29 d8                	sub    %ebx,%eax
f0100a3c:	83 f8 17             	cmp    $0x17,%eax
f0100a3f:	77 16                	ja     f0100a57 <e820_init+0x80>
f0100a41:	68 83 56 10 f0       	push   $0xf0105683
f0100a46:	68 d8 50 10 f0       	push   $0xf01050d8
f0100a4b:	6a 30                	push   $0x30
f0100a4d:	68 77 56 10 f0       	push   $0xf0105677
f0100a52:	e8 f5 f5 ff ff       	call   f010004c <_panic>
		e = (struct multiboot_mmap_entry *)addr;
f0100a57:	89 de                	mov    %ebx,%esi
		cprintf("  [mem %08p-%08p] ",
f0100a59:	8b 53 04             	mov    0x4(%ebx),%edx
f0100a5c:	83 ec 04             	sub    $0x4,%esp
f0100a5f:	89 d0                	mov    %edx,%eax
f0100a61:	03 43 0c             	add    0xc(%ebx),%eax
f0100a64:	48                   	dec    %eax
f0100a65:	50                   	push   %eax
f0100a66:	52                   	push   %edx
f0100a67:	68 a1 56 10 f0       	push   $0xf01056a1
f0100a6c:	e8 20 29 00 00       	call   f0103391 <cprintf>
			(uintptr_t)e->e820.addr,
			(uintptr_t)(e->e820.addr + e->e820.len - 1));
		print_e820_map_type(e->e820.type);
f0100a71:	8b 43 14             	mov    0x14(%ebx),%eax
	"unusable",
};

static void
print_e820_map_type(uint32_t type) {
	switch (type) {
f0100a74:	83 c4 10             	add    $0x10,%esp
f0100a77:	8d 50 ff             	lea    -0x1(%eax),%edx
f0100a7a:	83 fa 04             	cmp    $0x4,%edx
f0100a7d:	77 14                	ja     f0100a93 <e820_init+0xbc>
	case 1 ... 5:
		cprintf(e820_map_types[type - 1]);
f0100a7f:	83 ec 0c             	sub    $0xc,%esp
f0100a82:	ff 34 85 00 57 10 f0 	pushl  -0xfefa900(,%eax,4)
f0100a89:	e8 03 29 00 00       	call   f0103391 <cprintf>
f0100a8e:	83 c4 10             	add    $0x10,%esp
f0100a91:	eb 11                	jmp    f0100aa4 <e820_init+0xcd>
		break;
	default:
		cprintf("type %u", type);
f0100a93:	83 ec 08             	sub    $0x8,%esp
f0100a96:	50                   	push   %eax
f0100a97:	68 b4 56 10 f0       	push   $0xf01056b4
f0100a9c:	e8 f0 28 00 00       	call   f0103391 <cprintf>
f0100aa1:	83 c4 10             	add    $0x10,%esp
		e = (struct multiboot_mmap_entry *)addr;
		cprintf("  [mem %08p-%08p] ",
			(uintptr_t)e->e820.addr,
			(uintptr_t)(e->e820.addr + e->e820.len - 1));
		print_e820_map_type(e->e820.type);
		cprintf("\n");
f0100aa4:	83 ec 0c             	sub    $0xc,%esp
f0100aa7:	68 dd 62 10 f0       	push   $0xf01062dd
f0100aac:	e8 e0 28 00 00       	call   f0103391 <cprintf>

		// Save a copy.
		assert(i < E820_NR_MAX);
f0100ab1:	83 c4 10             	add    $0x10,%esp
f0100ab4:	83 7d e0 40          	cmpl   $0x40,-0x20(%ebp)
f0100ab8:	75 16                	jne    f0100ad0 <e820_init+0xf9>
f0100aba:	68 bc 56 10 f0       	push   $0xf01056bc
f0100abf:	68 d8 50 10 f0       	push   $0xf01050d8
f0100ac4:	6a 39                	push   $0x39
f0100ac6:	68 77 56 10 f0       	push   $0xf0105677
f0100acb:	e8 7c f5 ff ff       	call   f010004c <_panic>
		e820_map.entries[i] = e->e820;
f0100ad0:	89 f0                	mov    %esi,%eax
f0100ad2:	8d 76 04             	lea    0x4(%esi),%esi
f0100ad5:	b9 05 00 00 00       	mov    $0x5,%ecx
f0100ada:	8b 7d dc             	mov    -0x24(%ebp),%edi
f0100add:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
		addr += (e->size + 4);
f0100adf:	8b 00                	mov    (%eax),%eax
f0100ae1:	8d 5c 03 04          	lea    0x4(%ebx,%eax,1),%ebx
	cprintf("E820: physical memory map [mem 0x%08x-0x%08x]\n",
		mbi->mmap_addr, mbi->mmap_addr + mbi->mmap_length - 1);

	addr = mbi->mmap_addr;
	addr_end = mbi->mmap_addr + mbi->mmap_length;
	for (i = 0; addr < addr_end; ++i) {
f0100ae5:	ff 45 e0             	incl   -0x20(%ebp)
f0100ae8:	83 45 dc 14          	addl   $0x14,-0x24(%ebp)
f0100aec:	3b 5d e4             	cmp    -0x1c(%ebp),%ebx
f0100aef:	0f 82 42 ff ff ff    	jb     f0100a37 <e820_init+0x60>
		// Save a copy.
		assert(i < E820_NR_MAX);
		e820_map.entries[i] = e->e820;
		addr += (e->size + 4);
	}
	e820_map.nr = i;
f0100af5:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100af8:	a3 40 8f 18 f0       	mov    %eax,0xf0188f40
}
f0100afd:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100b00:	5b                   	pop    %ebx
f0100b01:	5e                   	pop    %esi
f0100b02:	5f                   	pop    %edi
f0100b03:	5d                   	pop    %ebp
f0100b04:	c3                   	ret    

f0100b05 <boot_alloc>:
// If we're out of memory, boot_alloc should panic.
// This function may ONLY be used during initialization,
// before the page_free_list list has been set up.
static void *
boot_alloc(uint32_t n)
{
f0100b05:	89 c1                	mov    %eax,%ecx
	// Initialize nextfree if this is the first time.
	// 'end' is a magic symbol automatically generated by the linker,
	// which points to the end of the kernel's bss segment:
	// the first virtual address that the linker did *not* assign
	// to any kernel code or global variables.
	if (!nextfree) {
f0100b07:	83 3d 58 82 18 f0 00 	cmpl   $0x0,0xf0188258
f0100b0e:	75 0f                	jne    f0100b1f <boot_alloc+0x1a>
		extern char end[]; //points to the end of the kernel
		nextfree = ROUNDUP((char *) end, PGSIZE); //set the nextfree pointer
f0100b10:	b8 4f a4 18 f0       	mov    $0xf018a44f,%eax
f0100b15:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0100b1a:	a3 58 82 18 f0       	mov    %eax,0xf0188258
	//
	// LAB 2: Your code here.
	//check if there's current enough space to allocate
	//(npages * PGSIZE) is the total memory space
	//if( ((npages * PGSIZE) - nextfree ) <  ROUNDUP((char *) n, PGSIZE) ){
	if( ( (char*)(npages * PGSIZE) - nextfree ) <  n ){
f0100b1f:	a1 58 82 18 f0       	mov    0xf0188258,%eax
f0100b24:	8b 15 44 94 18 f0    	mov    0xf0189444,%edx
f0100b2a:	c1 e2 0c             	shl    $0xc,%edx
f0100b2d:	29 c2                	sub    %eax,%edx
f0100b2f:	39 ca                	cmp    %ecx,%edx
f0100b31:	73 17                	jae    f0100b4a <boot_alloc+0x45>
// If we're out of memory, boot_alloc should panic.
// This function may ONLY be used during initialization,
// before the page_free_list list has been set up.
static void *
boot_alloc(uint32_t n)
{
f0100b33:	55                   	push   %ebp
f0100b34:	89 e5                	mov    %esp,%ebp
f0100b36:	83 ec 0c             	sub    $0xc,%esp
	// LAB 2: Your code here.
	//check if there's current enough space to allocate
	//(npages * PGSIZE) is the total memory space
	//if( ((npages * PGSIZE) - nextfree ) <  ROUNDUP((char *) n, PGSIZE) ){
	if( ( (char*)(npages * PGSIZE) - nextfree ) <  n ){
		panic("boot_alloc: not enough physical address to allocate\n");
f0100b39:	68 18 57 10 f0       	push   $0xf0105718
f0100b3e:	6a 62                	push   $0x62
f0100b40:	68 41 5f 10 f0       	push   $0xf0105f41
f0100b45:	e8 02 f5 ff ff       	call   f010004c <_panic>
	}
	//nextfree = ROUNDUP( ((char *)n + nextfree), PGSIZE); //update the nextfree pointer
	result = nextfree;
	nextfree = n + nextfree; //update the nextfree pointer
f0100b4a:	01 c1                	add    %eax,%ecx
f0100b4c:	89 0d 58 82 18 f0    	mov    %ecx,0xf0188258
	return result;
	//return NULL;
}
f0100b52:	c3                   	ret    

f0100b53 <check_va2pa>:
check_va2pa(pde_t *pgdir, uintptr_t va)
{
	pte_t *p;
	//cprintf("VA check_va2pa looks at is: %x\n", va);
	pgdir = &pgdir[PDX(va)];
	if (!(*pgdir & PTE_P)){ //no page directory entry there
f0100b53:	89 d1                	mov    %edx,%ecx
f0100b55:	c1 e9 16             	shr    $0x16,%ecx
f0100b58:	8b 04 88             	mov    (%eax,%ecx,4),%eax
f0100b5b:	a8 01                	test   $0x1,%al
f0100b5d:	74 47                	je     f0100ba6 <check_va2pa+0x53>
		//cprintf("check_va2pa no pde, return fff...\n");
		return ~0;
	}

	p = (pte_t*) KADDR(PTE_ADDR(*pgdir)); //convert PA to VA so that we can deref later to get pte
f0100b5f:	25 00 f0 ff ff       	and    $0xfffff000,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100b64:	89 c1                	mov    %eax,%ecx
f0100b66:	c1 e9 0c             	shr    $0xc,%ecx
f0100b69:	3b 0d 44 94 18 f0    	cmp    0xf0189444,%ecx
f0100b6f:	72 1b                	jb     f0100b8c <check_va2pa+0x39>
// this functionality for us!  We define our own version to help check
// the check_kern_pgdir() function; it shouldn't be used elsewhere.

static physaddr_t
check_va2pa(pde_t *pgdir, uintptr_t va)
{
f0100b71:	55                   	push   %ebp
f0100b72:	89 e5                	mov    %esp,%ebp
f0100b74:	83 ec 08             	sub    $0x8,%esp
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0100b77:	50                   	push   %eax
f0100b78:	68 50 57 10 f0       	push   $0xf0105750
f0100b7d:	68 16 04 00 00       	push   $0x416
f0100b82:	68 41 5f 10 f0       	push   $0xf0105f41
f0100b87:	e8 c0 f4 ff ff       	call   f010004c <_panic>
		return ~0;
	}

	p = (pte_t*) KADDR(PTE_ADDR(*pgdir)); //convert PA to VA so that we can deref later to get pte
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	if (!(p[PTX(va)] & PTE_P)){ //no page table entry there
f0100b8c:	c1 ea 0c             	shr    $0xc,%edx
f0100b8f:	81 e2 ff 03 00 00    	and    $0x3ff,%edx
f0100b95:	8b 84 90 00 00 00 f0 	mov    -0x10000000(%eax,%edx,4),%eax
f0100b9c:	a8 01                	test   $0x1,%al
f0100b9e:	74 0c                	je     f0100bac <check_va2pa+0x59>
	}

	//cprintf("in check_va2pa, the pte* is: %x\n", &(p[PTX(va)]) );
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	//cprintf("check_va2pa returns: %x\n", PTE_ADDR(p[PTX(va)]));
	return PTE_ADDR(p[PTX(va)]); //return physical address of the page containing 'va'
f0100ba0:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0100ba5:	c3                   	ret    
	pte_t *p;
	//cprintf("VA check_va2pa looks at is: %x\n", va);
	pgdir = &pgdir[PDX(va)];
	if (!(*pgdir & PTE_P)){ //no page directory entry there
		//cprintf("check_va2pa no pde, return fff...\n");
		return ~0;
f0100ba6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100bab:	c3                   	ret    
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	if (!(p[PTX(va)] & PTE_P)){ //no page table entry there
		//cprintf("in check_va2pa, the pte* is: %x\n", &(p[PTX(va)]) );
		//cprintf("check_va2pa no pte, return fff...\n");
		//cprintf("shit happens\n");
		return ~0;
f0100bac:	b8 ff ff ff ff       	mov    $0xffffffff,%eax

	//cprintf("in check_va2pa, the pte* is: %x\n", &(p[PTX(va)]) );
	//cprintf("pte_t* check_va2pa looks at is: %x\n", p);
	//cprintf("check_va2pa returns: %x\n", PTE_ADDR(p[PTX(va)]));
	return PTE_ADDR(p[PTX(va)]); //return physical address of the page containing 'va'
}
f0100bb1:	c3                   	ret    

f0100bb2 <check_page_free_list>:
//
// Check that the pages on the page_free_list are reasonable.
//
static void
check_page_free_list(bool only_low_memory)
{
f0100bb2:	55                   	push   %ebp
f0100bb3:	89 e5                	mov    %esp,%ebp
f0100bb5:	57                   	push   %edi
f0100bb6:	56                   	push   %esi
f0100bb7:	53                   	push   %ebx
f0100bb8:	83 ec 2c             	sub    $0x2c,%esp
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
f0100bbb:	84 c0                	test   %al,%al
f0100bbd:	0f 85 6d 02 00 00    	jne    f0100e30 <check_page_free_list+0x27e>
f0100bc3:	e9 7a 02 00 00       	jmp    f0100e42 <check_page_free_list+0x290>
	int nfree_basemem = 0, nfree_extmem = 0;
	char *first_free_page;

	if (!page_free_list)
		panic("'page_free_list' is a null pointer!");
f0100bc8:	83 ec 04             	sub    $0x4,%esp
f0100bcb:	68 74 57 10 f0       	push   $0xf0105774
f0100bd0:	68 4d 03 00 00       	push   $0x34d
f0100bd5:	68 41 5f 10 f0       	push   $0xf0105f41
f0100bda:	e8 6d f4 ff ff       	call   f010004c <_panic>

	if (only_low_memory) {
		// Move pages with lower addresses first in the free
		// list, since entry_pgdir does not map all pages.
		struct PageInfo *pp1, *pp2;
		struct PageInfo **tp[2] = { &pp1, &pp2 };
f0100bdf:	8d 55 d8             	lea    -0x28(%ebp),%edx
f0100be2:	89 55 e0             	mov    %edx,-0x20(%ebp)
f0100be5:	8d 55 dc             	lea    -0x24(%ebp),%edx
f0100be8:	89 55 e4             	mov    %edx,-0x1c(%ebp)
		for (pp = page_free_list; pp; pp = pp->pp_link) {
			int pagetype = PDX(page2pa(pp)) >= pdx_limit;
f0100beb:	89 c2                	mov    %eax,%edx
f0100bed:	2b 15 4c 94 18 f0    	sub    0xf018944c,%edx
f0100bf3:	f7 c2 00 e0 7f 00    	test   $0x7fe000,%edx
f0100bf9:	0f 95 c2             	setne  %dl
f0100bfc:	0f b6 d2             	movzbl %dl,%edx
			*tp[pagetype] = pp;
f0100bff:	8b 4c 95 e0          	mov    -0x20(%ebp,%edx,4),%ecx
f0100c03:	89 01                	mov    %eax,(%ecx)
			tp[pagetype] = &pp->pp_link;
f0100c05:	89 44 95 e0          	mov    %eax,-0x20(%ebp,%edx,4)
	if (only_low_memory) {
		// Move pages with lower addresses first in the free
		// list, since entry_pgdir does not map all pages.
		struct PageInfo *pp1, *pp2;
		struct PageInfo **tp[2] = { &pp1, &pp2 };
		for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100c09:	8b 00                	mov    (%eax),%eax
f0100c0b:	85 c0                	test   %eax,%eax
f0100c0d:	75 dc                	jne    f0100beb <check_page_free_list+0x39>
			int pagetype = PDX(page2pa(pp)) >= pdx_limit;
			*tp[pagetype] = pp;
			tp[pagetype] = &pp->pp_link;
		}
		*tp[1] = 0;
f0100c0f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100c12:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		*tp[0] = pp2;
f0100c18:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100c1b:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0100c1e:	89 10                	mov    %edx,(%eax)
		page_free_list = pp1;
f0100c20:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0100c23:	a3 60 82 18 f0       	mov    %eax,0xf0188260
//
static void
check_page_free_list(bool only_low_memory)
{
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
f0100c28:	be 01 00 00 00       	mov    $0x1,%esi
		page_free_list = pp1;
	}

	// if there's a page that shouldn't be on the free list,
	// try to make sure it eventually causes trouble.
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0100c2d:	8b 1d 60 82 18 f0    	mov    0xf0188260,%ebx
f0100c33:	eb 53                	jmp    f0100c88 <check_page_free_list+0xd6>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0100c35:	89 d8                	mov    %ebx,%eax
f0100c37:	2b 05 4c 94 18 f0    	sub    0xf018944c,%eax
f0100c3d:	c1 f8 03             	sar    $0x3,%eax
f0100c40:	c1 e0 0c             	shl    $0xc,%eax
		if (PDX(page2pa(pp)) < pdx_limit)
f0100c43:	89 c2                	mov    %eax,%edx
f0100c45:	c1 ea 16             	shr    $0x16,%edx
f0100c48:	39 f2                	cmp    %esi,%edx
f0100c4a:	73 3a                	jae    f0100c86 <check_page_free_list+0xd4>
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100c4c:	89 c2                	mov    %eax,%edx
f0100c4e:	c1 ea 0c             	shr    $0xc,%edx
f0100c51:	3b 15 44 94 18 f0    	cmp    0xf0189444,%edx
f0100c57:	72 12                	jb     f0100c6b <check_page_free_list+0xb9>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0100c59:	50                   	push   %eax
f0100c5a:	68 50 57 10 f0       	push   $0xf0105750
f0100c5f:	6a 56                	push   $0x56
f0100c61:	68 4d 5f 10 f0       	push   $0xf0105f4d
f0100c66:	e8 e1 f3 ff ff       	call   f010004c <_panic>
			memset(page2kva(pp), 0x97, 128);
f0100c6b:	83 ec 04             	sub    $0x4,%esp
f0100c6e:	68 80 00 00 00       	push   $0x80
f0100c73:	68 97 00 00 00       	push   $0x97
f0100c78:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0100c7d:	50                   	push   %eax
f0100c7e:	e8 c0 3f 00 00       	call   f0104c43 <memset>
f0100c83:	83 c4 10             	add    $0x10,%esp
		page_free_list = pp1;
	}

	// if there's a page that shouldn't be on the free list,
	// try to make sure it eventually causes trouble.
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0100c86:	8b 1b                	mov    (%ebx),%ebx
f0100c88:	85 db                	test   %ebx,%ebx
f0100c8a:	75 a9                	jne    f0100c35 <check_page_free_list+0x83>
		if (PDX(page2pa(pp)) < pdx_limit)
			memset(page2kva(pp), 0x97, 128);

	first_free_page = (char *) boot_alloc(0);
f0100c8c:	b8 00 00 00 00       	mov    $0x0,%eax
f0100c91:	e8 6f fe ff ff       	call   f0100b05 <boot_alloc>
f0100c96:	89 45 cc             	mov    %eax,-0x34(%ebp)
	for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100c99:	8b 15 60 82 18 f0    	mov    0xf0188260,%edx
		// check that we didn't corrupt the free list itself
		assert(pp >= pages);
f0100c9f:	8b 0d 4c 94 18 f0    	mov    0xf018944c,%ecx
		assert(pp < pages + npages);
f0100ca5:	a1 44 94 18 f0       	mov    0xf0189444,%eax
f0100caa:	89 45 c8             	mov    %eax,-0x38(%ebp)
f0100cad:	8d 3c c1             	lea    (%ecx,%eax,8),%edi
		assert(((char *) pp - (char *) pages) % sizeof(*pp) == 0);
f0100cb0:	89 4d d4             	mov    %ecx,-0x2c(%ebp)
static void
check_page_free_list(bool only_low_memory)
{
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
	int nfree_basemem = 0, nfree_extmem = 0;
f0100cb3:	be 00 00 00 00       	mov    $0x0,%esi
f0100cb8:	89 5d d0             	mov    %ebx,-0x30(%ebp)
	for (pp = page_free_list; pp; pp = pp->pp_link)
		if (PDX(page2pa(pp)) < pdx_limit)
			memset(page2kva(pp), 0x97, 128);

	first_free_page = (char *) boot_alloc(0);
	for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100cbb:	e9 2b 01 00 00       	jmp    f0100deb <check_page_free_list+0x239>
		// check that we didn't corrupt the free list itself
		assert(pp >= pages);
f0100cc0:	39 ca                	cmp    %ecx,%edx
f0100cc2:	73 19                	jae    f0100cdd <check_page_free_list+0x12b>
f0100cc4:	68 5b 5f 10 f0       	push   $0xf0105f5b
f0100cc9:	68 d8 50 10 f0       	push   $0xf01050d8
f0100cce:	68 67 03 00 00       	push   $0x367
f0100cd3:	68 41 5f 10 f0       	push   $0xf0105f41
f0100cd8:	e8 6f f3 ff ff       	call   f010004c <_panic>
		assert(pp < pages + npages);
f0100cdd:	39 fa                	cmp    %edi,%edx
f0100cdf:	72 19                	jb     f0100cfa <check_page_free_list+0x148>
f0100ce1:	68 67 5f 10 f0       	push   $0xf0105f67
f0100ce6:	68 d8 50 10 f0       	push   $0xf01050d8
f0100ceb:	68 68 03 00 00       	push   $0x368
f0100cf0:	68 41 5f 10 f0       	push   $0xf0105f41
f0100cf5:	e8 52 f3 ff ff       	call   f010004c <_panic>
		assert(((char *) pp - (char *) pages) % sizeof(*pp) == 0);
f0100cfa:	89 d0                	mov    %edx,%eax
f0100cfc:	2b 45 d4             	sub    -0x2c(%ebp),%eax
f0100cff:	a8 07                	test   $0x7,%al
f0100d01:	74 19                	je     f0100d1c <check_page_free_list+0x16a>
f0100d03:	68 98 57 10 f0       	push   $0xf0105798
f0100d08:	68 d8 50 10 f0       	push   $0xf01050d8
f0100d0d:	68 69 03 00 00       	push   $0x369
f0100d12:	68 41 5f 10 f0       	push   $0xf0105f41
f0100d17:	e8 30 f3 ff ff       	call   f010004c <_panic>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0100d1c:	c1 f8 03             	sar    $0x3,%eax

		// check a few pages that shouldn't be on the free list
		assert(page2pa(pp) != 0);
f0100d1f:	c1 e0 0c             	shl    $0xc,%eax
f0100d22:	75 19                	jne    f0100d3d <check_page_free_list+0x18b>
f0100d24:	68 7b 5f 10 f0       	push   $0xf0105f7b
f0100d29:	68 d8 50 10 f0       	push   $0xf01050d8
f0100d2e:	68 6c 03 00 00       	push   $0x36c
f0100d33:	68 41 5f 10 f0       	push   $0xf0105f41
f0100d38:	e8 0f f3 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) != IOPHYSMEM);
f0100d3d:	3d 00 00 0a 00       	cmp    $0xa0000,%eax
f0100d42:	75 19                	jne    f0100d5d <check_page_free_list+0x1ab>
f0100d44:	68 8c 5f 10 f0       	push   $0xf0105f8c
f0100d49:	68 d8 50 10 f0       	push   $0xf01050d8
f0100d4e:	68 6d 03 00 00       	push   $0x36d
f0100d53:	68 41 5f 10 f0       	push   $0xf0105f41
f0100d58:	e8 ef f2 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) != EXTPHYSMEM - PGSIZE);
f0100d5d:	3d 00 f0 0f 00       	cmp    $0xff000,%eax
f0100d62:	75 19                	jne    f0100d7d <check_page_free_list+0x1cb>
f0100d64:	68 cc 57 10 f0       	push   $0xf01057cc
f0100d69:	68 d8 50 10 f0       	push   $0xf01050d8
f0100d6e:	68 6e 03 00 00       	push   $0x36e
f0100d73:	68 41 5f 10 f0       	push   $0xf0105f41
f0100d78:	e8 cf f2 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) != EXTPHYSMEM);
f0100d7d:	3d 00 00 10 00       	cmp    $0x100000,%eax
f0100d82:	75 19                	jne    f0100d9d <check_page_free_list+0x1eb>
f0100d84:	68 a5 5f 10 f0       	push   $0xf0105fa5
f0100d89:	68 d8 50 10 f0       	push   $0xf01050d8
f0100d8e:	68 6f 03 00 00       	push   $0x36f
f0100d93:	68 41 5f 10 f0       	push   $0xf0105f41
f0100d98:	e8 af f2 ff ff       	call   f010004c <_panic>
		assert(page2pa(pp) < EXTPHYSMEM || (char *) page2kva(pp) >= first_free_page);
f0100d9d:	3d ff ff 0f 00       	cmp    $0xfffff,%eax
f0100da2:	76 3f                	jbe    f0100de3 <check_page_free_list+0x231>
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100da4:	89 c3                	mov    %eax,%ebx
f0100da6:	c1 eb 0c             	shr    $0xc,%ebx
f0100da9:	39 5d c8             	cmp    %ebx,-0x38(%ebp)
f0100dac:	77 12                	ja     f0100dc0 <check_page_free_list+0x20e>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0100dae:	50                   	push   %eax
f0100daf:	68 50 57 10 f0       	push   $0xf0105750
f0100db4:	6a 56                	push   $0x56
f0100db6:	68 4d 5f 10 f0       	push   $0xf0105f4d
f0100dbb:	e8 8c f2 ff ff       	call   f010004c <_panic>
f0100dc0:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0100dc5:	39 45 cc             	cmp    %eax,-0x34(%ebp)
f0100dc8:	76 1c                	jbe    f0100de6 <check_page_free_list+0x234>
f0100dca:	68 f0 57 10 f0       	push   $0xf01057f0
f0100dcf:	68 d8 50 10 f0       	push   $0xf01050d8
f0100dd4:	68 70 03 00 00       	push   $0x370
f0100dd9:	68 41 5f 10 f0       	push   $0xf0105f41
f0100dde:	e8 69 f2 ff ff       	call   f010004c <_panic>

		if (page2pa(pp) < EXTPHYSMEM)
			++nfree_basemem;
f0100de3:	46                   	inc    %esi
f0100de4:	eb 03                	jmp    f0100de9 <check_page_free_list+0x237>
		else
			++nfree_extmem;
f0100de6:	ff 45 d0             	incl   -0x30(%ebp)
	for (pp = page_free_list; pp; pp = pp->pp_link)
		if (PDX(page2pa(pp)) < pdx_limit)
			memset(page2kva(pp), 0x97, 128);

	first_free_page = (char *) boot_alloc(0);
	for (pp = page_free_list; pp; pp = pp->pp_link) {
f0100de9:	8b 12                	mov    (%edx),%edx
f0100deb:	85 d2                	test   %edx,%edx
f0100ded:	0f 85 cd fe ff ff    	jne    f0100cc0 <check_page_free_list+0x10e>
f0100df3:	8b 5d d0             	mov    -0x30(%ebp),%ebx
			++nfree_basemem;
		else
			++nfree_extmem;
	}

	assert(nfree_basemem > 0);
f0100df6:	85 f6                	test   %esi,%esi
f0100df8:	7f 19                	jg     f0100e13 <check_page_free_list+0x261>
f0100dfa:	68 bf 5f 10 f0       	push   $0xf0105fbf
f0100dff:	68 d8 50 10 f0       	push   $0xf01050d8
f0100e04:	68 78 03 00 00       	push   $0x378
f0100e09:	68 41 5f 10 f0       	push   $0xf0105f41
f0100e0e:	e8 39 f2 ff ff       	call   f010004c <_panic>
	assert(nfree_extmem > 0);
f0100e13:	85 db                	test   %ebx,%ebx
f0100e15:	7f 42                	jg     f0100e59 <check_page_free_list+0x2a7>
f0100e17:	68 d1 5f 10 f0       	push   $0xf0105fd1
f0100e1c:	68 d8 50 10 f0       	push   $0xf01050d8
f0100e21:	68 79 03 00 00       	push   $0x379
f0100e26:	68 41 5f 10 f0       	push   $0xf0105f41
f0100e2b:	e8 1c f2 ff ff       	call   f010004c <_panic>
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
	int nfree_basemem = 0, nfree_extmem = 0;
	char *first_free_page;

	if (!page_free_list)
f0100e30:	a1 60 82 18 f0       	mov    0xf0188260,%eax
f0100e35:	85 c0                	test   %eax,%eax
f0100e37:	0f 85 a2 fd ff ff    	jne    f0100bdf <check_page_free_list+0x2d>
f0100e3d:	e9 86 fd ff ff       	jmp    f0100bc8 <check_page_free_list+0x16>
f0100e42:	83 3d 60 82 18 f0 00 	cmpl   $0x0,0xf0188260
f0100e49:	0f 84 79 fd ff ff    	je     f0100bc8 <check_page_free_list+0x16>
//
static void
check_page_free_list(bool only_low_memory)
{
	struct PageInfo *pp;
	unsigned pdx_limit = only_low_memory ? 1 : NPDENTRIES;
f0100e4f:	be 00 04 00 00       	mov    $0x400,%esi
f0100e54:	e9 d4 fd ff ff       	jmp    f0100c2d <check_page_free_list+0x7b>
			++nfree_extmem;
	}

	assert(nfree_basemem > 0);
	assert(nfree_extmem > 0);
}
f0100e59:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100e5c:	5b                   	pop    %ebx
f0100e5d:	5e                   	pop    %esi
f0100e5e:	5f                   	pop    %edi
f0100e5f:	5d                   	pop    %ebp
f0100e60:	c3                   	ret    

f0100e61 <page_init>:
// allocator functions below to allocate and deallocate physical
// memory via the page_free_list.
//
void
page_init(void)
{
f0100e61:	55                   	push   %ebp
f0100e62:	89 e5                	mov    %esp,%ebp
f0100e64:	56                   	push   %esi
f0100e65:	53                   	push   %ebx

	//page_free_list -> Pn -> Pn-1 -> ... -> P2 -> P1
	//physical page 0 is in use.
	//[mem 0x00001000-0x0009fbff] available
	int i;
	cprintf("npages: %d\n", npages);
f0100e66:	83 ec 08             	sub    $0x8,%esp
f0100e69:	ff 35 44 94 18 f0    	pushl  0xf0189444
f0100e6f:	68 e2 5f 10 f0       	push   $0xf0105fe2
f0100e74:	e8 18 25 00 00       	call   f0103391 <cprintf>
	assert(PGNUM(0x0009fbff) <= npages);
f0100e79:	83 c4 10             	add    $0x10,%esp
f0100e7c:	81 3d 44 94 18 f0 9e 	cmpl   $0x9e,0xf0189444
f0100e83:	00 00 00 
f0100e86:	76 0f                	jbe    f0100e97 <page_init+0x36>
f0100e88:	8b 0d 60 82 18 f0    	mov    0xf0188260,%ecx
f0100e8e:	b8 00 00 00 00       	mov    $0x0,%eax
f0100e93:	b3 00                	mov    $0x0,%bl
f0100e95:	eb 2d                	jmp    f0100ec4 <page_init+0x63>
f0100e97:	68 ee 5f 10 f0       	push   $0xf0105fee
f0100e9c:	68 d8 50 10 f0       	push   $0xf01050d8
f0100ea1:	68 4a 01 00 00       	push   $0x14a
f0100ea6:	68 41 5f 10 f0       	push   $0xf0105f41
f0100eab:	e8 9c f1 ff ff       	call   f010004c <_panic>
	for(i=1; i<(pa2page(0x0009fbff) - pages); i++){
		pages[i].pp_ref = 0;
f0100eb0:	01 c2                	add    %eax,%edx
f0100eb2:	66 c7 42 04 00 00    	movw   $0x0,0x4(%edx)
		pages[i].pp_link = page_free_list;
f0100eb8:	89 0a                	mov    %ecx,(%edx)
		page_free_list = &pages[i];
f0100eba:	89 c1                	mov    %eax,%ecx
f0100ebc:	03 0d 4c 94 18 f0    	add    0xf018944c,%ecx
f0100ec2:	b3 01                	mov    $0x1,%bl
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100ec4:	81 3d 44 94 18 f0 9f 	cmpl   $0x9f,0xf0189444
f0100ecb:	00 00 00 
f0100ece:	77 1e                	ja     f0100eee <page_init+0x8d>
f0100ed0:	84 db                	test   %bl,%bl
f0100ed2:	74 06                	je     f0100eda <page_init+0x79>
f0100ed4:	89 0d 60 82 18 f0    	mov    %ecx,0xf0188260
		panic("pa2page called with invalid pa");
f0100eda:	83 ec 04             	sub    $0x4,%esp
f0100edd:	68 38 58 10 f0       	push   $0xf0105838
f0100ee2:	6a 4f                	push   $0x4f
f0100ee4:	68 4d 5f 10 f0       	push   $0xf0105f4d
f0100ee9:	e8 5e f1 ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f0100eee:	8b 15 4c 94 18 f0    	mov    0xf018944c,%edx
f0100ef4:	83 c0 08             	add    $0x8,%eax
	//physical page 0 is in use.
	//[mem 0x00001000-0x0009fbff] available
	int i;
	cprintf("npages: %d\n", npages);
	assert(PGNUM(0x0009fbff) <= npages);
	for(i=1; i<(pa2page(0x0009fbff) - pages); i++){
f0100ef7:	3d f8 04 00 00       	cmp    $0x4f8,%eax
f0100efc:	75 b2                	jne    f0100eb0 <page_init+0x4f>
f0100efe:	84 db                	test   %bl,%bl
f0100f00:	74 06                	je     f0100f08 <page_init+0xa7>
f0100f02:	89 0d 60 82 18 f0    	mov    %ecx,0xf0188260
	//     0x00100000-0x????????  in use for kernel
    //[mem 0x00100000-0x07fdffff] available
	//physaddr_t start_PA = PADDR(boot_alloc(0)); //physical address of the end of the kernel plus previously allocated stuff
	//physaddr_t end_PA = PADDR(0x07fdffff);

	physaddr_t start_PA = (physaddr_t)boot_alloc(0) - KERNBASE;
f0100f08:	b8 00 00 00 00       	mov    $0x0,%eax
f0100f0d:	e8 f3 fb ff ff       	call   f0100b05 <boot_alloc>
	physaddr_t end_PA = (physaddr_t)0x07fdffff;
	assert(PGNUM(start_PA) <= npages);
f0100f12:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f0100f18:	c1 ea 0c             	shr    $0xc,%edx
f0100f1b:	a1 44 94 18 f0       	mov    0xf0189444,%eax
f0100f20:	39 c2                	cmp    %eax,%edx
f0100f22:	76 19                	jbe    f0100f3d <page_init+0xdc>
f0100f24:	68 0a 60 10 f0       	push   $0xf010600a
f0100f29:	68 d8 50 10 f0       	push   $0xf01050d8
f0100f2e:	68 58 01 00 00       	push   $0x158
f0100f33:	68 41 5f 10 f0       	push   $0xf0105f41
f0100f38:	e8 0f f1 ff ff       	call   f010004c <_panic>
	assert(PGNUM(end_PA) <= npages);
f0100f3d:	3d de 7f 00 00       	cmp    $0x7fde,%eax
f0100f42:	77 19                	ja     f0100f5d <page_init+0xfc>
f0100f44:	68 24 60 10 f0       	push   $0xf0106024
f0100f49:	68 d8 50 10 f0       	push   $0xf01050d8
f0100f4e:	68 59 01 00 00       	push   $0x159
f0100f53:	68 41 5f 10 f0       	push   $0xf0105f41
f0100f58:	e8 ef f0 ff ff       	call   f010004c <_panic>
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100f5d:	39 c2                	cmp    %eax,%edx
f0100f5f:	72 14                	jb     f0100f75 <page_init+0x114>
		panic("pa2page called with invalid pa");
f0100f61:	83 ec 04             	sub    $0x4,%esp
f0100f64:	68 38 58 10 f0       	push   $0xf0105838
f0100f69:	6a 4f                	push   $0x4f
f0100f6b:	68 4d 5f 10 f0       	push   $0xf0105f4d
f0100f70:	e8 d7 f0 ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f0100f75:	c1 e2 03             	shl    $0x3,%edx
	for(i= (pa2page(start_PA) - pages); i<(pa2page(end_PA) - pages); i++){
f0100f78:	89 d1                	mov    %edx,%ecx
f0100f7a:	c1 f9 03             	sar    $0x3,%ecx
f0100f7d:	8b 1d 60 82 18 f0    	mov    0xf0188260,%ebx
f0100f83:	be 00 00 00 00       	mov    $0x0,%esi
f0100f88:	eb 1b                	jmp    f0100fa5 <page_init+0x144>
		pages[i].pp_ref = 0;
f0100f8a:	01 d0                	add    %edx,%eax
f0100f8c:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)
		pages[i].pp_link = page_free_list;
f0100f92:	89 18                	mov    %ebx,(%eax)
		page_free_list = &pages[i];
f0100f94:	89 d3                	mov    %edx,%ebx
f0100f96:	03 1d 4c 94 18 f0    	add    0xf018944c,%ebx

	physaddr_t start_PA = (physaddr_t)boot_alloc(0) - KERNBASE;
	physaddr_t end_PA = (physaddr_t)0x07fdffff;
	assert(PGNUM(start_PA) <= npages);
	assert(PGNUM(end_PA) <= npages);
	for(i= (pa2page(start_PA) - pages); i<(pa2page(end_PA) - pages); i++){
f0100f9c:	41                   	inc    %ecx
f0100f9d:	83 c2 08             	add    $0x8,%edx
f0100fa0:	be 01 00 00 00       	mov    $0x1,%esi
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0100fa5:	81 3d 44 94 18 f0 df 	cmpl   $0x7fdf,0xf0189444
f0100fac:	7f 00 00 
f0100faf:	77 20                	ja     f0100fd1 <page_init+0x170>
f0100fb1:	89 f0                	mov    %esi,%eax
f0100fb3:	84 c0                	test   %al,%al
f0100fb5:	74 06                	je     f0100fbd <page_init+0x15c>
f0100fb7:	89 1d 60 82 18 f0    	mov    %ebx,0xf0188260
		panic("pa2page called with invalid pa");
f0100fbd:	83 ec 04             	sub    $0x4,%esp
f0100fc0:	68 38 58 10 f0       	push   $0xf0105838
f0100fc5:	6a 4f                	push   $0x4f
f0100fc7:	68 4d 5f 10 f0       	push   $0xf0105f4d
f0100fcc:	e8 7b f0 ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f0100fd1:	a1 4c 94 18 f0       	mov    0xf018944c,%eax
f0100fd6:	81 f9 de 7f 00 00    	cmp    $0x7fde,%ecx
f0100fdc:	7e ac                	jle    f0100f8a <page_init+0x129>
f0100fde:	89 f0                	mov    %esi,%eax
f0100fe0:	84 c0                	test   %al,%al
f0100fe2:	74 06                	je     f0100fea <page_init+0x189>
f0100fe4:	89 1d 60 82 18 f0    	mov    %ebx,0xf0188260
		page_free_list = &pages[i];
	}
	//page_free_list->pp_ref = 1;
	//page_free_list->pp_ref = 0;
	//cprintf("Got here1\n");
}
f0100fea:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0100fed:	5b                   	pop    %ebx
f0100fee:	5e                   	pop    %esi
f0100fef:	5d                   	pop    %ebp
f0100ff0:	c3                   	ret    

f0100ff1 <page_alloc>:
// Returns NULL if out of free memory.
//
// Hint: use page2kva and memset
struct PageInfo *
page_alloc(int alloc_flags)
{
f0100ff1:	55                   	push   %ebp
f0100ff2:	89 e5                	mov    %esp,%ebp
f0100ff4:	53                   	push   %ebx
f0100ff5:	83 ec 04             	sub    $0x4,%esp
	
	// Fill this function in
	if(page_free_list == NULL){ //check if out of memory
f0100ff8:	8b 1d 60 82 18 f0    	mov    0xf0188260,%ebx
f0100ffe:	85 db                	test   %ebx,%ebx
f0101000:	74 5e                	je     f0101060 <page_alloc+0x6f>
	}
	//page_free_list->pp_ref = 1;
	//page_free_list->pp_ref = 0;
	//page_free_list->pp_link = NULL;
	struct PageInfo *page = page_free_list;
	page_free_list = page_free_list->pp_link;
f0101002:	8b 03                	mov    (%ebx),%eax
f0101004:	a3 60 82 18 f0       	mov    %eax,0xf0188260
	page->pp_link = NULL;
f0101009:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
	page->pp_ref = 0;
f010100f:	66 c7 43 04 00 00    	movw   $0x0,0x4(%ebx)
	if(alloc_flags & ALLOC_ZERO){  //fills the entire returned physical page with '\0' bytes
f0101015:	f6 45 08 01          	testb  $0x1,0x8(%ebp)
f0101019:	74 45                	je     f0101060 <page_alloc+0x6f>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f010101b:	89 d8                	mov    %ebx,%eax
f010101d:	2b 05 4c 94 18 f0    	sub    0xf018944c,%eax
f0101023:	c1 f8 03             	sar    $0x3,%eax
f0101026:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101029:	89 c2                	mov    %eax,%edx
f010102b:	c1 ea 0c             	shr    $0xc,%edx
f010102e:	3b 15 44 94 18 f0    	cmp    0xf0189444,%edx
f0101034:	72 12                	jb     f0101048 <page_alloc+0x57>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101036:	50                   	push   %eax
f0101037:	68 50 57 10 f0       	push   $0xf0105750
f010103c:	6a 56                	push   $0x56
f010103e:	68 4d 5f 10 f0       	push   $0xf0105f4d
f0101043:	e8 04 f0 ff ff       	call   f010004c <_panic>
		memset(page2kva(page), '\0', PGSIZE);
f0101048:	83 ec 04             	sub    $0x4,%esp
f010104b:	68 00 10 00 00       	push   $0x1000
f0101050:	6a 00                	push   $0x0
f0101052:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0101057:	50                   	push   %eax
f0101058:	e8 e6 3b 00 00       	call   f0104c43 <memset>
f010105d:	83 c4 10             	add    $0x10,%esp
		char *kva = page2kva(result);
		memset(kva, '\0', PGSIZE);
	}
	
	return result;*/
}
f0101060:	89 d8                	mov    %ebx,%eax
f0101062:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0101065:	c9                   	leave  
f0101066:	c3                   	ret    

f0101067 <page_free>:
// Return a page to the free list.
// (This function should only be called when pp->pp_ref reaches 0.)
//
void
page_free(struct PageInfo *pp)
{
f0101067:	55                   	push   %ebp
f0101068:	89 e5                	mov    %esp,%ebp
f010106a:	83 ec 08             	sub    $0x8,%esp
f010106d:	8b 45 08             	mov    0x8(%ebp),%eax
	// Fill this function in
	// Hint: You may want to panic if pp->pp_ref is nonzero or
	// pp->pp_link is not NULL.
	if(pp->pp_ref != 0){
f0101070:	66 83 78 04 00       	cmpw   $0x0,0x4(%eax)
f0101075:	74 17                	je     f010108e <page_free+0x27>
		panic("pp->pp_ref is nonzero");
f0101077:	83 ec 04             	sub    $0x4,%esp
f010107a:	68 3c 60 10 f0       	push   $0xf010603c
f010107f:	68 a5 01 00 00       	push   $0x1a5
f0101084:	68 41 5f 10 f0       	push   $0xf0105f41
f0101089:	e8 be ef ff ff       	call   f010004c <_panic>
	}

	if(pp->pp_link != NULL){
f010108e:	83 38 00             	cmpl   $0x0,(%eax)
f0101091:	74 17                	je     f01010aa <page_free+0x43>
		panic("pp->pp_link is not NULL");
f0101093:	83 ec 04             	sub    $0x4,%esp
f0101096:	68 52 60 10 f0       	push   $0xf0106052
f010109b:	68 a9 01 00 00       	push   $0x1a9
f01010a0:	68 41 5f 10 f0       	push   $0xf0105f41
f01010a5:	e8 a2 ef ff ff       	call   f010004c <_panic>
	}

	if(page_free_list == NULL){
f01010aa:	8b 15 60 82 18 f0    	mov    0xf0188260,%edx
f01010b0:	85 d2                	test   %edx,%edx
f01010b2:	75 07                	jne    f01010bb <page_free+0x54>
		page_free_list = pp;
f01010b4:	a3 60 82 18 f0       	mov    %eax,0xf0188260
f01010b9:	eb 07                	jmp    f01010c2 <page_free+0x5b>
	}else{
		pp->pp_link = page_free_list;
f01010bb:	89 10                	mov    %edx,(%eax)
		page_free_list = pp;
f01010bd:	a3 60 82 18 f0       	mov    %eax,0xf0188260
	}
	//cprintf("Free-ed %x\n", pp);
}
f01010c2:	c9                   	leave  
f01010c3:	c3                   	ret    

f01010c4 <page_decref>:
// Decrement the reference count on a page,
// freeing it if there are no more refs.
//
void
page_decref(struct PageInfo* pp)
{
f01010c4:	55                   	push   %ebp
f01010c5:	89 e5                	mov    %esp,%ebp
f01010c7:	83 ec 08             	sub    $0x8,%esp
f01010ca:	8b 55 08             	mov    0x8(%ebp),%edx
	if (--pp->pp_ref == 0)
f01010cd:	8b 42 04             	mov    0x4(%edx),%eax
f01010d0:	48                   	dec    %eax
f01010d1:	66 89 42 04          	mov    %ax,0x4(%edx)
f01010d5:	66 85 c0             	test   %ax,%ax
f01010d8:	75 0c                	jne    f01010e6 <page_decref+0x22>
		page_free(pp);
f01010da:	83 ec 0c             	sub    $0xc,%esp
f01010dd:	52                   	push   %edx
f01010de:	e8 84 ff ff ff       	call   f0101067 <page_free>
f01010e3:	83 c4 10             	add    $0x10,%esp
}
f01010e6:	c9                   	leave  
f01010e7:	c3                   	ret    

f01010e8 <pgdir_walk>:
// Hint 3: look at inc/mmu.h for useful macros that mainipulate page
// table and page directory entries.
//
pte_t *
pgdir_walk(pde_t *pgdir, const void *va, int create)
{
f01010e8:	55                   	push   %ebp
f01010e9:	89 e5                	mov    %esp,%ebp
f01010eb:	53                   	push   %ebx
f01010ec:	83 ec 04             	sub    $0x4,%esp
f01010ef:	8b 45 08             	mov    0x8(%ebp),%eax
	//pte_t* pte_Ptr = (pte_t*)(KADDR(pde) + PTX(va));
	return pte_Ptr;
	*/

	//reference code:
	assert(pgdir);
f01010f2:	85 c0                	test   %eax,%eax
f01010f4:	75 19                	jne    f010110f <pgdir_walk+0x27>
f01010f6:	68 6a 60 10 f0       	push   $0xf010606a
f01010fb:	68 d8 50 10 f0       	push   $0xf01050d8
f0101100:	68 fe 01 00 00       	push   $0x1fe
f0101105:	68 41 5f 10 f0       	push   $0xf0105f41
f010110a:	e8 3d ef ff ff       	call   f010004c <_panic>

	// Page directory index.
	size_t pdx = PDX(va);

	// Page directory entry.
	pde_t pde = pgdir[pdx];
f010110f:	8b 55 0c             	mov    0xc(%ebp),%edx
f0101112:	c1 ea 16             	shr    $0x16,%edx
f0101115:	8d 1c 90             	lea    (%eax,%edx,4),%ebx
f0101118:	8b 03                	mov    (%ebx),%eax

	// Page table present?
	// pt is a (kernel) virtual address.
	if (pde & PTE_P) {
f010111a:	a8 01                	test   $0x1,%al
f010111c:	74 2f                	je     f010114d <pgdir_walk+0x65>
		// Get page table's kva from physical address.
		pt = KADDR(PTE_ADDR(pde));
f010111e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101123:	89 c2                	mov    %eax,%edx
f0101125:	c1 ea 0c             	shr    $0xc,%edx
f0101128:	39 15 44 94 18 f0    	cmp    %edx,0xf0189444
f010112e:	77 15                	ja     f0101145 <pgdir_walk+0x5d>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101130:	50                   	push   %eax
f0101131:	68 50 57 10 f0       	push   $0xf0105750
f0101136:	68 10 02 00 00       	push   $0x210
f010113b:	68 41 5f 10 f0       	push   $0xf0105f41
f0101140:	e8 07 ef ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f0101145:	8d 90 00 00 00 f0    	lea    -0x10000000(%eax),%edx
f010114b:	eb 5f                	jmp    f01011ac <pgdir_walk+0xc4>
	} else {
		if (!create)
f010114d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0101151:	74 68                	je     f01011bb <pgdir_walk+0xd3>
			return NULL;
		
		// Create a new page for the page table.
		struct PageInfo * page = page_alloc(ALLOC_ZERO);
f0101153:	83 ec 0c             	sub    $0xc,%esp
f0101156:	6a 01                	push   $0x1
f0101158:	e8 94 fe ff ff       	call   f0100ff1 <page_alloc>
		if (!page)
f010115d:	83 c4 10             	add    $0x10,%esp
f0101160:	85 c0                	test   %eax,%eax
f0101162:	74 5e                	je     f01011c2 <pgdir_walk+0xda>
			return NULL;

		// The new page has these permissions for convenience.
		pgdir[pdx] = page2pa(page) | PTE_U | PTE_W | PTE_P;
f0101164:	89 c2                	mov    %eax,%edx
f0101166:	2b 15 4c 94 18 f0    	sub    0xf018944c,%edx
f010116c:	c1 fa 03             	sar    $0x3,%edx
f010116f:	c1 e2 0c             	shl    $0xc,%edx
f0101172:	83 ca 07             	or     $0x7,%edx
f0101175:	89 13                	mov    %edx,(%ebx)

		// It appeared in page directory, so increment refcount.
		page->pp_ref++;
f0101177:	66 ff 40 04          	incw   0x4(%eax)
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f010117b:	2b 05 4c 94 18 f0    	sub    0xf018944c,%eax
f0101181:	c1 f8 03             	sar    $0x3,%eax
f0101184:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101187:	89 c2                	mov    %eax,%edx
f0101189:	c1 ea 0c             	shr    $0xc,%edx
f010118c:	3b 15 44 94 18 f0    	cmp    0xf0189444,%edx
f0101192:	72 12                	jb     f01011a6 <pgdir_walk+0xbe>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101194:	50                   	push   %eax
f0101195:	68 50 57 10 f0       	push   $0xf0105750
f010119a:	6a 56                	push   $0x56
f010119c:	68 4d 5f 10 f0       	push   $0xf0105f4d
f01011a1:	e8 a6 ee ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f01011a6:	8d 90 00 00 00 f0    	lea    -0x10000000(%eax),%edx

	// Page table index.
	size_t ptx = PTX(va);

	// Return page table entry pointer.
	return &pt[ptx];
f01011ac:	8b 45 0c             	mov    0xc(%ebp),%eax
f01011af:	c1 e8 0a             	shr    $0xa,%eax
f01011b2:	25 fc 0f 00 00       	and    $0xffc,%eax
f01011b7:	01 d0                	add    %edx,%eax
f01011b9:	eb 0c                	jmp    f01011c7 <pgdir_walk+0xdf>
	if (pde & PTE_P) {
		// Get page table's kva from physical address.
		pt = KADDR(PTE_ADDR(pde));
	} else {
		if (!create)
			return NULL;
f01011bb:	b8 00 00 00 00       	mov    $0x0,%eax
f01011c0:	eb 05                	jmp    f01011c7 <pgdir_walk+0xdf>
		
		// Create a new page for the page table.
		struct PageInfo * page = page_alloc(ALLOC_ZERO);
		if (!page)
			return NULL;
f01011c2:	b8 00 00 00 00       	mov    $0x0,%eax
	// Page table index.
	size_t ptx = PTX(va);

	// Return page table entry pointer.
	return &pt[ptx];
}
f01011c7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01011ca:	c9                   	leave  
f01011cb:	c3                   	ret    

f01011cc <boot_map_region>:
// mapped pages.
//
// Hint: the TA solution uses pgdir_walk
static void
boot_map_region(pde_t *pgdir, uintptr_t va, size_t size, physaddr_t pa, int perm)
{
f01011cc:	55                   	push   %ebp
f01011cd:	89 e5                	mov    %esp,%ebp
f01011cf:	57                   	push   %edi
f01011d0:	56                   	push   %esi
f01011d1:	53                   	push   %ebx
f01011d2:	83 ec 1c             	sub    $0x1c,%esp
f01011d5:	89 c3                	mov    %eax,%ebx
f01011d7:	89 45 e0             	mov    %eax,-0x20(%ebp)
f01011da:	89 cf                	mov    %ecx,%edi
f01011dc:	8b 45 08             	mov    0x8(%ebp),%eax
		//cprintf("pte boot_map_region get is: %x\n", (*pte) );
		//page_insert(pgdir, pa2page(pa+i*PGSIZE), (void*)(va+i*PGSIZE), perm);
//	}
	
	//reference code:
	assert(pgdir);
f01011df:	85 db                	test   %ebx,%ebx
f01011e1:	75 19                	jne    f01011fc <boot_map_region+0x30>
f01011e3:	68 6a 60 10 f0       	push   $0xf010606a
f01011e8:	68 d8 50 10 f0       	push   $0xf01050d8
f01011ed:	68 4f 02 00 00       	push   $0x24f
f01011f2:	68 41 5f 10 f0       	push   $0xf0105f41
f01011f7:	e8 50 ee ff ff       	call   f010004c <_panic>
	assert(ROUNDDOWN(va, PGSIZE) == va);
f01011fc:	f7 c2 ff 0f 00 00    	test   $0xfff,%edx
f0101202:	74 19                	je     f010121d <boot_map_region+0x51>
f0101204:	68 70 60 10 f0       	push   $0xf0106070
f0101209:	68 d8 50 10 f0       	push   $0xf01050d8
f010120e:	68 50 02 00 00       	push   $0x250
f0101213:	68 41 5f 10 f0       	push   $0xf0105f41
f0101218:	e8 2f ee ff ff       	call   f010004c <_panic>
	assert(ROUNDDOWN(size, PGSIZE) == size);
f010121d:	f7 c7 ff 0f 00 00    	test   $0xfff,%edi
f0101223:	74 19                	je     f010123e <boot_map_region+0x72>
f0101225:	68 58 58 10 f0       	push   $0xf0105858
f010122a:	68 d8 50 10 f0       	push   $0xf01050d8
f010122f:	68 51 02 00 00       	push   $0x251
f0101234:	68 41 5f 10 f0       	push   $0xf0105f41
f0101239:	e8 0e ee ff ff       	call   f010004c <_panic>
	assert(ROUNDDOWN(pa, PGSIZE) == pa);
f010123e:	a9 ff 0f 00 00       	test   $0xfff,%eax
f0101243:	74 3d                	je     f0101282 <boot_map_region+0xb6>
f0101245:	68 8c 60 10 f0       	push   $0xf010608c
f010124a:	68 d8 50 10 f0       	push   $0xf01050d8
f010124f:	68 52 02 00 00       	push   $0x252
f0101254:	68 41 5f 10 f0       	push   $0xf0105f41
f0101259:	e8 ee ed ff ff       	call   f010004c <_panic>

	while (size >= PGSIZE) {
		// Find PTE of va.
		pte_t *pte = pgdir_walk(pgdir, (void *)va, 1);
f010125e:	83 ec 04             	sub    $0x4,%esp
f0101261:	6a 01                	push   $0x1
f0101263:	53                   	push   %ebx
f0101264:	ff 75 e0             	pushl  -0x20(%ebp)
f0101267:	e8 7c fe ff ff       	call   f01010e8 <pgdir_walk>

		// Make it present and map to pa.
		*pte = pa | perm | PTE_P;
f010126c:	0b 75 dc             	or     -0x24(%ebp),%esi
f010126f:	89 30                	mov    %esi,(%eax)

		// Next page.
		va += PGSIZE;
f0101271:	81 c3 00 10 00 00    	add    $0x1000,%ebx
		pa += PGSIZE;
		size -= PGSIZE;
f0101277:	81 ef 00 10 00 00    	sub    $0x1000,%edi
f010127d:	83 c4 10             	add    $0x10,%esp
f0101280:	eb 10                	jmp    f0101292 <boot_map_region+0xc6>
f0101282:	89 d3                	mov    %edx,%ebx
f0101284:	29 d0                	sub    %edx,%eax
f0101286:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	while (size >= PGSIZE) {
		// Find PTE of va.
		pte_t *pte = pgdir_walk(pgdir, (void *)va, 1);

		// Make it present and map to pa.
		*pte = pa | perm | PTE_P;
f0101289:	8b 45 0c             	mov    0xc(%ebp),%eax
f010128c:	83 c8 01             	or     $0x1,%eax
f010128f:	89 45 dc             	mov    %eax,-0x24(%ebp)
f0101292:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0101295:	8d 34 18             	lea    (%eax,%ebx,1),%esi
	assert(pgdir);
	assert(ROUNDDOWN(va, PGSIZE) == va);
	assert(ROUNDDOWN(size, PGSIZE) == size);
	assert(ROUNDDOWN(pa, PGSIZE) == pa);

	while (size >= PGSIZE) {
f0101298:	81 ff ff 0f 00 00    	cmp    $0xfff,%edi
f010129e:	77 be                	ja     f010125e <boot_map_region+0x92>
		// Next page.
		va += PGSIZE;
		pa += PGSIZE;
		size -= PGSIZE;
	}
}
f01012a0:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01012a3:	5b                   	pop    %ebx
f01012a4:	5e                   	pop    %esi
f01012a5:	5f                   	pop    %edi
f01012a6:	5d                   	pop    %ebp
f01012a7:	c3                   	ret    

f01012a8 <page_lookup>:
//
// Hint: the TA solution uses pgdir_walk and pa2page.
//
struct PageInfo *
page_lookup(pde_t *pgdir, void *va, pte_t **pte_store)
{
f01012a8:	55                   	push   %ebp
f01012a9:	89 e5                	mov    %esp,%ebp
f01012ab:	53                   	push   %ebx
f01012ac:	83 ec 08             	sub    $0x8,%esp
f01012af:	8b 5d 10             	mov    0x10(%ebp),%ebx
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
f01012b2:	6a 00                	push   $0x0
f01012b4:	ff 75 0c             	pushl  0xc(%ebp)
f01012b7:	ff 75 08             	pushl  0x8(%ebp)
f01012ba:	e8 29 fe ff ff       	call   f01010e8 <pgdir_walk>
	//cprintf("H3\n");
	while(pte_Ptr == NULL){
f01012bf:	83 c4 10             	add    $0x10,%esp
f01012c2:	85 c0                	test   %eax,%eax
f01012c4:	74 5a                	je     f0101320 <page_lookup+0x78>
		return NULL; //no page mapped at va (not even a table)
	}

	if(pte_store != 0){
f01012c6:	85 db                	test   %ebx,%ebx
f01012c8:	74 02                	je     f01012cc <page_lookup+0x24>
		*pte_store = pte_Ptr;
f01012ca:	89 03                	mov    %eax,(%ebx)
	}

	pte_t pte = *pte_Ptr;
f01012cc:	8b 00                	mov    (%eax),%eax
	if(pte & PTE_P){ //if present
f01012ce:	a8 01                	test   $0x1,%al
f01012d0:	74 55                	je     f0101327 <page_lookup+0x7f>
		assert(PGNUM( PTE_ADDR(pte) ) < npages);
f01012d2:	8b 15 44 94 18 f0    	mov    0xf0189444,%edx
f01012d8:	89 c1                	mov    %eax,%ecx
f01012da:	c1 e9 0c             	shr    $0xc,%ecx
f01012dd:	39 d1                	cmp    %edx,%ecx
f01012df:	72 19                	jb     f01012fa <page_lookup+0x52>
f01012e1:	68 78 58 10 f0       	push   $0xf0105878
f01012e6:	68 d8 50 10 f0       	push   $0xf01050d8
f01012eb:	68 aa 02 00 00       	push   $0x2aa
f01012f0:	68 41 5f 10 f0       	push   $0xf0105f41
f01012f5:	e8 52 ed ff ff       	call   f010004c <_panic>
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01012fa:	c1 e8 0c             	shr    $0xc,%eax
f01012fd:	39 c2                	cmp    %eax,%edx
f01012ff:	77 14                	ja     f0101315 <page_lookup+0x6d>
		panic("pa2page called with invalid pa");
f0101301:	83 ec 04             	sub    $0x4,%esp
f0101304:	68 38 58 10 f0       	push   $0xf0105838
f0101309:	6a 4f                	push   $0x4f
f010130b:	68 4d 5f 10 f0       	push   $0xf0105f4d
f0101310:	e8 37 ed ff ff       	call   f010004c <_panic>
	return &pages[PGNUM(pa)];
f0101315:	8b 15 4c 94 18 f0    	mov    0xf018944c,%edx
f010131b:	8d 04 c2             	lea    (%edx,%eax,8),%eax
		return (struct PageInfo*)pa2page(PTE_ADDR(pte));
f010131e:	eb 0c                	jmp    f010132c <page_lookup+0x84>
{
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
	//cprintf("H3\n");
	while(pte_Ptr == NULL){
		return NULL; //no page mapped at va (not even a table)
f0101320:	b8 00 00 00 00       	mov    $0x0,%eax
f0101325:	eb 05                	jmp    f010132c <page_lookup+0x84>
	pte_t pte = *pte_Ptr;
	if(pte & PTE_P){ //if present
		assert(PGNUM( PTE_ADDR(pte) ) < npages);
		return (struct PageInfo*)pa2page(PTE_ADDR(pte));
	}else{
		return NULL;
f0101327:	b8 00 00 00 00       	mov    $0x0,%eax
	}
}
f010132c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f010132f:	c9                   	leave  
f0101330:	c3                   	ret    

f0101331 <page_remove>:
// Hint: The TA solution is implemented using page_lookup,
// 	tlb_invalidate, and page_decref.
//
void
page_remove(pde_t *pgdir, void *va)
{
f0101331:	55                   	push   %ebp
f0101332:	89 e5                	mov    %esp,%ebp
f0101334:	56                   	push   %esi
f0101335:	53                   	push   %ebx
f0101336:	8b 75 08             	mov    0x8(%ebp),%esi
f0101339:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	// Fill this function in
	//pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
	if( page_lookup(pgdir, va, 0) == NULL){
f010133c:	83 ec 04             	sub    $0x4,%esp
f010133f:	6a 00                	push   $0x0
f0101341:	53                   	push   %ebx
f0101342:	56                   	push   %esi
f0101343:	e8 60 ff ff ff       	call   f01012a8 <page_lookup>
f0101348:	83 c4 10             	add    $0x10,%esp
f010134b:	85 c0                	test   %eax,%eax
f010134d:	74 74                	je     f01013c3 <page_remove+0x92>
}

static inline void
invlpg(void *addr)
{
	asm volatile("invlpg (%0)" : : "r" (addr) : "memory");
f010134f:	0f 01 3b             	invlpg (%ebx)
		return;
	}
	tlb_invalidate(pgdir, va);
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 0);
f0101352:	83 ec 04             	sub    $0x4,%esp
f0101355:	6a 00                	push   $0x0
f0101357:	53                   	push   %ebx
f0101358:	56                   	push   %esi
f0101359:	e8 8a fd ff ff       	call   f01010e8 <pgdir_walk>
	pte_t pte = *pte_Ptr;
f010135e:	8b 10                	mov    (%eax),%edx
	*pte_Ptr = 0;
f0101360:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	assert(PGNUM(PTE_ADDR(pte)) < npages);
f0101366:	8b 0d 44 94 18 f0    	mov    0xf0189444,%ecx
f010136c:	89 d0                	mov    %edx,%eax
f010136e:	c1 e8 0c             	shr    $0xc,%eax
f0101371:	83 c4 10             	add    $0x10,%esp
f0101374:	39 c8                	cmp    %ecx,%eax
f0101376:	72 19                	jb     f0101391 <page_remove+0x60>
f0101378:	68 a8 60 10 f0       	push   $0xf01060a8
f010137d:	68 d8 50 10 f0       	push   $0xf01050d8
f0101382:	68 cc 02 00 00       	push   $0x2cc
f0101387:	68 41 5f 10 f0       	push   $0xf0105f41
f010138c:	e8 bb ec ff ff       	call   f010004c <_panic>
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101391:	89 d0                	mov    %edx,%eax
f0101393:	c1 e8 0c             	shr    $0xc,%eax
f0101396:	39 c8                	cmp    %ecx,%eax
f0101398:	72 14                	jb     f01013ae <page_remove+0x7d>
		panic("pa2page called with invalid pa");
f010139a:	83 ec 04             	sub    $0x4,%esp
f010139d:	68 38 58 10 f0       	push   $0xf0105838
f01013a2:	6a 4f                	push   $0x4f
f01013a4:	68 4d 5f 10 f0       	push   $0xf0105f4d
f01013a9:	e8 9e ec ff ff       	call   f010004c <_panic>
	struct PageInfo* pp = pa2page( PTE_ADDR(pte));
	page_decref(pp);
f01013ae:	83 ec 0c             	sub    $0xc,%esp
f01013b1:	8b 15 4c 94 18 f0    	mov    0xf018944c,%edx
f01013b7:	8d 04 c2             	lea    (%edx,%eax,8),%eax
f01013ba:	50                   	push   %eax
f01013bb:	e8 04 fd ff ff       	call   f01010c4 <page_decref>
f01013c0:	83 c4 10             	add    $0x10,%esp

}
f01013c3:	8d 65 f8             	lea    -0x8(%ebp),%esp
f01013c6:	5b                   	pop    %ebx
f01013c7:	5e                   	pop    %esi
f01013c8:	5d                   	pop    %ebp
f01013c9:	c3                   	ret    

f01013ca <page_insert>:
// Hint: The TA solution is implemented using pgdir_walk, page_remove,
// and page2pa.
//
int
page_insert(pde_t *pgdir, struct PageInfo *pp, void *va, int perm)
{
f01013ca:	55                   	push   %ebp
f01013cb:	89 e5                	mov    %esp,%ebp
f01013cd:	57                   	push   %edi
f01013ce:	56                   	push   %esi
f01013cf:	53                   	push   %ebx
f01013d0:	83 ec 10             	sub    $0x10,%esp
f01013d3:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f01013d6:	8b 7d 10             	mov    0x10(%ebp),%edi
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 1); //find the pointer to the page table entry
f01013d9:	6a 01                	push   $0x1
f01013db:	57                   	push   %edi
f01013dc:	ff 75 08             	pushl  0x8(%ebp)
f01013df:	e8 04 fd ff ff       	call   f01010e8 <pgdir_walk>
	//cprintf("Addr from pgdir_walk: %x\n", pte_Ptr);
	if(pte_Ptr == NULL){
f01013e4:	83 c4 10             	add    $0x10,%esp
f01013e7:	85 c0                	test   %eax,%eax
f01013e9:	74 38                	je     f0101423 <page_insert+0x59>
f01013eb:	89 c6                	mov    %eax,%esi
		return -E_NO_MEM; //page table couldn't be allocated
	}
	pte_t pte = *pte_Ptr;
f01013ed:	8b 00                	mov    (%eax),%eax
	pp->pp_ref++;
f01013ef:	66 ff 43 04          	incw   0x4(%ebx)
	if(pte & PTE_P){ //check if there is already a page mapped at 'va'
f01013f3:	a8 01                	test   $0x1,%al
f01013f5:	74 0f                	je     f0101406 <page_insert+0x3c>
		page_remove(pgdir, va); //remove the exising page
f01013f7:	83 ec 08             	sub    $0x8,%esp
f01013fa:	57                   	push   %edi
f01013fb:	ff 75 08             	pushl  0x8(%ebp)
f01013fe:	e8 2e ff ff ff       	call   f0101331 <page_remove>
f0101403:	83 c4 10             	add    $0x10,%esp
	}
	*pte_Ptr = PTE_ADDR(page2pa(pp)) | PTE_P | perm; //insert the page
f0101406:	2b 1d 4c 94 18 f0    	sub    0xf018944c,%ebx
f010140c:	c1 fb 03             	sar    $0x3,%ebx
f010140f:	c1 e3 0c             	shl    $0xc,%ebx
f0101412:	8b 45 14             	mov    0x14(%ebp),%eax
f0101415:	83 c8 01             	or     $0x1,%eax
f0101418:	09 c3                	or     %eax,%ebx
f010141a:	89 1e                	mov    %ebx,(%esi)
	//pgdir[PDX(va)] |= perm;
	//pp->pp_ref++;
	return 0; //success
f010141c:	b8 00 00 00 00       	mov    $0x0,%eax
f0101421:	eb 05                	jmp    f0101428 <page_insert+0x5e>
{
	// Fill this function in
	pte_t* pte_Ptr = pgdir_walk(pgdir, va, 1); //find the pointer to the page table entry
	//cprintf("Addr from pgdir_walk: %x\n", pte_Ptr);
	if(pte_Ptr == NULL){
		return -E_NO_MEM; //page table couldn't be allocated
f0101423:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
	}
	*pte_Ptr = PTE_ADDR(page2pa(pp)) | PTE_P | perm; //insert the page
	//pgdir[PDX(va)] |= perm;
	//pp->pp_ref++;
	return 0; //success
}
f0101428:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010142b:	5b                   	pop    %ebx
f010142c:	5e                   	pop    %esi
f010142d:	5f                   	pop    %edi
f010142e:	5d                   	pop    %ebp
f010142f:	c3                   	ret    

f0101430 <mem_init>:
//
// From UTOP to ULIM, the user is allowed to read but not write.
// Above ULIM the user cannot read or write.
void
mem_init(void)
{
f0101430:	55                   	push   %ebp
f0101431:	89 e5                	mov    %esp,%ebp
f0101433:	57                   	push   %edi
f0101434:	56                   	push   %esi
f0101435:	53                   	push   %ebx
f0101436:	83 ec 3c             	sub    $0x3c,%esp
	uint32_t i;
	struct e820_entry *e;
	size_t mem = 0, mem_max = -KERNBASE;

	e = e820_map.entries;
	for (i = 0; i != e820_map.nr; ++i, ++e) {
f0101439:	8b 1d 40 8f 18 f0    	mov    0xf0188f40,%ebx
static void
detect_memory(void)
{
	uint32_t i;
	struct e820_entry *e;
	size_t mem = 0, mem_max = -KERNBASE;
f010143f:	be 00 00 00 00       	mov    $0x0,%esi

	e = e820_map.entries;
f0101444:	b8 44 8f 18 f0       	mov    $0xf0188f44,%eax
	for (i = 0; i != e820_map.nr; ++i, ++e) {
f0101449:	b9 00 00 00 00       	mov    $0x0,%ecx
f010144e:	eb 1d                	jmp    f010146d <mem_init+0x3d>
		if (e->addr >= mem_max)
f0101450:	8b 10                	mov    (%eax),%edx
f0101452:	83 78 04 00          	cmpl   $0x0,0x4(%eax)
f0101456:	75 11                	jne    f0101469 <mem_init+0x39>
f0101458:	81 fa ff ff ff 0f    	cmp    $0xfffffff,%edx
f010145e:	77 09                	ja     f0101469 <mem_init+0x39>
			continue;
		mem = MAX(mem, (size_t)(e->addr + e->len));
f0101460:	03 50 08             	add    0x8(%eax),%edx
f0101463:	39 d6                	cmp    %edx,%esi
f0101465:	73 02                	jae    f0101469 <mem_init+0x39>
f0101467:	89 d6                	mov    %edx,%esi
	uint32_t i;
	struct e820_entry *e;
	size_t mem = 0, mem_max = -KERNBASE;

	e = e820_map.entries;
	for (i = 0; i != e820_map.nr; ++i, ++e) {
f0101469:	41                   	inc    %ecx
f010146a:	83 c0 14             	add    $0x14,%eax
f010146d:	39 d9                	cmp    %ebx,%ecx
f010146f:	75 df                	jne    f0101450 <mem_init+0x20>
			continue;
		mem = MAX(mem, (size_t)(e->addr + e->len));
	}

	// Limit memory to 256MB.
	mem = MIN(mem, mem_max);
f0101471:	89 f0                	mov    %esi,%eax
f0101473:	81 fe 00 00 00 10    	cmp    $0x10000000,%esi
f0101479:	76 05                	jbe    f0101480 <mem_init+0x50>
f010147b:	b8 00 00 00 10       	mov    $0x10000000,%eax
	npages = mem / PGSIZE;
f0101480:	89 c2                	mov    %eax,%edx
f0101482:	c1 ea 0c             	shr    $0xc,%edx
f0101485:	89 15 44 94 18 f0    	mov    %edx,0xf0189444
	cprintf("E820: physical memory %uMB\n", mem / 1024 / 1024);
f010148b:	83 ec 08             	sub    $0x8,%esp
f010148e:	c1 e8 14             	shr    $0x14,%eax
f0101491:	50                   	push   %eax
f0101492:	68 c6 60 10 f0       	push   $0xf01060c6
f0101497:	e8 f5 1e 00 00       	call   f0103391 <cprintf>
	// Remove this line when you're ready to test this function.
	//panic("mem_init: This function is not finished\n");

	//////////////////////////////////////////////////////////////////////
	// create initial page directory.
	kern_pgdir = (pde_t *) boot_alloc(PGSIZE);
f010149c:	b8 00 10 00 00       	mov    $0x1000,%eax
f01014a1:	e8 5f f6 ff ff       	call   f0100b05 <boot_alloc>
f01014a6:	a3 48 94 18 f0       	mov    %eax,0xf0189448
	memset(kern_pgdir, 0, PGSIZE);
f01014ab:	83 c4 0c             	add    $0xc,%esp
f01014ae:	68 00 10 00 00       	push   $0x1000
f01014b3:	6a 00                	push   $0x0
f01014b5:	50                   	push   %eax
f01014b6:	e8 88 37 00 00       	call   f0104c43 <memset>
	// a virtual page table at virtual address UVPT.
	// (For now, you don't have understand the greater purpose of the
	// following line.)

	// Permissions: kernel R, user R
	kern_pgdir[PDX(UVPT)] = PADDR(kern_pgdir) | PTE_U | PTE_P;
f01014bb:	a1 48 94 18 f0       	mov    0xf0189448,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01014c0:	83 c4 10             	add    $0x10,%esp
f01014c3:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01014c8:	77 15                	ja     f01014df <mem_init+0xaf>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01014ca:	50                   	push   %eax
f01014cb:	68 98 58 10 f0       	push   $0xf0105898
f01014d0:	68 8c 00 00 00       	push   $0x8c
f01014d5:	68 41 5f 10 f0       	push   $0xf0105f41
f01014da:	e8 6d eb ff ff       	call   f010004c <_panic>
f01014df:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f01014e5:	83 ca 05             	or     $0x5,%edx
f01014e8:	89 90 f4 0e 00 00    	mov    %edx,0xef4(%eax)
	// The kernel uses this array to keep track of physical pages: for
	// each physical page, there is a corresponding struct PageInfo in this
	// array.  'npages' is the number of physical pages in memory.  Use memset
	// to initialize all fields of each struct PageInfo to 0.
	// Your code goes here:
	pages = boot_alloc(npages * sizeof(struct PageInfo)); 
f01014ee:	a1 44 94 18 f0       	mov    0xf0189444,%eax
f01014f3:	c1 e0 03             	shl    $0x3,%eax
f01014f6:	e8 0a f6 ff ff       	call   f0100b05 <boot_alloc>
f01014fb:	a3 4c 94 18 f0       	mov    %eax,0xf018944c

	//Lab3 Exercise 1: allocate and map the envs array
	//pages = boot_alloc(npages * sizeof(struct PageInfo)); 
	envs = (struct Env *)boot_alloc(NENV * sizeof(struct Env));
f0101500:	b8 00 80 01 00       	mov    $0x18000,%eax
f0101505:	e8 fb f5 ff ff       	call   f0100b05 <boot_alloc>
f010150a:	a3 68 82 18 f0       	mov    %eax,0xf0188268

	//initialize all fields of each struct PageInfo to 0.
	memset(pages, 0, npages * sizeof(struct PageInfo)); //npages * 8 is the total number of bytes
f010150f:	83 ec 04             	sub    $0x4,%esp
f0101512:	a1 44 94 18 f0       	mov    0xf0189444,%eax
f0101517:	c1 e0 03             	shl    $0x3,%eax
f010151a:	50                   	push   %eax
f010151b:	6a 00                	push   $0x0
f010151d:	ff 35 4c 94 18 f0    	pushl  0xf018944c
f0101523:	e8 1b 37 00 00       	call   f0104c43 <memset>
	// Now that we've allocated the initial kernel data structures, we set
	// up the list of free physical pages. Once we've done so, all further
	// memory management will go through the page_* functions. In
	// particular, we can now map memory using boot_map_region
	// or page_insert
	page_init();
f0101528:	e8 34 f9 ff ff       	call   f0100e61 <page_init>

	check_page_free_list(1);
f010152d:	b8 01 00 00 00       	mov    $0x1,%eax
f0101532:	e8 7b f6 ff ff       	call   f0100bb2 <check_page_free_list>
	int nfree;
	struct PageInfo *fl;
	char *c;
	int i;

	if (!pages)
f0101537:	83 c4 10             	add    $0x10,%esp
f010153a:	83 3d 4c 94 18 f0 00 	cmpl   $0x0,0xf018944c
f0101541:	75 17                	jne    f010155a <mem_init+0x12a>
		panic("'pages' is a null pointer!");
f0101543:	83 ec 04             	sub    $0x4,%esp
f0101546:	68 e2 60 10 f0       	push   $0xf01060e2
f010154b:	68 8a 03 00 00       	push   $0x38a
f0101550:	68 41 5f 10 f0       	push   $0xf0105f41
f0101555:	e8 f2 ea ff ff       	call   f010004c <_panic>

	// check number of free pages
	for (pp = page_free_list, nfree = 0; pp; pp = pp->pp_link)
f010155a:	a1 60 82 18 f0       	mov    0xf0188260,%eax
f010155f:	bb 00 00 00 00       	mov    $0x0,%ebx
f0101564:	eb 03                	jmp    f0101569 <mem_init+0x139>
		++nfree;
f0101566:	43                   	inc    %ebx

	if (!pages)
		panic("'pages' is a null pointer!");

	// check number of free pages
	for (pp = page_free_list, nfree = 0; pp; pp = pp->pp_link)
f0101567:	8b 00                	mov    (%eax),%eax
f0101569:	85 c0                	test   %eax,%eax
f010156b:	75 f9                	jne    f0101566 <mem_init+0x136>
		++nfree;

	// should be able to allocate three pages
	pp0 = pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f010156d:	83 ec 0c             	sub    $0xc,%esp
f0101570:	6a 00                	push   $0x0
f0101572:	e8 7a fa ff ff       	call   f0100ff1 <page_alloc>
f0101577:	89 c7                	mov    %eax,%edi
f0101579:	83 c4 10             	add    $0x10,%esp
f010157c:	85 c0                	test   %eax,%eax
f010157e:	75 19                	jne    f0101599 <mem_init+0x169>
f0101580:	68 fd 60 10 f0       	push   $0xf01060fd
f0101585:	68 d8 50 10 f0       	push   $0xf01050d8
f010158a:	68 92 03 00 00       	push   $0x392
f010158f:	68 41 5f 10 f0       	push   $0xf0105f41
f0101594:	e8 b3 ea ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f0101599:	83 ec 0c             	sub    $0xc,%esp
f010159c:	6a 00                	push   $0x0
f010159e:	e8 4e fa ff ff       	call   f0100ff1 <page_alloc>
f01015a3:	89 c6                	mov    %eax,%esi
f01015a5:	83 c4 10             	add    $0x10,%esp
f01015a8:	85 c0                	test   %eax,%eax
f01015aa:	75 19                	jne    f01015c5 <mem_init+0x195>
f01015ac:	68 13 61 10 f0       	push   $0xf0106113
f01015b1:	68 d8 50 10 f0       	push   $0xf01050d8
f01015b6:	68 93 03 00 00       	push   $0x393
f01015bb:	68 41 5f 10 f0       	push   $0xf0105f41
f01015c0:	e8 87 ea ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f01015c5:	83 ec 0c             	sub    $0xc,%esp
f01015c8:	6a 00                	push   $0x0
f01015ca:	e8 22 fa ff ff       	call   f0100ff1 <page_alloc>
f01015cf:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f01015d2:	83 c4 10             	add    $0x10,%esp
f01015d5:	85 c0                	test   %eax,%eax
f01015d7:	75 19                	jne    f01015f2 <mem_init+0x1c2>
f01015d9:	68 29 61 10 f0       	push   $0xf0106129
f01015de:	68 d8 50 10 f0       	push   $0xf01050d8
f01015e3:	68 94 03 00 00       	push   $0x394
f01015e8:	68 41 5f 10 f0       	push   $0xf0105f41
f01015ed:	e8 5a ea ff ff       	call   f010004c <_panic>

	assert(pp0);
	assert(pp1 && pp1 != pp0);
f01015f2:	39 f7                	cmp    %esi,%edi
f01015f4:	75 19                	jne    f010160f <mem_init+0x1df>
f01015f6:	68 3f 61 10 f0       	push   $0xf010613f
f01015fb:	68 d8 50 10 f0       	push   $0xf01050d8
f0101600:	68 97 03 00 00       	push   $0x397
f0101605:	68 41 5f 10 f0       	push   $0xf0105f41
f010160a:	e8 3d ea ff ff       	call   f010004c <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f010160f:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0101612:	39 c6                	cmp    %eax,%esi
f0101614:	74 04                	je     f010161a <mem_init+0x1ea>
f0101616:	39 c7                	cmp    %eax,%edi
f0101618:	75 19                	jne    f0101633 <mem_init+0x203>
f010161a:	68 bc 58 10 f0       	push   $0xf01058bc
f010161f:	68 d8 50 10 f0       	push   $0xf01050d8
f0101624:	68 98 03 00 00       	push   $0x398
f0101629:	68 41 5f 10 f0       	push   $0xf0105f41
f010162e:	e8 19 ea ff ff       	call   f010004c <_panic>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101633:	8b 0d 4c 94 18 f0    	mov    0xf018944c,%ecx
	assert(page2pa(pp0) < npages*PGSIZE);
f0101639:	8b 15 44 94 18 f0    	mov    0xf0189444,%edx
f010163f:	c1 e2 0c             	shl    $0xc,%edx
f0101642:	89 f8                	mov    %edi,%eax
f0101644:	29 c8                	sub    %ecx,%eax
f0101646:	c1 f8 03             	sar    $0x3,%eax
f0101649:	c1 e0 0c             	shl    $0xc,%eax
f010164c:	39 d0                	cmp    %edx,%eax
f010164e:	72 19                	jb     f0101669 <mem_init+0x239>
f0101650:	68 51 61 10 f0       	push   $0xf0106151
f0101655:	68 d8 50 10 f0       	push   $0xf01050d8
f010165a:	68 99 03 00 00       	push   $0x399
f010165f:	68 41 5f 10 f0       	push   $0xf0105f41
f0101664:	e8 e3 e9 ff ff       	call   f010004c <_panic>
	assert(page2pa(pp1) < npages*PGSIZE);
f0101669:	89 f0                	mov    %esi,%eax
f010166b:	29 c8                	sub    %ecx,%eax
f010166d:	c1 f8 03             	sar    $0x3,%eax
f0101670:	c1 e0 0c             	shl    $0xc,%eax
f0101673:	39 c2                	cmp    %eax,%edx
f0101675:	77 19                	ja     f0101690 <mem_init+0x260>
f0101677:	68 6e 61 10 f0       	push   $0xf010616e
f010167c:	68 d8 50 10 f0       	push   $0xf01050d8
f0101681:	68 9a 03 00 00       	push   $0x39a
f0101686:	68 41 5f 10 f0       	push   $0xf0105f41
f010168b:	e8 bc e9 ff ff       	call   f010004c <_panic>
	assert(page2pa(pp2) < npages*PGSIZE);
f0101690:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0101693:	29 c8                	sub    %ecx,%eax
f0101695:	c1 f8 03             	sar    $0x3,%eax
f0101698:	c1 e0 0c             	shl    $0xc,%eax
f010169b:	39 c2                	cmp    %eax,%edx
f010169d:	77 19                	ja     f01016b8 <mem_init+0x288>
f010169f:	68 8b 61 10 f0       	push   $0xf010618b
f01016a4:	68 d8 50 10 f0       	push   $0xf01050d8
f01016a9:	68 9b 03 00 00       	push   $0x39b
f01016ae:	68 41 5f 10 f0       	push   $0xf0105f41
f01016b3:	e8 94 e9 ff ff       	call   f010004c <_panic>

	// temporarily steal the rest of the free pages
	fl = page_free_list;
f01016b8:	a1 60 82 18 f0       	mov    0xf0188260,%eax
f01016bd:	89 45 d0             	mov    %eax,-0x30(%ebp)
	page_free_list = 0;
f01016c0:	c7 05 60 82 18 f0 00 	movl   $0x0,0xf0188260
f01016c7:	00 00 00 

	// should be no free memory
	assert(!page_alloc(0));
f01016ca:	83 ec 0c             	sub    $0xc,%esp
f01016cd:	6a 00                	push   $0x0
f01016cf:	e8 1d f9 ff ff       	call   f0100ff1 <page_alloc>
f01016d4:	83 c4 10             	add    $0x10,%esp
f01016d7:	85 c0                	test   %eax,%eax
f01016d9:	74 19                	je     f01016f4 <mem_init+0x2c4>
f01016db:	68 a8 61 10 f0       	push   $0xf01061a8
f01016e0:	68 d8 50 10 f0       	push   $0xf01050d8
f01016e5:	68 a2 03 00 00       	push   $0x3a2
f01016ea:	68 41 5f 10 f0       	push   $0xf0105f41
f01016ef:	e8 58 e9 ff ff       	call   f010004c <_panic>

	// free and re-allocate?
	page_free(pp0);
f01016f4:	83 ec 0c             	sub    $0xc,%esp
f01016f7:	57                   	push   %edi
f01016f8:	e8 6a f9 ff ff       	call   f0101067 <page_free>
	page_free(pp1);
f01016fd:	89 34 24             	mov    %esi,(%esp)
f0101700:	e8 62 f9 ff ff       	call   f0101067 <page_free>
	page_free(pp2);
f0101705:	83 c4 04             	add    $0x4,%esp
f0101708:	ff 75 d4             	pushl  -0x2c(%ebp)
f010170b:	e8 57 f9 ff ff       	call   f0101067 <page_free>
	pp0 = pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f0101710:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
f0101717:	e8 d5 f8 ff ff       	call   f0100ff1 <page_alloc>
f010171c:	89 c6                	mov    %eax,%esi
f010171e:	83 c4 10             	add    $0x10,%esp
f0101721:	85 c0                	test   %eax,%eax
f0101723:	75 19                	jne    f010173e <mem_init+0x30e>
f0101725:	68 fd 60 10 f0       	push   $0xf01060fd
f010172a:	68 d8 50 10 f0       	push   $0xf01050d8
f010172f:	68 a9 03 00 00       	push   $0x3a9
f0101734:	68 41 5f 10 f0       	push   $0xf0105f41
f0101739:	e8 0e e9 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f010173e:	83 ec 0c             	sub    $0xc,%esp
f0101741:	6a 00                	push   $0x0
f0101743:	e8 a9 f8 ff ff       	call   f0100ff1 <page_alloc>
f0101748:	89 c7                	mov    %eax,%edi
f010174a:	83 c4 10             	add    $0x10,%esp
f010174d:	85 c0                	test   %eax,%eax
f010174f:	75 19                	jne    f010176a <mem_init+0x33a>
f0101751:	68 13 61 10 f0       	push   $0xf0106113
f0101756:	68 d8 50 10 f0       	push   $0xf01050d8
f010175b:	68 aa 03 00 00       	push   $0x3aa
f0101760:	68 41 5f 10 f0       	push   $0xf0105f41
f0101765:	e8 e2 e8 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f010176a:	83 ec 0c             	sub    $0xc,%esp
f010176d:	6a 00                	push   $0x0
f010176f:	e8 7d f8 ff ff       	call   f0100ff1 <page_alloc>
f0101774:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f0101777:	83 c4 10             	add    $0x10,%esp
f010177a:	85 c0                	test   %eax,%eax
f010177c:	75 19                	jne    f0101797 <mem_init+0x367>
f010177e:	68 29 61 10 f0       	push   $0xf0106129
f0101783:	68 d8 50 10 f0       	push   $0xf01050d8
f0101788:	68 ab 03 00 00       	push   $0x3ab
f010178d:	68 41 5f 10 f0       	push   $0xf0105f41
f0101792:	e8 b5 e8 ff ff       	call   f010004c <_panic>
	assert(pp0);
	assert(pp1 && pp1 != pp0);
f0101797:	39 fe                	cmp    %edi,%esi
f0101799:	75 19                	jne    f01017b4 <mem_init+0x384>
f010179b:	68 3f 61 10 f0       	push   $0xf010613f
f01017a0:	68 d8 50 10 f0       	push   $0xf01050d8
f01017a5:	68 ad 03 00 00       	push   $0x3ad
f01017aa:	68 41 5f 10 f0       	push   $0xf0105f41
f01017af:	e8 98 e8 ff ff       	call   f010004c <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f01017b4:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01017b7:	39 c7                	cmp    %eax,%edi
f01017b9:	74 04                	je     f01017bf <mem_init+0x38f>
f01017bb:	39 c6                	cmp    %eax,%esi
f01017bd:	75 19                	jne    f01017d8 <mem_init+0x3a8>
f01017bf:	68 bc 58 10 f0       	push   $0xf01058bc
f01017c4:	68 d8 50 10 f0       	push   $0xf01050d8
f01017c9:	68 ae 03 00 00       	push   $0x3ae
f01017ce:	68 41 5f 10 f0       	push   $0xf0105f41
f01017d3:	e8 74 e8 ff ff       	call   f010004c <_panic>
	assert(!page_alloc(0));
f01017d8:	83 ec 0c             	sub    $0xc,%esp
f01017db:	6a 00                	push   $0x0
f01017dd:	e8 0f f8 ff ff       	call   f0100ff1 <page_alloc>
f01017e2:	83 c4 10             	add    $0x10,%esp
f01017e5:	85 c0                	test   %eax,%eax
f01017e7:	74 19                	je     f0101802 <mem_init+0x3d2>
f01017e9:	68 a8 61 10 f0       	push   $0xf01061a8
f01017ee:	68 d8 50 10 f0       	push   $0xf01050d8
f01017f3:	68 af 03 00 00       	push   $0x3af
f01017f8:	68 41 5f 10 f0       	push   $0xf0105f41
f01017fd:	e8 4a e8 ff ff       	call   f010004c <_panic>
f0101802:	89 f0                	mov    %esi,%eax
f0101804:	2b 05 4c 94 18 f0    	sub    0xf018944c,%eax
f010180a:	c1 f8 03             	sar    $0x3,%eax
f010180d:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101810:	89 c2                	mov    %eax,%edx
f0101812:	c1 ea 0c             	shr    $0xc,%edx
f0101815:	3b 15 44 94 18 f0    	cmp    0xf0189444,%edx
f010181b:	72 12                	jb     f010182f <mem_init+0x3ff>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f010181d:	50                   	push   %eax
f010181e:	68 50 57 10 f0       	push   $0xf0105750
f0101823:	6a 56                	push   $0x56
f0101825:	68 4d 5f 10 f0       	push   $0xf0105f4d
f010182a:	e8 1d e8 ff ff       	call   f010004c <_panic>

	// test flags
	memset(page2kva(pp0), 1, PGSIZE);
f010182f:	83 ec 04             	sub    $0x4,%esp
f0101832:	68 00 10 00 00       	push   $0x1000
f0101837:	6a 01                	push   $0x1
f0101839:	2d 00 00 00 10       	sub    $0x10000000,%eax
f010183e:	50                   	push   %eax
f010183f:	e8 ff 33 00 00       	call   f0104c43 <memset>
	page_free(pp0);
f0101844:	89 34 24             	mov    %esi,(%esp)
f0101847:	e8 1b f8 ff ff       	call   f0101067 <page_free>
	assert((pp = page_alloc(ALLOC_ZERO)));
f010184c:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
f0101853:	e8 99 f7 ff ff       	call   f0100ff1 <page_alloc>
f0101858:	83 c4 10             	add    $0x10,%esp
f010185b:	85 c0                	test   %eax,%eax
f010185d:	75 19                	jne    f0101878 <mem_init+0x448>
f010185f:	68 b7 61 10 f0       	push   $0xf01061b7
f0101864:	68 d8 50 10 f0       	push   $0xf01050d8
f0101869:	68 b4 03 00 00       	push   $0x3b4
f010186e:	68 41 5f 10 f0       	push   $0xf0105f41
f0101873:	e8 d4 e7 ff ff       	call   f010004c <_panic>
	assert(pp && pp0 == pp);
f0101878:	39 c6                	cmp    %eax,%esi
f010187a:	74 19                	je     f0101895 <mem_init+0x465>
f010187c:	68 d5 61 10 f0       	push   $0xf01061d5
f0101881:	68 d8 50 10 f0       	push   $0xf01050d8
f0101886:	68 b5 03 00 00       	push   $0x3b5
f010188b:	68 41 5f 10 f0       	push   $0xf0105f41
f0101890:	e8 b7 e7 ff ff       	call   f010004c <_panic>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101895:	89 f0                	mov    %esi,%eax
f0101897:	2b 05 4c 94 18 f0    	sub    0xf018944c,%eax
f010189d:	c1 f8 03             	sar    $0x3,%eax
f01018a0:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01018a3:	89 c2                	mov    %eax,%edx
f01018a5:	c1 ea 0c             	shr    $0xc,%edx
f01018a8:	3b 15 44 94 18 f0    	cmp    0xf0189444,%edx
f01018ae:	72 12                	jb     f01018c2 <mem_init+0x492>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01018b0:	50                   	push   %eax
f01018b1:	68 50 57 10 f0       	push   $0xf0105750
f01018b6:	6a 56                	push   $0x56
f01018b8:	68 4d 5f 10 f0       	push   $0xf0105f4d
f01018bd:	e8 8a e7 ff ff       	call   f010004c <_panic>
f01018c2:	8d 90 00 10 00 f0    	lea    -0xffff000(%eax),%edx
	return (void *)(pa + KERNBASE);
f01018c8:	8d 80 00 00 00 f0    	lea    -0x10000000(%eax),%eax
	c = page2kva(pp);
	for (i = 0; i < PGSIZE; i++)
		assert(c[i] == 0);
f01018ce:	80 38 00             	cmpb   $0x0,(%eax)
f01018d1:	74 19                	je     f01018ec <mem_init+0x4bc>
f01018d3:	68 e5 61 10 f0       	push   $0xf01061e5
f01018d8:	68 d8 50 10 f0       	push   $0xf01050d8
f01018dd:	68 b8 03 00 00       	push   $0x3b8
f01018e2:	68 41 5f 10 f0       	push   $0xf0105f41
f01018e7:	e8 60 e7 ff ff       	call   f010004c <_panic>
f01018ec:	40                   	inc    %eax
	memset(page2kva(pp0), 1, PGSIZE);
	page_free(pp0);
	assert((pp = page_alloc(ALLOC_ZERO)));
	assert(pp && pp0 == pp);
	c = page2kva(pp);
	for (i = 0; i < PGSIZE; i++)
f01018ed:	39 d0                	cmp    %edx,%eax
f01018ef:	75 dd                	jne    f01018ce <mem_init+0x49e>
		assert(c[i] == 0);

	// give free list back
	page_free_list = fl;
f01018f1:	8b 45 d0             	mov    -0x30(%ebp),%eax
f01018f4:	a3 60 82 18 f0       	mov    %eax,0xf0188260

	// free the pages we took
	page_free(pp0);
f01018f9:	83 ec 0c             	sub    $0xc,%esp
f01018fc:	56                   	push   %esi
f01018fd:	e8 65 f7 ff ff       	call   f0101067 <page_free>
	page_free(pp1);
f0101902:	89 3c 24             	mov    %edi,(%esp)
f0101905:	e8 5d f7 ff ff       	call   f0101067 <page_free>
	page_free(pp2);
f010190a:	83 c4 04             	add    $0x4,%esp
f010190d:	ff 75 d4             	pushl  -0x2c(%ebp)
f0101910:	e8 52 f7 ff ff       	call   f0101067 <page_free>

	// number of free pages should be the same
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0101915:	a1 60 82 18 f0       	mov    0xf0188260,%eax
f010191a:	83 c4 10             	add    $0x10,%esp
f010191d:	eb 03                	jmp    f0101922 <mem_init+0x4f2>
		--nfree;
f010191f:	4b                   	dec    %ebx
	page_free(pp0);
	page_free(pp1);
	page_free(pp2);

	// number of free pages should be the same
	for (pp = page_free_list; pp; pp = pp->pp_link)
f0101920:	8b 00                	mov    (%eax),%eax
f0101922:	85 c0                	test   %eax,%eax
f0101924:	75 f9                	jne    f010191f <mem_init+0x4ef>
		--nfree;
	assert(nfree == 0);
f0101926:	85 db                	test   %ebx,%ebx
f0101928:	74 19                	je     f0101943 <mem_init+0x513>
f010192a:	68 ef 61 10 f0       	push   $0xf01061ef
f010192f:	68 d8 50 10 f0       	push   $0xf01050d8
f0101934:	68 c5 03 00 00       	push   $0x3c5
f0101939:	68 41 5f 10 f0       	push   $0xf0105f41
f010193e:	e8 09 e7 ff ff       	call   f010004c <_panic>

	cprintf("check_page_alloc() succeeded!\n");
f0101943:	83 ec 0c             	sub    $0xc,%esp
f0101946:	68 dc 58 10 f0       	push   $0xf01058dc
f010194b:	e8 41 1a 00 00       	call   f0103391 <cprintf>
	int i;
	extern pde_t entry_pgdir[];

	// should be able to allocate three pages
	pp0 = pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f0101950:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
f0101957:	e8 95 f6 ff ff       	call   f0100ff1 <page_alloc>
f010195c:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f010195f:	83 c4 10             	add    $0x10,%esp
f0101962:	85 c0                	test   %eax,%eax
f0101964:	75 19                	jne    f010197f <mem_init+0x54f>
f0101966:	68 fd 60 10 f0       	push   $0xf01060fd
f010196b:	68 d8 50 10 f0       	push   $0xf01050d8
f0101970:	68 34 04 00 00       	push   $0x434
f0101975:	68 41 5f 10 f0       	push   $0xf0105f41
f010197a:	e8 cd e6 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f010197f:	83 ec 0c             	sub    $0xc,%esp
f0101982:	6a 00                	push   $0x0
f0101984:	e8 68 f6 ff ff       	call   f0100ff1 <page_alloc>
f0101989:	89 c6                	mov    %eax,%esi
f010198b:	83 c4 10             	add    $0x10,%esp
f010198e:	85 c0                	test   %eax,%eax
f0101990:	75 19                	jne    f01019ab <mem_init+0x57b>
f0101992:	68 13 61 10 f0       	push   $0xf0106113
f0101997:	68 d8 50 10 f0       	push   $0xf01050d8
f010199c:	68 35 04 00 00       	push   $0x435
f01019a1:	68 41 5f 10 f0       	push   $0xf0105f41
f01019a6:	e8 a1 e6 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f01019ab:	83 ec 0c             	sub    $0xc,%esp
f01019ae:	6a 00                	push   $0x0
f01019b0:	e8 3c f6 ff ff       	call   f0100ff1 <page_alloc>
f01019b5:	89 c3                	mov    %eax,%ebx
f01019b7:	83 c4 10             	add    $0x10,%esp
f01019ba:	85 c0                	test   %eax,%eax
f01019bc:	75 19                	jne    f01019d7 <mem_init+0x5a7>
f01019be:	68 29 61 10 f0       	push   $0xf0106129
f01019c3:	68 d8 50 10 f0       	push   $0xf01050d8
f01019c8:	68 36 04 00 00       	push   $0x436
f01019cd:	68 41 5f 10 f0       	push   $0xf0105f41
f01019d2:	e8 75 e6 ff ff       	call   f010004c <_panic>

	assert(pp0);
	assert(pp1 && pp1 != pp0);
f01019d7:	39 75 d4             	cmp    %esi,-0x2c(%ebp)
f01019da:	75 19                	jne    f01019f5 <mem_init+0x5c5>
f01019dc:	68 3f 61 10 f0       	push   $0xf010613f
f01019e1:	68 d8 50 10 f0       	push   $0xf01050d8
f01019e6:	68 39 04 00 00       	push   $0x439
f01019eb:	68 41 5f 10 f0       	push   $0xf0105f41
f01019f0:	e8 57 e6 ff ff       	call   f010004c <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f01019f5:	39 c6                	cmp    %eax,%esi
f01019f7:	74 05                	je     f01019fe <mem_init+0x5ce>
f01019f9:	39 45 d4             	cmp    %eax,-0x2c(%ebp)
f01019fc:	75 19                	jne    f0101a17 <mem_init+0x5e7>
f01019fe:	68 bc 58 10 f0       	push   $0xf01058bc
f0101a03:	68 d8 50 10 f0       	push   $0xf01050d8
f0101a08:	68 3a 04 00 00       	push   $0x43a
f0101a0d:	68 41 5f 10 f0       	push   $0xf0105f41
f0101a12:	e8 35 e6 ff ff       	call   f010004c <_panic>

	// temporarily steal the rest of the free pages
	fl = page_free_list;
f0101a17:	a1 60 82 18 f0       	mov    0xf0188260,%eax
f0101a1c:	89 45 d0             	mov    %eax,-0x30(%ebp)

	page_free_list = 0;
f0101a1f:	c7 05 60 82 18 f0 00 	movl   $0x0,0xf0188260
f0101a26:	00 00 00 
	//print_fl();
	// should be no free memory
	assert(!page_alloc(0));
f0101a29:	83 ec 0c             	sub    $0xc,%esp
f0101a2c:	6a 00                	push   $0x0
f0101a2e:	e8 be f5 ff ff       	call   f0100ff1 <page_alloc>
f0101a33:	83 c4 10             	add    $0x10,%esp
f0101a36:	85 c0                	test   %eax,%eax
f0101a38:	74 19                	je     f0101a53 <mem_init+0x623>
f0101a3a:	68 a8 61 10 f0       	push   $0xf01061a8
f0101a3f:	68 d8 50 10 f0       	push   $0xf01050d8
f0101a44:	68 42 04 00 00       	push   $0x442
f0101a49:	68 41 5f 10 f0       	push   $0xf0105f41
f0101a4e:	e8 f9 e5 ff ff       	call   f010004c <_panic>
	//print_fl();
	// there is no page allocated at address 0
	assert(page_lookup(kern_pgdir, (void *) 0x0, &ptep) == NULL);
f0101a53:	83 ec 04             	sub    $0x4,%esp
f0101a56:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0101a59:	50                   	push   %eax
f0101a5a:	6a 00                	push   $0x0
f0101a5c:	ff 35 48 94 18 f0    	pushl  0xf0189448
f0101a62:	e8 41 f8 ff ff       	call   f01012a8 <page_lookup>
f0101a67:	83 c4 10             	add    $0x10,%esp
f0101a6a:	85 c0                	test   %eax,%eax
f0101a6c:	74 19                	je     f0101a87 <mem_init+0x657>
f0101a6e:	68 fc 58 10 f0       	push   $0xf01058fc
f0101a73:	68 d8 50 10 f0       	push   $0xf01050d8
f0101a78:	68 45 04 00 00       	push   $0x445
f0101a7d:	68 41 5f 10 f0       	push   $0xf0105f41
f0101a82:	e8 c5 e5 ff ff       	call   f010004c <_panic>
	//cprintf("H1\n");
	// there is no free memory, so we can't allocate a page table
	assert(page_insert(kern_pgdir, pp1, 0x0, PTE_W) < 0); //insert fail
f0101a87:	6a 02                	push   $0x2
f0101a89:	6a 00                	push   $0x0
f0101a8b:	56                   	push   %esi
f0101a8c:	ff 35 48 94 18 f0    	pushl  0xf0189448
f0101a92:	e8 33 f9 ff ff       	call   f01013ca <page_insert>
f0101a97:	83 c4 10             	add    $0x10,%esp
f0101a9a:	85 c0                	test   %eax,%eax
f0101a9c:	78 19                	js     f0101ab7 <mem_init+0x687>
f0101a9e:	68 34 59 10 f0       	push   $0xf0105934
f0101aa3:	68 d8 50 10 f0       	push   $0xf01050d8
f0101aa8:	68 48 04 00 00       	push   $0x448
f0101aad:	68 41 5f 10 f0       	push   $0xf0105f41
f0101ab2:	e8 95 e5 ff ff       	call   f010004c <_panic>
	//print_fl();
	//cprintf("H2\n");
	// free pp0 and try again: pp0 should be used for page table
	page_free(pp0);
f0101ab7:	83 ec 0c             	sub    $0xc,%esp
f0101aba:	ff 75 d4             	pushl  -0x2c(%ebp)
f0101abd:	e8 a5 f5 ff ff       	call   f0101067 <page_free>
	//cprintf("H3\n");
	
	assert(page_insert(kern_pgdir, pp1, 0x0, PTE_W) == 0); //insert success
f0101ac2:	6a 02                	push   $0x2
f0101ac4:	6a 00                	push   $0x0
f0101ac6:	56                   	push   %esi
f0101ac7:	ff 35 48 94 18 f0    	pushl  0xf0189448
f0101acd:	e8 f8 f8 ff ff       	call   f01013ca <page_insert>
f0101ad2:	83 c4 20             	add    $0x20,%esp
f0101ad5:	85 c0                	test   %eax,%eax
f0101ad7:	74 19                	je     f0101af2 <mem_init+0x6c2>
f0101ad9:	68 64 59 10 f0       	push   $0xf0105964
f0101ade:	68 d8 50 10 f0       	push   $0xf01050d8
f0101ae3:	68 4f 04 00 00       	push   $0x44f
f0101ae8:	68 41 5f 10 f0       	push   $0xf0105f41
f0101aed:	e8 5a e5 ff ff       	call   f010004c <_panic>
	//print_fl();
	//cprintf("H3\n");
	assert(PTE_ADDR(kern_pgdir[0]) == page2pa(pp0)); //problem
f0101af2:	8b 3d 48 94 18 f0    	mov    0xf0189448,%edi
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0101af8:	a1 4c 94 18 f0       	mov    0xf018944c,%eax
f0101afd:	89 45 cc             	mov    %eax,-0x34(%ebp)
f0101b00:	8b 17                	mov    (%edi),%edx
f0101b02:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f0101b08:	8b 4d d4             	mov    -0x2c(%ebp),%ecx
f0101b0b:	29 c1                	sub    %eax,%ecx
f0101b0d:	89 c8                	mov    %ecx,%eax
f0101b0f:	c1 f8 03             	sar    $0x3,%eax
f0101b12:	c1 e0 0c             	shl    $0xc,%eax
f0101b15:	39 c2                	cmp    %eax,%edx
f0101b17:	74 19                	je     f0101b32 <mem_init+0x702>
f0101b19:	68 94 59 10 f0       	push   $0xf0105994
f0101b1e:	68 d8 50 10 f0       	push   $0xf01050d8
f0101b23:	68 52 04 00 00       	push   $0x452
f0101b28:	68 41 5f 10 f0       	push   $0xf0105f41
f0101b2d:	e8 1a e5 ff ff       	call   f010004c <_panic>
	//cprintf("H4\n");
	//cprintf("Address returned by check_va2pa is: %x\n", check_va2pa(kern_pgdir, 0x0));
	assert(check_va2pa(kern_pgdir, 0x0) == page2pa(pp1)); //problem
f0101b32:	ba 00 00 00 00       	mov    $0x0,%edx
f0101b37:	89 f8                	mov    %edi,%eax
f0101b39:	e8 15 f0 ff ff       	call   f0100b53 <check_va2pa>
f0101b3e:	89 f2                	mov    %esi,%edx
f0101b40:	2b 55 cc             	sub    -0x34(%ebp),%edx
f0101b43:	c1 fa 03             	sar    $0x3,%edx
f0101b46:	c1 e2 0c             	shl    $0xc,%edx
f0101b49:	39 d0                	cmp    %edx,%eax
f0101b4b:	74 19                	je     f0101b66 <mem_init+0x736>
f0101b4d:	68 bc 59 10 f0       	push   $0xf01059bc
f0101b52:	68 d8 50 10 f0       	push   $0xf01050d8
f0101b57:	68 55 04 00 00       	push   $0x455
f0101b5c:	68 41 5f 10 f0       	push   $0xf0105f41
f0101b61:	e8 e6 e4 ff ff       	call   f010004c <_panic>
	//cprintf("H5\n");
	assert(pp1->pp_ref == 1);
f0101b66:	66 83 7e 04 01       	cmpw   $0x1,0x4(%esi)
f0101b6b:	74 19                	je     f0101b86 <mem_init+0x756>
f0101b6d:	68 fa 61 10 f0       	push   $0xf01061fa
f0101b72:	68 d8 50 10 f0       	push   $0xf01050d8
f0101b77:	68 57 04 00 00       	push   $0x457
f0101b7c:	68 41 5f 10 f0       	push   $0xf0105f41
f0101b81:	e8 c6 e4 ff ff       	call   f010004c <_panic>
	assert(pp0->pp_ref == 1);
f0101b86:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0101b89:	66 83 78 04 01       	cmpw   $0x1,0x4(%eax)
f0101b8e:	74 19                	je     f0101ba9 <mem_init+0x779>
f0101b90:	68 0b 62 10 f0       	push   $0xf010620b
f0101b95:	68 d8 50 10 f0       	push   $0xf01050d8
f0101b9a:	68 58 04 00 00       	push   $0x458
f0101b9f:	68 41 5f 10 f0       	push   $0xf0105f41
f0101ba4:	e8 a3 e4 ff ff       	call   f010004c <_panic>

	//print_fl();
	// should be able to map pp2 at PGSIZE because pp0 is already allocated for page table
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W) == 0);
f0101ba9:	6a 02                	push   $0x2
f0101bab:	68 00 10 00 00       	push   $0x1000
f0101bb0:	53                   	push   %ebx
f0101bb1:	57                   	push   %edi
f0101bb2:	e8 13 f8 ff ff       	call   f01013ca <page_insert>
f0101bb7:	83 c4 10             	add    $0x10,%esp
f0101bba:	85 c0                	test   %eax,%eax
f0101bbc:	74 19                	je     f0101bd7 <mem_init+0x7a7>
f0101bbe:	68 ec 59 10 f0       	push   $0xf01059ec
f0101bc3:	68 d8 50 10 f0       	push   $0xf01050d8
f0101bc8:	68 5c 04 00 00       	push   $0x45c
f0101bcd:	68 41 5f 10 f0       	push   $0xf0105f41
f0101bd2:	e8 75 e4 ff ff       	call   f010004c <_panic>
	//print_fl();
	//cprintf("Address returned by check_va2pa is: %x\n", check_va2pa(kern_pgdir, PGSIZE));
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp2));
f0101bd7:	ba 00 10 00 00       	mov    $0x1000,%edx
f0101bdc:	a1 48 94 18 f0       	mov    0xf0189448,%eax
f0101be1:	e8 6d ef ff ff       	call   f0100b53 <check_va2pa>
f0101be6:	89 da                	mov    %ebx,%edx
f0101be8:	2b 15 4c 94 18 f0    	sub    0xf018944c,%edx
f0101bee:	c1 fa 03             	sar    $0x3,%edx
f0101bf1:	c1 e2 0c             	shl    $0xc,%edx
f0101bf4:	39 d0                	cmp    %edx,%eax
f0101bf6:	74 19                	je     f0101c11 <mem_init+0x7e1>
f0101bf8:	68 28 5a 10 f0       	push   $0xf0105a28
f0101bfd:	68 d8 50 10 f0       	push   $0xf01050d8
f0101c02:	68 5f 04 00 00       	push   $0x45f
f0101c07:	68 41 5f 10 f0       	push   $0xf0105f41
f0101c0c:	e8 3b e4 ff ff       	call   f010004c <_panic>
	//cprintf("H6\n");
	assert(pp2->pp_ref == 1);
f0101c11:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f0101c16:	74 19                	je     f0101c31 <mem_init+0x801>
f0101c18:	68 1c 62 10 f0       	push   $0xf010621c
f0101c1d:	68 d8 50 10 f0       	push   $0xf01050d8
f0101c22:	68 61 04 00 00       	push   $0x461
f0101c27:	68 41 5f 10 f0       	push   $0xf0105f41
f0101c2c:	e8 1b e4 ff ff       	call   f010004c <_panic>

	// should be no free memory
	//print_fl();
	assert(!page_alloc(0));
f0101c31:	83 ec 0c             	sub    $0xc,%esp
f0101c34:	6a 00                	push   $0x0
f0101c36:	e8 b6 f3 ff ff       	call   f0100ff1 <page_alloc>
f0101c3b:	83 c4 10             	add    $0x10,%esp
f0101c3e:	85 c0                	test   %eax,%eax
f0101c40:	74 19                	je     f0101c5b <mem_init+0x82b>
f0101c42:	68 a8 61 10 f0       	push   $0xf01061a8
f0101c47:	68 d8 50 10 f0       	push   $0xf01050d8
f0101c4c:	68 65 04 00 00       	push   $0x465
f0101c51:	68 41 5f 10 f0       	push   $0xf0105f41
f0101c56:	e8 f1 e3 ff ff       	call   f010004c <_panic>
	//print_fl();
	// should be able to map pp2 at PGSIZE because it's already there
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W) == 0);
f0101c5b:	6a 02                	push   $0x2
f0101c5d:	68 00 10 00 00       	push   $0x1000
f0101c62:	53                   	push   %ebx
f0101c63:	ff 35 48 94 18 f0    	pushl  0xf0189448
f0101c69:	e8 5c f7 ff ff       	call   f01013ca <page_insert>
f0101c6e:	83 c4 10             	add    $0x10,%esp
f0101c71:	85 c0                	test   %eax,%eax
f0101c73:	74 19                	je     f0101c8e <mem_init+0x85e>
f0101c75:	68 ec 59 10 f0       	push   $0xf01059ec
f0101c7a:	68 d8 50 10 f0       	push   $0xf01050d8
f0101c7f:	68 68 04 00 00       	push   $0x468
f0101c84:	68 41 5f 10 f0       	push   $0xf0105f41
f0101c89:	e8 be e3 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp2));
f0101c8e:	ba 00 10 00 00       	mov    $0x1000,%edx
f0101c93:	a1 48 94 18 f0       	mov    0xf0189448,%eax
f0101c98:	e8 b6 ee ff ff       	call   f0100b53 <check_va2pa>
f0101c9d:	89 da                	mov    %ebx,%edx
f0101c9f:	2b 15 4c 94 18 f0    	sub    0xf018944c,%edx
f0101ca5:	c1 fa 03             	sar    $0x3,%edx
f0101ca8:	c1 e2 0c             	shl    $0xc,%edx
f0101cab:	39 d0                	cmp    %edx,%eax
f0101cad:	74 19                	je     f0101cc8 <mem_init+0x898>
f0101caf:	68 28 5a 10 f0       	push   $0xf0105a28
f0101cb4:	68 d8 50 10 f0       	push   $0xf01050d8
f0101cb9:	68 69 04 00 00       	push   $0x469
f0101cbe:	68 41 5f 10 f0       	push   $0xf0105f41
f0101cc3:	e8 84 e3 ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 1);
f0101cc8:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f0101ccd:	74 19                	je     f0101ce8 <mem_init+0x8b8>
f0101ccf:	68 1c 62 10 f0       	push   $0xf010621c
f0101cd4:	68 d8 50 10 f0       	push   $0xf01050d8
f0101cd9:	68 6a 04 00 00       	push   $0x46a
f0101cde:	68 41 5f 10 f0       	push   $0xf0105f41
f0101ce3:	e8 64 e3 ff ff       	call   f010004c <_panic>
	//print_fl();
	// pp2 should NOT be on the free list
	// could happen in ref counts are handled sloppily in page_insert
	assert(!page_alloc(0));
f0101ce8:	83 ec 0c             	sub    $0xc,%esp
f0101ceb:	6a 00                	push   $0x0
f0101ced:	e8 ff f2 ff ff       	call   f0100ff1 <page_alloc>
f0101cf2:	83 c4 10             	add    $0x10,%esp
f0101cf5:	85 c0                	test   %eax,%eax
f0101cf7:	74 19                	je     f0101d12 <mem_init+0x8e2>
f0101cf9:	68 a8 61 10 f0       	push   $0xf01061a8
f0101cfe:	68 d8 50 10 f0       	push   $0xf01050d8
f0101d03:	68 6e 04 00 00       	push   $0x46e
f0101d08:	68 41 5f 10 f0       	push   $0xf0105f41
f0101d0d:	e8 3a e3 ff ff       	call   f010004c <_panic>

	// check that pgdir_walk returns a pointer to the pte
	ptep = (pte_t *) KADDR(PTE_ADDR(kern_pgdir[PDX(PGSIZE)]));
f0101d12:	8b 15 48 94 18 f0    	mov    0xf0189448,%edx
f0101d18:	8b 02                	mov    (%edx),%eax
f0101d1a:	25 00 f0 ff ff       	and    $0xfffff000,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0101d1f:	89 c1                	mov    %eax,%ecx
f0101d21:	c1 e9 0c             	shr    $0xc,%ecx
f0101d24:	3b 0d 44 94 18 f0    	cmp    0xf0189444,%ecx
f0101d2a:	72 15                	jb     f0101d41 <mem_init+0x911>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0101d2c:	50                   	push   %eax
f0101d2d:	68 50 57 10 f0       	push   $0xf0105750
f0101d32:	68 71 04 00 00       	push   $0x471
f0101d37:	68 41 5f 10 f0       	push   $0xf0105f41
f0101d3c:	e8 0b e3 ff ff       	call   f010004c <_panic>
f0101d41:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0101d46:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	assert(pgdir_walk(kern_pgdir, (void*)PGSIZE, 0) == ptep+PTX(PGSIZE));
f0101d49:	83 ec 04             	sub    $0x4,%esp
f0101d4c:	6a 00                	push   $0x0
f0101d4e:	68 00 10 00 00       	push   $0x1000
f0101d53:	52                   	push   %edx
f0101d54:	e8 8f f3 ff ff       	call   f01010e8 <pgdir_walk>
f0101d59:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0101d5c:	8d 57 04             	lea    0x4(%edi),%edx
f0101d5f:	83 c4 10             	add    $0x10,%esp
f0101d62:	39 d0                	cmp    %edx,%eax
f0101d64:	74 19                	je     f0101d7f <mem_init+0x94f>
f0101d66:	68 58 5a 10 f0       	push   $0xf0105a58
f0101d6b:	68 d8 50 10 f0       	push   $0xf01050d8
f0101d70:	68 72 04 00 00       	push   $0x472
f0101d75:	68 41 5f 10 f0       	push   $0xf0105f41
f0101d7a:	e8 cd e2 ff ff       	call   f010004c <_panic>

	// should be able to change permissions too.
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W|PTE_U) == 0);
f0101d7f:	6a 06                	push   $0x6
f0101d81:	68 00 10 00 00       	push   $0x1000
f0101d86:	53                   	push   %ebx
f0101d87:	ff 35 48 94 18 f0    	pushl  0xf0189448
f0101d8d:	e8 38 f6 ff ff       	call   f01013ca <page_insert>
f0101d92:	83 c4 10             	add    $0x10,%esp
f0101d95:	85 c0                	test   %eax,%eax
f0101d97:	74 19                	je     f0101db2 <mem_init+0x982>
f0101d99:	68 98 5a 10 f0       	push   $0xf0105a98
f0101d9e:	68 d8 50 10 f0       	push   $0xf01050d8
f0101da3:	68 75 04 00 00       	push   $0x475
f0101da8:	68 41 5f 10 f0       	push   $0xf0105f41
f0101dad:	e8 9a e2 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp2));
f0101db2:	8b 3d 48 94 18 f0    	mov    0xf0189448,%edi
f0101db8:	ba 00 10 00 00       	mov    $0x1000,%edx
f0101dbd:	89 f8                	mov    %edi,%eax
f0101dbf:	e8 8f ed ff ff       	call   f0100b53 <check_va2pa>
f0101dc4:	89 da                	mov    %ebx,%edx
f0101dc6:	2b 15 4c 94 18 f0    	sub    0xf018944c,%edx
f0101dcc:	c1 fa 03             	sar    $0x3,%edx
f0101dcf:	c1 e2 0c             	shl    $0xc,%edx
f0101dd2:	39 d0                	cmp    %edx,%eax
f0101dd4:	74 19                	je     f0101def <mem_init+0x9bf>
f0101dd6:	68 28 5a 10 f0       	push   $0xf0105a28
f0101ddb:	68 d8 50 10 f0       	push   $0xf01050d8
f0101de0:	68 76 04 00 00       	push   $0x476
f0101de5:	68 41 5f 10 f0       	push   $0xf0105f41
f0101dea:	e8 5d e2 ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 1);
f0101def:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f0101df4:	74 19                	je     f0101e0f <mem_init+0x9df>
f0101df6:	68 1c 62 10 f0       	push   $0xf010621c
f0101dfb:	68 d8 50 10 f0       	push   $0xf01050d8
f0101e00:	68 77 04 00 00       	push   $0x477
f0101e05:	68 41 5f 10 f0       	push   $0xf0105f41
f0101e0a:	e8 3d e2 ff ff       	call   f010004c <_panic>
	assert(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_U);
f0101e0f:	83 ec 04             	sub    $0x4,%esp
f0101e12:	6a 00                	push   $0x0
f0101e14:	68 00 10 00 00       	push   $0x1000
f0101e19:	57                   	push   %edi
f0101e1a:	e8 c9 f2 ff ff       	call   f01010e8 <pgdir_walk>
f0101e1f:	83 c4 10             	add    $0x10,%esp
f0101e22:	f6 00 04             	testb  $0x4,(%eax)
f0101e25:	75 19                	jne    f0101e40 <mem_init+0xa10>
f0101e27:	68 d8 5a 10 f0       	push   $0xf0105ad8
f0101e2c:	68 d8 50 10 f0       	push   $0xf01050d8
f0101e31:	68 78 04 00 00       	push   $0x478
f0101e36:	68 41 5f 10 f0       	push   $0xf0105f41
f0101e3b:	e8 0c e2 ff ff       	call   f010004c <_panic>
	assert(kern_pgdir[0] & PTE_U);
f0101e40:	a1 48 94 18 f0       	mov    0xf0189448,%eax
f0101e45:	f6 00 04             	testb  $0x4,(%eax)
f0101e48:	75 19                	jne    f0101e63 <mem_init+0xa33>
f0101e4a:	68 2d 62 10 f0       	push   $0xf010622d
f0101e4f:	68 d8 50 10 f0       	push   $0xf01050d8
f0101e54:	68 79 04 00 00       	push   $0x479
f0101e59:	68 41 5f 10 f0       	push   $0xf0105f41
f0101e5e:	e8 e9 e1 ff ff       	call   f010004c <_panic>

	// should be able to remap with fewer permissions
	assert(page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W) == 0);
f0101e63:	6a 02                	push   $0x2
f0101e65:	68 00 10 00 00       	push   $0x1000
f0101e6a:	53                   	push   %ebx
f0101e6b:	50                   	push   %eax
f0101e6c:	e8 59 f5 ff ff       	call   f01013ca <page_insert>
f0101e71:	83 c4 10             	add    $0x10,%esp
f0101e74:	85 c0                	test   %eax,%eax
f0101e76:	74 19                	je     f0101e91 <mem_init+0xa61>
f0101e78:	68 ec 59 10 f0       	push   $0xf01059ec
f0101e7d:	68 d8 50 10 f0       	push   $0xf01050d8
f0101e82:	68 7c 04 00 00       	push   $0x47c
f0101e87:	68 41 5f 10 f0       	push   $0xf0105f41
f0101e8c:	e8 bb e1 ff ff       	call   f010004c <_panic>
	assert(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_W);
f0101e91:	83 ec 04             	sub    $0x4,%esp
f0101e94:	6a 00                	push   $0x0
f0101e96:	68 00 10 00 00       	push   $0x1000
f0101e9b:	ff 35 48 94 18 f0    	pushl  0xf0189448
f0101ea1:	e8 42 f2 ff ff       	call   f01010e8 <pgdir_walk>
f0101ea6:	83 c4 10             	add    $0x10,%esp
f0101ea9:	f6 00 02             	testb  $0x2,(%eax)
f0101eac:	75 19                	jne    f0101ec7 <mem_init+0xa97>
f0101eae:	68 0c 5b 10 f0       	push   $0xf0105b0c
f0101eb3:	68 d8 50 10 f0       	push   $0xf01050d8
f0101eb8:	68 7d 04 00 00       	push   $0x47d
f0101ebd:	68 41 5f 10 f0       	push   $0xf0105f41
f0101ec2:	e8 85 e1 ff ff       	call   f010004c <_panic>
	assert(!(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_U));
f0101ec7:	83 ec 04             	sub    $0x4,%esp
f0101eca:	6a 00                	push   $0x0
f0101ecc:	68 00 10 00 00       	push   $0x1000
f0101ed1:	ff 35 48 94 18 f0    	pushl  0xf0189448
f0101ed7:	e8 0c f2 ff ff       	call   f01010e8 <pgdir_walk>
f0101edc:	83 c4 10             	add    $0x10,%esp
f0101edf:	f6 00 04             	testb  $0x4,(%eax)
f0101ee2:	74 19                	je     f0101efd <mem_init+0xacd>
f0101ee4:	68 40 5b 10 f0       	push   $0xf0105b40
f0101ee9:	68 d8 50 10 f0       	push   $0xf01050d8
f0101eee:	68 7e 04 00 00       	push   $0x47e
f0101ef3:	68 41 5f 10 f0       	push   $0xf0105f41
f0101ef8:	e8 4f e1 ff ff       	call   f010004c <_panic>

	// should not be able to map at PTSIZE because need free page for page table
	assert(page_insert(kern_pgdir, pp0, (void*) PTSIZE, PTE_W) < 0);
f0101efd:	6a 02                	push   $0x2
f0101eff:	68 00 00 40 00       	push   $0x400000
f0101f04:	ff 75 d4             	pushl  -0x2c(%ebp)
f0101f07:	ff 35 48 94 18 f0    	pushl  0xf0189448
f0101f0d:	e8 b8 f4 ff ff       	call   f01013ca <page_insert>
f0101f12:	83 c4 10             	add    $0x10,%esp
f0101f15:	85 c0                	test   %eax,%eax
f0101f17:	78 19                	js     f0101f32 <mem_init+0xb02>
f0101f19:	68 78 5b 10 f0       	push   $0xf0105b78
f0101f1e:	68 d8 50 10 f0       	push   $0xf01050d8
f0101f23:	68 81 04 00 00       	push   $0x481
f0101f28:	68 41 5f 10 f0       	push   $0xf0105f41
f0101f2d:	e8 1a e1 ff ff       	call   f010004c <_panic>

	// insert pp1 at PGSIZE (replacing pp2)
	assert(page_insert(kern_pgdir, pp1, (void*) PGSIZE, PTE_W) == 0);
f0101f32:	6a 02                	push   $0x2
f0101f34:	68 00 10 00 00       	push   $0x1000
f0101f39:	56                   	push   %esi
f0101f3a:	ff 35 48 94 18 f0    	pushl  0xf0189448
f0101f40:	e8 85 f4 ff ff       	call   f01013ca <page_insert>
f0101f45:	83 c4 10             	add    $0x10,%esp
f0101f48:	85 c0                	test   %eax,%eax
f0101f4a:	74 19                	je     f0101f65 <mem_init+0xb35>
f0101f4c:	68 b0 5b 10 f0       	push   $0xf0105bb0
f0101f51:	68 d8 50 10 f0       	push   $0xf01050d8
f0101f56:	68 84 04 00 00       	push   $0x484
f0101f5b:	68 41 5f 10 f0       	push   $0xf0105f41
f0101f60:	e8 e7 e0 ff ff       	call   f010004c <_panic>
	assert(!(*pgdir_walk(kern_pgdir, (void*) PGSIZE, 0) & PTE_U));
f0101f65:	83 ec 04             	sub    $0x4,%esp
f0101f68:	6a 00                	push   $0x0
f0101f6a:	68 00 10 00 00       	push   $0x1000
f0101f6f:	ff 35 48 94 18 f0    	pushl  0xf0189448
f0101f75:	e8 6e f1 ff ff       	call   f01010e8 <pgdir_walk>
f0101f7a:	83 c4 10             	add    $0x10,%esp
f0101f7d:	f6 00 04             	testb  $0x4,(%eax)
f0101f80:	74 19                	je     f0101f9b <mem_init+0xb6b>
f0101f82:	68 40 5b 10 f0       	push   $0xf0105b40
f0101f87:	68 d8 50 10 f0       	push   $0xf01050d8
f0101f8c:	68 85 04 00 00       	push   $0x485
f0101f91:	68 41 5f 10 f0       	push   $0xf0105f41
f0101f96:	e8 b1 e0 ff ff       	call   f010004c <_panic>

	// should have pp1 at both 0 and PGSIZE, pp2 nowhere, ...
	assert(check_va2pa(kern_pgdir, 0) == page2pa(pp1));
f0101f9b:	8b 3d 48 94 18 f0    	mov    0xf0189448,%edi
f0101fa1:	ba 00 00 00 00       	mov    $0x0,%edx
f0101fa6:	89 f8                	mov    %edi,%eax
f0101fa8:	e8 a6 eb ff ff       	call   f0100b53 <check_va2pa>
f0101fad:	89 c1                	mov    %eax,%ecx
f0101faf:	89 45 cc             	mov    %eax,-0x34(%ebp)
f0101fb2:	89 f0                	mov    %esi,%eax
f0101fb4:	2b 05 4c 94 18 f0    	sub    0xf018944c,%eax
f0101fba:	c1 f8 03             	sar    $0x3,%eax
f0101fbd:	c1 e0 0c             	shl    $0xc,%eax
f0101fc0:	39 c1                	cmp    %eax,%ecx
f0101fc2:	74 19                	je     f0101fdd <mem_init+0xbad>
f0101fc4:	68 ec 5b 10 f0       	push   $0xf0105bec
f0101fc9:	68 d8 50 10 f0       	push   $0xf01050d8
f0101fce:	68 88 04 00 00       	push   $0x488
f0101fd3:	68 41 5f 10 f0       	push   $0xf0105f41
f0101fd8:	e8 6f e0 ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp1));
f0101fdd:	ba 00 10 00 00       	mov    $0x1000,%edx
f0101fe2:	89 f8                	mov    %edi,%eax
f0101fe4:	e8 6a eb ff ff       	call   f0100b53 <check_va2pa>
f0101fe9:	39 45 cc             	cmp    %eax,-0x34(%ebp)
f0101fec:	74 19                	je     f0102007 <mem_init+0xbd7>
f0101fee:	68 18 5c 10 f0       	push   $0xf0105c18
f0101ff3:	68 d8 50 10 f0       	push   $0xf01050d8
f0101ff8:	68 89 04 00 00       	push   $0x489
f0101ffd:	68 41 5f 10 f0       	push   $0xf0105f41
f0102002:	e8 45 e0 ff ff       	call   f010004c <_panic>
	// ... and ref counts should reflect this
	assert(pp1->pp_ref == 2);
f0102007:	66 83 7e 04 02       	cmpw   $0x2,0x4(%esi)
f010200c:	74 19                	je     f0102027 <mem_init+0xbf7>
f010200e:	68 43 62 10 f0       	push   $0xf0106243
f0102013:	68 d8 50 10 f0       	push   $0xf01050d8
f0102018:	68 8b 04 00 00       	push   $0x48b
f010201d:	68 41 5f 10 f0       	push   $0xf0105f41
f0102022:	e8 25 e0 ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 0);
f0102027:	66 83 7b 04 00       	cmpw   $0x0,0x4(%ebx)
f010202c:	74 19                	je     f0102047 <mem_init+0xc17>
f010202e:	68 54 62 10 f0       	push   $0xf0106254
f0102033:	68 d8 50 10 f0       	push   $0xf01050d8
f0102038:	68 8c 04 00 00       	push   $0x48c
f010203d:	68 41 5f 10 f0       	push   $0xf0105f41
f0102042:	e8 05 e0 ff ff       	call   f010004c <_panic>

	// pp2 should be returned by page_alloc
	assert((pp = page_alloc(0)) && pp == pp2);
f0102047:	83 ec 0c             	sub    $0xc,%esp
f010204a:	6a 00                	push   $0x0
f010204c:	e8 a0 ef ff ff       	call   f0100ff1 <page_alloc>
f0102051:	83 c4 10             	add    $0x10,%esp
f0102054:	85 c0                	test   %eax,%eax
f0102056:	74 04                	je     f010205c <mem_init+0xc2c>
f0102058:	39 c3                	cmp    %eax,%ebx
f010205a:	74 19                	je     f0102075 <mem_init+0xc45>
f010205c:	68 48 5c 10 f0       	push   $0xf0105c48
f0102061:	68 d8 50 10 f0       	push   $0xf01050d8
f0102066:	68 8f 04 00 00       	push   $0x48f
f010206b:	68 41 5f 10 f0       	push   $0xf0105f41
f0102070:	e8 d7 df ff ff       	call   f010004c <_panic>

	// unmapping pp1 at 0 should keep pp1 at PGSIZE
	page_remove(kern_pgdir, 0x0);
f0102075:	83 ec 08             	sub    $0x8,%esp
f0102078:	6a 00                	push   $0x0
f010207a:	ff 35 48 94 18 f0    	pushl  0xf0189448
f0102080:	e8 ac f2 ff ff       	call   f0101331 <page_remove>
	assert(check_va2pa(kern_pgdir, 0x0) == ~0);
f0102085:	8b 3d 48 94 18 f0    	mov    0xf0189448,%edi
f010208b:	ba 00 00 00 00       	mov    $0x0,%edx
f0102090:	89 f8                	mov    %edi,%eax
f0102092:	e8 bc ea ff ff       	call   f0100b53 <check_va2pa>
f0102097:	83 c4 10             	add    $0x10,%esp
f010209a:	83 f8 ff             	cmp    $0xffffffff,%eax
f010209d:	74 19                	je     f01020b8 <mem_init+0xc88>
f010209f:	68 6c 5c 10 f0       	push   $0xf0105c6c
f01020a4:	68 d8 50 10 f0       	push   $0xf01050d8
f01020a9:	68 93 04 00 00       	push   $0x493
f01020ae:	68 41 5f 10 f0       	push   $0xf0105f41
f01020b3:	e8 94 df ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == page2pa(pp1));
f01020b8:	ba 00 10 00 00       	mov    $0x1000,%edx
f01020bd:	89 f8                	mov    %edi,%eax
f01020bf:	e8 8f ea ff ff       	call   f0100b53 <check_va2pa>
f01020c4:	89 f2                	mov    %esi,%edx
f01020c6:	2b 15 4c 94 18 f0    	sub    0xf018944c,%edx
f01020cc:	c1 fa 03             	sar    $0x3,%edx
f01020cf:	c1 e2 0c             	shl    $0xc,%edx
f01020d2:	39 d0                	cmp    %edx,%eax
f01020d4:	74 19                	je     f01020ef <mem_init+0xcbf>
f01020d6:	68 18 5c 10 f0       	push   $0xf0105c18
f01020db:	68 d8 50 10 f0       	push   $0xf01050d8
f01020e0:	68 94 04 00 00       	push   $0x494
f01020e5:	68 41 5f 10 f0       	push   $0xf0105f41
f01020ea:	e8 5d df ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref == 1);
f01020ef:	66 83 7e 04 01       	cmpw   $0x1,0x4(%esi)
f01020f4:	74 19                	je     f010210f <mem_init+0xcdf>
f01020f6:	68 fa 61 10 f0       	push   $0xf01061fa
f01020fb:	68 d8 50 10 f0       	push   $0xf01050d8
f0102100:	68 95 04 00 00       	push   $0x495
f0102105:	68 41 5f 10 f0       	push   $0xf0105f41
f010210a:	e8 3d df ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 0);
f010210f:	66 83 7b 04 00       	cmpw   $0x0,0x4(%ebx)
f0102114:	74 19                	je     f010212f <mem_init+0xcff>
f0102116:	68 54 62 10 f0       	push   $0xf0106254
f010211b:	68 d8 50 10 f0       	push   $0xf01050d8
f0102120:	68 96 04 00 00       	push   $0x496
f0102125:	68 41 5f 10 f0       	push   $0xf0105f41
f010212a:	e8 1d df ff ff       	call   f010004c <_panic>

	// test re-inserting pp1 at PGSIZE
	assert(page_insert(kern_pgdir, pp1, (void*) PGSIZE, 0) == 0);
f010212f:	6a 00                	push   $0x0
f0102131:	68 00 10 00 00       	push   $0x1000
f0102136:	56                   	push   %esi
f0102137:	57                   	push   %edi
f0102138:	e8 8d f2 ff ff       	call   f01013ca <page_insert>
f010213d:	83 c4 10             	add    $0x10,%esp
f0102140:	85 c0                	test   %eax,%eax
f0102142:	74 19                	je     f010215d <mem_init+0xd2d>
f0102144:	68 90 5c 10 f0       	push   $0xf0105c90
f0102149:	68 d8 50 10 f0       	push   $0xf01050d8
f010214e:	68 99 04 00 00       	push   $0x499
f0102153:	68 41 5f 10 f0       	push   $0xf0105f41
f0102158:	e8 ef de ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref);
f010215d:	66 83 7e 04 00       	cmpw   $0x0,0x4(%esi)
f0102162:	75 19                	jne    f010217d <mem_init+0xd4d>
f0102164:	68 65 62 10 f0       	push   $0xf0106265
f0102169:	68 d8 50 10 f0       	push   $0xf01050d8
f010216e:	68 9a 04 00 00       	push   $0x49a
f0102173:	68 41 5f 10 f0       	push   $0xf0105f41
f0102178:	e8 cf de ff ff       	call   f010004c <_panic>
	assert(pp1->pp_link == NULL);
f010217d:	83 3e 00             	cmpl   $0x0,(%esi)
f0102180:	74 19                	je     f010219b <mem_init+0xd6b>
f0102182:	68 71 62 10 f0       	push   $0xf0106271
f0102187:	68 d8 50 10 f0       	push   $0xf01050d8
f010218c:	68 9b 04 00 00       	push   $0x49b
f0102191:	68 41 5f 10 f0       	push   $0xf0105f41
f0102196:	e8 b1 de ff ff       	call   f010004c <_panic>

	// unmapping pp1 at PGSIZE should free it
	page_remove(kern_pgdir, (void*) PGSIZE);
f010219b:	83 ec 08             	sub    $0x8,%esp
f010219e:	68 00 10 00 00       	push   $0x1000
f01021a3:	ff 35 48 94 18 f0    	pushl  0xf0189448
f01021a9:	e8 83 f1 ff ff       	call   f0101331 <page_remove>
	assert(check_va2pa(kern_pgdir, 0x0) == ~0);
f01021ae:	8b 3d 48 94 18 f0    	mov    0xf0189448,%edi
f01021b4:	ba 00 00 00 00       	mov    $0x0,%edx
f01021b9:	89 f8                	mov    %edi,%eax
f01021bb:	e8 93 e9 ff ff       	call   f0100b53 <check_va2pa>
f01021c0:	83 c4 10             	add    $0x10,%esp
f01021c3:	83 f8 ff             	cmp    $0xffffffff,%eax
f01021c6:	74 19                	je     f01021e1 <mem_init+0xdb1>
f01021c8:	68 6c 5c 10 f0       	push   $0xf0105c6c
f01021cd:	68 d8 50 10 f0       	push   $0xf01050d8
f01021d2:	68 9f 04 00 00       	push   $0x49f
f01021d7:	68 41 5f 10 f0       	push   $0xf0105f41
f01021dc:	e8 6b de ff ff       	call   f010004c <_panic>
	assert(check_va2pa(kern_pgdir, PGSIZE) == ~0);
f01021e1:	ba 00 10 00 00       	mov    $0x1000,%edx
f01021e6:	89 f8                	mov    %edi,%eax
f01021e8:	e8 66 e9 ff ff       	call   f0100b53 <check_va2pa>
f01021ed:	83 f8 ff             	cmp    $0xffffffff,%eax
f01021f0:	74 19                	je     f010220b <mem_init+0xddb>
f01021f2:	68 c8 5c 10 f0       	push   $0xf0105cc8
f01021f7:	68 d8 50 10 f0       	push   $0xf01050d8
f01021fc:	68 a0 04 00 00       	push   $0x4a0
f0102201:	68 41 5f 10 f0       	push   $0xf0105f41
f0102206:	e8 41 de ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref == 0);
f010220b:	66 83 7e 04 00       	cmpw   $0x0,0x4(%esi)
f0102210:	74 19                	je     f010222b <mem_init+0xdfb>
f0102212:	68 86 62 10 f0       	push   $0xf0106286
f0102217:	68 d8 50 10 f0       	push   $0xf01050d8
f010221c:	68 a1 04 00 00       	push   $0x4a1
f0102221:	68 41 5f 10 f0       	push   $0xf0105f41
f0102226:	e8 21 de ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 0);
f010222b:	66 83 7b 04 00       	cmpw   $0x0,0x4(%ebx)
f0102230:	74 19                	je     f010224b <mem_init+0xe1b>
f0102232:	68 54 62 10 f0       	push   $0xf0106254
f0102237:	68 d8 50 10 f0       	push   $0xf01050d8
f010223c:	68 a2 04 00 00       	push   $0x4a2
f0102241:	68 41 5f 10 f0       	push   $0xf0105f41
f0102246:	e8 01 de ff ff       	call   f010004c <_panic>

	// so it should be returned by page_alloc
	assert((pp = page_alloc(0)) && pp == pp1);
f010224b:	83 ec 0c             	sub    $0xc,%esp
f010224e:	6a 00                	push   $0x0
f0102250:	e8 9c ed ff ff       	call   f0100ff1 <page_alloc>
f0102255:	83 c4 10             	add    $0x10,%esp
f0102258:	85 c0                	test   %eax,%eax
f010225a:	74 04                	je     f0102260 <mem_init+0xe30>
f010225c:	39 c6                	cmp    %eax,%esi
f010225e:	74 19                	je     f0102279 <mem_init+0xe49>
f0102260:	68 f0 5c 10 f0       	push   $0xf0105cf0
f0102265:	68 d8 50 10 f0       	push   $0xf01050d8
f010226a:	68 a5 04 00 00       	push   $0x4a5
f010226f:	68 41 5f 10 f0       	push   $0xf0105f41
f0102274:	e8 d3 dd ff ff       	call   f010004c <_panic>

	// should be no free memory
	assert(!page_alloc(0));
f0102279:	83 ec 0c             	sub    $0xc,%esp
f010227c:	6a 00                	push   $0x0
f010227e:	e8 6e ed ff ff       	call   f0100ff1 <page_alloc>
f0102283:	83 c4 10             	add    $0x10,%esp
f0102286:	85 c0                	test   %eax,%eax
f0102288:	74 19                	je     f01022a3 <mem_init+0xe73>
f010228a:	68 a8 61 10 f0       	push   $0xf01061a8
f010228f:	68 d8 50 10 f0       	push   $0xf01050d8
f0102294:	68 a8 04 00 00       	push   $0x4a8
f0102299:	68 41 5f 10 f0       	push   $0xf0105f41
f010229e:	e8 a9 dd ff ff       	call   f010004c <_panic>

	// forcibly take pp0 back
	assert(PTE_ADDR(kern_pgdir[0]) == page2pa(pp0));
f01022a3:	8b 0d 48 94 18 f0    	mov    0xf0189448,%ecx
f01022a9:	8b 11                	mov    (%ecx),%edx
f01022ab:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f01022b1:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01022b4:	2b 05 4c 94 18 f0    	sub    0xf018944c,%eax
f01022ba:	c1 f8 03             	sar    $0x3,%eax
f01022bd:	c1 e0 0c             	shl    $0xc,%eax
f01022c0:	39 c2                	cmp    %eax,%edx
f01022c2:	74 19                	je     f01022dd <mem_init+0xead>
f01022c4:	68 94 59 10 f0       	push   $0xf0105994
f01022c9:	68 d8 50 10 f0       	push   $0xf01050d8
f01022ce:	68 ab 04 00 00       	push   $0x4ab
f01022d3:	68 41 5f 10 f0       	push   $0xf0105f41
f01022d8:	e8 6f dd ff ff       	call   f010004c <_panic>
	kern_pgdir[0] = 0;
f01022dd:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	assert(pp0->pp_ref == 1);
f01022e3:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f01022e6:	66 83 78 04 01       	cmpw   $0x1,0x4(%eax)
f01022eb:	74 19                	je     f0102306 <mem_init+0xed6>
f01022ed:	68 0b 62 10 f0       	push   $0xf010620b
f01022f2:	68 d8 50 10 f0       	push   $0xf01050d8
f01022f7:	68 ad 04 00 00       	push   $0x4ad
f01022fc:	68 41 5f 10 f0       	push   $0xf0105f41
f0102301:	e8 46 dd ff ff       	call   f010004c <_panic>
	pp0->pp_ref = 0;
f0102306:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0102309:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)

	// check pointer arithmetic in pgdir_walk
	page_free(pp0);
f010230f:	83 ec 0c             	sub    $0xc,%esp
f0102312:	50                   	push   %eax
f0102313:	e8 4f ed ff ff       	call   f0101067 <page_free>
	va = (void*)(PGSIZE * NPDENTRIES + PGSIZE);
	ptep = pgdir_walk(kern_pgdir, va, 1);
f0102318:	83 c4 0c             	add    $0xc,%esp
f010231b:	6a 01                	push   $0x1
f010231d:	68 00 10 40 00       	push   $0x401000
f0102322:	ff 35 48 94 18 f0    	pushl  0xf0189448
f0102328:	e8 bb ed ff ff       	call   f01010e8 <pgdir_walk>
f010232d:	89 45 cc             	mov    %eax,-0x34(%ebp)
f0102330:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	ptep1 = (pte_t *) KADDR(PTE_ADDR(kern_pgdir[PDX(va)]));
f0102333:	8b 0d 48 94 18 f0    	mov    0xf0189448,%ecx
f0102339:	8b 51 04             	mov    0x4(%ecx),%edx
f010233c:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102342:	8b 3d 44 94 18 f0    	mov    0xf0189444,%edi
f0102348:	89 d0                	mov    %edx,%eax
f010234a:	c1 e8 0c             	shr    $0xc,%eax
f010234d:	83 c4 10             	add    $0x10,%esp
f0102350:	39 f8                	cmp    %edi,%eax
f0102352:	72 15                	jb     f0102369 <mem_init+0xf39>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102354:	52                   	push   %edx
f0102355:	68 50 57 10 f0       	push   $0xf0105750
f010235a:	68 b4 04 00 00       	push   $0x4b4
f010235f:	68 41 5f 10 f0       	push   $0xf0105f41
f0102364:	e8 e3 dc ff ff       	call   f010004c <_panic>
	assert(ptep == ptep1 + PTX(va));
f0102369:	81 ea fc ff ff 0f    	sub    $0xffffffc,%edx
f010236f:	39 55 cc             	cmp    %edx,-0x34(%ebp)
f0102372:	74 19                	je     f010238d <mem_init+0xf5d>
f0102374:	68 97 62 10 f0       	push   $0xf0106297
f0102379:	68 d8 50 10 f0       	push   $0xf01050d8
f010237e:	68 b5 04 00 00       	push   $0x4b5
f0102383:	68 41 5f 10 f0       	push   $0xf0105f41
f0102388:	e8 bf dc ff ff       	call   f010004c <_panic>
	kern_pgdir[PDX(va)] = 0;
f010238d:	c7 41 04 00 00 00 00 	movl   $0x0,0x4(%ecx)
	pp0->pp_ref = 0;
f0102394:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0102397:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f010239d:	2b 05 4c 94 18 f0    	sub    0xf018944c,%eax
f01023a3:	c1 f8 03             	sar    $0x3,%eax
f01023a6:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01023a9:	89 c2                	mov    %eax,%edx
f01023ab:	c1 ea 0c             	shr    $0xc,%edx
f01023ae:	39 d7                	cmp    %edx,%edi
f01023b0:	77 12                	ja     f01023c4 <mem_init+0xf94>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01023b2:	50                   	push   %eax
f01023b3:	68 50 57 10 f0       	push   $0xf0105750
f01023b8:	6a 56                	push   $0x56
f01023ba:	68 4d 5f 10 f0       	push   $0xf0105f4d
f01023bf:	e8 88 dc ff ff       	call   f010004c <_panic>

	// check that new page tables get cleared
	memset(page2kva(pp0), 0xFF, PGSIZE);
f01023c4:	83 ec 04             	sub    $0x4,%esp
f01023c7:	68 00 10 00 00       	push   $0x1000
f01023cc:	68 ff 00 00 00       	push   $0xff
f01023d1:	2d 00 00 00 10       	sub    $0x10000000,%eax
f01023d6:	50                   	push   %eax
f01023d7:	e8 67 28 00 00       	call   f0104c43 <memset>
	page_free(pp0);
f01023dc:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f01023df:	89 3c 24             	mov    %edi,(%esp)
f01023e2:	e8 80 ec ff ff       	call   f0101067 <page_free>
	pgdir_walk(kern_pgdir, 0x0, 1);
f01023e7:	83 c4 0c             	add    $0xc,%esp
f01023ea:	6a 01                	push   $0x1
f01023ec:	6a 00                	push   $0x0
f01023ee:	ff 35 48 94 18 f0    	pushl  0xf0189448
f01023f4:	e8 ef ec ff ff       	call   f01010e8 <pgdir_walk>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f01023f9:	89 fa                	mov    %edi,%edx
f01023fb:	2b 15 4c 94 18 f0    	sub    0xf018944c,%edx
f0102401:	c1 fa 03             	sar    $0x3,%edx
f0102404:	c1 e2 0c             	shl    $0xc,%edx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102407:	89 d0                	mov    %edx,%eax
f0102409:	c1 e8 0c             	shr    $0xc,%eax
f010240c:	83 c4 10             	add    $0x10,%esp
f010240f:	3b 05 44 94 18 f0    	cmp    0xf0189444,%eax
f0102415:	72 12                	jb     f0102429 <mem_init+0xff9>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102417:	52                   	push   %edx
f0102418:	68 50 57 10 f0       	push   $0xf0105750
f010241d:	6a 56                	push   $0x56
f010241f:	68 4d 5f 10 f0       	push   $0xf0105f4d
f0102424:	e8 23 dc ff ff       	call   f010004c <_panic>
	return (void *)(pa + KERNBASE);
f0102429:	8d 82 00 00 00 f0    	lea    -0x10000000(%edx),%eax
	ptep = (pte_t *) page2kva(pp0);
f010242f:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0102432:	81 ea 00 f0 ff 0f    	sub    $0xffff000,%edx
	for(i=0; i<NPTENTRIES; i++)
		assert((ptep[i] & PTE_P) == 0);
f0102438:	f6 00 01             	testb  $0x1,(%eax)
f010243b:	74 19                	je     f0102456 <mem_init+0x1026>
f010243d:	68 af 62 10 f0       	push   $0xf01062af
f0102442:	68 d8 50 10 f0       	push   $0xf01050d8
f0102447:	68 bf 04 00 00       	push   $0x4bf
f010244c:	68 41 5f 10 f0       	push   $0xf0105f41
f0102451:	e8 f6 db ff ff       	call   f010004c <_panic>
f0102456:	83 c0 04             	add    $0x4,%eax
	// check that new page tables get cleared
	memset(page2kva(pp0), 0xFF, PGSIZE);
	page_free(pp0);
	pgdir_walk(kern_pgdir, 0x0, 1);
	ptep = (pte_t *) page2kva(pp0);
	for(i=0; i<NPTENTRIES; i++)
f0102459:	39 d0                	cmp    %edx,%eax
f010245b:	75 db                	jne    f0102438 <mem_init+0x1008>
		assert((ptep[i] & PTE_P) == 0);
	kern_pgdir[0] = 0;
f010245d:	a1 48 94 18 f0       	mov    0xf0189448,%eax
f0102462:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	pp0->pp_ref = 0;
f0102468:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f010246b:	66 c7 40 04 00 00    	movw   $0x0,0x4(%eax)

	// give free list back
	page_free_list = fl;
f0102471:	8b 7d d0             	mov    -0x30(%ebp),%edi
f0102474:	89 3d 60 82 18 f0    	mov    %edi,0xf0188260

	// free the pages we took
	page_free(pp0);
f010247a:	83 ec 0c             	sub    $0xc,%esp
f010247d:	50                   	push   %eax
f010247e:	e8 e4 eb ff ff       	call   f0101067 <page_free>
	page_free(pp1);
f0102483:	89 34 24             	mov    %esi,(%esp)
f0102486:	e8 dc eb ff ff       	call   f0101067 <page_free>
	page_free(pp2);
f010248b:	89 1c 24             	mov    %ebx,(%esp)
f010248e:	e8 d4 eb ff ff       	call   f0101067 <page_free>

	cprintf("check_page() succeeded!\n");
f0102493:	c7 04 24 c6 62 10 f0 	movl   $0xf01062c6,(%esp)
f010249a:	e8 f2 0e 00 00       	call   f0103391 <cprintf>
	//    - the new image at UPAGES -- kernel R, user R
	//      (ie. perm = PTE_U | PTE_P)
	//    - pages itself -- kernel RW, user NONE
	// Your code goes here:
	size_t pags_Size = npages * sizeof(struct PageInfo);
	physaddr_t pages_PA = PADDR(pages);//page2pa(pages);
f010249f:	a1 4c 94 18 f0       	mov    0xf018944c,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01024a4:	83 c4 10             	add    $0x10,%esp
f01024a7:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01024ac:	77 15                	ja     f01024c3 <mem_init+0x1093>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01024ae:	50                   	push   %eax
f01024af:	68 98 58 10 f0       	push   $0xf0105898
f01024b4:	68 cb 00 00 00       	push   $0xcb
f01024b9:	68 41 5f 10 f0       	push   $0xf0105f41
f01024be:	e8 89 db ff ff       	call   f010004c <_panic>
//boot_map_region(kern_pgdir, UPAGES, PTSIZE, pages_PA, PTE_U | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, UPAGES, PTSIZE, PADDR(pages), PTE_U);
f01024c3:	83 ec 08             	sub    $0x8,%esp
f01024c6:	6a 04                	push   $0x4
f01024c8:	05 00 00 00 10       	add    $0x10000000,%eax
f01024cd:	50                   	push   %eax
f01024ce:	b9 00 00 40 00       	mov    $0x400000,%ecx
f01024d3:	ba 00 00 00 ef       	mov    $0xef000000,%edx
f01024d8:	a1 48 94 18 f0       	mov    0xf0189448,%eax
f01024dd:	e8 ea ec ff ff       	call   f01011cc <boot_map_region>

	size_t envs_Size = NENV * sizeof(struct Env);
	physaddr_t envs_PA = PADDR(envs);
f01024e2:	a1 68 82 18 f0       	mov    0xf0188268,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01024e7:	83 c4 10             	add    $0x10,%esp
f01024ea:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01024ef:	77 15                	ja     f0102506 <mem_init+0x10d6>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01024f1:	50                   	push   %eax
f01024f2:	68 98 58 10 f0       	push   $0xf0105898
f01024f7:	68 d1 00 00 00       	push   $0xd1
f01024fc:	68 41 5f 10 f0       	push   $0xf0105f41
f0102501:	e8 46 db ff ff       	call   f010004c <_panic>
	//the memory backing envs should also be mapped user read-only at UENVS
	//boot_map_region(kern_pgdir, UENVS, envs_Size, envs_PA, PTE_U | PTE_P);
//PTSIZE
//boot_map_region(kern_pgdir, UENVS, PTSIZE, envs_PA, PTE_U | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, UENVS, PTSIZE, PADDR(envs), PTE_U);
f0102506:	83 ec 08             	sub    $0x8,%esp
f0102509:	6a 04                	push   $0x4
f010250b:	05 00 00 00 10       	add    $0x10000000,%eax
f0102510:	50                   	push   %eax
f0102511:	b9 00 00 40 00       	mov    $0x400000,%ecx
f0102516:	ba 00 00 c0 ee       	mov    $0xeec00000,%edx
f010251b:	a1 48 94 18 f0       	mov    0xf0189448,%eax
f0102520:	e8 a7 ec ff ff       	call   f01011cc <boot_map_region>
	cprintf("In mem_init(), UENVS is %p, and envs is: %p\n\n\n\n", UENVS, envs);
f0102525:	83 c4 0c             	add    $0xc,%esp
f0102528:	ff 35 68 82 18 f0    	pushl  0xf0188268
f010252e:	68 00 00 c0 ee       	push   $0xeec00000
f0102533:	68 14 5d 10 f0       	push   $0xf0105d14
f0102538:	e8 54 0e 00 00       	call   f0103391 <cprintf>
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f010253d:	83 c4 10             	add    $0x10,%esp
f0102540:	b8 00 30 11 f0       	mov    $0xf0113000,%eax
f0102545:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f010254a:	77 15                	ja     f0102561 <mem_init+0x1131>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f010254c:	50                   	push   %eax
f010254d:	68 98 58 10 f0       	push   $0xf0105898
f0102552:	68 f6 00 00 00       	push   $0xf6
f0102557:	68 41 5f 10 f0       	push   $0xf0105f41
f010255c:	e8 eb da ff ff       	call   f010004c <_panic>
	  #### (unmapped)
	  KSTACKTOP - PTSIZE
	*/
//boot_map_region(kern_pgdir, (KSTACKTOP-KSTKSIZE), KSTKSIZE, ((physaddr_t)PADDR(bootstack)), PTE_W | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, KSTACKTOP - KSTKSIZE, KSTKSIZE, PADDR(bootstack), PTE_W);  
f0102561:	83 ec 08             	sub    $0x8,%esp
f0102564:	6a 02                	push   $0x2
f0102566:	68 00 30 11 00       	push   $0x113000
f010256b:	b9 00 80 00 00       	mov    $0x8000,%ecx
f0102570:	ba 00 80 ff ef       	mov    $0xefff8000,%edx
f0102575:	a1 48 94 18 f0       	mov    0xf0189448,%eax
f010257a:	e8 4d ec ff ff       	call   f01011cc <boot_map_region>
	// we just set up the mapping anyway.
	// Permissions: kernel RW, user NONE
	// Your code goes here:
//boot_map_region(kern_pgdir, KERNBASE, (0xFFFFFFFF - KERNBASE), 0, PTE_W | PTE_P);
	//reference code:
	boot_map_region(kern_pgdir, KERNBASE, -KERNBASE/* ~KERNBASE + 1 */, 0, PTE_W);
f010257f:	83 c4 08             	add    $0x8,%esp
f0102582:	6a 02                	push   $0x2
f0102584:	6a 00                	push   $0x0
f0102586:	b9 00 00 00 10       	mov    $0x10000000,%ecx
f010258b:	ba 00 00 00 f0       	mov    $0xf0000000,%edx
f0102590:	a1 48 94 18 f0       	mov    0xf0189448,%eax
f0102595:	e8 32 ec ff ff       	call   f01011cc <boot_map_region>
check_kern_pgdir(void)
{
	uint32_t i, n;
	pde_t *pgdir;

	pgdir = kern_pgdir;
f010259a:	8b 35 48 94 18 f0    	mov    0xf0189448,%esi

	// check pages array
	n = ROUNDUP(npages*sizeof(struct PageInfo), PGSIZE);
f01025a0:	a1 44 94 18 f0       	mov    0xf0189444,%eax
f01025a5:	89 45 c8             	mov    %eax,-0x38(%ebp)
f01025a8:	8d 04 c5 ff 0f 00 00 	lea    0xfff(,%eax,8),%eax
f01025af:	25 00 f0 ff ff       	and    $0xfffff000,%eax
	for (i = 0; i < n; i += PGSIZE){
		assert(check_va2pa(pgdir, UPAGES + i) == PADDR(pages) + i);
f01025b4:	8b 3d 4c 94 18 f0    	mov    0xf018944c,%edi
f01025ba:	89 7d c4             	mov    %edi,-0x3c(%ebp)
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01025bd:	89 7d d0             	mov    %edi,-0x30(%ebp)
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
	return (physaddr_t)kva - KERNBASE;
f01025c0:	81 c7 00 00 00 10    	add    $0x10000000,%edi
f01025c6:	89 7d cc             	mov    %edi,-0x34(%ebp)
//<<<<<<< HEAD

	// check envs array (new test for lab 3)
	n = ROUNDUP(NENV*sizeof(struct Env), PGSIZE);
	for (i = 0; i < n; i += PGSIZE)
		assert(check_va2pa(pgdir, UENVS + i) == PADDR(envs) + i);
f01025c9:	8b 3d 68 82 18 f0    	mov    0xf0188268,%edi
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01025cf:	89 7d d4             	mov    %edi,-0x2c(%ebp)
f01025d2:	83 c4 10             	add    $0x10,%esp

	pgdir = kern_pgdir;

	// check pages array
	n = ROUNDUP(npages*sizeof(struct PageInfo), PGSIZE);
	for (i = 0; i < n; i += PGSIZE){
f01025d5:	bb 00 00 00 00       	mov    $0x0,%ebx
f01025da:	e9 b8 00 00 00       	jmp    f0102697 <mem_init+0x1267>
		assert(check_va2pa(pgdir, UPAGES + i) == PADDR(pages) + i);
f01025df:	8d 93 00 00 00 ef    	lea    -0x11000000(%ebx),%edx
f01025e5:	89 f0                	mov    %esi,%eax
f01025e7:	e8 67 e5 ff ff       	call   f0100b53 <check_va2pa>
f01025ec:	81 7d d0 ff ff ff ef 	cmpl   $0xefffffff,-0x30(%ebp)
f01025f3:	77 17                	ja     f010260c <mem_init+0x11dc>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01025f5:	ff 75 c4             	pushl  -0x3c(%ebp)
f01025f8:	68 98 58 10 f0       	push   $0xf0105898
f01025fd:	68 dd 03 00 00       	push   $0x3dd
f0102602:	68 41 5f 10 f0       	push   $0xf0105f41
f0102607:	e8 40 da ff ff       	call   f010004c <_panic>
f010260c:	03 5d cc             	add    -0x34(%ebp),%ebx
f010260f:	39 d8                	cmp    %ebx,%eax
f0102611:	74 19                	je     f010262c <mem_init+0x11fc>
f0102613:	68 44 5d 10 f0       	push   $0xf0105d44
f0102618:	68 d8 50 10 f0       	push   $0xf01050d8
f010261d:	68 dd 03 00 00       	push   $0x3dd
f0102622:	68 41 5f 10 f0       	push   $0xf0105f41
f0102627:	e8 20 da ff ff       	call   f010004c <_panic>
f010262c:	bb 00 00 00 00       	mov    $0x0,%ebx
f0102631:	eb 02                	jmp    f0102635 <mem_init+0x1205>
//<<<<<<< HEAD

	// check envs array (new test for lab 3)
	n = ROUNDUP(NENV*sizeof(struct Env), PGSIZE);
	for (i = 0; i < n; i += PGSIZE)
f0102633:	89 c3                	mov    %eax,%ebx
		assert(check_va2pa(pgdir, UENVS + i) == PADDR(envs) + i);
f0102635:	8d 93 00 00 c0 ee    	lea    -0x11400000(%ebx),%edx
f010263b:	89 f0                	mov    %esi,%eax
f010263d:	e8 11 e5 ff ff       	call   f0100b53 <check_va2pa>
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102642:	81 7d d4 ff ff ff ef 	cmpl   $0xefffffff,-0x2c(%ebp)
f0102649:	77 15                	ja     f0102660 <mem_init+0x1230>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f010264b:	57                   	push   %edi
f010264c:	68 98 58 10 f0       	push   $0xf0105898
f0102651:	68 e3 03 00 00       	push   $0x3e3
f0102656:	68 41 5f 10 f0       	push   $0xf0105f41
f010265b:	e8 ec d9 ff ff       	call   f010004c <_panic>
f0102660:	8d 94 1f 00 00 00 10 	lea    0x10000000(%edi,%ebx,1),%edx
f0102667:	39 d0                	cmp    %edx,%eax
f0102669:	74 19                	je     f0102684 <mem_init+0x1254>
f010266b:	68 78 5d 10 f0       	push   $0xf0105d78
f0102670:	68 d8 50 10 f0       	push   $0xf01050d8
f0102675:	68 e3 03 00 00       	push   $0x3e3
f010267a:	68 41 5f 10 f0       	push   $0xf0105f41
f010267f:	e8 c8 d9 ff ff       	call   f010004c <_panic>
		assert(check_va2pa(pgdir, UPAGES + i) == PADDR(pages) + i);
//<<<<<<< HEAD

	// check envs array (new test for lab 3)
	n = ROUNDUP(NENV*sizeof(struct Env), PGSIZE);
	for (i = 0; i < n; i += PGSIZE)
f0102684:	8d 83 00 10 00 00    	lea    0x1000(%ebx),%eax
f010268a:	3d 00 80 01 00       	cmp    $0x18000,%eax
f010268f:	75 a2                	jne    f0102633 <mem_init+0x1203>

	pgdir = kern_pgdir;

	// check pages array
	n = ROUNDUP(npages*sizeof(struct PageInfo), PGSIZE);
	for (i = 0; i < n; i += PGSIZE){
f0102691:	81 c3 00 20 00 00    	add    $0x2000,%ebx
f0102697:	39 c3                	cmp    %eax,%ebx
f0102699:	0f 82 40 ff ff ff    	jb     f01025df <mem_init+0x11af>
//=======
	}
//>>>>>>> lab2

	// check phys mem
	for (i = 0; i < npages * PGSIZE; i += PGSIZE)
f010269f:	8b 7d c8             	mov    -0x38(%ebp),%edi
f01026a2:	c1 e7 0c             	shl    $0xc,%edi
f01026a5:	bb 00 00 00 00       	mov    $0x0,%ebx
f01026aa:	eb 30                	jmp    f01026dc <mem_init+0x12ac>
		assert(check_va2pa(pgdir, KERNBASE + i) == i);
f01026ac:	8d 93 00 00 00 f0    	lea    -0x10000000(%ebx),%edx
f01026b2:	89 f0                	mov    %esi,%eax
f01026b4:	e8 9a e4 ff ff       	call   f0100b53 <check_va2pa>
f01026b9:	39 c3                	cmp    %eax,%ebx
f01026bb:	74 19                	je     f01026d6 <mem_init+0x12a6>
f01026bd:	68 ac 5d 10 f0       	push   $0xf0105dac
f01026c2:	68 d8 50 10 f0       	push   $0xf01050d8
f01026c7:	68 ea 03 00 00       	push   $0x3ea
f01026cc:	68 41 5f 10 f0       	push   $0xf0105f41
f01026d1:	e8 76 d9 ff ff       	call   f010004c <_panic>
//=======
	}
//>>>>>>> lab2

	// check phys mem
	for (i = 0; i < npages * PGSIZE; i += PGSIZE)
f01026d6:	81 c3 00 10 00 00    	add    $0x1000,%ebx
f01026dc:	39 fb                	cmp    %edi,%ebx
f01026de:	72 cc                	jb     f01026ac <mem_init+0x127c>
f01026e0:	bb 00 80 ff ef       	mov    $0xefff8000,%ebx
		assert(check_va2pa(pgdir, KERNBASE + i) == i);

	// check kernel stack
	for (i = 0; i < KSTKSIZE; i += PGSIZE)
		assert(check_va2pa(pgdir, KSTACKTOP - KSTKSIZE + i) == PADDR(bootstack) + i);
f01026e5:	89 da                	mov    %ebx,%edx
f01026e7:	89 f0                	mov    %esi,%eax
f01026e9:	e8 65 e4 ff ff       	call   f0100b53 <check_va2pa>
f01026ee:	8d 93 00 b0 11 10    	lea    0x1011b000(%ebx),%edx
f01026f4:	39 c2                	cmp    %eax,%edx
f01026f6:	74 19                	je     f0102711 <mem_init+0x12e1>
f01026f8:	68 d4 5d 10 f0       	push   $0xf0105dd4
f01026fd:	68 d8 50 10 f0       	push   $0xf01050d8
f0102702:	68 ee 03 00 00       	push   $0x3ee
f0102707:	68 41 5f 10 f0       	push   $0xf0105f41
f010270c:	e8 3b d9 ff ff       	call   f010004c <_panic>
f0102711:	81 c3 00 10 00 00    	add    $0x1000,%ebx
	// check phys mem
	for (i = 0; i < npages * PGSIZE; i += PGSIZE)
		assert(check_va2pa(pgdir, KERNBASE + i) == i);

	// check kernel stack
	for (i = 0; i < KSTKSIZE; i += PGSIZE)
f0102717:	81 fb 00 00 00 f0    	cmp    $0xf0000000,%ebx
f010271d:	75 c6                	jne    f01026e5 <mem_init+0x12b5>
		assert(check_va2pa(pgdir, KSTACKTOP - KSTKSIZE + i) == PADDR(bootstack) + i);
	assert(check_va2pa(pgdir, KSTACKTOP - PTSIZE) == ~0);
f010271f:	ba 00 00 c0 ef       	mov    $0xefc00000,%edx
f0102724:	89 f0                	mov    %esi,%eax
f0102726:	e8 28 e4 ff ff       	call   f0100b53 <check_va2pa>
f010272b:	83 f8 ff             	cmp    $0xffffffff,%eax
f010272e:	74 51                	je     f0102781 <mem_init+0x1351>
f0102730:	68 1c 5e 10 f0       	push   $0xf0105e1c
f0102735:	68 d8 50 10 f0       	push   $0xf01050d8
f010273a:	68 ef 03 00 00       	push   $0x3ef
f010273f:	68 41 5f 10 f0       	push   $0xf0105f41
f0102744:	e8 03 d9 ff ff       	call   f010004c <_panic>

	// check PDE permissions
	for (i = 0; i < NPDENTRIES; i++) {
		switch (i) {
f0102749:	3d bb 03 00 00       	cmp    $0x3bb,%eax
f010274e:	72 36                	jb     f0102786 <mem_init+0x1356>
f0102750:	3d bd 03 00 00       	cmp    $0x3bd,%eax
f0102755:	76 07                	jbe    f010275e <mem_init+0x132e>
f0102757:	3d bf 03 00 00       	cmp    $0x3bf,%eax
f010275c:	75 28                	jne    f0102786 <mem_init+0x1356>
		case PDX(UVPT):
		case PDX(KSTACKTOP-1):
		case PDX(UPAGES):
		case PDX(UENVS):
			assert(pgdir[i] & PTE_P);
f010275e:	f6 04 86 01          	testb  $0x1,(%esi,%eax,4)
f0102762:	0f 85 83 00 00 00    	jne    f01027eb <mem_init+0x13bb>
f0102768:	68 df 62 10 f0       	push   $0xf01062df
f010276d:	68 d8 50 10 f0       	push   $0xf01050d8
f0102772:	68 f8 03 00 00       	push   $0x3f8
f0102777:	68 41 5f 10 f0       	push   $0xf0105f41
f010277c:	e8 cb d8 ff ff       	call   f010004c <_panic>
		assert(check_va2pa(pgdir, KERNBASE + i) == i);

	// check kernel stack
	for (i = 0; i < KSTKSIZE; i += PGSIZE)
		assert(check_va2pa(pgdir, KSTACKTOP - KSTKSIZE + i) == PADDR(bootstack) + i);
	assert(check_va2pa(pgdir, KSTACKTOP - PTSIZE) == ~0);
f0102781:	b8 00 00 00 00       	mov    $0x0,%eax
		case PDX(UPAGES):
		case PDX(UENVS):
			assert(pgdir[i] & PTE_P);
			break;
		default:
			if (i >= PDX(KERNBASE)) {
f0102786:	3d bf 03 00 00       	cmp    $0x3bf,%eax
f010278b:	76 3f                	jbe    f01027cc <mem_init+0x139c>
				assert(pgdir[i] & PTE_P);
f010278d:	8b 14 86             	mov    (%esi,%eax,4),%edx
f0102790:	f6 c2 01             	test   $0x1,%dl
f0102793:	75 19                	jne    f01027ae <mem_init+0x137e>
f0102795:	68 df 62 10 f0       	push   $0xf01062df
f010279a:	68 d8 50 10 f0       	push   $0xf01050d8
f010279f:	68 fc 03 00 00       	push   $0x3fc
f01027a4:	68 41 5f 10 f0       	push   $0xf0105f41
f01027a9:	e8 9e d8 ff ff       	call   f010004c <_panic>
				assert(pgdir[i] & PTE_W);
f01027ae:	f6 c2 02             	test   $0x2,%dl
f01027b1:	75 38                	jne    f01027eb <mem_init+0x13bb>
f01027b3:	68 f0 62 10 f0       	push   $0xf01062f0
f01027b8:	68 d8 50 10 f0       	push   $0xf01050d8
f01027bd:	68 fd 03 00 00       	push   $0x3fd
f01027c2:	68 41 5f 10 f0       	push   $0xf0105f41
f01027c7:	e8 80 d8 ff ff       	call   f010004c <_panic>
			} else
				assert(pgdir[i] == 0);
f01027cc:	83 3c 86 00          	cmpl   $0x0,(%esi,%eax,4)
f01027d0:	74 19                	je     f01027eb <mem_init+0x13bb>
f01027d2:	68 01 63 10 f0       	push   $0xf0106301
f01027d7:	68 d8 50 10 f0       	push   $0xf01050d8
f01027dc:	68 ff 03 00 00       	push   $0x3ff
f01027e1:	68 41 5f 10 f0       	push   $0xf0105f41
f01027e6:	e8 61 d8 ff ff       	call   f010004c <_panic>
	for (i = 0; i < KSTKSIZE; i += PGSIZE)
		assert(check_va2pa(pgdir, KSTACKTOP - KSTKSIZE + i) == PADDR(bootstack) + i);
	assert(check_va2pa(pgdir, KSTACKTOP - PTSIZE) == ~0);

	// check PDE permissions
	for (i = 0; i < NPDENTRIES; i++) {
f01027eb:	40                   	inc    %eax
f01027ec:	3d ff 03 00 00       	cmp    $0x3ff,%eax
f01027f1:	0f 86 52 ff ff ff    	jbe    f0102749 <mem_init+0x1319>
			} else
				assert(pgdir[i] == 0);
			break;
		}
	}
	cprintf("check_kern_pgdir() succeeded!\n");
f01027f7:	83 ec 0c             	sub    $0xc,%esp
f01027fa:	68 4c 5e 10 f0       	push   $0xf0105e4c
f01027ff:	e8 8d 0b 00 00       	call   f0103391 <cprintf>
	// somewhere between KERNBASE and KERNBASE+4MB right now, which is
	// mapped the same way by both page tables.
	//
	// If the machine reboots at this point, you've probably set up your
	// kern_pgdir wrong.
	lcr3(PADDR(kern_pgdir));
f0102804:	a1 48 94 18 f0       	mov    0xf0189448,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102809:	83 c4 10             	add    $0x10,%esp
f010280c:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0102811:	77 15                	ja     f0102828 <mem_init+0x13f8>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102813:	50                   	push   %eax
f0102814:	68 98 58 10 f0       	push   $0xf0105898
f0102819:	68 0f 01 00 00       	push   $0x10f
f010281e:	68 41 5f 10 f0       	push   $0xf0105f41
f0102823:	e8 24 d8 ff ff       	call   f010004c <_panic>
}

static inline void
lcr3(uint32_t val)
{
	asm volatile("movl %0,%%cr3" : : "r" (val));
f0102828:	05 00 00 00 10       	add    $0x10000000,%eax
f010282d:	0f 22 d8             	mov    %eax,%cr3

	check_page_free_list(0);
f0102830:	b8 00 00 00 00       	mov    $0x0,%eax
f0102835:	e8 78 e3 ff ff       	call   f0100bb2 <check_page_free_list>

static inline uint32_t
rcr0(void)
{
	uint32_t val;
	asm volatile("movl %%cr0,%0" : "=r" (val));
f010283a:	0f 20 c0             	mov    %cr0,%eax
f010283d:	83 e0 f3             	and    $0xfffffff3,%eax
}

static inline void
lcr0(uint32_t val)
{
	asm volatile("movl %0,%%cr0" : : "r" (val));
f0102840:	0d 23 00 05 80       	or     $0x80050023,%eax
f0102845:	0f 22 c0             	mov    %eax,%cr0
	uintptr_t va;
	int i;

	// check that we can read and write installed pages
	pp1 = pp2 = 0;
	assert((pp0 = page_alloc(0)));
f0102848:	83 ec 0c             	sub    $0xc,%esp
f010284b:	6a 00                	push   $0x0
f010284d:	e8 9f e7 ff ff       	call   f0100ff1 <page_alloc>
f0102852:	89 c3                	mov    %eax,%ebx
f0102854:	83 c4 10             	add    $0x10,%esp
f0102857:	85 c0                	test   %eax,%eax
f0102859:	75 19                	jne    f0102874 <mem_init+0x1444>
f010285b:	68 fd 60 10 f0       	push   $0xf01060fd
f0102860:	68 d8 50 10 f0       	push   $0xf01050d8
f0102865:	68 da 04 00 00       	push   $0x4da
f010286a:	68 41 5f 10 f0       	push   $0xf0105f41
f010286f:	e8 d8 d7 ff ff       	call   f010004c <_panic>
	assert((pp1 = page_alloc(0)));
f0102874:	83 ec 0c             	sub    $0xc,%esp
f0102877:	6a 00                	push   $0x0
f0102879:	e8 73 e7 ff ff       	call   f0100ff1 <page_alloc>
f010287e:	89 c7                	mov    %eax,%edi
f0102880:	83 c4 10             	add    $0x10,%esp
f0102883:	85 c0                	test   %eax,%eax
f0102885:	75 19                	jne    f01028a0 <mem_init+0x1470>
f0102887:	68 13 61 10 f0       	push   $0xf0106113
f010288c:	68 d8 50 10 f0       	push   $0xf01050d8
f0102891:	68 db 04 00 00       	push   $0x4db
f0102896:	68 41 5f 10 f0       	push   $0xf0105f41
f010289b:	e8 ac d7 ff ff       	call   f010004c <_panic>
	assert((pp2 = page_alloc(0)));
f01028a0:	83 ec 0c             	sub    $0xc,%esp
f01028a3:	6a 00                	push   $0x0
f01028a5:	e8 47 e7 ff ff       	call   f0100ff1 <page_alloc>
f01028aa:	89 c6                	mov    %eax,%esi
f01028ac:	83 c4 10             	add    $0x10,%esp
f01028af:	85 c0                	test   %eax,%eax
f01028b1:	75 19                	jne    f01028cc <mem_init+0x149c>
f01028b3:	68 29 61 10 f0       	push   $0xf0106129
f01028b8:	68 d8 50 10 f0       	push   $0xf01050d8
f01028bd:	68 dc 04 00 00       	push   $0x4dc
f01028c2:	68 41 5f 10 f0       	push   $0xf0105f41
f01028c7:	e8 80 d7 ff ff       	call   f010004c <_panic>
	page_free(pp0);
f01028cc:	83 ec 0c             	sub    $0xc,%esp
f01028cf:	53                   	push   %ebx
f01028d0:	e8 92 e7 ff ff       	call   f0101067 <page_free>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f01028d5:	89 f8                	mov    %edi,%eax
f01028d7:	2b 05 4c 94 18 f0    	sub    0xf018944c,%eax
f01028dd:	c1 f8 03             	sar    $0x3,%eax
f01028e0:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01028e3:	89 c2                	mov    %eax,%edx
f01028e5:	c1 ea 0c             	shr    $0xc,%edx
f01028e8:	83 c4 10             	add    $0x10,%esp
f01028eb:	3b 15 44 94 18 f0    	cmp    0xf0189444,%edx
f01028f1:	72 12                	jb     f0102905 <mem_init+0x14d5>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f01028f3:	50                   	push   %eax
f01028f4:	68 50 57 10 f0       	push   $0xf0105750
f01028f9:	6a 56                	push   $0x56
f01028fb:	68 4d 5f 10 f0       	push   $0xf0105f4d
f0102900:	e8 47 d7 ff ff       	call   f010004c <_panic>
	memset(page2kva(pp1), 1, PGSIZE);
f0102905:	83 ec 04             	sub    $0x4,%esp
f0102908:	68 00 10 00 00       	push   $0x1000
f010290d:	6a 01                	push   $0x1
f010290f:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102914:	50                   	push   %eax
f0102915:	e8 29 23 00 00       	call   f0104c43 <memset>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f010291a:	89 f0                	mov    %esi,%eax
f010291c:	2b 05 4c 94 18 f0    	sub    0xf018944c,%eax
f0102922:	c1 f8 03             	sar    $0x3,%eax
f0102925:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102928:	89 c2                	mov    %eax,%edx
f010292a:	c1 ea 0c             	shr    $0xc,%edx
f010292d:	83 c4 10             	add    $0x10,%esp
f0102930:	3b 15 44 94 18 f0    	cmp    0xf0189444,%edx
f0102936:	72 12                	jb     f010294a <mem_init+0x151a>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102938:	50                   	push   %eax
f0102939:	68 50 57 10 f0       	push   $0xf0105750
f010293e:	6a 56                	push   $0x56
f0102940:	68 4d 5f 10 f0       	push   $0xf0105f4d
f0102945:	e8 02 d7 ff ff       	call   f010004c <_panic>
	memset(page2kva(pp2), 2, PGSIZE);
f010294a:	83 ec 04             	sub    $0x4,%esp
f010294d:	68 00 10 00 00       	push   $0x1000
f0102952:	6a 02                	push   $0x2
f0102954:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102959:	50                   	push   %eax
f010295a:	e8 e4 22 00 00       	call   f0104c43 <memset>
	page_insert(kern_pgdir, pp1, (void*) PGSIZE, PTE_W);
f010295f:	6a 02                	push   $0x2
f0102961:	68 00 10 00 00       	push   $0x1000
f0102966:	57                   	push   %edi
f0102967:	ff 35 48 94 18 f0    	pushl  0xf0189448
f010296d:	e8 58 ea ff ff       	call   f01013ca <page_insert>
	assert(pp1->pp_ref == 1);
f0102972:	83 c4 20             	add    $0x20,%esp
f0102975:	66 83 7f 04 01       	cmpw   $0x1,0x4(%edi)
f010297a:	74 19                	je     f0102995 <mem_init+0x1565>
f010297c:	68 fa 61 10 f0       	push   $0xf01061fa
f0102981:	68 d8 50 10 f0       	push   $0xf01050d8
f0102986:	68 e1 04 00 00       	push   $0x4e1
f010298b:	68 41 5f 10 f0       	push   $0xf0105f41
f0102990:	e8 b7 d6 ff ff       	call   f010004c <_panic>
	assert(*(uint32_t *)PGSIZE == 0x01010101U);
f0102995:	81 3d 00 10 00 00 01 	cmpl   $0x1010101,0x1000
f010299c:	01 01 01 
f010299f:	74 19                	je     f01029ba <mem_init+0x158a>
f01029a1:	68 6c 5e 10 f0       	push   $0xf0105e6c
f01029a6:	68 d8 50 10 f0       	push   $0xf01050d8
f01029ab:	68 e2 04 00 00       	push   $0x4e2
f01029b0:	68 41 5f 10 f0       	push   $0xf0105f41
f01029b5:	e8 92 d6 ff ff       	call   f010004c <_panic>
	page_insert(kern_pgdir, pp2, (void*) PGSIZE, PTE_W);
f01029ba:	6a 02                	push   $0x2
f01029bc:	68 00 10 00 00       	push   $0x1000
f01029c1:	56                   	push   %esi
f01029c2:	ff 35 48 94 18 f0    	pushl  0xf0189448
f01029c8:	e8 fd e9 ff ff       	call   f01013ca <page_insert>
	assert(*(uint32_t *)PGSIZE == 0x02020202U);
f01029cd:	83 c4 10             	add    $0x10,%esp
f01029d0:	81 3d 00 10 00 00 02 	cmpl   $0x2020202,0x1000
f01029d7:	02 02 02 
f01029da:	74 19                	je     f01029f5 <mem_init+0x15c5>
f01029dc:	68 90 5e 10 f0       	push   $0xf0105e90
f01029e1:	68 d8 50 10 f0       	push   $0xf01050d8
f01029e6:	68 e4 04 00 00       	push   $0x4e4
f01029eb:	68 41 5f 10 f0       	push   $0xf0105f41
f01029f0:	e8 57 d6 ff ff       	call   f010004c <_panic>
	assert(pp2->pp_ref == 1);
f01029f5:	66 83 7e 04 01       	cmpw   $0x1,0x4(%esi)
f01029fa:	74 19                	je     f0102a15 <mem_init+0x15e5>
f01029fc:	68 1c 62 10 f0       	push   $0xf010621c
f0102a01:	68 d8 50 10 f0       	push   $0xf01050d8
f0102a06:	68 e5 04 00 00       	push   $0x4e5
f0102a0b:	68 41 5f 10 f0       	push   $0xf0105f41
f0102a10:	e8 37 d6 ff ff       	call   f010004c <_panic>
	assert(pp1->pp_ref == 0);
f0102a15:	66 83 7f 04 00       	cmpw   $0x0,0x4(%edi)
f0102a1a:	74 19                	je     f0102a35 <mem_init+0x1605>
f0102a1c:	68 86 62 10 f0       	push   $0xf0106286
f0102a21:	68 d8 50 10 f0       	push   $0xf01050d8
f0102a26:	68 e6 04 00 00       	push   $0x4e6
f0102a2b:	68 41 5f 10 f0       	push   $0xf0105f41
f0102a30:	e8 17 d6 ff ff       	call   f010004c <_panic>
	*(uint32_t *)PGSIZE = 0x03030303U;
f0102a35:	c7 05 00 10 00 00 03 	movl   $0x3030303,0x1000
f0102a3c:	03 03 03 
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0102a3f:	89 f0                	mov    %esi,%eax
f0102a41:	2b 05 4c 94 18 f0    	sub    0xf018944c,%eax
f0102a47:	c1 f8 03             	sar    $0x3,%eax
f0102a4a:	c1 e0 0c             	shl    $0xc,%eax
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102a4d:	89 c2                	mov    %eax,%edx
f0102a4f:	c1 ea 0c             	shr    $0xc,%edx
f0102a52:	3b 15 44 94 18 f0    	cmp    0xf0189444,%edx
f0102a58:	72 12                	jb     f0102a6c <mem_init+0x163c>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102a5a:	50                   	push   %eax
f0102a5b:	68 50 57 10 f0       	push   $0xf0105750
f0102a60:	6a 56                	push   $0x56
f0102a62:	68 4d 5f 10 f0       	push   $0xf0105f4d
f0102a67:	e8 e0 d5 ff ff       	call   f010004c <_panic>
	assert(*(uint32_t *)page2kva(pp2) == 0x03030303U);
f0102a6c:	81 b8 00 00 00 f0 03 	cmpl   $0x3030303,-0x10000000(%eax)
f0102a73:	03 03 03 
f0102a76:	74 19                	je     f0102a91 <mem_init+0x1661>
f0102a78:	68 b4 5e 10 f0       	push   $0xf0105eb4
f0102a7d:	68 d8 50 10 f0       	push   $0xf01050d8
f0102a82:	68 e8 04 00 00       	push   $0x4e8
f0102a87:	68 41 5f 10 f0       	push   $0xf0105f41
f0102a8c:	e8 bb d5 ff ff       	call   f010004c <_panic>
	page_remove(kern_pgdir, (void*) PGSIZE);
f0102a91:	83 ec 08             	sub    $0x8,%esp
f0102a94:	68 00 10 00 00       	push   $0x1000
f0102a99:	ff 35 48 94 18 f0    	pushl  0xf0189448
f0102a9f:	e8 8d e8 ff ff       	call   f0101331 <page_remove>
	assert(pp2->pp_ref == 0);
f0102aa4:	83 c4 10             	add    $0x10,%esp
f0102aa7:	66 83 7e 04 00       	cmpw   $0x0,0x4(%esi)
f0102aac:	74 19                	je     f0102ac7 <mem_init+0x1697>
f0102aae:	68 54 62 10 f0       	push   $0xf0106254
f0102ab3:	68 d8 50 10 f0       	push   $0xf01050d8
f0102ab8:	68 ea 04 00 00       	push   $0x4ea
f0102abd:	68 41 5f 10 f0       	push   $0xf0105f41
f0102ac2:	e8 85 d5 ff ff       	call   f010004c <_panic>

	// forcibly take pp0 back
	assert(PTE_ADDR(kern_pgdir[0]) == page2pa(pp0));
f0102ac7:	8b 0d 48 94 18 f0    	mov    0xf0189448,%ecx
f0102acd:	8b 11                	mov    (%ecx),%edx
f0102acf:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f0102ad5:	89 d8                	mov    %ebx,%eax
f0102ad7:	2b 05 4c 94 18 f0    	sub    0xf018944c,%eax
f0102add:	c1 f8 03             	sar    $0x3,%eax
f0102ae0:	c1 e0 0c             	shl    $0xc,%eax
f0102ae3:	39 c2                	cmp    %eax,%edx
f0102ae5:	74 19                	je     f0102b00 <mem_init+0x16d0>
f0102ae7:	68 94 59 10 f0       	push   $0xf0105994
f0102aec:	68 d8 50 10 f0       	push   $0xf01050d8
f0102af1:	68 ed 04 00 00       	push   $0x4ed
f0102af6:	68 41 5f 10 f0       	push   $0xf0105f41
f0102afb:	e8 4c d5 ff ff       	call   f010004c <_panic>
	kern_pgdir[0] = 0;
f0102b00:	c7 01 00 00 00 00    	movl   $0x0,(%ecx)
	assert(pp0->pp_ref == 1);
f0102b06:	66 83 7b 04 01       	cmpw   $0x1,0x4(%ebx)
f0102b0b:	74 19                	je     f0102b26 <mem_init+0x16f6>
f0102b0d:	68 0b 62 10 f0       	push   $0xf010620b
f0102b12:	68 d8 50 10 f0       	push   $0xf01050d8
f0102b17:	68 ef 04 00 00       	push   $0x4ef
f0102b1c:	68 41 5f 10 f0       	push   $0xf0105f41
f0102b21:	e8 26 d5 ff ff       	call   f010004c <_panic>
	pp0->pp_ref = 0;
f0102b26:	66 c7 43 04 00 00    	movw   $0x0,0x4(%ebx)

	// free the pages we took
	page_free(pp0);
f0102b2c:	83 ec 0c             	sub    $0xc,%esp
f0102b2f:	53                   	push   %ebx
f0102b30:	e8 32 e5 ff ff       	call   f0101067 <page_free>

	cprintf("check_page_installed_pgdir() succeeded!\n");
f0102b35:	c7 04 24 e0 5e 10 f0 	movl   $0xf0105ee0,(%esp)
f0102b3c:	e8 50 08 00 00       	call   f0103391 <cprintf>
	cr0 &= ~(CR0_TS|CR0_EM);
	lcr0(cr0);

	// Some more checks, only possible after kern_pgdir is installed.
	check_page_installed_pgdir();
}
f0102b41:	83 c4 10             	add    $0x10,%esp
f0102b44:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0102b47:	5b                   	pop    %ebx
f0102b48:	5e                   	pop    %esi
f0102b49:	5f                   	pop    %edi
f0102b4a:	5d                   	pop    %ebp
f0102b4b:	c3                   	ret    

f0102b4c <print_fl>:

}

void
print_fl()
{	
f0102b4c:	55                   	push   %ebp
f0102b4d:	89 e5                	mov    %esp,%ebp
f0102b4f:	53                   	push   %ebx
f0102b50:	83 ec 10             	sub    $0x10,%esp
	struct PageInfo *pp = page_free_list;
f0102b53:	8b 1d 60 82 18 f0    	mov    0xf0188260,%ebx
	cprintf("Freelist is: ");
f0102b59:	68 0f 63 10 f0       	push   $0xf010630f
f0102b5e:	e8 2e 08 00 00       	call   f0103391 <cprintf>
	while(pp != NULL){
f0102b63:	83 c4 10             	add    $0x10,%esp
f0102b66:	eb 1e                	jmp    f0102b86 <print_fl+0x3a>
		cprintf("-> p%x ", (pp-pages));
f0102b68:	83 ec 08             	sub    $0x8,%esp
f0102b6b:	89 d8                	mov    %ebx,%eax
f0102b6d:	2b 05 4c 94 18 f0    	sub    0xf018944c,%eax
f0102b73:	c1 f8 03             	sar    $0x3,%eax
f0102b76:	50                   	push   %eax
f0102b77:	68 1d 63 10 f0       	push   $0xf010631d
f0102b7c:	e8 10 08 00 00       	call   f0103391 <cprintf>
		pp = pp->pp_link;
f0102b81:	8b 1b                	mov    (%ebx),%ebx
f0102b83:	83 c4 10             	add    $0x10,%esp
void
print_fl()
{	
	struct PageInfo *pp = page_free_list;
	cprintf("Freelist is: ");
	while(pp != NULL){
f0102b86:	85 db                	test   %ebx,%ebx
f0102b88:	75 de                	jne    f0102b68 <print_fl+0x1c>
		cprintf("-> p%x ", (pp-pages));
		pp = pp->pp_link;
	}
	cprintf("->end\n");
f0102b8a:	83 ec 0c             	sub    $0xc,%esp
f0102b8d:	68 25 63 10 f0       	push   $0xf0106325
f0102b92:	e8 fa 07 00 00       	call   f0103391 <cprintf>
}
f0102b97:	83 c4 10             	add    $0x10,%esp
f0102b9a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0102b9d:	c9                   	leave  
f0102b9e:	c3                   	ret    

f0102b9f <tlb_invalidate>:
// Invalidate a TLB entry, but only if the page tables being
// edited are the ones currently in use by the processor.
//
void
tlb_invalidate(pde_t *pgdir, void *va)
{
f0102b9f:	55                   	push   %ebp
f0102ba0:	89 e5                	mov    %esp,%ebp
}

static inline void
invlpg(void *addr)
{
	asm volatile("invlpg (%0)" : : "r" (addr) : "memory");
f0102ba2:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102ba5:	0f 01 38             	invlpg (%eax)
	// Flush the entry only if we're modifying the current address space.
	// For now, there is only one address space, so always invalidate.
	invlpg(va);
}
f0102ba8:	5d                   	pop    %ebp
f0102ba9:	c3                   	ret    

f0102baa <user_mem_check>:
// Returns 0 if the user program can access this range of addresses,
// and -E_FAULT otherwise.
//
int
user_mem_check(struct Env *env, const void *va, size_t len, int perm)
{
f0102baa:	55                   	push   %ebp
f0102bab:	89 e5                	mov    %esp,%ebp
f0102bad:	57                   	push   %edi
f0102bae:	56                   	push   %esi
f0102baf:	53                   	push   %ebx
f0102bb0:	83 ec 1c             	sub    $0x1c,%esp
f0102bb3:	8b 7d 08             	mov    0x8(%ebp),%edi
	num_Page = (end_Va - va_temp)/PGSIZE;

	//PGSIZE
	return 0;*/
		// LAB 3: Your code here.
    perm |= PTE_P;
f0102bb6:	8b 75 14             	mov    0x14(%ebp),%esi
f0102bb9:	83 ce 01             	or     $0x1,%esi

	uintptr_t addr = ROUNDDOWN((uint32_t)va, PGSIZE);
f0102bbc:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f0102bbf:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
	uintptr_t end = ROUNDUP((uint32_t)va + len, PGSIZE);
f0102bc5:	8b 45 10             	mov    0x10(%ebp),%eax
f0102bc8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0102bcb:	8d 84 01 ff 0f 00 00 	lea    0xfff(%ecx,%eax,1),%eax
f0102bd2:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0102bd7:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	while (addr < end) {
f0102bda:	eb 2b                	jmp    f0102c07 <user_mem_check+0x5d>
		if (addr >= ULIM)
f0102bdc:	81 fb ff ff 7f ef    	cmp    $0xef7fffff,%ebx
f0102be2:	77 2f                	ja     f0102c13 <user_mem_check+0x69>
            goto bad;
        
        pte_t *ppte = pgdir_walk(env->env_pgdir, (void *)addr, 0);
f0102be4:	83 ec 04             	sub    $0x4,%esp
f0102be7:	6a 00                	push   $0x0
f0102be9:	53                   	push   %ebx
f0102bea:	ff 77 5c             	pushl  0x5c(%edi)
f0102bed:	e8 f6 e4 ff ff       	call   f01010e8 <pgdir_walk>
        if (!ppte)
f0102bf2:	83 c4 10             	add    $0x10,%esp
f0102bf5:	85 c0                	test   %eax,%eax
f0102bf7:	74 1a                	je     f0102c13 <user_mem_check+0x69>
            goto bad;
        
        if ((*ppte & perm) != perm)
f0102bf9:	89 f2                	mov    %esi,%edx
f0102bfb:	23 10                	and    (%eax),%edx
f0102bfd:	39 d6                	cmp    %edx,%esi
f0102bff:	75 12                	jne    f0102c13 <user_mem_check+0x69>
            goto bad;

        addr += PGSIZE;
f0102c01:	81 c3 00 10 00 00    	add    $0x1000,%ebx
		// LAB 3: Your code here.
    perm |= PTE_P;

	uintptr_t addr = ROUNDDOWN((uint32_t)va, PGSIZE);
	uintptr_t end = ROUNDUP((uint32_t)va + len, PGSIZE);
	while (addr < end) {
f0102c07:	3b 5d e4             	cmp    -0x1c(%ebp),%ebx
f0102c0a:	72 d0                	jb     f0102bdc <user_mem_check+0x32>
            goto bad;

        addr += PGSIZE;
	}

	return 0;
f0102c0c:	b8 00 00 00 00       	mov    $0x0,%eax
f0102c11:	eb 1f                	jmp    f0102c32 <user_mem_check+0x88>

bad:
    user_mem_check_addr = (uintptr_t)va;
    if (user_mem_check_addr < addr)
f0102c13:	3b 5d 0c             	cmp    0xc(%ebp),%ebx
f0102c16:	77 0f                	ja     f0102c27 <user_mem_check+0x7d>
	}

	return 0;

bad:
    user_mem_check_addr = (uintptr_t)va;
f0102c18:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102c1b:	a3 5c 82 18 f0       	mov    %eax,0xf018825c
    if (user_mem_check_addr < addr)
        user_mem_check_addr = addr;
    return -E_FAULT;
f0102c20:	b8 fa ff ff ff       	mov    $0xfffffffa,%eax
f0102c25:	eb 0b                	jmp    f0102c32 <user_mem_check+0x88>
	return 0;

bad:
    user_mem_check_addr = (uintptr_t)va;
    if (user_mem_check_addr < addr)
        user_mem_check_addr = addr;
f0102c27:	89 1d 5c 82 18 f0    	mov    %ebx,0xf018825c
    return -E_FAULT;
f0102c2d:	b8 fa ff ff ff       	mov    $0xfffffffa,%eax
}
f0102c32:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0102c35:	5b                   	pop    %ebx
f0102c36:	5e                   	pop    %esi
f0102c37:	5f                   	pop    %edi
f0102c38:	5d                   	pop    %ebp
f0102c39:	c3                   	ret    

f0102c3a <user_mem_assert>:
// If it cannot, 'env' is destroyed and, if env is the current
// environment, this function will not return.
//
void
user_mem_assert(struct Env *env, const void *va, size_t len, int perm)
{
f0102c3a:	55                   	push   %ebp
f0102c3b:	89 e5                	mov    %esp,%ebp
f0102c3d:	53                   	push   %ebx
f0102c3e:	83 ec 04             	sub    $0x4,%esp
f0102c41:	8b 5d 08             	mov    0x8(%ebp),%ebx
	if (user_mem_check(env, va, len, perm | PTE_U) < 0) {
f0102c44:	8b 45 14             	mov    0x14(%ebp),%eax
f0102c47:	83 c8 04             	or     $0x4,%eax
f0102c4a:	50                   	push   %eax
f0102c4b:	ff 75 10             	pushl  0x10(%ebp)
f0102c4e:	ff 75 0c             	pushl  0xc(%ebp)
f0102c51:	53                   	push   %ebx
f0102c52:	e8 53 ff ff ff       	call   f0102baa <user_mem_check>
f0102c57:	83 c4 10             	add    $0x10,%esp
f0102c5a:	85 c0                	test   %eax,%eax
f0102c5c:	79 21                	jns    f0102c7f <user_mem_assert+0x45>
		cprintf("[%08x] user_mem_check assertion failure for "
f0102c5e:	83 ec 04             	sub    $0x4,%esp
f0102c61:	ff 35 5c 82 18 f0    	pushl  0xf018825c
f0102c67:	ff 73 48             	pushl  0x48(%ebx)
f0102c6a:	68 0c 5f 10 f0       	push   $0xf0105f0c
f0102c6f:	e8 1d 07 00 00       	call   f0103391 <cprintf>
			"va %08x\n", env->env_id, user_mem_check_addr);
		env_destroy(env);	// may not return
f0102c74:	89 1c 24             	mov    %ebx,(%esp)
f0102c77:	e8 1c 06 00 00       	call   f0103298 <env_destroy>
f0102c7c:	83 c4 10             	add    $0x10,%esp
	}
}
f0102c7f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0102c82:	c9                   	leave  
f0102c83:	c3                   	ret    

f0102c84 <region_alloc>:
{
	// LAB 3: Your code here.
	// (But only if you need it for load_icode.)
	//
	int num_Page;
	if(len == 0){
f0102c84:	85 c9                	test   %ecx,%ecx
f0102c86:	0f 84 9b 00 00 00    	je     f0102d27 <region_alloc+0xa3>
// Pages should be writable by user and kernel.
// Panic if any allocation attempt fails.
//
static void
region_alloc(struct Env *e, void *va, size_t len)
{
f0102c8c:	55                   	push   %ebp
f0102c8d:	89 e5                	mov    %esp,%ebp
f0102c8f:	57                   	push   %edi
f0102c90:	56                   	push   %esi
f0102c91:	53                   	push   %ebx
f0102c92:	83 ec 1c             	sub    $0x1c,%esp
	//
	int num_Page;
	if(len == 0){
		return;
	}
	void* end_Va = va + len;
f0102c95:	01 d1                	add    %edx,%ecx
	void* start_Va = (void*)PTE_ADDR(va); //va & ~0xFFF; //round down va
f0102c97:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
f0102c9d:	89 d3                	mov    %edx,%ebx
	//figure out how many pages needs to be allocated
	if(((int)end_Va % PGSIZE) == 0){ //already aligned 
f0102c9f:	f7 c1 ff 0f 00 00    	test   $0xfff,%ecx
f0102ca5:	75 16                	jne    f0102cbd <region_alloc+0x39>
		num_Page = ((end_Va - start_Va) / PGSIZE);
f0102ca7:	29 d1                	sub    %edx,%ecx
f0102ca9:	89 ca                	mov    %ecx,%edx
f0102cab:	85 c9                	test   %ecx,%ecx
f0102cad:	79 06                	jns    f0102cb5 <region_alloc+0x31>
f0102caf:	8d 91 ff 0f 00 00    	lea    0xfff(%ecx),%edx
f0102cb5:	c1 fa 0c             	sar    $0xc,%edx
f0102cb8:	89 55 e4             	mov    %edx,-0x1c(%ebp)
f0102cbb:	eb 17                	jmp    f0102cd4 <region_alloc+0x50>
	}else{
		num_Page = ((end_Va - start_Va) / PGSIZE) + 1;
f0102cbd:	29 d1                	sub    %edx,%ecx
f0102cbf:	89 ca                	mov    %ecx,%edx
f0102cc1:	85 c9                	test   %ecx,%ecx
f0102cc3:	79 06                	jns    f0102ccb <region_alloc+0x47>
f0102cc5:	8d 91 ff 0f 00 00    	lea    0xfff(%ecx),%edx
f0102ccb:	c1 fa 0c             	sar    $0xc,%edx
f0102cce:	8d 4a 01             	lea    0x1(%edx),%ecx
f0102cd1:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
f0102cd4:	89 c7                	mov    %eax,%edi
	}
	//boot_map_region(e->pg_dir, );
	for(int i=0; i<num_Page; i++){
f0102cd6:	be 00 00 00 00       	mov    $0x0,%esi
f0102cdb:	eb 3e                	jmp    f0102d1b <region_alloc+0x97>
		struct PageInfo* pp = page_alloc((!ALLOC_ZERO)); //Does not zero
f0102cdd:	83 ec 0c             	sub    $0xc,%esp
f0102ce0:	6a 00                	push   $0x0
f0102ce2:	e8 0a e3 ff ff       	call   f0100ff1 <page_alloc>
		if(pp == NULL){
f0102ce7:	83 c4 10             	add    $0x10,%esp
f0102cea:	85 c0                	test   %eax,%eax
f0102cec:	75 17                	jne    f0102d05 <region_alloc+0x81>
			panic("allocation attempt fails in region_alloc");
f0102cee:	83 ec 04             	sub    $0x4,%esp
f0102cf1:	68 2c 63 10 f0       	push   $0xf010632c
f0102cf6:	68 30 01 00 00       	push   $0x130
f0102cfb:	68 ae 63 10 f0       	push   $0xf01063ae
f0102d00:	e8 47 d3 ff ff       	call   f010004c <_panic>
			//return -E_NO_MEM; //page table couldn't be allocated
		}
		page_insert(e->env_pgdir, pp, start_Va+i*PGSIZE, PTE_W|PTE_U|PTE_P); //user read and write
f0102d05:	6a 07                	push   $0x7
f0102d07:	53                   	push   %ebx
f0102d08:	50                   	push   %eax
f0102d09:	ff 77 5c             	pushl  0x5c(%edi)
f0102d0c:	e8 b9 e6 ff ff       	call   f01013ca <page_insert>
		num_Page = ((end_Va - start_Va) / PGSIZE);
	}else{
		num_Page = ((end_Va - start_Va) / PGSIZE) + 1;
	}
	//boot_map_region(e->pg_dir, );
	for(int i=0; i<num_Page; i++){
f0102d11:	46                   	inc    %esi
f0102d12:	81 c3 00 10 00 00    	add    $0x1000,%ebx
f0102d18:	83 c4 10             	add    $0x10,%esp
f0102d1b:	39 75 e4             	cmp    %esi,-0x1c(%ebp)
f0102d1e:	7f bd                	jg     f0102cdd <region_alloc+0x59>

	// Hint: It is easier to use region_alloc if the caller can pass
	//   'va' and 'len' values that are not page-aligned.
	//   You should round va down, and round (va + len) up.
	//   (Watch out for corner-cases!)
}
f0102d20:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0102d23:	5b                   	pop    %ebx
f0102d24:	5e                   	pop    %esi
f0102d25:	5f                   	pop    %edi
f0102d26:	5d                   	pop    %ebp
f0102d27:	c3                   	ret    

f0102d28 <envid2env>:
//   On success, sets *env_store to the environment.
//   On error, sets *env_store to NULL.
//
int
envid2env(envid_t envid, struct Env **env_store, bool checkperm)
{
f0102d28:	55                   	push   %ebp
f0102d29:	89 e5                	mov    %esp,%ebp
f0102d2b:	53                   	push   %ebx
f0102d2c:	8b 55 08             	mov    0x8(%ebp),%edx
f0102d2f:	8b 4d 10             	mov    0x10(%ebp),%ecx
	struct Env *e;

	// If envid is zero, return the current environment.
	if (envid == 0) {
f0102d32:	85 d2                	test   %edx,%edx
f0102d34:	75 11                	jne    f0102d47 <envid2env+0x1f>
		*env_store = curenv;
f0102d36:	a1 64 82 18 f0       	mov    0xf0188264,%eax
f0102d3b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0102d3e:	89 01                	mov    %eax,(%ecx)
		return 0;
f0102d40:	b8 00 00 00 00       	mov    $0x0,%eax
f0102d45:	eb 60                	jmp    f0102da7 <envid2env+0x7f>
	// Look up the Env structure via the index part of the envid,
	// then check the env_id field in that struct Env
	// to ensure that the envid is not stale
	// (i.e., does not refer to a _previous_ environment
	// that used the same slot in the envs[] array).
	e = &envs[ENVX(envid)];
f0102d47:	89 d0                	mov    %edx,%eax
f0102d49:	25 ff 03 00 00       	and    $0x3ff,%eax
f0102d4e:	8d 1c 00             	lea    (%eax,%eax,1),%ebx
f0102d51:	01 d8                	add    %ebx,%eax
f0102d53:	c1 e0 05             	shl    $0x5,%eax
f0102d56:	03 05 68 82 18 f0    	add    0xf0188268,%eax
	if (e->env_status == ENV_FREE || e->env_id != envid) {
f0102d5c:	83 78 54 00          	cmpl   $0x0,0x54(%eax)
f0102d60:	74 05                	je     f0102d67 <envid2env+0x3f>
f0102d62:	3b 50 48             	cmp    0x48(%eax),%edx
f0102d65:	74 10                	je     f0102d77 <envid2env+0x4f>
		*env_store = 0;
f0102d67:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102d6a:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		return -E_BAD_ENV;
f0102d70:	b8 fe ff ff ff       	mov    $0xfffffffe,%eax
f0102d75:	eb 30                	jmp    f0102da7 <envid2env+0x7f>
	// Check that the calling environment has legitimate permission
	// to manipulate the specified environment.
	// If checkperm is set, the specified environment
	// must be either the current environment
	// or an immediate child of the current environment.
	if (checkperm && e != curenv && e->env_parent_id != curenv->env_id) {
f0102d77:	84 c9                	test   %cl,%cl
f0102d79:	74 22                	je     f0102d9d <envid2env+0x75>
f0102d7b:	8b 15 64 82 18 f0    	mov    0xf0188264,%edx
f0102d81:	39 d0                	cmp    %edx,%eax
f0102d83:	74 18                	je     f0102d9d <envid2env+0x75>
f0102d85:	8b 5a 48             	mov    0x48(%edx),%ebx
f0102d88:	39 58 4c             	cmp    %ebx,0x4c(%eax)
f0102d8b:	74 10                	je     f0102d9d <envid2env+0x75>
		*env_store = 0;
f0102d8d:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102d90:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		return -E_BAD_ENV;
f0102d96:	b8 fe ff ff ff       	mov    $0xfffffffe,%eax
f0102d9b:	eb 0a                	jmp    f0102da7 <envid2env+0x7f>
	}

	*env_store = e;
f0102d9d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f0102da0:	89 03                	mov    %eax,(%ebx)
	return 0;
f0102da2:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0102da7:	5b                   	pop    %ebx
f0102da8:	5d                   	pop    %ebp
f0102da9:	c3                   	ret    

f0102daa <env_init_percpu>:
}

// Load GDT and segment descriptors.
void
env_init_percpu(void)
{
f0102daa:	55                   	push   %ebp
f0102dab:	89 e5                	mov    %esp,%ebp
}

static inline void
lgdt(void *p)
{
	asm volatile("lgdt (%0)" : : "r" (p));
f0102dad:	b8 00 c3 11 f0       	mov    $0xf011c300,%eax
f0102db2:	0f 01 10             	lgdtl  (%eax)
	lgdt(&gdt_pd);
	// The kernel never uses GS or FS, so we leave those set to
	// the user data segment.
	asm volatile("movw %%ax,%%gs" : : "a" (GD_UD|3));
f0102db5:	b8 23 00 00 00       	mov    $0x23,%eax
f0102dba:	8e e8                	mov    %eax,%gs
	asm volatile("movw %%ax,%%fs" : : "a" (GD_UD|3));
f0102dbc:	8e e0                	mov    %eax,%fs
	// The kernel does use ES, DS, and SS.  We'll change between
	// the kernel and user data segments as needed.
	asm volatile("movw %%ax,%%es" : : "a" (GD_KD));
f0102dbe:	b8 10 00 00 00       	mov    $0x10,%eax
f0102dc3:	8e c0                	mov    %eax,%es
	asm volatile("movw %%ax,%%ds" : : "a" (GD_KD));
f0102dc5:	8e d8                	mov    %eax,%ds
	asm volatile("movw %%ax,%%ss" : : "a" (GD_KD));
f0102dc7:	8e d0                	mov    %eax,%ss
	// Load the kernel text segment into CS.
	asm volatile("ljmp %0,$1f\n 1:\n" : : "i" (GD_KT));
f0102dc9:	ea d0 2d 10 f0 08 00 	ljmp   $0x8,$0xf0102dd0
}

static inline void
lldt(uint16_t sel)
{
	asm volatile("lldt %0" : : "r" (sel));
f0102dd0:	b8 00 00 00 00       	mov    $0x0,%eax
f0102dd5:	0f 00 d0             	lldt   %ax
	// For good measure, clear the local descriptor table (LDT),
	// since we don't use it.
	lldt(0);
}
f0102dd8:	5d                   	pop    %ebp
f0102dd9:	c3                   	ret    

f0102dda <env_init>:
	env_init_percpu();*/

	//reference code:
	int i;
	for (i = 0; i != NENV - 1; ++i) {
		envs[i].env_id = 0;
f0102dda:	8b 15 68 82 18 f0    	mov    0xf0188268,%edx
f0102de0:	8d 42 60             	lea    0x60(%edx),%eax
f0102de3:	81 c2 00 80 01 00    	add    $0x18000,%edx
f0102de9:	c7 40 e8 00 00 00 00 	movl   $0x0,-0x18(%eax)
		envs[i].env_link = &envs[i + 1];
f0102df0:	89 40 e4             	mov    %eax,-0x1c(%eax)
f0102df3:	83 c0 60             	add    $0x60,%eax
	// Per-CPU part of the initialization
	env_init_percpu();*/

	//reference code:
	int i;
	for (i = 0; i != NENV - 1; ++i) {
f0102df6:	39 d0                	cmp    %edx,%eax
f0102df8:	75 ef                	jne    f0102de9 <env_init+0xf>
// they are in the envs array (i.e., so that the first call to
// env_alloc() returns envs[0]).
//
void
env_init(void)
{
f0102dfa:	55                   	push   %ebp
f0102dfb:	89 e5                	mov    %esp,%ebp
	int i;
	for (i = 0; i != NENV - 1; ++i) {
		envs[i].env_id = 0;
		envs[i].env_link = &envs[i + 1];
	}
	envs[NENV - 1].env_link = NULL;
f0102dfd:	a1 68 82 18 f0       	mov    0xf0188268,%eax
f0102e02:	c7 80 e4 7f 01 00 00 	movl   $0x0,0x17fe4(%eax)
f0102e09:	00 00 00 
	env_free_list = &envs[0];
f0102e0c:	a3 6c 82 18 f0       	mov    %eax,0xf018826c

	// Per-CPU part of the initialization
	env_init_percpu();
f0102e11:	e8 94 ff ff ff       	call   f0102daa <env_init_percpu>
}
f0102e16:	5d                   	pop    %ebp
f0102e17:	c3                   	ret    

f0102e18 <env_alloc>:
//	-E_NO_FREE_ENV if all NENVS environments are allocated
//	-E_NO_MEM on memory exhaustion
//
int
env_alloc(struct Env **newenv_store, envid_t parent_id)
{
f0102e18:	55                   	push   %ebp
f0102e19:	89 e5                	mov    %esp,%ebp
f0102e1b:	56                   	push   %esi
f0102e1c:	53                   	push   %ebx
	int32_t generation;
	int r;
	struct Env *e;

	if (!(e = env_free_list))
f0102e1d:	8b 1d 6c 82 18 f0    	mov    0xf018826c,%ebx
f0102e23:	85 db                	test   %ebx,%ebx
f0102e25:	0f 84 61 01 00 00    	je     f0102f8c <env_alloc+0x174>
{
	int i;
	struct PageInfo *p = NULL;

	// Allocate a page for the page directory
	if (!(p = page_alloc(ALLOC_ZERO))){
f0102e2b:	83 ec 0c             	sub    $0xc,%esp
f0102e2e:	6a 01                	push   $0x1
f0102e30:	e8 bc e1 ff ff       	call   f0100ff1 <page_alloc>
f0102e35:	83 c4 10             	add    $0x10,%esp
f0102e38:	85 c0                	test   %eax,%eax
f0102e3a:	0f 84 53 01 00 00    	je     f0102f93 <env_alloc+0x17b>
void	user_mem_assert(struct Env *env, const void *va, size_t len, int perm);

static inline physaddr_t
page2pa(struct PageInfo *pp)
{
	return (pp - pages) << PGSHIFT; //... << 12
f0102e40:	89 c2                	mov    %eax,%edx
f0102e42:	2b 15 4c 94 18 f0    	sub    0xf018944c,%edx
f0102e48:	c1 fa 03             	sar    $0x3,%edx
f0102e4b:	c1 e2 0c             	shl    $0xc,%edx
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0102e4e:	89 d1                	mov    %edx,%ecx
f0102e50:	c1 e9 0c             	shr    $0xc,%ecx
f0102e53:	3b 0d 44 94 18 f0    	cmp    0xf0189444,%ecx
f0102e59:	72 12                	jb     f0102e6d <env_alloc+0x55>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0102e5b:	52                   	push   %edx
f0102e5c:	68 50 57 10 f0       	push   $0xf0105750
f0102e61:	6a 56                	push   $0x56
f0102e63:	68 4d 5f 10 f0       	push   $0xf0105f4d
f0102e68:	e8 df d1 ff ff       	call   f010004c <_panic>
	//	is an exception -- you need to increment env_pgdir's
	//	pp_ref for env_free to work correctly.
	//    - The functions in kern/pmap.h are handy.

	// LAB 3: Your code here.
	e->env_pgdir = (pde_t *)page2kva(p); //page2kva() ??
f0102e6d:	81 ea 00 00 00 10    	sub    $0x10000000,%edx
f0102e73:	89 53 5c             	mov    %edx,0x5c(%ebx)
f0102e76:	ba ec 0e 00 00       	mov    $0xeec,%edx
	//for(int i=PDX(ULIM); i<1024; i++){ //everything above ULIM is kernel!
	for(int i=PDX(UTOP); i<1024; i++){
		e->env_pgdir[i] = kern_pgdir[i];
f0102e7b:	8b 0d 48 94 18 f0    	mov    0xf0189448,%ecx
f0102e81:	8b 34 11             	mov    (%ecx,%edx,1),%esi
f0102e84:	8b 4b 5c             	mov    0x5c(%ebx),%ecx
f0102e87:	89 34 11             	mov    %esi,(%ecx,%edx,1)
f0102e8a:	83 c2 04             	add    $0x4,%edx
	//    - The functions in kern/pmap.h are handy.

	// LAB 3: Your code here.
	e->env_pgdir = (pde_t *)page2kva(p); //page2kva() ??
	//for(int i=PDX(ULIM); i<1024; i++){ //everything above ULIM is kernel!
	for(int i=PDX(UTOP); i<1024; i++){
f0102e8d:	81 fa 00 10 00 00    	cmp    $0x1000,%edx
f0102e93:	75 e6                	jne    f0102e7b <env_alloc+0x63>
		e->env_pgdir[i] = kern_pgdir[i];
	}
	p->pp_ref++;
f0102e95:	66 ff 40 04          	incw   0x4(%eax)
	// UVPT maps the env's own page table read-only.
	// Permissions: kernel R, user R
	e->env_pgdir[PDX(UVPT)] = PADDR(e->env_pgdir) | PTE_P | PTE_U;
f0102e99:	8b 43 5c             	mov    0x5c(%ebx),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0102e9c:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0102ea1:	77 15                	ja     f0102eb8 <env_alloc+0xa0>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0102ea3:	50                   	push   %eax
f0102ea4:	68 98 58 10 f0       	push   $0xf0105898
f0102ea9:	68 d0 00 00 00       	push   $0xd0
f0102eae:	68 ae 63 10 f0       	push   $0xf01063ae
f0102eb3:	e8 94 d1 ff ff       	call   f010004c <_panic>
f0102eb8:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f0102ebe:	83 ca 05             	or     $0x5,%edx
f0102ec1:	89 90 f4 0e 00 00    	mov    %edx,0xef4(%eax)

	// Allocate and set up the page directory for this environment.
	if ((r = env_setup_vm(e)) < 0)
		return r;
	// Generate an env_id for this environment.
	generation = (e->env_id + (1 << ENVGENSHIFT)) & ~(NENV - 1);
f0102ec7:	8b 43 48             	mov    0x48(%ebx),%eax
f0102eca:	05 00 10 00 00       	add    $0x1000,%eax
	if (generation <= 0)	// Don't create a negative env_id.
f0102ecf:	25 00 fc ff ff       	and    $0xfffffc00,%eax
f0102ed4:	7f 05                	jg     f0102edb <env_alloc+0xc3>
		generation = 1 << ENVGENSHIFT;
f0102ed6:	b8 00 10 00 00       	mov    $0x1000,%eax
	e->env_id = generation | (e - envs);
f0102edb:	89 da                	mov    %ebx,%edx
f0102edd:	2b 15 68 82 18 f0    	sub    0xf0188268,%edx
f0102ee3:	c1 fa 05             	sar    $0x5,%edx
f0102ee6:	8d 0c 92             	lea    (%edx,%edx,4),%ecx
f0102ee9:	8d 0c 8a             	lea    (%edx,%ecx,4),%ecx
f0102eec:	8d 34 8a             	lea    (%edx,%ecx,4),%esi
f0102eef:	89 f1                	mov    %esi,%ecx
f0102ef1:	c1 e1 08             	shl    $0x8,%ecx
f0102ef4:	01 ce                	add    %ecx,%esi
f0102ef6:	89 f1                	mov    %esi,%ecx
f0102ef8:	c1 e1 10             	shl    $0x10,%ecx
f0102efb:	01 f1                	add    %esi,%ecx
f0102efd:	01 c9                	add    %ecx,%ecx
f0102eff:	01 ca                	add    %ecx,%edx
f0102f01:	09 c2                	or     %eax,%edx
f0102f03:	89 53 48             	mov    %edx,0x48(%ebx)

	// Set the basic status variables.
	e->env_parent_id = parent_id;
f0102f06:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102f09:	89 43 4c             	mov    %eax,0x4c(%ebx)
	e->env_type = ENV_TYPE_USER;
f0102f0c:	c7 43 50 00 00 00 00 	movl   $0x0,0x50(%ebx)
	e->env_status = ENV_RUNNABLE;
f0102f13:	c7 43 54 02 00 00 00 	movl   $0x2,0x54(%ebx)
	e->env_runs = 0;
f0102f1a:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)

	// Clear out all the saved register state,
	// to prevent the register values
	// of a prior environment inhabiting this Env structure
	// from "leaking" into our new environment.
	memset(&e->env_tf, 0, sizeof(e->env_tf));
f0102f21:	83 ec 04             	sub    $0x4,%esp
f0102f24:	6a 44                	push   $0x44
f0102f26:	6a 00                	push   $0x0
f0102f28:	53                   	push   %ebx
f0102f29:	e8 15 1d 00 00       	call   f0104c43 <memset>
	// The low 2 bits of each segment register contains the
	// Requestor Privilege Level (RPL); 3 means user mode.  When
	// we switch privilege levels, the hardware does various
	// checks involving the RPL and the Descriptor Privilege Level
	// (DPL) stored in the descriptors themselves.
	e->env_tf.tf_ds = GD_UD | 3;
f0102f2e:	66 c7 43 24 23 00    	movw   $0x23,0x24(%ebx)
	e->env_tf.tf_es = GD_UD | 3;
f0102f34:	66 c7 43 20 23 00    	movw   $0x23,0x20(%ebx)
	e->env_tf.tf_ss = GD_UD | 3;
f0102f3a:	66 c7 43 40 23 00    	movw   $0x23,0x40(%ebx)
	e->env_tf.tf_esp = USTACKTOP;
f0102f40:	c7 43 3c 00 e0 bf ee 	movl   $0xeebfe000,0x3c(%ebx)
	e->env_tf.tf_cs = GD_UT | 3;
f0102f47:	66 c7 43 34 1b 00    	movw   $0x1b,0x34(%ebx)
	// You will set e->env_tf.tf_eip later.

	// commit the allocation
	env_free_list = e->env_link;
f0102f4d:	8b 43 44             	mov    0x44(%ebx),%eax
f0102f50:	a3 6c 82 18 f0       	mov    %eax,0xf018826c
	*newenv_store = e;
f0102f55:	8b 45 08             	mov    0x8(%ebp),%eax
f0102f58:	89 18                	mov    %ebx,(%eax)

	cprintf("[%08x] new env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
f0102f5a:	8b 53 48             	mov    0x48(%ebx),%edx
f0102f5d:	a1 64 82 18 f0       	mov    0xf0188264,%eax
f0102f62:	83 c4 10             	add    $0x10,%esp
f0102f65:	85 c0                	test   %eax,%eax
f0102f67:	74 05                	je     f0102f6e <env_alloc+0x156>
f0102f69:	8b 40 48             	mov    0x48(%eax),%eax
f0102f6c:	eb 05                	jmp    f0102f73 <env_alloc+0x15b>
f0102f6e:	b8 00 00 00 00       	mov    $0x0,%eax
f0102f73:	83 ec 04             	sub    $0x4,%esp
f0102f76:	52                   	push   %edx
f0102f77:	50                   	push   %eax
f0102f78:	68 b9 63 10 f0       	push   $0xf01063b9
f0102f7d:	e8 0f 04 00 00       	call   f0103391 <cprintf>
	return 0;
f0102f82:	83 c4 10             	add    $0x10,%esp
f0102f85:	b8 00 00 00 00       	mov    $0x0,%eax
f0102f8a:	eb 0c                	jmp    f0102f98 <env_alloc+0x180>
	int32_t generation;
	int r;
	struct Env *e;

	if (!(e = env_free_list))
		return -E_NO_FREE_ENV;
f0102f8c:	b8 fb ff ff ff       	mov    $0xfffffffb,%eax
f0102f91:	eb 05                	jmp    f0102f98 <env_alloc+0x180>
	int i;
	struct PageInfo *p = NULL;

	// Allocate a page for the page directory
	if (!(p = page_alloc(ALLOC_ZERO))){
		return -E_NO_MEM;
f0102f93:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
	env_free_list = e->env_link;
	*newenv_store = e;

	cprintf("[%08x] new env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
	return 0;
}
f0102f98:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0102f9b:	5b                   	pop    %ebx
f0102f9c:	5e                   	pop    %esi
f0102f9d:	5d                   	pop    %ebp
f0102f9e:	c3                   	ret    

f0102f9f <env_create>:
// before running the first user-mode environment.
// The new env's parent ID is set to 0.
//
void
env_create(uint8_t *binary, enum EnvType type)
{
f0102f9f:	55                   	push   %ebp
f0102fa0:	89 e5                	mov    %esp,%ebp
f0102fa2:	57                   	push   %edi
f0102fa3:	56                   	push   %esi
f0102fa4:	53                   	push   %ebx
f0102fa5:	83 ec 34             	sub    $0x34,%esp
f0102fa8:	8b 7d 08             	mov    0x8(%ebp),%edi
	// LAB 3: Your code here.
	struct Env *newenv;
	int env_alloc_Result;
	env_alloc_Result = env_alloc(&newenv, 0); //new env's parent ID is set to 0.
f0102fab:	6a 00                	push   $0x0
f0102fad:	8d 45 e4             	lea    -0x1c(%ebp),%eax
f0102fb0:	50                   	push   %eax
f0102fb1:	e8 62 fe ff ff       	call   f0102e18 <env_alloc>
	if(env_alloc_Result == -E_NO_FREE_ENV){
f0102fb6:	83 c4 10             	add    $0x10,%esp
f0102fb9:	83 f8 fb             	cmp    $0xfffffffb,%eax
f0102fbc:	75 16                	jne    f0102fd4 <env_create+0x35>
		panic("env_create: %e", env_alloc_Result);
f0102fbe:	6a fb                	push   $0xfffffffb
f0102fc0:	68 ce 63 10 f0       	push   $0xf01063ce
f0102fc5:	68 9e 01 00 00       	push   $0x19e
f0102fca:	68 ae 63 10 f0       	push   $0xf01063ae
f0102fcf:	e8 78 d0 ff ff       	call   f010004c <_panic>
	}
	if(env_alloc_Result == -E_NO_MEM){
f0102fd4:	83 f8 fc             	cmp    $0xfffffffc,%eax
f0102fd7:	75 16                	jne    f0102fef <env_create+0x50>
		panic("env_create: %e", env_alloc_Result);
f0102fd9:	6a fc                	push   $0xfffffffc
f0102fdb:	68 ce 63 10 f0       	push   $0xf01063ce
f0102fe0:	68 a1 01 00 00       	push   $0x1a1
f0102fe5:	68 ae 63 10 f0       	push   $0xf01063ae
f0102fea:	e8 5d d0 ff ff       	call   f010004c <_panic>
	}
	newenv->env_type = type;
f0102fef:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0102ff2:	89 c1                	mov    %eax,%ecx
f0102ff4:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f0102ff7:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102ffa:	89 41 50             	mov    %eax,0x50(%ecx)
	//  You must also do something with the program's entry point,
	//  to make sure that the environment starts executing there.
	//  What?  (See env_run() and env_pop_tf() below.)

	// LAB 3: Your code here.
	cprintf("load_icode starts:\n");
f0102ffd:	83 ec 0c             	sub    $0xc,%esp
f0103000:	68 dd 63 10 f0       	push   $0xf01063dd
f0103005:	e8 87 03 00 00       	call   f0103391 <cprintf>

static inline uint32_t
rcr3(void)
{
	uint32_t val;
	asm volatile("movl %%cr3,%0" : "=r" (val));
f010300a:	0f 20 d8             	mov    %cr3,%eax
f010300d:	89 45 d0             	mov    %eax,-0x30(%ebp)
	physaddr_t  old_cr3 = rcr3(); 

	struct Proghdr *ph, *eph;
	if(((struct Elf *)binary)->e_magic != ELF_MAGIC) {
f0103010:	83 c4 10             	add    $0x10,%esp
f0103013:	81 3f 7f 45 4c 46    	cmpl   $0x464c457f,(%edi)
f0103019:	74 17                	je     f0103032 <env_create+0x93>
		panic("Invalid binary in load_icode.\n");
f010301b:	83 ec 04             	sub    $0x4,%esp
f010301e:	68 58 63 10 f0       	push   $0xf0106358
f0103023:	68 77 01 00 00       	push   $0x177
f0103028:	68 ae 63 10 f0       	push   $0xf01063ae
f010302d:	e8 1a d0 ff ff       	call   f010004c <_panic>
	}
	ph = (struct Proghdr *) ((uint8_t *) binary + ((struct Elf *)binary)->e_phoff);
f0103032:	89 fb                	mov    %edi,%ebx
f0103034:	03 5f 1c             	add    0x1c(%edi),%ebx
	eph = ph + ((struct Elf *)binary)->e_phnum;
f0103037:	0f b7 77 2c          	movzwl 0x2c(%edi),%esi
f010303b:	c1 e6 05             	shl    $0x5,%esi
f010303e:	01 de                	add    %ebx,%esi
	lcr3( PADDR(e->env_pgdir) );
f0103040:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0103043:	8b 40 5c             	mov    0x5c(%eax),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0103046:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f010304b:	77 15                	ja     f0103062 <env_create+0xc3>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f010304d:	50                   	push   %eax
f010304e:	68 98 58 10 f0       	push   $0xf0105898
f0103053:	68 7b 01 00 00       	push   $0x17b
f0103058:	68 ae 63 10 f0       	push   $0xf01063ae
f010305d:	e8 ea cf ff ff       	call   f010004c <_panic>
}

static inline void
lcr3(uint32_t val)
{
	asm volatile("movl %0,%%cr3" : : "r" (val));
f0103062:	05 00 00 00 10       	add    $0x10000000,%eax
f0103067:	0f 22 d8             	mov    %eax,%cr3
f010306a:	eb 44                	jmp    f01030b0 <env_create+0x111>

	for (; ph < eph; ph++){
		if(ph->p_type == ELF_PROG_LOAD){
f010306c:	83 3b 01             	cmpl   $0x1,(%ebx)
f010306f:	75 3c                	jne    f01030ad <env_create+0x10e>
			region_alloc(e, (void *)ph->p_va, ph->p_memsz);
f0103071:	8b 4b 14             	mov    0x14(%ebx),%ecx
f0103074:	8b 53 08             	mov    0x8(%ebx),%edx
f0103077:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f010307a:	e8 05 fc ff ff       	call   f0102c84 <region_alloc>
			memmove((void *)ph->p_va,(binary  + ph->p_offset), ph->p_filesz);
f010307f:	83 ec 04             	sub    $0x4,%esp
f0103082:	ff 73 10             	pushl  0x10(%ebx)
f0103085:	89 f8                	mov    %edi,%eax
f0103087:	03 43 04             	add    0x4(%ebx),%eax
f010308a:	50                   	push   %eax
f010308b:	ff 73 08             	pushl  0x8(%ebx)
f010308e:	e8 fd 1b 00 00       	call   f0104c90 <memmove>
			memset((void *)ph->p_va+ph->p_filesz, 0, ph->p_memsz-ph->p_filesz);
f0103093:	8b 43 10             	mov    0x10(%ebx),%eax
f0103096:	83 c4 0c             	add    $0xc,%esp
f0103099:	8b 53 14             	mov    0x14(%ebx),%edx
f010309c:	29 c2                	sub    %eax,%edx
f010309e:	52                   	push   %edx
f010309f:	6a 00                	push   $0x0
f01030a1:	03 43 08             	add    0x8(%ebx),%eax
f01030a4:	50                   	push   %eax
f01030a5:	e8 99 1b 00 00       	call   f0104c43 <memset>
f01030aa:	83 c4 10             	add    $0x10,%esp
	}
	ph = (struct Proghdr *) ((uint8_t *) binary + ((struct Elf *)binary)->e_phoff);
	eph = ph + ((struct Elf *)binary)->e_phnum;
	lcr3( PADDR(e->env_pgdir) );

	for (; ph < eph; ph++){
f01030ad:	83 c3 20             	add    $0x20,%ebx
f01030b0:	39 de                	cmp    %ebx,%esi
f01030b2:	77 b8                	ja     f010306c <env_create+0xcd>
f01030b4:	8b 45 d0             	mov    -0x30(%ebp),%eax
f01030b7:	0f 22 d8             	mov    %eax,%cr3
	}

	lcr3(old_cr3);
	// Now map one page for the program's initial stack
	// at virtual address USTACKTOP - PGSIZE.
	(e->env_tf).tf_eip = ((struct Elf *)binary)->e_entry;
f01030ba:	8b 47 18             	mov    0x18(%edi),%eax
f01030bd:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f01030c0:	89 47 30             	mov    %eax,0x30(%edi)
	region_alloc(e, (void*)(USTACKTOP - PGSIZE), PGSIZE);
f01030c3:	b9 00 10 00 00       	mov    $0x1000,%ecx
f01030c8:	ba 00 d0 bf ee       	mov    $0xeebfd000,%edx
f01030cd:	89 f8                	mov    %edi,%eax
f01030cf:	e8 b0 fb ff ff       	call   f0102c84 <region_alloc>
		panic("env_create: %e", env_alloc_Result);
	}
	newenv->env_type = type;
	load_icode(newenv, binary);

}
f01030d4:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01030d7:	5b                   	pop    %ebx
f01030d8:	5e                   	pop    %esi
f01030d9:	5f                   	pop    %edi
f01030da:	5d                   	pop    %ebp
f01030db:	c3                   	ret    

f01030dc <env_free>:
//
// Frees env e and all memory it uses.
//
void
env_free(struct Env *e)
{
f01030dc:	55                   	push   %ebp
f01030dd:	89 e5                	mov    %esp,%ebp
f01030df:	57                   	push   %edi
f01030e0:	56                   	push   %esi
f01030e1:	53                   	push   %ebx
f01030e2:	83 ec 1c             	sub    $0x1c,%esp
f01030e5:	8b 7d 08             	mov    0x8(%ebp),%edi
	physaddr_t pa;

	// If freeing the current environment, switch to kern_pgdir
	// before freeing the page directory, just in case the page
	// gets reused.
	if (e == curenv)
f01030e8:	8b 15 64 82 18 f0    	mov    0xf0188264,%edx
f01030ee:	39 fa                	cmp    %edi,%edx
f01030f0:	75 29                	jne    f010311b <env_free+0x3f>
		lcr3(PADDR(kern_pgdir));
f01030f2:	a1 48 94 18 f0       	mov    0xf0189448,%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f01030f7:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f01030fc:	77 15                	ja     f0103113 <env_free+0x37>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f01030fe:	50                   	push   %eax
f01030ff:	68 98 58 10 f0       	push   $0xf0105898
f0103104:	68 b6 01 00 00       	push   $0x1b6
f0103109:	68 ae 63 10 f0       	push   $0xf01063ae
f010310e:	e8 39 cf ff ff       	call   f010004c <_panic>
f0103113:	05 00 00 00 10       	add    $0x10000000,%eax
f0103118:	0f 22 d8             	mov    %eax,%cr3

	// Note the environment's demise.
	cprintf("[%08x] free env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
f010311b:	8b 4f 48             	mov    0x48(%edi),%ecx
f010311e:	85 d2                	test   %edx,%edx
f0103120:	74 05                	je     f0103127 <env_free+0x4b>
f0103122:	8b 42 48             	mov    0x48(%edx),%eax
f0103125:	eb 05                	jmp    f010312c <env_free+0x50>
f0103127:	b8 00 00 00 00       	mov    $0x0,%eax
f010312c:	83 ec 04             	sub    $0x4,%esp
f010312f:	51                   	push   %ecx
f0103130:	50                   	push   %eax
f0103131:	68 f1 63 10 f0       	push   $0xf01063f1
f0103136:	e8 56 02 00 00       	call   f0103391 <cprintf>
f010313b:	83 c4 10             	add    $0x10,%esp
f010313e:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

	// Flush all mapped pages in the user portion of the address space
	static_assert(UTOP % PTSIZE == 0);
	for (pdeno = 0; pdeno < PDX(UTOP); pdeno++) {
f0103145:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)

		// only look at mapped page tables
		if (!(e->env_pgdir[pdeno] & PTE_P))
f010314c:	8b 47 5c             	mov    0x5c(%edi),%eax
f010314f:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0103152:	8b 34 10             	mov    (%eax,%edx,1),%esi
f0103155:	f7 c6 01 00 00 00    	test   $0x1,%esi
f010315b:	0f 84 a6 00 00 00    	je     f0103207 <env_free+0x12b>
			continue;

		// find the pa and va of the page table
		pa = PTE_ADDR(e->env_pgdir[pdeno]);
f0103161:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
#define KADDR(pa) _kaddr(__FILE__, __LINE__, pa)

static inline void*
_kaddr(const char *file, int line, physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0103167:	89 f0                	mov    %esi,%eax
f0103169:	c1 e8 0c             	shr    $0xc,%eax
f010316c:	89 45 d8             	mov    %eax,-0x28(%ebp)
f010316f:	39 05 44 94 18 f0    	cmp    %eax,0xf0189444
f0103175:	77 15                	ja     f010318c <env_free+0xb0>
		_panic(file, line, "KADDR called with invalid pa %08lx", pa);
f0103177:	56                   	push   %esi
f0103178:	68 50 57 10 f0       	push   $0xf0105750
f010317d:	68 c5 01 00 00       	push   $0x1c5
f0103182:	68 ae 63 10 f0       	push   $0xf01063ae
f0103187:	e8 c0 ce ff ff       	call   f010004c <_panic>
		pt = (pte_t*) KADDR(pa);

		// unmap all PTEs in this page table
		for (pteno = 0; pteno <= PTX(~0); pteno++) {
			if (pt[pteno] & PTE_P)
				page_remove(e->env_pgdir, PGADDR(pdeno, pteno, 0));
f010318c:	8b 45 dc             	mov    -0x24(%ebp),%eax
f010318f:	c1 e0 16             	shl    $0x16,%eax
f0103192:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		// find the pa and va of the page table
		pa = PTE_ADDR(e->env_pgdir[pdeno]);
		pt = (pte_t*) KADDR(pa);

		// unmap all PTEs in this page table
		for (pteno = 0; pteno <= PTX(~0); pteno++) {
f0103195:	bb 00 00 00 00       	mov    $0x0,%ebx
			if (pt[pteno] & PTE_P)
f010319a:	f6 84 9e 00 00 00 f0 	testb  $0x1,-0x10000000(%esi,%ebx,4)
f01031a1:	01 
f01031a2:	74 17                	je     f01031bb <env_free+0xdf>
				page_remove(e->env_pgdir, PGADDR(pdeno, pteno, 0));
f01031a4:	83 ec 08             	sub    $0x8,%esp
f01031a7:	89 d8                	mov    %ebx,%eax
f01031a9:	c1 e0 0c             	shl    $0xc,%eax
f01031ac:	0b 45 e4             	or     -0x1c(%ebp),%eax
f01031af:	50                   	push   %eax
f01031b0:	ff 77 5c             	pushl  0x5c(%edi)
f01031b3:	e8 79 e1 ff ff       	call   f0101331 <page_remove>
f01031b8:	83 c4 10             	add    $0x10,%esp
		// find the pa and va of the page table
		pa = PTE_ADDR(e->env_pgdir[pdeno]);
		pt = (pte_t*) KADDR(pa);

		// unmap all PTEs in this page table
		for (pteno = 0; pteno <= PTX(~0); pteno++) {
f01031bb:	43                   	inc    %ebx
f01031bc:	81 fb 00 04 00 00    	cmp    $0x400,%ebx
f01031c2:	75 d6                	jne    f010319a <env_free+0xbe>
			if (pt[pteno] & PTE_P)
				page_remove(e->env_pgdir, PGADDR(pdeno, pteno, 0));
		}

		// free the page table itself
		e->env_pgdir[pdeno] = 0;
f01031c4:	8b 47 5c             	mov    0x5c(%edi),%eax
f01031c7:	8b 55 e0             	mov    -0x20(%ebp),%edx
f01031ca:	c7 04 10 00 00 00 00 	movl   $0x0,(%eax,%edx,1)
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f01031d1:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01031d4:	3b 05 44 94 18 f0    	cmp    0xf0189444,%eax
f01031da:	72 14                	jb     f01031f0 <env_free+0x114>
		panic("pa2page called with invalid pa");
f01031dc:	83 ec 04             	sub    $0x4,%esp
f01031df:	68 38 58 10 f0       	push   $0xf0105838
f01031e4:	6a 4f                	push   $0x4f
f01031e6:	68 4d 5f 10 f0       	push   $0xf0105f4d
f01031eb:	e8 5c ce ff ff       	call   f010004c <_panic>
		page_decref(pa2page(pa));
f01031f0:	83 ec 0c             	sub    $0xc,%esp
f01031f3:	a1 4c 94 18 f0       	mov    0xf018944c,%eax
f01031f8:	8b 55 d8             	mov    -0x28(%ebp),%edx
f01031fb:	8d 04 d0             	lea    (%eax,%edx,8),%eax
f01031fe:	50                   	push   %eax
f01031ff:	e8 c0 de ff ff       	call   f01010c4 <page_decref>
f0103204:	83 c4 10             	add    $0x10,%esp
	// Note the environment's demise.
	cprintf("[%08x] free env %08x\n", curenv ? curenv->env_id : 0, e->env_id);

	// Flush all mapped pages in the user portion of the address space
	static_assert(UTOP % PTSIZE == 0);
	for (pdeno = 0; pdeno < PDX(UTOP); pdeno++) {
f0103207:	ff 45 dc             	incl   -0x24(%ebp)
f010320a:	83 45 e0 04          	addl   $0x4,-0x20(%ebp)
f010320e:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0103211:	3d ec 0e 00 00       	cmp    $0xeec,%eax
f0103216:	0f 85 30 ff ff ff    	jne    f010314c <env_free+0x70>
		e->env_pgdir[pdeno] = 0;
		page_decref(pa2page(pa));
	}

	// free the page directory
	pa = PADDR(e->env_pgdir);
f010321c:	8b 47 5c             	mov    0x5c(%edi),%eax
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f010321f:	3d ff ff ff ef       	cmp    $0xefffffff,%eax
f0103224:	77 15                	ja     f010323b <env_free+0x15f>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0103226:	50                   	push   %eax
f0103227:	68 98 58 10 f0       	push   $0xf0105898
f010322c:	68 d3 01 00 00       	push   $0x1d3
f0103231:	68 ae 63 10 f0       	push   $0xf01063ae
f0103236:	e8 11 ce ff ff       	call   f010004c <_panic>
	e->env_pgdir = 0;
f010323b:	c7 47 5c 00 00 00 00 	movl   $0x0,0x5c(%edi)
}

static inline struct PageInfo*
pa2page(physaddr_t pa)
{
	if (PGNUM(pa) >= npages)
f0103242:	05 00 00 00 10       	add    $0x10000000,%eax
f0103247:	c1 e8 0c             	shr    $0xc,%eax
f010324a:	3b 05 44 94 18 f0    	cmp    0xf0189444,%eax
f0103250:	72 14                	jb     f0103266 <env_free+0x18a>
		panic("pa2page called with invalid pa");
f0103252:	83 ec 04             	sub    $0x4,%esp
f0103255:	68 38 58 10 f0       	push   $0xf0105838
f010325a:	6a 4f                	push   $0x4f
f010325c:	68 4d 5f 10 f0       	push   $0xf0105f4d
f0103261:	e8 e6 cd ff ff       	call   f010004c <_panic>
	page_decref(pa2page(pa));
f0103266:	83 ec 0c             	sub    $0xc,%esp
f0103269:	8b 15 4c 94 18 f0    	mov    0xf018944c,%edx
f010326f:	8d 04 c2             	lea    (%edx,%eax,8),%eax
f0103272:	50                   	push   %eax
f0103273:	e8 4c de ff ff       	call   f01010c4 <page_decref>

	// return the environment to the free list
	e->env_status = ENV_FREE;
f0103278:	c7 47 54 00 00 00 00 	movl   $0x0,0x54(%edi)
	e->env_link = env_free_list;
f010327f:	a1 6c 82 18 f0       	mov    0xf018826c,%eax
f0103284:	89 47 44             	mov    %eax,0x44(%edi)
	env_free_list = e;
f0103287:	89 3d 6c 82 18 f0    	mov    %edi,0xf018826c
}
f010328d:	83 c4 10             	add    $0x10,%esp
f0103290:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0103293:	5b                   	pop    %ebx
f0103294:	5e                   	pop    %esi
f0103295:	5f                   	pop    %edi
f0103296:	5d                   	pop    %ebp
f0103297:	c3                   	ret    

f0103298 <env_destroy>:
//
// Frees environment e.
//
void
env_destroy(struct Env *e)
{
f0103298:	55                   	push   %ebp
f0103299:	89 e5                	mov    %esp,%ebp
f010329b:	83 ec 14             	sub    $0x14,%esp
	env_free(e);
f010329e:	ff 75 08             	pushl  0x8(%ebp)
f01032a1:	e8 36 fe ff ff       	call   f01030dc <env_free>

	cprintf("Destroyed the only environment - nothing more to do!\n");
f01032a6:	c7 04 24 78 63 10 f0 	movl   $0xf0106378,(%esp)
f01032ad:	e8 df 00 00 00       	call   f0103391 <cprintf>
f01032b2:	83 c4 10             	add    $0x10,%esp
	while (1)
		monitor(NULL);
f01032b5:	83 ec 0c             	sub    $0xc,%esp
f01032b8:	6a 00                	push   $0x0
f01032ba:	e8 d4 d5 ff ff       	call   f0100893 <monitor>
f01032bf:	83 c4 10             	add    $0x10,%esp
f01032c2:	eb f1                	jmp    f01032b5 <env_destroy+0x1d>

f01032c4 <env_pop_tf>:
//
// This function does not return.
//
void
env_pop_tf(struct Trapframe *tf)
{
f01032c4:	55                   	push   %ebp
f01032c5:	89 e5                	mov    %esp,%ebp
f01032c7:	83 ec 0c             	sub    $0xc,%esp
	asm volatile(
f01032ca:	8b 65 08             	mov    0x8(%ebp),%esp
f01032cd:	61                   	popa   
f01032ce:	07                   	pop    %es
f01032cf:	1f                   	pop    %ds
f01032d0:	83 c4 08             	add    $0x8,%esp
f01032d3:	cf                   	iret   
		"\tpopl %%es\n"
		"\tpopl %%ds\n"
		"\taddl $0x8,%%esp\n" /* skip tf_trapno and tf_errcode */
		"\tiret\n"
		: : "g" (tf) : "memory");
	panic("iret failed");  /* mostly to placate the compiler */
f01032d4:	68 07 64 10 f0       	push   $0xf0106407
f01032d9:	68 fc 01 00 00       	push   $0x1fc
f01032de:	68 ae 63 10 f0       	push   $0xf01063ae
f01032e3:	e8 64 cd ff ff       	call   f010004c <_panic>

f01032e8 <env_run>:
//
// This function does not return.
//
void
env_run(struct Env *e)
{
f01032e8:	55                   	push   %ebp
f01032e9:	89 e5                	mov    %esp,%ebp
f01032eb:	83 ec 08             	sub    $0x8,%esp
f01032ee:	8b 55 08             	mov    0x8(%ebp),%edx
	//if(curenv == NULL && (curenv->env_id == e->env_id) ){
	//	goto end;
	//}
	//if(curenv != NULL && (curenv->env_id != e->env_id)){ //check if it's a context switch
	
	if(curenv != NULL && (curenv->env_id != e->env_id)){
f01032f1:	a1 64 82 18 f0       	mov    0xf0188264,%eax
f01032f6:	85 c0                	test   %eax,%eax
f01032f8:	74 17                	je     f0103311 <env_run+0x29>
f01032fa:	8b 4a 48             	mov    0x48(%edx),%ecx
f01032fd:	39 48 48             	cmp    %ecx,0x48(%eax)
f0103300:	74 0f                	je     f0103311 <env_run+0x29>
		curenv->env_status = ENV_RUNNABLE;
f0103302:	c7 40 54 02 00 00 00 	movl   $0x2,0x54(%eax)
		curenv = e;
f0103309:	89 15 64 82 18 f0    	mov    %edx,0xf0188264
f010330f:	eb 06                	jmp    f0103317 <env_run+0x2f>
	}else{
		curenv = e;
f0103311:	89 15 64 82 18 f0    	mov    %edx,0xf0188264
	}
		//cprintf("In env_run()\n");
		curenv->env_status = ENV_RUNNING;
f0103317:	a1 64 82 18 f0       	mov    0xf0188264,%eax
f010331c:	c7 40 54 03 00 00 00 	movl   $0x3,0x54(%eax)
		(curenv->env_runs)++;
f0103323:	ff 40 58             	incl   0x58(%eax)
		lcr3(PADDR(curenv->env_pgdir));
f0103326:	8b 50 5c             	mov    0x5c(%eax),%edx
#define PADDR(kva) _paddr(__FILE__, __LINE__, kva)

static inline physaddr_t
_paddr(const char *file, int line, void *kva)
{
	if ((uint32_t)kva < KERNBASE)
f0103329:	81 fa ff ff ff ef    	cmp    $0xefffffff,%edx
f010332f:	77 15                	ja     f0103346 <env_run+0x5e>
		_panic(file, line, "PADDR called with invalid kva %08lx", kva);
f0103331:	52                   	push   %edx
f0103332:	68 98 58 10 f0       	push   $0xf0105898
f0103337:	68 28 02 00 00       	push   $0x228
f010333c:	68 ae 63 10 f0       	push   $0xf01063ae
f0103341:	e8 06 cd ff ff       	call   f010004c <_panic>
f0103346:	81 c2 00 00 00 10    	add    $0x10000000,%edx
f010334c:	0f 22 da             	mov    %edx,%cr3
		env_pop_tf(&(curenv->env_tf));
f010334f:	83 ec 0c             	sub    $0xc,%esp
f0103352:	50                   	push   %eax
f0103353:	e8 6c ff ff ff       	call   f01032c4 <env_pop_tf>

f0103358 <putch>:
#include <inc/stdarg.h>


static void
putch(int ch, int *cnt)
{
f0103358:	55                   	push   %ebp
f0103359:	89 e5                	mov    %esp,%ebp
f010335b:	83 ec 14             	sub    $0x14,%esp
	cputchar(ch);
f010335e:	ff 75 08             	pushl  0x8(%ebp)
f0103361:	e8 b0 d2 ff ff       	call   f0100616 <cputchar>
	*cnt++;
}
f0103366:	83 c4 10             	add    $0x10,%esp
f0103369:	c9                   	leave  
f010336a:	c3                   	ret    

f010336b <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
f010336b:	55                   	push   %ebp
f010336c:	89 e5                	mov    %esp,%ebp
f010336e:	83 ec 18             	sub    $0x18,%esp
	int cnt = 0;
f0103371:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	vprintfmt((void*)putch, &cnt, fmt, ap);
f0103378:	ff 75 0c             	pushl  0xc(%ebp)
f010337b:	ff 75 08             	pushl  0x8(%ebp)
f010337e:	8d 45 f4             	lea    -0xc(%ebp),%eax
f0103381:	50                   	push   %eax
f0103382:	68 58 33 10 f0       	push   $0xf0103358
f0103387:	e8 6f 12 00 00       	call   f01045fb <vprintfmt>
	return cnt;
}
f010338c:	8b 45 f4             	mov    -0xc(%ebp),%eax
f010338f:	c9                   	leave  
f0103390:	c3                   	ret    

f0103391 <cprintf>:

int
cprintf(const char *fmt, ...)
{
f0103391:	55                   	push   %ebp
f0103392:	89 e5                	mov    %esp,%ebp
f0103394:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
f0103397:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
f010339a:	50                   	push   %eax
f010339b:	ff 75 08             	pushl  0x8(%ebp)
f010339e:	e8 c8 ff ff ff       	call   f010336b <vcprintf>
	va_end(ap);

	return cnt;
}
f01033a3:	c9                   	leave  
f01033a4:	c3                   	ret    

f01033a5 <trap_init_percpu>:
}

// Initialize and load the per-CPU TSS and IDT
void
trap_init_percpu(void)
{
f01033a5:	55                   	push   %ebp
f01033a6:	89 e5                	mov    %esp,%ebp
	// Setup a TSS so that we get the right stack
	// when we trap to the kernel.
	ts.ts_esp0 = KSTACKTOP;
f01033a8:	b8 a0 8a 18 f0       	mov    $0xf0188aa0,%eax
f01033ad:	c7 05 a4 8a 18 f0 00 	movl   $0xf0000000,0xf0188aa4
f01033b4:	00 00 f0 
	ts.ts_ss0 = GD_KD;
f01033b7:	66 c7 05 a8 8a 18 f0 	movw   $0x10,0xf0188aa8
f01033be:	10 00 

	// Initialize the TSS slot of the gdt.
	gdt[GD_TSS0 >> 3] = SEG16(STS_T32A, (uint32_t) (&ts),
f01033c0:	66 c7 05 48 c3 11 f0 	movw   $0x67,0xf011c348
f01033c7:	67 00 
f01033c9:	66 a3 4a c3 11 f0    	mov    %ax,0xf011c34a
f01033cf:	89 c2                	mov    %eax,%edx
f01033d1:	c1 ea 10             	shr    $0x10,%edx
f01033d4:	88 15 4c c3 11 f0    	mov    %dl,0xf011c34c
f01033da:	c6 05 4e c3 11 f0 40 	movb   $0x40,0xf011c34e
f01033e1:	c1 e8 18             	shr    $0x18,%eax
f01033e4:	a2 4f c3 11 f0       	mov    %al,0xf011c34f
					sizeof(struct Taskstate) - 1, 0);
	gdt[GD_TSS0 >> 3].sd_s = 0;
f01033e9:	c6 05 4d c3 11 f0 89 	movb   $0x89,0xf011c34d
}

static inline void
ltr(uint16_t sel)
{
	asm volatile("ltr %0" : : "r" (sel));
f01033f0:	b8 28 00 00 00       	mov    $0x28,%eax
f01033f5:	0f 00 d8             	ltr    %ax
}

static inline void
lidt(void *p)
{
	asm volatile("lidt (%0)" : : "r" (p));
f01033f8:	b8 50 c3 11 f0       	mov    $0xf011c350,%eax
f01033fd:	0f 01 18             	lidtl  (%eax)
	// bottom three bits are special; we leave them 0)
	ltr(GD_TSS0);

	// Load the IDT
	lidt(&idt_pd);
}
f0103400:	5d                   	pop    %ebp
f0103401:	c3                   	ret    

f0103402 <trap_init>:
    } while (0)


void
trap_init(void)
{
f0103402:	55                   	push   %ebp
f0103403:	89 e5                	mov    %esp,%ebp
	//SETGATE(idt[0], 0, GD_KT, );

	// Note that ISTRAP must be zero for all gates
    // because JOS says when it enters kernel,
    // all interrupts are masked.
	SG(0, 0, 0);
f0103405:	b8 b4 3d 10 f0       	mov    $0xf0103db4,%eax
f010340a:	66 a3 80 82 18 f0    	mov    %ax,0xf0188280
f0103410:	66 c7 05 82 82 18 f0 	movw   $0x8,0xf0188282
f0103417:	08 00 
f0103419:	c6 05 84 82 18 f0 00 	movb   $0x0,0xf0188284
f0103420:	c6 05 85 82 18 f0 8e 	movb   $0x8e,0xf0188285
f0103427:	c1 e8 10             	shr    $0x10,%eax
f010342a:	66 a3 86 82 18 f0    	mov    %ax,0xf0188286
	SG(1, 0, 0);
f0103430:	b8 be 3d 10 f0       	mov    $0xf0103dbe,%eax
f0103435:	66 a3 88 82 18 f0    	mov    %ax,0xf0188288
f010343b:	66 c7 05 8a 82 18 f0 	movw   $0x8,0xf018828a
f0103442:	08 00 
f0103444:	c6 05 8c 82 18 f0 00 	movb   $0x0,0xf018828c
f010344b:	c6 05 8d 82 18 f0 8e 	movb   $0x8e,0xf018828d
f0103452:	c1 e8 10             	shr    $0x10,%eax
f0103455:	66 a3 8e 82 18 f0    	mov    %ax,0xf018828e
	SG(2, 0, 0);
f010345b:	b8 c8 3d 10 f0       	mov    $0xf0103dc8,%eax
f0103460:	66 a3 90 82 18 f0    	mov    %ax,0xf0188290
f0103466:	66 c7 05 92 82 18 f0 	movw   $0x8,0xf0188292
f010346d:	08 00 
f010346f:	c6 05 94 82 18 f0 00 	movb   $0x0,0xf0188294
f0103476:	c6 05 95 82 18 f0 8e 	movb   $0x8e,0xf0188295
f010347d:	c1 e8 10             	shr    $0x10,%eax
f0103480:	66 a3 96 82 18 f0    	mov    %ax,0xf0188296
	SG(3, 0, 3);
f0103486:	b8 d2 3d 10 f0       	mov    $0xf0103dd2,%eax
f010348b:	66 a3 98 82 18 f0    	mov    %ax,0xf0188298
f0103491:	66 c7 05 9a 82 18 f0 	movw   $0x8,0xf018829a
f0103498:	08 00 
f010349a:	c6 05 9c 82 18 f0 00 	movb   $0x0,0xf018829c
f01034a1:	c6 05 9d 82 18 f0 ee 	movb   $0xee,0xf018829d
f01034a8:	c1 e8 10             	shr    $0x10,%eax
f01034ab:	66 a3 9e 82 18 f0    	mov    %ax,0xf018829e
	SG(4, 0, 0);
f01034b1:	b8 dc 3d 10 f0       	mov    $0xf0103ddc,%eax
f01034b6:	66 a3 a0 82 18 f0    	mov    %ax,0xf01882a0
f01034bc:	66 c7 05 a2 82 18 f0 	movw   $0x8,0xf01882a2
f01034c3:	08 00 
f01034c5:	c6 05 a4 82 18 f0 00 	movb   $0x0,0xf01882a4
f01034cc:	c6 05 a5 82 18 f0 8e 	movb   $0x8e,0xf01882a5
f01034d3:	c1 e8 10             	shr    $0x10,%eax
f01034d6:	66 a3 a6 82 18 f0    	mov    %ax,0xf01882a6
	SG(5, 0, 0);
f01034dc:	b8 e6 3d 10 f0       	mov    $0xf0103de6,%eax
f01034e1:	66 a3 a8 82 18 f0    	mov    %ax,0xf01882a8
f01034e7:	66 c7 05 aa 82 18 f0 	movw   $0x8,0xf01882aa
f01034ee:	08 00 
f01034f0:	c6 05 ac 82 18 f0 00 	movb   $0x0,0xf01882ac
f01034f7:	c6 05 ad 82 18 f0 8e 	movb   $0x8e,0xf01882ad
f01034fe:	c1 e8 10             	shr    $0x10,%eax
f0103501:	66 a3 ae 82 18 f0    	mov    %ax,0xf01882ae
	SG(6, 0, 0);
f0103507:	b8 f0 3d 10 f0       	mov    $0xf0103df0,%eax
f010350c:	66 a3 b0 82 18 f0    	mov    %ax,0xf01882b0
f0103512:	66 c7 05 b2 82 18 f0 	movw   $0x8,0xf01882b2
f0103519:	08 00 
f010351b:	c6 05 b4 82 18 f0 00 	movb   $0x0,0xf01882b4
f0103522:	c6 05 b5 82 18 f0 8e 	movb   $0x8e,0xf01882b5
f0103529:	c1 e8 10             	shr    $0x10,%eax
f010352c:	66 a3 b6 82 18 f0    	mov    %ax,0xf01882b6
	SG(7, 0, 0);
f0103532:	b8 fa 3d 10 f0       	mov    $0xf0103dfa,%eax
f0103537:	66 a3 b8 82 18 f0    	mov    %ax,0xf01882b8
f010353d:	66 c7 05 ba 82 18 f0 	movw   $0x8,0xf01882ba
f0103544:	08 00 
f0103546:	c6 05 bc 82 18 f0 00 	movb   $0x0,0xf01882bc
f010354d:	c6 05 bd 82 18 f0 8e 	movb   $0x8e,0xf01882bd
f0103554:	c1 e8 10             	shr    $0x10,%eax
f0103557:	66 a3 be 82 18 f0    	mov    %ax,0xf01882be
	SG(8, 0, 0);
f010355d:	b8 04 3e 10 f0       	mov    $0xf0103e04,%eax
f0103562:	66 a3 c0 82 18 f0    	mov    %ax,0xf01882c0
f0103568:	66 c7 05 c2 82 18 f0 	movw   $0x8,0xf01882c2
f010356f:	08 00 
f0103571:	c6 05 c4 82 18 f0 00 	movb   $0x0,0xf01882c4
f0103578:	c6 05 c5 82 18 f0 8e 	movb   $0x8e,0xf01882c5
f010357f:	c1 e8 10             	shr    $0x10,%eax
f0103582:	66 a3 c6 82 18 f0    	mov    %ax,0xf01882c6
	SG(10, 0, 0);
f0103588:	b8 0c 3e 10 f0       	mov    $0xf0103e0c,%eax
f010358d:	66 a3 d0 82 18 f0    	mov    %ax,0xf01882d0
f0103593:	66 c7 05 d2 82 18 f0 	movw   $0x8,0xf01882d2
f010359a:	08 00 
f010359c:	c6 05 d4 82 18 f0 00 	movb   $0x0,0xf01882d4
f01035a3:	c6 05 d5 82 18 f0 8e 	movb   $0x8e,0xf01882d5
f01035aa:	c1 e8 10             	shr    $0x10,%eax
f01035ad:	66 a3 d6 82 18 f0    	mov    %ax,0xf01882d6
	SG(11, 0, 0);
f01035b3:	b8 14 3e 10 f0       	mov    $0xf0103e14,%eax
f01035b8:	66 a3 d8 82 18 f0    	mov    %ax,0xf01882d8
f01035be:	66 c7 05 da 82 18 f0 	movw   $0x8,0xf01882da
f01035c5:	08 00 
f01035c7:	c6 05 dc 82 18 f0 00 	movb   $0x0,0xf01882dc
f01035ce:	c6 05 dd 82 18 f0 8e 	movb   $0x8e,0xf01882dd
f01035d5:	c1 e8 10             	shr    $0x10,%eax
f01035d8:	66 a3 de 82 18 f0    	mov    %ax,0xf01882de
	SG(12, 0, 0);
f01035de:	b8 1c 3e 10 f0       	mov    $0xf0103e1c,%eax
f01035e3:	66 a3 e0 82 18 f0    	mov    %ax,0xf01882e0
f01035e9:	66 c7 05 e2 82 18 f0 	movw   $0x8,0xf01882e2
f01035f0:	08 00 
f01035f2:	c6 05 e4 82 18 f0 00 	movb   $0x0,0xf01882e4
f01035f9:	c6 05 e5 82 18 f0 8e 	movb   $0x8e,0xf01882e5
f0103600:	c1 e8 10             	shr    $0x10,%eax
f0103603:	66 a3 e6 82 18 f0    	mov    %ax,0xf01882e6
	SG(13, 0, 0);
f0103609:	b8 24 3e 10 f0       	mov    $0xf0103e24,%eax
f010360e:	66 a3 e8 82 18 f0    	mov    %ax,0xf01882e8
f0103614:	66 c7 05 ea 82 18 f0 	movw   $0x8,0xf01882ea
f010361b:	08 00 
f010361d:	c6 05 ec 82 18 f0 00 	movb   $0x0,0xf01882ec
f0103624:	c6 05 ed 82 18 f0 8e 	movb   $0x8e,0xf01882ed
f010362b:	c1 e8 10             	shr    $0x10,%eax
f010362e:	66 a3 ee 82 18 f0    	mov    %ax,0xf01882ee
	SG(14, 0, 0);
f0103634:	b8 2c 3e 10 f0       	mov    $0xf0103e2c,%eax
f0103639:	66 a3 f0 82 18 f0    	mov    %ax,0xf01882f0
f010363f:	66 c7 05 f2 82 18 f0 	movw   $0x8,0xf01882f2
f0103646:	08 00 
f0103648:	c6 05 f4 82 18 f0 00 	movb   $0x0,0xf01882f4
f010364f:	c6 05 f5 82 18 f0 8e 	movb   $0x8e,0xf01882f5
f0103656:	c1 e8 10             	shr    $0x10,%eax
f0103659:	66 a3 f6 82 18 f0    	mov    %ax,0xf01882f6
	SG(16, 0, 0);
f010365f:	b8 34 3e 10 f0       	mov    $0xf0103e34,%eax
f0103664:	66 a3 00 83 18 f0    	mov    %ax,0xf0188300
f010366a:	66 c7 05 02 83 18 f0 	movw   $0x8,0xf0188302
f0103671:	08 00 
f0103673:	c6 05 04 83 18 f0 00 	movb   $0x0,0xf0188304
f010367a:	c6 05 05 83 18 f0 8e 	movb   $0x8e,0xf0188305
f0103681:	c1 e8 10             	shr    $0x10,%eax
f0103684:	66 a3 06 83 18 f0    	mov    %ax,0xf0188306
	SG(17, 0, 0);
f010368a:	b8 3e 3e 10 f0       	mov    $0xf0103e3e,%eax
f010368f:	66 a3 08 83 18 f0    	mov    %ax,0xf0188308
f0103695:	66 c7 05 0a 83 18 f0 	movw   $0x8,0xf018830a
f010369c:	08 00 
f010369e:	c6 05 0c 83 18 f0 00 	movb   $0x0,0xf018830c
f01036a5:	c6 05 0d 83 18 f0 8e 	movb   $0x8e,0xf018830d
f01036ac:	c1 e8 10             	shr    $0x10,%eax
f01036af:	66 a3 0e 83 18 f0    	mov    %ax,0xf018830e
	SG(18, 0, 0);
f01036b5:	b8 46 3e 10 f0       	mov    $0xf0103e46,%eax
f01036ba:	66 a3 10 83 18 f0    	mov    %ax,0xf0188310
f01036c0:	66 c7 05 12 83 18 f0 	movw   $0x8,0xf0188312
f01036c7:	08 00 
f01036c9:	c6 05 14 83 18 f0 00 	movb   $0x0,0xf0188314
f01036d0:	c6 05 15 83 18 f0 8e 	movb   $0x8e,0xf0188315
f01036d7:	c1 e8 10             	shr    $0x10,%eax
f01036da:	66 a3 16 83 18 f0    	mov    %ax,0xf0188316
	SG(19, 0, 0);
f01036e0:	b8 50 3e 10 f0       	mov    $0xf0103e50,%eax
f01036e5:	66 a3 18 83 18 f0    	mov    %ax,0xf0188318
f01036eb:	66 c7 05 1a 83 18 f0 	movw   $0x8,0xf018831a
f01036f2:	08 00 
f01036f4:	c6 05 1c 83 18 f0 00 	movb   $0x0,0xf018831c
f01036fb:	c6 05 1d 83 18 f0 8e 	movb   $0x8e,0xf018831d
f0103702:	c1 e8 10             	shr    $0x10,%eax
f0103705:	66 a3 1e 83 18 f0    	mov    %ax,0xf018831e

	// 16 IRQs
	SG(32, 0, 0);
f010370b:	b8 5a 3e 10 f0       	mov    $0xf0103e5a,%eax
f0103710:	66 a3 80 83 18 f0    	mov    %ax,0xf0188380
f0103716:	66 c7 05 82 83 18 f0 	movw   $0x8,0xf0188382
f010371d:	08 00 
f010371f:	c6 05 84 83 18 f0 00 	movb   $0x0,0xf0188384
f0103726:	c6 05 85 83 18 f0 8e 	movb   $0x8e,0xf0188385
f010372d:	c1 e8 10             	shr    $0x10,%eax
f0103730:	66 a3 86 83 18 f0    	mov    %ax,0xf0188386
	SG(33, 0, 0);
f0103736:	b8 64 3e 10 f0       	mov    $0xf0103e64,%eax
f010373b:	66 a3 88 83 18 f0    	mov    %ax,0xf0188388
f0103741:	66 c7 05 8a 83 18 f0 	movw   $0x8,0xf018838a
f0103748:	08 00 
f010374a:	c6 05 8c 83 18 f0 00 	movb   $0x0,0xf018838c
f0103751:	c6 05 8d 83 18 f0 8e 	movb   $0x8e,0xf018838d
f0103758:	c1 e8 10             	shr    $0x10,%eax
f010375b:	66 a3 8e 83 18 f0    	mov    %ax,0xf018838e
	SG(34, 0, 0);
f0103761:	b8 6e 3e 10 f0       	mov    $0xf0103e6e,%eax
f0103766:	66 a3 90 83 18 f0    	mov    %ax,0xf0188390
f010376c:	66 c7 05 92 83 18 f0 	movw   $0x8,0xf0188392
f0103773:	08 00 
f0103775:	c6 05 94 83 18 f0 00 	movb   $0x0,0xf0188394
f010377c:	c6 05 95 83 18 f0 8e 	movb   $0x8e,0xf0188395
f0103783:	c1 e8 10             	shr    $0x10,%eax
f0103786:	66 a3 96 83 18 f0    	mov    %ax,0xf0188396
	SG(35, 0, 0);
f010378c:	b8 78 3e 10 f0       	mov    $0xf0103e78,%eax
f0103791:	66 a3 98 83 18 f0    	mov    %ax,0xf0188398
f0103797:	66 c7 05 9a 83 18 f0 	movw   $0x8,0xf018839a
f010379e:	08 00 
f01037a0:	c6 05 9c 83 18 f0 00 	movb   $0x0,0xf018839c
f01037a7:	c6 05 9d 83 18 f0 8e 	movb   $0x8e,0xf018839d
f01037ae:	c1 e8 10             	shr    $0x10,%eax
f01037b1:	66 a3 9e 83 18 f0    	mov    %ax,0xf018839e
	SG(36, 0, 0);
f01037b7:	b8 82 3e 10 f0       	mov    $0xf0103e82,%eax
f01037bc:	66 a3 a0 83 18 f0    	mov    %ax,0xf01883a0
f01037c2:	66 c7 05 a2 83 18 f0 	movw   $0x8,0xf01883a2
f01037c9:	08 00 
f01037cb:	c6 05 a4 83 18 f0 00 	movb   $0x0,0xf01883a4
f01037d2:	c6 05 a5 83 18 f0 8e 	movb   $0x8e,0xf01883a5
f01037d9:	c1 e8 10             	shr    $0x10,%eax
f01037dc:	66 a3 a6 83 18 f0    	mov    %ax,0xf01883a6
	SG(37, 0, 0);
f01037e2:	b8 8c 3e 10 f0       	mov    $0xf0103e8c,%eax
f01037e7:	66 a3 a8 83 18 f0    	mov    %ax,0xf01883a8
f01037ed:	66 c7 05 aa 83 18 f0 	movw   $0x8,0xf01883aa
f01037f4:	08 00 
f01037f6:	c6 05 ac 83 18 f0 00 	movb   $0x0,0xf01883ac
f01037fd:	c6 05 ad 83 18 f0 8e 	movb   $0x8e,0xf01883ad
f0103804:	c1 e8 10             	shr    $0x10,%eax
f0103807:	66 a3 ae 83 18 f0    	mov    %ax,0xf01883ae
	SG(38, 0, 0);
f010380d:	b8 96 3e 10 f0       	mov    $0xf0103e96,%eax
f0103812:	66 a3 b0 83 18 f0    	mov    %ax,0xf01883b0
f0103818:	66 c7 05 b2 83 18 f0 	movw   $0x8,0xf01883b2
f010381f:	08 00 
f0103821:	c6 05 b4 83 18 f0 00 	movb   $0x0,0xf01883b4
f0103828:	c6 05 b5 83 18 f0 8e 	movb   $0x8e,0xf01883b5
f010382f:	c1 e8 10             	shr    $0x10,%eax
f0103832:	66 a3 b6 83 18 f0    	mov    %ax,0xf01883b6
	SG(39, 0, 0);
f0103838:	b8 a0 3e 10 f0       	mov    $0xf0103ea0,%eax
f010383d:	66 a3 b8 83 18 f0    	mov    %ax,0xf01883b8
f0103843:	66 c7 05 ba 83 18 f0 	movw   $0x8,0xf01883ba
f010384a:	08 00 
f010384c:	c6 05 bc 83 18 f0 00 	movb   $0x0,0xf01883bc
f0103853:	c6 05 bd 83 18 f0 8e 	movb   $0x8e,0xf01883bd
f010385a:	c1 e8 10             	shr    $0x10,%eax
f010385d:	66 a3 be 83 18 f0    	mov    %ax,0xf01883be
	SG(40, 0, 0);
f0103863:	b8 aa 3e 10 f0       	mov    $0xf0103eaa,%eax
f0103868:	66 a3 c0 83 18 f0    	mov    %ax,0xf01883c0
f010386e:	66 c7 05 c2 83 18 f0 	movw   $0x8,0xf01883c2
f0103875:	08 00 
f0103877:	c6 05 c4 83 18 f0 00 	movb   $0x0,0xf01883c4
f010387e:	c6 05 c5 83 18 f0 8e 	movb   $0x8e,0xf01883c5
f0103885:	c1 e8 10             	shr    $0x10,%eax
f0103888:	66 a3 c6 83 18 f0    	mov    %ax,0xf01883c6
	SG(41, 0, 0);
f010388e:	b8 b4 3e 10 f0       	mov    $0xf0103eb4,%eax
f0103893:	66 a3 c8 83 18 f0    	mov    %ax,0xf01883c8
f0103899:	66 c7 05 ca 83 18 f0 	movw   $0x8,0xf01883ca
f01038a0:	08 00 
f01038a2:	c6 05 cc 83 18 f0 00 	movb   $0x0,0xf01883cc
f01038a9:	c6 05 cd 83 18 f0 8e 	movb   $0x8e,0xf01883cd
f01038b0:	c1 e8 10             	shr    $0x10,%eax
f01038b3:	66 a3 ce 83 18 f0    	mov    %ax,0xf01883ce
	SG(42, 0, 0);
f01038b9:	b8 be 3e 10 f0       	mov    $0xf0103ebe,%eax
f01038be:	66 a3 d0 83 18 f0    	mov    %ax,0xf01883d0
f01038c4:	66 c7 05 d2 83 18 f0 	movw   $0x8,0xf01883d2
f01038cb:	08 00 
f01038cd:	c6 05 d4 83 18 f0 00 	movb   $0x0,0xf01883d4
f01038d4:	c6 05 d5 83 18 f0 8e 	movb   $0x8e,0xf01883d5
f01038db:	c1 e8 10             	shr    $0x10,%eax
f01038de:	66 a3 d6 83 18 f0    	mov    %ax,0xf01883d6
	SG(43, 0, 0);
f01038e4:	b8 c8 3e 10 f0       	mov    $0xf0103ec8,%eax
f01038e9:	66 a3 d8 83 18 f0    	mov    %ax,0xf01883d8
f01038ef:	66 c7 05 da 83 18 f0 	movw   $0x8,0xf01883da
f01038f6:	08 00 
f01038f8:	c6 05 dc 83 18 f0 00 	movb   $0x0,0xf01883dc
f01038ff:	c6 05 dd 83 18 f0 8e 	movb   $0x8e,0xf01883dd
f0103906:	c1 e8 10             	shr    $0x10,%eax
f0103909:	66 a3 de 83 18 f0    	mov    %ax,0xf01883de
	SG(44, 0, 0);
f010390f:	b8 d2 3e 10 f0       	mov    $0xf0103ed2,%eax
f0103914:	66 a3 e0 83 18 f0    	mov    %ax,0xf01883e0
f010391a:	66 c7 05 e2 83 18 f0 	movw   $0x8,0xf01883e2
f0103921:	08 00 
f0103923:	c6 05 e4 83 18 f0 00 	movb   $0x0,0xf01883e4
f010392a:	c6 05 e5 83 18 f0 8e 	movb   $0x8e,0xf01883e5
f0103931:	c1 e8 10             	shr    $0x10,%eax
f0103934:	66 a3 e6 83 18 f0    	mov    %ax,0xf01883e6
	SG(45, 0, 0);
f010393a:	b8 dc 3e 10 f0       	mov    $0xf0103edc,%eax
f010393f:	66 a3 e8 83 18 f0    	mov    %ax,0xf01883e8
f0103945:	66 c7 05 ea 83 18 f0 	movw   $0x8,0xf01883ea
f010394c:	08 00 
f010394e:	c6 05 ec 83 18 f0 00 	movb   $0x0,0xf01883ec
f0103955:	c6 05 ed 83 18 f0 8e 	movb   $0x8e,0xf01883ed
f010395c:	c1 e8 10             	shr    $0x10,%eax
f010395f:	66 a3 ee 83 18 f0    	mov    %ax,0xf01883ee
	SG(46, 0, 0);
f0103965:	b8 e6 3e 10 f0       	mov    $0xf0103ee6,%eax
f010396a:	66 a3 f0 83 18 f0    	mov    %ax,0xf01883f0
f0103970:	66 c7 05 f2 83 18 f0 	movw   $0x8,0xf01883f2
f0103977:	08 00 
f0103979:	c6 05 f4 83 18 f0 00 	movb   $0x0,0xf01883f4
f0103980:	c6 05 f5 83 18 f0 8e 	movb   $0x8e,0xf01883f5
f0103987:	c1 e8 10             	shr    $0x10,%eax
f010398a:	66 a3 f6 83 18 f0    	mov    %ax,0xf01883f6
	SG(47, 0, 0);
f0103990:	b8 f0 3e 10 f0       	mov    $0xf0103ef0,%eax
f0103995:	66 a3 f8 83 18 f0    	mov    %ax,0xf01883f8
f010399b:	66 c7 05 fa 83 18 f0 	movw   $0x8,0xf01883fa
f01039a2:	08 00 
f01039a4:	c6 05 fc 83 18 f0 00 	movb   $0x0,0xf01883fc
f01039ab:	c6 05 fd 83 18 f0 8e 	movb   $0x8e,0xf01883fd
f01039b2:	c1 e8 10             	shr    $0x10,%eax
f01039b5:	66 a3 fe 83 18 f0    	mov    %ax,0xf01883fe
 
	SG(48, 0, 3); //for interrupt
f01039bb:	b8 fa 3e 10 f0       	mov    $0xf0103efa,%eax
f01039c0:	66 a3 00 84 18 f0    	mov    %ax,0xf0188400
f01039c6:	66 c7 05 02 84 18 f0 	movw   $0x8,0xf0188402
f01039cd:	08 00 
f01039cf:	c6 05 04 84 18 f0 00 	movb   $0x0,0xf0188404
f01039d6:	c6 05 05 84 18 f0 ee 	movb   $0xee,0xf0188405
f01039dd:	c1 e8 10             	shr    $0x10,%eax
f01039e0:	66 a3 06 84 18 f0    	mov    %ax,0xf0188406
	// warn("trap_init: treat system call as not trap");

	// Per-CPU setup 
	trap_init_percpu();
f01039e6:	e8 ba f9 ff ff       	call   f01033a5 <trap_init_percpu>
}
f01039eb:	5d                   	pop    %ebp
f01039ec:	c3                   	ret    

f01039ed <print_regs>:
	}
}

void
print_regs(struct PushRegs *regs)
{
f01039ed:	55                   	push   %ebp
f01039ee:	89 e5                	mov    %esp,%ebp
f01039f0:	53                   	push   %ebx
f01039f1:	83 ec 0c             	sub    $0xc,%esp
f01039f4:	8b 5d 08             	mov    0x8(%ebp),%ebx
	cprintf("  edi  0x%08x\n", regs->reg_edi);
f01039f7:	ff 33                	pushl  (%ebx)
f01039f9:	68 13 64 10 f0       	push   $0xf0106413
f01039fe:	e8 8e f9 ff ff       	call   f0103391 <cprintf>
	cprintf("  esi  0x%08x\n", regs->reg_esi);
f0103a03:	83 c4 08             	add    $0x8,%esp
f0103a06:	ff 73 04             	pushl  0x4(%ebx)
f0103a09:	68 22 64 10 f0       	push   $0xf0106422
f0103a0e:	e8 7e f9 ff ff       	call   f0103391 <cprintf>
	cprintf("  ebp  0x%08x\n", regs->reg_ebp);
f0103a13:	83 c4 08             	add    $0x8,%esp
f0103a16:	ff 73 08             	pushl  0x8(%ebx)
f0103a19:	68 31 64 10 f0       	push   $0xf0106431
f0103a1e:	e8 6e f9 ff ff       	call   f0103391 <cprintf>
	cprintf("  oesp 0x%08x\n", regs->reg_oesp);
f0103a23:	83 c4 08             	add    $0x8,%esp
f0103a26:	ff 73 0c             	pushl  0xc(%ebx)
f0103a29:	68 40 64 10 f0       	push   $0xf0106440
f0103a2e:	e8 5e f9 ff ff       	call   f0103391 <cprintf>
	cprintf("  ebx  0x%08x\n", regs->reg_ebx);
f0103a33:	83 c4 08             	add    $0x8,%esp
f0103a36:	ff 73 10             	pushl  0x10(%ebx)
f0103a39:	68 4f 64 10 f0       	push   $0xf010644f
f0103a3e:	e8 4e f9 ff ff       	call   f0103391 <cprintf>
	cprintf("  edx  0x%08x\n", regs->reg_edx);
f0103a43:	83 c4 08             	add    $0x8,%esp
f0103a46:	ff 73 14             	pushl  0x14(%ebx)
f0103a49:	68 5e 64 10 f0       	push   $0xf010645e
f0103a4e:	e8 3e f9 ff ff       	call   f0103391 <cprintf>
	cprintf("  ecx  0x%08x\n", regs->reg_ecx);
f0103a53:	83 c4 08             	add    $0x8,%esp
f0103a56:	ff 73 18             	pushl  0x18(%ebx)
f0103a59:	68 6d 64 10 f0       	push   $0xf010646d
f0103a5e:	e8 2e f9 ff ff       	call   f0103391 <cprintf>
	cprintf("  eax  0x%08x\n", regs->reg_eax);
f0103a63:	83 c4 08             	add    $0x8,%esp
f0103a66:	ff 73 1c             	pushl  0x1c(%ebx)
f0103a69:	68 7c 64 10 f0       	push   $0xf010647c
f0103a6e:	e8 1e f9 ff ff       	call   f0103391 <cprintf>
}
f0103a73:	83 c4 10             	add    $0x10,%esp
f0103a76:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0103a79:	c9                   	leave  
f0103a7a:	c3                   	ret    

f0103a7b <print_trapframe>:
	lidt(&idt_pd);
}

void
print_trapframe(struct Trapframe *tf)
{
f0103a7b:	55                   	push   %ebp
f0103a7c:	89 e5                	mov    %esp,%ebp
f0103a7e:	53                   	push   %ebx
f0103a7f:	83 ec 0c             	sub    $0xc,%esp
f0103a82:	8b 5d 08             	mov    0x8(%ebp),%ebx
	cprintf("TRAP frame at %p\n", tf);
f0103a85:	53                   	push   %ebx
f0103a86:	68 cd 65 10 f0       	push   $0xf01065cd
f0103a8b:	e8 01 f9 ff ff       	call   f0103391 <cprintf>
	print_regs(&tf->tf_regs);
f0103a90:	89 1c 24             	mov    %ebx,(%esp)
f0103a93:	e8 55 ff ff ff       	call   f01039ed <print_regs>
	cprintf("  es   0x----%04x\n", tf->tf_es);
f0103a98:	83 c4 08             	add    $0x8,%esp
f0103a9b:	0f b7 43 20          	movzwl 0x20(%ebx),%eax
f0103a9f:	50                   	push   %eax
f0103aa0:	68 cd 64 10 f0       	push   $0xf01064cd
f0103aa5:	e8 e7 f8 ff ff       	call   f0103391 <cprintf>
	cprintf("  ds   0x----%04x\n", tf->tf_ds);
f0103aaa:	83 c4 08             	add    $0x8,%esp
f0103aad:	0f b7 43 24          	movzwl 0x24(%ebx),%eax
f0103ab1:	50                   	push   %eax
f0103ab2:	68 e0 64 10 f0       	push   $0xf01064e0
f0103ab7:	e8 d5 f8 ff ff       	call   f0103391 <cprintf>
	cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
f0103abc:	8b 43 28             	mov    0x28(%ebx),%eax
		"Alignment Check",
		"Machine-Check",
		"SIMD Floating-Point Exception"
	};

	if (trapno < ARRAY_SIZE(excnames))
f0103abf:	83 c4 10             	add    $0x10,%esp
f0103ac2:	83 f8 13             	cmp    $0x13,%eax
f0103ac5:	77 09                	ja     f0103ad0 <print_trapframe+0x55>
		return excnames[trapno];
f0103ac7:	8b 14 85 c0 67 10 f0 	mov    -0xfef9840(,%eax,4),%edx
f0103ace:	eb 11                	jmp    f0103ae1 <print_trapframe+0x66>
	if (trapno == T_SYSCALL)
f0103ad0:	83 f8 30             	cmp    $0x30,%eax
f0103ad3:	74 07                	je     f0103adc <print_trapframe+0x61>
		return "System call";
	return "(unknown trap)";
f0103ad5:	ba 97 64 10 f0       	mov    $0xf0106497,%edx
f0103ada:	eb 05                	jmp    f0103ae1 <print_trapframe+0x66>
	};

	if (trapno < ARRAY_SIZE(excnames))
		return excnames[trapno];
	if (trapno == T_SYSCALL)
		return "System call";
f0103adc:	ba 8b 64 10 f0       	mov    $0xf010648b,%edx
{
	cprintf("TRAP frame at %p\n", tf);
	print_regs(&tf->tf_regs);
	cprintf("  es   0x----%04x\n", tf->tf_es);
	cprintf("  ds   0x----%04x\n", tf->tf_ds);
	cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
f0103ae1:	83 ec 04             	sub    $0x4,%esp
f0103ae4:	52                   	push   %edx
f0103ae5:	50                   	push   %eax
f0103ae6:	68 f3 64 10 f0       	push   $0xf01064f3
f0103aeb:	e8 a1 f8 ff ff       	call   f0103391 <cprintf>
	// If this trap was a page fault that just happened
	// (so %cr2 is meaningful), print the faulting linear address.
	if (tf == last_tf && tf->tf_trapno == T_PGFLT)
f0103af0:	83 c4 10             	add    $0x10,%esp
f0103af3:	3b 1d 80 8a 18 f0    	cmp    0xf0188a80,%ebx
f0103af9:	75 1a                	jne    f0103b15 <print_trapframe+0x9a>
f0103afb:	83 7b 28 0e          	cmpl   $0xe,0x28(%ebx)
f0103aff:	75 14                	jne    f0103b15 <print_trapframe+0x9a>

static inline uint32_t
rcr2(void)
{
	uint32_t val;
	asm volatile("movl %%cr2,%0" : "=r" (val));
f0103b01:	0f 20 d0             	mov    %cr2,%eax
		cprintf("  cr2  0x%08x\n", rcr2());
f0103b04:	83 ec 08             	sub    $0x8,%esp
f0103b07:	50                   	push   %eax
f0103b08:	68 05 65 10 f0       	push   $0xf0106505
f0103b0d:	e8 7f f8 ff ff       	call   f0103391 <cprintf>
f0103b12:	83 c4 10             	add    $0x10,%esp
	cprintf("  err  0x%08x", tf->tf_err);
f0103b15:	83 ec 08             	sub    $0x8,%esp
f0103b18:	ff 73 2c             	pushl  0x2c(%ebx)
f0103b1b:	68 14 65 10 f0       	push   $0xf0106514
f0103b20:	e8 6c f8 ff ff       	call   f0103391 <cprintf>
	// For page faults, print decoded fault error code:
	// U/K=fault occurred in user/kernel mode
	// W/R=a write/read caused the fault
	// PR=a protection violation caused the fault (NP=page not present).
	if (tf->tf_trapno == T_PGFLT)
f0103b25:	83 c4 10             	add    $0x10,%esp
f0103b28:	83 7b 28 0e          	cmpl   $0xe,0x28(%ebx)
f0103b2c:	75 45                	jne    f0103b73 <print_trapframe+0xf8>
		cprintf(" [%s, %s, %s]\n",
			tf->tf_err & 4 ? "user" : "kernel",
			tf->tf_err & 2 ? "write" : "read",
			tf->tf_err & 1 ? "protection" : "not-present");
f0103b2e:	8b 43 2c             	mov    0x2c(%ebx),%eax
	// For page faults, print decoded fault error code:
	// U/K=fault occurred in user/kernel mode
	// W/R=a write/read caused the fault
	// PR=a protection violation caused the fault (NP=page not present).
	if (tf->tf_trapno == T_PGFLT)
		cprintf(" [%s, %s, %s]\n",
f0103b31:	a8 01                	test   $0x1,%al
f0103b33:	75 07                	jne    f0103b3c <print_trapframe+0xc1>
f0103b35:	b9 b1 64 10 f0       	mov    $0xf01064b1,%ecx
f0103b3a:	eb 05                	jmp    f0103b41 <print_trapframe+0xc6>
f0103b3c:	b9 a6 64 10 f0       	mov    $0xf01064a6,%ecx
f0103b41:	a8 02                	test   $0x2,%al
f0103b43:	75 07                	jne    f0103b4c <print_trapframe+0xd1>
f0103b45:	ba c3 64 10 f0       	mov    $0xf01064c3,%edx
f0103b4a:	eb 05                	jmp    f0103b51 <print_trapframe+0xd6>
f0103b4c:	ba bd 64 10 f0       	mov    $0xf01064bd,%edx
f0103b51:	a8 04                	test   $0x4,%al
f0103b53:	75 07                	jne    f0103b5c <print_trapframe+0xe1>
f0103b55:	b8 15 66 10 f0       	mov    $0xf0106615,%eax
f0103b5a:	eb 05                	jmp    f0103b61 <print_trapframe+0xe6>
f0103b5c:	b8 c8 64 10 f0       	mov    $0xf01064c8,%eax
f0103b61:	51                   	push   %ecx
f0103b62:	52                   	push   %edx
f0103b63:	50                   	push   %eax
f0103b64:	68 22 65 10 f0       	push   $0xf0106522
f0103b69:	e8 23 f8 ff ff       	call   f0103391 <cprintf>
f0103b6e:	83 c4 10             	add    $0x10,%esp
f0103b71:	eb 10                	jmp    f0103b83 <print_trapframe+0x108>
			tf->tf_err & 4 ? "user" : "kernel",
			tf->tf_err & 2 ? "write" : "read",
			tf->tf_err & 1 ? "protection" : "not-present");
	else
		cprintf("\n");
f0103b73:	83 ec 0c             	sub    $0xc,%esp
f0103b76:	68 dd 62 10 f0       	push   $0xf01062dd
f0103b7b:	e8 11 f8 ff ff       	call   f0103391 <cprintf>
f0103b80:	83 c4 10             	add    $0x10,%esp
	cprintf("  eip  0x%08x\n", tf->tf_eip);
f0103b83:	83 ec 08             	sub    $0x8,%esp
f0103b86:	ff 73 30             	pushl  0x30(%ebx)
f0103b89:	68 31 65 10 f0       	push   $0xf0106531
f0103b8e:	e8 fe f7 ff ff       	call   f0103391 <cprintf>
	cprintf("  cs   0x----%04x\n", tf->tf_cs);
f0103b93:	83 c4 08             	add    $0x8,%esp
f0103b96:	0f b7 43 34          	movzwl 0x34(%ebx),%eax
f0103b9a:	50                   	push   %eax
f0103b9b:	68 40 65 10 f0       	push   $0xf0106540
f0103ba0:	e8 ec f7 ff ff       	call   f0103391 <cprintf>
	cprintf("  flag 0x%08x\n", tf->tf_eflags);
f0103ba5:	83 c4 08             	add    $0x8,%esp
f0103ba8:	ff 73 38             	pushl  0x38(%ebx)
f0103bab:	68 53 65 10 f0       	push   $0xf0106553
f0103bb0:	e8 dc f7 ff ff       	call   f0103391 <cprintf>
	if ((tf->tf_cs & 3) != 0) {
f0103bb5:	83 c4 10             	add    $0x10,%esp
f0103bb8:	f6 43 34 03          	testb  $0x3,0x34(%ebx)
f0103bbc:	74 25                	je     f0103be3 <print_trapframe+0x168>
		cprintf("  esp  0x%08x\n", tf->tf_esp);
f0103bbe:	83 ec 08             	sub    $0x8,%esp
f0103bc1:	ff 73 3c             	pushl  0x3c(%ebx)
f0103bc4:	68 62 65 10 f0       	push   $0xf0106562
f0103bc9:	e8 c3 f7 ff ff       	call   f0103391 <cprintf>
		cprintf("  ss   0x----%04x\n", tf->tf_ss);
f0103bce:	83 c4 08             	add    $0x8,%esp
f0103bd1:	0f b7 43 40          	movzwl 0x40(%ebx),%eax
f0103bd5:	50                   	push   %eax
f0103bd6:	68 71 65 10 f0       	push   $0xf0106571
f0103bdb:	e8 b1 f7 ff ff       	call   f0103391 <cprintf>
f0103be0:	83 c4 10             	add    $0x10,%esp
	}
}
f0103be3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0103be6:	c9                   	leave  
f0103be7:	c3                   	ret    

f0103be8 <page_fault_handler>:
}

//special function to handle page fault
void
page_fault_handler(struct Trapframe *tf)
{
f0103be8:	55                   	push   %ebp
f0103be9:	89 e5                	mov    %esp,%ebp
f0103beb:	53                   	push   %ebx
f0103bec:	83 ec 04             	sub    $0x4,%esp
f0103bef:	8b 5d 08             	mov    0x8(%ebp),%ebx
f0103bf2:	0f 20 d0             	mov    %cr2,%eax

	// Handle kernel-mode page faults.

	// LAB 3: Your code here.
//------------
	if ((tf->tf_cs & 0x3) == 0) {
f0103bf5:	f6 43 34 03          	testb  $0x3,0x34(%ebx)
f0103bf9:	75 20                	jne    f0103c1b <page_fault_handler+0x33>
        print_trapframe(tf);
f0103bfb:	83 ec 0c             	sub    $0xc,%esp
f0103bfe:	53                   	push   %ebx
f0103bff:	e8 77 fe ff ff       	call   f0103a7b <print_trapframe>
		panic("page fault in kernel space");
f0103c04:	83 c4 0c             	add    $0xc,%esp
f0103c07:	68 84 65 10 f0       	push   $0xf0106584
f0103c0c:	68 2d 01 00 00       	push   $0x12d
f0103c11:	68 9f 65 10 f0       	push   $0xf010659f
f0103c16:	e8 31 c4 ff ff       	call   f010004c <_panic>
//------------
	// We've already handled kernel-mode exceptions, so if we get here,
	// the page fault happened in user mode.

	// Destroy the environment that caused the fault.
	cprintf("[%08x] user fault va %08x ip %08x\n",
f0103c1b:	ff 73 30             	pushl  0x30(%ebx)
f0103c1e:	50                   	push   %eax
f0103c1f:	a1 64 82 18 f0       	mov    0xf0188264,%eax
f0103c24:	ff 70 48             	pushl  0x48(%eax)
f0103c27:	68 60 67 10 f0       	push   $0xf0106760
f0103c2c:	e8 60 f7 ff ff       	call   f0103391 <cprintf>
		curenv->env_id, fault_va, tf->tf_eip);
	print_trapframe(tf);
f0103c31:	89 1c 24             	mov    %ebx,(%esp)
f0103c34:	e8 42 fe ff ff       	call   f0103a7b <print_trapframe>
	env_destroy(curenv);
f0103c39:	83 c4 04             	add    $0x4,%esp
f0103c3c:	ff 35 64 82 18 f0    	pushl  0xf0188264
f0103c42:	e8 51 f6 ff ff       	call   f0103298 <env_destroy>
}
f0103c47:	83 c4 10             	add    $0x10,%esp
f0103c4a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0103c4d:	c9                   	leave  
f0103c4e:	c3                   	ret    

f0103c4f <trap>:
	}*/
}

void
trap(struct Trapframe *tf)
{
f0103c4f:	55                   	push   %ebp
f0103c50:	89 e5                	mov    %esp,%ebp
f0103c52:	57                   	push   %edi
f0103c53:	56                   	push   %esi
f0103c54:	8b 75 08             	mov    0x8(%ebp),%esi
	// The environment may have set DF and some versions
	// of GCC rely on DF being clear
	asm volatile("cld" ::: "cc");
f0103c57:	fc                   	cld    

static inline uint32_t
read_eflags(void)
{
	uint32_t eflags;
	asm volatile("pushfl; popl %0" : "=r" (eflags));
f0103c58:	9c                   	pushf  
f0103c59:	58                   	pop    %eax

	// Check that interrupts are disabled.  If this assertion
	// fails, DO NOT be tempted to fix it by inserting a "cli" in
	// the interrupt path.
	assert(!(read_eflags() & FL_IF));
f0103c5a:	f6 c4 02             	test   $0x2,%ah
f0103c5d:	74 19                	je     f0103c78 <trap+0x29>
f0103c5f:	68 ab 65 10 f0       	push   $0xf01065ab
f0103c64:	68 d8 50 10 f0       	push   $0xf01050d8
f0103c69:	68 01 01 00 00       	push   $0x101
f0103c6e:	68 9f 65 10 f0       	push   $0xf010659f
f0103c73:	e8 d4 c3 ff ff       	call   f010004c <_panic>

	cprintf("Incoming TRAP frame at %p\n", tf);
f0103c78:	83 ec 08             	sub    $0x8,%esp
f0103c7b:	56                   	push   %esi
f0103c7c:	68 c4 65 10 f0       	push   $0xf01065c4
f0103c81:	e8 0b f7 ff ff       	call   f0103391 <cprintf>

	if ((tf->tf_cs & 3) == 3) {
f0103c86:	66 8b 46 34          	mov    0x34(%esi),%ax
f0103c8a:	83 e0 03             	and    $0x3,%eax
f0103c8d:	83 c4 10             	add    $0x10,%esp
f0103c90:	66 83 f8 03          	cmp    $0x3,%ax
f0103c94:	75 31                	jne    f0103cc7 <trap+0x78>
		// Trapped from user mode.
		assert(curenv);
f0103c96:	a1 64 82 18 f0       	mov    0xf0188264,%eax
f0103c9b:	85 c0                	test   %eax,%eax
f0103c9d:	75 19                	jne    f0103cb8 <trap+0x69>
f0103c9f:	68 df 65 10 f0       	push   $0xf01065df
f0103ca4:	68 d8 50 10 f0       	push   $0xf01050d8
f0103ca9:	68 07 01 00 00       	push   $0x107
f0103cae:	68 9f 65 10 f0       	push   $0xf010659f
f0103cb3:	e8 94 c3 ff ff       	call   f010004c <_panic>

		// Copy trap frame (which is currently on the stack)
		// into 'curenv->env_tf', so that running the environment
		// will restart at the trap point.
		curenv->env_tf = *tf;
f0103cb8:	b9 11 00 00 00       	mov    $0x11,%ecx
f0103cbd:	89 c7                	mov    %eax,%edi
f0103cbf:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
		// The trapframe on the stack should be ignored from here on.
		tf = &curenv->env_tf;
f0103cc1:	8b 35 64 82 18 f0    	mov    0xf0188264,%esi
	}

	// Record that tf is the last real trapframe so
	// print_trapframe can print some additional information.
	last_tf = tf;
f0103cc7:	89 35 80 8a 18 f0    	mov    %esi,0xf0188a80
	// LAB 3: Your code here.

	// Handle spurious interrupts
	// The hardware sometimes raises these because of noise on the
	// IRQ line or other reasons. We don't care.
	if (tf->tf_trapno == IRQ_OFFSET + IRQ_SPURIOUS) {
f0103ccd:	8b 46 28             	mov    0x28(%esi),%eax
f0103cd0:	83 f8 27             	cmp    $0x27,%eax
f0103cd3:	75 1d                	jne    f0103cf2 <trap+0xa3>
		cprintf("Spurious interrupt on irq 7\n");
f0103cd5:	83 ec 0c             	sub    $0xc,%esp
f0103cd8:	68 e6 65 10 f0       	push   $0xf01065e6
f0103cdd:	e8 af f6 ff ff       	call   f0103391 <cprintf>
		print_trapframe(tf);
f0103ce2:	89 34 24             	mov    %esi,(%esp)
f0103ce5:	e8 91 fd ff ff       	call   f0103a7b <print_trapframe>
f0103cea:	83 c4 10             	add    $0x10,%esp
f0103ced:	e9 91 00 00 00       	jmp    f0103d83 <trap+0x134>
		return;
	}

	switch (tf->tf_trapno) {
f0103cf2:	83 f8 0e             	cmp    $0xe,%eax
f0103cf5:	74 0c                	je     f0103d03 <trap+0xb4>
f0103cf7:	83 f8 30             	cmp    $0x30,%eax
f0103cfa:	74 2b                	je     f0103d27 <trap+0xd8>
f0103cfc:	83 f8 03             	cmp    $0x3,%eax
f0103cff:	75 47                	jne    f0103d48 <trap+0xf9>
f0103d01:	eb 0e                	jmp    f0103d11 <trap+0xc2>
		case T_PGFLT:
			page_fault_handler(tf);
f0103d03:	83 ec 0c             	sub    $0xc,%esp
f0103d06:	56                   	push   %esi
f0103d07:	e8 dc fe ff ff       	call   f0103be8 <page_fault_handler>
f0103d0c:	83 c4 10             	add    $0x10,%esp
f0103d0f:	eb 72                	jmp    f0103d83 <trap+0x134>
			break;
		case T_BRKPT:
			print_trapframe(tf);
f0103d11:	83 ec 0c             	sub    $0xc,%esp
f0103d14:	56                   	push   %esi
f0103d15:	e8 61 fd ff ff       	call   f0103a7b <print_trapframe>
			monitor(tf); //invoke the kernel monitor
f0103d1a:	89 34 24             	mov    %esi,(%esp)
f0103d1d:	e8 71 cb ff ff       	call   f0100893 <monitor>
f0103d22:	83 c4 10             	add    $0x10,%esp
f0103d25:	eb 5c                	jmp    f0103d83 <trap+0x134>
			break;
		case T_SYSCALL:
			//syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5)
			tf->tf_regs.reg_eax = syscall(
f0103d27:	83 ec 08             	sub    $0x8,%esp
f0103d2a:	ff 76 04             	pushl  0x4(%esi)
f0103d2d:	ff 36                	pushl  (%esi)
f0103d2f:	ff 76 10             	pushl  0x10(%esi)
f0103d32:	ff 76 18             	pushl  0x18(%esi)
f0103d35:	ff 76 14             	pushl  0x14(%esi)
f0103d38:	ff 76 1c             	pushl  0x1c(%esi)
f0103d3b:	e8 d5 01 00 00       	call   f0103f15 <syscall>
f0103d40:	89 46 1c             	mov    %eax,0x1c(%esi)
f0103d43:	83 c4 20             	add    $0x20,%esp
f0103d46:	eb 3b                	jmp    f0103d83 <trap+0x134>
            tf->tf_regs.reg_esi);
            break;
        //break;
		default:
			// Unexpected trap: The user process or the kernel has a bug.
			print_trapframe(tf);
f0103d48:	83 ec 0c             	sub    $0xc,%esp
f0103d4b:	56                   	push   %esi
f0103d4c:	e8 2a fd ff ff       	call   f0103a7b <print_trapframe>
			if (tf->tf_cs == GD_KT)
f0103d51:	83 c4 10             	add    $0x10,%esp
f0103d54:	66 83 7e 34 08       	cmpw   $0x8,0x34(%esi)
f0103d59:	75 17                	jne    f0103d72 <trap+0x123>
				panic("unhandled trap in kernel");
f0103d5b:	83 ec 04             	sub    $0x4,%esp
f0103d5e:	68 03 66 10 f0       	push   $0xf0106603
f0103d63:	68 e7 00 00 00       	push   $0xe7
f0103d68:	68 9f 65 10 f0       	push   $0xf010659f
f0103d6d:	e8 da c2 ff ff       	call   f010004c <_panic>
			else {
				env_destroy(curenv);
f0103d72:	83 ec 0c             	sub    $0xc,%esp
f0103d75:	ff 35 64 82 18 f0    	pushl  0xf0188264
f0103d7b:	e8 18 f5 ff ff       	call   f0103298 <env_destroy>
f0103d80:	83 c4 10             	add    $0x10,%esp

	// Dispatch based on what type of trap occurred
	trap_dispatch(tf);

	// Return to the current environment, which should be running.
	assert(curenv && curenv->env_status == ENV_RUNNING);
f0103d83:	a1 64 82 18 f0       	mov    0xf0188264,%eax
f0103d88:	85 c0                	test   %eax,%eax
f0103d8a:	74 06                	je     f0103d92 <trap+0x143>
f0103d8c:	83 78 54 03          	cmpl   $0x3,0x54(%eax)
f0103d90:	74 19                	je     f0103dab <trap+0x15c>
f0103d92:	68 84 67 10 f0       	push   $0xf0106784
f0103d97:	68 d8 50 10 f0       	push   $0xf01050d8
f0103d9c:	68 19 01 00 00       	push   $0x119
f0103da1:	68 9f 65 10 f0       	push   $0xf010659f
f0103da6:	e8 a1 c2 ff ff       	call   f010004c <_panic>
	env_run(curenv);
f0103dab:	83 ec 0c             	sub    $0xc,%esp
f0103dae:	50                   	push   %eax
f0103daf:	e8 34 f5 ff ff       	call   f01032e8 <env_run>

f0103db4 <handler0>:
#define TY(N) TRAPHANDLER(H(N), N)
#define TN(N) TRAPHANDLER_NOEC(H(N), N)
#define L(N) .long H(N)
#define S(N) .long 0 /* skip */

TN(0)
f0103db4:	6a 00                	push   $0x0
f0103db6:	6a 00                	push   $0x0
f0103db8:	e9 46 01 00 00       	jmp    f0103f03 <_alltraps>
f0103dbd:	90                   	nop

f0103dbe <handler1>:
TN(1)
f0103dbe:	6a 00                	push   $0x0
f0103dc0:	6a 01                	push   $0x1
f0103dc2:	e9 3c 01 00 00       	jmp    f0103f03 <_alltraps>
f0103dc7:	90                   	nop

f0103dc8 <handler2>:
TN(2)
f0103dc8:	6a 00                	push   $0x0
f0103dca:	6a 02                	push   $0x2
f0103dcc:	e9 32 01 00 00       	jmp    f0103f03 <_alltraps>
f0103dd1:	90                   	nop

f0103dd2 <handler3>:
TN(3)
f0103dd2:	6a 00                	push   $0x0
f0103dd4:	6a 03                	push   $0x3
f0103dd6:	e9 28 01 00 00       	jmp    f0103f03 <_alltraps>
f0103ddb:	90                   	nop

f0103ddc <handler4>:
TN(4)
f0103ddc:	6a 00                	push   $0x0
f0103dde:	6a 04                	push   $0x4
f0103de0:	e9 1e 01 00 00       	jmp    f0103f03 <_alltraps>
f0103de5:	90                   	nop

f0103de6 <handler5>:
TN(5)
f0103de6:	6a 00                	push   $0x0
f0103de8:	6a 05                	push   $0x5
f0103dea:	e9 14 01 00 00       	jmp    f0103f03 <_alltraps>
f0103def:	90                   	nop

f0103df0 <handler6>:
TN(6)
f0103df0:	6a 00                	push   $0x0
f0103df2:	6a 06                	push   $0x6
f0103df4:	e9 0a 01 00 00       	jmp    f0103f03 <_alltraps>
f0103df9:	90                   	nop

f0103dfa <handler7>:
TN(7)
f0103dfa:	6a 00                	push   $0x0
f0103dfc:	6a 07                	push   $0x7
f0103dfe:	e9 00 01 00 00       	jmp    f0103f03 <_alltraps>
f0103e03:	90                   	nop

f0103e04 <handler8>:
TY(8)
f0103e04:	6a 08                	push   $0x8
f0103e06:	e9 f8 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e0b:	90                   	nop

f0103e0c <handler10>:
// TN(9) /* reserved */
TY(10)
f0103e0c:	6a 0a                	push   $0xa
f0103e0e:	e9 f0 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e13:	90                   	nop

f0103e14 <handler11>:
TY(11)
f0103e14:	6a 0b                	push   $0xb
f0103e16:	e9 e8 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e1b:	90                   	nop

f0103e1c <handler12>:
TY(12)
f0103e1c:	6a 0c                	push   $0xc
f0103e1e:	e9 e0 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e23:	90                   	nop

f0103e24 <handler13>:
TY(13)
f0103e24:	6a 0d                	push   $0xd
f0103e26:	e9 d8 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e2b:	90                   	nop

f0103e2c <handler14>:
TY(14)
f0103e2c:	6a 0e                	push   $0xe
f0103e2e:	e9 d0 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e33:	90                   	nop

f0103e34 <handler16>:
// TN(15) /* reserved */
TN(16)
f0103e34:	6a 00                	push   $0x0
f0103e36:	6a 10                	push   $0x10
f0103e38:	e9 c6 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e3d:	90                   	nop

f0103e3e <handler17>:
TY(17)
f0103e3e:	6a 11                	push   $0x11
f0103e40:	e9 be 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e45:	90                   	nop

f0103e46 <handler18>:
TN(18)
f0103e46:	6a 00                	push   $0x0
f0103e48:	6a 12                	push   $0x12
f0103e4a:	e9 b4 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e4f:	90                   	nop

f0103e50 <handler19>:
TN(19)
f0103e50:	6a 00                	push   $0x0
f0103e52:	6a 13                	push   $0x13
f0103e54:	e9 aa 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e59:	90                   	nop

f0103e5a <handler32>:

/* 16 IRQs */
TN(32)
f0103e5a:	6a 00                	push   $0x0
f0103e5c:	6a 20                	push   $0x20
f0103e5e:	e9 a0 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e63:	90                   	nop

f0103e64 <handler33>:
TN(33)
f0103e64:	6a 00                	push   $0x0
f0103e66:	6a 21                	push   $0x21
f0103e68:	e9 96 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e6d:	90                   	nop

f0103e6e <handler34>:
TN(34)
f0103e6e:	6a 00                	push   $0x0
f0103e70:	6a 22                	push   $0x22
f0103e72:	e9 8c 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e77:	90                   	nop

f0103e78 <handler35>:
TN(35)
f0103e78:	6a 00                	push   $0x0
f0103e7a:	6a 23                	push   $0x23
f0103e7c:	e9 82 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e81:	90                   	nop

f0103e82 <handler36>:
TN(36)
f0103e82:	6a 00                	push   $0x0
f0103e84:	6a 24                	push   $0x24
f0103e86:	e9 78 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e8b:	90                   	nop

f0103e8c <handler37>:
TN(37)
f0103e8c:	6a 00                	push   $0x0
f0103e8e:	6a 25                	push   $0x25
f0103e90:	e9 6e 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e95:	90                   	nop

f0103e96 <handler38>:
TN(38)
f0103e96:	6a 00                	push   $0x0
f0103e98:	6a 26                	push   $0x26
f0103e9a:	e9 64 00 00 00       	jmp    f0103f03 <_alltraps>
f0103e9f:	90                   	nop

f0103ea0 <handler39>:
TN(39)
f0103ea0:	6a 00                	push   $0x0
f0103ea2:	6a 27                	push   $0x27
f0103ea4:	e9 5a 00 00 00       	jmp    f0103f03 <_alltraps>
f0103ea9:	90                   	nop

f0103eaa <handler40>:
TN(40)
f0103eaa:	6a 00                	push   $0x0
f0103eac:	6a 28                	push   $0x28
f0103eae:	e9 50 00 00 00       	jmp    f0103f03 <_alltraps>
f0103eb3:	90                   	nop

f0103eb4 <handler41>:
TN(41)
f0103eb4:	6a 00                	push   $0x0
f0103eb6:	6a 29                	push   $0x29
f0103eb8:	e9 46 00 00 00       	jmp    f0103f03 <_alltraps>
f0103ebd:	90                   	nop

f0103ebe <handler42>:
TN(42)
f0103ebe:	6a 00                	push   $0x0
f0103ec0:	6a 2a                	push   $0x2a
f0103ec2:	e9 3c 00 00 00       	jmp    f0103f03 <_alltraps>
f0103ec7:	90                   	nop

f0103ec8 <handler43>:
TN(43)
f0103ec8:	6a 00                	push   $0x0
f0103eca:	6a 2b                	push   $0x2b
f0103ecc:	e9 32 00 00 00       	jmp    f0103f03 <_alltraps>
f0103ed1:	90                   	nop

f0103ed2 <handler44>:
TN(44)
f0103ed2:	6a 00                	push   $0x0
f0103ed4:	6a 2c                	push   $0x2c
f0103ed6:	e9 28 00 00 00       	jmp    f0103f03 <_alltraps>
f0103edb:	90                   	nop

f0103edc <handler45>:
TN(45)
f0103edc:	6a 00                	push   $0x0
f0103ede:	6a 2d                	push   $0x2d
f0103ee0:	e9 1e 00 00 00       	jmp    f0103f03 <_alltraps>
f0103ee5:	90                   	nop

f0103ee6 <handler46>:
TN(46)
f0103ee6:	6a 00                	push   $0x0
f0103ee8:	6a 2e                	push   $0x2e
f0103eea:	e9 14 00 00 00       	jmp    f0103f03 <_alltraps>
f0103eef:	90                   	nop

f0103ef0 <handler47>:
TN(47)
f0103ef0:	6a 00                	push   $0x0
f0103ef2:	6a 2f                	push   $0x2f
f0103ef4:	e9 0a 00 00 00       	jmp    f0103f03 <_alltraps>
f0103ef9:	90                   	nop

f0103efa <handler48>:
	
TN(48)
f0103efa:	6a 00                	push   $0x0
f0103efc:	6a 30                	push   $0x30
f0103efe:	e9 00 00 00 00       	jmp    f0103f03 <_alltraps>

f0103f03 <_alltraps>:
 */

.global _alltraps
_alltraps:
	// Build struct Trapframe
	pushl %ds
f0103f03:	1e                   	push   %ds
	pushl %es
f0103f04:	06                   	push   %es
	pushal
f0103f05:	60                   	pusha  

	movl $GD_KD, %eax
f0103f06:	b8 10 00 00 00       	mov    $0x10,%eax
	movw %ax, %ds
f0103f0b:	8e d8                	mov    %eax,%ds
	movw %ax, %es
f0103f0d:	8e c0                	mov    %eax,%es

	pushl %esp /* struct Trapframe * as argument */
f0103f0f:	54                   	push   %esp
	call trap
f0103f10:	e8 3a fd ff ff       	call   f0103c4f <trap>

f0103f15 <syscall>:
}

// Dispatches to the correct kernel function, passing the arguments.
int32_t
syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5)
{
f0103f15:	55                   	push   %ebp
f0103f16:	89 e5                	mov    %esp,%ebp
f0103f18:	83 ec 18             	sub    $0x18,%esp
f0103f1b:	8b 45 08             	mov    0x8(%ebp),%eax
	// Return any appropriate return value.
	// LAB 3: Your code here.

	//panic("syscall not implemented");

	switch (syscallno) {
f0103f1e:	83 f8 01             	cmp    $0x1,%eax
f0103f21:	74 48                	je     f0103f6b <syscall+0x56>
f0103f23:	83 f8 01             	cmp    $0x1,%eax
f0103f26:	72 13                	jb     f0103f3b <syscall+0x26>
f0103f28:	83 f8 02             	cmp    $0x2,%eax
f0103f2b:	0f 84 a6 00 00 00    	je     f0103fd7 <syscall+0xc2>
f0103f31:	83 f8 03             	cmp    $0x3,%eax
f0103f34:	74 3c                	je     f0103f72 <syscall+0x5d>
f0103f36:	e9 a6 00 00 00       	jmp    f0103fe1 <syscall+0xcc>
	// Check that the user has permission to read memory [s, s+len).
	// Destroy the environment if not.

	// LAB 3: Your code here.
	//user_mem_assert(struct Env *env, const void *va, size_t len, int perm)
	user_mem_assert(curenv, (void*)s, len, 0);
f0103f3b:	6a 00                	push   $0x0
f0103f3d:	ff 75 10             	pushl  0x10(%ebp)
f0103f40:	ff 75 0c             	pushl  0xc(%ebp)
f0103f43:	ff 35 64 82 18 f0    	pushl  0xf0188264
f0103f49:	e8 ec ec ff ff       	call   f0102c3a <user_mem_assert>
	// Print the string supplied by the user.
	cprintf("%.*s", len, s);
f0103f4e:	83 c4 0c             	add    $0xc,%esp
f0103f51:	ff 75 0c             	pushl  0xc(%ebp)
f0103f54:	ff 75 10             	pushl  0x10(%ebp)
f0103f57:	68 10 68 10 f0       	push   $0xf0106810
f0103f5c:	e8 30 f4 ff ff       	call   f0103391 <cprintf>
f0103f61:	83 c4 10             	add    $0x10,%esp
	//panic("syscall not implemented");

	switch (syscallno) {
		case SYS_cputs:
			sys_cputs((char*)a1, (size_t)a2);  //Print a string to the system console.
			return 0;
f0103f64:	b8 00 00 00 00       	mov    $0x0,%eax
f0103f69:	eb 7b                	jmp    f0103fe6 <syscall+0xd1>
// Read a character from the system console without blocking.
// Returns the character, or 0 if there is no input waiting.
static int
sys_cgetc(void)
{
	return cons_getc();
f0103f6b:	e8 75 c5 ff ff       	call   f01004e5 <cons_getc>
		case SYS_cputs:
			sys_cputs((char*)a1, (size_t)a2);  //Print a string to the system console.
			return 0;

		case SYS_cgetc:
			return sys_cgetc(); //Read a character from the system console without blocking.
f0103f70:	eb 74                	jmp    f0103fe6 <syscall+0xd1>
sys_env_destroy(envid_t envid)
{
	int r;
	struct Env *e;

	if ((r = envid2env(envid, &e, 1)) < 0)
f0103f72:	83 ec 04             	sub    $0x4,%esp
f0103f75:	6a 01                	push   $0x1
f0103f77:	8d 45 f4             	lea    -0xc(%ebp),%eax
f0103f7a:	50                   	push   %eax
f0103f7b:	ff 75 10             	pushl  0x10(%ebp)
f0103f7e:	e8 a5 ed ff ff       	call   f0102d28 <envid2env>
f0103f83:	83 c4 10             	add    $0x10,%esp
f0103f86:	85 c0                	test   %eax,%eax
f0103f88:	78 5c                	js     f0103fe6 <syscall+0xd1>
		return r;
	if (e == curenv)
f0103f8a:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103f8d:	8b 15 64 82 18 f0    	mov    0xf0188264,%edx
f0103f93:	39 d0                	cmp    %edx,%eax
f0103f95:	75 15                	jne    f0103fac <syscall+0x97>
		cprintf("[%08x] exiting gracefully\n", curenv->env_id);
f0103f97:	83 ec 08             	sub    $0x8,%esp
f0103f9a:	ff 70 48             	pushl  0x48(%eax)
f0103f9d:	68 15 68 10 f0       	push   $0xf0106815
f0103fa2:	e8 ea f3 ff ff       	call   f0103391 <cprintf>
f0103fa7:	83 c4 10             	add    $0x10,%esp
f0103faa:	eb 16                	jmp    f0103fc2 <syscall+0xad>
	else
		cprintf("[%08x] destroying %08x\n", curenv->env_id, e->env_id);
f0103fac:	83 ec 04             	sub    $0x4,%esp
f0103faf:	ff 70 48             	pushl  0x48(%eax)
f0103fb2:	ff 72 48             	pushl  0x48(%edx)
f0103fb5:	68 30 68 10 f0       	push   $0xf0106830
f0103fba:	e8 d2 f3 ff ff       	call   f0103391 <cprintf>
f0103fbf:	83 c4 10             	add    $0x10,%esp
	env_destroy(e);
f0103fc2:	83 ec 0c             	sub    $0xc,%esp
f0103fc5:	ff 75 f4             	pushl  -0xc(%ebp)
f0103fc8:	e8 cb f2 ff ff       	call   f0103298 <env_destroy>
f0103fcd:	83 c4 10             	add    $0x10,%esp
	return 0;
f0103fd0:	b8 00 00 00 00       	mov    $0x0,%eax
f0103fd5:	eb 0f                	jmp    f0103fe6 <syscall+0xd1>

// Returns the current environment's envid.
static envid_t
sys_getenvid(void)
{
	return curenv->env_id;
f0103fd7:	a1 64 82 18 f0       	mov    0xf0188264,%eax
f0103fdc:	8b 40 48             	mov    0x48(%eax),%eax

		case SYS_env_destroy:
			return sys_env_destroy(a2); //Destroy a given environment (possibly the currently running environment).

		case SYS_getenvid:
			return sys_getenvid(); //get the current environment's envid.
f0103fdf:	eb 05                	jmp    f0103fe6 <syscall+0xd1>

		default:
			return -E_INVAL;
f0103fe1:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
	}
}
f0103fe6:	c9                   	leave  
f0103fe7:	c3                   	ret    

f0103fe8 <stab_binsearch>:
//	will exit setting left = 118, right = 554.
//
static void
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
f0103fe8:	55                   	push   %ebp
f0103fe9:	89 e5                	mov    %esp,%ebp
f0103feb:	57                   	push   %edi
f0103fec:	56                   	push   %esi
f0103fed:	53                   	push   %ebx
f0103fee:	83 ec 14             	sub    $0x14,%esp
f0103ff1:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0103ff4:	89 55 e4             	mov    %edx,-0x1c(%ebp)
f0103ff7:	89 4d e0             	mov    %ecx,-0x20(%ebp)
f0103ffa:	8b 7d 08             	mov    0x8(%ebp),%edi
	int l = *region_left, r = *region_right, any_matches = 0;
f0103ffd:	8b 1a                	mov    (%edx),%ebx
f0103fff:	8b 01                	mov    (%ecx),%eax
f0104001:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0104004:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)

	while (l <= r) {
f010400b:	eb 7e                	jmp    f010408b <stab_binsearch+0xa3>
		int true_m = (l + r) / 2, m = true_m;
f010400d:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0104010:	01 d8                	add    %ebx,%eax
f0104012:	89 c6                	mov    %eax,%esi
f0104014:	c1 ee 1f             	shr    $0x1f,%esi
f0104017:	01 c6                	add    %eax,%esi
f0104019:	d1 fe                	sar    %esi
f010401b:	8d 04 36             	lea    (%esi,%esi,1),%eax
f010401e:	01 f0                	add    %esi,%eax
f0104020:	8b 4d ec             	mov    -0x14(%ebp),%ecx
f0104023:	8d 54 81 04          	lea    0x4(%ecx,%eax,4),%edx
f0104027:	89 f0                	mov    %esi,%eax

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f0104029:	eb 01                	jmp    f010402c <stab_binsearch+0x44>
			m--;
f010402b:	48                   	dec    %eax

	while (l <= r) {
		int true_m = (l + r) / 2, m = true_m;

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f010402c:	39 c3                	cmp    %eax,%ebx
f010402e:	7f 0c                	jg     f010403c <stab_binsearch+0x54>
f0104030:	0f b6 0a             	movzbl (%edx),%ecx
f0104033:	83 ea 0c             	sub    $0xc,%edx
f0104036:	39 f9                	cmp    %edi,%ecx
f0104038:	75 f1                	jne    f010402b <stab_binsearch+0x43>
f010403a:	eb 05                	jmp    f0104041 <stab_binsearch+0x59>
			m--;
		if (m < l) {	// no match in [l, m]
			l = true_m + 1;
f010403c:	8d 5e 01             	lea    0x1(%esi),%ebx
			continue;
f010403f:	eb 4a                	jmp    f010408b <stab_binsearch+0xa3>
		}

		// actual binary search
		any_matches = 1;
		if (stabs[m].n_value < addr) {
f0104041:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104044:	01 c2                	add    %eax,%edx
f0104046:	8b 4d ec             	mov    -0x14(%ebp),%ecx
f0104049:	8b 54 91 08          	mov    0x8(%ecx,%edx,4),%edx
f010404d:	39 55 0c             	cmp    %edx,0xc(%ebp)
f0104050:	76 11                	jbe    f0104063 <stab_binsearch+0x7b>
			*region_left = m;
f0104052:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
f0104055:	89 03                	mov    %eax,(%ebx)
			l = true_m + 1;
f0104057:	8d 5e 01             	lea    0x1(%esi),%ebx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f010405a:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
f0104061:	eb 28                	jmp    f010408b <stab_binsearch+0xa3>
		if (stabs[m].n_value < addr) {
			*region_left = m;
			l = true_m + 1;
		} else if (stabs[m].n_value > addr) {
f0104063:	39 55 0c             	cmp    %edx,0xc(%ebp)
f0104066:	73 12                	jae    f010407a <stab_binsearch+0x92>
			*region_right = m - 1;
f0104068:	48                   	dec    %eax
f0104069:	89 45 f0             	mov    %eax,-0x10(%ebp)
f010406c:	8b 75 e0             	mov    -0x20(%ebp),%esi
f010406f:	89 06                	mov    %eax,(%esi)
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0104071:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
f0104078:	eb 11                	jmp    f010408b <stab_binsearch+0xa3>
			*region_right = m - 1;
			r = m - 1;
		} else {
			// exact match for 'addr', but continue loop to find
			// *region_right
			*region_left = m;
f010407a:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f010407d:	89 06                	mov    %eax,(%esi)
			l = m;
			addr++;
f010407f:	ff 45 0c             	incl   0xc(%ebp)
f0104082:	89 c3                	mov    %eax,%ebx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0104084:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
	int l = *region_left, r = *region_right, any_matches = 0;

	while (l <= r) {
f010408b:	3b 5d f0             	cmp    -0x10(%ebp),%ebx
f010408e:	0f 8e 79 ff ff ff    	jle    f010400d <stab_binsearch+0x25>
			l = m;
			addr++;
		}
	}

	if (!any_matches)
f0104094:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
f0104098:	75 0d                	jne    f01040a7 <stab_binsearch+0xbf>
		*region_right = *region_left - 1;
f010409a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f010409d:	8b 00                	mov    (%eax),%eax
f010409f:	48                   	dec    %eax
f01040a0:	8b 7d e0             	mov    -0x20(%ebp),%edi
f01040a3:	89 07                	mov    %eax,(%edi)
f01040a5:	eb 2c                	jmp    f01040d3 <stab_binsearch+0xeb>
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f01040a7:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01040aa:	8b 00                	mov    (%eax),%eax
		     l > *region_left && stabs[l].n_type != type;
f01040ac:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f01040af:	8b 0e                	mov    (%esi),%ecx
f01040b1:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01040b4:	01 c2                	add    %eax,%edx
f01040b6:	8b 75 ec             	mov    -0x14(%ebp),%esi
f01040b9:	8d 54 96 04          	lea    0x4(%esi,%edx,4),%edx

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f01040bd:	eb 01                	jmp    f01040c0 <stab_binsearch+0xd8>
		     l > *region_left && stabs[l].n_type != type;
		     l--)
f01040bf:	48                   	dec    %eax

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f01040c0:	39 c8                	cmp    %ecx,%eax
f01040c2:	7e 0a                	jle    f01040ce <stab_binsearch+0xe6>
		     l > *region_left && stabs[l].n_type != type;
f01040c4:	0f b6 1a             	movzbl (%edx),%ebx
f01040c7:	83 ea 0c             	sub    $0xc,%edx
f01040ca:	39 df                	cmp    %ebx,%edi
f01040cc:	75 f1                	jne    f01040bf <stab_binsearch+0xd7>
		     l--)
			/* do nothing */;
		*region_left = l;
f01040ce:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f01040d1:	89 07                	mov    %eax,(%edi)
	}
}
f01040d3:	83 c4 14             	add    $0x14,%esp
f01040d6:	5b                   	pop    %ebx
f01040d7:	5e                   	pop    %esi
f01040d8:	5f                   	pop    %edi
f01040d9:	5d                   	pop    %ebp
f01040da:	c3                   	ret    

f01040db <debuginfo_eip>:
//	negative if not.  But even if it returns negative it has stored some
//	information into '*info'.
//
int
debuginfo_eip(uintptr_t addr, struct Eipdebuginfo *info)
{
f01040db:	55                   	push   %ebp
f01040dc:	89 e5                	mov    %esp,%ebp
f01040de:	57                   	push   %edi
f01040df:	56                   	push   %esi
f01040e0:	53                   	push   %ebx
f01040e1:	83 ec 3c             	sub    $0x3c,%esp
f01040e4:	8b 75 08             	mov    0x8(%ebp),%esi
f01040e7:	8b 7d 0c             	mov    0xc(%ebp),%edi
	const struct Stab *stabs, *stab_end;
	const char *stabstr, *stabstr_end;
	int lfile, rfile, lfun, rfun, lline, rline;

	// Initialize *info
	info->eip_file = "<unknown>";
f01040ea:	c7 07 48 68 10 f0    	movl   $0xf0106848,(%edi)
	info->eip_line = 0;
f01040f0:	c7 47 04 00 00 00 00 	movl   $0x0,0x4(%edi)
	info->eip_fn_name = "<unknown>";
f01040f7:	c7 47 08 48 68 10 f0 	movl   $0xf0106848,0x8(%edi)
	info->eip_fn_namelen = 9;
f01040fe:	c7 47 0c 09 00 00 00 	movl   $0x9,0xc(%edi)
	info->eip_fn_addr = addr;
f0104105:	89 77 10             	mov    %esi,0x10(%edi)
	info->eip_fn_narg = 0;
f0104108:	c7 47 14 00 00 00 00 	movl   $0x0,0x14(%edi)

	// Find the relevant set of stabs
	if (addr >= ULIM) {
f010410f:	81 fe ff ff 7f ef    	cmp    $0xef7fffff,%esi
f0104115:	77 68                	ja     f010417f <debuginfo_eip+0xa4>

		// Make sure this memory is valid.
		// Return -1 if it is not.  Hint: Call user_mem_check.
		// LAB 3: Your code here.

		stabs = usd->stabs;
f0104117:	a1 00 00 20 00       	mov    0x200000,%eax
f010411c:	89 45 c0             	mov    %eax,-0x40(%ebp)
		stab_end = usd->stab_end;
f010411f:	8b 1d 04 00 20 00    	mov    0x200004,%ebx
		stabstr = usd->stabstr;
f0104125:	8b 15 08 00 20 00    	mov    0x200008,%edx
f010412b:	89 55 b8             	mov    %edx,-0x48(%ebp)
		stabstr_end = usd->stabstr_end;
f010412e:	a1 0c 00 20 00       	mov    0x20000c,%eax
f0104133:	89 45 bc             	mov    %eax,-0x44(%ebp)

		// Make sure the STABS and string table memory is valid.
		// LAB 3: Your code here.
		user_mem_assert(curenv, usd, sizeof(struct UserStabData), PTE_U);
f0104136:	6a 04                	push   $0x4
f0104138:	6a 10                	push   $0x10
f010413a:	68 00 00 20 00       	push   $0x200000
f010413f:	ff 35 64 82 18 f0    	pushl  0xf0188264
f0104145:	e8 f0 ea ff ff       	call   f0102c3a <user_mem_assert>
		user_mem_assert(curenv, stabs, (uintptr_t)stab_end - (uintptr_t)stabs, PTE_U);
f010414a:	6a 04                	push   $0x4
f010414c:	89 d8                	mov    %ebx,%eax
f010414e:	8b 4d c0             	mov    -0x40(%ebp),%ecx
f0104151:	29 c8                	sub    %ecx,%eax
f0104153:	50                   	push   %eax
f0104154:	51                   	push   %ecx
f0104155:	ff 35 64 82 18 f0    	pushl  0xf0188264
f010415b:	e8 da ea ff ff       	call   f0102c3a <user_mem_assert>
		user_mem_assert(curenv, stabstr, (uintptr_t)stabstr_end - (uintptr_t)stabstr, PTE_U);
f0104160:	83 c4 20             	add    $0x20,%esp
f0104163:	6a 04                	push   $0x4
f0104165:	8b 45 bc             	mov    -0x44(%ebp),%eax
f0104168:	8b 55 b8             	mov    -0x48(%ebp),%edx
f010416b:	29 d0                	sub    %edx,%eax
f010416d:	50                   	push   %eax
f010416e:	52                   	push   %edx
f010416f:	ff 35 64 82 18 f0    	pushl  0xf0188264
f0104175:	e8 c0 ea ff ff       	call   f0102c3a <user_mem_assert>
f010417a:	83 c4 10             	add    $0x10,%esp
f010417d:	eb 1a                	jmp    f0104199 <debuginfo_eip+0xbe>
	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
		stab_end = __STAB_END__;
		stabstr = __STABSTR_BEGIN__;
		stabstr_end = __STABSTR_END__;
f010417f:	c7 45 bc df 27 11 f0 	movl   $0xf01127df,-0x44(%ebp)

	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
		stab_end = __STAB_END__;
		stabstr = __STABSTR_BEGIN__;
f0104186:	c7 45 b8 7d f1 10 f0 	movl   $0xf010f17d,-0x48(%ebp)
	info->eip_fn_narg = 0;

	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
		stab_end = __STAB_END__;
f010418d:	bb 7c f1 10 f0       	mov    $0xf010f17c,%ebx
	info->eip_fn_addr = addr;
	info->eip_fn_narg = 0;

	// Find the relevant set of stabs
	if (addr >= ULIM) {
		stabs = __STAB_BEGIN__;
f0104192:	c7 45 c0 50 6f 10 f0 	movl   $0xf0106f50,-0x40(%ebp)
		user_mem_assert(curenv, stabs, (uintptr_t)stab_end - (uintptr_t)stabs, PTE_U);
		user_mem_assert(curenv, stabstr, (uintptr_t)stabstr_end - (uintptr_t)stabstr, PTE_U);
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
f0104199:	8b 45 bc             	mov    -0x44(%ebp),%eax
f010419c:	39 45 b8             	cmp    %eax,-0x48(%ebp)
f010419f:	0f 83 a6 01 00 00    	jae    f010434b <debuginfo_eip+0x270>
f01041a5:	80 78 ff 00          	cmpb   $0x0,-0x1(%eax)
f01041a9:	0f 85 a3 01 00 00    	jne    f0104352 <debuginfo_eip+0x277>
	// 'eip'.  First, we find the basic source file containing 'eip'.
	// Then, we look in that source file for the function.  Then we look
	// for the line number.

	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
f01041af:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
	rfile = (stab_end - stabs) - 1;
f01041b6:	2b 5d c0             	sub    -0x40(%ebp),%ebx
f01041b9:	c1 fb 02             	sar    $0x2,%ebx
f01041bc:	8d 04 9b             	lea    (%ebx,%ebx,4),%eax
f01041bf:	8d 04 83             	lea    (%ebx,%eax,4),%eax
f01041c2:	8d 14 83             	lea    (%ebx,%eax,4),%edx
f01041c5:	89 d1                	mov    %edx,%ecx
f01041c7:	c1 e1 08             	shl    $0x8,%ecx
f01041ca:	01 ca                	add    %ecx,%edx
f01041cc:	89 d1                	mov    %edx,%ecx
f01041ce:	c1 e1 10             	shl    $0x10,%ecx
f01041d1:	89 c8                	mov    %ecx,%eax
f01041d3:	01 d0                	add    %edx,%eax
f01041d5:	01 c0                	add    %eax,%eax
f01041d7:	8d 44 03 ff          	lea    -0x1(%ebx,%eax,1),%eax
f01041db:	89 45 e0             	mov    %eax,-0x20(%ebp)
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
f01041de:	83 ec 08             	sub    $0x8,%esp
f01041e1:	56                   	push   %esi
f01041e2:	6a 64                	push   $0x64
f01041e4:	8d 55 e0             	lea    -0x20(%ebp),%edx
f01041e7:	89 d1                	mov    %edx,%ecx
f01041e9:	8d 55 e4             	lea    -0x1c(%ebp),%edx
f01041ec:	8b 5d c0             	mov    -0x40(%ebp),%ebx
f01041ef:	89 d8                	mov    %ebx,%eax
f01041f1:	e8 f2 fd ff ff       	call   f0103fe8 <stab_binsearch>
	if (lfile == 0)
f01041f6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01041f9:	83 c4 10             	add    $0x10,%esp
f01041fc:	85 c0                	test   %eax,%eax
f01041fe:	0f 84 55 01 00 00    	je     f0104359 <debuginfo_eip+0x27e>
		return -1;

	// Search within that file's stabs for the function definition
	// (N_FUN).
	lfun = lfile;
f0104204:	89 45 dc             	mov    %eax,-0x24(%ebp)
	rfun = rfile;
f0104207:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010420a:	89 45 d8             	mov    %eax,-0x28(%ebp)
	stab_binsearch(stabs, &lfun, &rfun, N_FUN, addr);
f010420d:	83 ec 08             	sub    $0x8,%esp
f0104210:	56                   	push   %esi
f0104211:	6a 24                	push   $0x24
f0104213:	8d 55 d8             	lea    -0x28(%ebp),%edx
f0104216:	89 d1                	mov    %edx,%ecx
f0104218:	8d 55 dc             	lea    -0x24(%ebp),%edx
f010421b:	89 d8                	mov    %ebx,%eax
f010421d:	e8 c6 fd ff ff       	call   f0103fe8 <stab_binsearch>

	if (lfun <= rfun) {
f0104222:	8b 45 dc             	mov    -0x24(%ebp),%eax
f0104225:	8b 55 d8             	mov    -0x28(%ebp),%edx
f0104228:	89 55 c4             	mov    %edx,-0x3c(%ebp)
f010422b:	83 c4 10             	add    $0x10,%esp
f010422e:	39 d0                	cmp    %edx,%eax
f0104230:	7f 2d                	jg     f010425f <debuginfo_eip+0x184>
		// stabs[lfun] points to the function name
		// in the string table, but check bounds just in case.
		if (stabs[lfun].n_strx < stabstr_end - stabstr)
f0104232:	8d 14 00             	lea    (%eax,%eax,1),%edx
f0104235:	01 c2                	add    %eax,%edx
f0104237:	8d 0c 93             	lea    (%ebx,%edx,4),%ecx
f010423a:	8b 11                	mov    (%ecx),%edx
f010423c:	8b 5d bc             	mov    -0x44(%ebp),%ebx
f010423f:	2b 5d b8             	sub    -0x48(%ebp),%ebx
f0104242:	39 da                	cmp    %ebx,%edx
f0104244:	73 06                	jae    f010424c <debuginfo_eip+0x171>
			info->eip_fn_name = stabstr + stabs[lfun].n_strx;
f0104246:	03 55 b8             	add    -0x48(%ebp),%edx
f0104249:	89 57 08             	mov    %edx,0x8(%edi)
		info->eip_fn_addr = stabs[lfun].n_value;
f010424c:	8b 51 08             	mov    0x8(%ecx),%edx
f010424f:	89 57 10             	mov    %edx,0x10(%edi)
		addr -= info->eip_fn_addr;
f0104252:	29 d6                	sub    %edx,%esi
		// Search within the function definition for the line number.
		lline = lfun;
f0104254:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		rline = rfun;
f0104257:	8b 45 c4             	mov    -0x3c(%ebp),%eax
f010425a:	89 45 d0             	mov    %eax,-0x30(%ebp)
f010425d:	eb 0f                	jmp    f010426e <debuginfo_eip+0x193>
	} else {
		// Couldn't find function stab!  Maybe we're in an assembly
		// file.  Search the whole file for the line number.
		info->eip_fn_addr = addr;
f010425f:	89 77 10             	mov    %esi,0x10(%edi)
		lline = lfile;
f0104262:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0104265:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		rline = rfile;
f0104268:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010426b:	89 45 d0             	mov    %eax,-0x30(%ebp)
	}
	// Ignore stuff after the colon.
	info->eip_fn_namelen = strfind(info->eip_fn_name, ':') - info->eip_fn_name;
f010426e:	83 ec 08             	sub    $0x8,%esp
f0104271:	6a 3a                	push   $0x3a
f0104273:	ff 77 08             	pushl  0x8(%edi)
f0104276:	e8 b0 09 00 00       	call   f0104c2b <strfind>
f010427b:	2b 47 08             	sub    0x8(%edi),%eax
f010427e:	89 47 0c             	mov    %eax,0xc(%edi)
	// Hint:
	//	There's a particular stabs type used for line numbers.
	//	Look at the STABS documentation and <inc/stab.h> to find
	//	which one.
	// Your code here.
	stab_binsearch(stabs, &lline, &rline, N_SLINE, addr);
f0104281:	83 c4 08             	add    $0x8,%esp
f0104284:	56                   	push   %esi
f0104285:	6a 44                	push   $0x44
f0104287:	8d 4d d0             	lea    -0x30(%ebp),%ecx
f010428a:	8d 55 d4             	lea    -0x2c(%ebp),%edx
f010428d:	8b 5d c0             	mov    -0x40(%ebp),%ebx
f0104290:	89 d8                	mov    %ebx,%eax
f0104292:	e8 51 fd ff ff       	call   f0103fe8 <stab_binsearch>
	info->eip_line = stabs[lline].n_value;
f0104297:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f010429a:	8d 14 00             	lea    (%eax,%eax,1),%edx
f010429d:	01 c2                	add    %eax,%edx
f010429f:	c1 e2 02             	shl    $0x2,%edx
f01042a2:	8b 4c 13 08          	mov    0x8(%ebx,%edx,1),%ecx
f01042a6:	89 4f 04             	mov    %ecx,0x4(%edi)
	// Search backwards from the line number for the relevant filename
	// stab.
	// We can't just use the "lfile" stab because inlined functions
	// can interpolate code from a different file!
	// Such included source files use the N_SOL stab type.
	while (lline >= lfile
f01042a9:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f01042ac:	8d 54 13 08          	lea    0x8(%ebx,%edx,1),%edx
f01042b0:	83 c4 10             	add    $0x10,%esp
f01042b3:	c6 45 c4 00          	movb   $0x0,-0x3c(%ebp)
f01042b7:	89 7d 0c             	mov    %edi,0xc(%ebp)
f01042ba:	eb 08                	jmp    f01042c4 <debuginfo_eip+0x1e9>
f01042bc:	48                   	dec    %eax
f01042bd:	83 ea 0c             	sub    $0xc,%edx
f01042c0:	c6 45 c4 01          	movb   $0x1,-0x3c(%ebp)
f01042c4:	39 c6                	cmp    %eax,%esi
f01042c6:	7e 05                	jle    f01042cd <debuginfo_eip+0x1f2>
f01042c8:	8b 7d 0c             	mov    0xc(%ebp),%edi
f01042cb:	eb 47                	jmp    f0104314 <debuginfo_eip+0x239>
	       && stabs[lline].n_type != N_SOL
f01042cd:	8a 4a fc             	mov    -0x4(%edx),%cl
f01042d0:	80 f9 84             	cmp    $0x84,%cl
f01042d3:	75 0e                	jne    f01042e3 <debuginfo_eip+0x208>
f01042d5:	8b 7d 0c             	mov    0xc(%ebp),%edi
f01042d8:	80 7d c4 00          	cmpb   $0x0,-0x3c(%ebp)
f01042dc:	74 1b                	je     f01042f9 <debuginfo_eip+0x21e>
f01042de:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f01042e1:	eb 16                	jmp    f01042f9 <debuginfo_eip+0x21e>
	       && (stabs[lline].n_type != N_SO || !stabs[lline].n_value))
f01042e3:	80 f9 64             	cmp    $0x64,%cl
f01042e6:	75 d4                	jne    f01042bc <debuginfo_eip+0x1e1>
f01042e8:	83 3a 00             	cmpl   $0x0,(%edx)
f01042eb:	74 cf                	je     f01042bc <debuginfo_eip+0x1e1>
f01042ed:	8b 7d 0c             	mov    0xc(%ebp),%edi
f01042f0:	80 7d c4 00          	cmpb   $0x0,-0x3c(%ebp)
f01042f4:	74 03                	je     f01042f9 <debuginfo_eip+0x21e>
f01042f6:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		lline--;
	if (lline >= lfile && stabs[lline].n_strx < stabstr_end - stabstr)
f01042f9:	8d 14 00             	lea    (%eax,%eax,1),%edx
f01042fc:	01 d0                	add    %edx,%eax
f01042fe:	8b 75 c0             	mov    -0x40(%ebp),%esi
f0104301:	8b 14 86             	mov    (%esi,%eax,4),%edx
f0104304:	8b 45 bc             	mov    -0x44(%ebp),%eax
f0104307:	8b 75 b8             	mov    -0x48(%ebp),%esi
f010430a:	29 f0                	sub    %esi,%eax
f010430c:	39 c2                	cmp    %eax,%edx
f010430e:	73 04                	jae    f0104314 <debuginfo_eip+0x239>
		info->eip_file = stabstr + stabs[lline].n_strx;
f0104310:	01 f2                	add    %esi,%edx
f0104312:	89 17                	mov    %edx,(%edi)


	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	if (lfun < rfun)
f0104314:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0104317:	8b 5d d8             	mov    -0x28(%ebp),%ebx
f010431a:	39 da                	cmp    %ebx,%edx
f010431c:	7d 42                	jge    f0104360 <debuginfo_eip+0x285>
		for (lline = lfun + 1;
f010431e:	42                   	inc    %edx
f010431f:	89 55 d4             	mov    %edx,-0x2c(%ebp)
f0104322:	89 d0                	mov    %edx,%eax
f0104324:	8d 0c 12             	lea    (%edx,%edx,1),%ecx
f0104327:	01 ca                	add    %ecx,%edx
f0104329:	8b 75 c0             	mov    -0x40(%ebp),%esi
f010432c:	8d 54 96 04          	lea    0x4(%esi,%edx,4),%edx
f0104330:	eb 03                	jmp    f0104335 <debuginfo_eip+0x25a>
		     lline < rfun && stabs[lline].n_type == N_PSYM;
		     lline++)
			info->eip_fn_narg++;
f0104332:	ff 47 14             	incl   0x14(%edi)


	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	if (lfun < rfun)
		for (lline = lfun + 1;
f0104335:	39 c3                	cmp    %eax,%ebx
f0104337:	7e 2e                	jle    f0104367 <debuginfo_eip+0x28c>
		     lline < rfun && stabs[lline].n_type == N_PSYM;
f0104339:	8a 0a                	mov    (%edx),%cl
f010433b:	40                   	inc    %eax
f010433c:	83 c2 0c             	add    $0xc,%edx
f010433f:	80 f9 a0             	cmp    $0xa0,%cl
f0104342:	74 ee                	je     f0104332 <debuginfo_eip+0x257>
		     lline++)
			info->eip_fn_narg++;

	return 0;
f0104344:	b8 00 00 00 00       	mov    $0x0,%eax
f0104349:	eb 21                	jmp    f010436c <debuginfo_eip+0x291>
		user_mem_assert(curenv, stabstr, (uintptr_t)stabstr_end - (uintptr_t)stabstr, PTE_U);
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
		return -1;
f010434b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0104350:	eb 1a                	jmp    f010436c <debuginfo_eip+0x291>
f0104352:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0104357:	eb 13                	jmp    f010436c <debuginfo_eip+0x291>
	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
	rfile = (stab_end - stabs) - 1;
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
	if (lfile == 0)
		return -1;
f0104359:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f010435e:	eb 0c                	jmp    f010436c <debuginfo_eip+0x291>
		for (lline = lfun + 1;
		     lline < rfun && stabs[lline].n_type == N_PSYM;
		     lline++)
			info->eip_fn_narg++;

	return 0;
f0104360:	b8 00 00 00 00       	mov    $0x0,%eax
f0104365:	eb 05                	jmp    f010436c <debuginfo_eip+0x291>
f0104367:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010436c:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010436f:	5b                   	pop    %ebx
f0104370:	5e                   	pop    %esi
f0104371:	5f                   	pop    %edi
f0104372:	5d                   	pop    %ebp
f0104373:	c3                   	ret    

f0104374 <cpuid_print>:
	return feature[bit / 32] & BIT(bit % 32);
}

void
cpuid_print(void)
{
f0104374:	55                   	push   %ebp
f0104375:	89 e5                	mov    %esp,%ebp
f0104377:	57                   	push   %edi
f0104378:	56                   	push   %esi
f0104379:	53                   	push   %ebx
f010437a:	83 ec 6c             	sub    $0x6c,%esp
	uint32_t eax, brand[12], feature[CPUID_NR_FLAGS] = {0};
f010437d:	8d 7d a4             	lea    -0x5c(%ebp),%edi
f0104380:	b9 05 00 00 00       	mov    $0x5,%ecx
f0104385:	b8 00 00 00 00       	mov    $0x0,%eax
f010438a:	f3 ab                	rep stos %eax,%es:(%edi)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f010438c:	b8 00 00 00 80       	mov    $0x80000000,%eax
f0104391:	0f a2                	cpuid  

	cpuid(0x80000000, &eax, NULL, NULL, NULL);
	if (eax < 0x80000004)
f0104393:	3d 03 00 00 80       	cmp    $0x80000003,%eax
f0104398:	77 17                	ja     f01043b1 <cpuid_print+0x3d>
		panic("CPU too old!");
f010439a:	83 ec 04             	sub    $0x4,%esp
f010439d:	68 52 68 10 f0       	push   $0xf0106852
f01043a2:	68 8d 00 00 00       	push   $0x8d
f01043a7:	68 5f 68 10 f0       	push   $0xf010685f
f01043ac:	e8 9b bc ff ff       	call   f010004c <_panic>
f01043b1:	b8 02 00 00 80       	mov    $0x80000002,%eax
f01043b6:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f01043b8:	89 45 b8             	mov    %eax,-0x48(%ebp)
	if (ebxp)
		*ebxp = ebx;
f01043bb:	89 5d bc             	mov    %ebx,-0x44(%ebp)
	if (ecxp)
		*ecxp = ecx;
f01043be:	89 4d c0             	mov    %ecx,-0x40(%ebp)
	if (edxp)
		*edxp = edx;
f01043c1:	89 55 c4             	mov    %edx,-0x3c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f01043c4:	b8 03 00 00 80       	mov    $0x80000003,%eax
f01043c9:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f01043cb:	89 45 c8             	mov    %eax,-0x38(%ebp)
	if (ebxp)
		*ebxp = ebx;
f01043ce:	89 5d cc             	mov    %ebx,-0x34(%ebp)
	if (ecxp)
		*ecxp = ecx;
f01043d1:	89 4d d0             	mov    %ecx,-0x30(%ebp)
	if (edxp)
		*edxp = edx;
f01043d4:	89 55 d4             	mov    %edx,-0x2c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f01043d7:	b8 04 00 00 80       	mov    $0x80000004,%eax
f01043dc:	0f a2                	cpuid  
		     : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		     : "a" (info));
	if (eaxp)
		*eaxp = eax;
f01043de:	89 45 d8             	mov    %eax,-0x28(%ebp)
	if (ebxp)
		*ebxp = ebx;
f01043e1:	89 5d dc             	mov    %ebx,-0x24(%ebp)
	if (ecxp)
		*ecxp = ecx;
f01043e4:	89 4d e0             	mov    %ecx,-0x20(%ebp)
	if (edxp)
		*edxp = edx;
f01043e7:	89 55 e4             	mov    %edx,-0x1c(%ebp)

	cpuid(0x80000002, &brand[0], &brand[1], &brand[2], &brand[3]);
	cpuid(0x80000003, &brand[4], &brand[5], &brand[6], &brand[7]);
	cpuid(0x80000004, &brand[8], &brand[9], &brand[10], &brand[11]);
	cprintf("CPU: %.48s\n", brand);
f01043ea:	83 ec 08             	sub    $0x8,%esp
f01043ed:	8d 45 b8             	lea    -0x48(%ebp),%eax
f01043f0:	50                   	push   %eax
f01043f1:	68 6b 68 10 f0       	push   $0xf010686b
f01043f6:	e8 96 ef ff ff       	call   f0103391 <cprintf>

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f01043fb:	b8 01 00 00 00       	mov    $0x1,%eax
f0104400:	0f a2                	cpuid  
f0104402:	89 55 90             	mov    %edx,-0x70(%ebp)
	if (eaxp)
		*eaxp = eax;
	if (ebxp)
		*ebxp = ebx;
	if (ecxp)
		*ecxp = ecx;
f0104405:	89 4d a8             	mov    %ecx,-0x58(%ebp)
	if (edxp)
		*edxp = edx;
f0104408:	89 55 a4             	mov    %edx,-0x5c(%ebp)

static inline void
cpuid(uint32_t info, uint32_t *eaxp, uint32_t *ebxp, uint32_t *ecxp, uint32_t *edxp)
{
	uint32_t eax, ebx, ecx, edx;
	asm volatile("cpuid"
f010440b:	b8 01 00 00 80       	mov    $0x80000001,%eax
f0104410:	0f a2                	cpuid  
	if (eaxp)
		*eaxp = eax;
	if (ebxp)
		*ebxp = ebx;
	if (ecxp)
		*ecxp = ecx;
f0104412:	89 4d b4             	mov    %ecx,-0x4c(%ebp)
	if (edxp)
		*edxp = edx;
f0104415:	89 55 b0             	mov    %edx,-0x50(%ebp)
f0104418:	83 c4 10             	add    $0x10,%esp
static void
print_feature(uint32_t *feature)
{
	int i, j;

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
f010441b:	c7 45 94 00 00 00 00 	movl   $0x0,-0x6c(%ebp)
		if (!feature[i])
f0104422:	8b 45 94             	mov    -0x6c(%ebp),%eax
f0104425:	8b 74 85 a4          	mov    -0x5c(%ebp,%eax,4),%esi
f0104429:	85 f6                	test   %esi,%esi
f010442b:	74 5a                	je     f0104487 <cpuid_print+0x113>
			continue;
		cprintf(" ");
f010442d:	83 ec 0c             	sub    $0xc,%esp
f0104430:	68 6c 54 10 f0       	push   $0xf010546c
f0104435:	e8 57 ef ff ff       	call   f0103391 <cprintf>
f010443a:	8b 7d 94             	mov    -0x6c(%ebp),%edi
f010443d:	c1 e7 05             	shl    $0x5,%edi
f0104440:	83 c4 10             	add    $0x10,%esp
		for (j = 0; j < 32; ++j) {
f0104443:	bb 00 00 00 00       	mov    $0x0,%ebx
			const char *name = names[CPUID_BIT(i, j)];

			if ((feature[i] & BIT(j)) && name)
f0104448:	89 f0                	mov    %esi,%eax
f010444a:	88 d9                	mov    %bl,%cl
f010444c:	d3 e8                	shr    %cl,%eax
f010444e:	a8 01                	test   $0x1,%al
f0104450:	74 1f                	je     f0104471 <cpuid_print+0xfd>
	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
		if (!feature[i])
			continue;
		cprintf(" ");
		for (j = 0; j < 32; ++j) {
			const char *name = names[CPUID_BIT(i, j)];
f0104452:	8d 04 3b             	lea    (%ebx,%edi,1),%eax
f0104455:	8b 04 85 c0 6a 10 f0 	mov    -0xfef9540(,%eax,4),%eax

			if ((feature[i] & BIT(j)) && name)
f010445c:	85 c0                	test   %eax,%eax
f010445e:	74 11                	je     f0104471 <cpuid_print+0xfd>
				cprintf(" %s", name);
f0104460:	83 ec 08             	sub    $0x8,%esp
f0104463:	50                   	push   %eax
f0104464:	68 e9 50 10 f0       	push   $0xf01050e9
f0104469:	e8 23 ef ff ff       	call   f0103391 <cprintf>
f010446e:	83 c4 10             	add    $0x10,%esp

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
		if (!feature[i])
			continue;
		cprintf(" ");
		for (j = 0; j < 32; ++j) {
f0104471:	43                   	inc    %ebx
f0104472:	83 fb 20             	cmp    $0x20,%ebx
f0104475:	75 d1                	jne    f0104448 <cpuid_print+0xd4>
			const char *name = names[CPUID_BIT(i, j)];

			if ((feature[i] & BIT(j)) && name)
				cprintf(" %s", name);
		}
		cprintf("\n");
f0104477:	83 ec 0c             	sub    $0xc,%esp
f010447a:	68 dd 62 10 f0       	push   $0xf01062dd
f010447f:	e8 0d ef ff ff       	call   f0103391 <cprintf>
f0104484:	83 c4 10             	add    $0x10,%esp
static void
print_feature(uint32_t *feature)
{
	int i, j;

	for (i = 0; i < CPUID_NR_FLAGS; ++i) {
f0104487:	ff 45 94             	incl   -0x6c(%ebp)
f010448a:	8b 45 94             	mov    -0x6c(%ebp),%eax
f010448d:	83 f8 05             	cmp    $0x5,%eax
f0104490:	75 90                	jne    f0104422 <cpuid_print+0xae>
	      &feature[CPUID_1_ECX], &feature[CPUID_1_EDX]);
	cpuid(0x80000001, NULL, NULL,
	      &feature[CPUID_80000001_ECX], &feature[CPUID_80000001_EDX]);
	print_feature(feature);
	// Check feature bits.
	assert(cpuid_has(feature, CPUID_FEATURE_PSE));
f0104492:	f6 45 90 08          	testb  $0x8,-0x70(%ebp)
f0104496:	75 19                	jne    f01044b1 <cpuid_print+0x13d>
f0104498:	68 70 6a 10 f0       	push   $0xf0106a70
f010449d:	68 d8 50 10 f0       	push   $0xf01050d8
f01044a2:	68 9a 00 00 00       	push   $0x9a
f01044a7:	68 5f 68 10 f0       	push   $0xf010685f
f01044ac:	e8 9b bb ff ff       	call   f010004c <_panic>
	assert(cpuid_has(feature, CPUID_FEATURE_APIC));
f01044b1:	f7 45 90 00 02 00 00 	testl  $0x200,-0x70(%ebp)
f01044b8:	75 19                	jne    f01044d3 <cpuid_print+0x15f>
f01044ba:	68 98 6a 10 f0       	push   $0xf0106a98
f01044bf:	68 d8 50 10 f0       	push   $0xf01050d8
f01044c4:	68 9b 00 00 00       	push   $0x9b
f01044c9:	68 5f 68 10 f0       	push   $0xf010685f
f01044ce:	e8 79 bb ff ff       	call   f010004c <_panic>
}
f01044d3:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01044d6:	5b                   	pop    %ebx
f01044d7:	5e                   	pop    %esi
f01044d8:	5f                   	pop    %edi
f01044d9:	5d                   	pop    %ebp
f01044da:	c3                   	ret    

f01044db <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
f01044db:	55                   	push   %ebp
f01044dc:	89 e5                	mov    %esp,%ebp
f01044de:	57                   	push   %edi
f01044df:	56                   	push   %esi
f01044e0:	53                   	push   %ebx
f01044e1:	83 ec 1c             	sub    $0x1c,%esp
f01044e4:	89 c7                	mov    %eax,%edi
f01044e6:	89 d6                	mov    %edx,%esi
f01044e8:	8b 45 08             	mov    0x8(%ebp),%eax
f01044eb:	8b 55 0c             	mov    0xc(%ebp),%edx
f01044ee:	89 45 d8             	mov    %eax,-0x28(%ebp)
f01044f1:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
f01044f4:	8b 4d 10             	mov    0x10(%ebp),%ecx
f01044f7:	bb 00 00 00 00       	mov    $0x0,%ebx
f01044fc:	89 4d e0             	mov    %ecx,-0x20(%ebp)
f01044ff:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
f0104502:	39 d3                	cmp    %edx,%ebx
f0104504:	72 05                	jb     f010450b <printnum+0x30>
f0104506:	39 45 10             	cmp    %eax,0x10(%ebp)
f0104509:	77 45                	ja     f0104550 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
f010450b:	83 ec 0c             	sub    $0xc,%esp
f010450e:	ff 75 18             	pushl  0x18(%ebp)
f0104511:	8b 45 14             	mov    0x14(%ebp),%eax
f0104514:	8d 58 ff             	lea    -0x1(%eax),%ebx
f0104517:	53                   	push   %ebx
f0104518:	ff 75 10             	pushl  0x10(%ebp)
f010451b:	83 ec 08             	sub    $0x8,%esp
f010451e:	ff 75 e4             	pushl  -0x1c(%ebp)
f0104521:	ff 75 e0             	pushl  -0x20(%ebp)
f0104524:	ff 75 dc             	pushl  -0x24(%ebp)
f0104527:	ff 75 d8             	pushl  -0x28(%ebp)
f010452a:	e8 15 09 00 00       	call   f0104e44 <__udivdi3>
f010452f:	83 c4 18             	add    $0x18,%esp
f0104532:	52                   	push   %edx
f0104533:	50                   	push   %eax
f0104534:	89 f2                	mov    %esi,%edx
f0104536:	89 f8                	mov    %edi,%eax
f0104538:	e8 9e ff ff ff       	call   f01044db <printnum>
f010453d:	83 c4 20             	add    $0x20,%esp
f0104540:	eb 16                	jmp    f0104558 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
f0104542:	83 ec 08             	sub    $0x8,%esp
f0104545:	56                   	push   %esi
f0104546:	ff 75 18             	pushl  0x18(%ebp)
f0104549:	ff d7                	call   *%edi
f010454b:	83 c4 10             	add    $0x10,%esp
f010454e:	eb 03                	jmp    f0104553 <printnum+0x78>
f0104550:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
f0104553:	4b                   	dec    %ebx
f0104554:	85 db                	test   %ebx,%ebx
f0104556:	7f ea                	jg     f0104542 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
f0104558:	83 ec 08             	sub    $0x8,%esp
f010455b:	56                   	push   %esi
f010455c:	83 ec 04             	sub    $0x4,%esp
f010455f:	ff 75 e4             	pushl  -0x1c(%ebp)
f0104562:	ff 75 e0             	pushl  -0x20(%ebp)
f0104565:	ff 75 dc             	pushl  -0x24(%ebp)
f0104568:	ff 75 d8             	pushl  -0x28(%ebp)
f010456b:	e8 e4 09 00 00       	call   f0104f54 <__umoddi3>
f0104570:	83 c4 14             	add    $0x14,%esp
f0104573:	0f be 80 40 6d 10 f0 	movsbl -0xfef92c0(%eax),%eax
f010457a:	50                   	push   %eax
f010457b:	ff d7                	call   *%edi
}
f010457d:	83 c4 10             	add    $0x10,%esp
f0104580:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0104583:	5b                   	pop    %ebx
f0104584:	5e                   	pop    %esi
f0104585:	5f                   	pop    %edi
f0104586:	5d                   	pop    %ebp
f0104587:	c3                   	ret    

f0104588 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
f0104588:	55                   	push   %ebp
f0104589:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
f010458b:	83 fa 01             	cmp    $0x1,%edx
f010458e:	7e 0e                	jle    f010459e <getuint+0x16>
		return va_arg(*ap, unsigned long long);
f0104590:	8b 10                	mov    (%eax),%edx
f0104592:	8d 4a 08             	lea    0x8(%edx),%ecx
f0104595:	89 08                	mov    %ecx,(%eax)
f0104597:	8b 02                	mov    (%edx),%eax
f0104599:	8b 52 04             	mov    0x4(%edx),%edx
f010459c:	eb 22                	jmp    f01045c0 <getuint+0x38>
	else if (lflag)
f010459e:	85 d2                	test   %edx,%edx
f01045a0:	74 10                	je     f01045b2 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
f01045a2:	8b 10                	mov    (%eax),%edx
f01045a4:	8d 4a 04             	lea    0x4(%edx),%ecx
f01045a7:	89 08                	mov    %ecx,(%eax)
f01045a9:	8b 02                	mov    (%edx),%eax
f01045ab:	ba 00 00 00 00       	mov    $0x0,%edx
f01045b0:	eb 0e                	jmp    f01045c0 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
f01045b2:	8b 10                	mov    (%eax),%edx
f01045b4:	8d 4a 04             	lea    0x4(%edx),%ecx
f01045b7:	89 08                	mov    %ecx,(%eax)
f01045b9:	8b 02                	mov    (%edx),%eax
f01045bb:	ba 00 00 00 00       	mov    $0x0,%edx
}
f01045c0:	5d                   	pop    %ebp
f01045c1:	c3                   	ret    

f01045c2 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
f01045c2:	55                   	push   %ebp
f01045c3:	89 e5                	mov    %esp,%ebp
f01045c5:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
f01045c8:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
f01045cb:	8b 10                	mov    (%eax),%edx
f01045cd:	3b 50 04             	cmp    0x4(%eax),%edx
f01045d0:	73 0a                	jae    f01045dc <sprintputch+0x1a>
		*b->buf++ = ch;
f01045d2:	8d 4a 01             	lea    0x1(%edx),%ecx
f01045d5:	89 08                	mov    %ecx,(%eax)
f01045d7:	8b 45 08             	mov    0x8(%ebp),%eax
f01045da:	88 02                	mov    %al,(%edx)
}
f01045dc:	5d                   	pop    %ebp
f01045dd:	c3                   	ret    

f01045de <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
f01045de:	55                   	push   %ebp
f01045df:	89 e5                	mov    %esp,%ebp
f01045e1:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
f01045e4:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
f01045e7:	50                   	push   %eax
f01045e8:	ff 75 10             	pushl  0x10(%ebp)
f01045eb:	ff 75 0c             	pushl  0xc(%ebp)
f01045ee:	ff 75 08             	pushl  0x8(%ebp)
f01045f1:	e8 05 00 00 00       	call   f01045fb <vprintfmt>
	va_end(ap);
}
f01045f6:	83 c4 10             	add    $0x10,%esp
f01045f9:	c9                   	leave  
f01045fa:	c3                   	ret    

f01045fb <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
f01045fb:	55                   	push   %ebp
f01045fc:	89 e5                	mov    %esp,%ebp
f01045fe:	57                   	push   %edi
f01045ff:	56                   	push   %esi
f0104600:	53                   	push   %ebx
f0104601:	83 ec 2c             	sub    $0x2c,%esp
f0104604:	8b 75 08             	mov    0x8(%ebp),%esi
f0104607:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f010460a:	8b 7d 10             	mov    0x10(%ebp),%edi
f010460d:	eb 12                	jmp    f0104621 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
f010460f:	85 c0                	test   %eax,%eax
f0104611:	0f 84 68 03 00 00    	je     f010497f <vprintfmt+0x384>
				return;
			putch(ch, putdat);
f0104617:	83 ec 08             	sub    $0x8,%esp
f010461a:	53                   	push   %ebx
f010461b:	50                   	push   %eax
f010461c:	ff d6                	call   *%esi
f010461e:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
f0104621:	47                   	inc    %edi
f0104622:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
f0104626:	83 f8 25             	cmp    $0x25,%eax
f0104629:	75 e4                	jne    f010460f <vprintfmt+0x14>
f010462b:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
f010462f:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
f0104636:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
f010463d:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
f0104644:	ba 00 00 00 00       	mov    $0x0,%edx
f0104649:	eb 07                	jmp    f0104652 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010464b:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
f010464e:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0104652:	8d 47 01             	lea    0x1(%edi),%eax
f0104655:	89 45 e0             	mov    %eax,-0x20(%ebp)
f0104658:	0f b6 0f             	movzbl (%edi),%ecx
f010465b:	8a 07                	mov    (%edi),%al
f010465d:	83 e8 23             	sub    $0x23,%eax
f0104660:	3c 55                	cmp    $0x55,%al
f0104662:	0f 87 fe 02 00 00    	ja     f0104966 <vprintfmt+0x36b>
f0104668:	0f b6 c0             	movzbl %al,%eax
f010466b:	ff 24 85 cc 6d 10 f0 	jmp    *-0xfef9234(,%eax,4)
f0104672:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
f0104675:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
f0104679:	eb d7                	jmp    f0104652 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010467b:	8b 7d e0             	mov    -0x20(%ebp),%edi
f010467e:	b8 00 00 00 00       	mov    $0x0,%eax
f0104683:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
f0104686:	8d 04 80             	lea    (%eax,%eax,4),%eax
f0104689:	01 c0                	add    %eax,%eax
f010468b:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
f010468f:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
f0104692:	8d 51 d0             	lea    -0x30(%ecx),%edx
f0104695:	83 fa 09             	cmp    $0x9,%edx
f0104698:	77 34                	ja     f01046ce <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
f010469a:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
f010469b:	eb e9                	jmp    f0104686 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
f010469d:	8b 45 14             	mov    0x14(%ebp),%eax
f01046a0:	8d 48 04             	lea    0x4(%eax),%ecx
f01046a3:	89 4d 14             	mov    %ecx,0x14(%ebp)
f01046a6:	8b 00                	mov    (%eax),%eax
f01046a8:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f01046ab:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
f01046ae:	eb 24                	jmp    f01046d4 <vprintfmt+0xd9>
f01046b0:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f01046b4:	79 07                	jns    f01046bd <vprintfmt+0xc2>
f01046b6:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f01046bd:	8b 7d e0             	mov    -0x20(%ebp),%edi
f01046c0:	eb 90                	jmp    f0104652 <vprintfmt+0x57>
f01046c2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
f01046c5:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
f01046cc:	eb 84                	jmp    f0104652 <vprintfmt+0x57>
f01046ce:	8b 55 e0             	mov    -0x20(%ebp),%edx
f01046d1:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
f01046d4:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f01046d8:	0f 89 74 ff ff ff    	jns    f0104652 <vprintfmt+0x57>
				width = precision, precision = -1;
f01046de:	8b 45 d0             	mov    -0x30(%ebp),%eax
f01046e1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f01046e4:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
f01046eb:	e9 62 ff ff ff       	jmp    f0104652 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
f01046f0:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f01046f1:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
f01046f4:	e9 59 ff ff ff       	jmp    f0104652 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
f01046f9:	8b 45 14             	mov    0x14(%ebp),%eax
f01046fc:	8d 50 04             	lea    0x4(%eax),%edx
f01046ff:	89 55 14             	mov    %edx,0x14(%ebp)
f0104702:	83 ec 08             	sub    $0x8,%esp
f0104705:	53                   	push   %ebx
f0104706:	ff 30                	pushl  (%eax)
f0104708:	ff d6                	call   *%esi
			break;
f010470a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010470d:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
f0104710:	e9 0c ff ff ff       	jmp    f0104621 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
f0104715:	8b 45 14             	mov    0x14(%ebp),%eax
f0104718:	8d 50 04             	lea    0x4(%eax),%edx
f010471b:	89 55 14             	mov    %edx,0x14(%ebp)
f010471e:	8b 00                	mov    (%eax),%eax
f0104720:	85 c0                	test   %eax,%eax
f0104722:	79 02                	jns    f0104726 <vprintfmt+0x12b>
f0104724:	f7 d8                	neg    %eax
f0104726:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
f0104728:	83 f8 06             	cmp    $0x6,%eax
f010472b:	7f 0b                	jg     f0104738 <vprintfmt+0x13d>
f010472d:	8b 04 85 24 6f 10 f0 	mov    -0xfef90dc(,%eax,4),%eax
f0104734:	85 c0                	test   %eax,%eax
f0104736:	75 18                	jne    f0104750 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
f0104738:	52                   	push   %edx
f0104739:	68 58 6d 10 f0       	push   $0xf0106d58
f010473e:	53                   	push   %ebx
f010473f:	56                   	push   %esi
f0104740:	e8 99 fe ff ff       	call   f01045de <printfmt>
f0104745:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0104748:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
f010474b:	e9 d1 fe ff ff       	jmp    f0104621 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
f0104750:	50                   	push   %eax
f0104751:	68 ea 50 10 f0       	push   $0xf01050ea
f0104756:	53                   	push   %ebx
f0104757:	56                   	push   %esi
f0104758:	e8 81 fe ff ff       	call   f01045de <printfmt>
f010475d:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0104760:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0104763:	e9 b9 fe ff ff       	jmp    f0104621 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
f0104768:	8b 45 14             	mov    0x14(%ebp),%eax
f010476b:	8d 50 04             	lea    0x4(%eax),%edx
f010476e:	89 55 14             	mov    %edx,0x14(%ebp)
f0104771:	8b 38                	mov    (%eax),%edi
f0104773:	85 ff                	test   %edi,%edi
f0104775:	75 05                	jne    f010477c <vprintfmt+0x181>
				p = "(null)";
f0104777:	bf 51 6d 10 f0       	mov    $0xf0106d51,%edi
			if (width > 0 && padc != '-')
f010477c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f0104780:	0f 8e 90 00 00 00    	jle    f0104816 <vprintfmt+0x21b>
f0104786:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
f010478a:	0f 84 8e 00 00 00    	je     f010481e <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
f0104790:	83 ec 08             	sub    $0x8,%esp
f0104793:	ff 75 d0             	pushl  -0x30(%ebp)
f0104796:	57                   	push   %edi
f0104797:	e8 60 03 00 00       	call   f0104afc <strnlen>
f010479c:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
f010479f:	29 c1                	sub    %eax,%ecx
f01047a1:	89 4d cc             	mov    %ecx,-0x34(%ebp)
f01047a4:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
f01047a7:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
f01047ab:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f01047ae:	89 7d d4             	mov    %edi,-0x2c(%ebp)
f01047b1:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f01047b3:	eb 0d                	jmp    f01047c2 <vprintfmt+0x1c7>
					putch(padc, putdat);
f01047b5:	83 ec 08             	sub    $0x8,%esp
f01047b8:	53                   	push   %ebx
f01047b9:	ff 75 e4             	pushl  -0x1c(%ebp)
f01047bc:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f01047be:	4f                   	dec    %edi
f01047bf:	83 c4 10             	add    $0x10,%esp
f01047c2:	85 ff                	test   %edi,%edi
f01047c4:	7f ef                	jg     f01047b5 <vprintfmt+0x1ba>
f01047c6:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f01047c9:	8b 4d cc             	mov    -0x34(%ebp),%ecx
f01047cc:	89 c8                	mov    %ecx,%eax
f01047ce:	85 c9                	test   %ecx,%ecx
f01047d0:	79 05                	jns    f01047d7 <vprintfmt+0x1dc>
f01047d2:	b8 00 00 00 00       	mov    $0x0,%eax
f01047d7:	8b 4d cc             	mov    -0x34(%ebp),%ecx
f01047da:	29 c1                	sub    %eax,%ecx
f01047dc:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
f01047df:	89 75 08             	mov    %esi,0x8(%ebp)
f01047e2:	8b 75 d0             	mov    -0x30(%ebp),%esi
f01047e5:	eb 3d                	jmp    f0104824 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
f01047e7:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
f01047eb:	74 19                	je     f0104806 <vprintfmt+0x20b>
f01047ed:	0f be c0             	movsbl %al,%eax
f01047f0:	83 e8 20             	sub    $0x20,%eax
f01047f3:	83 f8 5e             	cmp    $0x5e,%eax
f01047f6:	76 0e                	jbe    f0104806 <vprintfmt+0x20b>
					putch('?', putdat);
f01047f8:	83 ec 08             	sub    $0x8,%esp
f01047fb:	53                   	push   %ebx
f01047fc:	6a 3f                	push   $0x3f
f01047fe:	ff 55 08             	call   *0x8(%ebp)
f0104801:	83 c4 10             	add    $0x10,%esp
f0104804:	eb 0b                	jmp    f0104811 <vprintfmt+0x216>
				else
					putch(ch, putdat);
f0104806:	83 ec 08             	sub    $0x8,%esp
f0104809:	53                   	push   %ebx
f010480a:	52                   	push   %edx
f010480b:	ff 55 08             	call   *0x8(%ebp)
f010480e:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
f0104811:	ff 4d e4             	decl   -0x1c(%ebp)
f0104814:	eb 0e                	jmp    f0104824 <vprintfmt+0x229>
f0104816:	89 75 08             	mov    %esi,0x8(%ebp)
f0104819:	8b 75 d0             	mov    -0x30(%ebp),%esi
f010481c:	eb 06                	jmp    f0104824 <vprintfmt+0x229>
f010481e:	89 75 08             	mov    %esi,0x8(%ebp)
f0104821:	8b 75 d0             	mov    -0x30(%ebp),%esi
f0104824:	47                   	inc    %edi
f0104825:	8a 47 ff             	mov    -0x1(%edi),%al
f0104828:	0f be d0             	movsbl %al,%edx
f010482b:	85 d2                	test   %edx,%edx
f010482d:	74 1d                	je     f010484c <vprintfmt+0x251>
f010482f:	85 f6                	test   %esi,%esi
f0104831:	78 b4                	js     f01047e7 <vprintfmt+0x1ec>
f0104833:	4e                   	dec    %esi
f0104834:	79 b1                	jns    f01047e7 <vprintfmt+0x1ec>
f0104836:	8b 75 08             	mov    0x8(%ebp),%esi
f0104839:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f010483c:	eb 14                	jmp    f0104852 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
f010483e:	83 ec 08             	sub    $0x8,%esp
f0104841:	53                   	push   %ebx
f0104842:	6a 20                	push   $0x20
f0104844:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
f0104846:	4f                   	dec    %edi
f0104847:	83 c4 10             	add    $0x10,%esp
f010484a:	eb 06                	jmp    f0104852 <vprintfmt+0x257>
f010484c:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f010484f:	8b 75 08             	mov    0x8(%ebp),%esi
f0104852:	85 ff                	test   %edi,%edi
f0104854:	7f e8                	jg     f010483e <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0104856:	8b 7d e0             	mov    -0x20(%ebp),%edi
f0104859:	e9 c3 fd ff ff       	jmp    f0104621 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
f010485e:	83 fa 01             	cmp    $0x1,%edx
f0104861:	7e 16                	jle    f0104879 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
f0104863:	8b 45 14             	mov    0x14(%ebp),%eax
f0104866:	8d 50 08             	lea    0x8(%eax),%edx
f0104869:	89 55 14             	mov    %edx,0x14(%ebp)
f010486c:	8b 50 04             	mov    0x4(%eax),%edx
f010486f:	8b 00                	mov    (%eax),%eax
f0104871:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0104874:	89 55 dc             	mov    %edx,-0x24(%ebp)
f0104877:	eb 32                	jmp    f01048ab <vprintfmt+0x2b0>
	else if (lflag)
f0104879:	85 d2                	test   %edx,%edx
f010487b:	74 18                	je     f0104895 <vprintfmt+0x29a>
		return va_arg(*ap, long);
f010487d:	8b 45 14             	mov    0x14(%ebp),%eax
f0104880:	8d 50 04             	lea    0x4(%eax),%edx
f0104883:	89 55 14             	mov    %edx,0x14(%ebp)
f0104886:	8b 00                	mov    (%eax),%eax
f0104888:	89 45 d8             	mov    %eax,-0x28(%ebp)
f010488b:	89 c1                	mov    %eax,%ecx
f010488d:	c1 f9 1f             	sar    $0x1f,%ecx
f0104890:	89 4d dc             	mov    %ecx,-0x24(%ebp)
f0104893:	eb 16                	jmp    f01048ab <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
f0104895:	8b 45 14             	mov    0x14(%ebp),%eax
f0104898:	8d 50 04             	lea    0x4(%eax),%edx
f010489b:	89 55 14             	mov    %edx,0x14(%ebp)
f010489e:	8b 00                	mov    (%eax),%eax
f01048a0:	89 45 d8             	mov    %eax,-0x28(%ebp)
f01048a3:	89 c1                	mov    %eax,%ecx
f01048a5:	c1 f9 1f             	sar    $0x1f,%ecx
f01048a8:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
f01048ab:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01048ae:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
f01048b1:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
f01048b5:	79 76                	jns    f010492d <vprintfmt+0x332>
				putch('-', putdat);
f01048b7:	83 ec 08             	sub    $0x8,%esp
f01048ba:	53                   	push   %ebx
f01048bb:	6a 2d                	push   $0x2d
f01048bd:	ff d6                	call   *%esi
				num = -(long long) num;
f01048bf:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01048c2:	8b 55 dc             	mov    -0x24(%ebp),%edx
f01048c5:	f7 d8                	neg    %eax
f01048c7:	83 d2 00             	adc    $0x0,%edx
f01048ca:	f7 da                	neg    %edx
f01048cc:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
f01048cf:	b9 0a 00 00 00       	mov    $0xa,%ecx
f01048d4:	eb 5c                	jmp    f0104932 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
f01048d6:	8d 45 14             	lea    0x14(%ebp),%eax
f01048d9:	e8 aa fc ff ff       	call   f0104588 <getuint>
			base = 10;
f01048de:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
f01048e3:	eb 4d                	jmp    f0104932 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
f01048e5:	8d 45 14             	lea    0x14(%ebp),%eax
f01048e8:	e8 9b fc ff ff       	call   f0104588 <getuint>
			base = 8;
f01048ed:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
f01048f2:	eb 3e                	jmp    f0104932 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
f01048f4:	83 ec 08             	sub    $0x8,%esp
f01048f7:	53                   	push   %ebx
f01048f8:	6a 30                	push   $0x30
f01048fa:	ff d6                	call   *%esi
			putch('x', putdat);
f01048fc:	83 c4 08             	add    $0x8,%esp
f01048ff:	53                   	push   %ebx
f0104900:	6a 78                	push   $0x78
f0104902:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
f0104904:	8b 45 14             	mov    0x14(%ebp),%eax
f0104907:	8d 50 04             	lea    0x4(%eax),%edx
f010490a:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
f010490d:	8b 00                	mov    (%eax),%eax
f010490f:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
f0104914:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
f0104917:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
f010491c:	eb 14                	jmp    f0104932 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
f010491e:	8d 45 14             	lea    0x14(%ebp),%eax
f0104921:	e8 62 fc ff ff       	call   f0104588 <getuint>
			base = 16;
f0104926:	b9 10 00 00 00       	mov    $0x10,%ecx
f010492b:	eb 05                	jmp    f0104932 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
f010492d:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
f0104932:	83 ec 0c             	sub    $0xc,%esp
f0104935:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
f0104939:	57                   	push   %edi
f010493a:	ff 75 e4             	pushl  -0x1c(%ebp)
f010493d:	51                   	push   %ecx
f010493e:	52                   	push   %edx
f010493f:	50                   	push   %eax
f0104940:	89 da                	mov    %ebx,%edx
f0104942:	89 f0                	mov    %esi,%eax
f0104944:	e8 92 fb ff ff       	call   f01044db <printnum>
			break;
f0104949:	83 c4 20             	add    $0x20,%esp
f010494c:	8b 7d e0             	mov    -0x20(%ebp),%edi
f010494f:	e9 cd fc ff ff       	jmp    f0104621 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
f0104954:	83 ec 08             	sub    $0x8,%esp
f0104957:	53                   	push   %ebx
f0104958:	51                   	push   %ecx
f0104959:	ff d6                	call   *%esi
			break;
f010495b:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010495e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
f0104961:	e9 bb fc ff ff       	jmp    f0104621 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
f0104966:	83 ec 08             	sub    $0x8,%esp
f0104969:	53                   	push   %ebx
f010496a:	6a 25                	push   $0x25
f010496c:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
f010496e:	83 c4 10             	add    $0x10,%esp
f0104971:	eb 01                	jmp    f0104974 <vprintfmt+0x379>
f0104973:	4f                   	dec    %edi
f0104974:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
f0104978:	75 f9                	jne    f0104973 <vprintfmt+0x378>
f010497a:	e9 a2 fc ff ff       	jmp    f0104621 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
f010497f:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0104982:	5b                   	pop    %ebx
f0104983:	5e                   	pop    %esi
f0104984:	5f                   	pop    %edi
f0104985:	5d                   	pop    %ebp
f0104986:	c3                   	ret    

f0104987 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
f0104987:	55                   	push   %ebp
f0104988:	89 e5                	mov    %esp,%ebp
f010498a:	83 ec 18             	sub    $0x18,%esp
f010498d:	8b 45 08             	mov    0x8(%ebp),%eax
f0104990:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
f0104993:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0104996:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
f010499a:	89 4d f0             	mov    %ecx,-0x10(%ebp)
f010499d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
f01049a4:	85 c0                	test   %eax,%eax
f01049a6:	74 26                	je     f01049ce <vsnprintf+0x47>
f01049a8:	85 d2                	test   %edx,%edx
f01049aa:	7e 29                	jle    f01049d5 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
f01049ac:	ff 75 14             	pushl  0x14(%ebp)
f01049af:	ff 75 10             	pushl  0x10(%ebp)
f01049b2:	8d 45 ec             	lea    -0x14(%ebp),%eax
f01049b5:	50                   	push   %eax
f01049b6:	68 c2 45 10 f0       	push   $0xf01045c2
f01049bb:	e8 3b fc ff ff       	call   f01045fb <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
f01049c0:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01049c3:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
f01049c6:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01049c9:	83 c4 10             	add    $0x10,%esp
f01049cc:	eb 0c                	jmp    f01049da <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
f01049ce:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
f01049d3:	eb 05                	jmp    f01049da <vsnprintf+0x53>
f01049d5:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
f01049da:	c9                   	leave  
f01049db:	c3                   	ret    

f01049dc <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
f01049dc:	55                   	push   %ebp
f01049dd:	89 e5                	mov    %esp,%ebp
f01049df:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
f01049e2:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
f01049e5:	50                   	push   %eax
f01049e6:	ff 75 10             	pushl  0x10(%ebp)
f01049e9:	ff 75 0c             	pushl  0xc(%ebp)
f01049ec:	ff 75 08             	pushl  0x8(%ebp)
f01049ef:	e8 93 ff ff ff       	call   f0104987 <vsnprintf>
	va_end(ap);

	return rc;
}
f01049f4:	c9                   	leave  
f01049f5:	c3                   	ret    

f01049f6 <readline>:
#define BUFLEN 1024
static char buf[BUFLEN];

char *
readline(const char *prompt)
{
f01049f6:	55                   	push   %ebp
f01049f7:	89 e5                	mov    %esp,%ebp
f01049f9:	57                   	push   %edi
f01049fa:	56                   	push   %esi
f01049fb:	53                   	push   %ebx
f01049fc:	83 ec 0c             	sub    $0xc,%esp
f01049ff:	8b 45 08             	mov    0x8(%ebp),%eax
	int i, c, echoing;

	if (prompt != NULL)
f0104a02:	85 c0                	test   %eax,%eax
f0104a04:	74 11                	je     f0104a17 <readline+0x21>
		cprintf("%s", prompt);
f0104a06:	83 ec 08             	sub    $0x8,%esp
f0104a09:	50                   	push   %eax
f0104a0a:	68 ea 50 10 f0       	push   $0xf01050ea
f0104a0f:	e8 7d e9 ff ff       	call   f0103391 <cprintf>
f0104a14:	83 c4 10             	add    $0x10,%esp

	i = 0;
	echoing = iscons(0);
f0104a17:	83 ec 0c             	sub    $0xc,%esp
f0104a1a:	6a 00                	push   $0x0
f0104a1c:	e8 16 bc ff ff       	call   f0100637 <iscons>
f0104a21:	89 c7                	mov    %eax,%edi
f0104a23:	83 c4 10             	add    $0x10,%esp
	int i, c, echoing;

	if (prompt != NULL)
		cprintf("%s", prompt);

	i = 0;
f0104a26:	be 00 00 00 00       	mov    $0x0,%esi
	echoing = iscons(0);
	while (1) {
		c = getchar();
f0104a2b:	e8 f6 bb ff ff       	call   f0100626 <getchar>
f0104a30:	89 c3                	mov    %eax,%ebx
		if (c < 0) {
f0104a32:	85 c0                	test   %eax,%eax
f0104a34:	79 1b                	jns    f0104a51 <readline+0x5b>
			cprintf("read error: %e\n", c);
f0104a36:	83 ec 08             	sub    $0x8,%esp
f0104a39:	50                   	push   %eax
f0104a3a:	68 40 6f 10 f0       	push   $0xf0106f40
f0104a3f:	e8 4d e9 ff ff       	call   f0103391 <cprintf>
			return NULL;
f0104a44:	83 c4 10             	add    $0x10,%esp
f0104a47:	b8 00 00 00 00       	mov    $0x0,%eax
f0104a4c:	e9 8d 00 00 00       	jmp    f0104ade <readline+0xe8>
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
f0104a51:	83 f8 08             	cmp    $0x8,%eax
f0104a54:	74 72                	je     f0104ac8 <readline+0xd2>
f0104a56:	83 f8 7f             	cmp    $0x7f,%eax
f0104a59:	75 16                	jne    f0104a71 <readline+0x7b>
f0104a5b:	eb 65                	jmp    f0104ac2 <readline+0xcc>
			if (echoing)
f0104a5d:	85 ff                	test   %edi,%edi
f0104a5f:	74 0d                	je     f0104a6e <readline+0x78>
				cputchar('\b');
f0104a61:	83 ec 0c             	sub    $0xc,%esp
f0104a64:	6a 08                	push   $0x8
f0104a66:	e8 ab bb ff ff       	call   f0100616 <cputchar>
f0104a6b:	83 c4 10             	add    $0x10,%esp
			i--;
f0104a6e:	4e                   	dec    %esi
f0104a6f:	eb ba                	jmp    f0104a2b <readline+0x35>
		} else if (c >= ' ' && i < BUFLEN-1) {
f0104a71:	83 f8 1f             	cmp    $0x1f,%eax
f0104a74:	7e 23                	jle    f0104a99 <readline+0xa3>
f0104a76:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
f0104a7c:	7f 1b                	jg     f0104a99 <readline+0xa3>
			if (echoing)
f0104a7e:	85 ff                	test   %edi,%edi
f0104a80:	74 0c                	je     f0104a8e <readline+0x98>
				cputchar(c);
f0104a82:	83 ec 0c             	sub    $0xc,%esp
f0104a85:	53                   	push   %ebx
f0104a86:	e8 8b bb ff ff       	call   f0100616 <cputchar>
f0104a8b:	83 c4 10             	add    $0x10,%esp
			buf[i++] = c;
f0104a8e:	88 9e 20 8b 18 f0    	mov    %bl,-0xfe774e0(%esi)
f0104a94:	8d 76 01             	lea    0x1(%esi),%esi
f0104a97:	eb 92                	jmp    f0104a2b <readline+0x35>
		} else if (c == '\n' || c == '\r') {
f0104a99:	83 fb 0a             	cmp    $0xa,%ebx
f0104a9c:	74 05                	je     f0104aa3 <readline+0xad>
f0104a9e:	83 fb 0d             	cmp    $0xd,%ebx
f0104aa1:	75 88                	jne    f0104a2b <readline+0x35>
			if (echoing)
f0104aa3:	85 ff                	test   %edi,%edi
f0104aa5:	74 0d                	je     f0104ab4 <readline+0xbe>
				cputchar('\n');
f0104aa7:	83 ec 0c             	sub    $0xc,%esp
f0104aaa:	6a 0a                	push   $0xa
f0104aac:	e8 65 bb ff ff       	call   f0100616 <cputchar>
f0104ab1:	83 c4 10             	add    $0x10,%esp
			buf[i] = 0;
f0104ab4:	c6 86 20 8b 18 f0 00 	movb   $0x0,-0xfe774e0(%esi)
			return buf;
f0104abb:	b8 20 8b 18 f0       	mov    $0xf0188b20,%eax
f0104ac0:	eb 1c                	jmp    f0104ade <readline+0xe8>
	while (1) {
		c = getchar();
		if (c < 0) {
			cprintf("read error: %e\n", c);
			return NULL;
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
f0104ac2:	85 f6                	test   %esi,%esi
f0104ac4:	7f 97                	jg     f0104a5d <readline+0x67>
f0104ac6:	eb 09                	jmp    f0104ad1 <readline+0xdb>
f0104ac8:	85 f6                	test   %esi,%esi
f0104aca:	7f 91                	jg     f0104a5d <readline+0x67>
f0104acc:	e9 5a ff ff ff       	jmp    f0104a2b <readline+0x35>
			if (echoing)
				cputchar('\b');
			i--;
		} else if (c >= ' ' && i < BUFLEN-1) {
f0104ad1:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
f0104ad7:	7e a5                	jle    f0104a7e <readline+0x88>
f0104ad9:	e9 4d ff ff ff       	jmp    f0104a2b <readline+0x35>
				cputchar('\n');
			buf[i] = 0;
			return buf;
		}
	}
}
f0104ade:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0104ae1:	5b                   	pop    %ebx
f0104ae2:	5e                   	pop    %esi
f0104ae3:	5f                   	pop    %edi
f0104ae4:	5d                   	pop    %ebp
f0104ae5:	c3                   	ret    

f0104ae6 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
f0104ae6:	55                   	push   %ebp
f0104ae7:	89 e5                	mov    %esp,%ebp
f0104ae9:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
f0104aec:	b8 00 00 00 00       	mov    $0x0,%eax
f0104af1:	eb 01                	jmp    f0104af4 <strlen+0xe>
		n++;
f0104af3:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
f0104af4:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
f0104af8:	75 f9                	jne    f0104af3 <strlen+0xd>
		n++;
	return n;
}
f0104afa:	5d                   	pop    %ebp
f0104afb:	c3                   	ret    

f0104afc <strnlen>:

int
strnlen(const char *s, size_t size)
{
f0104afc:	55                   	push   %ebp
f0104afd:	89 e5                	mov    %esp,%ebp
f0104aff:	8b 4d 08             	mov    0x8(%ebp),%ecx
f0104b02:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f0104b05:	ba 00 00 00 00       	mov    $0x0,%edx
f0104b0a:	eb 01                	jmp    f0104b0d <strnlen+0x11>
		n++;
f0104b0c:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f0104b0d:	39 c2                	cmp    %eax,%edx
f0104b0f:	74 08                	je     f0104b19 <strnlen+0x1d>
f0104b11:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
f0104b15:	75 f5                	jne    f0104b0c <strnlen+0x10>
f0104b17:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
f0104b19:	5d                   	pop    %ebp
f0104b1a:	c3                   	ret    

f0104b1b <strcpy>:

char *
strcpy(char *dst, const char *src)
{
f0104b1b:	55                   	push   %ebp
f0104b1c:	89 e5                	mov    %esp,%ebp
f0104b1e:	53                   	push   %ebx
f0104b1f:	8b 45 08             	mov    0x8(%ebp),%eax
f0104b22:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
f0104b25:	89 c2                	mov    %eax,%edx
f0104b27:	42                   	inc    %edx
f0104b28:	41                   	inc    %ecx
f0104b29:	8a 59 ff             	mov    -0x1(%ecx),%bl
f0104b2c:	88 5a ff             	mov    %bl,-0x1(%edx)
f0104b2f:	84 db                	test   %bl,%bl
f0104b31:	75 f4                	jne    f0104b27 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
f0104b33:	5b                   	pop    %ebx
f0104b34:	5d                   	pop    %ebp
f0104b35:	c3                   	ret    

f0104b36 <strcat>:

char *
strcat(char *dst, const char *src)
{
f0104b36:	55                   	push   %ebp
f0104b37:	89 e5                	mov    %esp,%ebp
f0104b39:	53                   	push   %ebx
f0104b3a:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
f0104b3d:	53                   	push   %ebx
f0104b3e:	e8 a3 ff ff ff       	call   f0104ae6 <strlen>
f0104b43:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
f0104b46:	ff 75 0c             	pushl  0xc(%ebp)
f0104b49:	01 d8                	add    %ebx,%eax
f0104b4b:	50                   	push   %eax
f0104b4c:	e8 ca ff ff ff       	call   f0104b1b <strcpy>
	return dst;
}
f0104b51:	89 d8                	mov    %ebx,%eax
f0104b53:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0104b56:	c9                   	leave  
f0104b57:	c3                   	ret    

f0104b58 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
f0104b58:	55                   	push   %ebp
f0104b59:	89 e5                	mov    %esp,%ebp
f0104b5b:	56                   	push   %esi
f0104b5c:	53                   	push   %ebx
f0104b5d:	8b 75 08             	mov    0x8(%ebp),%esi
f0104b60:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0104b63:	89 f3                	mov    %esi,%ebx
f0104b65:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f0104b68:	89 f2                	mov    %esi,%edx
f0104b6a:	eb 0c                	jmp    f0104b78 <strncpy+0x20>
		*dst++ = *src;
f0104b6c:	42                   	inc    %edx
f0104b6d:	8a 01                	mov    (%ecx),%al
f0104b6f:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
f0104b72:	80 39 01             	cmpb   $0x1,(%ecx)
f0104b75:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f0104b78:	39 da                	cmp    %ebx,%edx
f0104b7a:	75 f0                	jne    f0104b6c <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
f0104b7c:	89 f0                	mov    %esi,%eax
f0104b7e:	5b                   	pop    %ebx
f0104b7f:	5e                   	pop    %esi
f0104b80:	5d                   	pop    %ebp
f0104b81:	c3                   	ret    

f0104b82 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
f0104b82:	55                   	push   %ebp
f0104b83:	89 e5                	mov    %esp,%ebp
f0104b85:	56                   	push   %esi
f0104b86:	53                   	push   %ebx
f0104b87:	8b 75 08             	mov    0x8(%ebp),%esi
f0104b8a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0104b8d:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
f0104b90:	85 c0                	test   %eax,%eax
f0104b92:	74 1e                	je     f0104bb2 <strlcpy+0x30>
f0104b94:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
f0104b98:	89 f2                	mov    %esi,%edx
f0104b9a:	eb 05                	jmp    f0104ba1 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
f0104b9c:	42                   	inc    %edx
f0104b9d:	41                   	inc    %ecx
f0104b9e:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
f0104ba1:	39 c2                	cmp    %eax,%edx
f0104ba3:	74 08                	je     f0104bad <strlcpy+0x2b>
f0104ba5:	8a 19                	mov    (%ecx),%bl
f0104ba7:	84 db                	test   %bl,%bl
f0104ba9:	75 f1                	jne    f0104b9c <strlcpy+0x1a>
f0104bab:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
f0104bad:	c6 00 00             	movb   $0x0,(%eax)
f0104bb0:	eb 02                	jmp    f0104bb4 <strlcpy+0x32>
f0104bb2:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
f0104bb4:	29 f0                	sub    %esi,%eax
}
f0104bb6:	5b                   	pop    %ebx
f0104bb7:	5e                   	pop    %esi
f0104bb8:	5d                   	pop    %ebp
f0104bb9:	c3                   	ret    

f0104bba <strcmp>:

int
strcmp(const char *p, const char *q)
{
f0104bba:	55                   	push   %ebp
f0104bbb:	89 e5                	mov    %esp,%ebp
f0104bbd:	8b 4d 08             	mov    0x8(%ebp),%ecx
f0104bc0:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
f0104bc3:	eb 02                	jmp    f0104bc7 <strcmp+0xd>
		p++, q++;
f0104bc5:	41                   	inc    %ecx
f0104bc6:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
f0104bc7:	8a 01                	mov    (%ecx),%al
f0104bc9:	84 c0                	test   %al,%al
f0104bcb:	74 04                	je     f0104bd1 <strcmp+0x17>
f0104bcd:	3a 02                	cmp    (%edx),%al
f0104bcf:	74 f4                	je     f0104bc5 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
f0104bd1:	0f b6 c0             	movzbl %al,%eax
f0104bd4:	0f b6 12             	movzbl (%edx),%edx
f0104bd7:	29 d0                	sub    %edx,%eax
}
f0104bd9:	5d                   	pop    %ebp
f0104bda:	c3                   	ret    

f0104bdb <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
f0104bdb:	55                   	push   %ebp
f0104bdc:	89 e5                	mov    %esp,%ebp
f0104bde:	53                   	push   %ebx
f0104bdf:	8b 45 08             	mov    0x8(%ebp),%eax
f0104be2:	8b 55 0c             	mov    0xc(%ebp),%edx
f0104be5:	89 c3                	mov    %eax,%ebx
f0104be7:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
f0104bea:	eb 02                	jmp    f0104bee <strncmp+0x13>
		n--, p++, q++;
f0104bec:	40                   	inc    %eax
f0104bed:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
f0104bee:	39 d8                	cmp    %ebx,%eax
f0104bf0:	74 14                	je     f0104c06 <strncmp+0x2b>
f0104bf2:	8a 08                	mov    (%eax),%cl
f0104bf4:	84 c9                	test   %cl,%cl
f0104bf6:	74 04                	je     f0104bfc <strncmp+0x21>
f0104bf8:	3a 0a                	cmp    (%edx),%cl
f0104bfa:	74 f0                	je     f0104bec <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
f0104bfc:	0f b6 00             	movzbl (%eax),%eax
f0104bff:	0f b6 12             	movzbl (%edx),%edx
f0104c02:	29 d0                	sub    %edx,%eax
f0104c04:	eb 05                	jmp    f0104c0b <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
f0104c06:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
f0104c0b:	5b                   	pop    %ebx
f0104c0c:	5d                   	pop    %ebp
f0104c0d:	c3                   	ret    

f0104c0e <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
f0104c0e:	55                   	push   %ebp
f0104c0f:	89 e5                	mov    %esp,%ebp
f0104c11:	8b 45 08             	mov    0x8(%ebp),%eax
f0104c14:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
f0104c17:	eb 05                	jmp    f0104c1e <strchr+0x10>
		if (*s == c)
f0104c19:	38 ca                	cmp    %cl,%dl
f0104c1b:	74 0c                	je     f0104c29 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
f0104c1d:	40                   	inc    %eax
f0104c1e:	8a 10                	mov    (%eax),%dl
f0104c20:	84 d2                	test   %dl,%dl
f0104c22:	75 f5                	jne    f0104c19 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
f0104c24:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0104c29:	5d                   	pop    %ebp
f0104c2a:	c3                   	ret    

f0104c2b <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
f0104c2b:	55                   	push   %ebp
f0104c2c:	89 e5                	mov    %esp,%ebp
f0104c2e:	8b 45 08             	mov    0x8(%ebp),%eax
f0104c31:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
f0104c34:	eb 05                	jmp    f0104c3b <strfind+0x10>
		if (*s == c)
f0104c36:	38 ca                	cmp    %cl,%dl
f0104c38:	74 07                	je     f0104c41 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
f0104c3a:	40                   	inc    %eax
f0104c3b:	8a 10                	mov    (%eax),%dl
f0104c3d:	84 d2                	test   %dl,%dl
f0104c3f:	75 f5                	jne    f0104c36 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
f0104c41:	5d                   	pop    %ebp
f0104c42:	c3                   	ret    

f0104c43 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
f0104c43:	55                   	push   %ebp
f0104c44:	89 e5                	mov    %esp,%ebp
f0104c46:	57                   	push   %edi
f0104c47:	56                   	push   %esi
f0104c48:	53                   	push   %ebx
f0104c49:	8b 7d 08             	mov    0x8(%ebp),%edi
f0104c4c:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
f0104c4f:	85 c9                	test   %ecx,%ecx
f0104c51:	74 36                	je     f0104c89 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
f0104c53:	f7 c7 03 00 00 00    	test   $0x3,%edi
f0104c59:	75 28                	jne    f0104c83 <memset+0x40>
f0104c5b:	f6 c1 03             	test   $0x3,%cl
f0104c5e:	75 23                	jne    f0104c83 <memset+0x40>
		c &= 0xFF;
f0104c60:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
f0104c64:	89 d3                	mov    %edx,%ebx
f0104c66:	c1 e3 08             	shl    $0x8,%ebx
f0104c69:	89 d6                	mov    %edx,%esi
f0104c6b:	c1 e6 18             	shl    $0x18,%esi
f0104c6e:	89 d0                	mov    %edx,%eax
f0104c70:	c1 e0 10             	shl    $0x10,%eax
f0104c73:	09 f0                	or     %esi,%eax
f0104c75:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
f0104c77:	89 d8                	mov    %ebx,%eax
f0104c79:	09 d0                	or     %edx,%eax
f0104c7b:	c1 e9 02             	shr    $0x2,%ecx
f0104c7e:	fc                   	cld    
f0104c7f:	f3 ab                	rep stos %eax,%es:(%edi)
f0104c81:	eb 06                	jmp    f0104c89 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
f0104c83:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104c86:	fc                   	cld    
f0104c87:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
f0104c89:	89 f8                	mov    %edi,%eax
f0104c8b:	5b                   	pop    %ebx
f0104c8c:	5e                   	pop    %esi
f0104c8d:	5f                   	pop    %edi
f0104c8e:	5d                   	pop    %ebp
f0104c8f:	c3                   	ret    

f0104c90 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
f0104c90:	55                   	push   %ebp
f0104c91:	89 e5                	mov    %esp,%ebp
f0104c93:	57                   	push   %edi
f0104c94:	56                   	push   %esi
f0104c95:	8b 45 08             	mov    0x8(%ebp),%eax
f0104c98:	8b 75 0c             	mov    0xc(%ebp),%esi
f0104c9b:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
f0104c9e:	39 c6                	cmp    %eax,%esi
f0104ca0:	73 33                	jae    f0104cd5 <memmove+0x45>
f0104ca2:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
f0104ca5:	39 d0                	cmp    %edx,%eax
f0104ca7:	73 2c                	jae    f0104cd5 <memmove+0x45>
		s += n;
		d += n;
f0104ca9:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
f0104cac:	89 d6                	mov    %edx,%esi
f0104cae:	09 fe                	or     %edi,%esi
f0104cb0:	f7 c6 03 00 00 00    	test   $0x3,%esi
f0104cb6:	75 13                	jne    f0104ccb <memmove+0x3b>
f0104cb8:	f6 c1 03             	test   $0x3,%cl
f0104cbb:	75 0e                	jne    f0104ccb <memmove+0x3b>
			asm volatile("std; rep movsl\n"
f0104cbd:	83 ef 04             	sub    $0x4,%edi
f0104cc0:	8d 72 fc             	lea    -0x4(%edx),%esi
f0104cc3:	c1 e9 02             	shr    $0x2,%ecx
f0104cc6:	fd                   	std    
f0104cc7:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f0104cc9:	eb 07                	jmp    f0104cd2 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
f0104ccb:	4f                   	dec    %edi
f0104ccc:	8d 72 ff             	lea    -0x1(%edx),%esi
f0104ccf:	fd                   	std    
f0104cd0:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
f0104cd2:	fc                   	cld    
f0104cd3:	eb 1d                	jmp    f0104cf2 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
f0104cd5:	89 f2                	mov    %esi,%edx
f0104cd7:	09 c2                	or     %eax,%edx
f0104cd9:	f6 c2 03             	test   $0x3,%dl
f0104cdc:	75 0f                	jne    f0104ced <memmove+0x5d>
f0104cde:	f6 c1 03             	test   $0x3,%cl
f0104ce1:	75 0a                	jne    f0104ced <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
f0104ce3:	c1 e9 02             	shr    $0x2,%ecx
f0104ce6:	89 c7                	mov    %eax,%edi
f0104ce8:	fc                   	cld    
f0104ce9:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f0104ceb:	eb 05                	jmp    f0104cf2 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
f0104ced:	89 c7                	mov    %eax,%edi
f0104cef:	fc                   	cld    
f0104cf0:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
f0104cf2:	5e                   	pop    %esi
f0104cf3:	5f                   	pop    %edi
f0104cf4:	5d                   	pop    %ebp
f0104cf5:	c3                   	ret    

f0104cf6 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
f0104cf6:	55                   	push   %ebp
f0104cf7:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
f0104cf9:	ff 75 10             	pushl  0x10(%ebp)
f0104cfc:	ff 75 0c             	pushl  0xc(%ebp)
f0104cff:	ff 75 08             	pushl  0x8(%ebp)
f0104d02:	e8 89 ff ff ff       	call   f0104c90 <memmove>
}
f0104d07:	c9                   	leave  
f0104d08:	c3                   	ret    

f0104d09 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
f0104d09:	55                   	push   %ebp
f0104d0a:	89 e5                	mov    %esp,%ebp
f0104d0c:	56                   	push   %esi
f0104d0d:	53                   	push   %ebx
f0104d0e:	8b 45 08             	mov    0x8(%ebp),%eax
f0104d11:	8b 55 0c             	mov    0xc(%ebp),%edx
f0104d14:	89 c6                	mov    %eax,%esi
f0104d16:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
f0104d19:	eb 14                	jmp    f0104d2f <memcmp+0x26>
		if (*s1 != *s2)
f0104d1b:	8a 08                	mov    (%eax),%cl
f0104d1d:	8a 1a                	mov    (%edx),%bl
f0104d1f:	38 d9                	cmp    %bl,%cl
f0104d21:	74 0a                	je     f0104d2d <memcmp+0x24>
			return (int) *s1 - (int) *s2;
f0104d23:	0f b6 c1             	movzbl %cl,%eax
f0104d26:	0f b6 db             	movzbl %bl,%ebx
f0104d29:	29 d8                	sub    %ebx,%eax
f0104d2b:	eb 0b                	jmp    f0104d38 <memcmp+0x2f>
		s1++, s2++;
f0104d2d:	40                   	inc    %eax
f0104d2e:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
f0104d2f:	39 f0                	cmp    %esi,%eax
f0104d31:	75 e8                	jne    f0104d1b <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
f0104d33:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0104d38:	5b                   	pop    %ebx
f0104d39:	5e                   	pop    %esi
f0104d3a:	5d                   	pop    %ebp
f0104d3b:	c3                   	ret    

f0104d3c <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
f0104d3c:	55                   	push   %ebp
f0104d3d:	89 e5                	mov    %esp,%ebp
f0104d3f:	53                   	push   %ebx
f0104d40:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
f0104d43:	89 c1                	mov    %eax,%ecx
f0104d45:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
f0104d48:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f0104d4c:	eb 08                	jmp    f0104d56 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
f0104d4e:	0f b6 10             	movzbl (%eax),%edx
f0104d51:	39 da                	cmp    %ebx,%edx
f0104d53:	74 05                	je     f0104d5a <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f0104d55:	40                   	inc    %eax
f0104d56:	39 c8                	cmp    %ecx,%eax
f0104d58:	72 f4                	jb     f0104d4e <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
f0104d5a:	5b                   	pop    %ebx
f0104d5b:	5d                   	pop    %ebp
f0104d5c:	c3                   	ret    

f0104d5d <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
f0104d5d:	55                   	push   %ebp
f0104d5e:	89 e5                	mov    %esp,%ebp
f0104d60:	57                   	push   %edi
f0104d61:	56                   	push   %esi
f0104d62:	53                   	push   %ebx
f0104d63:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f0104d66:	eb 01                	jmp    f0104d69 <strtol+0xc>
		s++;
f0104d68:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f0104d69:	8a 01                	mov    (%ecx),%al
f0104d6b:	3c 20                	cmp    $0x20,%al
f0104d6d:	74 f9                	je     f0104d68 <strtol+0xb>
f0104d6f:	3c 09                	cmp    $0x9,%al
f0104d71:	74 f5                	je     f0104d68 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
f0104d73:	3c 2b                	cmp    $0x2b,%al
f0104d75:	75 08                	jne    f0104d7f <strtol+0x22>
		s++;
f0104d77:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
f0104d78:	bf 00 00 00 00       	mov    $0x0,%edi
f0104d7d:	eb 11                	jmp    f0104d90 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
f0104d7f:	3c 2d                	cmp    $0x2d,%al
f0104d81:	75 08                	jne    f0104d8b <strtol+0x2e>
		s++, neg = 1;
f0104d83:	41                   	inc    %ecx
f0104d84:	bf 01 00 00 00       	mov    $0x1,%edi
f0104d89:	eb 05                	jmp    f0104d90 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
f0104d8b:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f0104d90:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0104d94:	0f 84 87 00 00 00    	je     f0104e21 <strtol+0xc4>
f0104d9a:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
f0104d9e:	75 27                	jne    f0104dc7 <strtol+0x6a>
f0104da0:	80 39 30             	cmpb   $0x30,(%ecx)
f0104da3:	75 22                	jne    f0104dc7 <strtol+0x6a>
f0104da5:	e9 88 00 00 00       	jmp    f0104e32 <strtol+0xd5>
		s += 2, base = 16;
f0104daa:	83 c1 02             	add    $0x2,%ecx
f0104dad:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
f0104db4:	eb 11                	jmp    f0104dc7 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
f0104db6:	41                   	inc    %ecx
f0104db7:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
f0104dbe:	eb 07                	jmp    f0104dc7 <strtol+0x6a>
	else if (base == 0)
		base = 10;
f0104dc0:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
f0104dc7:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
f0104dcc:	8a 11                	mov    (%ecx),%dl
f0104dce:	8d 5a d0             	lea    -0x30(%edx),%ebx
f0104dd1:	80 fb 09             	cmp    $0x9,%bl
f0104dd4:	77 08                	ja     f0104dde <strtol+0x81>
			dig = *s - '0';
f0104dd6:	0f be d2             	movsbl %dl,%edx
f0104dd9:	83 ea 30             	sub    $0x30,%edx
f0104ddc:	eb 22                	jmp    f0104e00 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
f0104dde:	8d 72 9f             	lea    -0x61(%edx),%esi
f0104de1:	89 f3                	mov    %esi,%ebx
f0104de3:	80 fb 19             	cmp    $0x19,%bl
f0104de6:	77 08                	ja     f0104df0 <strtol+0x93>
			dig = *s - 'a' + 10;
f0104de8:	0f be d2             	movsbl %dl,%edx
f0104deb:	83 ea 57             	sub    $0x57,%edx
f0104dee:	eb 10                	jmp    f0104e00 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
f0104df0:	8d 72 bf             	lea    -0x41(%edx),%esi
f0104df3:	89 f3                	mov    %esi,%ebx
f0104df5:	80 fb 19             	cmp    $0x19,%bl
f0104df8:	77 14                	ja     f0104e0e <strtol+0xb1>
			dig = *s - 'A' + 10;
f0104dfa:	0f be d2             	movsbl %dl,%edx
f0104dfd:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
f0104e00:	3b 55 10             	cmp    0x10(%ebp),%edx
f0104e03:	7d 09                	jge    f0104e0e <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
f0104e05:	41                   	inc    %ecx
f0104e06:	0f af 45 10          	imul   0x10(%ebp),%eax
f0104e0a:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
f0104e0c:	eb be                	jmp    f0104dcc <strtol+0x6f>

	if (endptr)
f0104e0e:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f0104e12:	74 05                	je     f0104e19 <strtol+0xbc>
		*endptr = (char *) s;
f0104e14:	8b 75 0c             	mov    0xc(%ebp),%esi
f0104e17:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
f0104e19:	85 ff                	test   %edi,%edi
f0104e1b:	74 21                	je     f0104e3e <strtol+0xe1>
f0104e1d:	f7 d8                	neg    %eax
f0104e1f:	eb 1d                	jmp    f0104e3e <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f0104e21:	80 39 30             	cmpb   $0x30,(%ecx)
f0104e24:	75 9a                	jne    f0104dc0 <strtol+0x63>
f0104e26:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
f0104e2a:	0f 84 7a ff ff ff    	je     f0104daa <strtol+0x4d>
f0104e30:	eb 84                	jmp    f0104db6 <strtol+0x59>
f0104e32:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
f0104e36:	0f 84 6e ff ff ff    	je     f0104daa <strtol+0x4d>
f0104e3c:	eb 89                	jmp    f0104dc7 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
f0104e3e:	5b                   	pop    %ebx
f0104e3f:	5e                   	pop    %esi
f0104e40:	5f                   	pop    %edi
f0104e41:	5d                   	pop    %ebp
f0104e42:	c3                   	ret    
f0104e43:	90                   	nop

f0104e44 <__udivdi3>:
f0104e44:	55                   	push   %ebp
f0104e45:	57                   	push   %edi
f0104e46:	56                   	push   %esi
f0104e47:	53                   	push   %ebx
f0104e48:	83 ec 1c             	sub    $0x1c,%esp
f0104e4b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
f0104e4f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
f0104e53:	8b 7c 24 38          	mov    0x38(%esp),%edi
f0104e57:	89 5c 24 08          	mov    %ebx,0x8(%esp)
f0104e5b:	89 ca                	mov    %ecx,%edx
f0104e5d:	89 f8                	mov    %edi,%eax
f0104e5f:	8b 74 24 3c          	mov    0x3c(%esp),%esi
f0104e63:	85 f6                	test   %esi,%esi
f0104e65:	75 2d                	jne    f0104e94 <__udivdi3+0x50>
f0104e67:	39 cf                	cmp    %ecx,%edi
f0104e69:	77 65                	ja     f0104ed0 <__udivdi3+0x8c>
f0104e6b:	89 fd                	mov    %edi,%ebp
f0104e6d:	85 ff                	test   %edi,%edi
f0104e6f:	75 0b                	jne    f0104e7c <__udivdi3+0x38>
f0104e71:	b8 01 00 00 00       	mov    $0x1,%eax
f0104e76:	31 d2                	xor    %edx,%edx
f0104e78:	f7 f7                	div    %edi
f0104e7a:	89 c5                	mov    %eax,%ebp
f0104e7c:	31 d2                	xor    %edx,%edx
f0104e7e:	89 c8                	mov    %ecx,%eax
f0104e80:	f7 f5                	div    %ebp
f0104e82:	89 c1                	mov    %eax,%ecx
f0104e84:	89 d8                	mov    %ebx,%eax
f0104e86:	f7 f5                	div    %ebp
f0104e88:	89 cf                	mov    %ecx,%edi
f0104e8a:	89 fa                	mov    %edi,%edx
f0104e8c:	83 c4 1c             	add    $0x1c,%esp
f0104e8f:	5b                   	pop    %ebx
f0104e90:	5e                   	pop    %esi
f0104e91:	5f                   	pop    %edi
f0104e92:	5d                   	pop    %ebp
f0104e93:	c3                   	ret    
f0104e94:	39 ce                	cmp    %ecx,%esi
f0104e96:	77 28                	ja     f0104ec0 <__udivdi3+0x7c>
f0104e98:	0f bd fe             	bsr    %esi,%edi
f0104e9b:	83 f7 1f             	xor    $0x1f,%edi
f0104e9e:	75 40                	jne    f0104ee0 <__udivdi3+0x9c>
f0104ea0:	39 ce                	cmp    %ecx,%esi
f0104ea2:	72 0a                	jb     f0104eae <__udivdi3+0x6a>
f0104ea4:	3b 44 24 08          	cmp    0x8(%esp),%eax
f0104ea8:	0f 87 9e 00 00 00    	ja     f0104f4c <__udivdi3+0x108>
f0104eae:	b8 01 00 00 00       	mov    $0x1,%eax
f0104eb3:	89 fa                	mov    %edi,%edx
f0104eb5:	83 c4 1c             	add    $0x1c,%esp
f0104eb8:	5b                   	pop    %ebx
f0104eb9:	5e                   	pop    %esi
f0104eba:	5f                   	pop    %edi
f0104ebb:	5d                   	pop    %ebp
f0104ebc:	c3                   	ret    
f0104ebd:	8d 76 00             	lea    0x0(%esi),%esi
f0104ec0:	31 ff                	xor    %edi,%edi
f0104ec2:	31 c0                	xor    %eax,%eax
f0104ec4:	89 fa                	mov    %edi,%edx
f0104ec6:	83 c4 1c             	add    $0x1c,%esp
f0104ec9:	5b                   	pop    %ebx
f0104eca:	5e                   	pop    %esi
f0104ecb:	5f                   	pop    %edi
f0104ecc:	5d                   	pop    %ebp
f0104ecd:	c3                   	ret    
f0104ece:	66 90                	xchg   %ax,%ax
f0104ed0:	89 d8                	mov    %ebx,%eax
f0104ed2:	f7 f7                	div    %edi
f0104ed4:	31 ff                	xor    %edi,%edi
f0104ed6:	89 fa                	mov    %edi,%edx
f0104ed8:	83 c4 1c             	add    $0x1c,%esp
f0104edb:	5b                   	pop    %ebx
f0104edc:	5e                   	pop    %esi
f0104edd:	5f                   	pop    %edi
f0104ede:	5d                   	pop    %ebp
f0104edf:	c3                   	ret    
f0104ee0:	bd 20 00 00 00       	mov    $0x20,%ebp
f0104ee5:	89 eb                	mov    %ebp,%ebx
f0104ee7:	29 fb                	sub    %edi,%ebx
f0104ee9:	89 f9                	mov    %edi,%ecx
f0104eeb:	d3 e6                	shl    %cl,%esi
f0104eed:	89 c5                	mov    %eax,%ebp
f0104eef:	88 d9                	mov    %bl,%cl
f0104ef1:	d3 ed                	shr    %cl,%ebp
f0104ef3:	89 e9                	mov    %ebp,%ecx
f0104ef5:	09 f1                	or     %esi,%ecx
f0104ef7:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
f0104efb:	89 f9                	mov    %edi,%ecx
f0104efd:	d3 e0                	shl    %cl,%eax
f0104eff:	89 c5                	mov    %eax,%ebp
f0104f01:	89 d6                	mov    %edx,%esi
f0104f03:	88 d9                	mov    %bl,%cl
f0104f05:	d3 ee                	shr    %cl,%esi
f0104f07:	89 f9                	mov    %edi,%ecx
f0104f09:	d3 e2                	shl    %cl,%edx
f0104f0b:	8b 44 24 08          	mov    0x8(%esp),%eax
f0104f0f:	88 d9                	mov    %bl,%cl
f0104f11:	d3 e8                	shr    %cl,%eax
f0104f13:	09 c2                	or     %eax,%edx
f0104f15:	89 d0                	mov    %edx,%eax
f0104f17:	89 f2                	mov    %esi,%edx
f0104f19:	f7 74 24 0c          	divl   0xc(%esp)
f0104f1d:	89 d6                	mov    %edx,%esi
f0104f1f:	89 c3                	mov    %eax,%ebx
f0104f21:	f7 e5                	mul    %ebp
f0104f23:	39 d6                	cmp    %edx,%esi
f0104f25:	72 19                	jb     f0104f40 <__udivdi3+0xfc>
f0104f27:	74 0b                	je     f0104f34 <__udivdi3+0xf0>
f0104f29:	89 d8                	mov    %ebx,%eax
f0104f2b:	31 ff                	xor    %edi,%edi
f0104f2d:	e9 58 ff ff ff       	jmp    f0104e8a <__udivdi3+0x46>
f0104f32:	66 90                	xchg   %ax,%ax
f0104f34:	8b 54 24 08          	mov    0x8(%esp),%edx
f0104f38:	89 f9                	mov    %edi,%ecx
f0104f3a:	d3 e2                	shl    %cl,%edx
f0104f3c:	39 c2                	cmp    %eax,%edx
f0104f3e:	73 e9                	jae    f0104f29 <__udivdi3+0xe5>
f0104f40:	8d 43 ff             	lea    -0x1(%ebx),%eax
f0104f43:	31 ff                	xor    %edi,%edi
f0104f45:	e9 40 ff ff ff       	jmp    f0104e8a <__udivdi3+0x46>
f0104f4a:	66 90                	xchg   %ax,%ax
f0104f4c:	31 c0                	xor    %eax,%eax
f0104f4e:	e9 37 ff ff ff       	jmp    f0104e8a <__udivdi3+0x46>
f0104f53:	90                   	nop

f0104f54 <__umoddi3>:
f0104f54:	55                   	push   %ebp
f0104f55:	57                   	push   %edi
f0104f56:	56                   	push   %esi
f0104f57:	53                   	push   %ebx
f0104f58:	83 ec 1c             	sub    $0x1c,%esp
f0104f5b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
f0104f5f:	8b 74 24 34          	mov    0x34(%esp),%esi
f0104f63:	8b 7c 24 38          	mov    0x38(%esp),%edi
f0104f67:	8b 44 24 3c          	mov    0x3c(%esp),%eax
f0104f6b:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0104f6f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0104f73:	89 f3                	mov    %esi,%ebx
f0104f75:	89 fa                	mov    %edi,%edx
f0104f77:	89 4c 24 04          	mov    %ecx,0x4(%esp)
f0104f7b:	89 34 24             	mov    %esi,(%esp)
f0104f7e:	85 c0                	test   %eax,%eax
f0104f80:	75 1a                	jne    f0104f9c <__umoddi3+0x48>
f0104f82:	39 f7                	cmp    %esi,%edi
f0104f84:	0f 86 a2 00 00 00    	jbe    f010502c <__umoddi3+0xd8>
f0104f8a:	89 c8                	mov    %ecx,%eax
f0104f8c:	89 f2                	mov    %esi,%edx
f0104f8e:	f7 f7                	div    %edi
f0104f90:	89 d0                	mov    %edx,%eax
f0104f92:	31 d2                	xor    %edx,%edx
f0104f94:	83 c4 1c             	add    $0x1c,%esp
f0104f97:	5b                   	pop    %ebx
f0104f98:	5e                   	pop    %esi
f0104f99:	5f                   	pop    %edi
f0104f9a:	5d                   	pop    %ebp
f0104f9b:	c3                   	ret    
f0104f9c:	39 f0                	cmp    %esi,%eax
f0104f9e:	0f 87 ac 00 00 00    	ja     f0105050 <__umoddi3+0xfc>
f0104fa4:	0f bd e8             	bsr    %eax,%ebp
f0104fa7:	83 f5 1f             	xor    $0x1f,%ebp
f0104faa:	0f 84 ac 00 00 00    	je     f010505c <__umoddi3+0x108>
f0104fb0:	bf 20 00 00 00       	mov    $0x20,%edi
f0104fb5:	29 ef                	sub    %ebp,%edi
f0104fb7:	89 fe                	mov    %edi,%esi
f0104fb9:	89 7c 24 0c          	mov    %edi,0xc(%esp)
f0104fbd:	89 e9                	mov    %ebp,%ecx
f0104fbf:	d3 e0                	shl    %cl,%eax
f0104fc1:	89 d7                	mov    %edx,%edi
f0104fc3:	89 f1                	mov    %esi,%ecx
f0104fc5:	d3 ef                	shr    %cl,%edi
f0104fc7:	09 c7                	or     %eax,%edi
f0104fc9:	89 e9                	mov    %ebp,%ecx
f0104fcb:	d3 e2                	shl    %cl,%edx
f0104fcd:	89 14 24             	mov    %edx,(%esp)
f0104fd0:	89 d8                	mov    %ebx,%eax
f0104fd2:	d3 e0                	shl    %cl,%eax
f0104fd4:	89 c2                	mov    %eax,%edx
f0104fd6:	8b 44 24 08          	mov    0x8(%esp),%eax
f0104fda:	d3 e0                	shl    %cl,%eax
f0104fdc:	89 44 24 04          	mov    %eax,0x4(%esp)
f0104fe0:	8b 44 24 08          	mov    0x8(%esp),%eax
f0104fe4:	89 f1                	mov    %esi,%ecx
f0104fe6:	d3 e8                	shr    %cl,%eax
f0104fe8:	09 d0                	or     %edx,%eax
f0104fea:	d3 eb                	shr    %cl,%ebx
f0104fec:	89 da                	mov    %ebx,%edx
f0104fee:	f7 f7                	div    %edi
f0104ff0:	89 d3                	mov    %edx,%ebx
f0104ff2:	f7 24 24             	mull   (%esp)
f0104ff5:	89 c6                	mov    %eax,%esi
f0104ff7:	89 d1                	mov    %edx,%ecx
f0104ff9:	39 d3                	cmp    %edx,%ebx
f0104ffb:	0f 82 87 00 00 00    	jb     f0105088 <__umoddi3+0x134>
f0105001:	0f 84 91 00 00 00    	je     f0105098 <__umoddi3+0x144>
f0105007:	8b 54 24 04          	mov    0x4(%esp),%edx
f010500b:	29 f2                	sub    %esi,%edx
f010500d:	19 cb                	sbb    %ecx,%ebx
f010500f:	89 d8                	mov    %ebx,%eax
f0105011:	8a 4c 24 0c          	mov    0xc(%esp),%cl
f0105015:	d3 e0                	shl    %cl,%eax
f0105017:	89 e9                	mov    %ebp,%ecx
f0105019:	d3 ea                	shr    %cl,%edx
f010501b:	09 d0                	or     %edx,%eax
f010501d:	89 e9                	mov    %ebp,%ecx
f010501f:	d3 eb                	shr    %cl,%ebx
f0105021:	89 da                	mov    %ebx,%edx
f0105023:	83 c4 1c             	add    $0x1c,%esp
f0105026:	5b                   	pop    %ebx
f0105027:	5e                   	pop    %esi
f0105028:	5f                   	pop    %edi
f0105029:	5d                   	pop    %ebp
f010502a:	c3                   	ret    
f010502b:	90                   	nop
f010502c:	89 fd                	mov    %edi,%ebp
f010502e:	85 ff                	test   %edi,%edi
f0105030:	75 0b                	jne    f010503d <__umoddi3+0xe9>
f0105032:	b8 01 00 00 00       	mov    $0x1,%eax
f0105037:	31 d2                	xor    %edx,%edx
f0105039:	f7 f7                	div    %edi
f010503b:	89 c5                	mov    %eax,%ebp
f010503d:	89 f0                	mov    %esi,%eax
f010503f:	31 d2                	xor    %edx,%edx
f0105041:	f7 f5                	div    %ebp
f0105043:	89 c8                	mov    %ecx,%eax
f0105045:	f7 f5                	div    %ebp
f0105047:	89 d0                	mov    %edx,%eax
f0105049:	e9 44 ff ff ff       	jmp    f0104f92 <__umoddi3+0x3e>
f010504e:	66 90                	xchg   %ax,%ax
f0105050:	89 c8                	mov    %ecx,%eax
f0105052:	89 f2                	mov    %esi,%edx
f0105054:	83 c4 1c             	add    $0x1c,%esp
f0105057:	5b                   	pop    %ebx
f0105058:	5e                   	pop    %esi
f0105059:	5f                   	pop    %edi
f010505a:	5d                   	pop    %ebp
f010505b:	c3                   	ret    
f010505c:	3b 04 24             	cmp    (%esp),%eax
f010505f:	72 06                	jb     f0105067 <__umoddi3+0x113>
f0105061:	3b 7c 24 04          	cmp    0x4(%esp),%edi
f0105065:	77 0f                	ja     f0105076 <__umoddi3+0x122>
f0105067:	89 f2                	mov    %esi,%edx
f0105069:	29 f9                	sub    %edi,%ecx
f010506b:	1b 54 24 0c          	sbb    0xc(%esp),%edx
f010506f:	89 14 24             	mov    %edx,(%esp)
f0105072:	89 4c 24 04          	mov    %ecx,0x4(%esp)
f0105076:	8b 44 24 04          	mov    0x4(%esp),%eax
f010507a:	8b 14 24             	mov    (%esp),%edx
f010507d:	83 c4 1c             	add    $0x1c,%esp
f0105080:	5b                   	pop    %ebx
f0105081:	5e                   	pop    %esi
f0105082:	5f                   	pop    %edi
f0105083:	5d                   	pop    %ebp
f0105084:	c3                   	ret    
f0105085:	8d 76 00             	lea    0x0(%esi),%esi
f0105088:	2b 04 24             	sub    (%esp),%eax
f010508b:	19 fa                	sbb    %edi,%edx
f010508d:	89 d1                	mov    %edx,%ecx
f010508f:	89 c6                	mov    %eax,%esi
f0105091:	e9 71 ff ff ff       	jmp    f0105007 <__umoddi3+0xb3>
f0105096:	66 90                	xchg   %ax,%ax
f0105098:	39 44 24 04          	cmp    %eax,0x4(%esp)
f010509c:	72 ea                	jb     f0105088 <__umoddi3+0x134>
f010509e:	89 d9                	mov    %ebx,%ecx
f01050a0:	e9 62 ff ff ff       	jmp    f0105007 <__umoddi3+0xb3>
