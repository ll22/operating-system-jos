
obj/user/evilhello:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 19 00 00 00       	call   80004a <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	// try to print the kernel entry point as a string!  mua ha ha!
	sys_cputs((char*)0xf010000c, 100);
  800039:	6a 64                	push   $0x64
  80003b:	68 0c 00 10 f0       	push   $0xf010000c
  800040:	e8 62 00 00 00       	call   8000a7 <sys_cputs>
}
  800045:	83 c4 10             	add    $0x10,%esp
  800048:	c9                   	leave  
  800049:	c3                   	ret    

0080004a <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80004a:	55                   	push   %ebp
  80004b:	89 e5                	mov    %esp,%ebp
  80004d:	56                   	push   %esi
  80004e:	53                   	push   %ebx
  80004f:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800052:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800055:	e8 cb 00 00 00       	call   800125 <sys_getenvid>
  80005a:	25 ff 03 00 00       	and    $0x3ff,%eax
  80005f:	8d 14 00             	lea    (%eax,%eax,1),%edx
  800062:	01 d0                	add    %edx,%eax
  800064:	c1 e0 05             	shl    $0x5,%eax
  800067:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80006c:	a3 04 10 80 00       	mov    %eax,0x801004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800071:	85 db                	test   %ebx,%ebx
  800073:	7e 07                	jle    80007c <libmain+0x32>
		binaryname = argv[0];
  800075:	8b 06                	mov    (%esi),%eax
  800077:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  80007c:	83 ec 08             	sub    $0x8,%esp
  80007f:	56                   	push   %esi
  800080:	53                   	push   %ebx
  800081:	e8 ad ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800086:	e8 0a 00 00 00       	call   800095 <exit>
}
  80008b:	83 c4 10             	add    $0x10,%esp
  80008e:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800091:	5b                   	pop    %ebx
  800092:	5e                   	pop    %esi
  800093:	5d                   	pop    %ebp
  800094:	c3                   	ret    

00800095 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800095:	55                   	push   %ebp
  800096:	89 e5                	mov    %esp,%ebp
  800098:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  80009b:	6a 00                	push   $0x0
  80009d:	e8 42 00 00 00       	call   8000e4 <sys_env_destroy>
}
  8000a2:	83 c4 10             	add    $0x10,%esp
  8000a5:	c9                   	leave  
  8000a6:	c3                   	ret    

008000a7 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000a7:	55                   	push   %ebp
  8000a8:	89 e5                	mov    %esp,%ebp
  8000aa:	57                   	push   %edi
  8000ab:	56                   	push   %esi
  8000ac:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000ad:	b8 00 00 00 00       	mov    $0x0,%eax
  8000b2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000b5:	8b 55 08             	mov    0x8(%ebp),%edx
  8000b8:	89 c3                	mov    %eax,%ebx
  8000ba:	89 c7                	mov    %eax,%edi
  8000bc:	89 c6                	mov    %eax,%esi
  8000be:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000c0:	5b                   	pop    %ebx
  8000c1:	5e                   	pop    %esi
  8000c2:	5f                   	pop    %edi
  8000c3:	5d                   	pop    %ebp
  8000c4:	c3                   	ret    

008000c5 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000c5:	55                   	push   %ebp
  8000c6:	89 e5                	mov    %esp,%ebp
  8000c8:	57                   	push   %edi
  8000c9:	56                   	push   %esi
  8000ca:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000cb:	ba 00 00 00 00       	mov    $0x0,%edx
  8000d0:	b8 01 00 00 00       	mov    $0x1,%eax
  8000d5:	89 d1                	mov    %edx,%ecx
  8000d7:	89 d3                	mov    %edx,%ebx
  8000d9:	89 d7                	mov    %edx,%edi
  8000db:	89 d6                	mov    %edx,%esi
  8000dd:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000df:	5b                   	pop    %ebx
  8000e0:	5e                   	pop    %esi
  8000e1:	5f                   	pop    %edi
  8000e2:	5d                   	pop    %ebp
  8000e3:	c3                   	ret    

008000e4 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000e4:	55                   	push   %ebp
  8000e5:	89 e5                	mov    %esp,%ebp
  8000e7:	57                   	push   %edi
  8000e8:	56                   	push   %esi
  8000e9:	53                   	push   %ebx
  8000ea:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000ed:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000f2:	b8 03 00 00 00       	mov    $0x3,%eax
  8000f7:	8b 55 08             	mov    0x8(%ebp),%edx
  8000fa:	89 cb                	mov    %ecx,%ebx
  8000fc:	89 cf                	mov    %ecx,%edi
  8000fe:	89 ce                	mov    %ecx,%esi
  800100:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800102:	85 c0                	test   %eax,%eax
  800104:	7e 17                	jle    80011d <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800106:	83 ec 0c             	sub    $0xc,%esp
  800109:	50                   	push   %eax
  80010a:	6a 03                	push   $0x3
  80010c:	68 16 0d 80 00       	push   $0x800d16
  800111:	6a 23                	push   $0x23
  800113:	68 33 0d 80 00       	push   $0x800d33
  800118:	e8 27 00 00 00       	call   800144 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  80011d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800120:	5b                   	pop    %ebx
  800121:	5e                   	pop    %esi
  800122:	5f                   	pop    %edi
  800123:	5d                   	pop    %ebp
  800124:	c3                   	ret    

00800125 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800125:	55                   	push   %ebp
  800126:	89 e5                	mov    %esp,%ebp
  800128:	57                   	push   %edi
  800129:	56                   	push   %esi
  80012a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80012b:	ba 00 00 00 00       	mov    $0x0,%edx
  800130:	b8 02 00 00 00       	mov    $0x2,%eax
  800135:	89 d1                	mov    %edx,%ecx
  800137:	89 d3                	mov    %edx,%ebx
  800139:	89 d7                	mov    %edx,%edi
  80013b:	89 d6                	mov    %edx,%esi
  80013d:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  80013f:	5b                   	pop    %ebx
  800140:	5e                   	pop    %esi
  800141:	5f                   	pop    %edi
  800142:	5d                   	pop    %ebp
  800143:	c3                   	ret    

00800144 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800144:	55                   	push   %ebp
  800145:	89 e5                	mov    %esp,%ebp
  800147:	56                   	push   %esi
  800148:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800149:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80014c:	8b 35 00 10 80 00    	mov    0x801000,%esi
  800152:	e8 ce ff ff ff       	call   800125 <sys_getenvid>
  800157:	83 ec 0c             	sub    $0xc,%esp
  80015a:	ff 75 0c             	pushl  0xc(%ebp)
  80015d:	ff 75 08             	pushl  0x8(%ebp)
  800160:	56                   	push   %esi
  800161:	50                   	push   %eax
  800162:	68 44 0d 80 00       	push   $0x800d44
  800167:	e8 b0 00 00 00       	call   80021c <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80016c:	83 c4 18             	add    $0x18,%esp
  80016f:	53                   	push   %ebx
  800170:	ff 75 10             	pushl  0x10(%ebp)
  800173:	e8 53 00 00 00       	call   8001cb <vcprintf>
	cprintf("\n");
  800178:	c7 04 24 68 0d 80 00 	movl   $0x800d68,(%esp)
  80017f:	e8 98 00 00 00       	call   80021c <cprintf>
  800184:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800187:	cc                   	int3   
  800188:	eb fd                	jmp    800187 <_panic+0x43>

0080018a <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80018a:	55                   	push   %ebp
  80018b:	89 e5                	mov    %esp,%ebp
  80018d:	53                   	push   %ebx
  80018e:	83 ec 04             	sub    $0x4,%esp
  800191:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800194:	8b 13                	mov    (%ebx),%edx
  800196:	8d 42 01             	lea    0x1(%edx),%eax
  800199:	89 03                	mov    %eax,(%ebx)
  80019b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80019e:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8001a2:	3d ff 00 00 00       	cmp    $0xff,%eax
  8001a7:	75 1a                	jne    8001c3 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8001a9:	83 ec 08             	sub    $0x8,%esp
  8001ac:	68 ff 00 00 00       	push   $0xff
  8001b1:	8d 43 08             	lea    0x8(%ebx),%eax
  8001b4:	50                   	push   %eax
  8001b5:	e8 ed fe ff ff       	call   8000a7 <sys_cputs>
		b->idx = 0;
  8001ba:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8001c0:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8001c3:	ff 43 04             	incl   0x4(%ebx)
}
  8001c6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001c9:	c9                   	leave  
  8001ca:	c3                   	ret    

008001cb <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8001cb:	55                   	push   %ebp
  8001cc:	89 e5                	mov    %esp,%ebp
  8001ce:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001d4:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001db:	00 00 00 
	b.cnt = 0;
  8001de:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001e5:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001e8:	ff 75 0c             	pushl  0xc(%ebp)
  8001eb:	ff 75 08             	pushl  0x8(%ebp)
  8001ee:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001f4:	50                   	push   %eax
  8001f5:	68 8a 01 80 00       	push   $0x80018a
  8001fa:	e8 51 01 00 00       	call   800350 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001ff:	83 c4 08             	add    $0x8,%esp
  800202:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800208:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80020e:	50                   	push   %eax
  80020f:	e8 93 fe ff ff       	call   8000a7 <sys_cputs>

	return b.cnt;
}
  800214:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80021a:	c9                   	leave  
  80021b:	c3                   	ret    

0080021c <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80021c:	55                   	push   %ebp
  80021d:	89 e5                	mov    %esp,%ebp
  80021f:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800222:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800225:	50                   	push   %eax
  800226:	ff 75 08             	pushl  0x8(%ebp)
  800229:	e8 9d ff ff ff       	call   8001cb <vcprintf>
	va_end(ap);

	return cnt;
}
  80022e:	c9                   	leave  
  80022f:	c3                   	ret    

00800230 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800230:	55                   	push   %ebp
  800231:	89 e5                	mov    %esp,%ebp
  800233:	57                   	push   %edi
  800234:	56                   	push   %esi
  800235:	53                   	push   %ebx
  800236:	83 ec 1c             	sub    $0x1c,%esp
  800239:	89 c7                	mov    %eax,%edi
  80023b:	89 d6                	mov    %edx,%esi
  80023d:	8b 45 08             	mov    0x8(%ebp),%eax
  800240:	8b 55 0c             	mov    0xc(%ebp),%edx
  800243:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800246:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800249:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80024c:	bb 00 00 00 00       	mov    $0x0,%ebx
  800251:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800254:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800257:	39 d3                	cmp    %edx,%ebx
  800259:	72 05                	jb     800260 <printnum+0x30>
  80025b:	39 45 10             	cmp    %eax,0x10(%ebp)
  80025e:	77 45                	ja     8002a5 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800260:	83 ec 0c             	sub    $0xc,%esp
  800263:	ff 75 18             	pushl  0x18(%ebp)
  800266:	8b 45 14             	mov    0x14(%ebp),%eax
  800269:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80026c:	53                   	push   %ebx
  80026d:	ff 75 10             	pushl  0x10(%ebp)
  800270:	83 ec 08             	sub    $0x8,%esp
  800273:	ff 75 e4             	pushl  -0x1c(%ebp)
  800276:	ff 75 e0             	pushl  -0x20(%ebp)
  800279:	ff 75 dc             	pushl  -0x24(%ebp)
  80027c:	ff 75 d8             	pushl  -0x28(%ebp)
  80027f:	e8 24 08 00 00       	call   800aa8 <__udivdi3>
  800284:	83 c4 18             	add    $0x18,%esp
  800287:	52                   	push   %edx
  800288:	50                   	push   %eax
  800289:	89 f2                	mov    %esi,%edx
  80028b:	89 f8                	mov    %edi,%eax
  80028d:	e8 9e ff ff ff       	call   800230 <printnum>
  800292:	83 c4 20             	add    $0x20,%esp
  800295:	eb 16                	jmp    8002ad <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800297:	83 ec 08             	sub    $0x8,%esp
  80029a:	56                   	push   %esi
  80029b:	ff 75 18             	pushl  0x18(%ebp)
  80029e:	ff d7                	call   *%edi
  8002a0:	83 c4 10             	add    $0x10,%esp
  8002a3:	eb 03                	jmp    8002a8 <printnum+0x78>
  8002a5:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8002a8:	4b                   	dec    %ebx
  8002a9:	85 db                	test   %ebx,%ebx
  8002ab:	7f ea                	jg     800297 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8002ad:	83 ec 08             	sub    $0x8,%esp
  8002b0:	56                   	push   %esi
  8002b1:	83 ec 04             	sub    $0x4,%esp
  8002b4:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002b7:	ff 75 e0             	pushl  -0x20(%ebp)
  8002ba:	ff 75 dc             	pushl  -0x24(%ebp)
  8002bd:	ff 75 d8             	pushl  -0x28(%ebp)
  8002c0:	e8 f3 08 00 00       	call   800bb8 <__umoddi3>
  8002c5:	83 c4 14             	add    $0x14,%esp
  8002c8:	0f be 80 6a 0d 80 00 	movsbl 0x800d6a(%eax),%eax
  8002cf:	50                   	push   %eax
  8002d0:	ff d7                	call   *%edi
}
  8002d2:	83 c4 10             	add    $0x10,%esp
  8002d5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002d8:	5b                   	pop    %ebx
  8002d9:	5e                   	pop    %esi
  8002da:	5f                   	pop    %edi
  8002db:	5d                   	pop    %ebp
  8002dc:	c3                   	ret    

008002dd <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002dd:	55                   	push   %ebp
  8002de:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002e0:	83 fa 01             	cmp    $0x1,%edx
  8002e3:	7e 0e                	jle    8002f3 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002e5:	8b 10                	mov    (%eax),%edx
  8002e7:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002ea:	89 08                	mov    %ecx,(%eax)
  8002ec:	8b 02                	mov    (%edx),%eax
  8002ee:	8b 52 04             	mov    0x4(%edx),%edx
  8002f1:	eb 22                	jmp    800315 <getuint+0x38>
	else if (lflag)
  8002f3:	85 d2                	test   %edx,%edx
  8002f5:	74 10                	je     800307 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8002f7:	8b 10                	mov    (%eax),%edx
  8002f9:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002fc:	89 08                	mov    %ecx,(%eax)
  8002fe:	8b 02                	mov    (%edx),%eax
  800300:	ba 00 00 00 00       	mov    $0x0,%edx
  800305:	eb 0e                	jmp    800315 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800307:	8b 10                	mov    (%eax),%edx
  800309:	8d 4a 04             	lea    0x4(%edx),%ecx
  80030c:	89 08                	mov    %ecx,(%eax)
  80030e:	8b 02                	mov    (%edx),%eax
  800310:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800315:	5d                   	pop    %ebp
  800316:	c3                   	ret    

00800317 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800317:	55                   	push   %ebp
  800318:	89 e5                	mov    %esp,%ebp
  80031a:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80031d:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800320:	8b 10                	mov    (%eax),%edx
  800322:	3b 50 04             	cmp    0x4(%eax),%edx
  800325:	73 0a                	jae    800331 <sprintputch+0x1a>
		*b->buf++ = ch;
  800327:	8d 4a 01             	lea    0x1(%edx),%ecx
  80032a:	89 08                	mov    %ecx,(%eax)
  80032c:	8b 45 08             	mov    0x8(%ebp),%eax
  80032f:	88 02                	mov    %al,(%edx)
}
  800331:	5d                   	pop    %ebp
  800332:	c3                   	ret    

00800333 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800333:	55                   	push   %ebp
  800334:	89 e5                	mov    %esp,%ebp
  800336:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800339:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80033c:	50                   	push   %eax
  80033d:	ff 75 10             	pushl  0x10(%ebp)
  800340:	ff 75 0c             	pushl  0xc(%ebp)
  800343:	ff 75 08             	pushl  0x8(%ebp)
  800346:	e8 05 00 00 00       	call   800350 <vprintfmt>
	va_end(ap);
}
  80034b:	83 c4 10             	add    $0x10,%esp
  80034e:	c9                   	leave  
  80034f:	c3                   	ret    

00800350 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800350:	55                   	push   %ebp
  800351:	89 e5                	mov    %esp,%ebp
  800353:	57                   	push   %edi
  800354:	56                   	push   %esi
  800355:	53                   	push   %ebx
  800356:	83 ec 2c             	sub    $0x2c,%esp
  800359:	8b 75 08             	mov    0x8(%ebp),%esi
  80035c:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80035f:	8b 7d 10             	mov    0x10(%ebp),%edi
  800362:	eb 12                	jmp    800376 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800364:	85 c0                	test   %eax,%eax
  800366:	0f 84 68 03 00 00    	je     8006d4 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80036c:	83 ec 08             	sub    $0x8,%esp
  80036f:	53                   	push   %ebx
  800370:	50                   	push   %eax
  800371:	ff d6                	call   *%esi
  800373:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800376:	47                   	inc    %edi
  800377:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80037b:	83 f8 25             	cmp    $0x25,%eax
  80037e:	75 e4                	jne    800364 <vprintfmt+0x14>
  800380:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800384:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80038b:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800392:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800399:	ba 00 00 00 00       	mov    $0x0,%edx
  80039e:	eb 07                	jmp    8003a7 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003a0:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8003a3:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003a7:	8d 47 01             	lea    0x1(%edi),%eax
  8003aa:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8003ad:	0f b6 0f             	movzbl (%edi),%ecx
  8003b0:	8a 07                	mov    (%edi),%al
  8003b2:	83 e8 23             	sub    $0x23,%eax
  8003b5:	3c 55                	cmp    $0x55,%al
  8003b7:	0f 87 fe 02 00 00    	ja     8006bb <vprintfmt+0x36b>
  8003bd:	0f b6 c0             	movzbl %al,%eax
  8003c0:	ff 24 85 f8 0d 80 00 	jmp    *0x800df8(,%eax,4)
  8003c7:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003ca:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003ce:	eb d7                	jmp    8003a7 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003d0:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003d3:	b8 00 00 00 00       	mov    $0x0,%eax
  8003d8:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003db:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003de:	01 c0                	add    %eax,%eax
  8003e0:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003e4:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003e7:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003ea:	83 fa 09             	cmp    $0x9,%edx
  8003ed:	77 34                	ja     800423 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003ef:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003f0:	eb e9                	jmp    8003db <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003f2:	8b 45 14             	mov    0x14(%ebp),%eax
  8003f5:	8d 48 04             	lea    0x4(%eax),%ecx
  8003f8:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8003fb:	8b 00                	mov    (%eax),%eax
  8003fd:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800400:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800403:	eb 24                	jmp    800429 <vprintfmt+0xd9>
  800405:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800409:	79 07                	jns    800412 <vprintfmt+0xc2>
  80040b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800412:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800415:	eb 90                	jmp    8003a7 <vprintfmt+0x57>
  800417:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80041a:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800421:	eb 84                	jmp    8003a7 <vprintfmt+0x57>
  800423:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800426:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800429:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80042d:	0f 89 74 ff ff ff    	jns    8003a7 <vprintfmt+0x57>
				width = precision, precision = -1;
  800433:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800436:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800439:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800440:	e9 62 ff ff ff       	jmp    8003a7 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800445:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800446:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800449:	e9 59 ff ff ff       	jmp    8003a7 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80044e:	8b 45 14             	mov    0x14(%ebp),%eax
  800451:	8d 50 04             	lea    0x4(%eax),%edx
  800454:	89 55 14             	mov    %edx,0x14(%ebp)
  800457:	83 ec 08             	sub    $0x8,%esp
  80045a:	53                   	push   %ebx
  80045b:	ff 30                	pushl  (%eax)
  80045d:	ff d6                	call   *%esi
			break;
  80045f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800462:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800465:	e9 0c ff ff ff       	jmp    800376 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80046a:	8b 45 14             	mov    0x14(%ebp),%eax
  80046d:	8d 50 04             	lea    0x4(%eax),%edx
  800470:	89 55 14             	mov    %edx,0x14(%ebp)
  800473:	8b 00                	mov    (%eax),%eax
  800475:	85 c0                	test   %eax,%eax
  800477:	79 02                	jns    80047b <vprintfmt+0x12b>
  800479:	f7 d8                	neg    %eax
  80047b:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80047d:	83 f8 06             	cmp    $0x6,%eax
  800480:	7f 0b                	jg     80048d <vprintfmt+0x13d>
  800482:	8b 04 85 50 0f 80 00 	mov    0x800f50(,%eax,4),%eax
  800489:	85 c0                	test   %eax,%eax
  80048b:	75 18                	jne    8004a5 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80048d:	52                   	push   %edx
  80048e:	68 82 0d 80 00       	push   $0x800d82
  800493:	53                   	push   %ebx
  800494:	56                   	push   %esi
  800495:	e8 99 fe ff ff       	call   800333 <printfmt>
  80049a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80049d:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8004a0:	e9 d1 fe ff ff       	jmp    800376 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8004a5:	50                   	push   %eax
  8004a6:	68 8b 0d 80 00       	push   $0x800d8b
  8004ab:	53                   	push   %ebx
  8004ac:	56                   	push   %esi
  8004ad:	e8 81 fe ff ff       	call   800333 <printfmt>
  8004b2:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004b5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004b8:	e9 b9 fe ff ff       	jmp    800376 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004bd:	8b 45 14             	mov    0x14(%ebp),%eax
  8004c0:	8d 50 04             	lea    0x4(%eax),%edx
  8004c3:	89 55 14             	mov    %edx,0x14(%ebp)
  8004c6:	8b 38                	mov    (%eax),%edi
  8004c8:	85 ff                	test   %edi,%edi
  8004ca:	75 05                	jne    8004d1 <vprintfmt+0x181>
				p = "(null)";
  8004cc:	bf 7b 0d 80 00       	mov    $0x800d7b,%edi
			if (width > 0 && padc != '-')
  8004d1:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004d5:	0f 8e 90 00 00 00    	jle    80056b <vprintfmt+0x21b>
  8004db:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004df:	0f 84 8e 00 00 00    	je     800573 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004e5:	83 ec 08             	sub    $0x8,%esp
  8004e8:	ff 75 d0             	pushl  -0x30(%ebp)
  8004eb:	57                   	push   %edi
  8004ec:	e8 70 02 00 00       	call   800761 <strnlen>
  8004f1:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8004f4:	29 c1                	sub    %eax,%ecx
  8004f6:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8004f9:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8004fc:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800500:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800503:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800506:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800508:	eb 0d                	jmp    800517 <vprintfmt+0x1c7>
					putch(padc, putdat);
  80050a:	83 ec 08             	sub    $0x8,%esp
  80050d:	53                   	push   %ebx
  80050e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800511:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800513:	4f                   	dec    %edi
  800514:	83 c4 10             	add    $0x10,%esp
  800517:	85 ff                	test   %edi,%edi
  800519:	7f ef                	jg     80050a <vprintfmt+0x1ba>
  80051b:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80051e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800521:	89 c8                	mov    %ecx,%eax
  800523:	85 c9                	test   %ecx,%ecx
  800525:	79 05                	jns    80052c <vprintfmt+0x1dc>
  800527:	b8 00 00 00 00       	mov    $0x0,%eax
  80052c:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80052f:	29 c1                	sub    %eax,%ecx
  800531:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800534:	89 75 08             	mov    %esi,0x8(%ebp)
  800537:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80053a:	eb 3d                	jmp    800579 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80053c:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800540:	74 19                	je     80055b <vprintfmt+0x20b>
  800542:	0f be c0             	movsbl %al,%eax
  800545:	83 e8 20             	sub    $0x20,%eax
  800548:	83 f8 5e             	cmp    $0x5e,%eax
  80054b:	76 0e                	jbe    80055b <vprintfmt+0x20b>
					putch('?', putdat);
  80054d:	83 ec 08             	sub    $0x8,%esp
  800550:	53                   	push   %ebx
  800551:	6a 3f                	push   $0x3f
  800553:	ff 55 08             	call   *0x8(%ebp)
  800556:	83 c4 10             	add    $0x10,%esp
  800559:	eb 0b                	jmp    800566 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80055b:	83 ec 08             	sub    $0x8,%esp
  80055e:	53                   	push   %ebx
  80055f:	52                   	push   %edx
  800560:	ff 55 08             	call   *0x8(%ebp)
  800563:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800566:	ff 4d e4             	decl   -0x1c(%ebp)
  800569:	eb 0e                	jmp    800579 <vprintfmt+0x229>
  80056b:	89 75 08             	mov    %esi,0x8(%ebp)
  80056e:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800571:	eb 06                	jmp    800579 <vprintfmt+0x229>
  800573:	89 75 08             	mov    %esi,0x8(%ebp)
  800576:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800579:	47                   	inc    %edi
  80057a:	8a 47 ff             	mov    -0x1(%edi),%al
  80057d:	0f be d0             	movsbl %al,%edx
  800580:	85 d2                	test   %edx,%edx
  800582:	74 1d                	je     8005a1 <vprintfmt+0x251>
  800584:	85 f6                	test   %esi,%esi
  800586:	78 b4                	js     80053c <vprintfmt+0x1ec>
  800588:	4e                   	dec    %esi
  800589:	79 b1                	jns    80053c <vprintfmt+0x1ec>
  80058b:	8b 75 08             	mov    0x8(%ebp),%esi
  80058e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800591:	eb 14                	jmp    8005a7 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800593:	83 ec 08             	sub    $0x8,%esp
  800596:	53                   	push   %ebx
  800597:	6a 20                	push   $0x20
  800599:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80059b:	4f                   	dec    %edi
  80059c:	83 c4 10             	add    $0x10,%esp
  80059f:	eb 06                	jmp    8005a7 <vprintfmt+0x257>
  8005a1:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8005a4:	8b 75 08             	mov    0x8(%ebp),%esi
  8005a7:	85 ff                	test   %edi,%edi
  8005a9:	7f e8                	jg     800593 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005ab:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005ae:	e9 c3 fd ff ff       	jmp    800376 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8005b3:	83 fa 01             	cmp    $0x1,%edx
  8005b6:	7e 16                	jle    8005ce <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8005b8:	8b 45 14             	mov    0x14(%ebp),%eax
  8005bb:	8d 50 08             	lea    0x8(%eax),%edx
  8005be:	89 55 14             	mov    %edx,0x14(%ebp)
  8005c1:	8b 50 04             	mov    0x4(%eax),%edx
  8005c4:	8b 00                	mov    (%eax),%eax
  8005c6:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005c9:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8005cc:	eb 32                	jmp    800600 <vprintfmt+0x2b0>
	else if (lflag)
  8005ce:	85 d2                	test   %edx,%edx
  8005d0:	74 18                	je     8005ea <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005d2:	8b 45 14             	mov    0x14(%ebp),%eax
  8005d5:	8d 50 04             	lea    0x4(%eax),%edx
  8005d8:	89 55 14             	mov    %edx,0x14(%ebp)
  8005db:	8b 00                	mov    (%eax),%eax
  8005dd:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005e0:	89 c1                	mov    %eax,%ecx
  8005e2:	c1 f9 1f             	sar    $0x1f,%ecx
  8005e5:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005e8:	eb 16                	jmp    800600 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005ea:	8b 45 14             	mov    0x14(%ebp),%eax
  8005ed:	8d 50 04             	lea    0x4(%eax),%edx
  8005f0:	89 55 14             	mov    %edx,0x14(%ebp)
  8005f3:	8b 00                	mov    (%eax),%eax
  8005f5:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005f8:	89 c1                	mov    %eax,%ecx
  8005fa:	c1 f9 1f             	sar    $0x1f,%ecx
  8005fd:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800600:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800603:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800606:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80060a:	79 76                	jns    800682 <vprintfmt+0x332>
				putch('-', putdat);
  80060c:	83 ec 08             	sub    $0x8,%esp
  80060f:	53                   	push   %ebx
  800610:	6a 2d                	push   $0x2d
  800612:	ff d6                	call   *%esi
				num = -(long long) num;
  800614:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800617:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80061a:	f7 d8                	neg    %eax
  80061c:	83 d2 00             	adc    $0x0,%edx
  80061f:	f7 da                	neg    %edx
  800621:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800624:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800629:	eb 5c                	jmp    800687 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80062b:	8d 45 14             	lea    0x14(%ebp),%eax
  80062e:	e8 aa fc ff ff       	call   8002dd <getuint>
			base = 10;
  800633:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800638:	eb 4d                	jmp    800687 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80063a:	8d 45 14             	lea    0x14(%ebp),%eax
  80063d:	e8 9b fc ff ff       	call   8002dd <getuint>
			base = 8;
  800642:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800647:	eb 3e                	jmp    800687 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800649:	83 ec 08             	sub    $0x8,%esp
  80064c:	53                   	push   %ebx
  80064d:	6a 30                	push   $0x30
  80064f:	ff d6                	call   *%esi
			putch('x', putdat);
  800651:	83 c4 08             	add    $0x8,%esp
  800654:	53                   	push   %ebx
  800655:	6a 78                	push   $0x78
  800657:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800659:	8b 45 14             	mov    0x14(%ebp),%eax
  80065c:	8d 50 04             	lea    0x4(%eax),%edx
  80065f:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800662:	8b 00                	mov    (%eax),%eax
  800664:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800669:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80066c:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800671:	eb 14                	jmp    800687 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800673:	8d 45 14             	lea    0x14(%ebp),%eax
  800676:	e8 62 fc ff ff       	call   8002dd <getuint>
			base = 16;
  80067b:	b9 10 00 00 00       	mov    $0x10,%ecx
  800680:	eb 05                	jmp    800687 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800682:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800687:	83 ec 0c             	sub    $0xc,%esp
  80068a:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80068e:	57                   	push   %edi
  80068f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800692:	51                   	push   %ecx
  800693:	52                   	push   %edx
  800694:	50                   	push   %eax
  800695:	89 da                	mov    %ebx,%edx
  800697:	89 f0                	mov    %esi,%eax
  800699:	e8 92 fb ff ff       	call   800230 <printnum>
			break;
  80069e:	83 c4 20             	add    $0x20,%esp
  8006a1:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006a4:	e9 cd fc ff ff       	jmp    800376 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8006a9:	83 ec 08             	sub    $0x8,%esp
  8006ac:	53                   	push   %ebx
  8006ad:	51                   	push   %ecx
  8006ae:	ff d6                	call   *%esi
			break;
  8006b0:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006b3:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8006b6:	e9 bb fc ff ff       	jmp    800376 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006bb:	83 ec 08             	sub    $0x8,%esp
  8006be:	53                   	push   %ebx
  8006bf:	6a 25                	push   $0x25
  8006c1:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006c3:	83 c4 10             	add    $0x10,%esp
  8006c6:	eb 01                	jmp    8006c9 <vprintfmt+0x379>
  8006c8:	4f                   	dec    %edi
  8006c9:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006cd:	75 f9                	jne    8006c8 <vprintfmt+0x378>
  8006cf:	e9 a2 fc ff ff       	jmp    800376 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006d4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006d7:	5b                   	pop    %ebx
  8006d8:	5e                   	pop    %esi
  8006d9:	5f                   	pop    %edi
  8006da:	5d                   	pop    %ebp
  8006db:	c3                   	ret    

008006dc <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006dc:	55                   	push   %ebp
  8006dd:	89 e5                	mov    %esp,%ebp
  8006df:	83 ec 18             	sub    $0x18,%esp
  8006e2:	8b 45 08             	mov    0x8(%ebp),%eax
  8006e5:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006e8:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006eb:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006ef:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8006f2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8006f9:	85 c0                	test   %eax,%eax
  8006fb:	74 26                	je     800723 <vsnprintf+0x47>
  8006fd:	85 d2                	test   %edx,%edx
  8006ff:	7e 29                	jle    80072a <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800701:	ff 75 14             	pushl  0x14(%ebp)
  800704:	ff 75 10             	pushl  0x10(%ebp)
  800707:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80070a:	50                   	push   %eax
  80070b:	68 17 03 80 00       	push   $0x800317
  800710:	e8 3b fc ff ff       	call   800350 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800715:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800718:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80071b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80071e:	83 c4 10             	add    $0x10,%esp
  800721:	eb 0c                	jmp    80072f <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800723:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800728:	eb 05                	jmp    80072f <vsnprintf+0x53>
  80072a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80072f:	c9                   	leave  
  800730:	c3                   	ret    

00800731 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800731:	55                   	push   %ebp
  800732:	89 e5                	mov    %esp,%ebp
  800734:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800737:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80073a:	50                   	push   %eax
  80073b:	ff 75 10             	pushl  0x10(%ebp)
  80073e:	ff 75 0c             	pushl  0xc(%ebp)
  800741:	ff 75 08             	pushl  0x8(%ebp)
  800744:	e8 93 ff ff ff       	call   8006dc <vsnprintf>
	va_end(ap);

	return rc;
}
  800749:	c9                   	leave  
  80074a:	c3                   	ret    

0080074b <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80074b:	55                   	push   %ebp
  80074c:	89 e5                	mov    %esp,%ebp
  80074e:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800751:	b8 00 00 00 00       	mov    $0x0,%eax
  800756:	eb 01                	jmp    800759 <strlen+0xe>
		n++;
  800758:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800759:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80075d:	75 f9                	jne    800758 <strlen+0xd>
		n++;
	return n;
}
  80075f:	5d                   	pop    %ebp
  800760:	c3                   	ret    

00800761 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800761:	55                   	push   %ebp
  800762:	89 e5                	mov    %esp,%ebp
  800764:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800767:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80076a:	ba 00 00 00 00       	mov    $0x0,%edx
  80076f:	eb 01                	jmp    800772 <strnlen+0x11>
		n++;
  800771:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800772:	39 c2                	cmp    %eax,%edx
  800774:	74 08                	je     80077e <strnlen+0x1d>
  800776:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80077a:	75 f5                	jne    800771 <strnlen+0x10>
  80077c:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80077e:	5d                   	pop    %ebp
  80077f:	c3                   	ret    

00800780 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800780:	55                   	push   %ebp
  800781:	89 e5                	mov    %esp,%ebp
  800783:	53                   	push   %ebx
  800784:	8b 45 08             	mov    0x8(%ebp),%eax
  800787:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80078a:	89 c2                	mov    %eax,%edx
  80078c:	42                   	inc    %edx
  80078d:	41                   	inc    %ecx
  80078e:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800791:	88 5a ff             	mov    %bl,-0x1(%edx)
  800794:	84 db                	test   %bl,%bl
  800796:	75 f4                	jne    80078c <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800798:	5b                   	pop    %ebx
  800799:	5d                   	pop    %ebp
  80079a:	c3                   	ret    

0080079b <strcat>:

char *
strcat(char *dst, const char *src)
{
  80079b:	55                   	push   %ebp
  80079c:	89 e5                	mov    %esp,%ebp
  80079e:	53                   	push   %ebx
  80079f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8007a2:	53                   	push   %ebx
  8007a3:	e8 a3 ff ff ff       	call   80074b <strlen>
  8007a8:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8007ab:	ff 75 0c             	pushl  0xc(%ebp)
  8007ae:	01 d8                	add    %ebx,%eax
  8007b0:	50                   	push   %eax
  8007b1:	e8 ca ff ff ff       	call   800780 <strcpy>
	return dst;
}
  8007b6:	89 d8                	mov    %ebx,%eax
  8007b8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8007bb:	c9                   	leave  
  8007bc:	c3                   	ret    

008007bd <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8007bd:	55                   	push   %ebp
  8007be:	89 e5                	mov    %esp,%ebp
  8007c0:	56                   	push   %esi
  8007c1:	53                   	push   %ebx
  8007c2:	8b 75 08             	mov    0x8(%ebp),%esi
  8007c5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007c8:	89 f3                	mov    %esi,%ebx
  8007ca:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007cd:	89 f2                	mov    %esi,%edx
  8007cf:	eb 0c                	jmp    8007dd <strncpy+0x20>
		*dst++ = *src;
  8007d1:	42                   	inc    %edx
  8007d2:	8a 01                	mov    (%ecx),%al
  8007d4:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007d7:	80 39 01             	cmpb   $0x1,(%ecx)
  8007da:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007dd:	39 da                	cmp    %ebx,%edx
  8007df:	75 f0                	jne    8007d1 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007e1:	89 f0                	mov    %esi,%eax
  8007e3:	5b                   	pop    %ebx
  8007e4:	5e                   	pop    %esi
  8007e5:	5d                   	pop    %ebp
  8007e6:	c3                   	ret    

008007e7 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007e7:	55                   	push   %ebp
  8007e8:	89 e5                	mov    %esp,%ebp
  8007ea:	56                   	push   %esi
  8007eb:	53                   	push   %ebx
  8007ec:	8b 75 08             	mov    0x8(%ebp),%esi
  8007ef:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007f2:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8007f5:	85 c0                	test   %eax,%eax
  8007f7:	74 1e                	je     800817 <strlcpy+0x30>
  8007f9:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8007fd:	89 f2                	mov    %esi,%edx
  8007ff:	eb 05                	jmp    800806 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800801:	42                   	inc    %edx
  800802:	41                   	inc    %ecx
  800803:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800806:	39 c2                	cmp    %eax,%edx
  800808:	74 08                	je     800812 <strlcpy+0x2b>
  80080a:	8a 19                	mov    (%ecx),%bl
  80080c:	84 db                	test   %bl,%bl
  80080e:	75 f1                	jne    800801 <strlcpy+0x1a>
  800810:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800812:	c6 00 00             	movb   $0x0,(%eax)
  800815:	eb 02                	jmp    800819 <strlcpy+0x32>
  800817:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800819:	29 f0                	sub    %esi,%eax
}
  80081b:	5b                   	pop    %ebx
  80081c:	5e                   	pop    %esi
  80081d:	5d                   	pop    %ebp
  80081e:	c3                   	ret    

0080081f <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80081f:	55                   	push   %ebp
  800820:	89 e5                	mov    %esp,%ebp
  800822:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800825:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800828:	eb 02                	jmp    80082c <strcmp+0xd>
		p++, q++;
  80082a:	41                   	inc    %ecx
  80082b:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80082c:	8a 01                	mov    (%ecx),%al
  80082e:	84 c0                	test   %al,%al
  800830:	74 04                	je     800836 <strcmp+0x17>
  800832:	3a 02                	cmp    (%edx),%al
  800834:	74 f4                	je     80082a <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800836:	0f b6 c0             	movzbl %al,%eax
  800839:	0f b6 12             	movzbl (%edx),%edx
  80083c:	29 d0                	sub    %edx,%eax
}
  80083e:	5d                   	pop    %ebp
  80083f:	c3                   	ret    

00800840 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800840:	55                   	push   %ebp
  800841:	89 e5                	mov    %esp,%ebp
  800843:	53                   	push   %ebx
  800844:	8b 45 08             	mov    0x8(%ebp),%eax
  800847:	8b 55 0c             	mov    0xc(%ebp),%edx
  80084a:	89 c3                	mov    %eax,%ebx
  80084c:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  80084f:	eb 02                	jmp    800853 <strncmp+0x13>
		n--, p++, q++;
  800851:	40                   	inc    %eax
  800852:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800853:	39 d8                	cmp    %ebx,%eax
  800855:	74 14                	je     80086b <strncmp+0x2b>
  800857:	8a 08                	mov    (%eax),%cl
  800859:	84 c9                	test   %cl,%cl
  80085b:	74 04                	je     800861 <strncmp+0x21>
  80085d:	3a 0a                	cmp    (%edx),%cl
  80085f:	74 f0                	je     800851 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800861:	0f b6 00             	movzbl (%eax),%eax
  800864:	0f b6 12             	movzbl (%edx),%edx
  800867:	29 d0                	sub    %edx,%eax
  800869:	eb 05                	jmp    800870 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80086b:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800870:	5b                   	pop    %ebx
  800871:	5d                   	pop    %ebp
  800872:	c3                   	ret    

00800873 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800873:	55                   	push   %ebp
  800874:	89 e5                	mov    %esp,%ebp
  800876:	8b 45 08             	mov    0x8(%ebp),%eax
  800879:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80087c:	eb 05                	jmp    800883 <strchr+0x10>
		if (*s == c)
  80087e:	38 ca                	cmp    %cl,%dl
  800880:	74 0c                	je     80088e <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800882:	40                   	inc    %eax
  800883:	8a 10                	mov    (%eax),%dl
  800885:	84 d2                	test   %dl,%dl
  800887:	75 f5                	jne    80087e <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800889:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80088e:	5d                   	pop    %ebp
  80088f:	c3                   	ret    

00800890 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800890:	55                   	push   %ebp
  800891:	89 e5                	mov    %esp,%ebp
  800893:	8b 45 08             	mov    0x8(%ebp),%eax
  800896:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800899:	eb 05                	jmp    8008a0 <strfind+0x10>
		if (*s == c)
  80089b:	38 ca                	cmp    %cl,%dl
  80089d:	74 07                	je     8008a6 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  80089f:	40                   	inc    %eax
  8008a0:	8a 10                	mov    (%eax),%dl
  8008a2:	84 d2                	test   %dl,%dl
  8008a4:	75 f5                	jne    80089b <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8008a6:	5d                   	pop    %ebp
  8008a7:	c3                   	ret    

008008a8 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8008a8:	55                   	push   %ebp
  8008a9:	89 e5                	mov    %esp,%ebp
  8008ab:	57                   	push   %edi
  8008ac:	56                   	push   %esi
  8008ad:	53                   	push   %ebx
  8008ae:	8b 7d 08             	mov    0x8(%ebp),%edi
  8008b1:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8008b4:	85 c9                	test   %ecx,%ecx
  8008b6:	74 36                	je     8008ee <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8008b8:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8008be:	75 28                	jne    8008e8 <memset+0x40>
  8008c0:	f6 c1 03             	test   $0x3,%cl
  8008c3:	75 23                	jne    8008e8 <memset+0x40>
		c &= 0xFF;
  8008c5:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8008c9:	89 d3                	mov    %edx,%ebx
  8008cb:	c1 e3 08             	shl    $0x8,%ebx
  8008ce:	89 d6                	mov    %edx,%esi
  8008d0:	c1 e6 18             	shl    $0x18,%esi
  8008d3:	89 d0                	mov    %edx,%eax
  8008d5:	c1 e0 10             	shl    $0x10,%eax
  8008d8:	09 f0                	or     %esi,%eax
  8008da:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008dc:	89 d8                	mov    %ebx,%eax
  8008de:	09 d0                	or     %edx,%eax
  8008e0:	c1 e9 02             	shr    $0x2,%ecx
  8008e3:	fc                   	cld    
  8008e4:	f3 ab                	rep stos %eax,%es:(%edi)
  8008e6:	eb 06                	jmp    8008ee <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008e8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008eb:	fc                   	cld    
  8008ec:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008ee:	89 f8                	mov    %edi,%eax
  8008f0:	5b                   	pop    %ebx
  8008f1:	5e                   	pop    %esi
  8008f2:	5f                   	pop    %edi
  8008f3:	5d                   	pop    %ebp
  8008f4:	c3                   	ret    

008008f5 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8008f5:	55                   	push   %ebp
  8008f6:	89 e5                	mov    %esp,%ebp
  8008f8:	57                   	push   %edi
  8008f9:	56                   	push   %esi
  8008fa:	8b 45 08             	mov    0x8(%ebp),%eax
  8008fd:	8b 75 0c             	mov    0xc(%ebp),%esi
  800900:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800903:	39 c6                	cmp    %eax,%esi
  800905:	73 33                	jae    80093a <memmove+0x45>
  800907:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80090a:	39 d0                	cmp    %edx,%eax
  80090c:	73 2c                	jae    80093a <memmove+0x45>
		s += n;
		d += n;
  80090e:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800911:	89 d6                	mov    %edx,%esi
  800913:	09 fe                	or     %edi,%esi
  800915:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80091b:	75 13                	jne    800930 <memmove+0x3b>
  80091d:	f6 c1 03             	test   $0x3,%cl
  800920:	75 0e                	jne    800930 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800922:	83 ef 04             	sub    $0x4,%edi
  800925:	8d 72 fc             	lea    -0x4(%edx),%esi
  800928:	c1 e9 02             	shr    $0x2,%ecx
  80092b:	fd                   	std    
  80092c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80092e:	eb 07                	jmp    800937 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800930:	4f                   	dec    %edi
  800931:	8d 72 ff             	lea    -0x1(%edx),%esi
  800934:	fd                   	std    
  800935:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800937:	fc                   	cld    
  800938:	eb 1d                	jmp    800957 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80093a:	89 f2                	mov    %esi,%edx
  80093c:	09 c2                	or     %eax,%edx
  80093e:	f6 c2 03             	test   $0x3,%dl
  800941:	75 0f                	jne    800952 <memmove+0x5d>
  800943:	f6 c1 03             	test   $0x3,%cl
  800946:	75 0a                	jne    800952 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800948:	c1 e9 02             	shr    $0x2,%ecx
  80094b:	89 c7                	mov    %eax,%edi
  80094d:	fc                   	cld    
  80094e:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800950:	eb 05                	jmp    800957 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800952:	89 c7                	mov    %eax,%edi
  800954:	fc                   	cld    
  800955:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800957:	5e                   	pop    %esi
  800958:	5f                   	pop    %edi
  800959:	5d                   	pop    %ebp
  80095a:	c3                   	ret    

0080095b <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80095b:	55                   	push   %ebp
  80095c:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  80095e:	ff 75 10             	pushl  0x10(%ebp)
  800961:	ff 75 0c             	pushl  0xc(%ebp)
  800964:	ff 75 08             	pushl  0x8(%ebp)
  800967:	e8 89 ff ff ff       	call   8008f5 <memmove>
}
  80096c:	c9                   	leave  
  80096d:	c3                   	ret    

0080096e <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  80096e:	55                   	push   %ebp
  80096f:	89 e5                	mov    %esp,%ebp
  800971:	56                   	push   %esi
  800972:	53                   	push   %ebx
  800973:	8b 45 08             	mov    0x8(%ebp),%eax
  800976:	8b 55 0c             	mov    0xc(%ebp),%edx
  800979:	89 c6                	mov    %eax,%esi
  80097b:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80097e:	eb 14                	jmp    800994 <memcmp+0x26>
		if (*s1 != *s2)
  800980:	8a 08                	mov    (%eax),%cl
  800982:	8a 1a                	mov    (%edx),%bl
  800984:	38 d9                	cmp    %bl,%cl
  800986:	74 0a                	je     800992 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800988:	0f b6 c1             	movzbl %cl,%eax
  80098b:	0f b6 db             	movzbl %bl,%ebx
  80098e:	29 d8                	sub    %ebx,%eax
  800990:	eb 0b                	jmp    80099d <memcmp+0x2f>
		s1++, s2++;
  800992:	40                   	inc    %eax
  800993:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800994:	39 f0                	cmp    %esi,%eax
  800996:	75 e8                	jne    800980 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800998:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80099d:	5b                   	pop    %ebx
  80099e:	5e                   	pop    %esi
  80099f:	5d                   	pop    %ebp
  8009a0:	c3                   	ret    

008009a1 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8009a1:	55                   	push   %ebp
  8009a2:	89 e5                	mov    %esp,%ebp
  8009a4:	53                   	push   %ebx
  8009a5:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8009a8:	89 c1                	mov    %eax,%ecx
  8009aa:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8009ad:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009b1:	eb 08                	jmp    8009bb <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8009b3:	0f b6 10             	movzbl (%eax),%edx
  8009b6:	39 da                	cmp    %ebx,%edx
  8009b8:	74 05                	je     8009bf <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009ba:	40                   	inc    %eax
  8009bb:	39 c8                	cmp    %ecx,%eax
  8009bd:	72 f4                	jb     8009b3 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8009bf:	5b                   	pop    %ebx
  8009c0:	5d                   	pop    %ebp
  8009c1:	c3                   	ret    

008009c2 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8009c2:	55                   	push   %ebp
  8009c3:	89 e5                	mov    %esp,%ebp
  8009c5:	57                   	push   %edi
  8009c6:	56                   	push   %esi
  8009c7:	53                   	push   %ebx
  8009c8:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009cb:	eb 01                	jmp    8009ce <strtol+0xc>
		s++;
  8009cd:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009ce:	8a 01                	mov    (%ecx),%al
  8009d0:	3c 20                	cmp    $0x20,%al
  8009d2:	74 f9                	je     8009cd <strtol+0xb>
  8009d4:	3c 09                	cmp    $0x9,%al
  8009d6:	74 f5                	je     8009cd <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009d8:	3c 2b                	cmp    $0x2b,%al
  8009da:	75 08                	jne    8009e4 <strtol+0x22>
		s++;
  8009dc:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009dd:	bf 00 00 00 00       	mov    $0x0,%edi
  8009e2:	eb 11                	jmp    8009f5 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009e4:	3c 2d                	cmp    $0x2d,%al
  8009e6:	75 08                	jne    8009f0 <strtol+0x2e>
		s++, neg = 1;
  8009e8:	41                   	inc    %ecx
  8009e9:	bf 01 00 00 00       	mov    $0x1,%edi
  8009ee:	eb 05                	jmp    8009f5 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009f0:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009f5:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009f9:	0f 84 87 00 00 00    	je     800a86 <strtol+0xc4>
  8009ff:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800a03:	75 27                	jne    800a2c <strtol+0x6a>
  800a05:	80 39 30             	cmpb   $0x30,(%ecx)
  800a08:	75 22                	jne    800a2c <strtol+0x6a>
  800a0a:	e9 88 00 00 00       	jmp    800a97 <strtol+0xd5>
		s += 2, base = 16;
  800a0f:	83 c1 02             	add    $0x2,%ecx
  800a12:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a19:	eb 11                	jmp    800a2c <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a1b:	41                   	inc    %ecx
  800a1c:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a23:	eb 07                	jmp    800a2c <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a25:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a2c:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a31:	8a 11                	mov    (%ecx),%dl
  800a33:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a36:	80 fb 09             	cmp    $0x9,%bl
  800a39:	77 08                	ja     800a43 <strtol+0x81>
			dig = *s - '0';
  800a3b:	0f be d2             	movsbl %dl,%edx
  800a3e:	83 ea 30             	sub    $0x30,%edx
  800a41:	eb 22                	jmp    800a65 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a43:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a46:	89 f3                	mov    %esi,%ebx
  800a48:	80 fb 19             	cmp    $0x19,%bl
  800a4b:	77 08                	ja     800a55 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a4d:	0f be d2             	movsbl %dl,%edx
  800a50:	83 ea 57             	sub    $0x57,%edx
  800a53:	eb 10                	jmp    800a65 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a55:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a58:	89 f3                	mov    %esi,%ebx
  800a5a:	80 fb 19             	cmp    $0x19,%bl
  800a5d:	77 14                	ja     800a73 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a5f:	0f be d2             	movsbl %dl,%edx
  800a62:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a65:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a68:	7d 09                	jge    800a73 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a6a:	41                   	inc    %ecx
  800a6b:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a6f:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a71:	eb be                	jmp    800a31 <strtol+0x6f>

	if (endptr)
  800a73:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a77:	74 05                	je     800a7e <strtol+0xbc>
		*endptr = (char *) s;
  800a79:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a7c:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a7e:	85 ff                	test   %edi,%edi
  800a80:	74 21                	je     800aa3 <strtol+0xe1>
  800a82:	f7 d8                	neg    %eax
  800a84:	eb 1d                	jmp    800aa3 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a86:	80 39 30             	cmpb   $0x30,(%ecx)
  800a89:	75 9a                	jne    800a25 <strtol+0x63>
  800a8b:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a8f:	0f 84 7a ff ff ff    	je     800a0f <strtol+0x4d>
  800a95:	eb 84                	jmp    800a1b <strtol+0x59>
  800a97:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a9b:	0f 84 6e ff ff ff    	je     800a0f <strtol+0x4d>
  800aa1:	eb 89                	jmp    800a2c <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800aa3:	5b                   	pop    %ebx
  800aa4:	5e                   	pop    %esi
  800aa5:	5f                   	pop    %edi
  800aa6:	5d                   	pop    %ebp
  800aa7:	c3                   	ret    

00800aa8 <__udivdi3>:
  800aa8:	55                   	push   %ebp
  800aa9:	57                   	push   %edi
  800aaa:	56                   	push   %esi
  800aab:	53                   	push   %ebx
  800aac:	83 ec 1c             	sub    $0x1c,%esp
  800aaf:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ab3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800ab7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800abb:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800abf:	89 ca                	mov    %ecx,%edx
  800ac1:	89 f8                	mov    %edi,%eax
  800ac3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800ac7:	85 f6                	test   %esi,%esi
  800ac9:	75 2d                	jne    800af8 <__udivdi3+0x50>
  800acb:	39 cf                	cmp    %ecx,%edi
  800acd:	77 65                	ja     800b34 <__udivdi3+0x8c>
  800acf:	89 fd                	mov    %edi,%ebp
  800ad1:	85 ff                	test   %edi,%edi
  800ad3:	75 0b                	jne    800ae0 <__udivdi3+0x38>
  800ad5:	b8 01 00 00 00       	mov    $0x1,%eax
  800ada:	31 d2                	xor    %edx,%edx
  800adc:	f7 f7                	div    %edi
  800ade:	89 c5                	mov    %eax,%ebp
  800ae0:	31 d2                	xor    %edx,%edx
  800ae2:	89 c8                	mov    %ecx,%eax
  800ae4:	f7 f5                	div    %ebp
  800ae6:	89 c1                	mov    %eax,%ecx
  800ae8:	89 d8                	mov    %ebx,%eax
  800aea:	f7 f5                	div    %ebp
  800aec:	89 cf                	mov    %ecx,%edi
  800aee:	89 fa                	mov    %edi,%edx
  800af0:	83 c4 1c             	add    $0x1c,%esp
  800af3:	5b                   	pop    %ebx
  800af4:	5e                   	pop    %esi
  800af5:	5f                   	pop    %edi
  800af6:	5d                   	pop    %ebp
  800af7:	c3                   	ret    
  800af8:	39 ce                	cmp    %ecx,%esi
  800afa:	77 28                	ja     800b24 <__udivdi3+0x7c>
  800afc:	0f bd fe             	bsr    %esi,%edi
  800aff:	83 f7 1f             	xor    $0x1f,%edi
  800b02:	75 40                	jne    800b44 <__udivdi3+0x9c>
  800b04:	39 ce                	cmp    %ecx,%esi
  800b06:	72 0a                	jb     800b12 <__udivdi3+0x6a>
  800b08:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b0c:	0f 87 9e 00 00 00    	ja     800bb0 <__udivdi3+0x108>
  800b12:	b8 01 00 00 00       	mov    $0x1,%eax
  800b17:	89 fa                	mov    %edi,%edx
  800b19:	83 c4 1c             	add    $0x1c,%esp
  800b1c:	5b                   	pop    %ebx
  800b1d:	5e                   	pop    %esi
  800b1e:	5f                   	pop    %edi
  800b1f:	5d                   	pop    %ebp
  800b20:	c3                   	ret    
  800b21:	8d 76 00             	lea    0x0(%esi),%esi
  800b24:	31 ff                	xor    %edi,%edi
  800b26:	31 c0                	xor    %eax,%eax
  800b28:	89 fa                	mov    %edi,%edx
  800b2a:	83 c4 1c             	add    $0x1c,%esp
  800b2d:	5b                   	pop    %ebx
  800b2e:	5e                   	pop    %esi
  800b2f:	5f                   	pop    %edi
  800b30:	5d                   	pop    %ebp
  800b31:	c3                   	ret    
  800b32:	66 90                	xchg   %ax,%ax
  800b34:	89 d8                	mov    %ebx,%eax
  800b36:	f7 f7                	div    %edi
  800b38:	31 ff                	xor    %edi,%edi
  800b3a:	89 fa                	mov    %edi,%edx
  800b3c:	83 c4 1c             	add    $0x1c,%esp
  800b3f:	5b                   	pop    %ebx
  800b40:	5e                   	pop    %esi
  800b41:	5f                   	pop    %edi
  800b42:	5d                   	pop    %ebp
  800b43:	c3                   	ret    
  800b44:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b49:	89 eb                	mov    %ebp,%ebx
  800b4b:	29 fb                	sub    %edi,%ebx
  800b4d:	89 f9                	mov    %edi,%ecx
  800b4f:	d3 e6                	shl    %cl,%esi
  800b51:	89 c5                	mov    %eax,%ebp
  800b53:	88 d9                	mov    %bl,%cl
  800b55:	d3 ed                	shr    %cl,%ebp
  800b57:	89 e9                	mov    %ebp,%ecx
  800b59:	09 f1                	or     %esi,%ecx
  800b5b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b5f:	89 f9                	mov    %edi,%ecx
  800b61:	d3 e0                	shl    %cl,%eax
  800b63:	89 c5                	mov    %eax,%ebp
  800b65:	89 d6                	mov    %edx,%esi
  800b67:	88 d9                	mov    %bl,%cl
  800b69:	d3 ee                	shr    %cl,%esi
  800b6b:	89 f9                	mov    %edi,%ecx
  800b6d:	d3 e2                	shl    %cl,%edx
  800b6f:	8b 44 24 08          	mov    0x8(%esp),%eax
  800b73:	88 d9                	mov    %bl,%cl
  800b75:	d3 e8                	shr    %cl,%eax
  800b77:	09 c2                	or     %eax,%edx
  800b79:	89 d0                	mov    %edx,%eax
  800b7b:	89 f2                	mov    %esi,%edx
  800b7d:	f7 74 24 0c          	divl   0xc(%esp)
  800b81:	89 d6                	mov    %edx,%esi
  800b83:	89 c3                	mov    %eax,%ebx
  800b85:	f7 e5                	mul    %ebp
  800b87:	39 d6                	cmp    %edx,%esi
  800b89:	72 19                	jb     800ba4 <__udivdi3+0xfc>
  800b8b:	74 0b                	je     800b98 <__udivdi3+0xf0>
  800b8d:	89 d8                	mov    %ebx,%eax
  800b8f:	31 ff                	xor    %edi,%edi
  800b91:	e9 58 ff ff ff       	jmp    800aee <__udivdi3+0x46>
  800b96:	66 90                	xchg   %ax,%ax
  800b98:	8b 54 24 08          	mov    0x8(%esp),%edx
  800b9c:	89 f9                	mov    %edi,%ecx
  800b9e:	d3 e2                	shl    %cl,%edx
  800ba0:	39 c2                	cmp    %eax,%edx
  800ba2:	73 e9                	jae    800b8d <__udivdi3+0xe5>
  800ba4:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800ba7:	31 ff                	xor    %edi,%edi
  800ba9:	e9 40 ff ff ff       	jmp    800aee <__udivdi3+0x46>
  800bae:	66 90                	xchg   %ax,%ax
  800bb0:	31 c0                	xor    %eax,%eax
  800bb2:	e9 37 ff ff ff       	jmp    800aee <__udivdi3+0x46>
  800bb7:	90                   	nop

00800bb8 <__umoddi3>:
  800bb8:	55                   	push   %ebp
  800bb9:	57                   	push   %edi
  800bba:	56                   	push   %esi
  800bbb:	53                   	push   %ebx
  800bbc:	83 ec 1c             	sub    $0x1c,%esp
  800bbf:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bc3:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bc7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800bcb:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800bcf:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800bd3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800bd7:	89 f3                	mov    %esi,%ebx
  800bd9:	89 fa                	mov    %edi,%edx
  800bdb:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800bdf:	89 34 24             	mov    %esi,(%esp)
  800be2:	85 c0                	test   %eax,%eax
  800be4:	75 1a                	jne    800c00 <__umoddi3+0x48>
  800be6:	39 f7                	cmp    %esi,%edi
  800be8:	0f 86 a2 00 00 00    	jbe    800c90 <__umoddi3+0xd8>
  800bee:	89 c8                	mov    %ecx,%eax
  800bf0:	89 f2                	mov    %esi,%edx
  800bf2:	f7 f7                	div    %edi
  800bf4:	89 d0                	mov    %edx,%eax
  800bf6:	31 d2                	xor    %edx,%edx
  800bf8:	83 c4 1c             	add    $0x1c,%esp
  800bfb:	5b                   	pop    %ebx
  800bfc:	5e                   	pop    %esi
  800bfd:	5f                   	pop    %edi
  800bfe:	5d                   	pop    %ebp
  800bff:	c3                   	ret    
  800c00:	39 f0                	cmp    %esi,%eax
  800c02:	0f 87 ac 00 00 00    	ja     800cb4 <__umoddi3+0xfc>
  800c08:	0f bd e8             	bsr    %eax,%ebp
  800c0b:	83 f5 1f             	xor    $0x1f,%ebp
  800c0e:	0f 84 ac 00 00 00    	je     800cc0 <__umoddi3+0x108>
  800c14:	bf 20 00 00 00       	mov    $0x20,%edi
  800c19:	29 ef                	sub    %ebp,%edi
  800c1b:	89 fe                	mov    %edi,%esi
  800c1d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c21:	89 e9                	mov    %ebp,%ecx
  800c23:	d3 e0                	shl    %cl,%eax
  800c25:	89 d7                	mov    %edx,%edi
  800c27:	89 f1                	mov    %esi,%ecx
  800c29:	d3 ef                	shr    %cl,%edi
  800c2b:	09 c7                	or     %eax,%edi
  800c2d:	89 e9                	mov    %ebp,%ecx
  800c2f:	d3 e2                	shl    %cl,%edx
  800c31:	89 14 24             	mov    %edx,(%esp)
  800c34:	89 d8                	mov    %ebx,%eax
  800c36:	d3 e0                	shl    %cl,%eax
  800c38:	89 c2                	mov    %eax,%edx
  800c3a:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c3e:	d3 e0                	shl    %cl,%eax
  800c40:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c44:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c48:	89 f1                	mov    %esi,%ecx
  800c4a:	d3 e8                	shr    %cl,%eax
  800c4c:	09 d0                	or     %edx,%eax
  800c4e:	d3 eb                	shr    %cl,%ebx
  800c50:	89 da                	mov    %ebx,%edx
  800c52:	f7 f7                	div    %edi
  800c54:	89 d3                	mov    %edx,%ebx
  800c56:	f7 24 24             	mull   (%esp)
  800c59:	89 c6                	mov    %eax,%esi
  800c5b:	89 d1                	mov    %edx,%ecx
  800c5d:	39 d3                	cmp    %edx,%ebx
  800c5f:	0f 82 87 00 00 00    	jb     800cec <__umoddi3+0x134>
  800c65:	0f 84 91 00 00 00    	je     800cfc <__umoddi3+0x144>
  800c6b:	8b 54 24 04          	mov    0x4(%esp),%edx
  800c6f:	29 f2                	sub    %esi,%edx
  800c71:	19 cb                	sbb    %ecx,%ebx
  800c73:	89 d8                	mov    %ebx,%eax
  800c75:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800c79:	d3 e0                	shl    %cl,%eax
  800c7b:	89 e9                	mov    %ebp,%ecx
  800c7d:	d3 ea                	shr    %cl,%edx
  800c7f:	09 d0                	or     %edx,%eax
  800c81:	89 e9                	mov    %ebp,%ecx
  800c83:	d3 eb                	shr    %cl,%ebx
  800c85:	89 da                	mov    %ebx,%edx
  800c87:	83 c4 1c             	add    $0x1c,%esp
  800c8a:	5b                   	pop    %ebx
  800c8b:	5e                   	pop    %esi
  800c8c:	5f                   	pop    %edi
  800c8d:	5d                   	pop    %ebp
  800c8e:	c3                   	ret    
  800c8f:	90                   	nop
  800c90:	89 fd                	mov    %edi,%ebp
  800c92:	85 ff                	test   %edi,%edi
  800c94:	75 0b                	jne    800ca1 <__umoddi3+0xe9>
  800c96:	b8 01 00 00 00       	mov    $0x1,%eax
  800c9b:	31 d2                	xor    %edx,%edx
  800c9d:	f7 f7                	div    %edi
  800c9f:	89 c5                	mov    %eax,%ebp
  800ca1:	89 f0                	mov    %esi,%eax
  800ca3:	31 d2                	xor    %edx,%edx
  800ca5:	f7 f5                	div    %ebp
  800ca7:	89 c8                	mov    %ecx,%eax
  800ca9:	f7 f5                	div    %ebp
  800cab:	89 d0                	mov    %edx,%eax
  800cad:	e9 44 ff ff ff       	jmp    800bf6 <__umoddi3+0x3e>
  800cb2:	66 90                	xchg   %ax,%ax
  800cb4:	89 c8                	mov    %ecx,%eax
  800cb6:	89 f2                	mov    %esi,%edx
  800cb8:	83 c4 1c             	add    $0x1c,%esp
  800cbb:	5b                   	pop    %ebx
  800cbc:	5e                   	pop    %esi
  800cbd:	5f                   	pop    %edi
  800cbe:	5d                   	pop    %ebp
  800cbf:	c3                   	ret    
  800cc0:	3b 04 24             	cmp    (%esp),%eax
  800cc3:	72 06                	jb     800ccb <__umoddi3+0x113>
  800cc5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800cc9:	77 0f                	ja     800cda <__umoddi3+0x122>
  800ccb:	89 f2                	mov    %esi,%edx
  800ccd:	29 f9                	sub    %edi,%ecx
  800ccf:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800cd3:	89 14 24             	mov    %edx,(%esp)
  800cd6:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800cda:	8b 44 24 04          	mov    0x4(%esp),%eax
  800cde:	8b 14 24             	mov    (%esp),%edx
  800ce1:	83 c4 1c             	add    $0x1c,%esp
  800ce4:	5b                   	pop    %ebx
  800ce5:	5e                   	pop    %esi
  800ce6:	5f                   	pop    %edi
  800ce7:	5d                   	pop    %ebp
  800ce8:	c3                   	ret    
  800ce9:	8d 76 00             	lea    0x0(%esi),%esi
  800cec:	2b 04 24             	sub    (%esp),%eax
  800cef:	19 fa                	sbb    %edi,%edx
  800cf1:	89 d1                	mov    %edx,%ecx
  800cf3:	89 c6                	mov    %eax,%esi
  800cf5:	e9 71 ff ff ff       	jmp    800c6b <__umoddi3+0xb3>
  800cfa:	66 90                	xchg   %ax,%ax
  800cfc:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d00:	72 ea                	jb     800cec <__umoddi3+0x134>
  800d02:	89 d9                	mov    %ebx,%ecx
  800d04:	e9 62 ff ff ff       	jmp    800c6b <__umoddi3+0xb3>
