
obj/user/badsegment:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 0d 00 00 00       	call   80003e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	// Try to load the kernel's TSS selector into the DS register.
	asm volatile("movw $0x28,%ax; movw %ax,%ds");
  800036:	66 b8 28 00          	mov    $0x28,%ax
  80003a:	8e d8                	mov    %eax,%ds
}
  80003c:	5d                   	pop    %ebp
  80003d:	c3                   	ret    

0080003e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80003e:	55                   	push   %ebp
  80003f:	89 e5                	mov    %esp,%ebp
  800041:	56                   	push   %esi
  800042:	53                   	push   %ebx
  800043:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800046:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800049:	e8 cb 00 00 00       	call   800119 <sys_getenvid>
  80004e:	25 ff 03 00 00       	and    $0x3ff,%eax
  800053:	8d 14 00             	lea    (%eax,%eax,1),%edx
  800056:	01 d0                	add    %edx,%eax
  800058:	c1 e0 05             	shl    $0x5,%eax
  80005b:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800060:	a3 04 10 80 00       	mov    %eax,0x801004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800065:	85 db                	test   %ebx,%ebx
  800067:	7e 07                	jle    800070 <libmain+0x32>
		binaryname = argv[0];
  800069:	8b 06                	mov    (%esi),%eax
  80006b:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  800070:	83 ec 08             	sub    $0x8,%esp
  800073:	56                   	push   %esi
  800074:	53                   	push   %ebx
  800075:	e8 b9 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80007a:	e8 0a 00 00 00       	call   800089 <exit>
}
  80007f:	83 c4 10             	add    $0x10,%esp
  800082:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800085:	5b                   	pop    %ebx
  800086:	5e                   	pop    %esi
  800087:	5d                   	pop    %ebp
  800088:	c3                   	ret    

00800089 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800089:	55                   	push   %ebp
  80008a:	89 e5                	mov    %esp,%ebp
  80008c:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  80008f:	6a 00                	push   $0x0
  800091:	e8 42 00 00 00       	call   8000d8 <sys_env_destroy>
}
  800096:	83 c4 10             	add    $0x10,%esp
  800099:	c9                   	leave  
  80009a:	c3                   	ret    

0080009b <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  80009b:	55                   	push   %ebp
  80009c:	89 e5                	mov    %esp,%ebp
  80009e:	57                   	push   %edi
  80009f:	56                   	push   %esi
  8000a0:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000a1:	b8 00 00 00 00       	mov    $0x0,%eax
  8000a6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000a9:	8b 55 08             	mov    0x8(%ebp),%edx
  8000ac:	89 c3                	mov    %eax,%ebx
  8000ae:	89 c7                	mov    %eax,%edi
  8000b0:	89 c6                	mov    %eax,%esi
  8000b2:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000b4:	5b                   	pop    %ebx
  8000b5:	5e                   	pop    %esi
  8000b6:	5f                   	pop    %edi
  8000b7:	5d                   	pop    %ebp
  8000b8:	c3                   	ret    

008000b9 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000b9:	55                   	push   %ebp
  8000ba:	89 e5                	mov    %esp,%ebp
  8000bc:	57                   	push   %edi
  8000bd:	56                   	push   %esi
  8000be:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000bf:	ba 00 00 00 00       	mov    $0x0,%edx
  8000c4:	b8 01 00 00 00       	mov    $0x1,%eax
  8000c9:	89 d1                	mov    %edx,%ecx
  8000cb:	89 d3                	mov    %edx,%ebx
  8000cd:	89 d7                	mov    %edx,%edi
  8000cf:	89 d6                	mov    %edx,%esi
  8000d1:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000d3:	5b                   	pop    %ebx
  8000d4:	5e                   	pop    %esi
  8000d5:	5f                   	pop    %edi
  8000d6:	5d                   	pop    %ebp
  8000d7:	c3                   	ret    

008000d8 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000d8:	55                   	push   %ebp
  8000d9:	89 e5                	mov    %esp,%ebp
  8000db:	57                   	push   %edi
  8000dc:	56                   	push   %esi
  8000dd:	53                   	push   %ebx
  8000de:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000e1:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000e6:	b8 03 00 00 00       	mov    $0x3,%eax
  8000eb:	8b 55 08             	mov    0x8(%ebp),%edx
  8000ee:	89 cb                	mov    %ecx,%ebx
  8000f0:	89 cf                	mov    %ecx,%edi
  8000f2:	89 ce                	mov    %ecx,%esi
  8000f4:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8000f6:	85 c0                	test   %eax,%eax
  8000f8:	7e 17                	jle    800111 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  8000fa:	83 ec 0c             	sub    $0xc,%esp
  8000fd:	50                   	push   %eax
  8000fe:	6a 03                	push   $0x3
  800100:	68 0a 0d 80 00       	push   $0x800d0a
  800105:	6a 23                	push   $0x23
  800107:	68 27 0d 80 00       	push   $0x800d27
  80010c:	e8 27 00 00 00       	call   800138 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800111:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800114:	5b                   	pop    %ebx
  800115:	5e                   	pop    %esi
  800116:	5f                   	pop    %edi
  800117:	5d                   	pop    %ebp
  800118:	c3                   	ret    

00800119 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800119:	55                   	push   %ebp
  80011a:	89 e5                	mov    %esp,%ebp
  80011c:	57                   	push   %edi
  80011d:	56                   	push   %esi
  80011e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80011f:	ba 00 00 00 00       	mov    $0x0,%edx
  800124:	b8 02 00 00 00       	mov    $0x2,%eax
  800129:	89 d1                	mov    %edx,%ecx
  80012b:	89 d3                	mov    %edx,%ebx
  80012d:	89 d7                	mov    %edx,%edi
  80012f:	89 d6                	mov    %edx,%esi
  800131:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800133:	5b                   	pop    %ebx
  800134:	5e                   	pop    %esi
  800135:	5f                   	pop    %edi
  800136:	5d                   	pop    %ebp
  800137:	c3                   	ret    

00800138 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800138:	55                   	push   %ebp
  800139:	89 e5                	mov    %esp,%ebp
  80013b:	56                   	push   %esi
  80013c:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80013d:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800140:	8b 35 00 10 80 00    	mov    0x801000,%esi
  800146:	e8 ce ff ff ff       	call   800119 <sys_getenvid>
  80014b:	83 ec 0c             	sub    $0xc,%esp
  80014e:	ff 75 0c             	pushl  0xc(%ebp)
  800151:	ff 75 08             	pushl  0x8(%ebp)
  800154:	56                   	push   %esi
  800155:	50                   	push   %eax
  800156:	68 38 0d 80 00       	push   $0x800d38
  80015b:	e8 b0 00 00 00       	call   800210 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800160:	83 c4 18             	add    $0x18,%esp
  800163:	53                   	push   %ebx
  800164:	ff 75 10             	pushl  0x10(%ebp)
  800167:	e8 53 00 00 00       	call   8001bf <vcprintf>
	cprintf("\n");
  80016c:	c7 04 24 5c 0d 80 00 	movl   $0x800d5c,(%esp)
  800173:	e8 98 00 00 00       	call   800210 <cprintf>
  800178:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  80017b:	cc                   	int3   
  80017c:	eb fd                	jmp    80017b <_panic+0x43>

0080017e <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80017e:	55                   	push   %ebp
  80017f:	89 e5                	mov    %esp,%ebp
  800181:	53                   	push   %ebx
  800182:	83 ec 04             	sub    $0x4,%esp
  800185:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800188:	8b 13                	mov    (%ebx),%edx
  80018a:	8d 42 01             	lea    0x1(%edx),%eax
  80018d:	89 03                	mov    %eax,(%ebx)
  80018f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800192:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800196:	3d ff 00 00 00       	cmp    $0xff,%eax
  80019b:	75 1a                	jne    8001b7 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  80019d:	83 ec 08             	sub    $0x8,%esp
  8001a0:	68 ff 00 00 00       	push   $0xff
  8001a5:	8d 43 08             	lea    0x8(%ebx),%eax
  8001a8:	50                   	push   %eax
  8001a9:	e8 ed fe ff ff       	call   80009b <sys_cputs>
		b->idx = 0;
  8001ae:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8001b4:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8001b7:	ff 43 04             	incl   0x4(%ebx)
}
  8001ba:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001bd:	c9                   	leave  
  8001be:	c3                   	ret    

008001bf <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8001bf:	55                   	push   %ebp
  8001c0:	89 e5                	mov    %esp,%ebp
  8001c2:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001c8:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001cf:	00 00 00 
	b.cnt = 0;
  8001d2:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001d9:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001dc:	ff 75 0c             	pushl  0xc(%ebp)
  8001df:	ff 75 08             	pushl  0x8(%ebp)
  8001e2:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001e8:	50                   	push   %eax
  8001e9:	68 7e 01 80 00       	push   $0x80017e
  8001ee:	e8 51 01 00 00       	call   800344 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001f3:	83 c4 08             	add    $0x8,%esp
  8001f6:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8001fc:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800202:	50                   	push   %eax
  800203:	e8 93 fe ff ff       	call   80009b <sys_cputs>

	return b.cnt;
}
  800208:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80020e:	c9                   	leave  
  80020f:	c3                   	ret    

00800210 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800210:	55                   	push   %ebp
  800211:	89 e5                	mov    %esp,%ebp
  800213:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800216:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800219:	50                   	push   %eax
  80021a:	ff 75 08             	pushl  0x8(%ebp)
  80021d:	e8 9d ff ff ff       	call   8001bf <vcprintf>
	va_end(ap);

	return cnt;
}
  800222:	c9                   	leave  
  800223:	c3                   	ret    

00800224 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800224:	55                   	push   %ebp
  800225:	89 e5                	mov    %esp,%ebp
  800227:	57                   	push   %edi
  800228:	56                   	push   %esi
  800229:	53                   	push   %ebx
  80022a:	83 ec 1c             	sub    $0x1c,%esp
  80022d:	89 c7                	mov    %eax,%edi
  80022f:	89 d6                	mov    %edx,%esi
  800231:	8b 45 08             	mov    0x8(%ebp),%eax
  800234:	8b 55 0c             	mov    0xc(%ebp),%edx
  800237:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80023a:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80023d:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800240:	bb 00 00 00 00       	mov    $0x0,%ebx
  800245:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800248:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80024b:	39 d3                	cmp    %edx,%ebx
  80024d:	72 05                	jb     800254 <printnum+0x30>
  80024f:	39 45 10             	cmp    %eax,0x10(%ebp)
  800252:	77 45                	ja     800299 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800254:	83 ec 0c             	sub    $0xc,%esp
  800257:	ff 75 18             	pushl  0x18(%ebp)
  80025a:	8b 45 14             	mov    0x14(%ebp),%eax
  80025d:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800260:	53                   	push   %ebx
  800261:	ff 75 10             	pushl  0x10(%ebp)
  800264:	83 ec 08             	sub    $0x8,%esp
  800267:	ff 75 e4             	pushl  -0x1c(%ebp)
  80026a:	ff 75 e0             	pushl  -0x20(%ebp)
  80026d:	ff 75 dc             	pushl  -0x24(%ebp)
  800270:	ff 75 d8             	pushl  -0x28(%ebp)
  800273:	e8 24 08 00 00       	call   800a9c <__udivdi3>
  800278:	83 c4 18             	add    $0x18,%esp
  80027b:	52                   	push   %edx
  80027c:	50                   	push   %eax
  80027d:	89 f2                	mov    %esi,%edx
  80027f:	89 f8                	mov    %edi,%eax
  800281:	e8 9e ff ff ff       	call   800224 <printnum>
  800286:	83 c4 20             	add    $0x20,%esp
  800289:	eb 16                	jmp    8002a1 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80028b:	83 ec 08             	sub    $0x8,%esp
  80028e:	56                   	push   %esi
  80028f:	ff 75 18             	pushl  0x18(%ebp)
  800292:	ff d7                	call   *%edi
  800294:	83 c4 10             	add    $0x10,%esp
  800297:	eb 03                	jmp    80029c <printnum+0x78>
  800299:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80029c:	4b                   	dec    %ebx
  80029d:	85 db                	test   %ebx,%ebx
  80029f:	7f ea                	jg     80028b <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8002a1:	83 ec 08             	sub    $0x8,%esp
  8002a4:	56                   	push   %esi
  8002a5:	83 ec 04             	sub    $0x4,%esp
  8002a8:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002ab:	ff 75 e0             	pushl  -0x20(%ebp)
  8002ae:	ff 75 dc             	pushl  -0x24(%ebp)
  8002b1:	ff 75 d8             	pushl  -0x28(%ebp)
  8002b4:	e8 f3 08 00 00       	call   800bac <__umoddi3>
  8002b9:	83 c4 14             	add    $0x14,%esp
  8002bc:	0f be 80 5e 0d 80 00 	movsbl 0x800d5e(%eax),%eax
  8002c3:	50                   	push   %eax
  8002c4:	ff d7                	call   *%edi
}
  8002c6:	83 c4 10             	add    $0x10,%esp
  8002c9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002cc:	5b                   	pop    %ebx
  8002cd:	5e                   	pop    %esi
  8002ce:	5f                   	pop    %edi
  8002cf:	5d                   	pop    %ebp
  8002d0:	c3                   	ret    

008002d1 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002d1:	55                   	push   %ebp
  8002d2:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002d4:	83 fa 01             	cmp    $0x1,%edx
  8002d7:	7e 0e                	jle    8002e7 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002d9:	8b 10                	mov    (%eax),%edx
  8002db:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002de:	89 08                	mov    %ecx,(%eax)
  8002e0:	8b 02                	mov    (%edx),%eax
  8002e2:	8b 52 04             	mov    0x4(%edx),%edx
  8002e5:	eb 22                	jmp    800309 <getuint+0x38>
	else if (lflag)
  8002e7:	85 d2                	test   %edx,%edx
  8002e9:	74 10                	je     8002fb <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8002eb:	8b 10                	mov    (%eax),%edx
  8002ed:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002f0:	89 08                	mov    %ecx,(%eax)
  8002f2:	8b 02                	mov    (%edx),%eax
  8002f4:	ba 00 00 00 00       	mov    $0x0,%edx
  8002f9:	eb 0e                	jmp    800309 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8002fb:	8b 10                	mov    (%eax),%edx
  8002fd:	8d 4a 04             	lea    0x4(%edx),%ecx
  800300:	89 08                	mov    %ecx,(%eax)
  800302:	8b 02                	mov    (%edx),%eax
  800304:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800309:	5d                   	pop    %ebp
  80030a:	c3                   	ret    

0080030b <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80030b:	55                   	push   %ebp
  80030c:	89 e5                	mov    %esp,%ebp
  80030e:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800311:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800314:	8b 10                	mov    (%eax),%edx
  800316:	3b 50 04             	cmp    0x4(%eax),%edx
  800319:	73 0a                	jae    800325 <sprintputch+0x1a>
		*b->buf++ = ch;
  80031b:	8d 4a 01             	lea    0x1(%edx),%ecx
  80031e:	89 08                	mov    %ecx,(%eax)
  800320:	8b 45 08             	mov    0x8(%ebp),%eax
  800323:	88 02                	mov    %al,(%edx)
}
  800325:	5d                   	pop    %ebp
  800326:	c3                   	ret    

00800327 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800327:	55                   	push   %ebp
  800328:	89 e5                	mov    %esp,%ebp
  80032a:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80032d:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800330:	50                   	push   %eax
  800331:	ff 75 10             	pushl  0x10(%ebp)
  800334:	ff 75 0c             	pushl  0xc(%ebp)
  800337:	ff 75 08             	pushl  0x8(%ebp)
  80033a:	e8 05 00 00 00       	call   800344 <vprintfmt>
	va_end(ap);
}
  80033f:	83 c4 10             	add    $0x10,%esp
  800342:	c9                   	leave  
  800343:	c3                   	ret    

00800344 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800344:	55                   	push   %ebp
  800345:	89 e5                	mov    %esp,%ebp
  800347:	57                   	push   %edi
  800348:	56                   	push   %esi
  800349:	53                   	push   %ebx
  80034a:	83 ec 2c             	sub    $0x2c,%esp
  80034d:	8b 75 08             	mov    0x8(%ebp),%esi
  800350:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800353:	8b 7d 10             	mov    0x10(%ebp),%edi
  800356:	eb 12                	jmp    80036a <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800358:	85 c0                	test   %eax,%eax
  80035a:	0f 84 68 03 00 00    	je     8006c8 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800360:	83 ec 08             	sub    $0x8,%esp
  800363:	53                   	push   %ebx
  800364:	50                   	push   %eax
  800365:	ff d6                	call   *%esi
  800367:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80036a:	47                   	inc    %edi
  80036b:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80036f:	83 f8 25             	cmp    $0x25,%eax
  800372:	75 e4                	jne    800358 <vprintfmt+0x14>
  800374:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800378:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80037f:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800386:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80038d:	ba 00 00 00 00       	mov    $0x0,%edx
  800392:	eb 07                	jmp    80039b <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800394:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800397:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80039b:	8d 47 01             	lea    0x1(%edi),%eax
  80039e:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8003a1:	0f b6 0f             	movzbl (%edi),%ecx
  8003a4:	8a 07                	mov    (%edi),%al
  8003a6:	83 e8 23             	sub    $0x23,%eax
  8003a9:	3c 55                	cmp    $0x55,%al
  8003ab:	0f 87 fe 02 00 00    	ja     8006af <vprintfmt+0x36b>
  8003b1:	0f b6 c0             	movzbl %al,%eax
  8003b4:	ff 24 85 ec 0d 80 00 	jmp    *0x800dec(,%eax,4)
  8003bb:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003be:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003c2:	eb d7                	jmp    80039b <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003c4:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003c7:	b8 00 00 00 00       	mov    $0x0,%eax
  8003cc:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003cf:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003d2:	01 c0                	add    %eax,%eax
  8003d4:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003d8:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003db:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003de:	83 fa 09             	cmp    $0x9,%edx
  8003e1:	77 34                	ja     800417 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003e3:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003e4:	eb e9                	jmp    8003cf <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003e6:	8b 45 14             	mov    0x14(%ebp),%eax
  8003e9:	8d 48 04             	lea    0x4(%eax),%ecx
  8003ec:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8003ef:	8b 00                	mov    (%eax),%eax
  8003f1:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f4:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8003f7:	eb 24                	jmp    80041d <vprintfmt+0xd9>
  8003f9:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003fd:	79 07                	jns    800406 <vprintfmt+0xc2>
  8003ff:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800406:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800409:	eb 90                	jmp    80039b <vprintfmt+0x57>
  80040b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80040e:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800415:	eb 84                	jmp    80039b <vprintfmt+0x57>
  800417:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80041a:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80041d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800421:	0f 89 74 ff ff ff    	jns    80039b <vprintfmt+0x57>
				width = precision, precision = -1;
  800427:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80042a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80042d:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800434:	e9 62 ff ff ff       	jmp    80039b <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800439:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80043a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80043d:	e9 59 ff ff ff       	jmp    80039b <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800442:	8b 45 14             	mov    0x14(%ebp),%eax
  800445:	8d 50 04             	lea    0x4(%eax),%edx
  800448:	89 55 14             	mov    %edx,0x14(%ebp)
  80044b:	83 ec 08             	sub    $0x8,%esp
  80044e:	53                   	push   %ebx
  80044f:	ff 30                	pushl  (%eax)
  800451:	ff d6                	call   *%esi
			break;
  800453:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800456:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800459:	e9 0c ff ff ff       	jmp    80036a <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80045e:	8b 45 14             	mov    0x14(%ebp),%eax
  800461:	8d 50 04             	lea    0x4(%eax),%edx
  800464:	89 55 14             	mov    %edx,0x14(%ebp)
  800467:	8b 00                	mov    (%eax),%eax
  800469:	85 c0                	test   %eax,%eax
  80046b:	79 02                	jns    80046f <vprintfmt+0x12b>
  80046d:	f7 d8                	neg    %eax
  80046f:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800471:	83 f8 06             	cmp    $0x6,%eax
  800474:	7f 0b                	jg     800481 <vprintfmt+0x13d>
  800476:	8b 04 85 44 0f 80 00 	mov    0x800f44(,%eax,4),%eax
  80047d:	85 c0                	test   %eax,%eax
  80047f:	75 18                	jne    800499 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800481:	52                   	push   %edx
  800482:	68 76 0d 80 00       	push   $0x800d76
  800487:	53                   	push   %ebx
  800488:	56                   	push   %esi
  800489:	e8 99 fe ff ff       	call   800327 <printfmt>
  80048e:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800491:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800494:	e9 d1 fe ff ff       	jmp    80036a <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800499:	50                   	push   %eax
  80049a:	68 7f 0d 80 00       	push   $0x800d7f
  80049f:	53                   	push   %ebx
  8004a0:	56                   	push   %esi
  8004a1:	e8 81 fe ff ff       	call   800327 <printfmt>
  8004a6:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004a9:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004ac:	e9 b9 fe ff ff       	jmp    80036a <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004b1:	8b 45 14             	mov    0x14(%ebp),%eax
  8004b4:	8d 50 04             	lea    0x4(%eax),%edx
  8004b7:	89 55 14             	mov    %edx,0x14(%ebp)
  8004ba:	8b 38                	mov    (%eax),%edi
  8004bc:	85 ff                	test   %edi,%edi
  8004be:	75 05                	jne    8004c5 <vprintfmt+0x181>
				p = "(null)";
  8004c0:	bf 6f 0d 80 00       	mov    $0x800d6f,%edi
			if (width > 0 && padc != '-')
  8004c5:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004c9:	0f 8e 90 00 00 00    	jle    80055f <vprintfmt+0x21b>
  8004cf:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004d3:	0f 84 8e 00 00 00    	je     800567 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004d9:	83 ec 08             	sub    $0x8,%esp
  8004dc:	ff 75 d0             	pushl  -0x30(%ebp)
  8004df:	57                   	push   %edi
  8004e0:	e8 70 02 00 00       	call   800755 <strnlen>
  8004e5:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8004e8:	29 c1                	sub    %eax,%ecx
  8004ea:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8004ed:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8004f0:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8004f4:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004f7:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8004fa:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004fc:	eb 0d                	jmp    80050b <vprintfmt+0x1c7>
					putch(padc, putdat);
  8004fe:	83 ec 08             	sub    $0x8,%esp
  800501:	53                   	push   %ebx
  800502:	ff 75 e4             	pushl  -0x1c(%ebp)
  800505:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800507:	4f                   	dec    %edi
  800508:	83 c4 10             	add    $0x10,%esp
  80050b:	85 ff                	test   %edi,%edi
  80050d:	7f ef                	jg     8004fe <vprintfmt+0x1ba>
  80050f:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800512:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800515:	89 c8                	mov    %ecx,%eax
  800517:	85 c9                	test   %ecx,%ecx
  800519:	79 05                	jns    800520 <vprintfmt+0x1dc>
  80051b:	b8 00 00 00 00       	mov    $0x0,%eax
  800520:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800523:	29 c1                	sub    %eax,%ecx
  800525:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800528:	89 75 08             	mov    %esi,0x8(%ebp)
  80052b:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80052e:	eb 3d                	jmp    80056d <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800530:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800534:	74 19                	je     80054f <vprintfmt+0x20b>
  800536:	0f be c0             	movsbl %al,%eax
  800539:	83 e8 20             	sub    $0x20,%eax
  80053c:	83 f8 5e             	cmp    $0x5e,%eax
  80053f:	76 0e                	jbe    80054f <vprintfmt+0x20b>
					putch('?', putdat);
  800541:	83 ec 08             	sub    $0x8,%esp
  800544:	53                   	push   %ebx
  800545:	6a 3f                	push   $0x3f
  800547:	ff 55 08             	call   *0x8(%ebp)
  80054a:	83 c4 10             	add    $0x10,%esp
  80054d:	eb 0b                	jmp    80055a <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80054f:	83 ec 08             	sub    $0x8,%esp
  800552:	53                   	push   %ebx
  800553:	52                   	push   %edx
  800554:	ff 55 08             	call   *0x8(%ebp)
  800557:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80055a:	ff 4d e4             	decl   -0x1c(%ebp)
  80055d:	eb 0e                	jmp    80056d <vprintfmt+0x229>
  80055f:	89 75 08             	mov    %esi,0x8(%ebp)
  800562:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800565:	eb 06                	jmp    80056d <vprintfmt+0x229>
  800567:	89 75 08             	mov    %esi,0x8(%ebp)
  80056a:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80056d:	47                   	inc    %edi
  80056e:	8a 47 ff             	mov    -0x1(%edi),%al
  800571:	0f be d0             	movsbl %al,%edx
  800574:	85 d2                	test   %edx,%edx
  800576:	74 1d                	je     800595 <vprintfmt+0x251>
  800578:	85 f6                	test   %esi,%esi
  80057a:	78 b4                	js     800530 <vprintfmt+0x1ec>
  80057c:	4e                   	dec    %esi
  80057d:	79 b1                	jns    800530 <vprintfmt+0x1ec>
  80057f:	8b 75 08             	mov    0x8(%ebp),%esi
  800582:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800585:	eb 14                	jmp    80059b <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800587:	83 ec 08             	sub    $0x8,%esp
  80058a:	53                   	push   %ebx
  80058b:	6a 20                	push   $0x20
  80058d:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80058f:	4f                   	dec    %edi
  800590:	83 c4 10             	add    $0x10,%esp
  800593:	eb 06                	jmp    80059b <vprintfmt+0x257>
  800595:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800598:	8b 75 08             	mov    0x8(%ebp),%esi
  80059b:	85 ff                	test   %edi,%edi
  80059d:	7f e8                	jg     800587 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80059f:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005a2:	e9 c3 fd ff ff       	jmp    80036a <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8005a7:	83 fa 01             	cmp    $0x1,%edx
  8005aa:	7e 16                	jle    8005c2 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8005ac:	8b 45 14             	mov    0x14(%ebp),%eax
  8005af:	8d 50 08             	lea    0x8(%eax),%edx
  8005b2:	89 55 14             	mov    %edx,0x14(%ebp)
  8005b5:	8b 50 04             	mov    0x4(%eax),%edx
  8005b8:	8b 00                	mov    (%eax),%eax
  8005ba:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005bd:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8005c0:	eb 32                	jmp    8005f4 <vprintfmt+0x2b0>
	else if (lflag)
  8005c2:	85 d2                	test   %edx,%edx
  8005c4:	74 18                	je     8005de <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005c6:	8b 45 14             	mov    0x14(%ebp),%eax
  8005c9:	8d 50 04             	lea    0x4(%eax),%edx
  8005cc:	89 55 14             	mov    %edx,0x14(%ebp)
  8005cf:	8b 00                	mov    (%eax),%eax
  8005d1:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005d4:	89 c1                	mov    %eax,%ecx
  8005d6:	c1 f9 1f             	sar    $0x1f,%ecx
  8005d9:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005dc:	eb 16                	jmp    8005f4 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005de:	8b 45 14             	mov    0x14(%ebp),%eax
  8005e1:	8d 50 04             	lea    0x4(%eax),%edx
  8005e4:	89 55 14             	mov    %edx,0x14(%ebp)
  8005e7:	8b 00                	mov    (%eax),%eax
  8005e9:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005ec:	89 c1                	mov    %eax,%ecx
  8005ee:	c1 f9 1f             	sar    $0x1f,%ecx
  8005f1:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005f4:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005f7:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8005fa:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8005fe:	79 76                	jns    800676 <vprintfmt+0x332>
				putch('-', putdat);
  800600:	83 ec 08             	sub    $0x8,%esp
  800603:	53                   	push   %ebx
  800604:	6a 2d                	push   $0x2d
  800606:	ff d6                	call   *%esi
				num = -(long long) num;
  800608:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80060b:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80060e:	f7 d8                	neg    %eax
  800610:	83 d2 00             	adc    $0x0,%edx
  800613:	f7 da                	neg    %edx
  800615:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800618:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80061d:	eb 5c                	jmp    80067b <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80061f:	8d 45 14             	lea    0x14(%ebp),%eax
  800622:	e8 aa fc ff ff       	call   8002d1 <getuint>
			base = 10;
  800627:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80062c:	eb 4d                	jmp    80067b <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80062e:	8d 45 14             	lea    0x14(%ebp),%eax
  800631:	e8 9b fc ff ff       	call   8002d1 <getuint>
			base = 8;
  800636:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80063b:	eb 3e                	jmp    80067b <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80063d:	83 ec 08             	sub    $0x8,%esp
  800640:	53                   	push   %ebx
  800641:	6a 30                	push   $0x30
  800643:	ff d6                	call   *%esi
			putch('x', putdat);
  800645:	83 c4 08             	add    $0x8,%esp
  800648:	53                   	push   %ebx
  800649:	6a 78                	push   $0x78
  80064b:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80064d:	8b 45 14             	mov    0x14(%ebp),%eax
  800650:	8d 50 04             	lea    0x4(%eax),%edx
  800653:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800656:	8b 00                	mov    (%eax),%eax
  800658:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80065d:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800660:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800665:	eb 14                	jmp    80067b <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800667:	8d 45 14             	lea    0x14(%ebp),%eax
  80066a:	e8 62 fc ff ff       	call   8002d1 <getuint>
			base = 16;
  80066f:	b9 10 00 00 00       	mov    $0x10,%ecx
  800674:	eb 05                	jmp    80067b <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800676:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80067b:	83 ec 0c             	sub    $0xc,%esp
  80067e:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800682:	57                   	push   %edi
  800683:	ff 75 e4             	pushl  -0x1c(%ebp)
  800686:	51                   	push   %ecx
  800687:	52                   	push   %edx
  800688:	50                   	push   %eax
  800689:	89 da                	mov    %ebx,%edx
  80068b:	89 f0                	mov    %esi,%eax
  80068d:	e8 92 fb ff ff       	call   800224 <printnum>
			break;
  800692:	83 c4 20             	add    $0x20,%esp
  800695:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800698:	e9 cd fc ff ff       	jmp    80036a <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80069d:	83 ec 08             	sub    $0x8,%esp
  8006a0:	53                   	push   %ebx
  8006a1:	51                   	push   %ecx
  8006a2:	ff d6                	call   *%esi
			break;
  8006a4:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006a7:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8006aa:	e9 bb fc ff ff       	jmp    80036a <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006af:	83 ec 08             	sub    $0x8,%esp
  8006b2:	53                   	push   %ebx
  8006b3:	6a 25                	push   $0x25
  8006b5:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006b7:	83 c4 10             	add    $0x10,%esp
  8006ba:	eb 01                	jmp    8006bd <vprintfmt+0x379>
  8006bc:	4f                   	dec    %edi
  8006bd:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006c1:	75 f9                	jne    8006bc <vprintfmt+0x378>
  8006c3:	e9 a2 fc ff ff       	jmp    80036a <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006c8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006cb:	5b                   	pop    %ebx
  8006cc:	5e                   	pop    %esi
  8006cd:	5f                   	pop    %edi
  8006ce:	5d                   	pop    %ebp
  8006cf:	c3                   	ret    

008006d0 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006d0:	55                   	push   %ebp
  8006d1:	89 e5                	mov    %esp,%ebp
  8006d3:	83 ec 18             	sub    $0x18,%esp
  8006d6:	8b 45 08             	mov    0x8(%ebp),%eax
  8006d9:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006dc:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006df:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006e3:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8006e6:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8006ed:	85 c0                	test   %eax,%eax
  8006ef:	74 26                	je     800717 <vsnprintf+0x47>
  8006f1:	85 d2                	test   %edx,%edx
  8006f3:	7e 29                	jle    80071e <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8006f5:	ff 75 14             	pushl  0x14(%ebp)
  8006f8:	ff 75 10             	pushl  0x10(%ebp)
  8006fb:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8006fe:	50                   	push   %eax
  8006ff:	68 0b 03 80 00       	push   $0x80030b
  800704:	e8 3b fc ff ff       	call   800344 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800709:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80070c:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80070f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800712:	83 c4 10             	add    $0x10,%esp
  800715:	eb 0c                	jmp    800723 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800717:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80071c:	eb 05                	jmp    800723 <vsnprintf+0x53>
  80071e:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800723:	c9                   	leave  
  800724:	c3                   	ret    

00800725 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800725:	55                   	push   %ebp
  800726:	89 e5                	mov    %esp,%ebp
  800728:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80072b:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80072e:	50                   	push   %eax
  80072f:	ff 75 10             	pushl  0x10(%ebp)
  800732:	ff 75 0c             	pushl  0xc(%ebp)
  800735:	ff 75 08             	pushl  0x8(%ebp)
  800738:	e8 93 ff ff ff       	call   8006d0 <vsnprintf>
	va_end(ap);

	return rc;
}
  80073d:	c9                   	leave  
  80073e:	c3                   	ret    

0080073f <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80073f:	55                   	push   %ebp
  800740:	89 e5                	mov    %esp,%ebp
  800742:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800745:	b8 00 00 00 00       	mov    $0x0,%eax
  80074a:	eb 01                	jmp    80074d <strlen+0xe>
		n++;
  80074c:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80074d:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800751:	75 f9                	jne    80074c <strlen+0xd>
		n++;
	return n;
}
  800753:	5d                   	pop    %ebp
  800754:	c3                   	ret    

00800755 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800755:	55                   	push   %ebp
  800756:	89 e5                	mov    %esp,%ebp
  800758:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80075b:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80075e:	ba 00 00 00 00       	mov    $0x0,%edx
  800763:	eb 01                	jmp    800766 <strnlen+0x11>
		n++;
  800765:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800766:	39 c2                	cmp    %eax,%edx
  800768:	74 08                	je     800772 <strnlen+0x1d>
  80076a:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80076e:	75 f5                	jne    800765 <strnlen+0x10>
  800770:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800772:	5d                   	pop    %ebp
  800773:	c3                   	ret    

00800774 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800774:	55                   	push   %ebp
  800775:	89 e5                	mov    %esp,%ebp
  800777:	53                   	push   %ebx
  800778:	8b 45 08             	mov    0x8(%ebp),%eax
  80077b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80077e:	89 c2                	mov    %eax,%edx
  800780:	42                   	inc    %edx
  800781:	41                   	inc    %ecx
  800782:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800785:	88 5a ff             	mov    %bl,-0x1(%edx)
  800788:	84 db                	test   %bl,%bl
  80078a:	75 f4                	jne    800780 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80078c:	5b                   	pop    %ebx
  80078d:	5d                   	pop    %ebp
  80078e:	c3                   	ret    

0080078f <strcat>:

char *
strcat(char *dst, const char *src)
{
  80078f:	55                   	push   %ebp
  800790:	89 e5                	mov    %esp,%ebp
  800792:	53                   	push   %ebx
  800793:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800796:	53                   	push   %ebx
  800797:	e8 a3 ff ff ff       	call   80073f <strlen>
  80079c:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80079f:	ff 75 0c             	pushl  0xc(%ebp)
  8007a2:	01 d8                	add    %ebx,%eax
  8007a4:	50                   	push   %eax
  8007a5:	e8 ca ff ff ff       	call   800774 <strcpy>
	return dst;
}
  8007aa:	89 d8                	mov    %ebx,%eax
  8007ac:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8007af:	c9                   	leave  
  8007b0:	c3                   	ret    

008007b1 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8007b1:	55                   	push   %ebp
  8007b2:	89 e5                	mov    %esp,%ebp
  8007b4:	56                   	push   %esi
  8007b5:	53                   	push   %ebx
  8007b6:	8b 75 08             	mov    0x8(%ebp),%esi
  8007b9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007bc:	89 f3                	mov    %esi,%ebx
  8007be:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007c1:	89 f2                	mov    %esi,%edx
  8007c3:	eb 0c                	jmp    8007d1 <strncpy+0x20>
		*dst++ = *src;
  8007c5:	42                   	inc    %edx
  8007c6:	8a 01                	mov    (%ecx),%al
  8007c8:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007cb:	80 39 01             	cmpb   $0x1,(%ecx)
  8007ce:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007d1:	39 da                	cmp    %ebx,%edx
  8007d3:	75 f0                	jne    8007c5 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007d5:	89 f0                	mov    %esi,%eax
  8007d7:	5b                   	pop    %ebx
  8007d8:	5e                   	pop    %esi
  8007d9:	5d                   	pop    %ebp
  8007da:	c3                   	ret    

008007db <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007db:	55                   	push   %ebp
  8007dc:	89 e5                	mov    %esp,%ebp
  8007de:	56                   	push   %esi
  8007df:	53                   	push   %ebx
  8007e0:	8b 75 08             	mov    0x8(%ebp),%esi
  8007e3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007e6:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8007e9:	85 c0                	test   %eax,%eax
  8007eb:	74 1e                	je     80080b <strlcpy+0x30>
  8007ed:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8007f1:	89 f2                	mov    %esi,%edx
  8007f3:	eb 05                	jmp    8007fa <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8007f5:	42                   	inc    %edx
  8007f6:	41                   	inc    %ecx
  8007f7:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8007fa:	39 c2                	cmp    %eax,%edx
  8007fc:	74 08                	je     800806 <strlcpy+0x2b>
  8007fe:	8a 19                	mov    (%ecx),%bl
  800800:	84 db                	test   %bl,%bl
  800802:	75 f1                	jne    8007f5 <strlcpy+0x1a>
  800804:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800806:	c6 00 00             	movb   $0x0,(%eax)
  800809:	eb 02                	jmp    80080d <strlcpy+0x32>
  80080b:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  80080d:	29 f0                	sub    %esi,%eax
}
  80080f:	5b                   	pop    %ebx
  800810:	5e                   	pop    %esi
  800811:	5d                   	pop    %ebp
  800812:	c3                   	ret    

00800813 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800813:	55                   	push   %ebp
  800814:	89 e5                	mov    %esp,%ebp
  800816:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800819:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80081c:	eb 02                	jmp    800820 <strcmp+0xd>
		p++, q++;
  80081e:	41                   	inc    %ecx
  80081f:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800820:	8a 01                	mov    (%ecx),%al
  800822:	84 c0                	test   %al,%al
  800824:	74 04                	je     80082a <strcmp+0x17>
  800826:	3a 02                	cmp    (%edx),%al
  800828:	74 f4                	je     80081e <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80082a:	0f b6 c0             	movzbl %al,%eax
  80082d:	0f b6 12             	movzbl (%edx),%edx
  800830:	29 d0                	sub    %edx,%eax
}
  800832:	5d                   	pop    %ebp
  800833:	c3                   	ret    

00800834 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800834:	55                   	push   %ebp
  800835:	89 e5                	mov    %esp,%ebp
  800837:	53                   	push   %ebx
  800838:	8b 45 08             	mov    0x8(%ebp),%eax
  80083b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80083e:	89 c3                	mov    %eax,%ebx
  800840:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800843:	eb 02                	jmp    800847 <strncmp+0x13>
		n--, p++, q++;
  800845:	40                   	inc    %eax
  800846:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800847:	39 d8                	cmp    %ebx,%eax
  800849:	74 14                	je     80085f <strncmp+0x2b>
  80084b:	8a 08                	mov    (%eax),%cl
  80084d:	84 c9                	test   %cl,%cl
  80084f:	74 04                	je     800855 <strncmp+0x21>
  800851:	3a 0a                	cmp    (%edx),%cl
  800853:	74 f0                	je     800845 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800855:	0f b6 00             	movzbl (%eax),%eax
  800858:	0f b6 12             	movzbl (%edx),%edx
  80085b:	29 d0                	sub    %edx,%eax
  80085d:	eb 05                	jmp    800864 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80085f:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800864:	5b                   	pop    %ebx
  800865:	5d                   	pop    %ebp
  800866:	c3                   	ret    

00800867 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800867:	55                   	push   %ebp
  800868:	89 e5                	mov    %esp,%ebp
  80086a:	8b 45 08             	mov    0x8(%ebp),%eax
  80086d:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800870:	eb 05                	jmp    800877 <strchr+0x10>
		if (*s == c)
  800872:	38 ca                	cmp    %cl,%dl
  800874:	74 0c                	je     800882 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800876:	40                   	inc    %eax
  800877:	8a 10                	mov    (%eax),%dl
  800879:	84 d2                	test   %dl,%dl
  80087b:	75 f5                	jne    800872 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  80087d:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800882:	5d                   	pop    %ebp
  800883:	c3                   	ret    

00800884 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800884:	55                   	push   %ebp
  800885:	89 e5                	mov    %esp,%ebp
  800887:	8b 45 08             	mov    0x8(%ebp),%eax
  80088a:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80088d:	eb 05                	jmp    800894 <strfind+0x10>
		if (*s == c)
  80088f:	38 ca                	cmp    %cl,%dl
  800891:	74 07                	je     80089a <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800893:	40                   	inc    %eax
  800894:	8a 10                	mov    (%eax),%dl
  800896:	84 d2                	test   %dl,%dl
  800898:	75 f5                	jne    80088f <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  80089a:	5d                   	pop    %ebp
  80089b:	c3                   	ret    

0080089c <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  80089c:	55                   	push   %ebp
  80089d:	89 e5                	mov    %esp,%ebp
  80089f:	57                   	push   %edi
  8008a0:	56                   	push   %esi
  8008a1:	53                   	push   %ebx
  8008a2:	8b 7d 08             	mov    0x8(%ebp),%edi
  8008a5:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8008a8:	85 c9                	test   %ecx,%ecx
  8008aa:	74 36                	je     8008e2 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8008ac:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8008b2:	75 28                	jne    8008dc <memset+0x40>
  8008b4:	f6 c1 03             	test   $0x3,%cl
  8008b7:	75 23                	jne    8008dc <memset+0x40>
		c &= 0xFF;
  8008b9:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8008bd:	89 d3                	mov    %edx,%ebx
  8008bf:	c1 e3 08             	shl    $0x8,%ebx
  8008c2:	89 d6                	mov    %edx,%esi
  8008c4:	c1 e6 18             	shl    $0x18,%esi
  8008c7:	89 d0                	mov    %edx,%eax
  8008c9:	c1 e0 10             	shl    $0x10,%eax
  8008cc:	09 f0                	or     %esi,%eax
  8008ce:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008d0:	89 d8                	mov    %ebx,%eax
  8008d2:	09 d0                	or     %edx,%eax
  8008d4:	c1 e9 02             	shr    $0x2,%ecx
  8008d7:	fc                   	cld    
  8008d8:	f3 ab                	rep stos %eax,%es:(%edi)
  8008da:	eb 06                	jmp    8008e2 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008dc:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008df:	fc                   	cld    
  8008e0:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008e2:	89 f8                	mov    %edi,%eax
  8008e4:	5b                   	pop    %ebx
  8008e5:	5e                   	pop    %esi
  8008e6:	5f                   	pop    %edi
  8008e7:	5d                   	pop    %ebp
  8008e8:	c3                   	ret    

008008e9 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8008e9:	55                   	push   %ebp
  8008ea:	89 e5                	mov    %esp,%ebp
  8008ec:	57                   	push   %edi
  8008ed:	56                   	push   %esi
  8008ee:	8b 45 08             	mov    0x8(%ebp),%eax
  8008f1:	8b 75 0c             	mov    0xc(%ebp),%esi
  8008f4:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8008f7:	39 c6                	cmp    %eax,%esi
  8008f9:	73 33                	jae    80092e <memmove+0x45>
  8008fb:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8008fe:	39 d0                	cmp    %edx,%eax
  800900:	73 2c                	jae    80092e <memmove+0x45>
		s += n;
		d += n;
  800902:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800905:	89 d6                	mov    %edx,%esi
  800907:	09 fe                	or     %edi,%esi
  800909:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80090f:	75 13                	jne    800924 <memmove+0x3b>
  800911:	f6 c1 03             	test   $0x3,%cl
  800914:	75 0e                	jne    800924 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800916:	83 ef 04             	sub    $0x4,%edi
  800919:	8d 72 fc             	lea    -0x4(%edx),%esi
  80091c:	c1 e9 02             	shr    $0x2,%ecx
  80091f:	fd                   	std    
  800920:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800922:	eb 07                	jmp    80092b <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800924:	4f                   	dec    %edi
  800925:	8d 72 ff             	lea    -0x1(%edx),%esi
  800928:	fd                   	std    
  800929:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80092b:	fc                   	cld    
  80092c:	eb 1d                	jmp    80094b <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80092e:	89 f2                	mov    %esi,%edx
  800930:	09 c2                	or     %eax,%edx
  800932:	f6 c2 03             	test   $0x3,%dl
  800935:	75 0f                	jne    800946 <memmove+0x5d>
  800937:	f6 c1 03             	test   $0x3,%cl
  80093a:	75 0a                	jne    800946 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80093c:	c1 e9 02             	shr    $0x2,%ecx
  80093f:	89 c7                	mov    %eax,%edi
  800941:	fc                   	cld    
  800942:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800944:	eb 05                	jmp    80094b <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800946:	89 c7                	mov    %eax,%edi
  800948:	fc                   	cld    
  800949:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  80094b:	5e                   	pop    %esi
  80094c:	5f                   	pop    %edi
  80094d:	5d                   	pop    %ebp
  80094e:	c3                   	ret    

0080094f <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80094f:	55                   	push   %ebp
  800950:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800952:	ff 75 10             	pushl  0x10(%ebp)
  800955:	ff 75 0c             	pushl  0xc(%ebp)
  800958:	ff 75 08             	pushl  0x8(%ebp)
  80095b:	e8 89 ff ff ff       	call   8008e9 <memmove>
}
  800960:	c9                   	leave  
  800961:	c3                   	ret    

00800962 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800962:	55                   	push   %ebp
  800963:	89 e5                	mov    %esp,%ebp
  800965:	56                   	push   %esi
  800966:	53                   	push   %ebx
  800967:	8b 45 08             	mov    0x8(%ebp),%eax
  80096a:	8b 55 0c             	mov    0xc(%ebp),%edx
  80096d:	89 c6                	mov    %eax,%esi
  80096f:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800972:	eb 14                	jmp    800988 <memcmp+0x26>
		if (*s1 != *s2)
  800974:	8a 08                	mov    (%eax),%cl
  800976:	8a 1a                	mov    (%edx),%bl
  800978:	38 d9                	cmp    %bl,%cl
  80097a:	74 0a                	je     800986 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  80097c:	0f b6 c1             	movzbl %cl,%eax
  80097f:	0f b6 db             	movzbl %bl,%ebx
  800982:	29 d8                	sub    %ebx,%eax
  800984:	eb 0b                	jmp    800991 <memcmp+0x2f>
		s1++, s2++;
  800986:	40                   	inc    %eax
  800987:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800988:	39 f0                	cmp    %esi,%eax
  80098a:	75 e8                	jne    800974 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  80098c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800991:	5b                   	pop    %ebx
  800992:	5e                   	pop    %esi
  800993:	5d                   	pop    %ebp
  800994:	c3                   	ret    

00800995 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800995:	55                   	push   %ebp
  800996:	89 e5                	mov    %esp,%ebp
  800998:	53                   	push   %ebx
  800999:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  80099c:	89 c1                	mov    %eax,%ecx
  80099e:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8009a1:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009a5:	eb 08                	jmp    8009af <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8009a7:	0f b6 10             	movzbl (%eax),%edx
  8009aa:	39 da                	cmp    %ebx,%edx
  8009ac:	74 05                	je     8009b3 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009ae:	40                   	inc    %eax
  8009af:	39 c8                	cmp    %ecx,%eax
  8009b1:	72 f4                	jb     8009a7 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8009b3:	5b                   	pop    %ebx
  8009b4:	5d                   	pop    %ebp
  8009b5:	c3                   	ret    

008009b6 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8009b6:	55                   	push   %ebp
  8009b7:	89 e5                	mov    %esp,%ebp
  8009b9:	57                   	push   %edi
  8009ba:	56                   	push   %esi
  8009bb:	53                   	push   %ebx
  8009bc:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009bf:	eb 01                	jmp    8009c2 <strtol+0xc>
		s++;
  8009c1:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009c2:	8a 01                	mov    (%ecx),%al
  8009c4:	3c 20                	cmp    $0x20,%al
  8009c6:	74 f9                	je     8009c1 <strtol+0xb>
  8009c8:	3c 09                	cmp    $0x9,%al
  8009ca:	74 f5                	je     8009c1 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009cc:	3c 2b                	cmp    $0x2b,%al
  8009ce:	75 08                	jne    8009d8 <strtol+0x22>
		s++;
  8009d0:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009d1:	bf 00 00 00 00       	mov    $0x0,%edi
  8009d6:	eb 11                	jmp    8009e9 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009d8:	3c 2d                	cmp    $0x2d,%al
  8009da:	75 08                	jne    8009e4 <strtol+0x2e>
		s++, neg = 1;
  8009dc:	41                   	inc    %ecx
  8009dd:	bf 01 00 00 00       	mov    $0x1,%edi
  8009e2:	eb 05                	jmp    8009e9 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009e4:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009e9:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009ed:	0f 84 87 00 00 00    	je     800a7a <strtol+0xc4>
  8009f3:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  8009f7:	75 27                	jne    800a20 <strtol+0x6a>
  8009f9:	80 39 30             	cmpb   $0x30,(%ecx)
  8009fc:	75 22                	jne    800a20 <strtol+0x6a>
  8009fe:	e9 88 00 00 00       	jmp    800a8b <strtol+0xd5>
		s += 2, base = 16;
  800a03:	83 c1 02             	add    $0x2,%ecx
  800a06:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a0d:	eb 11                	jmp    800a20 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a0f:	41                   	inc    %ecx
  800a10:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a17:	eb 07                	jmp    800a20 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a19:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a20:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a25:	8a 11                	mov    (%ecx),%dl
  800a27:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a2a:	80 fb 09             	cmp    $0x9,%bl
  800a2d:	77 08                	ja     800a37 <strtol+0x81>
			dig = *s - '0';
  800a2f:	0f be d2             	movsbl %dl,%edx
  800a32:	83 ea 30             	sub    $0x30,%edx
  800a35:	eb 22                	jmp    800a59 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a37:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a3a:	89 f3                	mov    %esi,%ebx
  800a3c:	80 fb 19             	cmp    $0x19,%bl
  800a3f:	77 08                	ja     800a49 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a41:	0f be d2             	movsbl %dl,%edx
  800a44:	83 ea 57             	sub    $0x57,%edx
  800a47:	eb 10                	jmp    800a59 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a49:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a4c:	89 f3                	mov    %esi,%ebx
  800a4e:	80 fb 19             	cmp    $0x19,%bl
  800a51:	77 14                	ja     800a67 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a53:	0f be d2             	movsbl %dl,%edx
  800a56:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a59:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a5c:	7d 09                	jge    800a67 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a5e:	41                   	inc    %ecx
  800a5f:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a63:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a65:	eb be                	jmp    800a25 <strtol+0x6f>

	if (endptr)
  800a67:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a6b:	74 05                	je     800a72 <strtol+0xbc>
		*endptr = (char *) s;
  800a6d:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a70:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a72:	85 ff                	test   %edi,%edi
  800a74:	74 21                	je     800a97 <strtol+0xe1>
  800a76:	f7 d8                	neg    %eax
  800a78:	eb 1d                	jmp    800a97 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a7a:	80 39 30             	cmpb   $0x30,(%ecx)
  800a7d:	75 9a                	jne    800a19 <strtol+0x63>
  800a7f:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a83:	0f 84 7a ff ff ff    	je     800a03 <strtol+0x4d>
  800a89:	eb 84                	jmp    800a0f <strtol+0x59>
  800a8b:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a8f:	0f 84 6e ff ff ff    	je     800a03 <strtol+0x4d>
  800a95:	eb 89                	jmp    800a20 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a97:	5b                   	pop    %ebx
  800a98:	5e                   	pop    %esi
  800a99:	5f                   	pop    %edi
  800a9a:	5d                   	pop    %ebp
  800a9b:	c3                   	ret    

00800a9c <__udivdi3>:
  800a9c:	55                   	push   %ebp
  800a9d:	57                   	push   %edi
  800a9e:	56                   	push   %esi
  800a9f:	53                   	push   %ebx
  800aa0:	83 ec 1c             	sub    $0x1c,%esp
  800aa3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800aa7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800aab:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800aaf:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800ab3:	89 ca                	mov    %ecx,%edx
  800ab5:	89 f8                	mov    %edi,%eax
  800ab7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800abb:	85 f6                	test   %esi,%esi
  800abd:	75 2d                	jne    800aec <__udivdi3+0x50>
  800abf:	39 cf                	cmp    %ecx,%edi
  800ac1:	77 65                	ja     800b28 <__udivdi3+0x8c>
  800ac3:	89 fd                	mov    %edi,%ebp
  800ac5:	85 ff                	test   %edi,%edi
  800ac7:	75 0b                	jne    800ad4 <__udivdi3+0x38>
  800ac9:	b8 01 00 00 00       	mov    $0x1,%eax
  800ace:	31 d2                	xor    %edx,%edx
  800ad0:	f7 f7                	div    %edi
  800ad2:	89 c5                	mov    %eax,%ebp
  800ad4:	31 d2                	xor    %edx,%edx
  800ad6:	89 c8                	mov    %ecx,%eax
  800ad8:	f7 f5                	div    %ebp
  800ada:	89 c1                	mov    %eax,%ecx
  800adc:	89 d8                	mov    %ebx,%eax
  800ade:	f7 f5                	div    %ebp
  800ae0:	89 cf                	mov    %ecx,%edi
  800ae2:	89 fa                	mov    %edi,%edx
  800ae4:	83 c4 1c             	add    $0x1c,%esp
  800ae7:	5b                   	pop    %ebx
  800ae8:	5e                   	pop    %esi
  800ae9:	5f                   	pop    %edi
  800aea:	5d                   	pop    %ebp
  800aeb:	c3                   	ret    
  800aec:	39 ce                	cmp    %ecx,%esi
  800aee:	77 28                	ja     800b18 <__udivdi3+0x7c>
  800af0:	0f bd fe             	bsr    %esi,%edi
  800af3:	83 f7 1f             	xor    $0x1f,%edi
  800af6:	75 40                	jne    800b38 <__udivdi3+0x9c>
  800af8:	39 ce                	cmp    %ecx,%esi
  800afa:	72 0a                	jb     800b06 <__udivdi3+0x6a>
  800afc:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b00:	0f 87 9e 00 00 00    	ja     800ba4 <__udivdi3+0x108>
  800b06:	b8 01 00 00 00       	mov    $0x1,%eax
  800b0b:	89 fa                	mov    %edi,%edx
  800b0d:	83 c4 1c             	add    $0x1c,%esp
  800b10:	5b                   	pop    %ebx
  800b11:	5e                   	pop    %esi
  800b12:	5f                   	pop    %edi
  800b13:	5d                   	pop    %ebp
  800b14:	c3                   	ret    
  800b15:	8d 76 00             	lea    0x0(%esi),%esi
  800b18:	31 ff                	xor    %edi,%edi
  800b1a:	31 c0                	xor    %eax,%eax
  800b1c:	89 fa                	mov    %edi,%edx
  800b1e:	83 c4 1c             	add    $0x1c,%esp
  800b21:	5b                   	pop    %ebx
  800b22:	5e                   	pop    %esi
  800b23:	5f                   	pop    %edi
  800b24:	5d                   	pop    %ebp
  800b25:	c3                   	ret    
  800b26:	66 90                	xchg   %ax,%ax
  800b28:	89 d8                	mov    %ebx,%eax
  800b2a:	f7 f7                	div    %edi
  800b2c:	31 ff                	xor    %edi,%edi
  800b2e:	89 fa                	mov    %edi,%edx
  800b30:	83 c4 1c             	add    $0x1c,%esp
  800b33:	5b                   	pop    %ebx
  800b34:	5e                   	pop    %esi
  800b35:	5f                   	pop    %edi
  800b36:	5d                   	pop    %ebp
  800b37:	c3                   	ret    
  800b38:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b3d:	89 eb                	mov    %ebp,%ebx
  800b3f:	29 fb                	sub    %edi,%ebx
  800b41:	89 f9                	mov    %edi,%ecx
  800b43:	d3 e6                	shl    %cl,%esi
  800b45:	89 c5                	mov    %eax,%ebp
  800b47:	88 d9                	mov    %bl,%cl
  800b49:	d3 ed                	shr    %cl,%ebp
  800b4b:	89 e9                	mov    %ebp,%ecx
  800b4d:	09 f1                	or     %esi,%ecx
  800b4f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b53:	89 f9                	mov    %edi,%ecx
  800b55:	d3 e0                	shl    %cl,%eax
  800b57:	89 c5                	mov    %eax,%ebp
  800b59:	89 d6                	mov    %edx,%esi
  800b5b:	88 d9                	mov    %bl,%cl
  800b5d:	d3 ee                	shr    %cl,%esi
  800b5f:	89 f9                	mov    %edi,%ecx
  800b61:	d3 e2                	shl    %cl,%edx
  800b63:	8b 44 24 08          	mov    0x8(%esp),%eax
  800b67:	88 d9                	mov    %bl,%cl
  800b69:	d3 e8                	shr    %cl,%eax
  800b6b:	09 c2                	or     %eax,%edx
  800b6d:	89 d0                	mov    %edx,%eax
  800b6f:	89 f2                	mov    %esi,%edx
  800b71:	f7 74 24 0c          	divl   0xc(%esp)
  800b75:	89 d6                	mov    %edx,%esi
  800b77:	89 c3                	mov    %eax,%ebx
  800b79:	f7 e5                	mul    %ebp
  800b7b:	39 d6                	cmp    %edx,%esi
  800b7d:	72 19                	jb     800b98 <__udivdi3+0xfc>
  800b7f:	74 0b                	je     800b8c <__udivdi3+0xf0>
  800b81:	89 d8                	mov    %ebx,%eax
  800b83:	31 ff                	xor    %edi,%edi
  800b85:	e9 58 ff ff ff       	jmp    800ae2 <__udivdi3+0x46>
  800b8a:	66 90                	xchg   %ax,%ax
  800b8c:	8b 54 24 08          	mov    0x8(%esp),%edx
  800b90:	89 f9                	mov    %edi,%ecx
  800b92:	d3 e2                	shl    %cl,%edx
  800b94:	39 c2                	cmp    %eax,%edx
  800b96:	73 e9                	jae    800b81 <__udivdi3+0xe5>
  800b98:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800b9b:	31 ff                	xor    %edi,%edi
  800b9d:	e9 40 ff ff ff       	jmp    800ae2 <__udivdi3+0x46>
  800ba2:	66 90                	xchg   %ax,%ax
  800ba4:	31 c0                	xor    %eax,%eax
  800ba6:	e9 37 ff ff ff       	jmp    800ae2 <__udivdi3+0x46>
  800bab:	90                   	nop

00800bac <__umoddi3>:
  800bac:	55                   	push   %ebp
  800bad:	57                   	push   %edi
  800bae:	56                   	push   %esi
  800baf:	53                   	push   %ebx
  800bb0:	83 ec 1c             	sub    $0x1c,%esp
  800bb3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bb7:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bbb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800bbf:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800bc3:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800bc7:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800bcb:	89 f3                	mov    %esi,%ebx
  800bcd:	89 fa                	mov    %edi,%edx
  800bcf:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800bd3:	89 34 24             	mov    %esi,(%esp)
  800bd6:	85 c0                	test   %eax,%eax
  800bd8:	75 1a                	jne    800bf4 <__umoddi3+0x48>
  800bda:	39 f7                	cmp    %esi,%edi
  800bdc:	0f 86 a2 00 00 00    	jbe    800c84 <__umoddi3+0xd8>
  800be2:	89 c8                	mov    %ecx,%eax
  800be4:	89 f2                	mov    %esi,%edx
  800be6:	f7 f7                	div    %edi
  800be8:	89 d0                	mov    %edx,%eax
  800bea:	31 d2                	xor    %edx,%edx
  800bec:	83 c4 1c             	add    $0x1c,%esp
  800bef:	5b                   	pop    %ebx
  800bf0:	5e                   	pop    %esi
  800bf1:	5f                   	pop    %edi
  800bf2:	5d                   	pop    %ebp
  800bf3:	c3                   	ret    
  800bf4:	39 f0                	cmp    %esi,%eax
  800bf6:	0f 87 ac 00 00 00    	ja     800ca8 <__umoddi3+0xfc>
  800bfc:	0f bd e8             	bsr    %eax,%ebp
  800bff:	83 f5 1f             	xor    $0x1f,%ebp
  800c02:	0f 84 ac 00 00 00    	je     800cb4 <__umoddi3+0x108>
  800c08:	bf 20 00 00 00       	mov    $0x20,%edi
  800c0d:	29 ef                	sub    %ebp,%edi
  800c0f:	89 fe                	mov    %edi,%esi
  800c11:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c15:	89 e9                	mov    %ebp,%ecx
  800c17:	d3 e0                	shl    %cl,%eax
  800c19:	89 d7                	mov    %edx,%edi
  800c1b:	89 f1                	mov    %esi,%ecx
  800c1d:	d3 ef                	shr    %cl,%edi
  800c1f:	09 c7                	or     %eax,%edi
  800c21:	89 e9                	mov    %ebp,%ecx
  800c23:	d3 e2                	shl    %cl,%edx
  800c25:	89 14 24             	mov    %edx,(%esp)
  800c28:	89 d8                	mov    %ebx,%eax
  800c2a:	d3 e0                	shl    %cl,%eax
  800c2c:	89 c2                	mov    %eax,%edx
  800c2e:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c32:	d3 e0                	shl    %cl,%eax
  800c34:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c38:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c3c:	89 f1                	mov    %esi,%ecx
  800c3e:	d3 e8                	shr    %cl,%eax
  800c40:	09 d0                	or     %edx,%eax
  800c42:	d3 eb                	shr    %cl,%ebx
  800c44:	89 da                	mov    %ebx,%edx
  800c46:	f7 f7                	div    %edi
  800c48:	89 d3                	mov    %edx,%ebx
  800c4a:	f7 24 24             	mull   (%esp)
  800c4d:	89 c6                	mov    %eax,%esi
  800c4f:	89 d1                	mov    %edx,%ecx
  800c51:	39 d3                	cmp    %edx,%ebx
  800c53:	0f 82 87 00 00 00    	jb     800ce0 <__umoddi3+0x134>
  800c59:	0f 84 91 00 00 00    	je     800cf0 <__umoddi3+0x144>
  800c5f:	8b 54 24 04          	mov    0x4(%esp),%edx
  800c63:	29 f2                	sub    %esi,%edx
  800c65:	19 cb                	sbb    %ecx,%ebx
  800c67:	89 d8                	mov    %ebx,%eax
  800c69:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800c6d:	d3 e0                	shl    %cl,%eax
  800c6f:	89 e9                	mov    %ebp,%ecx
  800c71:	d3 ea                	shr    %cl,%edx
  800c73:	09 d0                	or     %edx,%eax
  800c75:	89 e9                	mov    %ebp,%ecx
  800c77:	d3 eb                	shr    %cl,%ebx
  800c79:	89 da                	mov    %ebx,%edx
  800c7b:	83 c4 1c             	add    $0x1c,%esp
  800c7e:	5b                   	pop    %ebx
  800c7f:	5e                   	pop    %esi
  800c80:	5f                   	pop    %edi
  800c81:	5d                   	pop    %ebp
  800c82:	c3                   	ret    
  800c83:	90                   	nop
  800c84:	89 fd                	mov    %edi,%ebp
  800c86:	85 ff                	test   %edi,%edi
  800c88:	75 0b                	jne    800c95 <__umoddi3+0xe9>
  800c8a:	b8 01 00 00 00       	mov    $0x1,%eax
  800c8f:	31 d2                	xor    %edx,%edx
  800c91:	f7 f7                	div    %edi
  800c93:	89 c5                	mov    %eax,%ebp
  800c95:	89 f0                	mov    %esi,%eax
  800c97:	31 d2                	xor    %edx,%edx
  800c99:	f7 f5                	div    %ebp
  800c9b:	89 c8                	mov    %ecx,%eax
  800c9d:	f7 f5                	div    %ebp
  800c9f:	89 d0                	mov    %edx,%eax
  800ca1:	e9 44 ff ff ff       	jmp    800bea <__umoddi3+0x3e>
  800ca6:	66 90                	xchg   %ax,%ax
  800ca8:	89 c8                	mov    %ecx,%eax
  800caa:	89 f2                	mov    %esi,%edx
  800cac:	83 c4 1c             	add    $0x1c,%esp
  800caf:	5b                   	pop    %ebx
  800cb0:	5e                   	pop    %esi
  800cb1:	5f                   	pop    %edi
  800cb2:	5d                   	pop    %ebp
  800cb3:	c3                   	ret    
  800cb4:	3b 04 24             	cmp    (%esp),%eax
  800cb7:	72 06                	jb     800cbf <__umoddi3+0x113>
  800cb9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800cbd:	77 0f                	ja     800cce <__umoddi3+0x122>
  800cbf:	89 f2                	mov    %esi,%edx
  800cc1:	29 f9                	sub    %edi,%ecx
  800cc3:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800cc7:	89 14 24             	mov    %edx,(%esp)
  800cca:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800cce:	8b 44 24 04          	mov    0x4(%esp),%eax
  800cd2:	8b 14 24             	mov    (%esp),%edx
  800cd5:	83 c4 1c             	add    $0x1c,%esp
  800cd8:	5b                   	pop    %ebx
  800cd9:	5e                   	pop    %esi
  800cda:	5f                   	pop    %edi
  800cdb:	5d                   	pop    %ebp
  800cdc:	c3                   	ret    
  800cdd:	8d 76 00             	lea    0x0(%esi),%esi
  800ce0:	2b 04 24             	sub    (%esp),%eax
  800ce3:	19 fa                	sbb    %edi,%edx
  800ce5:	89 d1                	mov    %edx,%ecx
  800ce7:	89 c6                	mov    %eax,%esi
  800ce9:	e9 71 ff ff ff       	jmp    800c5f <__umoddi3+0xb3>
  800cee:	66 90                	xchg   %ax,%ax
  800cf0:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800cf4:	72 ea                	jb     800ce0 <__umoddi3+0x134>
  800cf6:	89 d9                	mov    %ebx,%ecx
  800cf8:	e9 62 ff ff ff       	jmp    800c5f <__umoddi3+0xb3>
