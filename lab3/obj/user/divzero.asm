
obj/user/divzero:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 2f 00 00 00       	call   800060 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

int zero;

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	zero = 0;
  800039:	c7 05 04 10 80 00 00 	movl   $0x0,0x801004
  800040:	00 00 00 
	cprintf("1/0 is %08x!\n", 1/zero);
  800043:	b8 01 00 00 00       	mov    $0x1,%eax
  800048:	b9 00 00 00 00       	mov    $0x0,%ecx
  80004d:	99                   	cltd   
  80004e:	f7 f9                	idiv   %ecx
  800050:	50                   	push   %eax
  800051:	68 24 0d 80 00       	push   $0x800d24
  800056:	e8 f4 00 00 00       	call   80014f <cprintf>
}
  80005b:	83 c4 10             	add    $0x10,%esp
  80005e:	c9                   	leave  
  80005f:	c3                   	ret    

00800060 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800060:	55                   	push   %ebp
  800061:	89 e5                	mov    %esp,%ebp
  800063:	56                   	push   %esi
  800064:	53                   	push   %ebx
  800065:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800068:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80006b:	e8 e9 09 00 00       	call   800a59 <sys_getenvid>
  800070:	25 ff 03 00 00       	and    $0x3ff,%eax
  800075:	8d 14 00             	lea    (%eax,%eax,1),%edx
  800078:	01 d0                	add    %edx,%eax
  80007a:	c1 e0 05             	shl    $0x5,%eax
  80007d:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800082:	a3 08 10 80 00       	mov    %eax,0x801008
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800087:	85 db                	test   %ebx,%ebx
  800089:	7e 07                	jle    800092 <libmain+0x32>
		binaryname = argv[0];
  80008b:	8b 06                	mov    (%esi),%eax
  80008d:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  800092:	83 ec 08             	sub    $0x8,%esp
  800095:	56                   	push   %esi
  800096:	53                   	push   %ebx
  800097:	e8 97 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80009c:	e8 0a 00 00 00       	call   8000ab <exit>
}
  8000a1:	83 c4 10             	add    $0x10,%esp
  8000a4:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000a7:	5b                   	pop    %ebx
  8000a8:	5e                   	pop    %esi
  8000a9:	5d                   	pop    %ebp
  8000aa:	c3                   	ret    

008000ab <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000ab:	55                   	push   %ebp
  8000ac:	89 e5                	mov    %esp,%ebp
  8000ae:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000b1:	6a 00                	push   $0x0
  8000b3:	e8 60 09 00 00       	call   800a18 <sys_env_destroy>
}
  8000b8:	83 c4 10             	add    $0x10,%esp
  8000bb:	c9                   	leave  
  8000bc:	c3                   	ret    

008000bd <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000bd:	55                   	push   %ebp
  8000be:	89 e5                	mov    %esp,%ebp
  8000c0:	53                   	push   %ebx
  8000c1:	83 ec 04             	sub    $0x4,%esp
  8000c4:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000c7:	8b 13                	mov    (%ebx),%edx
  8000c9:	8d 42 01             	lea    0x1(%edx),%eax
  8000cc:	89 03                	mov    %eax,(%ebx)
  8000ce:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000d1:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8000d5:	3d ff 00 00 00       	cmp    $0xff,%eax
  8000da:	75 1a                	jne    8000f6 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8000dc:	83 ec 08             	sub    $0x8,%esp
  8000df:	68 ff 00 00 00       	push   $0xff
  8000e4:	8d 43 08             	lea    0x8(%ebx),%eax
  8000e7:	50                   	push   %eax
  8000e8:	e8 ee 08 00 00       	call   8009db <sys_cputs>
		b->idx = 0;
  8000ed:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8000f3:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8000f6:	ff 43 04             	incl   0x4(%ebx)
}
  8000f9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8000fc:	c9                   	leave  
  8000fd:	c3                   	ret    

008000fe <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8000fe:	55                   	push   %ebp
  8000ff:	89 e5                	mov    %esp,%ebp
  800101:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800107:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80010e:	00 00 00 
	b.cnt = 0;
  800111:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800118:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80011b:	ff 75 0c             	pushl  0xc(%ebp)
  80011e:	ff 75 08             	pushl  0x8(%ebp)
  800121:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800127:	50                   	push   %eax
  800128:	68 bd 00 80 00       	push   $0x8000bd
  80012d:	e8 51 01 00 00       	call   800283 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800132:	83 c4 08             	add    $0x8,%esp
  800135:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80013b:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800141:	50                   	push   %eax
  800142:	e8 94 08 00 00       	call   8009db <sys_cputs>

	return b.cnt;
}
  800147:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80014d:	c9                   	leave  
  80014e:	c3                   	ret    

0080014f <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80014f:	55                   	push   %ebp
  800150:	89 e5                	mov    %esp,%ebp
  800152:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800155:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800158:	50                   	push   %eax
  800159:	ff 75 08             	pushl  0x8(%ebp)
  80015c:	e8 9d ff ff ff       	call   8000fe <vcprintf>
	va_end(ap);

	return cnt;
}
  800161:	c9                   	leave  
  800162:	c3                   	ret    

00800163 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800163:	55                   	push   %ebp
  800164:	89 e5                	mov    %esp,%ebp
  800166:	57                   	push   %edi
  800167:	56                   	push   %esi
  800168:	53                   	push   %ebx
  800169:	83 ec 1c             	sub    $0x1c,%esp
  80016c:	89 c7                	mov    %eax,%edi
  80016e:	89 d6                	mov    %edx,%esi
  800170:	8b 45 08             	mov    0x8(%ebp),%eax
  800173:	8b 55 0c             	mov    0xc(%ebp),%edx
  800176:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800179:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80017c:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80017f:	bb 00 00 00 00       	mov    $0x0,%ebx
  800184:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800187:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80018a:	39 d3                	cmp    %edx,%ebx
  80018c:	72 05                	jb     800193 <printnum+0x30>
  80018e:	39 45 10             	cmp    %eax,0x10(%ebp)
  800191:	77 45                	ja     8001d8 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800193:	83 ec 0c             	sub    $0xc,%esp
  800196:	ff 75 18             	pushl  0x18(%ebp)
  800199:	8b 45 14             	mov    0x14(%ebp),%eax
  80019c:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80019f:	53                   	push   %ebx
  8001a0:	ff 75 10             	pushl  0x10(%ebp)
  8001a3:	83 ec 08             	sub    $0x8,%esp
  8001a6:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001a9:	ff 75 e0             	pushl  -0x20(%ebp)
  8001ac:	ff 75 dc             	pushl  -0x24(%ebp)
  8001af:	ff 75 d8             	pushl  -0x28(%ebp)
  8001b2:	e8 09 09 00 00       	call   800ac0 <__udivdi3>
  8001b7:	83 c4 18             	add    $0x18,%esp
  8001ba:	52                   	push   %edx
  8001bb:	50                   	push   %eax
  8001bc:	89 f2                	mov    %esi,%edx
  8001be:	89 f8                	mov    %edi,%eax
  8001c0:	e8 9e ff ff ff       	call   800163 <printnum>
  8001c5:	83 c4 20             	add    $0x20,%esp
  8001c8:	eb 16                	jmp    8001e0 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001ca:	83 ec 08             	sub    $0x8,%esp
  8001cd:	56                   	push   %esi
  8001ce:	ff 75 18             	pushl  0x18(%ebp)
  8001d1:	ff d7                	call   *%edi
  8001d3:	83 c4 10             	add    $0x10,%esp
  8001d6:	eb 03                	jmp    8001db <printnum+0x78>
  8001d8:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8001db:	4b                   	dec    %ebx
  8001dc:	85 db                	test   %ebx,%ebx
  8001de:	7f ea                	jg     8001ca <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8001e0:	83 ec 08             	sub    $0x8,%esp
  8001e3:	56                   	push   %esi
  8001e4:	83 ec 04             	sub    $0x4,%esp
  8001e7:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001ea:	ff 75 e0             	pushl  -0x20(%ebp)
  8001ed:	ff 75 dc             	pushl  -0x24(%ebp)
  8001f0:	ff 75 d8             	pushl  -0x28(%ebp)
  8001f3:	e8 d8 09 00 00       	call   800bd0 <__umoddi3>
  8001f8:	83 c4 14             	add    $0x14,%esp
  8001fb:	0f be 80 3c 0d 80 00 	movsbl 0x800d3c(%eax),%eax
  800202:	50                   	push   %eax
  800203:	ff d7                	call   *%edi
}
  800205:	83 c4 10             	add    $0x10,%esp
  800208:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80020b:	5b                   	pop    %ebx
  80020c:	5e                   	pop    %esi
  80020d:	5f                   	pop    %edi
  80020e:	5d                   	pop    %ebp
  80020f:	c3                   	ret    

00800210 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800210:	55                   	push   %ebp
  800211:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800213:	83 fa 01             	cmp    $0x1,%edx
  800216:	7e 0e                	jle    800226 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800218:	8b 10                	mov    (%eax),%edx
  80021a:	8d 4a 08             	lea    0x8(%edx),%ecx
  80021d:	89 08                	mov    %ecx,(%eax)
  80021f:	8b 02                	mov    (%edx),%eax
  800221:	8b 52 04             	mov    0x4(%edx),%edx
  800224:	eb 22                	jmp    800248 <getuint+0x38>
	else if (lflag)
  800226:	85 d2                	test   %edx,%edx
  800228:	74 10                	je     80023a <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  80022a:	8b 10                	mov    (%eax),%edx
  80022c:	8d 4a 04             	lea    0x4(%edx),%ecx
  80022f:	89 08                	mov    %ecx,(%eax)
  800231:	8b 02                	mov    (%edx),%eax
  800233:	ba 00 00 00 00       	mov    $0x0,%edx
  800238:	eb 0e                	jmp    800248 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80023a:	8b 10                	mov    (%eax),%edx
  80023c:	8d 4a 04             	lea    0x4(%edx),%ecx
  80023f:	89 08                	mov    %ecx,(%eax)
  800241:	8b 02                	mov    (%edx),%eax
  800243:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800248:	5d                   	pop    %ebp
  800249:	c3                   	ret    

0080024a <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80024a:	55                   	push   %ebp
  80024b:	89 e5                	mov    %esp,%ebp
  80024d:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800250:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800253:	8b 10                	mov    (%eax),%edx
  800255:	3b 50 04             	cmp    0x4(%eax),%edx
  800258:	73 0a                	jae    800264 <sprintputch+0x1a>
		*b->buf++ = ch;
  80025a:	8d 4a 01             	lea    0x1(%edx),%ecx
  80025d:	89 08                	mov    %ecx,(%eax)
  80025f:	8b 45 08             	mov    0x8(%ebp),%eax
  800262:	88 02                	mov    %al,(%edx)
}
  800264:	5d                   	pop    %ebp
  800265:	c3                   	ret    

00800266 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800266:	55                   	push   %ebp
  800267:	89 e5                	mov    %esp,%ebp
  800269:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80026c:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80026f:	50                   	push   %eax
  800270:	ff 75 10             	pushl  0x10(%ebp)
  800273:	ff 75 0c             	pushl  0xc(%ebp)
  800276:	ff 75 08             	pushl  0x8(%ebp)
  800279:	e8 05 00 00 00       	call   800283 <vprintfmt>
	va_end(ap);
}
  80027e:	83 c4 10             	add    $0x10,%esp
  800281:	c9                   	leave  
  800282:	c3                   	ret    

00800283 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800283:	55                   	push   %ebp
  800284:	89 e5                	mov    %esp,%ebp
  800286:	57                   	push   %edi
  800287:	56                   	push   %esi
  800288:	53                   	push   %ebx
  800289:	83 ec 2c             	sub    $0x2c,%esp
  80028c:	8b 75 08             	mov    0x8(%ebp),%esi
  80028f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800292:	8b 7d 10             	mov    0x10(%ebp),%edi
  800295:	eb 12                	jmp    8002a9 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800297:	85 c0                	test   %eax,%eax
  800299:	0f 84 68 03 00 00    	je     800607 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80029f:	83 ec 08             	sub    $0x8,%esp
  8002a2:	53                   	push   %ebx
  8002a3:	50                   	push   %eax
  8002a4:	ff d6                	call   *%esi
  8002a6:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002a9:	47                   	inc    %edi
  8002aa:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002ae:	83 f8 25             	cmp    $0x25,%eax
  8002b1:	75 e4                	jne    800297 <vprintfmt+0x14>
  8002b3:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002b7:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002be:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002c5:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002cc:	ba 00 00 00 00       	mov    $0x0,%edx
  8002d1:	eb 07                	jmp    8002da <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002d3:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8002d6:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002da:	8d 47 01             	lea    0x1(%edi),%eax
  8002dd:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8002e0:	0f b6 0f             	movzbl (%edi),%ecx
  8002e3:	8a 07                	mov    (%edi),%al
  8002e5:	83 e8 23             	sub    $0x23,%eax
  8002e8:	3c 55                	cmp    $0x55,%al
  8002ea:	0f 87 fe 02 00 00    	ja     8005ee <vprintfmt+0x36b>
  8002f0:	0f b6 c0             	movzbl %al,%eax
  8002f3:	ff 24 85 cc 0d 80 00 	jmp    *0x800dcc(,%eax,4)
  8002fa:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8002fd:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  800301:	eb d7                	jmp    8002da <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800303:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800306:	b8 00 00 00 00       	mov    $0x0,%eax
  80030b:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80030e:	8d 04 80             	lea    (%eax,%eax,4),%eax
  800311:	01 c0                	add    %eax,%eax
  800313:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800317:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  80031a:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80031d:	83 fa 09             	cmp    $0x9,%edx
  800320:	77 34                	ja     800356 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800322:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800323:	eb e9                	jmp    80030e <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800325:	8b 45 14             	mov    0x14(%ebp),%eax
  800328:	8d 48 04             	lea    0x4(%eax),%ecx
  80032b:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80032e:	8b 00                	mov    (%eax),%eax
  800330:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800333:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800336:	eb 24                	jmp    80035c <vprintfmt+0xd9>
  800338:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80033c:	79 07                	jns    800345 <vprintfmt+0xc2>
  80033e:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800345:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800348:	eb 90                	jmp    8002da <vprintfmt+0x57>
  80034a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80034d:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800354:	eb 84                	jmp    8002da <vprintfmt+0x57>
  800356:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800359:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80035c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800360:	0f 89 74 ff ff ff    	jns    8002da <vprintfmt+0x57>
				width = precision, precision = -1;
  800366:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800369:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80036c:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800373:	e9 62 ff ff ff       	jmp    8002da <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800378:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800379:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80037c:	e9 59 ff ff ff       	jmp    8002da <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800381:	8b 45 14             	mov    0x14(%ebp),%eax
  800384:	8d 50 04             	lea    0x4(%eax),%edx
  800387:	89 55 14             	mov    %edx,0x14(%ebp)
  80038a:	83 ec 08             	sub    $0x8,%esp
  80038d:	53                   	push   %ebx
  80038e:	ff 30                	pushl  (%eax)
  800390:	ff d6                	call   *%esi
			break;
  800392:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800395:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800398:	e9 0c ff ff ff       	jmp    8002a9 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80039d:	8b 45 14             	mov    0x14(%ebp),%eax
  8003a0:	8d 50 04             	lea    0x4(%eax),%edx
  8003a3:	89 55 14             	mov    %edx,0x14(%ebp)
  8003a6:	8b 00                	mov    (%eax),%eax
  8003a8:	85 c0                	test   %eax,%eax
  8003aa:	79 02                	jns    8003ae <vprintfmt+0x12b>
  8003ac:	f7 d8                	neg    %eax
  8003ae:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003b0:	83 f8 06             	cmp    $0x6,%eax
  8003b3:	7f 0b                	jg     8003c0 <vprintfmt+0x13d>
  8003b5:	8b 04 85 24 0f 80 00 	mov    0x800f24(,%eax,4),%eax
  8003bc:	85 c0                	test   %eax,%eax
  8003be:	75 18                	jne    8003d8 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003c0:	52                   	push   %edx
  8003c1:	68 54 0d 80 00       	push   $0x800d54
  8003c6:	53                   	push   %ebx
  8003c7:	56                   	push   %esi
  8003c8:	e8 99 fe ff ff       	call   800266 <printfmt>
  8003cd:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003d0:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8003d3:	e9 d1 fe ff ff       	jmp    8002a9 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8003d8:	50                   	push   %eax
  8003d9:	68 5d 0d 80 00       	push   $0x800d5d
  8003de:	53                   	push   %ebx
  8003df:	56                   	push   %esi
  8003e0:	e8 81 fe ff ff       	call   800266 <printfmt>
  8003e5:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003e8:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003eb:	e9 b9 fe ff ff       	jmp    8002a9 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8003f0:	8b 45 14             	mov    0x14(%ebp),%eax
  8003f3:	8d 50 04             	lea    0x4(%eax),%edx
  8003f6:	89 55 14             	mov    %edx,0x14(%ebp)
  8003f9:	8b 38                	mov    (%eax),%edi
  8003fb:	85 ff                	test   %edi,%edi
  8003fd:	75 05                	jne    800404 <vprintfmt+0x181>
				p = "(null)";
  8003ff:	bf 4d 0d 80 00       	mov    $0x800d4d,%edi
			if (width > 0 && padc != '-')
  800404:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800408:	0f 8e 90 00 00 00    	jle    80049e <vprintfmt+0x21b>
  80040e:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800412:	0f 84 8e 00 00 00    	je     8004a6 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800418:	83 ec 08             	sub    $0x8,%esp
  80041b:	ff 75 d0             	pushl  -0x30(%ebp)
  80041e:	57                   	push   %edi
  80041f:	e8 70 02 00 00       	call   800694 <strnlen>
  800424:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800427:	29 c1                	sub    %eax,%ecx
  800429:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80042c:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80042f:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800433:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800436:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800439:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80043b:	eb 0d                	jmp    80044a <vprintfmt+0x1c7>
					putch(padc, putdat);
  80043d:	83 ec 08             	sub    $0x8,%esp
  800440:	53                   	push   %ebx
  800441:	ff 75 e4             	pushl  -0x1c(%ebp)
  800444:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800446:	4f                   	dec    %edi
  800447:	83 c4 10             	add    $0x10,%esp
  80044a:	85 ff                	test   %edi,%edi
  80044c:	7f ef                	jg     80043d <vprintfmt+0x1ba>
  80044e:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800451:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800454:	89 c8                	mov    %ecx,%eax
  800456:	85 c9                	test   %ecx,%ecx
  800458:	79 05                	jns    80045f <vprintfmt+0x1dc>
  80045a:	b8 00 00 00 00       	mov    $0x0,%eax
  80045f:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800462:	29 c1                	sub    %eax,%ecx
  800464:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800467:	89 75 08             	mov    %esi,0x8(%ebp)
  80046a:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80046d:	eb 3d                	jmp    8004ac <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80046f:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800473:	74 19                	je     80048e <vprintfmt+0x20b>
  800475:	0f be c0             	movsbl %al,%eax
  800478:	83 e8 20             	sub    $0x20,%eax
  80047b:	83 f8 5e             	cmp    $0x5e,%eax
  80047e:	76 0e                	jbe    80048e <vprintfmt+0x20b>
					putch('?', putdat);
  800480:	83 ec 08             	sub    $0x8,%esp
  800483:	53                   	push   %ebx
  800484:	6a 3f                	push   $0x3f
  800486:	ff 55 08             	call   *0x8(%ebp)
  800489:	83 c4 10             	add    $0x10,%esp
  80048c:	eb 0b                	jmp    800499 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80048e:	83 ec 08             	sub    $0x8,%esp
  800491:	53                   	push   %ebx
  800492:	52                   	push   %edx
  800493:	ff 55 08             	call   *0x8(%ebp)
  800496:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800499:	ff 4d e4             	decl   -0x1c(%ebp)
  80049c:	eb 0e                	jmp    8004ac <vprintfmt+0x229>
  80049e:	89 75 08             	mov    %esi,0x8(%ebp)
  8004a1:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004a4:	eb 06                	jmp    8004ac <vprintfmt+0x229>
  8004a6:	89 75 08             	mov    %esi,0x8(%ebp)
  8004a9:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004ac:	47                   	inc    %edi
  8004ad:	8a 47 ff             	mov    -0x1(%edi),%al
  8004b0:	0f be d0             	movsbl %al,%edx
  8004b3:	85 d2                	test   %edx,%edx
  8004b5:	74 1d                	je     8004d4 <vprintfmt+0x251>
  8004b7:	85 f6                	test   %esi,%esi
  8004b9:	78 b4                	js     80046f <vprintfmt+0x1ec>
  8004bb:	4e                   	dec    %esi
  8004bc:	79 b1                	jns    80046f <vprintfmt+0x1ec>
  8004be:	8b 75 08             	mov    0x8(%ebp),%esi
  8004c1:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004c4:	eb 14                	jmp    8004da <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004c6:	83 ec 08             	sub    $0x8,%esp
  8004c9:	53                   	push   %ebx
  8004ca:	6a 20                	push   $0x20
  8004cc:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004ce:	4f                   	dec    %edi
  8004cf:	83 c4 10             	add    $0x10,%esp
  8004d2:	eb 06                	jmp    8004da <vprintfmt+0x257>
  8004d4:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004d7:	8b 75 08             	mov    0x8(%ebp),%esi
  8004da:	85 ff                	test   %edi,%edi
  8004dc:	7f e8                	jg     8004c6 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004de:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004e1:	e9 c3 fd ff ff       	jmp    8002a9 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8004e6:	83 fa 01             	cmp    $0x1,%edx
  8004e9:	7e 16                	jle    800501 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8004eb:	8b 45 14             	mov    0x14(%ebp),%eax
  8004ee:	8d 50 08             	lea    0x8(%eax),%edx
  8004f1:	89 55 14             	mov    %edx,0x14(%ebp)
  8004f4:	8b 50 04             	mov    0x4(%eax),%edx
  8004f7:	8b 00                	mov    (%eax),%eax
  8004f9:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8004fc:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8004ff:	eb 32                	jmp    800533 <vprintfmt+0x2b0>
	else if (lflag)
  800501:	85 d2                	test   %edx,%edx
  800503:	74 18                	je     80051d <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800505:	8b 45 14             	mov    0x14(%ebp),%eax
  800508:	8d 50 04             	lea    0x4(%eax),%edx
  80050b:	89 55 14             	mov    %edx,0x14(%ebp)
  80050e:	8b 00                	mov    (%eax),%eax
  800510:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800513:	89 c1                	mov    %eax,%ecx
  800515:	c1 f9 1f             	sar    $0x1f,%ecx
  800518:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  80051b:	eb 16                	jmp    800533 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80051d:	8b 45 14             	mov    0x14(%ebp),%eax
  800520:	8d 50 04             	lea    0x4(%eax),%edx
  800523:	89 55 14             	mov    %edx,0x14(%ebp)
  800526:	8b 00                	mov    (%eax),%eax
  800528:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80052b:	89 c1                	mov    %eax,%ecx
  80052d:	c1 f9 1f             	sar    $0x1f,%ecx
  800530:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800533:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800536:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800539:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80053d:	79 76                	jns    8005b5 <vprintfmt+0x332>
				putch('-', putdat);
  80053f:	83 ec 08             	sub    $0x8,%esp
  800542:	53                   	push   %ebx
  800543:	6a 2d                	push   $0x2d
  800545:	ff d6                	call   *%esi
				num = -(long long) num;
  800547:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80054a:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80054d:	f7 d8                	neg    %eax
  80054f:	83 d2 00             	adc    $0x0,%edx
  800552:	f7 da                	neg    %edx
  800554:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800557:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80055c:	eb 5c                	jmp    8005ba <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80055e:	8d 45 14             	lea    0x14(%ebp),%eax
  800561:	e8 aa fc ff ff       	call   800210 <getuint>
			base = 10;
  800566:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80056b:	eb 4d                	jmp    8005ba <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80056d:	8d 45 14             	lea    0x14(%ebp),%eax
  800570:	e8 9b fc ff ff       	call   800210 <getuint>
			base = 8;
  800575:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80057a:	eb 3e                	jmp    8005ba <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80057c:	83 ec 08             	sub    $0x8,%esp
  80057f:	53                   	push   %ebx
  800580:	6a 30                	push   $0x30
  800582:	ff d6                	call   *%esi
			putch('x', putdat);
  800584:	83 c4 08             	add    $0x8,%esp
  800587:	53                   	push   %ebx
  800588:	6a 78                	push   $0x78
  80058a:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80058c:	8b 45 14             	mov    0x14(%ebp),%eax
  80058f:	8d 50 04             	lea    0x4(%eax),%edx
  800592:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800595:	8b 00                	mov    (%eax),%eax
  800597:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80059c:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80059f:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005a4:	eb 14                	jmp    8005ba <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005a6:	8d 45 14             	lea    0x14(%ebp),%eax
  8005a9:	e8 62 fc ff ff       	call   800210 <getuint>
			base = 16;
  8005ae:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005b3:	eb 05                	jmp    8005ba <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005b5:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005ba:	83 ec 0c             	sub    $0xc,%esp
  8005bd:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005c1:	57                   	push   %edi
  8005c2:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005c5:	51                   	push   %ecx
  8005c6:	52                   	push   %edx
  8005c7:	50                   	push   %eax
  8005c8:	89 da                	mov    %ebx,%edx
  8005ca:	89 f0                	mov    %esi,%eax
  8005cc:	e8 92 fb ff ff       	call   800163 <printnum>
			break;
  8005d1:	83 c4 20             	add    $0x20,%esp
  8005d4:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005d7:	e9 cd fc ff ff       	jmp    8002a9 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8005dc:	83 ec 08             	sub    $0x8,%esp
  8005df:	53                   	push   %ebx
  8005e0:	51                   	push   %ecx
  8005e1:	ff d6                	call   *%esi
			break;
  8005e3:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005e6:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8005e9:	e9 bb fc ff ff       	jmp    8002a9 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8005ee:	83 ec 08             	sub    $0x8,%esp
  8005f1:	53                   	push   %ebx
  8005f2:	6a 25                	push   $0x25
  8005f4:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8005f6:	83 c4 10             	add    $0x10,%esp
  8005f9:	eb 01                	jmp    8005fc <vprintfmt+0x379>
  8005fb:	4f                   	dec    %edi
  8005fc:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  800600:	75 f9                	jne    8005fb <vprintfmt+0x378>
  800602:	e9 a2 fc ff ff       	jmp    8002a9 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800607:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80060a:	5b                   	pop    %ebx
  80060b:	5e                   	pop    %esi
  80060c:	5f                   	pop    %edi
  80060d:	5d                   	pop    %ebp
  80060e:	c3                   	ret    

0080060f <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80060f:	55                   	push   %ebp
  800610:	89 e5                	mov    %esp,%ebp
  800612:	83 ec 18             	sub    $0x18,%esp
  800615:	8b 45 08             	mov    0x8(%ebp),%eax
  800618:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  80061b:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80061e:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800622:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800625:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80062c:	85 c0                	test   %eax,%eax
  80062e:	74 26                	je     800656 <vsnprintf+0x47>
  800630:	85 d2                	test   %edx,%edx
  800632:	7e 29                	jle    80065d <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800634:	ff 75 14             	pushl  0x14(%ebp)
  800637:	ff 75 10             	pushl  0x10(%ebp)
  80063a:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80063d:	50                   	push   %eax
  80063e:	68 4a 02 80 00       	push   $0x80024a
  800643:	e8 3b fc ff ff       	call   800283 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800648:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80064b:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80064e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800651:	83 c4 10             	add    $0x10,%esp
  800654:	eb 0c                	jmp    800662 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800656:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80065b:	eb 05                	jmp    800662 <vsnprintf+0x53>
  80065d:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800662:	c9                   	leave  
  800663:	c3                   	ret    

00800664 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800664:	55                   	push   %ebp
  800665:	89 e5                	mov    %esp,%ebp
  800667:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80066a:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80066d:	50                   	push   %eax
  80066e:	ff 75 10             	pushl  0x10(%ebp)
  800671:	ff 75 0c             	pushl  0xc(%ebp)
  800674:	ff 75 08             	pushl  0x8(%ebp)
  800677:	e8 93 ff ff ff       	call   80060f <vsnprintf>
	va_end(ap);

	return rc;
}
  80067c:	c9                   	leave  
  80067d:	c3                   	ret    

0080067e <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80067e:	55                   	push   %ebp
  80067f:	89 e5                	mov    %esp,%ebp
  800681:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800684:	b8 00 00 00 00       	mov    $0x0,%eax
  800689:	eb 01                	jmp    80068c <strlen+0xe>
		n++;
  80068b:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80068c:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800690:	75 f9                	jne    80068b <strlen+0xd>
		n++;
	return n;
}
  800692:	5d                   	pop    %ebp
  800693:	c3                   	ret    

00800694 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800694:	55                   	push   %ebp
  800695:	89 e5                	mov    %esp,%ebp
  800697:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80069a:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80069d:	ba 00 00 00 00       	mov    $0x0,%edx
  8006a2:	eb 01                	jmp    8006a5 <strnlen+0x11>
		n++;
  8006a4:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006a5:	39 c2                	cmp    %eax,%edx
  8006a7:	74 08                	je     8006b1 <strnlen+0x1d>
  8006a9:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006ad:	75 f5                	jne    8006a4 <strnlen+0x10>
  8006af:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006b1:	5d                   	pop    %ebp
  8006b2:	c3                   	ret    

008006b3 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006b3:	55                   	push   %ebp
  8006b4:	89 e5                	mov    %esp,%ebp
  8006b6:	53                   	push   %ebx
  8006b7:	8b 45 08             	mov    0x8(%ebp),%eax
  8006ba:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006bd:	89 c2                	mov    %eax,%edx
  8006bf:	42                   	inc    %edx
  8006c0:	41                   	inc    %ecx
  8006c1:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006c4:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006c7:	84 db                	test   %bl,%bl
  8006c9:	75 f4                	jne    8006bf <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006cb:	5b                   	pop    %ebx
  8006cc:	5d                   	pop    %ebp
  8006cd:	c3                   	ret    

008006ce <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006ce:	55                   	push   %ebp
  8006cf:	89 e5                	mov    %esp,%ebp
  8006d1:	53                   	push   %ebx
  8006d2:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8006d5:	53                   	push   %ebx
  8006d6:	e8 a3 ff ff ff       	call   80067e <strlen>
  8006db:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8006de:	ff 75 0c             	pushl  0xc(%ebp)
  8006e1:	01 d8                	add    %ebx,%eax
  8006e3:	50                   	push   %eax
  8006e4:	e8 ca ff ff ff       	call   8006b3 <strcpy>
	return dst;
}
  8006e9:	89 d8                	mov    %ebx,%eax
  8006eb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8006ee:	c9                   	leave  
  8006ef:	c3                   	ret    

008006f0 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8006f0:	55                   	push   %ebp
  8006f1:	89 e5                	mov    %esp,%ebp
  8006f3:	56                   	push   %esi
  8006f4:	53                   	push   %ebx
  8006f5:	8b 75 08             	mov    0x8(%ebp),%esi
  8006f8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8006fb:	89 f3                	mov    %esi,%ebx
  8006fd:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800700:	89 f2                	mov    %esi,%edx
  800702:	eb 0c                	jmp    800710 <strncpy+0x20>
		*dst++ = *src;
  800704:	42                   	inc    %edx
  800705:	8a 01                	mov    (%ecx),%al
  800707:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  80070a:	80 39 01             	cmpb   $0x1,(%ecx)
  80070d:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800710:	39 da                	cmp    %ebx,%edx
  800712:	75 f0                	jne    800704 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800714:	89 f0                	mov    %esi,%eax
  800716:	5b                   	pop    %ebx
  800717:	5e                   	pop    %esi
  800718:	5d                   	pop    %ebp
  800719:	c3                   	ret    

0080071a <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  80071a:	55                   	push   %ebp
  80071b:	89 e5                	mov    %esp,%ebp
  80071d:	56                   	push   %esi
  80071e:	53                   	push   %ebx
  80071f:	8b 75 08             	mov    0x8(%ebp),%esi
  800722:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800725:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800728:	85 c0                	test   %eax,%eax
  80072a:	74 1e                	je     80074a <strlcpy+0x30>
  80072c:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800730:	89 f2                	mov    %esi,%edx
  800732:	eb 05                	jmp    800739 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800734:	42                   	inc    %edx
  800735:	41                   	inc    %ecx
  800736:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800739:	39 c2                	cmp    %eax,%edx
  80073b:	74 08                	je     800745 <strlcpy+0x2b>
  80073d:	8a 19                	mov    (%ecx),%bl
  80073f:	84 db                	test   %bl,%bl
  800741:	75 f1                	jne    800734 <strlcpy+0x1a>
  800743:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800745:	c6 00 00             	movb   $0x0,(%eax)
  800748:	eb 02                	jmp    80074c <strlcpy+0x32>
  80074a:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  80074c:	29 f0                	sub    %esi,%eax
}
  80074e:	5b                   	pop    %ebx
  80074f:	5e                   	pop    %esi
  800750:	5d                   	pop    %ebp
  800751:	c3                   	ret    

00800752 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800752:	55                   	push   %ebp
  800753:	89 e5                	mov    %esp,%ebp
  800755:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800758:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80075b:	eb 02                	jmp    80075f <strcmp+0xd>
		p++, q++;
  80075d:	41                   	inc    %ecx
  80075e:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80075f:	8a 01                	mov    (%ecx),%al
  800761:	84 c0                	test   %al,%al
  800763:	74 04                	je     800769 <strcmp+0x17>
  800765:	3a 02                	cmp    (%edx),%al
  800767:	74 f4                	je     80075d <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800769:	0f b6 c0             	movzbl %al,%eax
  80076c:	0f b6 12             	movzbl (%edx),%edx
  80076f:	29 d0                	sub    %edx,%eax
}
  800771:	5d                   	pop    %ebp
  800772:	c3                   	ret    

00800773 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800773:	55                   	push   %ebp
  800774:	89 e5                	mov    %esp,%ebp
  800776:	53                   	push   %ebx
  800777:	8b 45 08             	mov    0x8(%ebp),%eax
  80077a:	8b 55 0c             	mov    0xc(%ebp),%edx
  80077d:	89 c3                	mov    %eax,%ebx
  80077f:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800782:	eb 02                	jmp    800786 <strncmp+0x13>
		n--, p++, q++;
  800784:	40                   	inc    %eax
  800785:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800786:	39 d8                	cmp    %ebx,%eax
  800788:	74 14                	je     80079e <strncmp+0x2b>
  80078a:	8a 08                	mov    (%eax),%cl
  80078c:	84 c9                	test   %cl,%cl
  80078e:	74 04                	je     800794 <strncmp+0x21>
  800790:	3a 0a                	cmp    (%edx),%cl
  800792:	74 f0                	je     800784 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800794:	0f b6 00             	movzbl (%eax),%eax
  800797:	0f b6 12             	movzbl (%edx),%edx
  80079a:	29 d0                	sub    %edx,%eax
  80079c:	eb 05                	jmp    8007a3 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80079e:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007a3:	5b                   	pop    %ebx
  8007a4:	5d                   	pop    %ebp
  8007a5:	c3                   	ret    

008007a6 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007a6:	55                   	push   %ebp
  8007a7:	89 e5                	mov    %esp,%ebp
  8007a9:	8b 45 08             	mov    0x8(%ebp),%eax
  8007ac:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007af:	eb 05                	jmp    8007b6 <strchr+0x10>
		if (*s == c)
  8007b1:	38 ca                	cmp    %cl,%dl
  8007b3:	74 0c                	je     8007c1 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007b5:	40                   	inc    %eax
  8007b6:	8a 10                	mov    (%eax),%dl
  8007b8:	84 d2                	test   %dl,%dl
  8007ba:	75 f5                	jne    8007b1 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007bc:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007c1:	5d                   	pop    %ebp
  8007c2:	c3                   	ret    

008007c3 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007c3:	55                   	push   %ebp
  8007c4:	89 e5                	mov    %esp,%ebp
  8007c6:	8b 45 08             	mov    0x8(%ebp),%eax
  8007c9:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007cc:	eb 05                	jmp    8007d3 <strfind+0x10>
		if (*s == c)
  8007ce:	38 ca                	cmp    %cl,%dl
  8007d0:	74 07                	je     8007d9 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007d2:	40                   	inc    %eax
  8007d3:	8a 10                	mov    (%eax),%dl
  8007d5:	84 d2                	test   %dl,%dl
  8007d7:	75 f5                	jne    8007ce <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8007d9:	5d                   	pop    %ebp
  8007da:	c3                   	ret    

008007db <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8007db:	55                   	push   %ebp
  8007dc:	89 e5                	mov    %esp,%ebp
  8007de:	57                   	push   %edi
  8007df:	56                   	push   %esi
  8007e0:	53                   	push   %ebx
  8007e1:	8b 7d 08             	mov    0x8(%ebp),%edi
  8007e4:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8007e7:	85 c9                	test   %ecx,%ecx
  8007e9:	74 36                	je     800821 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8007eb:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8007f1:	75 28                	jne    80081b <memset+0x40>
  8007f3:	f6 c1 03             	test   $0x3,%cl
  8007f6:	75 23                	jne    80081b <memset+0x40>
		c &= 0xFF;
  8007f8:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8007fc:	89 d3                	mov    %edx,%ebx
  8007fe:	c1 e3 08             	shl    $0x8,%ebx
  800801:	89 d6                	mov    %edx,%esi
  800803:	c1 e6 18             	shl    $0x18,%esi
  800806:	89 d0                	mov    %edx,%eax
  800808:	c1 e0 10             	shl    $0x10,%eax
  80080b:	09 f0                	or     %esi,%eax
  80080d:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  80080f:	89 d8                	mov    %ebx,%eax
  800811:	09 d0                	or     %edx,%eax
  800813:	c1 e9 02             	shr    $0x2,%ecx
  800816:	fc                   	cld    
  800817:	f3 ab                	rep stos %eax,%es:(%edi)
  800819:	eb 06                	jmp    800821 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  80081b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80081e:	fc                   	cld    
  80081f:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  800821:	89 f8                	mov    %edi,%eax
  800823:	5b                   	pop    %ebx
  800824:	5e                   	pop    %esi
  800825:	5f                   	pop    %edi
  800826:	5d                   	pop    %ebp
  800827:	c3                   	ret    

00800828 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800828:	55                   	push   %ebp
  800829:	89 e5                	mov    %esp,%ebp
  80082b:	57                   	push   %edi
  80082c:	56                   	push   %esi
  80082d:	8b 45 08             	mov    0x8(%ebp),%eax
  800830:	8b 75 0c             	mov    0xc(%ebp),%esi
  800833:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800836:	39 c6                	cmp    %eax,%esi
  800838:	73 33                	jae    80086d <memmove+0x45>
  80083a:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80083d:	39 d0                	cmp    %edx,%eax
  80083f:	73 2c                	jae    80086d <memmove+0x45>
		s += n;
		d += n;
  800841:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800844:	89 d6                	mov    %edx,%esi
  800846:	09 fe                	or     %edi,%esi
  800848:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80084e:	75 13                	jne    800863 <memmove+0x3b>
  800850:	f6 c1 03             	test   $0x3,%cl
  800853:	75 0e                	jne    800863 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800855:	83 ef 04             	sub    $0x4,%edi
  800858:	8d 72 fc             	lea    -0x4(%edx),%esi
  80085b:	c1 e9 02             	shr    $0x2,%ecx
  80085e:	fd                   	std    
  80085f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800861:	eb 07                	jmp    80086a <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800863:	4f                   	dec    %edi
  800864:	8d 72 ff             	lea    -0x1(%edx),%esi
  800867:	fd                   	std    
  800868:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80086a:	fc                   	cld    
  80086b:	eb 1d                	jmp    80088a <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80086d:	89 f2                	mov    %esi,%edx
  80086f:	09 c2                	or     %eax,%edx
  800871:	f6 c2 03             	test   $0x3,%dl
  800874:	75 0f                	jne    800885 <memmove+0x5d>
  800876:	f6 c1 03             	test   $0x3,%cl
  800879:	75 0a                	jne    800885 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80087b:	c1 e9 02             	shr    $0x2,%ecx
  80087e:	89 c7                	mov    %eax,%edi
  800880:	fc                   	cld    
  800881:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800883:	eb 05                	jmp    80088a <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800885:	89 c7                	mov    %eax,%edi
  800887:	fc                   	cld    
  800888:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  80088a:	5e                   	pop    %esi
  80088b:	5f                   	pop    %edi
  80088c:	5d                   	pop    %ebp
  80088d:	c3                   	ret    

0080088e <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80088e:	55                   	push   %ebp
  80088f:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800891:	ff 75 10             	pushl  0x10(%ebp)
  800894:	ff 75 0c             	pushl  0xc(%ebp)
  800897:	ff 75 08             	pushl  0x8(%ebp)
  80089a:	e8 89 ff ff ff       	call   800828 <memmove>
}
  80089f:	c9                   	leave  
  8008a0:	c3                   	ret    

008008a1 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  8008a1:	55                   	push   %ebp
  8008a2:	89 e5                	mov    %esp,%ebp
  8008a4:	56                   	push   %esi
  8008a5:	53                   	push   %ebx
  8008a6:	8b 45 08             	mov    0x8(%ebp),%eax
  8008a9:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008ac:	89 c6                	mov    %eax,%esi
  8008ae:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008b1:	eb 14                	jmp    8008c7 <memcmp+0x26>
		if (*s1 != *s2)
  8008b3:	8a 08                	mov    (%eax),%cl
  8008b5:	8a 1a                	mov    (%edx),%bl
  8008b7:	38 d9                	cmp    %bl,%cl
  8008b9:	74 0a                	je     8008c5 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008bb:	0f b6 c1             	movzbl %cl,%eax
  8008be:	0f b6 db             	movzbl %bl,%ebx
  8008c1:	29 d8                	sub    %ebx,%eax
  8008c3:	eb 0b                	jmp    8008d0 <memcmp+0x2f>
		s1++, s2++;
  8008c5:	40                   	inc    %eax
  8008c6:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008c7:	39 f0                	cmp    %esi,%eax
  8008c9:	75 e8                	jne    8008b3 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008cb:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008d0:	5b                   	pop    %ebx
  8008d1:	5e                   	pop    %esi
  8008d2:	5d                   	pop    %ebp
  8008d3:	c3                   	ret    

008008d4 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8008d4:	55                   	push   %ebp
  8008d5:	89 e5                	mov    %esp,%ebp
  8008d7:	53                   	push   %ebx
  8008d8:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8008db:	89 c1                	mov    %eax,%ecx
  8008dd:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8008e0:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008e4:	eb 08                	jmp    8008ee <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8008e6:	0f b6 10             	movzbl (%eax),%edx
  8008e9:	39 da                	cmp    %ebx,%edx
  8008eb:	74 05                	je     8008f2 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008ed:	40                   	inc    %eax
  8008ee:	39 c8                	cmp    %ecx,%eax
  8008f0:	72 f4                	jb     8008e6 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8008f2:	5b                   	pop    %ebx
  8008f3:	5d                   	pop    %ebp
  8008f4:	c3                   	ret    

008008f5 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8008f5:	55                   	push   %ebp
  8008f6:	89 e5                	mov    %esp,%ebp
  8008f8:	57                   	push   %edi
  8008f9:	56                   	push   %esi
  8008fa:	53                   	push   %ebx
  8008fb:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8008fe:	eb 01                	jmp    800901 <strtol+0xc>
		s++;
  800900:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800901:	8a 01                	mov    (%ecx),%al
  800903:	3c 20                	cmp    $0x20,%al
  800905:	74 f9                	je     800900 <strtol+0xb>
  800907:	3c 09                	cmp    $0x9,%al
  800909:	74 f5                	je     800900 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  80090b:	3c 2b                	cmp    $0x2b,%al
  80090d:	75 08                	jne    800917 <strtol+0x22>
		s++;
  80090f:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800910:	bf 00 00 00 00       	mov    $0x0,%edi
  800915:	eb 11                	jmp    800928 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800917:	3c 2d                	cmp    $0x2d,%al
  800919:	75 08                	jne    800923 <strtol+0x2e>
		s++, neg = 1;
  80091b:	41                   	inc    %ecx
  80091c:	bf 01 00 00 00       	mov    $0x1,%edi
  800921:	eb 05                	jmp    800928 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800923:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800928:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80092c:	0f 84 87 00 00 00    	je     8009b9 <strtol+0xc4>
  800932:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800936:	75 27                	jne    80095f <strtol+0x6a>
  800938:	80 39 30             	cmpb   $0x30,(%ecx)
  80093b:	75 22                	jne    80095f <strtol+0x6a>
  80093d:	e9 88 00 00 00       	jmp    8009ca <strtol+0xd5>
		s += 2, base = 16;
  800942:	83 c1 02             	add    $0x2,%ecx
  800945:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  80094c:	eb 11                	jmp    80095f <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  80094e:	41                   	inc    %ecx
  80094f:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800956:	eb 07                	jmp    80095f <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800958:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  80095f:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800964:	8a 11                	mov    (%ecx),%dl
  800966:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800969:	80 fb 09             	cmp    $0x9,%bl
  80096c:	77 08                	ja     800976 <strtol+0x81>
			dig = *s - '0';
  80096e:	0f be d2             	movsbl %dl,%edx
  800971:	83 ea 30             	sub    $0x30,%edx
  800974:	eb 22                	jmp    800998 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800976:	8d 72 9f             	lea    -0x61(%edx),%esi
  800979:	89 f3                	mov    %esi,%ebx
  80097b:	80 fb 19             	cmp    $0x19,%bl
  80097e:	77 08                	ja     800988 <strtol+0x93>
			dig = *s - 'a' + 10;
  800980:	0f be d2             	movsbl %dl,%edx
  800983:	83 ea 57             	sub    $0x57,%edx
  800986:	eb 10                	jmp    800998 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800988:	8d 72 bf             	lea    -0x41(%edx),%esi
  80098b:	89 f3                	mov    %esi,%ebx
  80098d:	80 fb 19             	cmp    $0x19,%bl
  800990:	77 14                	ja     8009a6 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800992:	0f be d2             	movsbl %dl,%edx
  800995:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800998:	3b 55 10             	cmp    0x10(%ebp),%edx
  80099b:	7d 09                	jge    8009a6 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  80099d:	41                   	inc    %ecx
  80099e:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009a2:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009a4:	eb be                	jmp    800964 <strtol+0x6f>

	if (endptr)
  8009a6:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009aa:	74 05                	je     8009b1 <strtol+0xbc>
		*endptr = (char *) s;
  8009ac:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009af:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009b1:	85 ff                	test   %edi,%edi
  8009b3:	74 21                	je     8009d6 <strtol+0xe1>
  8009b5:	f7 d8                	neg    %eax
  8009b7:	eb 1d                	jmp    8009d6 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009b9:	80 39 30             	cmpb   $0x30,(%ecx)
  8009bc:	75 9a                	jne    800958 <strtol+0x63>
  8009be:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009c2:	0f 84 7a ff ff ff    	je     800942 <strtol+0x4d>
  8009c8:	eb 84                	jmp    80094e <strtol+0x59>
  8009ca:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009ce:	0f 84 6e ff ff ff    	je     800942 <strtol+0x4d>
  8009d4:	eb 89                	jmp    80095f <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  8009d6:	5b                   	pop    %ebx
  8009d7:	5e                   	pop    %esi
  8009d8:	5f                   	pop    %edi
  8009d9:	5d                   	pop    %ebp
  8009da:	c3                   	ret    

008009db <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8009db:	55                   	push   %ebp
  8009dc:	89 e5                	mov    %esp,%ebp
  8009de:	57                   	push   %edi
  8009df:	56                   	push   %esi
  8009e0:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8009e1:	b8 00 00 00 00       	mov    $0x0,%eax
  8009e6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009e9:	8b 55 08             	mov    0x8(%ebp),%edx
  8009ec:	89 c3                	mov    %eax,%ebx
  8009ee:	89 c7                	mov    %eax,%edi
  8009f0:	89 c6                	mov    %eax,%esi
  8009f2:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8009f4:	5b                   	pop    %ebx
  8009f5:	5e                   	pop    %esi
  8009f6:	5f                   	pop    %edi
  8009f7:	5d                   	pop    %ebp
  8009f8:	c3                   	ret    

008009f9 <sys_cgetc>:

int
sys_cgetc(void)
{
  8009f9:	55                   	push   %ebp
  8009fa:	89 e5                	mov    %esp,%ebp
  8009fc:	57                   	push   %edi
  8009fd:	56                   	push   %esi
  8009fe:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8009ff:	ba 00 00 00 00       	mov    $0x0,%edx
  800a04:	b8 01 00 00 00       	mov    $0x1,%eax
  800a09:	89 d1                	mov    %edx,%ecx
  800a0b:	89 d3                	mov    %edx,%ebx
  800a0d:	89 d7                	mov    %edx,%edi
  800a0f:	89 d6                	mov    %edx,%esi
  800a11:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a13:	5b                   	pop    %ebx
  800a14:	5e                   	pop    %esi
  800a15:	5f                   	pop    %edi
  800a16:	5d                   	pop    %ebp
  800a17:	c3                   	ret    

00800a18 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a18:	55                   	push   %ebp
  800a19:	89 e5                	mov    %esp,%ebp
  800a1b:	57                   	push   %edi
  800a1c:	56                   	push   %esi
  800a1d:	53                   	push   %ebx
  800a1e:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a21:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a26:	b8 03 00 00 00       	mov    $0x3,%eax
  800a2b:	8b 55 08             	mov    0x8(%ebp),%edx
  800a2e:	89 cb                	mov    %ecx,%ebx
  800a30:	89 cf                	mov    %ecx,%edi
  800a32:	89 ce                	mov    %ecx,%esi
  800a34:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a36:	85 c0                	test   %eax,%eax
  800a38:	7e 17                	jle    800a51 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a3a:	83 ec 0c             	sub    $0xc,%esp
  800a3d:	50                   	push   %eax
  800a3e:	6a 03                	push   $0x3
  800a40:	68 40 0f 80 00       	push   $0x800f40
  800a45:	6a 23                	push   $0x23
  800a47:	68 5d 0f 80 00       	push   $0x800f5d
  800a4c:	e8 27 00 00 00       	call   800a78 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a51:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a54:	5b                   	pop    %ebx
  800a55:	5e                   	pop    %esi
  800a56:	5f                   	pop    %edi
  800a57:	5d                   	pop    %ebp
  800a58:	c3                   	ret    

00800a59 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a59:	55                   	push   %ebp
  800a5a:	89 e5                	mov    %esp,%ebp
  800a5c:	57                   	push   %edi
  800a5d:	56                   	push   %esi
  800a5e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a5f:	ba 00 00 00 00       	mov    $0x0,%edx
  800a64:	b8 02 00 00 00       	mov    $0x2,%eax
  800a69:	89 d1                	mov    %edx,%ecx
  800a6b:	89 d3                	mov    %edx,%ebx
  800a6d:	89 d7                	mov    %edx,%edi
  800a6f:	89 d6                	mov    %edx,%esi
  800a71:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800a73:	5b                   	pop    %ebx
  800a74:	5e                   	pop    %esi
  800a75:	5f                   	pop    %edi
  800a76:	5d                   	pop    %ebp
  800a77:	c3                   	ret    

00800a78 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800a78:	55                   	push   %ebp
  800a79:	89 e5                	mov    %esp,%ebp
  800a7b:	56                   	push   %esi
  800a7c:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800a7d:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800a80:	8b 35 00 10 80 00    	mov    0x801000,%esi
  800a86:	e8 ce ff ff ff       	call   800a59 <sys_getenvid>
  800a8b:	83 ec 0c             	sub    $0xc,%esp
  800a8e:	ff 75 0c             	pushl  0xc(%ebp)
  800a91:	ff 75 08             	pushl  0x8(%ebp)
  800a94:	56                   	push   %esi
  800a95:	50                   	push   %eax
  800a96:	68 6c 0f 80 00       	push   $0x800f6c
  800a9b:	e8 af f6 ff ff       	call   80014f <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800aa0:	83 c4 18             	add    $0x18,%esp
  800aa3:	53                   	push   %ebx
  800aa4:	ff 75 10             	pushl  0x10(%ebp)
  800aa7:	e8 52 f6 ff ff       	call   8000fe <vcprintf>
	cprintf("\n");
  800aac:	c7 04 24 30 0d 80 00 	movl   $0x800d30,(%esp)
  800ab3:	e8 97 f6 ff ff       	call   80014f <cprintf>
  800ab8:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800abb:	cc                   	int3   
  800abc:	eb fd                	jmp    800abb <_panic+0x43>
  800abe:	66 90                	xchg   %ax,%ax

00800ac0 <__udivdi3>:
  800ac0:	55                   	push   %ebp
  800ac1:	57                   	push   %edi
  800ac2:	56                   	push   %esi
  800ac3:	53                   	push   %ebx
  800ac4:	83 ec 1c             	sub    $0x1c,%esp
  800ac7:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800acb:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800acf:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800ad3:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800ad7:	89 ca                	mov    %ecx,%edx
  800ad9:	89 f8                	mov    %edi,%eax
  800adb:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800adf:	85 f6                	test   %esi,%esi
  800ae1:	75 2d                	jne    800b10 <__udivdi3+0x50>
  800ae3:	39 cf                	cmp    %ecx,%edi
  800ae5:	77 65                	ja     800b4c <__udivdi3+0x8c>
  800ae7:	89 fd                	mov    %edi,%ebp
  800ae9:	85 ff                	test   %edi,%edi
  800aeb:	75 0b                	jne    800af8 <__udivdi3+0x38>
  800aed:	b8 01 00 00 00       	mov    $0x1,%eax
  800af2:	31 d2                	xor    %edx,%edx
  800af4:	f7 f7                	div    %edi
  800af6:	89 c5                	mov    %eax,%ebp
  800af8:	31 d2                	xor    %edx,%edx
  800afa:	89 c8                	mov    %ecx,%eax
  800afc:	f7 f5                	div    %ebp
  800afe:	89 c1                	mov    %eax,%ecx
  800b00:	89 d8                	mov    %ebx,%eax
  800b02:	f7 f5                	div    %ebp
  800b04:	89 cf                	mov    %ecx,%edi
  800b06:	89 fa                	mov    %edi,%edx
  800b08:	83 c4 1c             	add    $0x1c,%esp
  800b0b:	5b                   	pop    %ebx
  800b0c:	5e                   	pop    %esi
  800b0d:	5f                   	pop    %edi
  800b0e:	5d                   	pop    %ebp
  800b0f:	c3                   	ret    
  800b10:	39 ce                	cmp    %ecx,%esi
  800b12:	77 28                	ja     800b3c <__udivdi3+0x7c>
  800b14:	0f bd fe             	bsr    %esi,%edi
  800b17:	83 f7 1f             	xor    $0x1f,%edi
  800b1a:	75 40                	jne    800b5c <__udivdi3+0x9c>
  800b1c:	39 ce                	cmp    %ecx,%esi
  800b1e:	72 0a                	jb     800b2a <__udivdi3+0x6a>
  800b20:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b24:	0f 87 9e 00 00 00    	ja     800bc8 <__udivdi3+0x108>
  800b2a:	b8 01 00 00 00       	mov    $0x1,%eax
  800b2f:	89 fa                	mov    %edi,%edx
  800b31:	83 c4 1c             	add    $0x1c,%esp
  800b34:	5b                   	pop    %ebx
  800b35:	5e                   	pop    %esi
  800b36:	5f                   	pop    %edi
  800b37:	5d                   	pop    %ebp
  800b38:	c3                   	ret    
  800b39:	8d 76 00             	lea    0x0(%esi),%esi
  800b3c:	31 ff                	xor    %edi,%edi
  800b3e:	31 c0                	xor    %eax,%eax
  800b40:	89 fa                	mov    %edi,%edx
  800b42:	83 c4 1c             	add    $0x1c,%esp
  800b45:	5b                   	pop    %ebx
  800b46:	5e                   	pop    %esi
  800b47:	5f                   	pop    %edi
  800b48:	5d                   	pop    %ebp
  800b49:	c3                   	ret    
  800b4a:	66 90                	xchg   %ax,%ax
  800b4c:	89 d8                	mov    %ebx,%eax
  800b4e:	f7 f7                	div    %edi
  800b50:	31 ff                	xor    %edi,%edi
  800b52:	89 fa                	mov    %edi,%edx
  800b54:	83 c4 1c             	add    $0x1c,%esp
  800b57:	5b                   	pop    %ebx
  800b58:	5e                   	pop    %esi
  800b59:	5f                   	pop    %edi
  800b5a:	5d                   	pop    %ebp
  800b5b:	c3                   	ret    
  800b5c:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b61:	89 eb                	mov    %ebp,%ebx
  800b63:	29 fb                	sub    %edi,%ebx
  800b65:	89 f9                	mov    %edi,%ecx
  800b67:	d3 e6                	shl    %cl,%esi
  800b69:	89 c5                	mov    %eax,%ebp
  800b6b:	88 d9                	mov    %bl,%cl
  800b6d:	d3 ed                	shr    %cl,%ebp
  800b6f:	89 e9                	mov    %ebp,%ecx
  800b71:	09 f1                	or     %esi,%ecx
  800b73:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b77:	89 f9                	mov    %edi,%ecx
  800b79:	d3 e0                	shl    %cl,%eax
  800b7b:	89 c5                	mov    %eax,%ebp
  800b7d:	89 d6                	mov    %edx,%esi
  800b7f:	88 d9                	mov    %bl,%cl
  800b81:	d3 ee                	shr    %cl,%esi
  800b83:	89 f9                	mov    %edi,%ecx
  800b85:	d3 e2                	shl    %cl,%edx
  800b87:	8b 44 24 08          	mov    0x8(%esp),%eax
  800b8b:	88 d9                	mov    %bl,%cl
  800b8d:	d3 e8                	shr    %cl,%eax
  800b8f:	09 c2                	or     %eax,%edx
  800b91:	89 d0                	mov    %edx,%eax
  800b93:	89 f2                	mov    %esi,%edx
  800b95:	f7 74 24 0c          	divl   0xc(%esp)
  800b99:	89 d6                	mov    %edx,%esi
  800b9b:	89 c3                	mov    %eax,%ebx
  800b9d:	f7 e5                	mul    %ebp
  800b9f:	39 d6                	cmp    %edx,%esi
  800ba1:	72 19                	jb     800bbc <__udivdi3+0xfc>
  800ba3:	74 0b                	je     800bb0 <__udivdi3+0xf0>
  800ba5:	89 d8                	mov    %ebx,%eax
  800ba7:	31 ff                	xor    %edi,%edi
  800ba9:	e9 58 ff ff ff       	jmp    800b06 <__udivdi3+0x46>
  800bae:	66 90                	xchg   %ax,%ax
  800bb0:	8b 54 24 08          	mov    0x8(%esp),%edx
  800bb4:	89 f9                	mov    %edi,%ecx
  800bb6:	d3 e2                	shl    %cl,%edx
  800bb8:	39 c2                	cmp    %eax,%edx
  800bba:	73 e9                	jae    800ba5 <__udivdi3+0xe5>
  800bbc:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800bbf:	31 ff                	xor    %edi,%edi
  800bc1:	e9 40 ff ff ff       	jmp    800b06 <__udivdi3+0x46>
  800bc6:	66 90                	xchg   %ax,%ax
  800bc8:	31 c0                	xor    %eax,%eax
  800bca:	e9 37 ff ff ff       	jmp    800b06 <__udivdi3+0x46>
  800bcf:	90                   	nop

00800bd0 <__umoddi3>:
  800bd0:	55                   	push   %ebp
  800bd1:	57                   	push   %edi
  800bd2:	56                   	push   %esi
  800bd3:	53                   	push   %ebx
  800bd4:	83 ec 1c             	sub    $0x1c,%esp
  800bd7:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bdb:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bdf:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800be3:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800be7:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800beb:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800bef:	89 f3                	mov    %esi,%ebx
  800bf1:	89 fa                	mov    %edi,%edx
  800bf3:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800bf7:	89 34 24             	mov    %esi,(%esp)
  800bfa:	85 c0                	test   %eax,%eax
  800bfc:	75 1a                	jne    800c18 <__umoddi3+0x48>
  800bfe:	39 f7                	cmp    %esi,%edi
  800c00:	0f 86 a2 00 00 00    	jbe    800ca8 <__umoddi3+0xd8>
  800c06:	89 c8                	mov    %ecx,%eax
  800c08:	89 f2                	mov    %esi,%edx
  800c0a:	f7 f7                	div    %edi
  800c0c:	89 d0                	mov    %edx,%eax
  800c0e:	31 d2                	xor    %edx,%edx
  800c10:	83 c4 1c             	add    $0x1c,%esp
  800c13:	5b                   	pop    %ebx
  800c14:	5e                   	pop    %esi
  800c15:	5f                   	pop    %edi
  800c16:	5d                   	pop    %ebp
  800c17:	c3                   	ret    
  800c18:	39 f0                	cmp    %esi,%eax
  800c1a:	0f 87 ac 00 00 00    	ja     800ccc <__umoddi3+0xfc>
  800c20:	0f bd e8             	bsr    %eax,%ebp
  800c23:	83 f5 1f             	xor    $0x1f,%ebp
  800c26:	0f 84 ac 00 00 00    	je     800cd8 <__umoddi3+0x108>
  800c2c:	bf 20 00 00 00       	mov    $0x20,%edi
  800c31:	29 ef                	sub    %ebp,%edi
  800c33:	89 fe                	mov    %edi,%esi
  800c35:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c39:	89 e9                	mov    %ebp,%ecx
  800c3b:	d3 e0                	shl    %cl,%eax
  800c3d:	89 d7                	mov    %edx,%edi
  800c3f:	89 f1                	mov    %esi,%ecx
  800c41:	d3 ef                	shr    %cl,%edi
  800c43:	09 c7                	or     %eax,%edi
  800c45:	89 e9                	mov    %ebp,%ecx
  800c47:	d3 e2                	shl    %cl,%edx
  800c49:	89 14 24             	mov    %edx,(%esp)
  800c4c:	89 d8                	mov    %ebx,%eax
  800c4e:	d3 e0                	shl    %cl,%eax
  800c50:	89 c2                	mov    %eax,%edx
  800c52:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c56:	d3 e0                	shl    %cl,%eax
  800c58:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c5c:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c60:	89 f1                	mov    %esi,%ecx
  800c62:	d3 e8                	shr    %cl,%eax
  800c64:	09 d0                	or     %edx,%eax
  800c66:	d3 eb                	shr    %cl,%ebx
  800c68:	89 da                	mov    %ebx,%edx
  800c6a:	f7 f7                	div    %edi
  800c6c:	89 d3                	mov    %edx,%ebx
  800c6e:	f7 24 24             	mull   (%esp)
  800c71:	89 c6                	mov    %eax,%esi
  800c73:	89 d1                	mov    %edx,%ecx
  800c75:	39 d3                	cmp    %edx,%ebx
  800c77:	0f 82 87 00 00 00    	jb     800d04 <__umoddi3+0x134>
  800c7d:	0f 84 91 00 00 00    	je     800d14 <__umoddi3+0x144>
  800c83:	8b 54 24 04          	mov    0x4(%esp),%edx
  800c87:	29 f2                	sub    %esi,%edx
  800c89:	19 cb                	sbb    %ecx,%ebx
  800c8b:	89 d8                	mov    %ebx,%eax
  800c8d:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800c91:	d3 e0                	shl    %cl,%eax
  800c93:	89 e9                	mov    %ebp,%ecx
  800c95:	d3 ea                	shr    %cl,%edx
  800c97:	09 d0                	or     %edx,%eax
  800c99:	89 e9                	mov    %ebp,%ecx
  800c9b:	d3 eb                	shr    %cl,%ebx
  800c9d:	89 da                	mov    %ebx,%edx
  800c9f:	83 c4 1c             	add    $0x1c,%esp
  800ca2:	5b                   	pop    %ebx
  800ca3:	5e                   	pop    %esi
  800ca4:	5f                   	pop    %edi
  800ca5:	5d                   	pop    %ebp
  800ca6:	c3                   	ret    
  800ca7:	90                   	nop
  800ca8:	89 fd                	mov    %edi,%ebp
  800caa:	85 ff                	test   %edi,%edi
  800cac:	75 0b                	jne    800cb9 <__umoddi3+0xe9>
  800cae:	b8 01 00 00 00       	mov    $0x1,%eax
  800cb3:	31 d2                	xor    %edx,%edx
  800cb5:	f7 f7                	div    %edi
  800cb7:	89 c5                	mov    %eax,%ebp
  800cb9:	89 f0                	mov    %esi,%eax
  800cbb:	31 d2                	xor    %edx,%edx
  800cbd:	f7 f5                	div    %ebp
  800cbf:	89 c8                	mov    %ecx,%eax
  800cc1:	f7 f5                	div    %ebp
  800cc3:	89 d0                	mov    %edx,%eax
  800cc5:	e9 44 ff ff ff       	jmp    800c0e <__umoddi3+0x3e>
  800cca:	66 90                	xchg   %ax,%ax
  800ccc:	89 c8                	mov    %ecx,%eax
  800cce:	89 f2                	mov    %esi,%edx
  800cd0:	83 c4 1c             	add    $0x1c,%esp
  800cd3:	5b                   	pop    %ebx
  800cd4:	5e                   	pop    %esi
  800cd5:	5f                   	pop    %edi
  800cd6:	5d                   	pop    %ebp
  800cd7:	c3                   	ret    
  800cd8:	3b 04 24             	cmp    (%esp),%eax
  800cdb:	72 06                	jb     800ce3 <__umoddi3+0x113>
  800cdd:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800ce1:	77 0f                	ja     800cf2 <__umoddi3+0x122>
  800ce3:	89 f2                	mov    %esi,%edx
  800ce5:	29 f9                	sub    %edi,%ecx
  800ce7:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800ceb:	89 14 24             	mov    %edx,(%esp)
  800cee:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800cf2:	8b 44 24 04          	mov    0x4(%esp),%eax
  800cf6:	8b 14 24             	mov    (%esp),%edx
  800cf9:	83 c4 1c             	add    $0x1c,%esp
  800cfc:	5b                   	pop    %ebx
  800cfd:	5e                   	pop    %esi
  800cfe:	5f                   	pop    %edi
  800cff:	5d                   	pop    %ebp
  800d00:	c3                   	ret    
  800d01:	8d 76 00             	lea    0x0(%esi),%esi
  800d04:	2b 04 24             	sub    (%esp),%eax
  800d07:	19 fa                	sbb    %edi,%edx
  800d09:	89 d1                	mov    %edx,%ecx
  800d0b:	89 c6                	mov    %eax,%esi
  800d0d:	e9 71 ff ff ff       	jmp    800c83 <__umoddi3+0xb3>
  800d12:	66 90                	xchg   %ax,%ax
  800d14:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d18:	72 ea                	jb     800d04 <__umoddi3+0x134>
  800d1a:	89 d9                	mov    %ebx,%ecx
  800d1c:	e9 62 ff ff ff       	jmp    800c83 <__umoddi3+0xb3>
