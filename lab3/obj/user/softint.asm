
obj/user/softint:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 09 00 00 00       	call   80003a <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	asm volatile("int $14");	// page fault
  800036:	cd 0e                	int    $0xe
}
  800038:	5d                   	pop    %ebp
  800039:	c3                   	ret    

0080003a <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80003a:	55                   	push   %ebp
  80003b:	89 e5                	mov    %esp,%ebp
  80003d:	56                   	push   %esi
  80003e:	53                   	push   %ebx
  80003f:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800042:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800045:	e8 cb 00 00 00       	call   800115 <sys_getenvid>
  80004a:	25 ff 03 00 00       	and    $0x3ff,%eax
  80004f:	8d 14 00             	lea    (%eax,%eax,1),%edx
  800052:	01 d0                	add    %edx,%eax
  800054:	c1 e0 05             	shl    $0x5,%eax
  800057:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80005c:	a3 04 10 80 00       	mov    %eax,0x801004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800061:	85 db                	test   %ebx,%ebx
  800063:	7e 07                	jle    80006c <libmain+0x32>
		binaryname = argv[0];
  800065:	8b 06                	mov    (%esi),%eax
  800067:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  80006c:	83 ec 08             	sub    $0x8,%esp
  80006f:	56                   	push   %esi
  800070:	53                   	push   %ebx
  800071:	e8 bd ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800076:	e8 0a 00 00 00       	call   800085 <exit>
}
  80007b:	83 c4 10             	add    $0x10,%esp
  80007e:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800081:	5b                   	pop    %ebx
  800082:	5e                   	pop    %esi
  800083:	5d                   	pop    %ebp
  800084:	c3                   	ret    

00800085 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800085:	55                   	push   %ebp
  800086:	89 e5                	mov    %esp,%ebp
  800088:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  80008b:	6a 00                	push   $0x0
  80008d:	e8 42 00 00 00       	call   8000d4 <sys_env_destroy>
}
  800092:	83 c4 10             	add    $0x10,%esp
  800095:	c9                   	leave  
  800096:	c3                   	ret    

00800097 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800097:	55                   	push   %ebp
  800098:	89 e5                	mov    %esp,%ebp
  80009a:	57                   	push   %edi
  80009b:	56                   	push   %esi
  80009c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80009d:	b8 00 00 00 00       	mov    $0x0,%eax
  8000a2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000a5:	8b 55 08             	mov    0x8(%ebp),%edx
  8000a8:	89 c3                	mov    %eax,%ebx
  8000aa:	89 c7                	mov    %eax,%edi
  8000ac:	89 c6                	mov    %eax,%esi
  8000ae:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000b0:	5b                   	pop    %ebx
  8000b1:	5e                   	pop    %esi
  8000b2:	5f                   	pop    %edi
  8000b3:	5d                   	pop    %ebp
  8000b4:	c3                   	ret    

008000b5 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000b5:	55                   	push   %ebp
  8000b6:	89 e5                	mov    %esp,%ebp
  8000b8:	57                   	push   %edi
  8000b9:	56                   	push   %esi
  8000ba:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000bb:	ba 00 00 00 00       	mov    $0x0,%edx
  8000c0:	b8 01 00 00 00       	mov    $0x1,%eax
  8000c5:	89 d1                	mov    %edx,%ecx
  8000c7:	89 d3                	mov    %edx,%ebx
  8000c9:	89 d7                	mov    %edx,%edi
  8000cb:	89 d6                	mov    %edx,%esi
  8000cd:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000cf:	5b                   	pop    %ebx
  8000d0:	5e                   	pop    %esi
  8000d1:	5f                   	pop    %edi
  8000d2:	5d                   	pop    %ebp
  8000d3:	c3                   	ret    

008000d4 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000d4:	55                   	push   %ebp
  8000d5:	89 e5                	mov    %esp,%ebp
  8000d7:	57                   	push   %edi
  8000d8:	56                   	push   %esi
  8000d9:	53                   	push   %ebx
  8000da:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000dd:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000e2:	b8 03 00 00 00       	mov    $0x3,%eax
  8000e7:	8b 55 08             	mov    0x8(%ebp),%edx
  8000ea:	89 cb                	mov    %ecx,%ebx
  8000ec:	89 cf                	mov    %ecx,%edi
  8000ee:	89 ce                	mov    %ecx,%esi
  8000f0:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8000f2:	85 c0                	test   %eax,%eax
  8000f4:	7e 17                	jle    80010d <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  8000f6:	83 ec 0c             	sub    $0xc,%esp
  8000f9:	50                   	push   %eax
  8000fa:	6a 03                	push   $0x3
  8000fc:	68 06 0d 80 00       	push   $0x800d06
  800101:	6a 23                	push   $0x23
  800103:	68 23 0d 80 00       	push   $0x800d23
  800108:	e8 27 00 00 00       	call   800134 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  80010d:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800110:	5b                   	pop    %ebx
  800111:	5e                   	pop    %esi
  800112:	5f                   	pop    %edi
  800113:	5d                   	pop    %ebp
  800114:	c3                   	ret    

00800115 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800115:	55                   	push   %ebp
  800116:	89 e5                	mov    %esp,%ebp
  800118:	57                   	push   %edi
  800119:	56                   	push   %esi
  80011a:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80011b:	ba 00 00 00 00       	mov    $0x0,%edx
  800120:	b8 02 00 00 00       	mov    $0x2,%eax
  800125:	89 d1                	mov    %edx,%ecx
  800127:	89 d3                	mov    %edx,%ebx
  800129:	89 d7                	mov    %edx,%edi
  80012b:	89 d6                	mov    %edx,%esi
  80012d:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  80012f:	5b                   	pop    %ebx
  800130:	5e                   	pop    %esi
  800131:	5f                   	pop    %edi
  800132:	5d                   	pop    %ebp
  800133:	c3                   	ret    

00800134 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800134:	55                   	push   %ebp
  800135:	89 e5                	mov    %esp,%ebp
  800137:	56                   	push   %esi
  800138:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800139:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80013c:	8b 35 00 10 80 00    	mov    0x801000,%esi
  800142:	e8 ce ff ff ff       	call   800115 <sys_getenvid>
  800147:	83 ec 0c             	sub    $0xc,%esp
  80014a:	ff 75 0c             	pushl  0xc(%ebp)
  80014d:	ff 75 08             	pushl  0x8(%ebp)
  800150:	56                   	push   %esi
  800151:	50                   	push   %eax
  800152:	68 34 0d 80 00       	push   $0x800d34
  800157:	e8 b0 00 00 00       	call   80020c <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80015c:	83 c4 18             	add    $0x18,%esp
  80015f:	53                   	push   %ebx
  800160:	ff 75 10             	pushl  0x10(%ebp)
  800163:	e8 53 00 00 00       	call   8001bb <vcprintf>
	cprintf("\n");
  800168:	c7 04 24 58 0d 80 00 	movl   $0x800d58,(%esp)
  80016f:	e8 98 00 00 00       	call   80020c <cprintf>
  800174:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800177:	cc                   	int3   
  800178:	eb fd                	jmp    800177 <_panic+0x43>

0080017a <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80017a:	55                   	push   %ebp
  80017b:	89 e5                	mov    %esp,%ebp
  80017d:	53                   	push   %ebx
  80017e:	83 ec 04             	sub    $0x4,%esp
  800181:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800184:	8b 13                	mov    (%ebx),%edx
  800186:	8d 42 01             	lea    0x1(%edx),%eax
  800189:	89 03                	mov    %eax,(%ebx)
  80018b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80018e:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800192:	3d ff 00 00 00       	cmp    $0xff,%eax
  800197:	75 1a                	jne    8001b3 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800199:	83 ec 08             	sub    $0x8,%esp
  80019c:	68 ff 00 00 00       	push   $0xff
  8001a1:	8d 43 08             	lea    0x8(%ebx),%eax
  8001a4:	50                   	push   %eax
  8001a5:	e8 ed fe ff ff       	call   800097 <sys_cputs>
		b->idx = 0;
  8001aa:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8001b0:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8001b3:	ff 43 04             	incl   0x4(%ebx)
}
  8001b6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001b9:	c9                   	leave  
  8001ba:	c3                   	ret    

008001bb <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8001bb:	55                   	push   %ebp
  8001bc:	89 e5                	mov    %esp,%ebp
  8001be:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001c4:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001cb:	00 00 00 
	b.cnt = 0;
  8001ce:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001d5:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001d8:	ff 75 0c             	pushl  0xc(%ebp)
  8001db:	ff 75 08             	pushl  0x8(%ebp)
  8001de:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001e4:	50                   	push   %eax
  8001e5:	68 7a 01 80 00       	push   $0x80017a
  8001ea:	e8 51 01 00 00       	call   800340 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001ef:	83 c4 08             	add    $0x8,%esp
  8001f2:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8001f8:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8001fe:	50                   	push   %eax
  8001ff:	e8 93 fe ff ff       	call   800097 <sys_cputs>

	return b.cnt;
}
  800204:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80020a:	c9                   	leave  
  80020b:	c3                   	ret    

0080020c <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80020c:	55                   	push   %ebp
  80020d:	89 e5                	mov    %esp,%ebp
  80020f:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800212:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800215:	50                   	push   %eax
  800216:	ff 75 08             	pushl  0x8(%ebp)
  800219:	e8 9d ff ff ff       	call   8001bb <vcprintf>
	va_end(ap);

	return cnt;
}
  80021e:	c9                   	leave  
  80021f:	c3                   	ret    

00800220 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800220:	55                   	push   %ebp
  800221:	89 e5                	mov    %esp,%ebp
  800223:	57                   	push   %edi
  800224:	56                   	push   %esi
  800225:	53                   	push   %ebx
  800226:	83 ec 1c             	sub    $0x1c,%esp
  800229:	89 c7                	mov    %eax,%edi
  80022b:	89 d6                	mov    %edx,%esi
  80022d:	8b 45 08             	mov    0x8(%ebp),%eax
  800230:	8b 55 0c             	mov    0xc(%ebp),%edx
  800233:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800236:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800239:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80023c:	bb 00 00 00 00       	mov    $0x0,%ebx
  800241:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800244:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800247:	39 d3                	cmp    %edx,%ebx
  800249:	72 05                	jb     800250 <printnum+0x30>
  80024b:	39 45 10             	cmp    %eax,0x10(%ebp)
  80024e:	77 45                	ja     800295 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800250:	83 ec 0c             	sub    $0xc,%esp
  800253:	ff 75 18             	pushl  0x18(%ebp)
  800256:	8b 45 14             	mov    0x14(%ebp),%eax
  800259:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80025c:	53                   	push   %ebx
  80025d:	ff 75 10             	pushl  0x10(%ebp)
  800260:	83 ec 08             	sub    $0x8,%esp
  800263:	ff 75 e4             	pushl  -0x1c(%ebp)
  800266:	ff 75 e0             	pushl  -0x20(%ebp)
  800269:	ff 75 dc             	pushl  -0x24(%ebp)
  80026c:	ff 75 d8             	pushl  -0x28(%ebp)
  80026f:	e8 24 08 00 00       	call   800a98 <__udivdi3>
  800274:	83 c4 18             	add    $0x18,%esp
  800277:	52                   	push   %edx
  800278:	50                   	push   %eax
  800279:	89 f2                	mov    %esi,%edx
  80027b:	89 f8                	mov    %edi,%eax
  80027d:	e8 9e ff ff ff       	call   800220 <printnum>
  800282:	83 c4 20             	add    $0x20,%esp
  800285:	eb 16                	jmp    80029d <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800287:	83 ec 08             	sub    $0x8,%esp
  80028a:	56                   	push   %esi
  80028b:	ff 75 18             	pushl  0x18(%ebp)
  80028e:	ff d7                	call   *%edi
  800290:	83 c4 10             	add    $0x10,%esp
  800293:	eb 03                	jmp    800298 <printnum+0x78>
  800295:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800298:	4b                   	dec    %ebx
  800299:	85 db                	test   %ebx,%ebx
  80029b:	7f ea                	jg     800287 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80029d:	83 ec 08             	sub    $0x8,%esp
  8002a0:	56                   	push   %esi
  8002a1:	83 ec 04             	sub    $0x4,%esp
  8002a4:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002a7:	ff 75 e0             	pushl  -0x20(%ebp)
  8002aa:	ff 75 dc             	pushl  -0x24(%ebp)
  8002ad:	ff 75 d8             	pushl  -0x28(%ebp)
  8002b0:	e8 f3 08 00 00       	call   800ba8 <__umoddi3>
  8002b5:	83 c4 14             	add    $0x14,%esp
  8002b8:	0f be 80 5a 0d 80 00 	movsbl 0x800d5a(%eax),%eax
  8002bf:	50                   	push   %eax
  8002c0:	ff d7                	call   *%edi
}
  8002c2:	83 c4 10             	add    $0x10,%esp
  8002c5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002c8:	5b                   	pop    %ebx
  8002c9:	5e                   	pop    %esi
  8002ca:	5f                   	pop    %edi
  8002cb:	5d                   	pop    %ebp
  8002cc:	c3                   	ret    

008002cd <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002cd:	55                   	push   %ebp
  8002ce:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002d0:	83 fa 01             	cmp    $0x1,%edx
  8002d3:	7e 0e                	jle    8002e3 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002d5:	8b 10                	mov    (%eax),%edx
  8002d7:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002da:	89 08                	mov    %ecx,(%eax)
  8002dc:	8b 02                	mov    (%edx),%eax
  8002de:	8b 52 04             	mov    0x4(%edx),%edx
  8002e1:	eb 22                	jmp    800305 <getuint+0x38>
	else if (lflag)
  8002e3:	85 d2                	test   %edx,%edx
  8002e5:	74 10                	je     8002f7 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8002e7:	8b 10                	mov    (%eax),%edx
  8002e9:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002ec:	89 08                	mov    %ecx,(%eax)
  8002ee:	8b 02                	mov    (%edx),%eax
  8002f0:	ba 00 00 00 00       	mov    $0x0,%edx
  8002f5:	eb 0e                	jmp    800305 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8002f7:	8b 10                	mov    (%eax),%edx
  8002f9:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002fc:	89 08                	mov    %ecx,(%eax)
  8002fe:	8b 02                	mov    (%edx),%eax
  800300:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800305:	5d                   	pop    %ebp
  800306:	c3                   	ret    

00800307 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800307:	55                   	push   %ebp
  800308:	89 e5                	mov    %esp,%ebp
  80030a:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80030d:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800310:	8b 10                	mov    (%eax),%edx
  800312:	3b 50 04             	cmp    0x4(%eax),%edx
  800315:	73 0a                	jae    800321 <sprintputch+0x1a>
		*b->buf++ = ch;
  800317:	8d 4a 01             	lea    0x1(%edx),%ecx
  80031a:	89 08                	mov    %ecx,(%eax)
  80031c:	8b 45 08             	mov    0x8(%ebp),%eax
  80031f:	88 02                	mov    %al,(%edx)
}
  800321:	5d                   	pop    %ebp
  800322:	c3                   	ret    

00800323 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800323:	55                   	push   %ebp
  800324:	89 e5                	mov    %esp,%ebp
  800326:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800329:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80032c:	50                   	push   %eax
  80032d:	ff 75 10             	pushl  0x10(%ebp)
  800330:	ff 75 0c             	pushl  0xc(%ebp)
  800333:	ff 75 08             	pushl  0x8(%ebp)
  800336:	e8 05 00 00 00       	call   800340 <vprintfmt>
	va_end(ap);
}
  80033b:	83 c4 10             	add    $0x10,%esp
  80033e:	c9                   	leave  
  80033f:	c3                   	ret    

00800340 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800340:	55                   	push   %ebp
  800341:	89 e5                	mov    %esp,%ebp
  800343:	57                   	push   %edi
  800344:	56                   	push   %esi
  800345:	53                   	push   %ebx
  800346:	83 ec 2c             	sub    $0x2c,%esp
  800349:	8b 75 08             	mov    0x8(%ebp),%esi
  80034c:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80034f:	8b 7d 10             	mov    0x10(%ebp),%edi
  800352:	eb 12                	jmp    800366 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800354:	85 c0                	test   %eax,%eax
  800356:	0f 84 68 03 00 00    	je     8006c4 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80035c:	83 ec 08             	sub    $0x8,%esp
  80035f:	53                   	push   %ebx
  800360:	50                   	push   %eax
  800361:	ff d6                	call   *%esi
  800363:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800366:	47                   	inc    %edi
  800367:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80036b:	83 f8 25             	cmp    $0x25,%eax
  80036e:	75 e4                	jne    800354 <vprintfmt+0x14>
  800370:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800374:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80037b:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800382:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800389:	ba 00 00 00 00       	mov    $0x0,%edx
  80038e:	eb 07                	jmp    800397 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800390:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800393:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800397:	8d 47 01             	lea    0x1(%edi),%eax
  80039a:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80039d:	0f b6 0f             	movzbl (%edi),%ecx
  8003a0:	8a 07                	mov    (%edi),%al
  8003a2:	83 e8 23             	sub    $0x23,%eax
  8003a5:	3c 55                	cmp    $0x55,%al
  8003a7:	0f 87 fe 02 00 00    	ja     8006ab <vprintfmt+0x36b>
  8003ad:	0f b6 c0             	movzbl %al,%eax
  8003b0:	ff 24 85 e8 0d 80 00 	jmp    *0x800de8(,%eax,4)
  8003b7:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003ba:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003be:	eb d7                	jmp    800397 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003c0:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003c3:	b8 00 00 00 00       	mov    $0x0,%eax
  8003c8:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003cb:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003ce:	01 c0                	add    %eax,%eax
  8003d0:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003d4:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003d7:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003da:	83 fa 09             	cmp    $0x9,%edx
  8003dd:	77 34                	ja     800413 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003df:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003e0:	eb e9                	jmp    8003cb <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003e2:	8b 45 14             	mov    0x14(%ebp),%eax
  8003e5:	8d 48 04             	lea    0x4(%eax),%ecx
  8003e8:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8003eb:	8b 00                	mov    (%eax),%eax
  8003ed:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f0:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8003f3:	eb 24                	jmp    800419 <vprintfmt+0xd9>
  8003f5:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003f9:	79 07                	jns    800402 <vprintfmt+0xc2>
  8003fb:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800402:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800405:	eb 90                	jmp    800397 <vprintfmt+0x57>
  800407:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80040a:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800411:	eb 84                	jmp    800397 <vprintfmt+0x57>
  800413:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800416:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800419:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80041d:	0f 89 74 ff ff ff    	jns    800397 <vprintfmt+0x57>
				width = precision, precision = -1;
  800423:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800426:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800429:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800430:	e9 62 ff ff ff       	jmp    800397 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800435:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800436:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800439:	e9 59 ff ff ff       	jmp    800397 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80043e:	8b 45 14             	mov    0x14(%ebp),%eax
  800441:	8d 50 04             	lea    0x4(%eax),%edx
  800444:	89 55 14             	mov    %edx,0x14(%ebp)
  800447:	83 ec 08             	sub    $0x8,%esp
  80044a:	53                   	push   %ebx
  80044b:	ff 30                	pushl  (%eax)
  80044d:	ff d6                	call   *%esi
			break;
  80044f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800452:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800455:	e9 0c ff ff ff       	jmp    800366 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80045a:	8b 45 14             	mov    0x14(%ebp),%eax
  80045d:	8d 50 04             	lea    0x4(%eax),%edx
  800460:	89 55 14             	mov    %edx,0x14(%ebp)
  800463:	8b 00                	mov    (%eax),%eax
  800465:	85 c0                	test   %eax,%eax
  800467:	79 02                	jns    80046b <vprintfmt+0x12b>
  800469:	f7 d8                	neg    %eax
  80046b:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80046d:	83 f8 06             	cmp    $0x6,%eax
  800470:	7f 0b                	jg     80047d <vprintfmt+0x13d>
  800472:	8b 04 85 40 0f 80 00 	mov    0x800f40(,%eax,4),%eax
  800479:	85 c0                	test   %eax,%eax
  80047b:	75 18                	jne    800495 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80047d:	52                   	push   %edx
  80047e:	68 72 0d 80 00       	push   $0x800d72
  800483:	53                   	push   %ebx
  800484:	56                   	push   %esi
  800485:	e8 99 fe ff ff       	call   800323 <printfmt>
  80048a:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80048d:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800490:	e9 d1 fe ff ff       	jmp    800366 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800495:	50                   	push   %eax
  800496:	68 7b 0d 80 00       	push   $0x800d7b
  80049b:	53                   	push   %ebx
  80049c:	56                   	push   %esi
  80049d:	e8 81 fe ff ff       	call   800323 <printfmt>
  8004a2:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004a5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004a8:	e9 b9 fe ff ff       	jmp    800366 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004ad:	8b 45 14             	mov    0x14(%ebp),%eax
  8004b0:	8d 50 04             	lea    0x4(%eax),%edx
  8004b3:	89 55 14             	mov    %edx,0x14(%ebp)
  8004b6:	8b 38                	mov    (%eax),%edi
  8004b8:	85 ff                	test   %edi,%edi
  8004ba:	75 05                	jne    8004c1 <vprintfmt+0x181>
				p = "(null)";
  8004bc:	bf 6b 0d 80 00       	mov    $0x800d6b,%edi
			if (width > 0 && padc != '-')
  8004c1:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004c5:	0f 8e 90 00 00 00    	jle    80055b <vprintfmt+0x21b>
  8004cb:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004cf:	0f 84 8e 00 00 00    	je     800563 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004d5:	83 ec 08             	sub    $0x8,%esp
  8004d8:	ff 75 d0             	pushl  -0x30(%ebp)
  8004db:	57                   	push   %edi
  8004dc:	e8 70 02 00 00       	call   800751 <strnlen>
  8004e1:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8004e4:	29 c1                	sub    %eax,%ecx
  8004e6:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8004e9:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8004ec:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8004f0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004f3:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8004f6:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004f8:	eb 0d                	jmp    800507 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8004fa:	83 ec 08             	sub    $0x8,%esp
  8004fd:	53                   	push   %ebx
  8004fe:	ff 75 e4             	pushl  -0x1c(%ebp)
  800501:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800503:	4f                   	dec    %edi
  800504:	83 c4 10             	add    $0x10,%esp
  800507:	85 ff                	test   %edi,%edi
  800509:	7f ef                	jg     8004fa <vprintfmt+0x1ba>
  80050b:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80050e:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800511:	89 c8                	mov    %ecx,%eax
  800513:	85 c9                	test   %ecx,%ecx
  800515:	79 05                	jns    80051c <vprintfmt+0x1dc>
  800517:	b8 00 00 00 00       	mov    $0x0,%eax
  80051c:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80051f:	29 c1                	sub    %eax,%ecx
  800521:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800524:	89 75 08             	mov    %esi,0x8(%ebp)
  800527:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80052a:	eb 3d                	jmp    800569 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80052c:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800530:	74 19                	je     80054b <vprintfmt+0x20b>
  800532:	0f be c0             	movsbl %al,%eax
  800535:	83 e8 20             	sub    $0x20,%eax
  800538:	83 f8 5e             	cmp    $0x5e,%eax
  80053b:	76 0e                	jbe    80054b <vprintfmt+0x20b>
					putch('?', putdat);
  80053d:	83 ec 08             	sub    $0x8,%esp
  800540:	53                   	push   %ebx
  800541:	6a 3f                	push   $0x3f
  800543:	ff 55 08             	call   *0x8(%ebp)
  800546:	83 c4 10             	add    $0x10,%esp
  800549:	eb 0b                	jmp    800556 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80054b:	83 ec 08             	sub    $0x8,%esp
  80054e:	53                   	push   %ebx
  80054f:	52                   	push   %edx
  800550:	ff 55 08             	call   *0x8(%ebp)
  800553:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800556:	ff 4d e4             	decl   -0x1c(%ebp)
  800559:	eb 0e                	jmp    800569 <vprintfmt+0x229>
  80055b:	89 75 08             	mov    %esi,0x8(%ebp)
  80055e:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800561:	eb 06                	jmp    800569 <vprintfmt+0x229>
  800563:	89 75 08             	mov    %esi,0x8(%ebp)
  800566:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800569:	47                   	inc    %edi
  80056a:	8a 47 ff             	mov    -0x1(%edi),%al
  80056d:	0f be d0             	movsbl %al,%edx
  800570:	85 d2                	test   %edx,%edx
  800572:	74 1d                	je     800591 <vprintfmt+0x251>
  800574:	85 f6                	test   %esi,%esi
  800576:	78 b4                	js     80052c <vprintfmt+0x1ec>
  800578:	4e                   	dec    %esi
  800579:	79 b1                	jns    80052c <vprintfmt+0x1ec>
  80057b:	8b 75 08             	mov    0x8(%ebp),%esi
  80057e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800581:	eb 14                	jmp    800597 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800583:	83 ec 08             	sub    $0x8,%esp
  800586:	53                   	push   %ebx
  800587:	6a 20                	push   $0x20
  800589:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80058b:	4f                   	dec    %edi
  80058c:	83 c4 10             	add    $0x10,%esp
  80058f:	eb 06                	jmp    800597 <vprintfmt+0x257>
  800591:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800594:	8b 75 08             	mov    0x8(%ebp),%esi
  800597:	85 ff                	test   %edi,%edi
  800599:	7f e8                	jg     800583 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80059b:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80059e:	e9 c3 fd ff ff       	jmp    800366 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8005a3:	83 fa 01             	cmp    $0x1,%edx
  8005a6:	7e 16                	jle    8005be <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8005a8:	8b 45 14             	mov    0x14(%ebp),%eax
  8005ab:	8d 50 08             	lea    0x8(%eax),%edx
  8005ae:	89 55 14             	mov    %edx,0x14(%ebp)
  8005b1:	8b 50 04             	mov    0x4(%eax),%edx
  8005b4:	8b 00                	mov    (%eax),%eax
  8005b6:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005b9:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8005bc:	eb 32                	jmp    8005f0 <vprintfmt+0x2b0>
	else if (lflag)
  8005be:	85 d2                	test   %edx,%edx
  8005c0:	74 18                	je     8005da <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005c2:	8b 45 14             	mov    0x14(%ebp),%eax
  8005c5:	8d 50 04             	lea    0x4(%eax),%edx
  8005c8:	89 55 14             	mov    %edx,0x14(%ebp)
  8005cb:	8b 00                	mov    (%eax),%eax
  8005cd:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005d0:	89 c1                	mov    %eax,%ecx
  8005d2:	c1 f9 1f             	sar    $0x1f,%ecx
  8005d5:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005d8:	eb 16                	jmp    8005f0 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005da:	8b 45 14             	mov    0x14(%ebp),%eax
  8005dd:	8d 50 04             	lea    0x4(%eax),%edx
  8005e0:	89 55 14             	mov    %edx,0x14(%ebp)
  8005e3:	8b 00                	mov    (%eax),%eax
  8005e5:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005e8:	89 c1                	mov    %eax,%ecx
  8005ea:	c1 f9 1f             	sar    $0x1f,%ecx
  8005ed:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005f0:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005f3:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8005f6:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8005fa:	79 76                	jns    800672 <vprintfmt+0x332>
				putch('-', putdat);
  8005fc:	83 ec 08             	sub    $0x8,%esp
  8005ff:	53                   	push   %ebx
  800600:	6a 2d                	push   $0x2d
  800602:	ff d6                	call   *%esi
				num = -(long long) num;
  800604:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800607:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80060a:	f7 d8                	neg    %eax
  80060c:	83 d2 00             	adc    $0x0,%edx
  80060f:	f7 da                	neg    %edx
  800611:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800614:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800619:	eb 5c                	jmp    800677 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80061b:	8d 45 14             	lea    0x14(%ebp),%eax
  80061e:	e8 aa fc ff ff       	call   8002cd <getuint>
			base = 10;
  800623:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800628:	eb 4d                	jmp    800677 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80062a:	8d 45 14             	lea    0x14(%ebp),%eax
  80062d:	e8 9b fc ff ff       	call   8002cd <getuint>
			base = 8;
  800632:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800637:	eb 3e                	jmp    800677 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800639:	83 ec 08             	sub    $0x8,%esp
  80063c:	53                   	push   %ebx
  80063d:	6a 30                	push   $0x30
  80063f:	ff d6                	call   *%esi
			putch('x', putdat);
  800641:	83 c4 08             	add    $0x8,%esp
  800644:	53                   	push   %ebx
  800645:	6a 78                	push   $0x78
  800647:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800649:	8b 45 14             	mov    0x14(%ebp),%eax
  80064c:	8d 50 04             	lea    0x4(%eax),%edx
  80064f:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800652:	8b 00                	mov    (%eax),%eax
  800654:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800659:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80065c:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800661:	eb 14                	jmp    800677 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800663:	8d 45 14             	lea    0x14(%ebp),%eax
  800666:	e8 62 fc ff ff       	call   8002cd <getuint>
			base = 16;
  80066b:	b9 10 00 00 00       	mov    $0x10,%ecx
  800670:	eb 05                	jmp    800677 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800672:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800677:	83 ec 0c             	sub    $0xc,%esp
  80067a:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80067e:	57                   	push   %edi
  80067f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800682:	51                   	push   %ecx
  800683:	52                   	push   %edx
  800684:	50                   	push   %eax
  800685:	89 da                	mov    %ebx,%edx
  800687:	89 f0                	mov    %esi,%eax
  800689:	e8 92 fb ff ff       	call   800220 <printnum>
			break;
  80068e:	83 c4 20             	add    $0x20,%esp
  800691:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800694:	e9 cd fc ff ff       	jmp    800366 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800699:	83 ec 08             	sub    $0x8,%esp
  80069c:	53                   	push   %ebx
  80069d:	51                   	push   %ecx
  80069e:	ff d6                	call   *%esi
			break;
  8006a0:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006a3:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8006a6:	e9 bb fc ff ff       	jmp    800366 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006ab:	83 ec 08             	sub    $0x8,%esp
  8006ae:	53                   	push   %ebx
  8006af:	6a 25                	push   $0x25
  8006b1:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006b3:	83 c4 10             	add    $0x10,%esp
  8006b6:	eb 01                	jmp    8006b9 <vprintfmt+0x379>
  8006b8:	4f                   	dec    %edi
  8006b9:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006bd:	75 f9                	jne    8006b8 <vprintfmt+0x378>
  8006bf:	e9 a2 fc ff ff       	jmp    800366 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006c4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006c7:	5b                   	pop    %ebx
  8006c8:	5e                   	pop    %esi
  8006c9:	5f                   	pop    %edi
  8006ca:	5d                   	pop    %ebp
  8006cb:	c3                   	ret    

008006cc <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006cc:	55                   	push   %ebp
  8006cd:	89 e5                	mov    %esp,%ebp
  8006cf:	83 ec 18             	sub    $0x18,%esp
  8006d2:	8b 45 08             	mov    0x8(%ebp),%eax
  8006d5:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006d8:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006db:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006df:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8006e2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8006e9:	85 c0                	test   %eax,%eax
  8006eb:	74 26                	je     800713 <vsnprintf+0x47>
  8006ed:	85 d2                	test   %edx,%edx
  8006ef:	7e 29                	jle    80071a <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8006f1:	ff 75 14             	pushl  0x14(%ebp)
  8006f4:	ff 75 10             	pushl  0x10(%ebp)
  8006f7:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8006fa:	50                   	push   %eax
  8006fb:	68 07 03 80 00       	push   $0x800307
  800700:	e8 3b fc ff ff       	call   800340 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800705:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800708:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80070b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80070e:	83 c4 10             	add    $0x10,%esp
  800711:	eb 0c                	jmp    80071f <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800713:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800718:	eb 05                	jmp    80071f <vsnprintf+0x53>
  80071a:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80071f:	c9                   	leave  
  800720:	c3                   	ret    

00800721 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800721:	55                   	push   %ebp
  800722:	89 e5                	mov    %esp,%ebp
  800724:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800727:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80072a:	50                   	push   %eax
  80072b:	ff 75 10             	pushl  0x10(%ebp)
  80072e:	ff 75 0c             	pushl  0xc(%ebp)
  800731:	ff 75 08             	pushl  0x8(%ebp)
  800734:	e8 93 ff ff ff       	call   8006cc <vsnprintf>
	va_end(ap);

	return rc;
}
  800739:	c9                   	leave  
  80073a:	c3                   	ret    

0080073b <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80073b:	55                   	push   %ebp
  80073c:	89 e5                	mov    %esp,%ebp
  80073e:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800741:	b8 00 00 00 00       	mov    $0x0,%eax
  800746:	eb 01                	jmp    800749 <strlen+0xe>
		n++;
  800748:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800749:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80074d:	75 f9                	jne    800748 <strlen+0xd>
		n++;
	return n;
}
  80074f:	5d                   	pop    %ebp
  800750:	c3                   	ret    

00800751 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800751:	55                   	push   %ebp
  800752:	89 e5                	mov    %esp,%ebp
  800754:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800757:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80075a:	ba 00 00 00 00       	mov    $0x0,%edx
  80075f:	eb 01                	jmp    800762 <strnlen+0x11>
		n++;
  800761:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800762:	39 c2                	cmp    %eax,%edx
  800764:	74 08                	je     80076e <strnlen+0x1d>
  800766:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80076a:	75 f5                	jne    800761 <strnlen+0x10>
  80076c:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80076e:	5d                   	pop    %ebp
  80076f:	c3                   	ret    

00800770 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800770:	55                   	push   %ebp
  800771:	89 e5                	mov    %esp,%ebp
  800773:	53                   	push   %ebx
  800774:	8b 45 08             	mov    0x8(%ebp),%eax
  800777:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80077a:	89 c2                	mov    %eax,%edx
  80077c:	42                   	inc    %edx
  80077d:	41                   	inc    %ecx
  80077e:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800781:	88 5a ff             	mov    %bl,-0x1(%edx)
  800784:	84 db                	test   %bl,%bl
  800786:	75 f4                	jne    80077c <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800788:	5b                   	pop    %ebx
  800789:	5d                   	pop    %ebp
  80078a:	c3                   	ret    

0080078b <strcat>:

char *
strcat(char *dst, const char *src)
{
  80078b:	55                   	push   %ebp
  80078c:	89 e5                	mov    %esp,%ebp
  80078e:	53                   	push   %ebx
  80078f:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800792:	53                   	push   %ebx
  800793:	e8 a3 ff ff ff       	call   80073b <strlen>
  800798:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80079b:	ff 75 0c             	pushl  0xc(%ebp)
  80079e:	01 d8                	add    %ebx,%eax
  8007a0:	50                   	push   %eax
  8007a1:	e8 ca ff ff ff       	call   800770 <strcpy>
	return dst;
}
  8007a6:	89 d8                	mov    %ebx,%eax
  8007a8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8007ab:	c9                   	leave  
  8007ac:	c3                   	ret    

008007ad <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8007ad:	55                   	push   %ebp
  8007ae:	89 e5                	mov    %esp,%ebp
  8007b0:	56                   	push   %esi
  8007b1:	53                   	push   %ebx
  8007b2:	8b 75 08             	mov    0x8(%ebp),%esi
  8007b5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007b8:	89 f3                	mov    %esi,%ebx
  8007ba:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007bd:	89 f2                	mov    %esi,%edx
  8007bf:	eb 0c                	jmp    8007cd <strncpy+0x20>
		*dst++ = *src;
  8007c1:	42                   	inc    %edx
  8007c2:	8a 01                	mov    (%ecx),%al
  8007c4:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007c7:	80 39 01             	cmpb   $0x1,(%ecx)
  8007ca:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007cd:	39 da                	cmp    %ebx,%edx
  8007cf:	75 f0                	jne    8007c1 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007d1:	89 f0                	mov    %esi,%eax
  8007d3:	5b                   	pop    %ebx
  8007d4:	5e                   	pop    %esi
  8007d5:	5d                   	pop    %ebp
  8007d6:	c3                   	ret    

008007d7 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007d7:	55                   	push   %ebp
  8007d8:	89 e5                	mov    %esp,%ebp
  8007da:	56                   	push   %esi
  8007db:	53                   	push   %ebx
  8007dc:	8b 75 08             	mov    0x8(%ebp),%esi
  8007df:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007e2:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8007e5:	85 c0                	test   %eax,%eax
  8007e7:	74 1e                	je     800807 <strlcpy+0x30>
  8007e9:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8007ed:	89 f2                	mov    %esi,%edx
  8007ef:	eb 05                	jmp    8007f6 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8007f1:	42                   	inc    %edx
  8007f2:	41                   	inc    %ecx
  8007f3:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8007f6:	39 c2                	cmp    %eax,%edx
  8007f8:	74 08                	je     800802 <strlcpy+0x2b>
  8007fa:	8a 19                	mov    (%ecx),%bl
  8007fc:	84 db                	test   %bl,%bl
  8007fe:	75 f1                	jne    8007f1 <strlcpy+0x1a>
  800800:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800802:	c6 00 00             	movb   $0x0,(%eax)
  800805:	eb 02                	jmp    800809 <strlcpy+0x32>
  800807:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800809:	29 f0                	sub    %esi,%eax
}
  80080b:	5b                   	pop    %ebx
  80080c:	5e                   	pop    %esi
  80080d:	5d                   	pop    %ebp
  80080e:	c3                   	ret    

0080080f <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80080f:	55                   	push   %ebp
  800810:	89 e5                	mov    %esp,%ebp
  800812:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800815:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800818:	eb 02                	jmp    80081c <strcmp+0xd>
		p++, q++;
  80081a:	41                   	inc    %ecx
  80081b:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80081c:	8a 01                	mov    (%ecx),%al
  80081e:	84 c0                	test   %al,%al
  800820:	74 04                	je     800826 <strcmp+0x17>
  800822:	3a 02                	cmp    (%edx),%al
  800824:	74 f4                	je     80081a <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800826:	0f b6 c0             	movzbl %al,%eax
  800829:	0f b6 12             	movzbl (%edx),%edx
  80082c:	29 d0                	sub    %edx,%eax
}
  80082e:	5d                   	pop    %ebp
  80082f:	c3                   	ret    

00800830 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800830:	55                   	push   %ebp
  800831:	89 e5                	mov    %esp,%ebp
  800833:	53                   	push   %ebx
  800834:	8b 45 08             	mov    0x8(%ebp),%eax
  800837:	8b 55 0c             	mov    0xc(%ebp),%edx
  80083a:	89 c3                	mov    %eax,%ebx
  80083c:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  80083f:	eb 02                	jmp    800843 <strncmp+0x13>
		n--, p++, q++;
  800841:	40                   	inc    %eax
  800842:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800843:	39 d8                	cmp    %ebx,%eax
  800845:	74 14                	je     80085b <strncmp+0x2b>
  800847:	8a 08                	mov    (%eax),%cl
  800849:	84 c9                	test   %cl,%cl
  80084b:	74 04                	je     800851 <strncmp+0x21>
  80084d:	3a 0a                	cmp    (%edx),%cl
  80084f:	74 f0                	je     800841 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800851:	0f b6 00             	movzbl (%eax),%eax
  800854:	0f b6 12             	movzbl (%edx),%edx
  800857:	29 d0                	sub    %edx,%eax
  800859:	eb 05                	jmp    800860 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80085b:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800860:	5b                   	pop    %ebx
  800861:	5d                   	pop    %ebp
  800862:	c3                   	ret    

00800863 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800863:	55                   	push   %ebp
  800864:	89 e5                	mov    %esp,%ebp
  800866:	8b 45 08             	mov    0x8(%ebp),%eax
  800869:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80086c:	eb 05                	jmp    800873 <strchr+0x10>
		if (*s == c)
  80086e:	38 ca                	cmp    %cl,%dl
  800870:	74 0c                	je     80087e <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800872:	40                   	inc    %eax
  800873:	8a 10                	mov    (%eax),%dl
  800875:	84 d2                	test   %dl,%dl
  800877:	75 f5                	jne    80086e <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800879:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80087e:	5d                   	pop    %ebp
  80087f:	c3                   	ret    

00800880 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800880:	55                   	push   %ebp
  800881:	89 e5                	mov    %esp,%ebp
  800883:	8b 45 08             	mov    0x8(%ebp),%eax
  800886:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800889:	eb 05                	jmp    800890 <strfind+0x10>
		if (*s == c)
  80088b:	38 ca                	cmp    %cl,%dl
  80088d:	74 07                	je     800896 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  80088f:	40                   	inc    %eax
  800890:	8a 10                	mov    (%eax),%dl
  800892:	84 d2                	test   %dl,%dl
  800894:	75 f5                	jne    80088b <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800896:	5d                   	pop    %ebp
  800897:	c3                   	ret    

00800898 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800898:	55                   	push   %ebp
  800899:	89 e5                	mov    %esp,%ebp
  80089b:	57                   	push   %edi
  80089c:	56                   	push   %esi
  80089d:	53                   	push   %ebx
  80089e:	8b 7d 08             	mov    0x8(%ebp),%edi
  8008a1:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8008a4:	85 c9                	test   %ecx,%ecx
  8008a6:	74 36                	je     8008de <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8008a8:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8008ae:	75 28                	jne    8008d8 <memset+0x40>
  8008b0:	f6 c1 03             	test   $0x3,%cl
  8008b3:	75 23                	jne    8008d8 <memset+0x40>
		c &= 0xFF;
  8008b5:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8008b9:	89 d3                	mov    %edx,%ebx
  8008bb:	c1 e3 08             	shl    $0x8,%ebx
  8008be:	89 d6                	mov    %edx,%esi
  8008c0:	c1 e6 18             	shl    $0x18,%esi
  8008c3:	89 d0                	mov    %edx,%eax
  8008c5:	c1 e0 10             	shl    $0x10,%eax
  8008c8:	09 f0                	or     %esi,%eax
  8008ca:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008cc:	89 d8                	mov    %ebx,%eax
  8008ce:	09 d0                	or     %edx,%eax
  8008d0:	c1 e9 02             	shr    $0x2,%ecx
  8008d3:	fc                   	cld    
  8008d4:	f3 ab                	rep stos %eax,%es:(%edi)
  8008d6:	eb 06                	jmp    8008de <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008d8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008db:	fc                   	cld    
  8008dc:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008de:	89 f8                	mov    %edi,%eax
  8008e0:	5b                   	pop    %ebx
  8008e1:	5e                   	pop    %esi
  8008e2:	5f                   	pop    %edi
  8008e3:	5d                   	pop    %ebp
  8008e4:	c3                   	ret    

008008e5 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8008e5:	55                   	push   %ebp
  8008e6:	89 e5                	mov    %esp,%ebp
  8008e8:	57                   	push   %edi
  8008e9:	56                   	push   %esi
  8008ea:	8b 45 08             	mov    0x8(%ebp),%eax
  8008ed:	8b 75 0c             	mov    0xc(%ebp),%esi
  8008f0:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8008f3:	39 c6                	cmp    %eax,%esi
  8008f5:	73 33                	jae    80092a <memmove+0x45>
  8008f7:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8008fa:	39 d0                	cmp    %edx,%eax
  8008fc:	73 2c                	jae    80092a <memmove+0x45>
		s += n;
		d += n;
  8008fe:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800901:	89 d6                	mov    %edx,%esi
  800903:	09 fe                	or     %edi,%esi
  800905:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80090b:	75 13                	jne    800920 <memmove+0x3b>
  80090d:	f6 c1 03             	test   $0x3,%cl
  800910:	75 0e                	jne    800920 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800912:	83 ef 04             	sub    $0x4,%edi
  800915:	8d 72 fc             	lea    -0x4(%edx),%esi
  800918:	c1 e9 02             	shr    $0x2,%ecx
  80091b:	fd                   	std    
  80091c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80091e:	eb 07                	jmp    800927 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800920:	4f                   	dec    %edi
  800921:	8d 72 ff             	lea    -0x1(%edx),%esi
  800924:	fd                   	std    
  800925:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800927:	fc                   	cld    
  800928:	eb 1d                	jmp    800947 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80092a:	89 f2                	mov    %esi,%edx
  80092c:	09 c2                	or     %eax,%edx
  80092e:	f6 c2 03             	test   $0x3,%dl
  800931:	75 0f                	jne    800942 <memmove+0x5d>
  800933:	f6 c1 03             	test   $0x3,%cl
  800936:	75 0a                	jne    800942 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800938:	c1 e9 02             	shr    $0x2,%ecx
  80093b:	89 c7                	mov    %eax,%edi
  80093d:	fc                   	cld    
  80093e:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800940:	eb 05                	jmp    800947 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800942:	89 c7                	mov    %eax,%edi
  800944:	fc                   	cld    
  800945:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800947:	5e                   	pop    %esi
  800948:	5f                   	pop    %edi
  800949:	5d                   	pop    %ebp
  80094a:	c3                   	ret    

0080094b <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80094b:	55                   	push   %ebp
  80094c:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  80094e:	ff 75 10             	pushl  0x10(%ebp)
  800951:	ff 75 0c             	pushl  0xc(%ebp)
  800954:	ff 75 08             	pushl  0x8(%ebp)
  800957:	e8 89 ff ff ff       	call   8008e5 <memmove>
}
  80095c:	c9                   	leave  
  80095d:	c3                   	ret    

0080095e <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  80095e:	55                   	push   %ebp
  80095f:	89 e5                	mov    %esp,%ebp
  800961:	56                   	push   %esi
  800962:	53                   	push   %ebx
  800963:	8b 45 08             	mov    0x8(%ebp),%eax
  800966:	8b 55 0c             	mov    0xc(%ebp),%edx
  800969:	89 c6                	mov    %eax,%esi
  80096b:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80096e:	eb 14                	jmp    800984 <memcmp+0x26>
		if (*s1 != *s2)
  800970:	8a 08                	mov    (%eax),%cl
  800972:	8a 1a                	mov    (%edx),%bl
  800974:	38 d9                	cmp    %bl,%cl
  800976:	74 0a                	je     800982 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800978:	0f b6 c1             	movzbl %cl,%eax
  80097b:	0f b6 db             	movzbl %bl,%ebx
  80097e:	29 d8                	sub    %ebx,%eax
  800980:	eb 0b                	jmp    80098d <memcmp+0x2f>
		s1++, s2++;
  800982:	40                   	inc    %eax
  800983:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800984:	39 f0                	cmp    %esi,%eax
  800986:	75 e8                	jne    800970 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800988:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80098d:	5b                   	pop    %ebx
  80098e:	5e                   	pop    %esi
  80098f:	5d                   	pop    %ebp
  800990:	c3                   	ret    

00800991 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800991:	55                   	push   %ebp
  800992:	89 e5                	mov    %esp,%ebp
  800994:	53                   	push   %ebx
  800995:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800998:	89 c1                	mov    %eax,%ecx
  80099a:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  80099d:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009a1:	eb 08                	jmp    8009ab <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8009a3:	0f b6 10             	movzbl (%eax),%edx
  8009a6:	39 da                	cmp    %ebx,%edx
  8009a8:	74 05                	je     8009af <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009aa:	40                   	inc    %eax
  8009ab:	39 c8                	cmp    %ecx,%eax
  8009ad:	72 f4                	jb     8009a3 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8009af:	5b                   	pop    %ebx
  8009b0:	5d                   	pop    %ebp
  8009b1:	c3                   	ret    

008009b2 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8009b2:	55                   	push   %ebp
  8009b3:	89 e5                	mov    %esp,%ebp
  8009b5:	57                   	push   %edi
  8009b6:	56                   	push   %esi
  8009b7:	53                   	push   %ebx
  8009b8:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009bb:	eb 01                	jmp    8009be <strtol+0xc>
		s++;
  8009bd:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009be:	8a 01                	mov    (%ecx),%al
  8009c0:	3c 20                	cmp    $0x20,%al
  8009c2:	74 f9                	je     8009bd <strtol+0xb>
  8009c4:	3c 09                	cmp    $0x9,%al
  8009c6:	74 f5                	je     8009bd <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009c8:	3c 2b                	cmp    $0x2b,%al
  8009ca:	75 08                	jne    8009d4 <strtol+0x22>
		s++;
  8009cc:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009cd:	bf 00 00 00 00       	mov    $0x0,%edi
  8009d2:	eb 11                	jmp    8009e5 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009d4:	3c 2d                	cmp    $0x2d,%al
  8009d6:	75 08                	jne    8009e0 <strtol+0x2e>
		s++, neg = 1;
  8009d8:	41                   	inc    %ecx
  8009d9:	bf 01 00 00 00       	mov    $0x1,%edi
  8009de:	eb 05                	jmp    8009e5 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009e0:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009e5:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009e9:	0f 84 87 00 00 00    	je     800a76 <strtol+0xc4>
  8009ef:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  8009f3:	75 27                	jne    800a1c <strtol+0x6a>
  8009f5:	80 39 30             	cmpb   $0x30,(%ecx)
  8009f8:	75 22                	jne    800a1c <strtol+0x6a>
  8009fa:	e9 88 00 00 00       	jmp    800a87 <strtol+0xd5>
		s += 2, base = 16;
  8009ff:	83 c1 02             	add    $0x2,%ecx
  800a02:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a09:	eb 11                	jmp    800a1c <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a0b:	41                   	inc    %ecx
  800a0c:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a13:	eb 07                	jmp    800a1c <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a15:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a1c:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a21:	8a 11                	mov    (%ecx),%dl
  800a23:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a26:	80 fb 09             	cmp    $0x9,%bl
  800a29:	77 08                	ja     800a33 <strtol+0x81>
			dig = *s - '0';
  800a2b:	0f be d2             	movsbl %dl,%edx
  800a2e:	83 ea 30             	sub    $0x30,%edx
  800a31:	eb 22                	jmp    800a55 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a33:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a36:	89 f3                	mov    %esi,%ebx
  800a38:	80 fb 19             	cmp    $0x19,%bl
  800a3b:	77 08                	ja     800a45 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a3d:	0f be d2             	movsbl %dl,%edx
  800a40:	83 ea 57             	sub    $0x57,%edx
  800a43:	eb 10                	jmp    800a55 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a45:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a48:	89 f3                	mov    %esi,%ebx
  800a4a:	80 fb 19             	cmp    $0x19,%bl
  800a4d:	77 14                	ja     800a63 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a4f:	0f be d2             	movsbl %dl,%edx
  800a52:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a55:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a58:	7d 09                	jge    800a63 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a5a:	41                   	inc    %ecx
  800a5b:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a5f:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a61:	eb be                	jmp    800a21 <strtol+0x6f>

	if (endptr)
  800a63:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a67:	74 05                	je     800a6e <strtol+0xbc>
		*endptr = (char *) s;
  800a69:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a6c:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a6e:	85 ff                	test   %edi,%edi
  800a70:	74 21                	je     800a93 <strtol+0xe1>
  800a72:	f7 d8                	neg    %eax
  800a74:	eb 1d                	jmp    800a93 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a76:	80 39 30             	cmpb   $0x30,(%ecx)
  800a79:	75 9a                	jne    800a15 <strtol+0x63>
  800a7b:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a7f:	0f 84 7a ff ff ff    	je     8009ff <strtol+0x4d>
  800a85:	eb 84                	jmp    800a0b <strtol+0x59>
  800a87:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a8b:	0f 84 6e ff ff ff    	je     8009ff <strtol+0x4d>
  800a91:	eb 89                	jmp    800a1c <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a93:	5b                   	pop    %ebx
  800a94:	5e                   	pop    %esi
  800a95:	5f                   	pop    %edi
  800a96:	5d                   	pop    %ebp
  800a97:	c3                   	ret    

00800a98 <__udivdi3>:
  800a98:	55                   	push   %ebp
  800a99:	57                   	push   %edi
  800a9a:	56                   	push   %esi
  800a9b:	53                   	push   %ebx
  800a9c:	83 ec 1c             	sub    $0x1c,%esp
  800a9f:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800aa3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800aa7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800aab:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800aaf:	89 ca                	mov    %ecx,%edx
  800ab1:	89 f8                	mov    %edi,%eax
  800ab3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800ab7:	85 f6                	test   %esi,%esi
  800ab9:	75 2d                	jne    800ae8 <__udivdi3+0x50>
  800abb:	39 cf                	cmp    %ecx,%edi
  800abd:	77 65                	ja     800b24 <__udivdi3+0x8c>
  800abf:	89 fd                	mov    %edi,%ebp
  800ac1:	85 ff                	test   %edi,%edi
  800ac3:	75 0b                	jne    800ad0 <__udivdi3+0x38>
  800ac5:	b8 01 00 00 00       	mov    $0x1,%eax
  800aca:	31 d2                	xor    %edx,%edx
  800acc:	f7 f7                	div    %edi
  800ace:	89 c5                	mov    %eax,%ebp
  800ad0:	31 d2                	xor    %edx,%edx
  800ad2:	89 c8                	mov    %ecx,%eax
  800ad4:	f7 f5                	div    %ebp
  800ad6:	89 c1                	mov    %eax,%ecx
  800ad8:	89 d8                	mov    %ebx,%eax
  800ada:	f7 f5                	div    %ebp
  800adc:	89 cf                	mov    %ecx,%edi
  800ade:	89 fa                	mov    %edi,%edx
  800ae0:	83 c4 1c             	add    $0x1c,%esp
  800ae3:	5b                   	pop    %ebx
  800ae4:	5e                   	pop    %esi
  800ae5:	5f                   	pop    %edi
  800ae6:	5d                   	pop    %ebp
  800ae7:	c3                   	ret    
  800ae8:	39 ce                	cmp    %ecx,%esi
  800aea:	77 28                	ja     800b14 <__udivdi3+0x7c>
  800aec:	0f bd fe             	bsr    %esi,%edi
  800aef:	83 f7 1f             	xor    $0x1f,%edi
  800af2:	75 40                	jne    800b34 <__udivdi3+0x9c>
  800af4:	39 ce                	cmp    %ecx,%esi
  800af6:	72 0a                	jb     800b02 <__udivdi3+0x6a>
  800af8:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800afc:	0f 87 9e 00 00 00    	ja     800ba0 <__udivdi3+0x108>
  800b02:	b8 01 00 00 00       	mov    $0x1,%eax
  800b07:	89 fa                	mov    %edi,%edx
  800b09:	83 c4 1c             	add    $0x1c,%esp
  800b0c:	5b                   	pop    %ebx
  800b0d:	5e                   	pop    %esi
  800b0e:	5f                   	pop    %edi
  800b0f:	5d                   	pop    %ebp
  800b10:	c3                   	ret    
  800b11:	8d 76 00             	lea    0x0(%esi),%esi
  800b14:	31 ff                	xor    %edi,%edi
  800b16:	31 c0                	xor    %eax,%eax
  800b18:	89 fa                	mov    %edi,%edx
  800b1a:	83 c4 1c             	add    $0x1c,%esp
  800b1d:	5b                   	pop    %ebx
  800b1e:	5e                   	pop    %esi
  800b1f:	5f                   	pop    %edi
  800b20:	5d                   	pop    %ebp
  800b21:	c3                   	ret    
  800b22:	66 90                	xchg   %ax,%ax
  800b24:	89 d8                	mov    %ebx,%eax
  800b26:	f7 f7                	div    %edi
  800b28:	31 ff                	xor    %edi,%edi
  800b2a:	89 fa                	mov    %edi,%edx
  800b2c:	83 c4 1c             	add    $0x1c,%esp
  800b2f:	5b                   	pop    %ebx
  800b30:	5e                   	pop    %esi
  800b31:	5f                   	pop    %edi
  800b32:	5d                   	pop    %ebp
  800b33:	c3                   	ret    
  800b34:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b39:	89 eb                	mov    %ebp,%ebx
  800b3b:	29 fb                	sub    %edi,%ebx
  800b3d:	89 f9                	mov    %edi,%ecx
  800b3f:	d3 e6                	shl    %cl,%esi
  800b41:	89 c5                	mov    %eax,%ebp
  800b43:	88 d9                	mov    %bl,%cl
  800b45:	d3 ed                	shr    %cl,%ebp
  800b47:	89 e9                	mov    %ebp,%ecx
  800b49:	09 f1                	or     %esi,%ecx
  800b4b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b4f:	89 f9                	mov    %edi,%ecx
  800b51:	d3 e0                	shl    %cl,%eax
  800b53:	89 c5                	mov    %eax,%ebp
  800b55:	89 d6                	mov    %edx,%esi
  800b57:	88 d9                	mov    %bl,%cl
  800b59:	d3 ee                	shr    %cl,%esi
  800b5b:	89 f9                	mov    %edi,%ecx
  800b5d:	d3 e2                	shl    %cl,%edx
  800b5f:	8b 44 24 08          	mov    0x8(%esp),%eax
  800b63:	88 d9                	mov    %bl,%cl
  800b65:	d3 e8                	shr    %cl,%eax
  800b67:	09 c2                	or     %eax,%edx
  800b69:	89 d0                	mov    %edx,%eax
  800b6b:	89 f2                	mov    %esi,%edx
  800b6d:	f7 74 24 0c          	divl   0xc(%esp)
  800b71:	89 d6                	mov    %edx,%esi
  800b73:	89 c3                	mov    %eax,%ebx
  800b75:	f7 e5                	mul    %ebp
  800b77:	39 d6                	cmp    %edx,%esi
  800b79:	72 19                	jb     800b94 <__udivdi3+0xfc>
  800b7b:	74 0b                	je     800b88 <__udivdi3+0xf0>
  800b7d:	89 d8                	mov    %ebx,%eax
  800b7f:	31 ff                	xor    %edi,%edi
  800b81:	e9 58 ff ff ff       	jmp    800ade <__udivdi3+0x46>
  800b86:	66 90                	xchg   %ax,%ax
  800b88:	8b 54 24 08          	mov    0x8(%esp),%edx
  800b8c:	89 f9                	mov    %edi,%ecx
  800b8e:	d3 e2                	shl    %cl,%edx
  800b90:	39 c2                	cmp    %eax,%edx
  800b92:	73 e9                	jae    800b7d <__udivdi3+0xe5>
  800b94:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800b97:	31 ff                	xor    %edi,%edi
  800b99:	e9 40 ff ff ff       	jmp    800ade <__udivdi3+0x46>
  800b9e:	66 90                	xchg   %ax,%ax
  800ba0:	31 c0                	xor    %eax,%eax
  800ba2:	e9 37 ff ff ff       	jmp    800ade <__udivdi3+0x46>
  800ba7:	90                   	nop

00800ba8 <__umoddi3>:
  800ba8:	55                   	push   %ebp
  800ba9:	57                   	push   %edi
  800baa:	56                   	push   %esi
  800bab:	53                   	push   %ebx
  800bac:	83 ec 1c             	sub    $0x1c,%esp
  800baf:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bb3:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bb7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800bbb:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800bbf:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800bc3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800bc7:	89 f3                	mov    %esi,%ebx
  800bc9:	89 fa                	mov    %edi,%edx
  800bcb:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800bcf:	89 34 24             	mov    %esi,(%esp)
  800bd2:	85 c0                	test   %eax,%eax
  800bd4:	75 1a                	jne    800bf0 <__umoddi3+0x48>
  800bd6:	39 f7                	cmp    %esi,%edi
  800bd8:	0f 86 a2 00 00 00    	jbe    800c80 <__umoddi3+0xd8>
  800bde:	89 c8                	mov    %ecx,%eax
  800be0:	89 f2                	mov    %esi,%edx
  800be2:	f7 f7                	div    %edi
  800be4:	89 d0                	mov    %edx,%eax
  800be6:	31 d2                	xor    %edx,%edx
  800be8:	83 c4 1c             	add    $0x1c,%esp
  800beb:	5b                   	pop    %ebx
  800bec:	5e                   	pop    %esi
  800bed:	5f                   	pop    %edi
  800bee:	5d                   	pop    %ebp
  800bef:	c3                   	ret    
  800bf0:	39 f0                	cmp    %esi,%eax
  800bf2:	0f 87 ac 00 00 00    	ja     800ca4 <__umoddi3+0xfc>
  800bf8:	0f bd e8             	bsr    %eax,%ebp
  800bfb:	83 f5 1f             	xor    $0x1f,%ebp
  800bfe:	0f 84 ac 00 00 00    	je     800cb0 <__umoddi3+0x108>
  800c04:	bf 20 00 00 00       	mov    $0x20,%edi
  800c09:	29 ef                	sub    %ebp,%edi
  800c0b:	89 fe                	mov    %edi,%esi
  800c0d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c11:	89 e9                	mov    %ebp,%ecx
  800c13:	d3 e0                	shl    %cl,%eax
  800c15:	89 d7                	mov    %edx,%edi
  800c17:	89 f1                	mov    %esi,%ecx
  800c19:	d3 ef                	shr    %cl,%edi
  800c1b:	09 c7                	or     %eax,%edi
  800c1d:	89 e9                	mov    %ebp,%ecx
  800c1f:	d3 e2                	shl    %cl,%edx
  800c21:	89 14 24             	mov    %edx,(%esp)
  800c24:	89 d8                	mov    %ebx,%eax
  800c26:	d3 e0                	shl    %cl,%eax
  800c28:	89 c2                	mov    %eax,%edx
  800c2a:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c2e:	d3 e0                	shl    %cl,%eax
  800c30:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c34:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c38:	89 f1                	mov    %esi,%ecx
  800c3a:	d3 e8                	shr    %cl,%eax
  800c3c:	09 d0                	or     %edx,%eax
  800c3e:	d3 eb                	shr    %cl,%ebx
  800c40:	89 da                	mov    %ebx,%edx
  800c42:	f7 f7                	div    %edi
  800c44:	89 d3                	mov    %edx,%ebx
  800c46:	f7 24 24             	mull   (%esp)
  800c49:	89 c6                	mov    %eax,%esi
  800c4b:	89 d1                	mov    %edx,%ecx
  800c4d:	39 d3                	cmp    %edx,%ebx
  800c4f:	0f 82 87 00 00 00    	jb     800cdc <__umoddi3+0x134>
  800c55:	0f 84 91 00 00 00    	je     800cec <__umoddi3+0x144>
  800c5b:	8b 54 24 04          	mov    0x4(%esp),%edx
  800c5f:	29 f2                	sub    %esi,%edx
  800c61:	19 cb                	sbb    %ecx,%ebx
  800c63:	89 d8                	mov    %ebx,%eax
  800c65:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800c69:	d3 e0                	shl    %cl,%eax
  800c6b:	89 e9                	mov    %ebp,%ecx
  800c6d:	d3 ea                	shr    %cl,%edx
  800c6f:	09 d0                	or     %edx,%eax
  800c71:	89 e9                	mov    %ebp,%ecx
  800c73:	d3 eb                	shr    %cl,%ebx
  800c75:	89 da                	mov    %ebx,%edx
  800c77:	83 c4 1c             	add    $0x1c,%esp
  800c7a:	5b                   	pop    %ebx
  800c7b:	5e                   	pop    %esi
  800c7c:	5f                   	pop    %edi
  800c7d:	5d                   	pop    %ebp
  800c7e:	c3                   	ret    
  800c7f:	90                   	nop
  800c80:	89 fd                	mov    %edi,%ebp
  800c82:	85 ff                	test   %edi,%edi
  800c84:	75 0b                	jne    800c91 <__umoddi3+0xe9>
  800c86:	b8 01 00 00 00       	mov    $0x1,%eax
  800c8b:	31 d2                	xor    %edx,%edx
  800c8d:	f7 f7                	div    %edi
  800c8f:	89 c5                	mov    %eax,%ebp
  800c91:	89 f0                	mov    %esi,%eax
  800c93:	31 d2                	xor    %edx,%edx
  800c95:	f7 f5                	div    %ebp
  800c97:	89 c8                	mov    %ecx,%eax
  800c99:	f7 f5                	div    %ebp
  800c9b:	89 d0                	mov    %edx,%eax
  800c9d:	e9 44 ff ff ff       	jmp    800be6 <__umoddi3+0x3e>
  800ca2:	66 90                	xchg   %ax,%ax
  800ca4:	89 c8                	mov    %ecx,%eax
  800ca6:	89 f2                	mov    %esi,%edx
  800ca8:	83 c4 1c             	add    $0x1c,%esp
  800cab:	5b                   	pop    %ebx
  800cac:	5e                   	pop    %esi
  800cad:	5f                   	pop    %edi
  800cae:	5d                   	pop    %ebp
  800caf:	c3                   	ret    
  800cb0:	3b 04 24             	cmp    (%esp),%eax
  800cb3:	72 06                	jb     800cbb <__umoddi3+0x113>
  800cb5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800cb9:	77 0f                	ja     800cca <__umoddi3+0x122>
  800cbb:	89 f2                	mov    %esi,%edx
  800cbd:	29 f9                	sub    %edi,%ecx
  800cbf:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800cc3:	89 14 24             	mov    %edx,(%esp)
  800cc6:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800cca:	8b 44 24 04          	mov    0x4(%esp),%eax
  800cce:	8b 14 24             	mov    (%esp),%edx
  800cd1:	83 c4 1c             	add    $0x1c,%esp
  800cd4:	5b                   	pop    %ebx
  800cd5:	5e                   	pop    %esi
  800cd6:	5f                   	pop    %edi
  800cd7:	5d                   	pop    %ebp
  800cd8:	c3                   	ret    
  800cd9:	8d 76 00             	lea    0x0(%esi),%esi
  800cdc:	2b 04 24             	sub    (%esp),%eax
  800cdf:	19 fa                	sbb    %edi,%edx
  800ce1:	89 d1                	mov    %edx,%ecx
  800ce3:	89 c6                	mov    %eax,%esi
  800ce5:	e9 71 ff ff ff       	jmp    800c5b <__umoddi3+0xb3>
  800cea:	66 90                	xchg   %ax,%ax
  800cec:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800cf0:	72 ea                	jb     800cdc <__umoddi3+0x134>
  800cf2:	89 d9                	mov    %ebx,%ecx
  800cf4:	e9 62 ff ff ff       	jmp    800c5b <__umoddi3+0xb3>
