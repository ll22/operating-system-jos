
obj/user/breakpoint:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 08 00 00 00       	call   800039 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	asm volatile("int $3"); //break point
  800036:	cc                   	int3   
}
  800037:	5d                   	pop    %ebp
  800038:	c3                   	ret    

00800039 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800039:	55                   	push   %ebp
  80003a:	89 e5                	mov    %esp,%ebp
  80003c:	56                   	push   %esi
  80003d:	53                   	push   %ebx
  80003e:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800041:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800044:	e8 cb 00 00 00       	call   800114 <sys_getenvid>
  800049:	25 ff 03 00 00       	and    $0x3ff,%eax
  80004e:	8d 14 00             	lea    (%eax,%eax,1),%edx
  800051:	01 d0                	add    %edx,%eax
  800053:	c1 e0 05             	shl    $0x5,%eax
  800056:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  80005b:	a3 04 10 80 00       	mov    %eax,0x801004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800060:	85 db                	test   %ebx,%ebx
  800062:	7e 07                	jle    80006b <libmain+0x32>
		binaryname = argv[0];
  800064:	8b 06                	mov    (%esi),%eax
  800066:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  80006b:	83 ec 08             	sub    $0x8,%esp
  80006e:	56                   	push   %esi
  80006f:	53                   	push   %ebx
  800070:	e8 be ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800075:	e8 0a 00 00 00       	call   800084 <exit>
}
  80007a:	83 c4 10             	add    $0x10,%esp
  80007d:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800080:	5b                   	pop    %ebx
  800081:	5e                   	pop    %esi
  800082:	5d                   	pop    %ebp
  800083:	c3                   	ret    

00800084 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800084:	55                   	push   %ebp
  800085:	89 e5                	mov    %esp,%ebp
  800087:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  80008a:	6a 00                	push   $0x0
  80008c:	e8 42 00 00 00       	call   8000d3 <sys_env_destroy>
}
  800091:	83 c4 10             	add    $0x10,%esp
  800094:	c9                   	leave  
  800095:	c3                   	ret    

00800096 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  800096:	55                   	push   %ebp
  800097:	89 e5                	mov    %esp,%ebp
  800099:	57                   	push   %edi
  80009a:	56                   	push   %esi
  80009b:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80009c:	b8 00 00 00 00       	mov    $0x0,%eax
  8000a1:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000a4:	8b 55 08             	mov    0x8(%ebp),%edx
  8000a7:	89 c3                	mov    %eax,%ebx
  8000a9:	89 c7                	mov    %eax,%edi
  8000ab:	89 c6                	mov    %eax,%esi
  8000ad:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000af:	5b                   	pop    %ebx
  8000b0:	5e                   	pop    %esi
  8000b1:	5f                   	pop    %edi
  8000b2:	5d                   	pop    %ebp
  8000b3:	c3                   	ret    

008000b4 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000b4:	55                   	push   %ebp
  8000b5:	89 e5                	mov    %esp,%ebp
  8000b7:	57                   	push   %edi
  8000b8:	56                   	push   %esi
  8000b9:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000ba:	ba 00 00 00 00       	mov    $0x0,%edx
  8000bf:	b8 01 00 00 00       	mov    $0x1,%eax
  8000c4:	89 d1                	mov    %edx,%ecx
  8000c6:	89 d3                	mov    %edx,%ebx
  8000c8:	89 d7                	mov    %edx,%edi
  8000ca:	89 d6                	mov    %edx,%esi
  8000cc:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000ce:	5b                   	pop    %ebx
  8000cf:	5e                   	pop    %esi
  8000d0:	5f                   	pop    %edi
  8000d1:	5d                   	pop    %ebp
  8000d2:	c3                   	ret    

008000d3 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000d3:	55                   	push   %ebp
  8000d4:	89 e5                	mov    %esp,%ebp
  8000d6:	57                   	push   %edi
  8000d7:	56                   	push   %esi
  8000d8:	53                   	push   %ebx
  8000d9:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000dc:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000e1:	b8 03 00 00 00       	mov    $0x3,%eax
  8000e6:	8b 55 08             	mov    0x8(%ebp),%edx
  8000e9:	89 cb                	mov    %ecx,%ebx
  8000eb:	89 cf                	mov    %ecx,%edi
  8000ed:	89 ce                	mov    %ecx,%esi
  8000ef:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8000f1:	85 c0                	test   %eax,%eax
  8000f3:	7e 17                	jle    80010c <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  8000f5:	83 ec 0c             	sub    $0xc,%esp
  8000f8:	50                   	push   %eax
  8000f9:	6a 03                	push   $0x3
  8000fb:	68 06 0d 80 00       	push   $0x800d06
  800100:	6a 23                	push   $0x23
  800102:	68 23 0d 80 00       	push   $0x800d23
  800107:	e8 27 00 00 00       	call   800133 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  80010c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80010f:	5b                   	pop    %ebx
  800110:	5e                   	pop    %esi
  800111:	5f                   	pop    %edi
  800112:	5d                   	pop    %ebp
  800113:	c3                   	ret    

00800114 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800114:	55                   	push   %ebp
  800115:	89 e5                	mov    %esp,%ebp
  800117:	57                   	push   %edi
  800118:	56                   	push   %esi
  800119:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80011a:	ba 00 00 00 00       	mov    $0x0,%edx
  80011f:	b8 02 00 00 00       	mov    $0x2,%eax
  800124:	89 d1                	mov    %edx,%ecx
  800126:	89 d3                	mov    %edx,%ebx
  800128:	89 d7                	mov    %edx,%edi
  80012a:	89 d6                	mov    %edx,%esi
  80012c:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  80012e:	5b                   	pop    %ebx
  80012f:	5e                   	pop    %esi
  800130:	5f                   	pop    %edi
  800131:	5d                   	pop    %ebp
  800132:	c3                   	ret    

00800133 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800133:	55                   	push   %ebp
  800134:	89 e5                	mov    %esp,%ebp
  800136:	56                   	push   %esi
  800137:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800138:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  80013b:	8b 35 00 10 80 00    	mov    0x801000,%esi
  800141:	e8 ce ff ff ff       	call   800114 <sys_getenvid>
  800146:	83 ec 0c             	sub    $0xc,%esp
  800149:	ff 75 0c             	pushl  0xc(%ebp)
  80014c:	ff 75 08             	pushl  0x8(%ebp)
  80014f:	56                   	push   %esi
  800150:	50                   	push   %eax
  800151:	68 34 0d 80 00       	push   $0x800d34
  800156:	e8 b0 00 00 00       	call   80020b <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  80015b:	83 c4 18             	add    $0x18,%esp
  80015e:	53                   	push   %ebx
  80015f:	ff 75 10             	pushl  0x10(%ebp)
  800162:	e8 53 00 00 00       	call   8001ba <vcprintf>
	cprintf("\n");
  800167:	c7 04 24 58 0d 80 00 	movl   $0x800d58,(%esp)
  80016e:	e8 98 00 00 00       	call   80020b <cprintf>
  800173:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800176:	cc                   	int3   
  800177:	eb fd                	jmp    800176 <_panic+0x43>

00800179 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800179:	55                   	push   %ebp
  80017a:	89 e5                	mov    %esp,%ebp
  80017c:	53                   	push   %ebx
  80017d:	83 ec 04             	sub    $0x4,%esp
  800180:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800183:	8b 13                	mov    (%ebx),%edx
  800185:	8d 42 01             	lea    0x1(%edx),%eax
  800188:	89 03                	mov    %eax,(%ebx)
  80018a:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80018d:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  800191:	3d ff 00 00 00       	cmp    $0xff,%eax
  800196:	75 1a                	jne    8001b2 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  800198:	83 ec 08             	sub    $0x8,%esp
  80019b:	68 ff 00 00 00       	push   $0xff
  8001a0:	8d 43 08             	lea    0x8(%ebx),%eax
  8001a3:	50                   	push   %eax
  8001a4:	e8 ed fe ff ff       	call   800096 <sys_cputs>
		b->idx = 0;
  8001a9:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8001af:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8001b2:	ff 43 04             	incl   0x4(%ebx)
}
  8001b5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001b8:	c9                   	leave  
  8001b9:	c3                   	ret    

008001ba <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8001ba:	55                   	push   %ebp
  8001bb:	89 e5                	mov    %esp,%ebp
  8001bd:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001c3:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001ca:	00 00 00 
	b.cnt = 0;
  8001cd:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001d4:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001d7:	ff 75 0c             	pushl  0xc(%ebp)
  8001da:	ff 75 08             	pushl  0x8(%ebp)
  8001dd:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001e3:	50                   	push   %eax
  8001e4:	68 79 01 80 00       	push   $0x800179
  8001e9:	e8 51 01 00 00       	call   80033f <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001ee:	83 c4 08             	add    $0x8,%esp
  8001f1:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  8001f7:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  8001fd:	50                   	push   %eax
  8001fe:	e8 93 fe ff ff       	call   800096 <sys_cputs>

	return b.cnt;
}
  800203:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800209:	c9                   	leave  
  80020a:	c3                   	ret    

0080020b <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80020b:	55                   	push   %ebp
  80020c:	89 e5                	mov    %esp,%ebp
  80020e:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800211:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800214:	50                   	push   %eax
  800215:	ff 75 08             	pushl  0x8(%ebp)
  800218:	e8 9d ff ff ff       	call   8001ba <vcprintf>
	va_end(ap);

	return cnt;
}
  80021d:	c9                   	leave  
  80021e:	c3                   	ret    

0080021f <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80021f:	55                   	push   %ebp
  800220:	89 e5                	mov    %esp,%ebp
  800222:	57                   	push   %edi
  800223:	56                   	push   %esi
  800224:	53                   	push   %ebx
  800225:	83 ec 1c             	sub    $0x1c,%esp
  800228:	89 c7                	mov    %eax,%edi
  80022a:	89 d6                	mov    %edx,%esi
  80022c:	8b 45 08             	mov    0x8(%ebp),%eax
  80022f:	8b 55 0c             	mov    0xc(%ebp),%edx
  800232:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800235:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800238:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80023b:	bb 00 00 00 00       	mov    $0x0,%ebx
  800240:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800243:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800246:	39 d3                	cmp    %edx,%ebx
  800248:	72 05                	jb     80024f <printnum+0x30>
  80024a:	39 45 10             	cmp    %eax,0x10(%ebp)
  80024d:	77 45                	ja     800294 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  80024f:	83 ec 0c             	sub    $0xc,%esp
  800252:	ff 75 18             	pushl  0x18(%ebp)
  800255:	8b 45 14             	mov    0x14(%ebp),%eax
  800258:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80025b:	53                   	push   %ebx
  80025c:	ff 75 10             	pushl  0x10(%ebp)
  80025f:	83 ec 08             	sub    $0x8,%esp
  800262:	ff 75 e4             	pushl  -0x1c(%ebp)
  800265:	ff 75 e0             	pushl  -0x20(%ebp)
  800268:	ff 75 dc             	pushl  -0x24(%ebp)
  80026b:	ff 75 d8             	pushl  -0x28(%ebp)
  80026e:	e8 25 08 00 00       	call   800a98 <__udivdi3>
  800273:	83 c4 18             	add    $0x18,%esp
  800276:	52                   	push   %edx
  800277:	50                   	push   %eax
  800278:	89 f2                	mov    %esi,%edx
  80027a:	89 f8                	mov    %edi,%eax
  80027c:	e8 9e ff ff ff       	call   80021f <printnum>
  800281:	83 c4 20             	add    $0x20,%esp
  800284:	eb 16                	jmp    80029c <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800286:	83 ec 08             	sub    $0x8,%esp
  800289:	56                   	push   %esi
  80028a:	ff 75 18             	pushl  0x18(%ebp)
  80028d:	ff d7                	call   *%edi
  80028f:	83 c4 10             	add    $0x10,%esp
  800292:	eb 03                	jmp    800297 <printnum+0x78>
  800294:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800297:	4b                   	dec    %ebx
  800298:	85 db                	test   %ebx,%ebx
  80029a:	7f ea                	jg     800286 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80029c:	83 ec 08             	sub    $0x8,%esp
  80029f:	56                   	push   %esi
  8002a0:	83 ec 04             	sub    $0x4,%esp
  8002a3:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002a6:	ff 75 e0             	pushl  -0x20(%ebp)
  8002a9:	ff 75 dc             	pushl  -0x24(%ebp)
  8002ac:	ff 75 d8             	pushl  -0x28(%ebp)
  8002af:	e8 f4 08 00 00       	call   800ba8 <__umoddi3>
  8002b4:	83 c4 14             	add    $0x14,%esp
  8002b7:	0f be 80 5a 0d 80 00 	movsbl 0x800d5a(%eax),%eax
  8002be:	50                   	push   %eax
  8002bf:	ff d7                	call   *%edi
}
  8002c1:	83 c4 10             	add    $0x10,%esp
  8002c4:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002c7:	5b                   	pop    %ebx
  8002c8:	5e                   	pop    %esi
  8002c9:	5f                   	pop    %edi
  8002ca:	5d                   	pop    %ebp
  8002cb:	c3                   	ret    

008002cc <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002cc:	55                   	push   %ebp
  8002cd:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002cf:	83 fa 01             	cmp    $0x1,%edx
  8002d2:	7e 0e                	jle    8002e2 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002d4:	8b 10                	mov    (%eax),%edx
  8002d6:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002d9:	89 08                	mov    %ecx,(%eax)
  8002db:	8b 02                	mov    (%edx),%eax
  8002dd:	8b 52 04             	mov    0x4(%edx),%edx
  8002e0:	eb 22                	jmp    800304 <getuint+0x38>
	else if (lflag)
  8002e2:	85 d2                	test   %edx,%edx
  8002e4:	74 10                	je     8002f6 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8002e6:	8b 10                	mov    (%eax),%edx
  8002e8:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002eb:	89 08                	mov    %ecx,(%eax)
  8002ed:	8b 02                	mov    (%edx),%eax
  8002ef:	ba 00 00 00 00       	mov    $0x0,%edx
  8002f4:	eb 0e                	jmp    800304 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8002f6:	8b 10                	mov    (%eax),%edx
  8002f8:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002fb:	89 08                	mov    %ecx,(%eax)
  8002fd:	8b 02                	mov    (%edx),%eax
  8002ff:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800304:	5d                   	pop    %ebp
  800305:	c3                   	ret    

00800306 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800306:	55                   	push   %ebp
  800307:	89 e5                	mov    %esp,%ebp
  800309:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80030c:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80030f:	8b 10                	mov    (%eax),%edx
  800311:	3b 50 04             	cmp    0x4(%eax),%edx
  800314:	73 0a                	jae    800320 <sprintputch+0x1a>
		*b->buf++ = ch;
  800316:	8d 4a 01             	lea    0x1(%edx),%ecx
  800319:	89 08                	mov    %ecx,(%eax)
  80031b:	8b 45 08             	mov    0x8(%ebp),%eax
  80031e:	88 02                	mov    %al,(%edx)
}
  800320:	5d                   	pop    %ebp
  800321:	c3                   	ret    

00800322 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800322:	55                   	push   %ebp
  800323:	89 e5                	mov    %esp,%ebp
  800325:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800328:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80032b:	50                   	push   %eax
  80032c:	ff 75 10             	pushl  0x10(%ebp)
  80032f:	ff 75 0c             	pushl  0xc(%ebp)
  800332:	ff 75 08             	pushl  0x8(%ebp)
  800335:	e8 05 00 00 00       	call   80033f <vprintfmt>
	va_end(ap);
}
  80033a:	83 c4 10             	add    $0x10,%esp
  80033d:	c9                   	leave  
  80033e:	c3                   	ret    

0080033f <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80033f:	55                   	push   %ebp
  800340:	89 e5                	mov    %esp,%ebp
  800342:	57                   	push   %edi
  800343:	56                   	push   %esi
  800344:	53                   	push   %ebx
  800345:	83 ec 2c             	sub    $0x2c,%esp
  800348:	8b 75 08             	mov    0x8(%ebp),%esi
  80034b:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80034e:	8b 7d 10             	mov    0x10(%ebp),%edi
  800351:	eb 12                	jmp    800365 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800353:	85 c0                	test   %eax,%eax
  800355:	0f 84 68 03 00 00    	je     8006c3 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80035b:	83 ec 08             	sub    $0x8,%esp
  80035e:	53                   	push   %ebx
  80035f:	50                   	push   %eax
  800360:	ff d6                	call   *%esi
  800362:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800365:	47                   	inc    %edi
  800366:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80036a:	83 f8 25             	cmp    $0x25,%eax
  80036d:	75 e4                	jne    800353 <vprintfmt+0x14>
  80036f:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800373:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80037a:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800381:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800388:	ba 00 00 00 00       	mov    $0x0,%edx
  80038d:	eb 07                	jmp    800396 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80038f:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  800392:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800396:	8d 47 01             	lea    0x1(%edi),%eax
  800399:	89 45 e0             	mov    %eax,-0x20(%ebp)
  80039c:	0f b6 0f             	movzbl (%edi),%ecx
  80039f:	8a 07                	mov    (%edi),%al
  8003a1:	83 e8 23             	sub    $0x23,%eax
  8003a4:	3c 55                	cmp    $0x55,%al
  8003a6:	0f 87 fe 02 00 00    	ja     8006aa <vprintfmt+0x36b>
  8003ac:	0f b6 c0             	movzbl %al,%eax
  8003af:	ff 24 85 e8 0d 80 00 	jmp    *0x800de8(,%eax,4)
  8003b6:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003b9:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003bd:	eb d7                	jmp    800396 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003bf:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003c2:	b8 00 00 00 00       	mov    $0x0,%eax
  8003c7:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003ca:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003cd:	01 c0                	add    %eax,%eax
  8003cf:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003d3:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003d6:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003d9:	83 fa 09             	cmp    $0x9,%edx
  8003dc:	77 34                	ja     800412 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003de:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003df:	eb e9                	jmp    8003ca <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003e1:	8b 45 14             	mov    0x14(%ebp),%eax
  8003e4:	8d 48 04             	lea    0x4(%eax),%ecx
  8003e7:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8003ea:	8b 00                	mov    (%eax),%eax
  8003ec:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ef:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8003f2:	eb 24                	jmp    800418 <vprintfmt+0xd9>
  8003f4:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003f8:	79 07                	jns    800401 <vprintfmt+0xc2>
  8003fa:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800401:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800404:	eb 90                	jmp    800396 <vprintfmt+0x57>
  800406:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800409:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800410:	eb 84                	jmp    800396 <vprintfmt+0x57>
  800412:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800415:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800418:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80041c:	0f 89 74 ff ff ff    	jns    800396 <vprintfmt+0x57>
				width = precision, precision = -1;
  800422:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800425:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800428:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80042f:	e9 62 ff ff ff       	jmp    800396 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800434:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800435:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800438:	e9 59 ff ff ff       	jmp    800396 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80043d:	8b 45 14             	mov    0x14(%ebp),%eax
  800440:	8d 50 04             	lea    0x4(%eax),%edx
  800443:	89 55 14             	mov    %edx,0x14(%ebp)
  800446:	83 ec 08             	sub    $0x8,%esp
  800449:	53                   	push   %ebx
  80044a:	ff 30                	pushl  (%eax)
  80044c:	ff d6                	call   *%esi
			break;
  80044e:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800451:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800454:	e9 0c ff ff ff       	jmp    800365 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800459:	8b 45 14             	mov    0x14(%ebp),%eax
  80045c:	8d 50 04             	lea    0x4(%eax),%edx
  80045f:	89 55 14             	mov    %edx,0x14(%ebp)
  800462:	8b 00                	mov    (%eax),%eax
  800464:	85 c0                	test   %eax,%eax
  800466:	79 02                	jns    80046a <vprintfmt+0x12b>
  800468:	f7 d8                	neg    %eax
  80046a:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80046c:	83 f8 06             	cmp    $0x6,%eax
  80046f:	7f 0b                	jg     80047c <vprintfmt+0x13d>
  800471:	8b 04 85 40 0f 80 00 	mov    0x800f40(,%eax,4),%eax
  800478:	85 c0                	test   %eax,%eax
  80047a:	75 18                	jne    800494 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80047c:	52                   	push   %edx
  80047d:	68 72 0d 80 00       	push   $0x800d72
  800482:	53                   	push   %ebx
  800483:	56                   	push   %esi
  800484:	e8 99 fe ff ff       	call   800322 <printfmt>
  800489:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80048c:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  80048f:	e9 d1 fe ff ff       	jmp    800365 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  800494:	50                   	push   %eax
  800495:	68 7b 0d 80 00       	push   $0x800d7b
  80049a:	53                   	push   %ebx
  80049b:	56                   	push   %esi
  80049c:	e8 81 fe ff ff       	call   800322 <printfmt>
  8004a1:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004a4:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004a7:	e9 b9 fe ff ff       	jmp    800365 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004ac:	8b 45 14             	mov    0x14(%ebp),%eax
  8004af:	8d 50 04             	lea    0x4(%eax),%edx
  8004b2:	89 55 14             	mov    %edx,0x14(%ebp)
  8004b5:	8b 38                	mov    (%eax),%edi
  8004b7:	85 ff                	test   %edi,%edi
  8004b9:	75 05                	jne    8004c0 <vprintfmt+0x181>
				p = "(null)";
  8004bb:	bf 6b 0d 80 00       	mov    $0x800d6b,%edi
			if (width > 0 && padc != '-')
  8004c0:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004c4:	0f 8e 90 00 00 00    	jle    80055a <vprintfmt+0x21b>
  8004ca:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004ce:	0f 84 8e 00 00 00    	je     800562 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004d4:	83 ec 08             	sub    $0x8,%esp
  8004d7:	ff 75 d0             	pushl  -0x30(%ebp)
  8004da:	57                   	push   %edi
  8004db:	e8 70 02 00 00       	call   800750 <strnlen>
  8004e0:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8004e3:	29 c1                	sub    %eax,%ecx
  8004e5:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8004e8:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8004eb:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8004ef:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004f2:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8004f5:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004f7:	eb 0d                	jmp    800506 <vprintfmt+0x1c7>
					putch(padc, putdat);
  8004f9:	83 ec 08             	sub    $0x8,%esp
  8004fc:	53                   	push   %ebx
  8004fd:	ff 75 e4             	pushl  -0x1c(%ebp)
  800500:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800502:	4f                   	dec    %edi
  800503:	83 c4 10             	add    $0x10,%esp
  800506:	85 ff                	test   %edi,%edi
  800508:	7f ef                	jg     8004f9 <vprintfmt+0x1ba>
  80050a:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80050d:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800510:	89 c8                	mov    %ecx,%eax
  800512:	85 c9                	test   %ecx,%ecx
  800514:	79 05                	jns    80051b <vprintfmt+0x1dc>
  800516:	b8 00 00 00 00       	mov    $0x0,%eax
  80051b:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80051e:	29 c1                	sub    %eax,%ecx
  800520:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800523:	89 75 08             	mov    %esi,0x8(%ebp)
  800526:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800529:	eb 3d                	jmp    800568 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80052b:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80052f:	74 19                	je     80054a <vprintfmt+0x20b>
  800531:	0f be c0             	movsbl %al,%eax
  800534:	83 e8 20             	sub    $0x20,%eax
  800537:	83 f8 5e             	cmp    $0x5e,%eax
  80053a:	76 0e                	jbe    80054a <vprintfmt+0x20b>
					putch('?', putdat);
  80053c:	83 ec 08             	sub    $0x8,%esp
  80053f:	53                   	push   %ebx
  800540:	6a 3f                	push   $0x3f
  800542:	ff 55 08             	call   *0x8(%ebp)
  800545:	83 c4 10             	add    $0x10,%esp
  800548:	eb 0b                	jmp    800555 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80054a:	83 ec 08             	sub    $0x8,%esp
  80054d:	53                   	push   %ebx
  80054e:	52                   	push   %edx
  80054f:	ff 55 08             	call   *0x8(%ebp)
  800552:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800555:	ff 4d e4             	decl   -0x1c(%ebp)
  800558:	eb 0e                	jmp    800568 <vprintfmt+0x229>
  80055a:	89 75 08             	mov    %esi,0x8(%ebp)
  80055d:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800560:	eb 06                	jmp    800568 <vprintfmt+0x229>
  800562:	89 75 08             	mov    %esi,0x8(%ebp)
  800565:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800568:	47                   	inc    %edi
  800569:	8a 47 ff             	mov    -0x1(%edi),%al
  80056c:	0f be d0             	movsbl %al,%edx
  80056f:	85 d2                	test   %edx,%edx
  800571:	74 1d                	je     800590 <vprintfmt+0x251>
  800573:	85 f6                	test   %esi,%esi
  800575:	78 b4                	js     80052b <vprintfmt+0x1ec>
  800577:	4e                   	dec    %esi
  800578:	79 b1                	jns    80052b <vprintfmt+0x1ec>
  80057a:	8b 75 08             	mov    0x8(%ebp),%esi
  80057d:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800580:	eb 14                	jmp    800596 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800582:	83 ec 08             	sub    $0x8,%esp
  800585:	53                   	push   %ebx
  800586:	6a 20                	push   $0x20
  800588:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80058a:	4f                   	dec    %edi
  80058b:	83 c4 10             	add    $0x10,%esp
  80058e:	eb 06                	jmp    800596 <vprintfmt+0x257>
  800590:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800593:	8b 75 08             	mov    0x8(%ebp),%esi
  800596:	85 ff                	test   %edi,%edi
  800598:	7f e8                	jg     800582 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80059a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80059d:	e9 c3 fd ff ff       	jmp    800365 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8005a2:	83 fa 01             	cmp    $0x1,%edx
  8005a5:	7e 16                	jle    8005bd <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8005a7:	8b 45 14             	mov    0x14(%ebp),%eax
  8005aa:	8d 50 08             	lea    0x8(%eax),%edx
  8005ad:	89 55 14             	mov    %edx,0x14(%ebp)
  8005b0:	8b 50 04             	mov    0x4(%eax),%edx
  8005b3:	8b 00                	mov    (%eax),%eax
  8005b5:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005b8:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8005bb:	eb 32                	jmp    8005ef <vprintfmt+0x2b0>
	else if (lflag)
  8005bd:	85 d2                	test   %edx,%edx
  8005bf:	74 18                	je     8005d9 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005c1:	8b 45 14             	mov    0x14(%ebp),%eax
  8005c4:	8d 50 04             	lea    0x4(%eax),%edx
  8005c7:	89 55 14             	mov    %edx,0x14(%ebp)
  8005ca:	8b 00                	mov    (%eax),%eax
  8005cc:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005cf:	89 c1                	mov    %eax,%ecx
  8005d1:	c1 f9 1f             	sar    $0x1f,%ecx
  8005d4:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005d7:	eb 16                	jmp    8005ef <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005d9:	8b 45 14             	mov    0x14(%ebp),%eax
  8005dc:	8d 50 04             	lea    0x4(%eax),%edx
  8005df:	89 55 14             	mov    %edx,0x14(%ebp)
  8005e2:	8b 00                	mov    (%eax),%eax
  8005e4:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005e7:	89 c1                	mov    %eax,%ecx
  8005e9:	c1 f9 1f             	sar    $0x1f,%ecx
  8005ec:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005ef:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005f2:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8005f5:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8005f9:	79 76                	jns    800671 <vprintfmt+0x332>
				putch('-', putdat);
  8005fb:	83 ec 08             	sub    $0x8,%esp
  8005fe:	53                   	push   %ebx
  8005ff:	6a 2d                	push   $0x2d
  800601:	ff d6                	call   *%esi
				num = -(long long) num;
  800603:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800606:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800609:	f7 d8                	neg    %eax
  80060b:	83 d2 00             	adc    $0x0,%edx
  80060e:	f7 da                	neg    %edx
  800610:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800613:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800618:	eb 5c                	jmp    800676 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80061a:	8d 45 14             	lea    0x14(%ebp),%eax
  80061d:	e8 aa fc ff ff       	call   8002cc <getuint>
			base = 10;
  800622:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800627:	eb 4d                	jmp    800676 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800629:	8d 45 14             	lea    0x14(%ebp),%eax
  80062c:	e8 9b fc ff ff       	call   8002cc <getuint>
			base = 8;
  800631:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800636:	eb 3e                	jmp    800676 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800638:	83 ec 08             	sub    $0x8,%esp
  80063b:	53                   	push   %ebx
  80063c:	6a 30                	push   $0x30
  80063e:	ff d6                	call   *%esi
			putch('x', putdat);
  800640:	83 c4 08             	add    $0x8,%esp
  800643:	53                   	push   %ebx
  800644:	6a 78                	push   $0x78
  800646:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800648:	8b 45 14             	mov    0x14(%ebp),%eax
  80064b:	8d 50 04             	lea    0x4(%eax),%edx
  80064e:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800651:	8b 00                	mov    (%eax),%eax
  800653:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800658:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80065b:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800660:	eb 14                	jmp    800676 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800662:	8d 45 14             	lea    0x14(%ebp),%eax
  800665:	e8 62 fc ff ff       	call   8002cc <getuint>
			base = 16;
  80066a:	b9 10 00 00 00       	mov    $0x10,%ecx
  80066f:	eb 05                	jmp    800676 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800671:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800676:	83 ec 0c             	sub    $0xc,%esp
  800679:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80067d:	57                   	push   %edi
  80067e:	ff 75 e4             	pushl  -0x1c(%ebp)
  800681:	51                   	push   %ecx
  800682:	52                   	push   %edx
  800683:	50                   	push   %eax
  800684:	89 da                	mov    %ebx,%edx
  800686:	89 f0                	mov    %esi,%eax
  800688:	e8 92 fb ff ff       	call   80021f <printnum>
			break;
  80068d:	83 c4 20             	add    $0x20,%esp
  800690:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800693:	e9 cd fc ff ff       	jmp    800365 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800698:	83 ec 08             	sub    $0x8,%esp
  80069b:	53                   	push   %ebx
  80069c:	51                   	push   %ecx
  80069d:	ff d6                	call   *%esi
			break;
  80069f:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006a2:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8006a5:	e9 bb fc ff ff       	jmp    800365 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006aa:	83 ec 08             	sub    $0x8,%esp
  8006ad:	53                   	push   %ebx
  8006ae:	6a 25                	push   $0x25
  8006b0:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006b2:	83 c4 10             	add    $0x10,%esp
  8006b5:	eb 01                	jmp    8006b8 <vprintfmt+0x379>
  8006b7:	4f                   	dec    %edi
  8006b8:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006bc:	75 f9                	jne    8006b7 <vprintfmt+0x378>
  8006be:	e9 a2 fc ff ff       	jmp    800365 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006c3:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006c6:	5b                   	pop    %ebx
  8006c7:	5e                   	pop    %esi
  8006c8:	5f                   	pop    %edi
  8006c9:	5d                   	pop    %ebp
  8006ca:	c3                   	ret    

008006cb <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006cb:	55                   	push   %ebp
  8006cc:	89 e5                	mov    %esp,%ebp
  8006ce:	83 ec 18             	sub    $0x18,%esp
  8006d1:	8b 45 08             	mov    0x8(%ebp),%eax
  8006d4:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006d7:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006da:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006de:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8006e1:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8006e8:	85 c0                	test   %eax,%eax
  8006ea:	74 26                	je     800712 <vsnprintf+0x47>
  8006ec:	85 d2                	test   %edx,%edx
  8006ee:	7e 29                	jle    800719 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8006f0:	ff 75 14             	pushl  0x14(%ebp)
  8006f3:	ff 75 10             	pushl  0x10(%ebp)
  8006f6:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8006f9:	50                   	push   %eax
  8006fa:	68 06 03 80 00       	push   $0x800306
  8006ff:	e8 3b fc ff ff       	call   80033f <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800704:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800707:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80070a:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80070d:	83 c4 10             	add    $0x10,%esp
  800710:	eb 0c                	jmp    80071e <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800712:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800717:	eb 05                	jmp    80071e <vsnprintf+0x53>
  800719:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80071e:	c9                   	leave  
  80071f:	c3                   	ret    

00800720 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800720:	55                   	push   %ebp
  800721:	89 e5                	mov    %esp,%ebp
  800723:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800726:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800729:	50                   	push   %eax
  80072a:	ff 75 10             	pushl  0x10(%ebp)
  80072d:	ff 75 0c             	pushl  0xc(%ebp)
  800730:	ff 75 08             	pushl  0x8(%ebp)
  800733:	e8 93 ff ff ff       	call   8006cb <vsnprintf>
	va_end(ap);

	return rc;
}
  800738:	c9                   	leave  
  800739:	c3                   	ret    

0080073a <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80073a:	55                   	push   %ebp
  80073b:	89 e5                	mov    %esp,%ebp
  80073d:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800740:	b8 00 00 00 00       	mov    $0x0,%eax
  800745:	eb 01                	jmp    800748 <strlen+0xe>
		n++;
  800747:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800748:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80074c:	75 f9                	jne    800747 <strlen+0xd>
		n++;
	return n;
}
  80074e:	5d                   	pop    %ebp
  80074f:	c3                   	ret    

00800750 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800750:	55                   	push   %ebp
  800751:	89 e5                	mov    %esp,%ebp
  800753:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800756:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800759:	ba 00 00 00 00       	mov    $0x0,%edx
  80075e:	eb 01                	jmp    800761 <strnlen+0x11>
		n++;
  800760:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800761:	39 c2                	cmp    %eax,%edx
  800763:	74 08                	je     80076d <strnlen+0x1d>
  800765:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800769:	75 f5                	jne    800760 <strnlen+0x10>
  80076b:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80076d:	5d                   	pop    %ebp
  80076e:	c3                   	ret    

0080076f <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  80076f:	55                   	push   %ebp
  800770:	89 e5                	mov    %esp,%ebp
  800772:	53                   	push   %ebx
  800773:	8b 45 08             	mov    0x8(%ebp),%eax
  800776:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800779:	89 c2                	mov    %eax,%edx
  80077b:	42                   	inc    %edx
  80077c:	41                   	inc    %ecx
  80077d:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800780:	88 5a ff             	mov    %bl,-0x1(%edx)
  800783:	84 db                	test   %bl,%bl
  800785:	75 f4                	jne    80077b <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800787:	5b                   	pop    %ebx
  800788:	5d                   	pop    %ebp
  800789:	c3                   	ret    

0080078a <strcat>:

char *
strcat(char *dst, const char *src)
{
  80078a:	55                   	push   %ebp
  80078b:	89 e5                	mov    %esp,%ebp
  80078d:	53                   	push   %ebx
  80078e:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  800791:	53                   	push   %ebx
  800792:	e8 a3 ff ff ff       	call   80073a <strlen>
  800797:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  80079a:	ff 75 0c             	pushl  0xc(%ebp)
  80079d:	01 d8                	add    %ebx,%eax
  80079f:	50                   	push   %eax
  8007a0:	e8 ca ff ff ff       	call   80076f <strcpy>
	return dst;
}
  8007a5:	89 d8                	mov    %ebx,%eax
  8007a7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8007aa:	c9                   	leave  
  8007ab:	c3                   	ret    

008007ac <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8007ac:	55                   	push   %ebp
  8007ad:	89 e5                	mov    %esp,%ebp
  8007af:	56                   	push   %esi
  8007b0:	53                   	push   %ebx
  8007b1:	8b 75 08             	mov    0x8(%ebp),%esi
  8007b4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007b7:	89 f3                	mov    %esi,%ebx
  8007b9:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007bc:	89 f2                	mov    %esi,%edx
  8007be:	eb 0c                	jmp    8007cc <strncpy+0x20>
		*dst++ = *src;
  8007c0:	42                   	inc    %edx
  8007c1:	8a 01                	mov    (%ecx),%al
  8007c3:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007c6:	80 39 01             	cmpb   $0x1,(%ecx)
  8007c9:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007cc:	39 da                	cmp    %ebx,%edx
  8007ce:	75 f0                	jne    8007c0 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007d0:	89 f0                	mov    %esi,%eax
  8007d2:	5b                   	pop    %ebx
  8007d3:	5e                   	pop    %esi
  8007d4:	5d                   	pop    %ebp
  8007d5:	c3                   	ret    

008007d6 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007d6:	55                   	push   %ebp
  8007d7:	89 e5                	mov    %esp,%ebp
  8007d9:	56                   	push   %esi
  8007da:	53                   	push   %ebx
  8007db:	8b 75 08             	mov    0x8(%ebp),%esi
  8007de:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007e1:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8007e4:	85 c0                	test   %eax,%eax
  8007e6:	74 1e                	je     800806 <strlcpy+0x30>
  8007e8:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8007ec:	89 f2                	mov    %esi,%edx
  8007ee:	eb 05                	jmp    8007f5 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8007f0:	42                   	inc    %edx
  8007f1:	41                   	inc    %ecx
  8007f2:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8007f5:	39 c2                	cmp    %eax,%edx
  8007f7:	74 08                	je     800801 <strlcpy+0x2b>
  8007f9:	8a 19                	mov    (%ecx),%bl
  8007fb:	84 db                	test   %bl,%bl
  8007fd:	75 f1                	jne    8007f0 <strlcpy+0x1a>
  8007ff:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800801:	c6 00 00             	movb   $0x0,(%eax)
  800804:	eb 02                	jmp    800808 <strlcpy+0x32>
  800806:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800808:	29 f0                	sub    %esi,%eax
}
  80080a:	5b                   	pop    %ebx
  80080b:	5e                   	pop    %esi
  80080c:	5d                   	pop    %ebp
  80080d:	c3                   	ret    

0080080e <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80080e:	55                   	push   %ebp
  80080f:	89 e5                	mov    %esp,%ebp
  800811:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800814:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800817:	eb 02                	jmp    80081b <strcmp+0xd>
		p++, q++;
  800819:	41                   	inc    %ecx
  80081a:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80081b:	8a 01                	mov    (%ecx),%al
  80081d:	84 c0                	test   %al,%al
  80081f:	74 04                	je     800825 <strcmp+0x17>
  800821:	3a 02                	cmp    (%edx),%al
  800823:	74 f4                	je     800819 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800825:	0f b6 c0             	movzbl %al,%eax
  800828:	0f b6 12             	movzbl (%edx),%edx
  80082b:	29 d0                	sub    %edx,%eax
}
  80082d:	5d                   	pop    %ebp
  80082e:	c3                   	ret    

0080082f <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  80082f:	55                   	push   %ebp
  800830:	89 e5                	mov    %esp,%ebp
  800832:	53                   	push   %ebx
  800833:	8b 45 08             	mov    0x8(%ebp),%eax
  800836:	8b 55 0c             	mov    0xc(%ebp),%edx
  800839:	89 c3                	mov    %eax,%ebx
  80083b:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  80083e:	eb 02                	jmp    800842 <strncmp+0x13>
		n--, p++, q++;
  800840:	40                   	inc    %eax
  800841:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800842:	39 d8                	cmp    %ebx,%eax
  800844:	74 14                	je     80085a <strncmp+0x2b>
  800846:	8a 08                	mov    (%eax),%cl
  800848:	84 c9                	test   %cl,%cl
  80084a:	74 04                	je     800850 <strncmp+0x21>
  80084c:	3a 0a                	cmp    (%edx),%cl
  80084e:	74 f0                	je     800840 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800850:	0f b6 00             	movzbl (%eax),%eax
  800853:	0f b6 12             	movzbl (%edx),%edx
  800856:	29 d0                	sub    %edx,%eax
  800858:	eb 05                	jmp    80085f <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80085a:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  80085f:	5b                   	pop    %ebx
  800860:	5d                   	pop    %ebp
  800861:	c3                   	ret    

00800862 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800862:	55                   	push   %ebp
  800863:	89 e5                	mov    %esp,%ebp
  800865:	8b 45 08             	mov    0x8(%ebp),%eax
  800868:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80086b:	eb 05                	jmp    800872 <strchr+0x10>
		if (*s == c)
  80086d:	38 ca                	cmp    %cl,%dl
  80086f:	74 0c                	je     80087d <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800871:	40                   	inc    %eax
  800872:	8a 10                	mov    (%eax),%dl
  800874:	84 d2                	test   %dl,%dl
  800876:	75 f5                	jne    80086d <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800878:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80087d:	5d                   	pop    %ebp
  80087e:	c3                   	ret    

0080087f <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  80087f:	55                   	push   %ebp
  800880:	89 e5                	mov    %esp,%ebp
  800882:	8b 45 08             	mov    0x8(%ebp),%eax
  800885:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800888:	eb 05                	jmp    80088f <strfind+0x10>
		if (*s == c)
  80088a:	38 ca                	cmp    %cl,%dl
  80088c:	74 07                	je     800895 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  80088e:	40                   	inc    %eax
  80088f:	8a 10                	mov    (%eax),%dl
  800891:	84 d2                	test   %dl,%dl
  800893:	75 f5                	jne    80088a <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  800895:	5d                   	pop    %ebp
  800896:	c3                   	ret    

00800897 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  800897:	55                   	push   %ebp
  800898:	89 e5                	mov    %esp,%ebp
  80089a:	57                   	push   %edi
  80089b:	56                   	push   %esi
  80089c:	53                   	push   %ebx
  80089d:	8b 7d 08             	mov    0x8(%ebp),%edi
  8008a0:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8008a3:	85 c9                	test   %ecx,%ecx
  8008a5:	74 36                	je     8008dd <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8008a7:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8008ad:	75 28                	jne    8008d7 <memset+0x40>
  8008af:	f6 c1 03             	test   $0x3,%cl
  8008b2:	75 23                	jne    8008d7 <memset+0x40>
		c &= 0xFF;
  8008b4:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8008b8:	89 d3                	mov    %edx,%ebx
  8008ba:	c1 e3 08             	shl    $0x8,%ebx
  8008bd:	89 d6                	mov    %edx,%esi
  8008bf:	c1 e6 18             	shl    $0x18,%esi
  8008c2:	89 d0                	mov    %edx,%eax
  8008c4:	c1 e0 10             	shl    $0x10,%eax
  8008c7:	09 f0                	or     %esi,%eax
  8008c9:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008cb:	89 d8                	mov    %ebx,%eax
  8008cd:	09 d0                	or     %edx,%eax
  8008cf:	c1 e9 02             	shr    $0x2,%ecx
  8008d2:	fc                   	cld    
  8008d3:	f3 ab                	rep stos %eax,%es:(%edi)
  8008d5:	eb 06                	jmp    8008dd <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008d7:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008da:	fc                   	cld    
  8008db:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008dd:	89 f8                	mov    %edi,%eax
  8008df:	5b                   	pop    %ebx
  8008e0:	5e                   	pop    %esi
  8008e1:	5f                   	pop    %edi
  8008e2:	5d                   	pop    %ebp
  8008e3:	c3                   	ret    

008008e4 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8008e4:	55                   	push   %ebp
  8008e5:	89 e5                	mov    %esp,%ebp
  8008e7:	57                   	push   %edi
  8008e8:	56                   	push   %esi
  8008e9:	8b 45 08             	mov    0x8(%ebp),%eax
  8008ec:	8b 75 0c             	mov    0xc(%ebp),%esi
  8008ef:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8008f2:	39 c6                	cmp    %eax,%esi
  8008f4:	73 33                	jae    800929 <memmove+0x45>
  8008f6:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  8008f9:	39 d0                	cmp    %edx,%eax
  8008fb:	73 2c                	jae    800929 <memmove+0x45>
		s += n;
		d += n;
  8008fd:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800900:	89 d6                	mov    %edx,%esi
  800902:	09 fe                	or     %edi,%esi
  800904:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80090a:	75 13                	jne    80091f <memmove+0x3b>
  80090c:	f6 c1 03             	test   $0x3,%cl
  80090f:	75 0e                	jne    80091f <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800911:	83 ef 04             	sub    $0x4,%edi
  800914:	8d 72 fc             	lea    -0x4(%edx),%esi
  800917:	c1 e9 02             	shr    $0x2,%ecx
  80091a:	fd                   	std    
  80091b:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80091d:	eb 07                	jmp    800926 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  80091f:	4f                   	dec    %edi
  800920:	8d 72 ff             	lea    -0x1(%edx),%esi
  800923:	fd                   	std    
  800924:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800926:	fc                   	cld    
  800927:	eb 1d                	jmp    800946 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800929:	89 f2                	mov    %esi,%edx
  80092b:	09 c2                	or     %eax,%edx
  80092d:	f6 c2 03             	test   $0x3,%dl
  800930:	75 0f                	jne    800941 <memmove+0x5d>
  800932:	f6 c1 03             	test   $0x3,%cl
  800935:	75 0a                	jne    800941 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800937:	c1 e9 02             	shr    $0x2,%ecx
  80093a:	89 c7                	mov    %eax,%edi
  80093c:	fc                   	cld    
  80093d:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80093f:	eb 05                	jmp    800946 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800941:	89 c7                	mov    %eax,%edi
  800943:	fc                   	cld    
  800944:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800946:	5e                   	pop    %esi
  800947:	5f                   	pop    %edi
  800948:	5d                   	pop    %ebp
  800949:	c3                   	ret    

0080094a <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80094a:	55                   	push   %ebp
  80094b:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  80094d:	ff 75 10             	pushl  0x10(%ebp)
  800950:	ff 75 0c             	pushl  0xc(%ebp)
  800953:	ff 75 08             	pushl  0x8(%ebp)
  800956:	e8 89 ff ff ff       	call   8008e4 <memmove>
}
  80095b:	c9                   	leave  
  80095c:	c3                   	ret    

0080095d <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  80095d:	55                   	push   %ebp
  80095e:	89 e5                	mov    %esp,%ebp
  800960:	56                   	push   %esi
  800961:	53                   	push   %ebx
  800962:	8b 45 08             	mov    0x8(%ebp),%eax
  800965:	8b 55 0c             	mov    0xc(%ebp),%edx
  800968:	89 c6                	mov    %eax,%esi
  80096a:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80096d:	eb 14                	jmp    800983 <memcmp+0x26>
		if (*s1 != *s2)
  80096f:	8a 08                	mov    (%eax),%cl
  800971:	8a 1a                	mov    (%edx),%bl
  800973:	38 d9                	cmp    %bl,%cl
  800975:	74 0a                	je     800981 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800977:	0f b6 c1             	movzbl %cl,%eax
  80097a:	0f b6 db             	movzbl %bl,%ebx
  80097d:	29 d8                	sub    %ebx,%eax
  80097f:	eb 0b                	jmp    80098c <memcmp+0x2f>
		s1++, s2++;
  800981:	40                   	inc    %eax
  800982:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800983:	39 f0                	cmp    %esi,%eax
  800985:	75 e8                	jne    80096f <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800987:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80098c:	5b                   	pop    %ebx
  80098d:	5e                   	pop    %esi
  80098e:	5d                   	pop    %ebp
  80098f:	c3                   	ret    

00800990 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800990:	55                   	push   %ebp
  800991:	89 e5                	mov    %esp,%ebp
  800993:	53                   	push   %ebx
  800994:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  800997:	89 c1                	mov    %eax,%ecx
  800999:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  80099c:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009a0:	eb 08                	jmp    8009aa <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8009a2:	0f b6 10             	movzbl (%eax),%edx
  8009a5:	39 da                	cmp    %ebx,%edx
  8009a7:	74 05                	je     8009ae <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009a9:	40                   	inc    %eax
  8009aa:	39 c8                	cmp    %ecx,%eax
  8009ac:	72 f4                	jb     8009a2 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8009ae:	5b                   	pop    %ebx
  8009af:	5d                   	pop    %ebp
  8009b0:	c3                   	ret    

008009b1 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8009b1:	55                   	push   %ebp
  8009b2:	89 e5                	mov    %esp,%ebp
  8009b4:	57                   	push   %edi
  8009b5:	56                   	push   %esi
  8009b6:	53                   	push   %ebx
  8009b7:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009ba:	eb 01                	jmp    8009bd <strtol+0xc>
		s++;
  8009bc:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009bd:	8a 01                	mov    (%ecx),%al
  8009bf:	3c 20                	cmp    $0x20,%al
  8009c1:	74 f9                	je     8009bc <strtol+0xb>
  8009c3:	3c 09                	cmp    $0x9,%al
  8009c5:	74 f5                	je     8009bc <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009c7:	3c 2b                	cmp    $0x2b,%al
  8009c9:	75 08                	jne    8009d3 <strtol+0x22>
		s++;
  8009cb:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009cc:	bf 00 00 00 00       	mov    $0x0,%edi
  8009d1:	eb 11                	jmp    8009e4 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009d3:	3c 2d                	cmp    $0x2d,%al
  8009d5:	75 08                	jne    8009df <strtol+0x2e>
		s++, neg = 1;
  8009d7:	41                   	inc    %ecx
  8009d8:	bf 01 00 00 00       	mov    $0x1,%edi
  8009dd:	eb 05                	jmp    8009e4 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009df:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009e4:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009e8:	0f 84 87 00 00 00    	je     800a75 <strtol+0xc4>
  8009ee:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  8009f2:	75 27                	jne    800a1b <strtol+0x6a>
  8009f4:	80 39 30             	cmpb   $0x30,(%ecx)
  8009f7:	75 22                	jne    800a1b <strtol+0x6a>
  8009f9:	e9 88 00 00 00       	jmp    800a86 <strtol+0xd5>
		s += 2, base = 16;
  8009fe:	83 c1 02             	add    $0x2,%ecx
  800a01:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a08:	eb 11                	jmp    800a1b <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a0a:	41                   	inc    %ecx
  800a0b:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a12:	eb 07                	jmp    800a1b <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a14:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a1b:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a20:	8a 11                	mov    (%ecx),%dl
  800a22:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a25:	80 fb 09             	cmp    $0x9,%bl
  800a28:	77 08                	ja     800a32 <strtol+0x81>
			dig = *s - '0';
  800a2a:	0f be d2             	movsbl %dl,%edx
  800a2d:	83 ea 30             	sub    $0x30,%edx
  800a30:	eb 22                	jmp    800a54 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a32:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a35:	89 f3                	mov    %esi,%ebx
  800a37:	80 fb 19             	cmp    $0x19,%bl
  800a3a:	77 08                	ja     800a44 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a3c:	0f be d2             	movsbl %dl,%edx
  800a3f:	83 ea 57             	sub    $0x57,%edx
  800a42:	eb 10                	jmp    800a54 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a44:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a47:	89 f3                	mov    %esi,%ebx
  800a49:	80 fb 19             	cmp    $0x19,%bl
  800a4c:	77 14                	ja     800a62 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a4e:	0f be d2             	movsbl %dl,%edx
  800a51:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a54:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a57:	7d 09                	jge    800a62 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a59:	41                   	inc    %ecx
  800a5a:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a5e:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a60:	eb be                	jmp    800a20 <strtol+0x6f>

	if (endptr)
  800a62:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a66:	74 05                	je     800a6d <strtol+0xbc>
		*endptr = (char *) s;
  800a68:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a6b:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a6d:	85 ff                	test   %edi,%edi
  800a6f:	74 21                	je     800a92 <strtol+0xe1>
  800a71:	f7 d8                	neg    %eax
  800a73:	eb 1d                	jmp    800a92 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a75:	80 39 30             	cmpb   $0x30,(%ecx)
  800a78:	75 9a                	jne    800a14 <strtol+0x63>
  800a7a:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a7e:	0f 84 7a ff ff ff    	je     8009fe <strtol+0x4d>
  800a84:	eb 84                	jmp    800a0a <strtol+0x59>
  800a86:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a8a:	0f 84 6e ff ff ff    	je     8009fe <strtol+0x4d>
  800a90:	eb 89                	jmp    800a1b <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a92:	5b                   	pop    %ebx
  800a93:	5e                   	pop    %esi
  800a94:	5f                   	pop    %edi
  800a95:	5d                   	pop    %ebp
  800a96:	c3                   	ret    
  800a97:	90                   	nop

00800a98 <__udivdi3>:
  800a98:	55                   	push   %ebp
  800a99:	57                   	push   %edi
  800a9a:	56                   	push   %esi
  800a9b:	53                   	push   %ebx
  800a9c:	83 ec 1c             	sub    $0x1c,%esp
  800a9f:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800aa3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800aa7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800aab:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800aaf:	89 ca                	mov    %ecx,%edx
  800ab1:	89 f8                	mov    %edi,%eax
  800ab3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800ab7:	85 f6                	test   %esi,%esi
  800ab9:	75 2d                	jne    800ae8 <__udivdi3+0x50>
  800abb:	39 cf                	cmp    %ecx,%edi
  800abd:	77 65                	ja     800b24 <__udivdi3+0x8c>
  800abf:	89 fd                	mov    %edi,%ebp
  800ac1:	85 ff                	test   %edi,%edi
  800ac3:	75 0b                	jne    800ad0 <__udivdi3+0x38>
  800ac5:	b8 01 00 00 00       	mov    $0x1,%eax
  800aca:	31 d2                	xor    %edx,%edx
  800acc:	f7 f7                	div    %edi
  800ace:	89 c5                	mov    %eax,%ebp
  800ad0:	31 d2                	xor    %edx,%edx
  800ad2:	89 c8                	mov    %ecx,%eax
  800ad4:	f7 f5                	div    %ebp
  800ad6:	89 c1                	mov    %eax,%ecx
  800ad8:	89 d8                	mov    %ebx,%eax
  800ada:	f7 f5                	div    %ebp
  800adc:	89 cf                	mov    %ecx,%edi
  800ade:	89 fa                	mov    %edi,%edx
  800ae0:	83 c4 1c             	add    $0x1c,%esp
  800ae3:	5b                   	pop    %ebx
  800ae4:	5e                   	pop    %esi
  800ae5:	5f                   	pop    %edi
  800ae6:	5d                   	pop    %ebp
  800ae7:	c3                   	ret    
  800ae8:	39 ce                	cmp    %ecx,%esi
  800aea:	77 28                	ja     800b14 <__udivdi3+0x7c>
  800aec:	0f bd fe             	bsr    %esi,%edi
  800aef:	83 f7 1f             	xor    $0x1f,%edi
  800af2:	75 40                	jne    800b34 <__udivdi3+0x9c>
  800af4:	39 ce                	cmp    %ecx,%esi
  800af6:	72 0a                	jb     800b02 <__udivdi3+0x6a>
  800af8:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800afc:	0f 87 9e 00 00 00    	ja     800ba0 <__udivdi3+0x108>
  800b02:	b8 01 00 00 00       	mov    $0x1,%eax
  800b07:	89 fa                	mov    %edi,%edx
  800b09:	83 c4 1c             	add    $0x1c,%esp
  800b0c:	5b                   	pop    %ebx
  800b0d:	5e                   	pop    %esi
  800b0e:	5f                   	pop    %edi
  800b0f:	5d                   	pop    %ebp
  800b10:	c3                   	ret    
  800b11:	8d 76 00             	lea    0x0(%esi),%esi
  800b14:	31 ff                	xor    %edi,%edi
  800b16:	31 c0                	xor    %eax,%eax
  800b18:	89 fa                	mov    %edi,%edx
  800b1a:	83 c4 1c             	add    $0x1c,%esp
  800b1d:	5b                   	pop    %ebx
  800b1e:	5e                   	pop    %esi
  800b1f:	5f                   	pop    %edi
  800b20:	5d                   	pop    %ebp
  800b21:	c3                   	ret    
  800b22:	66 90                	xchg   %ax,%ax
  800b24:	89 d8                	mov    %ebx,%eax
  800b26:	f7 f7                	div    %edi
  800b28:	31 ff                	xor    %edi,%edi
  800b2a:	89 fa                	mov    %edi,%edx
  800b2c:	83 c4 1c             	add    $0x1c,%esp
  800b2f:	5b                   	pop    %ebx
  800b30:	5e                   	pop    %esi
  800b31:	5f                   	pop    %edi
  800b32:	5d                   	pop    %ebp
  800b33:	c3                   	ret    
  800b34:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b39:	89 eb                	mov    %ebp,%ebx
  800b3b:	29 fb                	sub    %edi,%ebx
  800b3d:	89 f9                	mov    %edi,%ecx
  800b3f:	d3 e6                	shl    %cl,%esi
  800b41:	89 c5                	mov    %eax,%ebp
  800b43:	88 d9                	mov    %bl,%cl
  800b45:	d3 ed                	shr    %cl,%ebp
  800b47:	89 e9                	mov    %ebp,%ecx
  800b49:	09 f1                	or     %esi,%ecx
  800b4b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b4f:	89 f9                	mov    %edi,%ecx
  800b51:	d3 e0                	shl    %cl,%eax
  800b53:	89 c5                	mov    %eax,%ebp
  800b55:	89 d6                	mov    %edx,%esi
  800b57:	88 d9                	mov    %bl,%cl
  800b59:	d3 ee                	shr    %cl,%esi
  800b5b:	89 f9                	mov    %edi,%ecx
  800b5d:	d3 e2                	shl    %cl,%edx
  800b5f:	8b 44 24 08          	mov    0x8(%esp),%eax
  800b63:	88 d9                	mov    %bl,%cl
  800b65:	d3 e8                	shr    %cl,%eax
  800b67:	09 c2                	or     %eax,%edx
  800b69:	89 d0                	mov    %edx,%eax
  800b6b:	89 f2                	mov    %esi,%edx
  800b6d:	f7 74 24 0c          	divl   0xc(%esp)
  800b71:	89 d6                	mov    %edx,%esi
  800b73:	89 c3                	mov    %eax,%ebx
  800b75:	f7 e5                	mul    %ebp
  800b77:	39 d6                	cmp    %edx,%esi
  800b79:	72 19                	jb     800b94 <__udivdi3+0xfc>
  800b7b:	74 0b                	je     800b88 <__udivdi3+0xf0>
  800b7d:	89 d8                	mov    %ebx,%eax
  800b7f:	31 ff                	xor    %edi,%edi
  800b81:	e9 58 ff ff ff       	jmp    800ade <__udivdi3+0x46>
  800b86:	66 90                	xchg   %ax,%ax
  800b88:	8b 54 24 08          	mov    0x8(%esp),%edx
  800b8c:	89 f9                	mov    %edi,%ecx
  800b8e:	d3 e2                	shl    %cl,%edx
  800b90:	39 c2                	cmp    %eax,%edx
  800b92:	73 e9                	jae    800b7d <__udivdi3+0xe5>
  800b94:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800b97:	31 ff                	xor    %edi,%edi
  800b99:	e9 40 ff ff ff       	jmp    800ade <__udivdi3+0x46>
  800b9e:	66 90                	xchg   %ax,%ax
  800ba0:	31 c0                	xor    %eax,%eax
  800ba2:	e9 37 ff ff ff       	jmp    800ade <__udivdi3+0x46>
  800ba7:	90                   	nop

00800ba8 <__umoddi3>:
  800ba8:	55                   	push   %ebp
  800ba9:	57                   	push   %edi
  800baa:	56                   	push   %esi
  800bab:	53                   	push   %ebx
  800bac:	83 ec 1c             	sub    $0x1c,%esp
  800baf:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bb3:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bb7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800bbb:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800bbf:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800bc3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800bc7:	89 f3                	mov    %esi,%ebx
  800bc9:	89 fa                	mov    %edi,%edx
  800bcb:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800bcf:	89 34 24             	mov    %esi,(%esp)
  800bd2:	85 c0                	test   %eax,%eax
  800bd4:	75 1a                	jne    800bf0 <__umoddi3+0x48>
  800bd6:	39 f7                	cmp    %esi,%edi
  800bd8:	0f 86 a2 00 00 00    	jbe    800c80 <__umoddi3+0xd8>
  800bde:	89 c8                	mov    %ecx,%eax
  800be0:	89 f2                	mov    %esi,%edx
  800be2:	f7 f7                	div    %edi
  800be4:	89 d0                	mov    %edx,%eax
  800be6:	31 d2                	xor    %edx,%edx
  800be8:	83 c4 1c             	add    $0x1c,%esp
  800beb:	5b                   	pop    %ebx
  800bec:	5e                   	pop    %esi
  800bed:	5f                   	pop    %edi
  800bee:	5d                   	pop    %ebp
  800bef:	c3                   	ret    
  800bf0:	39 f0                	cmp    %esi,%eax
  800bf2:	0f 87 ac 00 00 00    	ja     800ca4 <__umoddi3+0xfc>
  800bf8:	0f bd e8             	bsr    %eax,%ebp
  800bfb:	83 f5 1f             	xor    $0x1f,%ebp
  800bfe:	0f 84 ac 00 00 00    	je     800cb0 <__umoddi3+0x108>
  800c04:	bf 20 00 00 00       	mov    $0x20,%edi
  800c09:	29 ef                	sub    %ebp,%edi
  800c0b:	89 fe                	mov    %edi,%esi
  800c0d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c11:	89 e9                	mov    %ebp,%ecx
  800c13:	d3 e0                	shl    %cl,%eax
  800c15:	89 d7                	mov    %edx,%edi
  800c17:	89 f1                	mov    %esi,%ecx
  800c19:	d3 ef                	shr    %cl,%edi
  800c1b:	09 c7                	or     %eax,%edi
  800c1d:	89 e9                	mov    %ebp,%ecx
  800c1f:	d3 e2                	shl    %cl,%edx
  800c21:	89 14 24             	mov    %edx,(%esp)
  800c24:	89 d8                	mov    %ebx,%eax
  800c26:	d3 e0                	shl    %cl,%eax
  800c28:	89 c2                	mov    %eax,%edx
  800c2a:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c2e:	d3 e0                	shl    %cl,%eax
  800c30:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c34:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c38:	89 f1                	mov    %esi,%ecx
  800c3a:	d3 e8                	shr    %cl,%eax
  800c3c:	09 d0                	or     %edx,%eax
  800c3e:	d3 eb                	shr    %cl,%ebx
  800c40:	89 da                	mov    %ebx,%edx
  800c42:	f7 f7                	div    %edi
  800c44:	89 d3                	mov    %edx,%ebx
  800c46:	f7 24 24             	mull   (%esp)
  800c49:	89 c6                	mov    %eax,%esi
  800c4b:	89 d1                	mov    %edx,%ecx
  800c4d:	39 d3                	cmp    %edx,%ebx
  800c4f:	0f 82 87 00 00 00    	jb     800cdc <__umoddi3+0x134>
  800c55:	0f 84 91 00 00 00    	je     800cec <__umoddi3+0x144>
  800c5b:	8b 54 24 04          	mov    0x4(%esp),%edx
  800c5f:	29 f2                	sub    %esi,%edx
  800c61:	19 cb                	sbb    %ecx,%ebx
  800c63:	89 d8                	mov    %ebx,%eax
  800c65:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800c69:	d3 e0                	shl    %cl,%eax
  800c6b:	89 e9                	mov    %ebp,%ecx
  800c6d:	d3 ea                	shr    %cl,%edx
  800c6f:	09 d0                	or     %edx,%eax
  800c71:	89 e9                	mov    %ebp,%ecx
  800c73:	d3 eb                	shr    %cl,%ebx
  800c75:	89 da                	mov    %ebx,%edx
  800c77:	83 c4 1c             	add    $0x1c,%esp
  800c7a:	5b                   	pop    %ebx
  800c7b:	5e                   	pop    %esi
  800c7c:	5f                   	pop    %edi
  800c7d:	5d                   	pop    %ebp
  800c7e:	c3                   	ret    
  800c7f:	90                   	nop
  800c80:	89 fd                	mov    %edi,%ebp
  800c82:	85 ff                	test   %edi,%edi
  800c84:	75 0b                	jne    800c91 <__umoddi3+0xe9>
  800c86:	b8 01 00 00 00       	mov    $0x1,%eax
  800c8b:	31 d2                	xor    %edx,%edx
  800c8d:	f7 f7                	div    %edi
  800c8f:	89 c5                	mov    %eax,%ebp
  800c91:	89 f0                	mov    %esi,%eax
  800c93:	31 d2                	xor    %edx,%edx
  800c95:	f7 f5                	div    %ebp
  800c97:	89 c8                	mov    %ecx,%eax
  800c99:	f7 f5                	div    %ebp
  800c9b:	89 d0                	mov    %edx,%eax
  800c9d:	e9 44 ff ff ff       	jmp    800be6 <__umoddi3+0x3e>
  800ca2:	66 90                	xchg   %ax,%ax
  800ca4:	89 c8                	mov    %ecx,%eax
  800ca6:	89 f2                	mov    %esi,%edx
  800ca8:	83 c4 1c             	add    $0x1c,%esp
  800cab:	5b                   	pop    %ebx
  800cac:	5e                   	pop    %esi
  800cad:	5f                   	pop    %edi
  800cae:	5d                   	pop    %ebp
  800caf:	c3                   	ret    
  800cb0:	3b 04 24             	cmp    (%esp),%eax
  800cb3:	72 06                	jb     800cbb <__umoddi3+0x113>
  800cb5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800cb9:	77 0f                	ja     800cca <__umoddi3+0x122>
  800cbb:	89 f2                	mov    %esi,%edx
  800cbd:	29 f9                	sub    %edi,%ecx
  800cbf:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800cc3:	89 14 24             	mov    %edx,(%esp)
  800cc6:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800cca:	8b 44 24 04          	mov    0x4(%esp),%eax
  800cce:	8b 14 24             	mov    (%esp),%edx
  800cd1:	83 c4 1c             	add    $0x1c,%esp
  800cd4:	5b                   	pop    %ebx
  800cd5:	5e                   	pop    %esi
  800cd6:	5f                   	pop    %edi
  800cd7:	5d                   	pop    %ebp
  800cd8:	c3                   	ret    
  800cd9:	8d 76 00             	lea    0x0(%esi),%esi
  800cdc:	2b 04 24             	sub    (%esp),%eax
  800cdf:	19 fa                	sbb    %edi,%edx
  800ce1:	89 d1                	mov    %edx,%ecx
  800ce3:	89 c6                	mov    %eax,%esi
  800ce5:	e9 71 ff ff ff       	jmp    800c5b <__umoddi3+0xb3>
  800cea:	66 90                	xchg   %ax,%ax
  800cec:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800cf0:	72 ea                	jb     800cdc <__umoddi3+0x134>
  800cf2:	89 d9                	mov    %ebx,%ecx
  800cf4:	e9 62 ff ff ff       	jmp    800c5b <__umoddi3+0xb3>
