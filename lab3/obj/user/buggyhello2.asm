
obj/user/buggyhello2:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 1d 00 00 00       	call   80004e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

const char *hello = "hello, world\n";

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	sys_cputs(hello, 1024*1024);
  800039:	68 00 00 10 00       	push   $0x100000
  80003e:	ff 35 00 10 80 00    	pushl  0x801000
  800044:	e8 62 00 00 00       	call   8000ab <sys_cputs>
}
  800049:	83 c4 10             	add    $0x10,%esp
  80004c:	c9                   	leave  
  80004d:	c3                   	ret    

0080004e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80004e:	55                   	push   %ebp
  80004f:	89 e5                	mov    %esp,%ebp
  800051:	56                   	push   %esi
  800052:	53                   	push   %ebx
  800053:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800056:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800059:	e8 cb 00 00 00       	call   800129 <sys_getenvid>
  80005e:	25 ff 03 00 00       	and    $0x3ff,%eax
  800063:	8d 14 00             	lea    (%eax,%eax,1),%edx
  800066:	01 d0                	add    %edx,%eax
  800068:	c1 e0 05             	shl    $0x5,%eax
  80006b:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800070:	a3 08 10 80 00       	mov    %eax,0x801008
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800075:	85 db                	test   %ebx,%ebx
  800077:	7e 07                	jle    800080 <libmain+0x32>
		binaryname = argv[0];
  800079:	8b 06                	mov    (%esi),%eax
  80007b:	a3 04 10 80 00       	mov    %eax,0x801004

	// call user main routine
	umain(argc, argv);
  800080:	83 ec 08             	sub    $0x8,%esp
  800083:	56                   	push   %esi
  800084:	53                   	push   %ebx
  800085:	e8 a9 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80008a:	e8 0a 00 00 00       	call   800099 <exit>
}
  80008f:	83 c4 10             	add    $0x10,%esp
  800092:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800095:	5b                   	pop    %ebx
  800096:	5e                   	pop    %esi
  800097:	5d                   	pop    %ebp
  800098:	c3                   	ret    

00800099 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800099:	55                   	push   %ebp
  80009a:	89 e5                	mov    %esp,%ebp
  80009c:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  80009f:	6a 00                	push   $0x0
  8000a1:	e8 42 00 00 00       	call   8000e8 <sys_env_destroy>
}
  8000a6:	83 c4 10             	add    $0x10,%esp
  8000a9:	c9                   	leave  
  8000aa:	c3                   	ret    

008000ab <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000ab:	55                   	push   %ebp
  8000ac:	89 e5                	mov    %esp,%ebp
  8000ae:	57                   	push   %edi
  8000af:	56                   	push   %esi
  8000b0:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000b1:	b8 00 00 00 00       	mov    $0x0,%eax
  8000b6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000b9:	8b 55 08             	mov    0x8(%ebp),%edx
  8000bc:	89 c3                	mov    %eax,%ebx
  8000be:	89 c7                	mov    %eax,%edi
  8000c0:	89 c6                	mov    %eax,%esi
  8000c2:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000c4:	5b                   	pop    %ebx
  8000c5:	5e                   	pop    %esi
  8000c6:	5f                   	pop    %edi
  8000c7:	5d                   	pop    %ebp
  8000c8:	c3                   	ret    

008000c9 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000c9:	55                   	push   %ebp
  8000ca:	89 e5                	mov    %esp,%ebp
  8000cc:	57                   	push   %edi
  8000cd:	56                   	push   %esi
  8000ce:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000cf:	ba 00 00 00 00       	mov    $0x0,%edx
  8000d4:	b8 01 00 00 00       	mov    $0x1,%eax
  8000d9:	89 d1                	mov    %edx,%ecx
  8000db:	89 d3                	mov    %edx,%ebx
  8000dd:	89 d7                	mov    %edx,%edi
  8000df:	89 d6                	mov    %edx,%esi
  8000e1:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000e3:	5b                   	pop    %ebx
  8000e4:	5e                   	pop    %esi
  8000e5:	5f                   	pop    %edi
  8000e6:	5d                   	pop    %ebp
  8000e7:	c3                   	ret    

008000e8 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000e8:	55                   	push   %ebp
  8000e9:	89 e5                	mov    %esp,%ebp
  8000eb:	57                   	push   %edi
  8000ec:	56                   	push   %esi
  8000ed:	53                   	push   %ebx
  8000ee:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000f1:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000f6:	b8 03 00 00 00       	mov    $0x3,%eax
  8000fb:	8b 55 08             	mov    0x8(%ebp),%edx
  8000fe:	89 cb                	mov    %ecx,%ebx
  800100:	89 cf                	mov    %ecx,%edi
  800102:	89 ce                	mov    %ecx,%esi
  800104:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800106:	85 c0                	test   %eax,%eax
  800108:	7e 17                	jle    800121 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  80010a:	83 ec 0c             	sub    $0xc,%esp
  80010d:	50                   	push   %eax
  80010e:	6a 03                	push   $0x3
  800110:	68 28 0d 80 00       	push   $0x800d28
  800115:	6a 23                	push   $0x23
  800117:	68 45 0d 80 00       	push   $0x800d45
  80011c:	e8 27 00 00 00       	call   800148 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800121:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800124:	5b                   	pop    %ebx
  800125:	5e                   	pop    %esi
  800126:	5f                   	pop    %edi
  800127:	5d                   	pop    %ebp
  800128:	c3                   	ret    

00800129 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800129:	55                   	push   %ebp
  80012a:	89 e5                	mov    %esp,%ebp
  80012c:	57                   	push   %edi
  80012d:	56                   	push   %esi
  80012e:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80012f:	ba 00 00 00 00       	mov    $0x0,%edx
  800134:	b8 02 00 00 00       	mov    $0x2,%eax
  800139:	89 d1                	mov    %edx,%ecx
  80013b:	89 d3                	mov    %edx,%ebx
  80013d:	89 d7                	mov    %edx,%edi
  80013f:	89 d6                	mov    %edx,%esi
  800141:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800143:	5b                   	pop    %ebx
  800144:	5e                   	pop    %esi
  800145:	5f                   	pop    %edi
  800146:	5d                   	pop    %ebp
  800147:	c3                   	ret    

00800148 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800148:	55                   	push   %ebp
  800149:	89 e5                	mov    %esp,%ebp
  80014b:	56                   	push   %esi
  80014c:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  80014d:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800150:	8b 35 04 10 80 00    	mov    0x801004,%esi
  800156:	e8 ce ff ff ff       	call   800129 <sys_getenvid>
  80015b:	83 ec 0c             	sub    $0xc,%esp
  80015e:	ff 75 0c             	pushl  0xc(%ebp)
  800161:	ff 75 08             	pushl  0x8(%ebp)
  800164:	56                   	push   %esi
  800165:	50                   	push   %eax
  800166:	68 54 0d 80 00       	push   $0x800d54
  80016b:	e8 b0 00 00 00       	call   800220 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800170:	83 c4 18             	add    $0x18,%esp
  800173:	53                   	push   %ebx
  800174:	ff 75 10             	pushl  0x10(%ebp)
  800177:	e8 53 00 00 00       	call   8001cf <vcprintf>
	cprintf("\n");
  80017c:	c7 04 24 1c 0d 80 00 	movl   $0x800d1c,(%esp)
  800183:	e8 98 00 00 00       	call   800220 <cprintf>
  800188:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  80018b:	cc                   	int3   
  80018c:	eb fd                	jmp    80018b <_panic+0x43>

0080018e <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  80018e:	55                   	push   %ebp
  80018f:	89 e5                	mov    %esp,%ebp
  800191:	53                   	push   %ebx
  800192:	83 ec 04             	sub    $0x4,%esp
  800195:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800198:	8b 13                	mov    (%ebx),%edx
  80019a:	8d 42 01             	lea    0x1(%edx),%eax
  80019d:	89 03                	mov    %eax,(%ebx)
  80019f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8001a2:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8001a6:	3d ff 00 00 00       	cmp    $0xff,%eax
  8001ab:	75 1a                	jne    8001c7 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8001ad:	83 ec 08             	sub    $0x8,%esp
  8001b0:	68 ff 00 00 00       	push   $0xff
  8001b5:	8d 43 08             	lea    0x8(%ebx),%eax
  8001b8:	50                   	push   %eax
  8001b9:	e8 ed fe ff ff       	call   8000ab <sys_cputs>
		b->idx = 0;
  8001be:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8001c4:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8001c7:	ff 43 04             	incl   0x4(%ebx)
}
  8001ca:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001cd:	c9                   	leave  
  8001ce:	c3                   	ret    

008001cf <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8001cf:	55                   	push   %ebp
  8001d0:	89 e5                	mov    %esp,%ebp
  8001d2:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001d8:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001df:	00 00 00 
	b.cnt = 0;
  8001e2:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001e9:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001ec:	ff 75 0c             	pushl  0xc(%ebp)
  8001ef:	ff 75 08             	pushl  0x8(%ebp)
  8001f2:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001f8:	50                   	push   %eax
  8001f9:	68 8e 01 80 00       	push   $0x80018e
  8001fe:	e8 51 01 00 00       	call   800354 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800203:	83 c4 08             	add    $0x8,%esp
  800206:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  80020c:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800212:	50                   	push   %eax
  800213:	e8 93 fe ff ff       	call   8000ab <sys_cputs>

	return b.cnt;
}
  800218:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80021e:	c9                   	leave  
  80021f:	c3                   	ret    

00800220 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800220:	55                   	push   %ebp
  800221:	89 e5                	mov    %esp,%ebp
  800223:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800226:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800229:	50                   	push   %eax
  80022a:	ff 75 08             	pushl  0x8(%ebp)
  80022d:	e8 9d ff ff ff       	call   8001cf <vcprintf>
	va_end(ap);

	return cnt;
}
  800232:	c9                   	leave  
  800233:	c3                   	ret    

00800234 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800234:	55                   	push   %ebp
  800235:	89 e5                	mov    %esp,%ebp
  800237:	57                   	push   %edi
  800238:	56                   	push   %esi
  800239:	53                   	push   %ebx
  80023a:	83 ec 1c             	sub    $0x1c,%esp
  80023d:	89 c7                	mov    %eax,%edi
  80023f:	89 d6                	mov    %edx,%esi
  800241:	8b 45 08             	mov    0x8(%ebp),%eax
  800244:	8b 55 0c             	mov    0xc(%ebp),%edx
  800247:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80024a:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80024d:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800250:	bb 00 00 00 00       	mov    $0x0,%ebx
  800255:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800258:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80025b:	39 d3                	cmp    %edx,%ebx
  80025d:	72 05                	jb     800264 <printnum+0x30>
  80025f:	39 45 10             	cmp    %eax,0x10(%ebp)
  800262:	77 45                	ja     8002a9 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800264:	83 ec 0c             	sub    $0xc,%esp
  800267:	ff 75 18             	pushl  0x18(%ebp)
  80026a:	8b 45 14             	mov    0x14(%ebp),%eax
  80026d:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800270:	53                   	push   %ebx
  800271:	ff 75 10             	pushl  0x10(%ebp)
  800274:	83 ec 08             	sub    $0x8,%esp
  800277:	ff 75 e4             	pushl  -0x1c(%ebp)
  80027a:	ff 75 e0             	pushl  -0x20(%ebp)
  80027d:	ff 75 dc             	pushl  -0x24(%ebp)
  800280:	ff 75 d8             	pushl  -0x28(%ebp)
  800283:	e8 24 08 00 00       	call   800aac <__udivdi3>
  800288:	83 c4 18             	add    $0x18,%esp
  80028b:	52                   	push   %edx
  80028c:	50                   	push   %eax
  80028d:	89 f2                	mov    %esi,%edx
  80028f:	89 f8                	mov    %edi,%eax
  800291:	e8 9e ff ff ff       	call   800234 <printnum>
  800296:	83 c4 20             	add    $0x20,%esp
  800299:	eb 16                	jmp    8002b1 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80029b:	83 ec 08             	sub    $0x8,%esp
  80029e:	56                   	push   %esi
  80029f:	ff 75 18             	pushl  0x18(%ebp)
  8002a2:	ff d7                	call   *%edi
  8002a4:	83 c4 10             	add    $0x10,%esp
  8002a7:	eb 03                	jmp    8002ac <printnum+0x78>
  8002a9:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8002ac:	4b                   	dec    %ebx
  8002ad:	85 db                	test   %ebx,%ebx
  8002af:	7f ea                	jg     80029b <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8002b1:	83 ec 08             	sub    $0x8,%esp
  8002b4:	56                   	push   %esi
  8002b5:	83 ec 04             	sub    $0x4,%esp
  8002b8:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002bb:	ff 75 e0             	pushl  -0x20(%ebp)
  8002be:	ff 75 dc             	pushl  -0x24(%ebp)
  8002c1:	ff 75 d8             	pushl  -0x28(%ebp)
  8002c4:	e8 f3 08 00 00       	call   800bbc <__umoddi3>
  8002c9:	83 c4 14             	add    $0x14,%esp
  8002cc:	0f be 80 78 0d 80 00 	movsbl 0x800d78(%eax),%eax
  8002d3:	50                   	push   %eax
  8002d4:	ff d7                	call   *%edi
}
  8002d6:	83 c4 10             	add    $0x10,%esp
  8002d9:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002dc:	5b                   	pop    %ebx
  8002dd:	5e                   	pop    %esi
  8002de:	5f                   	pop    %edi
  8002df:	5d                   	pop    %ebp
  8002e0:	c3                   	ret    

008002e1 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002e1:	55                   	push   %ebp
  8002e2:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002e4:	83 fa 01             	cmp    $0x1,%edx
  8002e7:	7e 0e                	jle    8002f7 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002e9:	8b 10                	mov    (%eax),%edx
  8002eb:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002ee:	89 08                	mov    %ecx,(%eax)
  8002f0:	8b 02                	mov    (%edx),%eax
  8002f2:	8b 52 04             	mov    0x4(%edx),%edx
  8002f5:	eb 22                	jmp    800319 <getuint+0x38>
	else if (lflag)
  8002f7:	85 d2                	test   %edx,%edx
  8002f9:	74 10                	je     80030b <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8002fb:	8b 10                	mov    (%eax),%edx
  8002fd:	8d 4a 04             	lea    0x4(%edx),%ecx
  800300:	89 08                	mov    %ecx,(%eax)
  800302:	8b 02                	mov    (%edx),%eax
  800304:	ba 00 00 00 00       	mov    $0x0,%edx
  800309:	eb 0e                	jmp    800319 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  80030b:	8b 10                	mov    (%eax),%edx
  80030d:	8d 4a 04             	lea    0x4(%edx),%ecx
  800310:	89 08                	mov    %ecx,(%eax)
  800312:	8b 02                	mov    (%edx),%eax
  800314:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800319:	5d                   	pop    %ebp
  80031a:	c3                   	ret    

0080031b <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80031b:	55                   	push   %ebp
  80031c:	89 e5                	mov    %esp,%ebp
  80031e:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800321:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800324:	8b 10                	mov    (%eax),%edx
  800326:	3b 50 04             	cmp    0x4(%eax),%edx
  800329:	73 0a                	jae    800335 <sprintputch+0x1a>
		*b->buf++ = ch;
  80032b:	8d 4a 01             	lea    0x1(%edx),%ecx
  80032e:	89 08                	mov    %ecx,(%eax)
  800330:	8b 45 08             	mov    0x8(%ebp),%eax
  800333:	88 02                	mov    %al,(%edx)
}
  800335:	5d                   	pop    %ebp
  800336:	c3                   	ret    

00800337 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800337:	55                   	push   %ebp
  800338:	89 e5                	mov    %esp,%ebp
  80033a:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80033d:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800340:	50                   	push   %eax
  800341:	ff 75 10             	pushl  0x10(%ebp)
  800344:	ff 75 0c             	pushl  0xc(%ebp)
  800347:	ff 75 08             	pushl  0x8(%ebp)
  80034a:	e8 05 00 00 00       	call   800354 <vprintfmt>
	va_end(ap);
}
  80034f:	83 c4 10             	add    $0x10,%esp
  800352:	c9                   	leave  
  800353:	c3                   	ret    

00800354 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800354:	55                   	push   %ebp
  800355:	89 e5                	mov    %esp,%ebp
  800357:	57                   	push   %edi
  800358:	56                   	push   %esi
  800359:	53                   	push   %ebx
  80035a:	83 ec 2c             	sub    $0x2c,%esp
  80035d:	8b 75 08             	mov    0x8(%ebp),%esi
  800360:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800363:	8b 7d 10             	mov    0x10(%ebp),%edi
  800366:	eb 12                	jmp    80037a <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800368:	85 c0                	test   %eax,%eax
  80036a:	0f 84 68 03 00 00    	je     8006d8 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800370:	83 ec 08             	sub    $0x8,%esp
  800373:	53                   	push   %ebx
  800374:	50                   	push   %eax
  800375:	ff d6                	call   *%esi
  800377:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80037a:	47                   	inc    %edi
  80037b:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80037f:	83 f8 25             	cmp    $0x25,%eax
  800382:	75 e4                	jne    800368 <vprintfmt+0x14>
  800384:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800388:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  80038f:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800396:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  80039d:	ba 00 00 00 00       	mov    $0x0,%edx
  8003a2:	eb 07                	jmp    8003ab <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003a4:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8003a7:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ab:	8d 47 01             	lea    0x1(%edi),%eax
  8003ae:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8003b1:	0f b6 0f             	movzbl (%edi),%ecx
  8003b4:	8a 07                	mov    (%edi),%al
  8003b6:	83 e8 23             	sub    $0x23,%eax
  8003b9:	3c 55                	cmp    $0x55,%al
  8003bb:	0f 87 fe 02 00 00    	ja     8006bf <vprintfmt+0x36b>
  8003c1:	0f b6 c0             	movzbl %al,%eax
  8003c4:	ff 24 85 08 0e 80 00 	jmp    *0x800e08(,%eax,4)
  8003cb:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003ce:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003d2:	eb d7                	jmp    8003ab <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003d4:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003d7:	b8 00 00 00 00       	mov    $0x0,%eax
  8003dc:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003df:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003e2:	01 c0                	add    %eax,%eax
  8003e4:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003e8:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003eb:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003ee:	83 fa 09             	cmp    $0x9,%edx
  8003f1:	77 34                	ja     800427 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003f3:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003f4:	eb e9                	jmp    8003df <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003f6:	8b 45 14             	mov    0x14(%ebp),%eax
  8003f9:	8d 48 04             	lea    0x4(%eax),%ecx
  8003fc:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8003ff:	8b 00                	mov    (%eax),%eax
  800401:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800404:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800407:	eb 24                	jmp    80042d <vprintfmt+0xd9>
  800409:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80040d:	79 07                	jns    800416 <vprintfmt+0xc2>
  80040f:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800416:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800419:	eb 90                	jmp    8003ab <vprintfmt+0x57>
  80041b:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80041e:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800425:	eb 84                	jmp    8003ab <vprintfmt+0x57>
  800427:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80042a:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80042d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800431:	0f 89 74 ff ff ff    	jns    8003ab <vprintfmt+0x57>
				width = precision, precision = -1;
  800437:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80043a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80043d:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800444:	e9 62 ff ff ff       	jmp    8003ab <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800449:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80044a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80044d:	e9 59 ff ff ff       	jmp    8003ab <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800452:	8b 45 14             	mov    0x14(%ebp),%eax
  800455:	8d 50 04             	lea    0x4(%eax),%edx
  800458:	89 55 14             	mov    %edx,0x14(%ebp)
  80045b:	83 ec 08             	sub    $0x8,%esp
  80045e:	53                   	push   %ebx
  80045f:	ff 30                	pushl  (%eax)
  800461:	ff d6                	call   *%esi
			break;
  800463:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800466:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800469:	e9 0c ff ff ff       	jmp    80037a <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80046e:	8b 45 14             	mov    0x14(%ebp),%eax
  800471:	8d 50 04             	lea    0x4(%eax),%edx
  800474:	89 55 14             	mov    %edx,0x14(%ebp)
  800477:	8b 00                	mov    (%eax),%eax
  800479:	85 c0                	test   %eax,%eax
  80047b:	79 02                	jns    80047f <vprintfmt+0x12b>
  80047d:	f7 d8                	neg    %eax
  80047f:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800481:	83 f8 06             	cmp    $0x6,%eax
  800484:	7f 0b                	jg     800491 <vprintfmt+0x13d>
  800486:	8b 04 85 60 0f 80 00 	mov    0x800f60(,%eax,4),%eax
  80048d:	85 c0                	test   %eax,%eax
  80048f:	75 18                	jne    8004a9 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800491:	52                   	push   %edx
  800492:	68 90 0d 80 00       	push   $0x800d90
  800497:	53                   	push   %ebx
  800498:	56                   	push   %esi
  800499:	e8 99 fe ff ff       	call   800337 <printfmt>
  80049e:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004a1:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8004a4:	e9 d1 fe ff ff       	jmp    80037a <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8004a9:	50                   	push   %eax
  8004aa:	68 99 0d 80 00       	push   $0x800d99
  8004af:	53                   	push   %ebx
  8004b0:	56                   	push   %esi
  8004b1:	e8 81 fe ff ff       	call   800337 <printfmt>
  8004b6:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004b9:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004bc:	e9 b9 fe ff ff       	jmp    80037a <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004c1:	8b 45 14             	mov    0x14(%ebp),%eax
  8004c4:	8d 50 04             	lea    0x4(%eax),%edx
  8004c7:	89 55 14             	mov    %edx,0x14(%ebp)
  8004ca:	8b 38                	mov    (%eax),%edi
  8004cc:	85 ff                	test   %edi,%edi
  8004ce:	75 05                	jne    8004d5 <vprintfmt+0x181>
				p = "(null)";
  8004d0:	bf 89 0d 80 00       	mov    $0x800d89,%edi
			if (width > 0 && padc != '-')
  8004d5:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004d9:	0f 8e 90 00 00 00    	jle    80056f <vprintfmt+0x21b>
  8004df:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004e3:	0f 84 8e 00 00 00    	je     800577 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004e9:	83 ec 08             	sub    $0x8,%esp
  8004ec:	ff 75 d0             	pushl  -0x30(%ebp)
  8004ef:	57                   	push   %edi
  8004f0:	e8 70 02 00 00       	call   800765 <strnlen>
  8004f5:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8004f8:	29 c1                	sub    %eax,%ecx
  8004fa:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8004fd:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  800500:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800504:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800507:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  80050a:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80050c:	eb 0d                	jmp    80051b <vprintfmt+0x1c7>
					putch(padc, putdat);
  80050e:	83 ec 08             	sub    $0x8,%esp
  800511:	53                   	push   %ebx
  800512:	ff 75 e4             	pushl  -0x1c(%ebp)
  800515:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800517:	4f                   	dec    %edi
  800518:	83 c4 10             	add    $0x10,%esp
  80051b:	85 ff                	test   %edi,%edi
  80051d:	7f ef                	jg     80050e <vprintfmt+0x1ba>
  80051f:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800522:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800525:	89 c8                	mov    %ecx,%eax
  800527:	85 c9                	test   %ecx,%ecx
  800529:	79 05                	jns    800530 <vprintfmt+0x1dc>
  80052b:	b8 00 00 00 00       	mov    $0x0,%eax
  800530:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800533:	29 c1                	sub    %eax,%ecx
  800535:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800538:	89 75 08             	mov    %esi,0x8(%ebp)
  80053b:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80053e:	eb 3d                	jmp    80057d <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800540:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800544:	74 19                	je     80055f <vprintfmt+0x20b>
  800546:	0f be c0             	movsbl %al,%eax
  800549:	83 e8 20             	sub    $0x20,%eax
  80054c:	83 f8 5e             	cmp    $0x5e,%eax
  80054f:	76 0e                	jbe    80055f <vprintfmt+0x20b>
					putch('?', putdat);
  800551:	83 ec 08             	sub    $0x8,%esp
  800554:	53                   	push   %ebx
  800555:	6a 3f                	push   $0x3f
  800557:	ff 55 08             	call   *0x8(%ebp)
  80055a:	83 c4 10             	add    $0x10,%esp
  80055d:	eb 0b                	jmp    80056a <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80055f:	83 ec 08             	sub    $0x8,%esp
  800562:	53                   	push   %ebx
  800563:	52                   	push   %edx
  800564:	ff 55 08             	call   *0x8(%ebp)
  800567:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80056a:	ff 4d e4             	decl   -0x1c(%ebp)
  80056d:	eb 0e                	jmp    80057d <vprintfmt+0x229>
  80056f:	89 75 08             	mov    %esi,0x8(%ebp)
  800572:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800575:	eb 06                	jmp    80057d <vprintfmt+0x229>
  800577:	89 75 08             	mov    %esi,0x8(%ebp)
  80057a:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80057d:	47                   	inc    %edi
  80057e:	8a 47 ff             	mov    -0x1(%edi),%al
  800581:	0f be d0             	movsbl %al,%edx
  800584:	85 d2                	test   %edx,%edx
  800586:	74 1d                	je     8005a5 <vprintfmt+0x251>
  800588:	85 f6                	test   %esi,%esi
  80058a:	78 b4                	js     800540 <vprintfmt+0x1ec>
  80058c:	4e                   	dec    %esi
  80058d:	79 b1                	jns    800540 <vprintfmt+0x1ec>
  80058f:	8b 75 08             	mov    0x8(%ebp),%esi
  800592:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800595:	eb 14                	jmp    8005ab <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800597:	83 ec 08             	sub    $0x8,%esp
  80059a:	53                   	push   %ebx
  80059b:	6a 20                	push   $0x20
  80059d:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80059f:	4f                   	dec    %edi
  8005a0:	83 c4 10             	add    $0x10,%esp
  8005a3:	eb 06                	jmp    8005ab <vprintfmt+0x257>
  8005a5:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8005a8:	8b 75 08             	mov    0x8(%ebp),%esi
  8005ab:	85 ff                	test   %edi,%edi
  8005ad:	7f e8                	jg     800597 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005af:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005b2:	e9 c3 fd ff ff       	jmp    80037a <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8005b7:	83 fa 01             	cmp    $0x1,%edx
  8005ba:	7e 16                	jle    8005d2 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8005bc:	8b 45 14             	mov    0x14(%ebp),%eax
  8005bf:	8d 50 08             	lea    0x8(%eax),%edx
  8005c2:	89 55 14             	mov    %edx,0x14(%ebp)
  8005c5:	8b 50 04             	mov    0x4(%eax),%edx
  8005c8:	8b 00                	mov    (%eax),%eax
  8005ca:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005cd:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8005d0:	eb 32                	jmp    800604 <vprintfmt+0x2b0>
	else if (lflag)
  8005d2:	85 d2                	test   %edx,%edx
  8005d4:	74 18                	je     8005ee <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005d6:	8b 45 14             	mov    0x14(%ebp),%eax
  8005d9:	8d 50 04             	lea    0x4(%eax),%edx
  8005dc:	89 55 14             	mov    %edx,0x14(%ebp)
  8005df:	8b 00                	mov    (%eax),%eax
  8005e1:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005e4:	89 c1                	mov    %eax,%ecx
  8005e6:	c1 f9 1f             	sar    $0x1f,%ecx
  8005e9:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005ec:	eb 16                	jmp    800604 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005ee:	8b 45 14             	mov    0x14(%ebp),%eax
  8005f1:	8d 50 04             	lea    0x4(%eax),%edx
  8005f4:	89 55 14             	mov    %edx,0x14(%ebp)
  8005f7:	8b 00                	mov    (%eax),%eax
  8005f9:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005fc:	89 c1                	mov    %eax,%ecx
  8005fe:	c1 f9 1f             	sar    $0x1f,%ecx
  800601:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800604:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800607:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  80060a:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80060e:	79 76                	jns    800686 <vprintfmt+0x332>
				putch('-', putdat);
  800610:	83 ec 08             	sub    $0x8,%esp
  800613:	53                   	push   %ebx
  800614:	6a 2d                	push   $0x2d
  800616:	ff d6                	call   *%esi
				num = -(long long) num;
  800618:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80061b:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80061e:	f7 d8                	neg    %eax
  800620:	83 d2 00             	adc    $0x0,%edx
  800623:	f7 da                	neg    %edx
  800625:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800628:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80062d:	eb 5c                	jmp    80068b <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80062f:	8d 45 14             	lea    0x14(%ebp),%eax
  800632:	e8 aa fc ff ff       	call   8002e1 <getuint>
			base = 10;
  800637:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  80063c:	eb 4d                	jmp    80068b <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80063e:	8d 45 14             	lea    0x14(%ebp),%eax
  800641:	e8 9b fc ff ff       	call   8002e1 <getuint>
			base = 8;
  800646:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80064b:	eb 3e                	jmp    80068b <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80064d:	83 ec 08             	sub    $0x8,%esp
  800650:	53                   	push   %ebx
  800651:	6a 30                	push   $0x30
  800653:	ff d6                	call   *%esi
			putch('x', putdat);
  800655:	83 c4 08             	add    $0x8,%esp
  800658:	53                   	push   %ebx
  800659:	6a 78                	push   $0x78
  80065b:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80065d:	8b 45 14             	mov    0x14(%ebp),%eax
  800660:	8d 50 04             	lea    0x4(%eax),%edx
  800663:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800666:	8b 00                	mov    (%eax),%eax
  800668:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80066d:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800670:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800675:	eb 14                	jmp    80068b <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800677:	8d 45 14             	lea    0x14(%ebp),%eax
  80067a:	e8 62 fc ff ff       	call   8002e1 <getuint>
			base = 16;
  80067f:	b9 10 00 00 00       	mov    $0x10,%ecx
  800684:	eb 05                	jmp    80068b <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  800686:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80068b:	83 ec 0c             	sub    $0xc,%esp
  80068e:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800692:	57                   	push   %edi
  800693:	ff 75 e4             	pushl  -0x1c(%ebp)
  800696:	51                   	push   %ecx
  800697:	52                   	push   %edx
  800698:	50                   	push   %eax
  800699:	89 da                	mov    %ebx,%edx
  80069b:	89 f0                	mov    %esi,%eax
  80069d:	e8 92 fb ff ff       	call   800234 <printnum>
			break;
  8006a2:	83 c4 20             	add    $0x20,%esp
  8006a5:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006a8:	e9 cd fc ff ff       	jmp    80037a <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8006ad:	83 ec 08             	sub    $0x8,%esp
  8006b0:	53                   	push   %ebx
  8006b1:	51                   	push   %ecx
  8006b2:	ff d6                	call   *%esi
			break;
  8006b4:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006b7:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8006ba:	e9 bb fc ff ff       	jmp    80037a <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006bf:	83 ec 08             	sub    $0x8,%esp
  8006c2:	53                   	push   %ebx
  8006c3:	6a 25                	push   $0x25
  8006c5:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006c7:	83 c4 10             	add    $0x10,%esp
  8006ca:	eb 01                	jmp    8006cd <vprintfmt+0x379>
  8006cc:	4f                   	dec    %edi
  8006cd:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006d1:	75 f9                	jne    8006cc <vprintfmt+0x378>
  8006d3:	e9 a2 fc ff ff       	jmp    80037a <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006d8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006db:	5b                   	pop    %ebx
  8006dc:	5e                   	pop    %esi
  8006dd:	5f                   	pop    %edi
  8006de:	5d                   	pop    %ebp
  8006df:	c3                   	ret    

008006e0 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006e0:	55                   	push   %ebp
  8006e1:	89 e5                	mov    %esp,%ebp
  8006e3:	83 ec 18             	sub    $0x18,%esp
  8006e6:	8b 45 08             	mov    0x8(%ebp),%eax
  8006e9:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006ec:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006ef:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006f3:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8006f6:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8006fd:	85 c0                	test   %eax,%eax
  8006ff:	74 26                	je     800727 <vsnprintf+0x47>
  800701:	85 d2                	test   %edx,%edx
  800703:	7e 29                	jle    80072e <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800705:	ff 75 14             	pushl  0x14(%ebp)
  800708:	ff 75 10             	pushl  0x10(%ebp)
  80070b:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80070e:	50                   	push   %eax
  80070f:	68 1b 03 80 00       	push   $0x80031b
  800714:	e8 3b fc ff ff       	call   800354 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800719:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80071c:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80071f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800722:	83 c4 10             	add    $0x10,%esp
  800725:	eb 0c                	jmp    800733 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800727:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  80072c:	eb 05                	jmp    800733 <vsnprintf+0x53>
  80072e:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800733:	c9                   	leave  
  800734:	c3                   	ret    

00800735 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800735:	55                   	push   %ebp
  800736:	89 e5                	mov    %esp,%ebp
  800738:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80073b:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80073e:	50                   	push   %eax
  80073f:	ff 75 10             	pushl  0x10(%ebp)
  800742:	ff 75 0c             	pushl  0xc(%ebp)
  800745:	ff 75 08             	pushl  0x8(%ebp)
  800748:	e8 93 ff ff ff       	call   8006e0 <vsnprintf>
	va_end(ap);

	return rc;
}
  80074d:	c9                   	leave  
  80074e:	c3                   	ret    

0080074f <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80074f:	55                   	push   %ebp
  800750:	89 e5                	mov    %esp,%ebp
  800752:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800755:	b8 00 00 00 00       	mov    $0x0,%eax
  80075a:	eb 01                	jmp    80075d <strlen+0xe>
		n++;
  80075c:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80075d:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800761:	75 f9                	jne    80075c <strlen+0xd>
		n++;
	return n;
}
  800763:	5d                   	pop    %ebp
  800764:	c3                   	ret    

00800765 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800765:	55                   	push   %ebp
  800766:	89 e5                	mov    %esp,%ebp
  800768:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80076b:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80076e:	ba 00 00 00 00       	mov    $0x0,%edx
  800773:	eb 01                	jmp    800776 <strnlen+0x11>
		n++;
  800775:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800776:	39 c2                	cmp    %eax,%edx
  800778:	74 08                	je     800782 <strnlen+0x1d>
  80077a:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80077e:	75 f5                	jne    800775 <strnlen+0x10>
  800780:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800782:	5d                   	pop    %ebp
  800783:	c3                   	ret    

00800784 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800784:	55                   	push   %ebp
  800785:	89 e5                	mov    %esp,%ebp
  800787:	53                   	push   %ebx
  800788:	8b 45 08             	mov    0x8(%ebp),%eax
  80078b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  80078e:	89 c2                	mov    %eax,%edx
  800790:	42                   	inc    %edx
  800791:	41                   	inc    %ecx
  800792:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800795:	88 5a ff             	mov    %bl,-0x1(%edx)
  800798:	84 db                	test   %bl,%bl
  80079a:	75 f4                	jne    800790 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  80079c:	5b                   	pop    %ebx
  80079d:	5d                   	pop    %ebp
  80079e:	c3                   	ret    

0080079f <strcat>:

char *
strcat(char *dst, const char *src)
{
  80079f:	55                   	push   %ebp
  8007a0:	89 e5                	mov    %esp,%ebp
  8007a2:	53                   	push   %ebx
  8007a3:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8007a6:	53                   	push   %ebx
  8007a7:	e8 a3 ff ff ff       	call   80074f <strlen>
  8007ac:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8007af:	ff 75 0c             	pushl  0xc(%ebp)
  8007b2:	01 d8                	add    %ebx,%eax
  8007b4:	50                   	push   %eax
  8007b5:	e8 ca ff ff ff       	call   800784 <strcpy>
	return dst;
}
  8007ba:	89 d8                	mov    %ebx,%eax
  8007bc:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8007bf:	c9                   	leave  
  8007c0:	c3                   	ret    

008007c1 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8007c1:	55                   	push   %ebp
  8007c2:	89 e5                	mov    %esp,%ebp
  8007c4:	56                   	push   %esi
  8007c5:	53                   	push   %ebx
  8007c6:	8b 75 08             	mov    0x8(%ebp),%esi
  8007c9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007cc:	89 f3                	mov    %esi,%ebx
  8007ce:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007d1:	89 f2                	mov    %esi,%edx
  8007d3:	eb 0c                	jmp    8007e1 <strncpy+0x20>
		*dst++ = *src;
  8007d5:	42                   	inc    %edx
  8007d6:	8a 01                	mov    (%ecx),%al
  8007d8:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007db:	80 39 01             	cmpb   $0x1,(%ecx)
  8007de:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007e1:	39 da                	cmp    %ebx,%edx
  8007e3:	75 f0                	jne    8007d5 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007e5:	89 f0                	mov    %esi,%eax
  8007e7:	5b                   	pop    %ebx
  8007e8:	5e                   	pop    %esi
  8007e9:	5d                   	pop    %ebp
  8007ea:	c3                   	ret    

008007eb <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007eb:	55                   	push   %ebp
  8007ec:	89 e5                	mov    %esp,%ebp
  8007ee:	56                   	push   %esi
  8007ef:	53                   	push   %ebx
  8007f0:	8b 75 08             	mov    0x8(%ebp),%esi
  8007f3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007f6:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8007f9:	85 c0                	test   %eax,%eax
  8007fb:	74 1e                	je     80081b <strlcpy+0x30>
  8007fd:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  800801:	89 f2                	mov    %esi,%edx
  800803:	eb 05                	jmp    80080a <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800805:	42                   	inc    %edx
  800806:	41                   	inc    %ecx
  800807:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  80080a:	39 c2                	cmp    %eax,%edx
  80080c:	74 08                	je     800816 <strlcpy+0x2b>
  80080e:	8a 19                	mov    (%ecx),%bl
  800810:	84 db                	test   %bl,%bl
  800812:	75 f1                	jne    800805 <strlcpy+0x1a>
  800814:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800816:	c6 00 00             	movb   $0x0,(%eax)
  800819:	eb 02                	jmp    80081d <strlcpy+0x32>
  80081b:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  80081d:	29 f0                	sub    %esi,%eax
}
  80081f:	5b                   	pop    %ebx
  800820:	5e                   	pop    %esi
  800821:	5d                   	pop    %ebp
  800822:	c3                   	ret    

00800823 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800823:	55                   	push   %ebp
  800824:	89 e5                	mov    %esp,%ebp
  800826:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800829:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  80082c:	eb 02                	jmp    800830 <strcmp+0xd>
		p++, q++;
  80082e:	41                   	inc    %ecx
  80082f:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800830:	8a 01                	mov    (%ecx),%al
  800832:	84 c0                	test   %al,%al
  800834:	74 04                	je     80083a <strcmp+0x17>
  800836:	3a 02                	cmp    (%edx),%al
  800838:	74 f4                	je     80082e <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80083a:	0f b6 c0             	movzbl %al,%eax
  80083d:	0f b6 12             	movzbl (%edx),%edx
  800840:	29 d0                	sub    %edx,%eax
}
  800842:	5d                   	pop    %ebp
  800843:	c3                   	ret    

00800844 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800844:	55                   	push   %ebp
  800845:	89 e5                	mov    %esp,%ebp
  800847:	53                   	push   %ebx
  800848:	8b 45 08             	mov    0x8(%ebp),%eax
  80084b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80084e:	89 c3                	mov    %eax,%ebx
  800850:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800853:	eb 02                	jmp    800857 <strncmp+0x13>
		n--, p++, q++;
  800855:	40                   	inc    %eax
  800856:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800857:	39 d8                	cmp    %ebx,%eax
  800859:	74 14                	je     80086f <strncmp+0x2b>
  80085b:	8a 08                	mov    (%eax),%cl
  80085d:	84 c9                	test   %cl,%cl
  80085f:	74 04                	je     800865 <strncmp+0x21>
  800861:	3a 0a                	cmp    (%edx),%cl
  800863:	74 f0                	je     800855 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800865:	0f b6 00             	movzbl (%eax),%eax
  800868:	0f b6 12             	movzbl (%edx),%edx
  80086b:	29 d0                	sub    %edx,%eax
  80086d:	eb 05                	jmp    800874 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80086f:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800874:	5b                   	pop    %ebx
  800875:	5d                   	pop    %ebp
  800876:	c3                   	ret    

00800877 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800877:	55                   	push   %ebp
  800878:	89 e5                	mov    %esp,%ebp
  80087a:	8b 45 08             	mov    0x8(%ebp),%eax
  80087d:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800880:	eb 05                	jmp    800887 <strchr+0x10>
		if (*s == c)
  800882:	38 ca                	cmp    %cl,%dl
  800884:	74 0c                	je     800892 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800886:	40                   	inc    %eax
  800887:	8a 10                	mov    (%eax),%dl
  800889:	84 d2                	test   %dl,%dl
  80088b:	75 f5                	jne    800882 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  80088d:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800892:	5d                   	pop    %ebp
  800893:	c3                   	ret    

00800894 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800894:	55                   	push   %ebp
  800895:	89 e5                	mov    %esp,%ebp
  800897:	8b 45 08             	mov    0x8(%ebp),%eax
  80089a:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80089d:	eb 05                	jmp    8008a4 <strfind+0x10>
		if (*s == c)
  80089f:	38 ca                	cmp    %cl,%dl
  8008a1:	74 07                	je     8008aa <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8008a3:	40                   	inc    %eax
  8008a4:	8a 10                	mov    (%eax),%dl
  8008a6:	84 d2                	test   %dl,%dl
  8008a8:	75 f5                	jne    80089f <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8008aa:	5d                   	pop    %ebp
  8008ab:	c3                   	ret    

008008ac <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8008ac:	55                   	push   %ebp
  8008ad:	89 e5                	mov    %esp,%ebp
  8008af:	57                   	push   %edi
  8008b0:	56                   	push   %esi
  8008b1:	53                   	push   %ebx
  8008b2:	8b 7d 08             	mov    0x8(%ebp),%edi
  8008b5:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8008b8:	85 c9                	test   %ecx,%ecx
  8008ba:	74 36                	je     8008f2 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8008bc:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8008c2:	75 28                	jne    8008ec <memset+0x40>
  8008c4:	f6 c1 03             	test   $0x3,%cl
  8008c7:	75 23                	jne    8008ec <memset+0x40>
		c &= 0xFF;
  8008c9:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8008cd:	89 d3                	mov    %edx,%ebx
  8008cf:	c1 e3 08             	shl    $0x8,%ebx
  8008d2:	89 d6                	mov    %edx,%esi
  8008d4:	c1 e6 18             	shl    $0x18,%esi
  8008d7:	89 d0                	mov    %edx,%eax
  8008d9:	c1 e0 10             	shl    $0x10,%eax
  8008dc:	09 f0                	or     %esi,%eax
  8008de:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008e0:	89 d8                	mov    %ebx,%eax
  8008e2:	09 d0                	or     %edx,%eax
  8008e4:	c1 e9 02             	shr    $0x2,%ecx
  8008e7:	fc                   	cld    
  8008e8:	f3 ab                	rep stos %eax,%es:(%edi)
  8008ea:	eb 06                	jmp    8008f2 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008ec:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008ef:	fc                   	cld    
  8008f0:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008f2:	89 f8                	mov    %edi,%eax
  8008f4:	5b                   	pop    %ebx
  8008f5:	5e                   	pop    %esi
  8008f6:	5f                   	pop    %edi
  8008f7:	5d                   	pop    %ebp
  8008f8:	c3                   	ret    

008008f9 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8008f9:	55                   	push   %ebp
  8008fa:	89 e5                	mov    %esp,%ebp
  8008fc:	57                   	push   %edi
  8008fd:	56                   	push   %esi
  8008fe:	8b 45 08             	mov    0x8(%ebp),%eax
  800901:	8b 75 0c             	mov    0xc(%ebp),%esi
  800904:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800907:	39 c6                	cmp    %eax,%esi
  800909:	73 33                	jae    80093e <memmove+0x45>
  80090b:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80090e:	39 d0                	cmp    %edx,%eax
  800910:	73 2c                	jae    80093e <memmove+0x45>
		s += n;
		d += n;
  800912:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800915:	89 d6                	mov    %edx,%esi
  800917:	09 fe                	or     %edi,%esi
  800919:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80091f:	75 13                	jne    800934 <memmove+0x3b>
  800921:	f6 c1 03             	test   $0x3,%cl
  800924:	75 0e                	jne    800934 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800926:	83 ef 04             	sub    $0x4,%edi
  800929:	8d 72 fc             	lea    -0x4(%edx),%esi
  80092c:	c1 e9 02             	shr    $0x2,%ecx
  80092f:	fd                   	std    
  800930:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800932:	eb 07                	jmp    80093b <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800934:	4f                   	dec    %edi
  800935:	8d 72 ff             	lea    -0x1(%edx),%esi
  800938:	fd                   	std    
  800939:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80093b:	fc                   	cld    
  80093c:	eb 1d                	jmp    80095b <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80093e:	89 f2                	mov    %esi,%edx
  800940:	09 c2                	or     %eax,%edx
  800942:	f6 c2 03             	test   $0x3,%dl
  800945:	75 0f                	jne    800956 <memmove+0x5d>
  800947:	f6 c1 03             	test   $0x3,%cl
  80094a:	75 0a                	jne    800956 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  80094c:	c1 e9 02             	shr    $0x2,%ecx
  80094f:	89 c7                	mov    %eax,%edi
  800951:	fc                   	cld    
  800952:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800954:	eb 05                	jmp    80095b <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800956:	89 c7                	mov    %eax,%edi
  800958:	fc                   	cld    
  800959:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  80095b:	5e                   	pop    %esi
  80095c:	5f                   	pop    %edi
  80095d:	5d                   	pop    %ebp
  80095e:	c3                   	ret    

0080095f <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80095f:	55                   	push   %ebp
  800960:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800962:	ff 75 10             	pushl  0x10(%ebp)
  800965:	ff 75 0c             	pushl  0xc(%ebp)
  800968:	ff 75 08             	pushl  0x8(%ebp)
  80096b:	e8 89 ff ff ff       	call   8008f9 <memmove>
}
  800970:	c9                   	leave  
  800971:	c3                   	ret    

00800972 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800972:	55                   	push   %ebp
  800973:	89 e5                	mov    %esp,%ebp
  800975:	56                   	push   %esi
  800976:	53                   	push   %ebx
  800977:	8b 45 08             	mov    0x8(%ebp),%eax
  80097a:	8b 55 0c             	mov    0xc(%ebp),%edx
  80097d:	89 c6                	mov    %eax,%esi
  80097f:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800982:	eb 14                	jmp    800998 <memcmp+0x26>
		if (*s1 != *s2)
  800984:	8a 08                	mov    (%eax),%cl
  800986:	8a 1a                	mov    (%edx),%bl
  800988:	38 d9                	cmp    %bl,%cl
  80098a:	74 0a                	je     800996 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  80098c:	0f b6 c1             	movzbl %cl,%eax
  80098f:	0f b6 db             	movzbl %bl,%ebx
  800992:	29 d8                	sub    %ebx,%eax
  800994:	eb 0b                	jmp    8009a1 <memcmp+0x2f>
		s1++, s2++;
  800996:	40                   	inc    %eax
  800997:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800998:	39 f0                	cmp    %esi,%eax
  80099a:	75 e8                	jne    800984 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  80099c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8009a1:	5b                   	pop    %ebx
  8009a2:	5e                   	pop    %esi
  8009a3:	5d                   	pop    %ebp
  8009a4:	c3                   	ret    

008009a5 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8009a5:	55                   	push   %ebp
  8009a6:	89 e5                	mov    %esp,%ebp
  8009a8:	53                   	push   %ebx
  8009a9:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8009ac:	89 c1                	mov    %eax,%ecx
  8009ae:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8009b1:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009b5:	eb 08                	jmp    8009bf <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8009b7:	0f b6 10             	movzbl (%eax),%edx
  8009ba:	39 da                	cmp    %ebx,%edx
  8009bc:	74 05                	je     8009c3 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009be:	40                   	inc    %eax
  8009bf:	39 c8                	cmp    %ecx,%eax
  8009c1:	72 f4                	jb     8009b7 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8009c3:	5b                   	pop    %ebx
  8009c4:	5d                   	pop    %ebp
  8009c5:	c3                   	ret    

008009c6 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8009c6:	55                   	push   %ebp
  8009c7:	89 e5                	mov    %esp,%ebp
  8009c9:	57                   	push   %edi
  8009ca:	56                   	push   %esi
  8009cb:	53                   	push   %ebx
  8009cc:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009cf:	eb 01                	jmp    8009d2 <strtol+0xc>
		s++;
  8009d1:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009d2:	8a 01                	mov    (%ecx),%al
  8009d4:	3c 20                	cmp    $0x20,%al
  8009d6:	74 f9                	je     8009d1 <strtol+0xb>
  8009d8:	3c 09                	cmp    $0x9,%al
  8009da:	74 f5                	je     8009d1 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009dc:	3c 2b                	cmp    $0x2b,%al
  8009de:	75 08                	jne    8009e8 <strtol+0x22>
		s++;
  8009e0:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009e1:	bf 00 00 00 00       	mov    $0x0,%edi
  8009e6:	eb 11                	jmp    8009f9 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009e8:	3c 2d                	cmp    $0x2d,%al
  8009ea:	75 08                	jne    8009f4 <strtol+0x2e>
		s++, neg = 1;
  8009ec:	41                   	inc    %ecx
  8009ed:	bf 01 00 00 00       	mov    $0x1,%edi
  8009f2:	eb 05                	jmp    8009f9 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009f4:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009f9:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009fd:	0f 84 87 00 00 00    	je     800a8a <strtol+0xc4>
  800a03:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800a07:	75 27                	jne    800a30 <strtol+0x6a>
  800a09:	80 39 30             	cmpb   $0x30,(%ecx)
  800a0c:	75 22                	jne    800a30 <strtol+0x6a>
  800a0e:	e9 88 00 00 00       	jmp    800a9b <strtol+0xd5>
		s += 2, base = 16;
  800a13:	83 c1 02             	add    $0x2,%ecx
  800a16:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a1d:	eb 11                	jmp    800a30 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a1f:	41                   	inc    %ecx
  800a20:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a27:	eb 07                	jmp    800a30 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a29:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a30:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a35:	8a 11                	mov    (%ecx),%dl
  800a37:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a3a:	80 fb 09             	cmp    $0x9,%bl
  800a3d:	77 08                	ja     800a47 <strtol+0x81>
			dig = *s - '0';
  800a3f:	0f be d2             	movsbl %dl,%edx
  800a42:	83 ea 30             	sub    $0x30,%edx
  800a45:	eb 22                	jmp    800a69 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a47:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a4a:	89 f3                	mov    %esi,%ebx
  800a4c:	80 fb 19             	cmp    $0x19,%bl
  800a4f:	77 08                	ja     800a59 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a51:	0f be d2             	movsbl %dl,%edx
  800a54:	83 ea 57             	sub    $0x57,%edx
  800a57:	eb 10                	jmp    800a69 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a59:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a5c:	89 f3                	mov    %esi,%ebx
  800a5e:	80 fb 19             	cmp    $0x19,%bl
  800a61:	77 14                	ja     800a77 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a63:	0f be d2             	movsbl %dl,%edx
  800a66:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a69:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a6c:	7d 09                	jge    800a77 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a6e:	41                   	inc    %ecx
  800a6f:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a73:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a75:	eb be                	jmp    800a35 <strtol+0x6f>

	if (endptr)
  800a77:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a7b:	74 05                	je     800a82 <strtol+0xbc>
		*endptr = (char *) s;
  800a7d:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a80:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a82:	85 ff                	test   %edi,%edi
  800a84:	74 21                	je     800aa7 <strtol+0xe1>
  800a86:	f7 d8                	neg    %eax
  800a88:	eb 1d                	jmp    800aa7 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a8a:	80 39 30             	cmpb   $0x30,(%ecx)
  800a8d:	75 9a                	jne    800a29 <strtol+0x63>
  800a8f:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a93:	0f 84 7a ff ff ff    	je     800a13 <strtol+0x4d>
  800a99:	eb 84                	jmp    800a1f <strtol+0x59>
  800a9b:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a9f:	0f 84 6e ff ff ff    	je     800a13 <strtol+0x4d>
  800aa5:	eb 89                	jmp    800a30 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800aa7:	5b                   	pop    %ebx
  800aa8:	5e                   	pop    %esi
  800aa9:	5f                   	pop    %edi
  800aaa:	5d                   	pop    %ebp
  800aab:	c3                   	ret    

00800aac <__udivdi3>:
  800aac:	55                   	push   %ebp
  800aad:	57                   	push   %edi
  800aae:	56                   	push   %esi
  800aaf:	53                   	push   %ebx
  800ab0:	83 ec 1c             	sub    $0x1c,%esp
  800ab3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ab7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800abb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800abf:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800ac3:	89 ca                	mov    %ecx,%edx
  800ac5:	89 f8                	mov    %edi,%eax
  800ac7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800acb:	85 f6                	test   %esi,%esi
  800acd:	75 2d                	jne    800afc <__udivdi3+0x50>
  800acf:	39 cf                	cmp    %ecx,%edi
  800ad1:	77 65                	ja     800b38 <__udivdi3+0x8c>
  800ad3:	89 fd                	mov    %edi,%ebp
  800ad5:	85 ff                	test   %edi,%edi
  800ad7:	75 0b                	jne    800ae4 <__udivdi3+0x38>
  800ad9:	b8 01 00 00 00       	mov    $0x1,%eax
  800ade:	31 d2                	xor    %edx,%edx
  800ae0:	f7 f7                	div    %edi
  800ae2:	89 c5                	mov    %eax,%ebp
  800ae4:	31 d2                	xor    %edx,%edx
  800ae6:	89 c8                	mov    %ecx,%eax
  800ae8:	f7 f5                	div    %ebp
  800aea:	89 c1                	mov    %eax,%ecx
  800aec:	89 d8                	mov    %ebx,%eax
  800aee:	f7 f5                	div    %ebp
  800af0:	89 cf                	mov    %ecx,%edi
  800af2:	89 fa                	mov    %edi,%edx
  800af4:	83 c4 1c             	add    $0x1c,%esp
  800af7:	5b                   	pop    %ebx
  800af8:	5e                   	pop    %esi
  800af9:	5f                   	pop    %edi
  800afa:	5d                   	pop    %ebp
  800afb:	c3                   	ret    
  800afc:	39 ce                	cmp    %ecx,%esi
  800afe:	77 28                	ja     800b28 <__udivdi3+0x7c>
  800b00:	0f bd fe             	bsr    %esi,%edi
  800b03:	83 f7 1f             	xor    $0x1f,%edi
  800b06:	75 40                	jne    800b48 <__udivdi3+0x9c>
  800b08:	39 ce                	cmp    %ecx,%esi
  800b0a:	72 0a                	jb     800b16 <__udivdi3+0x6a>
  800b0c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b10:	0f 87 9e 00 00 00    	ja     800bb4 <__udivdi3+0x108>
  800b16:	b8 01 00 00 00       	mov    $0x1,%eax
  800b1b:	89 fa                	mov    %edi,%edx
  800b1d:	83 c4 1c             	add    $0x1c,%esp
  800b20:	5b                   	pop    %ebx
  800b21:	5e                   	pop    %esi
  800b22:	5f                   	pop    %edi
  800b23:	5d                   	pop    %ebp
  800b24:	c3                   	ret    
  800b25:	8d 76 00             	lea    0x0(%esi),%esi
  800b28:	31 ff                	xor    %edi,%edi
  800b2a:	31 c0                	xor    %eax,%eax
  800b2c:	89 fa                	mov    %edi,%edx
  800b2e:	83 c4 1c             	add    $0x1c,%esp
  800b31:	5b                   	pop    %ebx
  800b32:	5e                   	pop    %esi
  800b33:	5f                   	pop    %edi
  800b34:	5d                   	pop    %ebp
  800b35:	c3                   	ret    
  800b36:	66 90                	xchg   %ax,%ax
  800b38:	89 d8                	mov    %ebx,%eax
  800b3a:	f7 f7                	div    %edi
  800b3c:	31 ff                	xor    %edi,%edi
  800b3e:	89 fa                	mov    %edi,%edx
  800b40:	83 c4 1c             	add    $0x1c,%esp
  800b43:	5b                   	pop    %ebx
  800b44:	5e                   	pop    %esi
  800b45:	5f                   	pop    %edi
  800b46:	5d                   	pop    %ebp
  800b47:	c3                   	ret    
  800b48:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b4d:	89 eb                	mov    %ebp,%ebx
  800b4f:	29 fb                	sub    %edi,%ebx
  800b51:	89 f9                	mov    %edi,%ecx
  800b53:	d3 e6                	shl    %cl,%esi
  800b55:	89 c5                	mov    %eax,%ebp
  800b57:	88 d9                	mov    %bl,%cl
  800b59:	d3 ed                	shr    %cl,%ebp
  800b5b:	89 e9                	mov    %ebp,%ecx
  800b5d:	09 f1                	or     %esi,%ecx
  800b5f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b63:	89 f9                	mov    %edi,%ecx
  800b65:	d3 e0                	shl    %cl,%eax
  800b67:	89 c5                	mov    %eax,%ebp
  800b69:	89 d6                	mov    %edx,%esi
  800b6b:	88 d9                	mov    %bl,%cl
  800b6d:	d3 ee                	shr    %cl,%esi
  800b6f:	89 f9                	mov    %edi,%ecx
  800b71:	d3 e2                	shl    %cl,%edx
  800b73:	8b 44 24 08          	mov    0x8(%esp),%eax
  800b77:	88 d9                	mov    %bl,%cl
  800b79:	d3 e8                	shr    %cl,%eax
  800b7b:	09 c2                	or     %eax,%edx
  800b7d:	89 d0                	mov    %edx,%eax
  800b7f:	89 f2                	mov    %esi,%edx
  800b81:	f7 74 24 0c          	divl   0xc(%esp)
  800b85:	89 d6                	mov    %edx,%esi
  800b87:	89 c3                	mov    %eax,%ebx
  800b89:	f7 e5                	mul    %ebp
  800b8b:	39 d6                	cmp    %edx,%esi
  800b8d:	72 19                	jb     800ba8 <__udivdi3+0xfc>
  800b8f:	74 0b                	je     800b9c <__udivdi3+0xf0>
  800b91:	89 d8                	mov    %ebx,%eax
  800b93:	31 ff                	xor    %edi,%edi
  800b95:	e9 58 ff ff ff       	jmp    800af2 <__udivdi3+0x46>
  800b9a:	66 90                	xchg   %ax,%ax
  800b9c:	8b 54 24 08          	mov    0x8(%esp),%edx
  800ba0:	89 f9                	mov    %edi,%ecx
  800ba2:	d3 e2                	shl    %cl,%edx
  800ba4:	39 c2                	cmp    %eax,%edx
  800ba6:	73 e9                	jae    800b91 <__udivdi3+0xe5>
  800ba8:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800bab:	31 ff                	xor    %edi,%edi
  800bad:	e9 40 ff ff ff       	jmp    800af2 <__udivdi3+0x46>
  800bb2:	66 90                	xchg   %ax,%ax
  800bb4:	31 c0                	xor    %eax,%eax
  800bb6:	e9 37 ff ff ff       	jmp    800af2 <__udivdi3+0x46>
  800bbb:	90                   	nop

00800bbc <__umoddi3>:
  800bbc:	55                   	push   %ebp
  800bbd:	57                   	push   %edi
  800bbe:	56                   	push   %esi
  800bbf:	53                   	push   %ebx
  800bc0:	83 ec 1c             	sub    $0x1c,%esp
  800bc3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bc7:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bcb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800bcf:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800bd3:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800bd7:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800bdb:	89 f3                	mov    %esi,%ebx
  800bdd:	89 fa                	mov    %edi,%edx
  800bdf:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800be3:	89 34 24             	mov    %esi,(%esp)
  800be6:	85 c0                	test   %eax,%eax
  800be8:	75 1a                	jne    800c04 <__umoddi3+0x48>
  800bea:	39 f7                	cmp    %esi,%edi
  800bec:	0f 86 a2 00 00 00    	jbe    800c94 <__umoddi3+0xd8>
  800bf2:	89 c8                	mov    %ecx,%eax
  800bf4:	89 f2                	mov    %esi,%edx
  800bf6:	f7 f7                	div    %edi
  800bf8:	89 d0                	mov    %edx,%eax
  800bfa:	31 d2                	xor    %edx,%edx
  800bfc:	83 c4 1c             	add    $0x1c,%esp
  800bff:	5b                   	pop    %ebx
  800c00:	5e                   	pop    %esi
  800c01:	5f                   	pop    %edi
  800c02:	5d                   	pop    %ebp
  800c03:	c3                   	ret    
  800c04:	39 f0                	cmp    %esi,%eax
  800c06:	0f 87 ac 00 00 00    	ja     800cb8 <__umoddi3+0xfc>
  800c0c:	0f bd e8             	bsr    %eax,%ebp
  800c0f:	83 f5 1f             	xor    $0x1f,%ebp
  800c12:	0f 84 ac 00 00 00    	je     800cc4 <__umoddi3+0x108>
  800c18:	bf 20 00 00 00       	mov    $0x20,%edi
  800c1d:	29 ef                	sub    %ebp,%edi
  800c1f:	89 fe                	mov    %edi,%esi
  800c21:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c25:	89 e9                	mov    %ebp,%ecx
  800c27:	d3 e0                	shl    %cl,%eax
  800c29:	89 d7                	mov    %edx,%edi
  800c2b:	89 f1                	mov    %esi,%ecx
  800c2d:	d3 ef                	shr    %cl,%edi
  800c2f:	09 c7                	or     %eax,%edi
  800c31:	89 e9                	mov    %ebp,%ecx
  800c33:	d3 e2                	shl    %cl,%edx
  800c35:	89 14 24             	mov    %edx,(%esp)
  800c38:	89 d8                	mov    %ebx,%eax
  800c3a:	d3 e0                	shl    %cl,%eax
  800c3c:	89 c2                	mov    %eax,%edx
  800c3e:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c42:	d3 e0                	shl    %cl,%eax
  800c44:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c48:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c4c:	89 f1                	mov    %esi,%ecx
  800c4e:	d3 e8                	shr    %cl,%eax
  800c50:	09 d0                	or     %edx,%eax
  800c52:	d3 eb                	shr    %cl,%ebx
  800c54:	89 da                	mov    %ebx,%edx
  800c56:	f7 f7                	div    %edi
  800c58:	89 d3                	mov    %edx,%ebx
  800c5a:	f7 24 24             	mull   (%esp)
  800c5d:	89 c6                	mov    %eax,%esi
  800c5f:	89 d1                	mov    %edx,%ecx
  800c61:	39 d3                	cmp    %edx,%ebx
  800c63:	0f 82 87 00 00 00    	jb     800cf0 <__umoddi3+0x134>
  800c69:	0f 84 91 00 00 00    	je     800d00 <__umoddi3+0x144>
  800c6f:	8b 54 24 04          	mov    0x4(%esp),%edx
  800c73:	29 f2                	sub    %esi,%edx
  800c75:	19 cb                	sbb    %ecx,%ebx
  800c77:	89 d8                	mov    %ebx,%eax
  800c79:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800c7d:	d3 e0                	shl    %cl,%eax
  800c7f:	89 e9                	mov    %ebp,%ecx
  800c81:	d3 ea                	shr    %cl,%edx
  800c83:	09 d0                	or     %edx,%eax
  800c85:	89 e9                	mov    %ebp,%ecx
  800c87:	d3 eb                	shr    %cl,%ebx
  800c89:	89 da                	mov    %ebx,%edx
  800c8b:	83 c4 1c             	add    $0x1c,%esp
  800c8e:	5b                   	pop    %ebx
  800c8f:	5e                   	pop    %esi
  800c90:	5f                   	pop    %edi
  800c91:	5d                   	pop    %ebp
  800c92:	c3                   	ret    
  800c93:	90                   	nop
  800c94:	89 fd                	mov    %edi,%ebp
  800c96:	85 ff                	test   %edi,%edi
  800c98:	75 0b                	jne    800ca5 <__umoddi3+0xe9>
  800c9a:	b8 01 00 00 00       	mov    $0x1,%eax
  800c9f:	31 d2                	xor    %edx,%edx
  800ca1:	f7 f7                	div    %edi
  800ca3:	89 c5                	mov    %eax,%ebp
  800ca5:	89 f0                	mov    %esi,%eax
  800ca7:	31 d2                	xor    %edx,%edx
  800ca9:	f7 f5                	div    %ebp
  800cab:	89 c8                	mov    %ecx,%eax
  800cad:	f7 f5                	div    %ebp
  800caf:	89 d0                	mov    %edx,%eax
  800cb1:	e9 44 ff ff ff       	jmp    800bfa <__umoddi3+0x3e>
  800cb6:	66 90                	xchg   %ax,%ax
  800cb8:	89 c8                	mov    %ecx,%eax
  800cba:	89 f2                	mov    %esi,%edx
  800cbc:	83 c4 1c             	add    $0x1c,%esp
  800cbf:	5b                   	pop    %ebx
  800cc0:	5e                   	pop    %esi
  800cc1:	5f                   	pop    %edi
  800cc2:	5d                   	pop    %ebp
  800cc3:	c3                   	ret    
  800cc4:	3b 04 24             	cmp    (%esp),%eax
  800cc7:	72 06                	jb     800ccf <__umoddi3+0x113>
  800cc9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800ccd:	77 0f                	ja     800cde <__umoddi3+0x122>
  800ccf:	89 f2                	mov    %esi,%edx
  800cd1:	29 f9                	sub    %edi,%ecx
  800cd3:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800cd7:	89 14 24             	mov    %edx,(%esp)
  800cda:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800cde:	8b 44 24 04          	mov    0x4(%esp),%eax
  800ce2:	8b 14 24             	mov    (%esp),%edx
  800ce5:	83 c4 1c             	add    $0x1c,%esp
  800ce8:	5b                   	pop    %ebx
  800ce9:	5e                   	pop    %esi
  800cea:	5f                   	pop    %edi
  800ceb:	5d                   	pop    %ebp
  800cec:	c3                   	ret    
  800ced:	8d 76 00             	lea    0x0(%esi),%esi
  800cf0:	2b 04 24             	sub    (%esp),%eax
  800cf3:	19 fa                	sbb    %edi,%edx
  800cf5:	89 d1                	mov    %edx,%ecx
  800cf7:	89 c6                	mov    %eax,%esi
  800cf9:	e9 71 ff ff ff       	jmp    800c6f <__umoddi3+0xb3>
  800cfe:	66 90                	xchg   %ax,%ax
  800d00:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d04:	72 ea                	jb     800cf0 <__umoddi3+0x134>
  800d06:	89 d9                	mov    %ebx,%ecx
  800d08:	e9 62 ff ff ff       	jmp    800c6f <__umoddi3+0xb3>
