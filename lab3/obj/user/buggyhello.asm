
obj/user/buggyhello:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 16 00 00 00       	call   800047 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	sys_cputs((char*)1, 1);
  800039:	6a 01                	push   $0x1
  80003b:	6a 01                	push   $0x1
  80003d:	e8 62 00 00 00       	call   8000a4 <sys_cputs>
}
  800042:	83 c4 10             	add    $0x10,%esp
  800045:	c9                   	leave  
  800046:	c3                   	ret    

00800047 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800047:	55                   	push   %ebp
  800048:	89 e5                	mov    %esp,%ebp
  80004a:	56                   	push   %esi
  80004b:	53                   	push   %ebx
  80004c:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80004f:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800052:	e8 cb 00 00 00       	call   800122 <sys_getenvid>
  800057:	25 ff 03 00 00       	and    $0x3ff,%eax
  80005c:	8d 14 00             	lea    (%eax,%eax,1),%edx
  80005f:	01 d0                	add    %edx,%eax
  800061:	c1 e0 05             	shl    $0x5,%eax
  800064:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800069:	a3 04 10 80 00       	mov    %eax,0x801004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  80006e:	85 db                	test   %ebx,%ebx
  800070:	7e 07                	jle    800079 <libmain+0x32>
		binaryname = argv[0];
  800072:	8b 06                	mov    (%esi),%eax
  800074:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  800079:	83 ec 08             	sub    $0x8,%esp
  80007c:	56                   	push   %esi
  80007d:	53                   	push   %ebx
  80007e:	e8 b0 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  800083:	e8 0a 00 00 00       	call   800092 <exit>
}
  800088:	83 c4 10             	add    $0x10,%esp
  80008b:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80008e:	5b                   	pop    %ebx
  80008f:	5e                   	pop    %esi
  800090:	5d                   	pop    %ebp
  800091:	c3                   	ret    

00800092 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800092:	55                   	push   %ebp
  800093:	89 e5                	mov    %esp,%ebp
  800095:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  800098:	6a 00                	push   $0x0
  80009a:	e8 42 00 00 00       	call   8000e1 <sys_env_destroy>
}
  80009f:	83 c4 10             	add    $0x10,%esp
  8000a2:	c9                   	leave  
  8000a3:	c3                   	ret    

008000a4 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8000a4:	55                   	push   %ebp
  8000a5:	89 e5                	mov    %esp,%ebp
  8000a7:	57                   	push   %edi
  8000a8:	56                   	push   %esi
  8000a9:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000aa:	b8 00 00 00 00       	mov    $0x0,%eax
  8000af:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000b2:	8b 55 08             	mov    0x8(%ebp),%edx
  8000b5:	89 c3                	mov    %eax,%ebx
  8000b7:	89 c7                	mov    %eax,%edi
  8000b9:	89 c6                	mov    %eax,%esi
  8000bb:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000bd:	5b                   	pop    %ebx
  8000be:	5e                   	pop    %esi
  8000bf:	5f                   	pop    %edi
  8000c0:	5d                   	pop    %ebp
  8000c1:	c3                   	ret    

008000c2 <sys_cgetc>:

int
sys_cgetc(void)
{
  8000c2:	55                   	push   %ebp
  8000c3:	89 e5                	mov    %esp,%ebp
  8000c5:	57                   	push   %edi
  8000c6:	56                   	push   %esi
  8000c7:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000c8:	ba 00 00 00 00       	mov    $0x0,%edx
  8000cd:	b8 01 00 00 00       	mov    $0x1,%eax
  8000d2:	89 d1                	mov    %edx,%ecx
  8000d4:	89 d3                	mov    %edx,%ebx
  8000d6:	89 d7                	mov    %edx,%edi
  8000d8:	89 d6                	mov    %edx,%esi
  8000da:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000dc:	5b                   	pop    %ebx
  8000dd:	5e                   	pop    %esi
  8000de:	5f                   	pop    %edi
  8000df:	5d                   	pop    %ebp
  8000e0:	c3                   	ret    

008000e1 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000e1:	55                   	push   %ebp
  8000e2:	89 e5                	mov    %esp,%ebp
  8000e4:	57                   	push   %edi
  8000e5:	56                   	push   %esi
  8000e6:	53                   	push   %ebx
  8000e7:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000ea:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000ef:	b8 03 00 00 00       	mov    $0x3,%eax
  8000f4:	8b 55 08             	mov    0x8(%ebp),%edx
  8000f7:	89 cb                	mov    %ecx,%ebx
  8000f9:	89 cf                	mov    %ecx,%edi
  8000fb:	89 ce                	mov    %ecx,%esi
  8000fd:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8000ff:	85 c0                	test   %eax,%eax
  800101:	7e 17                	jle    80011a <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800103:	83 ec 0c             	sub    $0xc,%esp
  800106:	50                   	push   %eax
  800107:	6a 03                	push   $0x3
  800109:	68 16 0d 80 00       	push   $0x800d16
  80010e:	6a 23                	push   $0x23
  800110:	68 33 0d 80 00       	push   $0x800d33
  800115:	e8 27 00 00 00       	call   800141 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  80011a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  80011d:	5b                   	pop    %ebx
  80011e:	5e                   	pop    %esi
  80011f:	5f                   	pop    %edi
  800120:	5d                   	pop    %ebp
  800121:	c3                   	ret    

00800122 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800122:	55                   	push   %ebp
  800123:	89 e5                	mov    %esp,%ebp
  800125:	57                   	push   %edi
  800126:	56                   	push   %esi
  800127:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800128:	ba 00 00 00 00       	mov    $0x0,%edx
  80012d:	b8 02 00 00 00       	mov    $0x2,%eax
  800132:	89 d1                	mov    %edx,%ecx
  800134:	89 d3                	mov    %edx,%ebx
  800136:	89 d7                	mov    %edx,%edi
  800138:	89 d6                	mov    %edx,%esi
  80013a:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  80013c:	5b                   	pop    %ebx
  80013d:	5e                   	pop    %esi
  80013e:	5f                   	pop    %edi
  80013f:	5d                   	pop    %ebp
  800140:	c3                   	ret    

00800141 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800141:	55                   	push   %ebp
  800142:	89 e5                	mov    %esp,%ebp
  800144:	56                   	push   %esi
  800145:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800146:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800149:	8b 35 00 10 80 00    	mov    0x801000,%esi
  80014f:	e8 ce ff ff ff       	call   800122 <sys_getenvid>
  800154:	83 ec 0c             	sub    $0xc,%esp
  800157:	ff 75 0c             	pushl  0xc(%ebp)
  80015a:	ff 75 08             	pushl  0x8(%ebp)
  80015d:	56                   	push   %esi
  80015e:	50                   	push   %eax
  80015f:	68 44 0d 80 00       	push   $0x800d44
  800164:	e8 b0 00 00 00       	call   800219 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800169:	83 c4 18             	add    $0x18,%esp
  80016c:	53                   	push   %ebx
  80016d:	ff 75 10             	pushl  0x10(%ebp)
  800170:	e8 53 00 00 00       	call   8001c8 <vcprintf>
	cprintf("\n");
  800175:	c7 04 24 68 0d 80 00 	movl   $0x800d68,(%esp)
  80017c:	e8 98 00 00 00       	call   800219 <cprintf>
  800181:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800184:	cc                   	int3   
  800185:	eb fd                	jmp    800184 <_panic+0x43>

00800187 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800187:	55                   	push   %ebp
  800188:	89 e5                	mov    %esp,%ebp
  80018a:	53                   	push   %ebx
  80018b:	83 ec 04             	sub    $0x4,%esp
  80018e:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  800191:	8b 13                	mov    (%ebx),%edx
  800193:	8d 42 01             	lea    0x1(%edx),%eax
  800196:	89 03                	mov    %eax,(%ebx)
  800198:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80019b:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  80019f:	3d ff 00 00 00       	cmp    $0xff,%eax
  8001a4:	75 1a                	jne    8001c0 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8001a6:	83 ec 08             	sub    $0x8,%esp
  8001a9:	68 ff 00 00 00       	push   $0xff
  8001ae:	8d 43 08             	lea    0x8(%ebx),%eax
  8001b1:	50                   	push   %eax
  8001b2:	e8 ed fe ff ff       	call   8000a4 <sys_cputs>
		b->idx = 0;
  8001b7:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8001bd:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8001c0:	ff 43 04             	incl   0x4(%ebx)
}
  8001c3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001c6:	c9                   	leave  
  8001c7:	c3                   	ret    

008001c8 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8001c8:	55                   	push   %ebp
  8001c9:	89 e5                	mov    %esp,%ebp
  8001cb:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001d1:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001d8:	00 00 00 
	b.cnt = 0;
  8001db:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001e2:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001e5:	ff 75 0c             	pushl  0xc(%ebp)
  8001e8:	ff 75 08             	pushl  0x8(%ebp)
  8001eb:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001f1:	50                   	push   %eax
  8001f2:	68 87 01 80 00       	push   $0x800187
  8001f7:	e8 51 01 00 00       	call   80034d <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001fc:	83 c4 08             	add    $0x8,%esp
  8001ff:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800205:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80020b:	50                   	push   %eax
  80020c:	e8 93 fe ff ff       	call   8000a4 <sys_cputs>

	return b.cnt;
}
  800211:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800217:	c9                   	leave  
  800218:	c3                   	ret    

00800219 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800219:	55                   	push   %ebp
  80021a:	89 e5                	mov    %esp,%ebp
  80021c:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80021f:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800222:	50                   	push   %eax
  800223:	ff 75 08             	pushl  0x8(%ebp)
  800226:	e8 9d ff ff ff       	call   8001c8 <vcprintf>
	va_end(ap);

	return cnt;
}
  80022b:	c9                   	leave  
  80022c:	c3                   	ret    

0080022d <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80022d:	55                   	push   %ebp
  80022e:	89 e5                	mov    %esp,%ebp
  800230:	57                   	push   %edi
  800231:	56                   	push   %esi
  800232:	53                   	push   %ebx
  800233:	83 ec 1c             	sub    $0x1c,%esp
  800236:	89 c7                	mov    %eax,%edi
  800238:	89 d6                	mov    %edx,%esi
  80023a:	8b 45 08             	mov    0x8(%ebp),%eax
  80023d:	8b 55 0c             	mov    0xc(%ebp),%edx
  800240:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800243:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800246:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800249:	bb 00 00 00 00       	mov    $0x0,%ebx
  80024e:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800251:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800254:	39 d3                	cmp    %edx,%ebx
  800256:	72 05                	jb     80025d <printnum+0x30>
  800258:	39 45 10             	cmp    %eax,0x10(%ebp)
  80025b:	77 45                	ja     8002a2 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  80025d:	83 ec 0c             	sub    $0xc,%esp
  800260:	ff 75 18             	pushl  0x18(%ebp)
  800263:	8b 45 14             	mov    0x14(%ebp),%eax
  800266:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800269:	53                   	push   %ebx
  80026a:	ff 75 10             	pushl  0x10(%ebp)
  80026d:	83 ec 08             	sub    $0x8,%esp
  800270:	ff 75 e4             	pushl  -0x1c(%ebp)
  800273:	ff 75 e0             	pushl  -0x20(%ebp)
  800276:	ff 75 dc             	pushl  -0x24(%ebp)
  800279:	ff 75 d8             	pushl  -0x28(%ebp)
  80027c:	e8 27 08 00 00       	call   800aa8 <__udivdi3>
  800281:	83 c4 18             	add    $0x18,%esp
  800284:	52                   	push   %edx
  800285:	50                   	push   %eax
  800286:	89 f2                	mov    %esi,%edx
  800288:	89 f8                	mov    %edi,%eax
  80028a:	e8 9e ff ff ff       	call   80022d <printnum>
  80028f:	83 c4 20             	add    $0x20,%esp
  800292:	eb 16                	jmp    8002aa <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800294:	83 ec 08             	sub    $0x8,%esp
  800297:	56                   	push   %esi
  800298:	ff 75 18             	pushl  0x18(%ebp)
  80029b:	ff d7                	call   *%edi
  80029d:	83 c4 10             	add    $0x10,%esp
  8002a0:	eb 03                	jmp    8002a5 <printnum+0x78>
  8002a2:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8002a5:	4b                   	dec    %ebx
  8002a6:	85 db                	test   %ebx,%ebx
  8002a8:	7f ea                	jg     800294 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8002aa:	83 ec 08             	sub    $0x8,%esp
  8002ad:	56                   	push   %esi
  8002ae:	83 ec 04             	sub    $0x4,%esp
  8002b1:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002b4:	ff 75 e0             	pushl  -0x20(%ebp)
  8002b7:	ff 75 dc             	pushl  -0x24(%ebp)
  8002ba:	ff 75 d8             	pushl  -0x28(%ebp)
  8002bd:	e8 f6 08 00 00       	call   800bb8 <__umoddi3>
  8002c2:	83 c4 14             	add    $0x14,%esp
  8002c5:	0f be 80 6a 0d 80 00 	movsbl 0x800d6a(%eax),%eax
  8002cc:	50                   	push   %eax
  8002cd:	ff d7                	call   *%edi
}
  8002cf:	83 c4 10             	add    $0x10,%esp
  8002d2:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002d5:	5b                   	pop    %ebx
  8002d6:	5e                   	pop    %esi
  8002d7:	5f                   	pop    %edi
  8002d8:	5d                   	pop    %ebp
  8002d9:	c3                   	ret    

008002da <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002da:	55                   	push   %ebp
  8002db:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002dd:	83 fa 01             	cmp    $0x1,%edx
  8002e0:	7e 0e                	jle    8002f0 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002e2:	8b 10                	mov    (%eax),%edx
  8002e4:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002e7:	89 08                	mov    %ecx,(%eax)
  8002e9:	8b 02                	mov    (%edx),%eax
  8002eb:	8b 52 04             	mov    0x4(%edx),%edx
  8002ee:	eb 22                	jmp    800312 <getuint+0x38>
	else if (lflag)
  8002f0:	85 d2                	test   %edx,%edx
  8002f2:	74 10                	je     800304 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8002f4:	8b 10                	mov    (%eax),%edx
  8002f6:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002f9:	89 08                	mov    %ecx,(%eax)
  8002fb:	8b 02                	mov    (%edx),%eax
  8002fd:	ba 00 00 00 00       	mov    $0x0,%edx
  800302:	eb 0e                	jmp    800312 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800304:	8b 10                	mov    (%eax),%edx
  800306:	8d 4a 04             	lea    0x4(%edx),%ecx
  800309:	89 08                	mov    %ecx,(%eax)
  80030b:	8b 02                	mov    (%edx),%eax
  80030d:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800312:	5d                   	pop    %ebp
  800313:	c3                   	ret    

00800314 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800314:	55                   	push   %ebp
  800315:	89 e5                	mov    %esp,%ebp
  800317:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80031a:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  80031d:	8b 10                	mov    (%eax),%edx
  80031f:	3b 50 04             	cmp    0x4(%eax),%edx
  800322:	73 0a                	jae    80032e <sprintputch+0x1a>
		*b->buf++ = ch;
  800324:	8d 4a 01             	lea    0x1(%edx),%ecx
  800327:	89 08                	mov    %ecx,(%eax)
  800329:	8b 45 08             	mov    0x8(%ebp),%eax
  80032c:	88 02                	mov    %al,(%edx)
}
  80032e:	5d                   	pop    %ebp
  80032f:	c3                   	ret    

00800330 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800330:	55                   	push   %ebp
  800331:	89 e5                	mov    %esp,%ebp
  800333:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800336:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800339:	50                   	push   %eax
  80033a:	ff 75 10             	pushl  0x10(%ebp)
  80033d:	ff 75 0c             	pushl  0xc(%ebp)
  800340:	ff 75 08             	pushl  0x8(%ebp)
  800343:	e8 05 00 00 00       	call   80034d <vprintfmt>
	va_end(ap);
}
  800348:	83 c4 10             	add    $0x10,%esp
  80034b:	c9                   	leave  
  80034c:	c3                   	ret    

0080034d <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80034d:	55                   	push   %ebp
  80034e:	89 e5                	mov    %esp,%ebp
  800350:	57                   	push   %edi
  800351:	56                   	push   %esi
  800352:	53                   	push   %ebx
  800353:	83 ec 2c             	sub    $0x2c,%esp
  800356:	8b 75 08             	mov    0x8(%ebp),%esi
  800359:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  80035c:	8b 7d 10             	mov    0x10(%ebp),%edi
  80035f:	eb 12                	jmp    800373 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800361:	85 c0                	test   %eax,%eax
  800363:	0f 84 68 03 00 00    	je     8006d1 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800369:	83 ec 08             	sub    $0x8,%esp
  80036c:	53                   	push   %ebx
  80036d:	50                   	push   %eax
  80036e:	ff d6                	call   *%esi
  800370:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800373:	47                   	inc    %edi
  800374:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800378:	83 f8 25             	cmp    $0x25,%eax
  80037b:	75 e4                	jne    800361 <vprintfmt+0x14>
  80037d:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  800381:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800388:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80038f:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800396:	ba 00 00 00 00       	mov    $0x0,%edx
  80039b:	eb 07                	jmp    8003a4 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80039d:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8003a0:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003a4:	8d 47 01             	lea    0x1(%edi),%eax
  8003a7:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8003aa:	0f b6 0f             	movzbl (%edi),%ecx
  8003ad:	8a 07                	mov    (%edi),%al
  8003af:	83 e8 23             	sub    $0x23,%eax
  8003b2:	3c 55                	cmp    $0x55,%al
  8003b4:	0f 87 fe 02 00 00    	ja     8006b8 <vprintfmt+0x36b>
  8003ba:	0f b6 c0             	movzbl %al,%eax
  8003bd:	ff 24 85 f8 0d 80 00 	jmp    *0x800df8(,%eax,4)
  8003c4:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003c7:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003cb:	eb d7                	jmp    8003a4 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003cd:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003d0:	b8 00 00 00 00       	mov    $0x0,%eax
  8003d5:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003d8:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003db:	01 c0                	add    %eax,%eax
  8003dd:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003e1:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003e4:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003e7:	83 fa 09             	cmp    $0x9,%edx
  8003ea:	77 34                	ja     800420 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003ec:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003ed:	eb e9                	jmp    8003d8 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003ef:	8b 45 14             	mov    0x14(%ebp),%eax
  8003f2:	8d 48 04             	lea    0x4(%eax),%ecx
  8003f5:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8003f8:	8b 00                	mov    (%eax),%eax
  8003fa:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003fd:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800400:	eb 24                	jmp    800426 <vprintfmt+0xd9>
  800402:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800406:	79 07                	jns    80040f <vprintfmt+0xc2>
  800408:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80040f:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800412:	eb 90                	jmp    8003a4 <vprintfmt+0x57>
  800414:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800417:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  80041e:	eb 84                	jmp    8003a4 <vprintfmt+0x57>
  800420:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800423:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800426:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80042a:	0f 89 74 ff ff ff    	jns    8003a4 <vprintfmt+0x57>
				width = precision, precision = -1;
  800430:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800433:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800436:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80043d:	e9 62 ff ff ff       	jmp    8003a4 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800442:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800443:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800446:	e9 59 ff ff ff       	jmp    8003a4 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80044b:	8b 45 14             	mov    0x14(%ebp),%eax
  80044e:	8d 50 04             	lea    0x4(%eax),%edx
  800451:	89 55 14             	mov    %edx,0x14(%ebp)
  800454:	83 ec 08             	sub    $0x8,%esp
  800457:	53                   	push   %ebx
  800458:	ff 30                	pushl  (%eax)
  80045a:	ff d6                	call   *%esi
			break;
  80045c:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80045f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800462:	e9 0c ff ff ff       	jmp    800373 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800467:	8b 45 14             	mov    0x14(%ebp),%eax
  80046a:	8d 50 04             	lea    0x4(%eax),%edx
  80046d:	89 55 14             	mov    %edx,0x14(%ebp)
  800470:	8b 00                	mov    (%eax),%eax
  800472:	85 c0                	test   %eax,%eax
  800474:	79 02                	jns    800478 <vprintfmt+0x12b>
  800476:	f7 d8                	neg    %eax
  800478:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80047a:	83 f8 06             	cmp    $0x6,%eax
  80047d:	7f 0b                	jg     80048a <vprintfmt+0x13d>
  80047f:	8b 04 85 50 0f 80 00 	mov    0x800f50(,%eax,4),%eax
  800486:	85 c0                	test   %eax,%eax
  800488:	75 18                	jne    8004a2 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  80048a:	52                   	push   %edx
  80048b:	68 82 0d 80 00       	push   $0x800d82
  800490:	53                   	push   %ebx
  800491:	56                   	push   %esi
  800492:	e8 99 fe ff ff       	call   800330 <printfmt>
  800497:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80049a:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  80049d:	e9 d1 fe ff ff       	jmp    800373 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8004a2:	50                   	push   %eax
  8004a3:	68 8b 0d 80 00       	push   $0x800d8b
  8004a8:	53                   	push   %ebx
  8004a9:	56                   	push   %esi
  8004aa:	e8 81 fe ff ff       	call   800330 <printfmt>
  8004af:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004b2:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004b5:	e9 b9 fe ff ff       	jmp    800373 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004ba:	8b 45 14             	mov    0x14(%ebp),%eax
  8004bd:	8d 50 04             	lea    0x4(%eax),%edx
  8004c0:	89 55 14             	mov    %edx,0x14(%ebp)
  8004c3:	8b 38                	mov    (%eax),%edi
  8004c5:	85 ff                	test   %edi,%edi
  8004c7:	75 05                	jne    8004ce <vprintfmt+0x181>
				p = "(null)";
  8004c9:	bf 7b 0d 80 00       	mov    $0x800d7b,%edi
			if (width > 0 && padc != '-')
  8004ce:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004d2:	0f 8e 90 00 00 00    	jle    800568 <vprintfmt+0x21b>
  8004d8:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004dc:	0f 84 8e 00 00 00    	je     800570 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004e2:	83 ec 08             	sub    $0x8,%esp
  8004e5:	ff 75 d0             	pushl  -0x30(%ebp)
  8004e8:	57                   	push   %edi
  8004e9:	e8 70 02 00 00       	call   80075e <strnlen>
  8004ee:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8004f1:	29 c1                	sub    %eax,%ecx
  8004f3:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8004f6:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8004f9:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8004fd:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800500:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800503:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800505:	eb 0d                	jmp    800514 <vprintfmt+0x1c7>
					putch(padc, putdat);
  800507:	83 ec 08             	sub    $0x8,%esp
  80050a:	53                   	push   %ebx
  80050b:	ff 75 e4             	pushl  -0x1c(%ebp)
  80050e:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800510:	4f                   	dec    %edi
  800511:	83 c4 10             	add    $0x10,%esp
  800514:	85 ff                	test   %edi,%edi
  800516:	7f ef                	jg     800507 <vprintfmt+0x1ba>
  800518:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80051b:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80051e:	89 c8                	mov    %ecx,%eax
  800520:	85 c9                	test   %ecx,%ecx
  800522:	79 05                	jns    800529 <vprintfmt+0x1dc>
  800524:	b8 00 00 00 00       	mov    $0x0,%eax
  800529:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  80052c:	29 c1                	sub    %eax,%ecx
  80052e:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800531:	89 75 08             	mov    %esi,0x8(%ebp)
  800534:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800537:	eb 3d                	jmp    800576 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800539:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  80053d:	74 19                	je     800558 <vprintfmt+0x20b>
  80053f:	0f be c0             	movsbl %al,%eax
  800542:	83 e8 20             	sub    $0x20,%eax
  800545:	83 f8 5e             	cmp    $0x5e,%eax
  800548:	76 0e                	jbe    800558 <vprintfmt+0x20b>
					putch('?', putdat);
  80054a:	83 ec 08             	sub    $0x8,%esp
  80054d:	53                   	push   %ebx
  80054e:	6a 3f                	push   $0x3f
  800550:	ff 55 08             	call   *0x8(%ebp)
  800553:	83 c4 10             	add    $0x10,%esp
  800556:	eb 0b                	jmp    800563 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800558:	83 ec 08             	sub    $0x8,%esp
  80055b:	53                   	push   %ebx
  80055c:	52                   	push   %edx
  80055d:	ff 55 08             	call   *0x8(%ebp)
  800560:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800563:	ff 4d e4             	decl   -0x1c(%ebp)
  800566:	eb 0e                	jmp    800576 <vprintfmt+0x229>
  800568:	89 75 08             	mov    %esi,0x8(%ebp)
  80056b:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80056e:	eb 06                	jmp    800576 <vprintfmt+0x229>
  800570:	89 75 08             	mov    %esi,0x8(%ebp)
  800573:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800576:	47                   	inc    %edi
  800577:	8a 47 ff             	mov    -0x1(%edi),%al
  80057a:	0f be d0             	movsbl %al,%edx
  80057d:	85 d2                	test   %edx,%edx
  80057f:	74 1d                	je     80059e <vprintfmt+0x251>
  800581:	85 f6                	test   %esi,%esi
  800583:	78 b4                	js     800539 <vprintfmt+0x1ec>
  800585:	4e                   	dec    %esi
  800586:	79 b1                	jns    800539 <vprintfmt+0x1ec>
  800588:	8b 75 08             	mov    0x8(%ebp),%esi
  80058b:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80058e:	eb 14                	jmp    8005a4 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  800590:	83 ec 08             	sub    $0x8,%esp
  800593:	53                   	push   %ebx
  800594:	6a 20                	push   $0x20
  800596:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800598:	4f                   	dec    %edi
  800599:	83 c4 10             	add    $0x10,%esp
  80059c:	eb 06                	jmp    8005a4 <vprintfmt+0x257>
  80059e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8005a1:	8b 75 08             	mov    0x8(%ebp),%esi
  8005a4:	85 ff                	test   %edi,%edi
  8005a6:	7f e8                	jg     800590 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005a8:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005ab:	e9 c3 fd ff ff       	jmp    800373 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8005b0:	83 fa 01             	cmp    $0x1,%edx
  8005b3:	7e 16                	jle    8005cb <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8005b5:	8b 45 14             	mov    0x14(%ebp),%eax
  8005b8:	8d 50 08             	lea    0x8(%eax),%edx
  8005bb:	89 55 14             	mov    %edx,0x14(%ebp)
  8005be:	8b 50 04             	mov    0x4(%eax),%edx
  8005c1:	8b 00                	mov    (%eax),%eax
  8005c3:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005c6:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8005c9:	eb 32                	jmp    8005fd <vprintfmt+0x2b0>
	else if (lflag)
  8005cb:	85 d2                	test   %edx,%edx
  8005cd:	74 18                	je     8005e7 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005cf:	8b 45 14             	mov    0x14(%ebp),%eax
  8005d2:	8d 50 04             	lea    0x4(%eax),%edx
  8005d5:	89 55 14             	mov    %edx,0x14(%ebp)
  8005d8:	8b 00                	mov    (%eax),%eax
  8005da:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005dd:	89 c1                	mov    %eax,%ecx
  8005df:	c1 f9 1f             	sar    $0x1f,%ecx
  8005e2:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005e5:	eb 16                	jmp    8005fd <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005e7:	8b 45 14             	mov    0x14(%ebp),%eax
  8005ea:	8d 50 04             	lea    0x4(%eax),%edx
  8005ed:	89 55 14             	mov    %edx,0x14(%ebp)
  8005f0:	8b 00                	mov    (%eax),%eax
  8005f2:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005f5:	89 c1                	mov    %eax,%ecx
  8005f7:	c1 f9 1f             	sar    $0x1f,%ecx
  8005fa:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005fd:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800600:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800603:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800607:	79 76                	jns    80067f <vprintfmt+0x332>
				putch('-', putdat);
  800609:	83 ec 08             	sub    $0x8,%esp
  80060c:	53                   	push   %ebx
  80060d:	6a 2d                	push   $0x2d
  80060f:	ff d6                	call   *%esi
				num = -(long long) num;
  800611:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800614:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800617:	f7 d8                	neg    %eax
  800619:	83 d2 00             	adc    $0x0,%edx
  80061c:	f7 da                	neg    %edx
  80061e:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800621:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800626:	eb 5c                	jmp    800684 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800628:	8d 45 14             	lea    0x14(%ebp),%eax
  80062b:	e8 aa fc ff ff       	call   8002da <getuint>
			base = 10;
  800630:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800635:	eb 4d                	jmp    800684 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800637:	8d 45 14             	lea    0x14(%ebp),%eax
  80063a:	e8 9b fc ff ff       	call   8002da <getuint>
			base = 8;
  80063f:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800644:	eb 3e                	jmp    800684 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800646:	83 ec 08             	sub    $0x8,%esp
  800649:	53                   	push   %ebx
  80064a:	6a 30                	push   $0x30
  80064c:	ff d6                	call   *%esi
			putch('x', putdat);
  80064e:	83 c4 08             	add    $0x8,%esp
  800651:	53                   	push   %ebx
  800652:	6a 78                	push   $0x78
  800654:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800656:	8b 45 14             	mov    0x14(%ebp),%eax
  800659:	8d 50 04             	lea    0x4(%eax),%edx
  80065c:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  80065f:	8b 00                	mov    (%eax),%eax
  800661:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800666:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800669:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  80066e:	eb 14                	jmp    800684 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800670:	8d 45 14             	lea    0x14(%ebp),%eax
  800673:	e8 62 fc ff ff       	call   8002da <getuint>
			base = 16;
  800678:	b9 10 00 00 00       	mov    $0x10,%ecx
  80067d:	eb 05                	jmp    800684 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  80067f:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  800684:	83 ec 0c             	sub    $0xc,%esp
  800687:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  80068b:	57                   	push   %edi
  80068c:	ff 75 e4             	pushl  -0x1c(%ebp)
  80068f:	51                   	push   %ecx
  800690:	52                   	push   %edx
  800691:	50                   	push   %eax
  800692:	89 da                	mov    %ebx,%edx
  800694:	89 f0                	mov    %esi,%eax
  800696:	e8 92 fb ff ff       	call   80022d <printnum>
			break;
  80069b:	83 c4 20             	add    $0x20,%esp
  80069e:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8006a1:	e9 cd fc ff ff       	jmp    800373 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8006a6:	83 ec 08             	sub    $0x8,%esp
  8006a9:	53                   	push   %ebx
  8006aa:	51                   	push   %ecx
  8006ab:	ff d6                	call   *%esi
			break;
  8006ad:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006b0:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8006b3:	e9 bb fc ff ff       	jmp    800373 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006b8:	83 ec 08             	sub    $0x8,%esp
  8006bb:	53                   	push   %ebx
  8006bc:	6a 25                	push   $0x25
  8006be:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006c0:	83 c4 10             	add    $0x10,%esp
  8006c3:	eb 01                	jmp    8006c6 <vprintfmt+0x379>
  8006c5:	4f                   	dec    %edi
  8006c6:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006ca:	75 f9                	jne    8006c5 <vprintfmt+0x378>
  8006cc:	e9 a2 fc ff ff       	jmp    800373 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006d1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006d4:	5b                   	pop    %ebx
  8006d5:	5e                   	pop    %esi
  8006d6:	5f                   	pop    %edi
  8006d7:	5d                   	pop    %ebp
  8006d8:	c3                   	ret    

008006d9 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006d9:	55                   	push   %ebp
  8006da:	89 e5                	mov    %esp,%ebp
  8006dc:	83 ec 18             	sub    $0x18,%esp
  8006df:	8b 45 08             	mov    0x8(%ebp),%eax
  8006e2:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006e5:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006e8:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006ec:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8006ef:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8006f6:	85 c0                	test   %eax,%eax
  8006f8:	74 26                	je     800720 <vsnprintf+0x47>
  8006fa:	85 d2                	test   %edx,%edx
  8006fc:	7e 29                	jle    800727 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8006fe:	ff 75 14             	pushl  0x14(%ebp)
  800701:	ff 75 10             	pushl  0x10(%ebp)
  800704:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800707:	50                   	push   %eax
  800708:	68 14 03 80 00       	push   $0x800314
  80070d:	e8 3b fc ff ff       	call   80034d <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800712:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800715:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800718:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80071b:	83 c4 10             	add    $0x10,%esp
  80071e:	eb 0c                	jmp    80072c <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800720:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800725:	eb 05                	jmp    80072c <vsnprintf+0x53>
  800727:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  80072c:	c9                   	leave  
  80072d:	c3                   	ret    

0080072e <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80072e:	55                   	push   %ebp
  80072f:	89 e5                	mov    %esp,%ebp
  800731:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800734:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800737:	50                   	push   %eax
  800738:	ff 75 10             	pushl  0x10(%ebp)
  80073b:	ff 75 0c             	pushl  0xc(%ebp)
  80073e:	ff 75 08             	pushl  0x8(%ebp)
  800741:	e8 93 ff ff ff       	call   8006d9 <vsnprintf>
	va_end(ap);

	return rc;
}
  800746:	c9                   	leave  
  800747:	c3                   	ret    

00800748 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800748:	55                   	push   %ebp
  800749:	89 e5                	mov    %esp,%ebp
  80074b:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  80074e:	b8 00 00 00 00       	mov    $0x0,%eax
  800753:	eb 01                	jmp    800756 <strlen+0xe>
		n++;
  800755:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800756:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80075a:	75 f9                	jne    800755 <strlen+0xd>
		n++;
	return n;
}
  80075c:	5d                   	pop    %ebp
  80075d:	c3                   	ret    

0080075e <strnlen>:

int
strnlen(const char *s, size_t size)
{
  80075e:	55                   	push   %ebp
  80075f:	89 e5                	mov    %esp,%ebp
  800761:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800764:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800767:	ba 00 00 00 00       	mov    $0x0,%edx
  80076c:	eb 01                	jmp    80076f <strnlen+0x11>
		n++;
  80076e:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80076f:	39 c2                	cmp    %eax,%edx
  800771:	74 08                	je     80077b <strnlen+0x1d>
  800773:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800777:	75 f5                	jne    80076e <strnlen+0x10>
  800779:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80077b:	5d                   	pop    %ebp
  80077c:	c3                   	ret    

0080077d <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  80077d:	55                   	push   %ebp
  80077e:	89 e5                	mov    %esp,%ebp
  800780:	53                   	push   %ebx
  800781:	8b 45 08             	mov    0x8(%ebp),%eax
  800784:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800787:	89 c2                	mov    %eax,%edx
  800789:	42                   	inc    %edx
  80078a:	41                   	inc    %ecx
  80078b:	8a 59 ff             	mov    -0x1(%ecx),%bl
  80078e:	88 5a ff             	mov    %bl,-0x1(%edx)
  800791:	84 db                	test   %bl,%bl
  800793:	75 f4                	jne    800789 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800795:	5b                   	pop    %ebx
  800796:	5d                   	pop    %ebp
  800797:	c3                   	ret    

00800798 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800798:	55                   	push   %ebp
  800799:	89 e5                	mov    %esp,%ebp
  80079b:	53                   	push   %ebx
  80079c:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  80079f:	53                   	push   %ebx
  8007a0:	e8 a3 ff ff ff       	call   800748 <strlen>
  8007a5:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8007a8:	ff 75 0c             	pushl  0xc(%ebp)
  8007ab:	01 d8                	add    %ebx,%eax
  8007ad:	50                   	push   %eax
  8007ae:	e8 ca ff ff ff       	call   80077d <strcpy>
	return dst;
}
  8007b3:	89 d8                	mov    %ebx,%eax
  8007b5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8007b8:	c9                   	leave  
  8007b9:	c3                   	ret    

008007ba <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8007ba:	55                   	push   %ebp
  8007bb:	89 e5                	mov    %esp,%ebp
  8007bd:	56                   	push   %esi
  8007be:	53                   	push   %ebx
  8007bf:	8b 75 08             	mov    0x8(%ebp),%esi
  8007c2:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007c5:	89 f3                	mov    %esi,%ebx
  8007c7:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007ca:	89 f2                	mov    %esi,%edx
  8007cc:	eb 0c                	jmp    8007da <strncpy+0x20>
		*dst++ = *src;
  8007ce:	42                   	inc    %edx
  8007cf:	8a 01                	mov    (%ecx),%al
  8007d1:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007d4:	80 39 01             	cmpb   $0x1,(%ecx)
  8007d7:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007da:	39 da                	cmp    %ebx,%edx
  8007dc:	75 f0                	jne    8007ce <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007de:	89 f0                	mov    %esi,%eax
  8007e0:	5b                   	pop    %ebx
  8007e1:	5e                   	pop    %esi
  8007e2:	5d                   	pop    %ebp
  8007e3:	c3                   	ret    

008007e4 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007e4:	55                   	push   %ebp
  8007e5:	89 e5                	mov    %esp,%ebp
  8007e7:	56                   	push   %esi
  8007e8:	53                   	push   %ebx
  8007e9:	8b 75 08             	mov    0x8(%ebp),%esi
  8007ec:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007ef:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8007f2:	85 c0                	test   %eax,%eax
  8007f4:	74 1e                	je     800814 <strlcpy+0x30>
  8007f6:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8007fa:	89 f2                	mov    %esi,%edx
  8007fc:	eb 05                	jmp    800803 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8007fe:	42                   	inc    %edx
  8007ff:	41                   	inc    %ecx
  800800:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800803:	39 c2                	cmp    %eax,%edx
  800805:	74 08                	je     80080f <strlcpy+0x2b>
  800807:	8a 19                	mov    (%ecx),%bl
  800809:	84 db                	test   %bl,%bl
  80080b:	75 f1                	jne    8007fe <strlcpy+0x1a>
  80080d:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  80080f:	c6 00 00             	movb   $0x0,(%eax)
  800812:	eb 02                	jmp    800816 <strlcpy+0x32>
  800814:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800816:	29 f0                	sub    %esi,%eax
}
  800818:	5b                   	pop    %ebx
  800819:	5e                   	pop    %esi
  80081a:	5d                   	pop    %ebp
  80081b:	c3                   	ret    

0080081c <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80081c:	55                   	push   %ebp
  80081d:	89 e5                	mov    %esp,%ebp
  80081f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800822:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800825:	eb 02                	jmp    800829 <strcmp+0xd>
		p++, q++;
  800827:	41                   	inc    %ecx
  800828:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800829:	8a 01                	mov    (%ecx),%al
  80082b:	84 c0                	test   %al,%al
  80082d:	74 04                	je     800833 <strcmp+0x17>
  80082f:	3a 02                	cmp    (%edx),%al
  800831:	74 f4                	je     800827 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800833:	0f b6 c0             	movzbl %al,%eax
  800836:	0f b6 12             	movzbl (%edx),%edx
  800839:	29 d0                	sub    %edx,%eax
}
  80083b:	5d                   	pop    %ebp
  80083c:	c3                   	ret    

0080083d <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  80083d:	55                   	push   %ebp
  80083e:	89 e5                	mov    %esp,%ebp
  800840:	53                   	push   %ebx
  800841:	8b 45 08             	mov    0x8(%ebp),%eax
  800844:	8b 55 0c             	mov    0xc(%ebp),%edx
  800847:	89 c3                	mov    %eax,%ebx
  800849:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  80084c:	eb 02                	jmp    800850 <strncmp+0x13>
		n--, p++, q++;
  80084e:	40                   	inc    %eax
  80084f:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800850:	39 d8                	cmp    %ebx,%eax
  800852:	74 14                	je     800868 <strncmp+0x2b>
  800854:	8a 08                	mov    (%eax),%cl
  800856:	84 c9                	test   %cl,%cl
  800858:	74 04                	je     80085e <strncmp+0x21>
  80085a:	3a 0a                	cmp    (%edx),%cl
  80085c:	74 f0                	je     80084e <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  80085e:	0f b6 00             	movzbl (%eax),%eax
  800861:	0f b6 12             	movzbl (%edx),%edx
  800864:	29 d0                	sub    %edx,%eax
  800866:	eb 05                	jmp    80086d <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800868:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  80086d:	5b                   	pop    %ebx
  80086e:	5d                   	pop    %ebp
  80086f:	c3                   	ret    

00800870 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800870:	55                   	push   %ebp
  800871:	89 e5                	mov    %esp,%ebp
  800873:	8b 45 08             	mov    0x8(%ebp),%eax
  800876:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800879:	eb 05                	jmp    800880 <strchr+0x10>
		if (*s == c)
  80087b:	38 ca                	cmp    %cl,%dl
  80087d:	74 0c                	je     80088b <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  80087f:	40                   	inc    %eax
  800880:	8a 10                	mov    (%eax),%dl
  800882:	84 d2                	test   %dl,%dl
  800884:	75 f5                	jne    80087b <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800886:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80088b:	5d                   	pop    %ebp
  80088c:	c3                   	ret    

0080088d <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  80088d:	55                   	push   %ebp
  80088e:	89 e5                	mov    %esp,%ebp
  800890:	8b 45 08             	mov    0x8(%ebp),%eax
  800893:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800896:	eb 05                	jmp    80089d <strfind+0x10>
		if (*s == c)
  800898:	38 ca                	cmp    %cl,%dl
  80089a:	74 07                	je     8008a3 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  80089c:	40                   	inc    %eax
  80089d:	8a 10                	mov    (%eax),%dl
  80089f:	84 d2                	test   %dl,%dl
  8008a1:	75 f5                	jne    800898 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8008a3:	5d                   	pop    %ebp
  8008a4:	c3                   	ret    

008008a5 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8008a5:	55                   	push   %ebp
  8008a6:	89 e5                	mov    %esp,%ebp
  8008a8:	57                   	push   %edi
  8008a9:	56                   	push   %esi
  8008aa:	53                   	push   %ebx
  8008ab:	8b 7d 08             	mov    0x8(%ebp),%edi
  8008ae:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8008b1:	85 c9                	test   %ecx,%ecx
  8008b3:	74 36                	je     8008eb <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8008b5:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8008bb:	75 28                	jne    8008e5 <memset+0x40>
  8008bd:	f6 c1 03             	test   $0x3,%cl
  8008c0:	75 23                	jne    8008e5 <memset+0x40>
		c &= 0xFF;
  8008c2:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8008c6:	89 d3                	mov    %edx,%ebx
  8008c8:	c1 e3 08             	shl    $0x8,%ebx
  8008cb:	89 d6                	mov    %edx,%esi
  8008cd:	c1 e6 18             	shl    $0x18,%esi
  8008d0:	89 d0                	mov    %edx,%eax
  8008d2:	c1 e0 10             	shl    $0x10,%eax
  8008d5:	09 f0                	or     %esi,%eax
  8008d7:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008d9:	89 d8                	mov    %ebx,%eax
  8008db:	09 d0                	or     %edx,%eax
  8008dd:	c1 e9 02             	shr    $0x2,%ecx
  8008e0:	fc                   	cld    
  8008e1:	f3 ab                	rep stos %eax,%es:(%edi)
  8008e3:	eb 06                	jmp    8008eb <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008e5:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008e8:	fc                   	cld    
  8008e9:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008eb:	89 f8                	mov    %edi,%eax
  8008ed:	5b                   	pop    %ebx
  8008ee:	5e                   	pop    %esi
  8008ef:	5f                   	pop    %edi
  8008f0:	5d                   	pop    %ebp
  8008f1:	c3                   	ret    

008008f2 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8008f2:	55                   	push   %ebp
  8008f3:	89 e5                	mov    %esp,%ebp
  8008f5:	57                   	push   %edi
  8008f6:	56                   	push   %esi
  8008f7:	8b 45 08             	mov    0x8(%ebp),%eax
  8008fa:	8b 75 0c             	mov    0xc(%ebp),%esi
  8008fd:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800900:	39 c6                	cmp    %eax,%esi
  800902:	73 33                	jae    800937 <memmove+0x45>
  800904:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800907:	39 d0                	cmp    %edx,%eax
  800909:	73 2c                	jae    800937 <memmove+0x45>
		s += n;
		d += n;
  80090b:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80090e:	89 d6                	mov    %edx,%esi
  800910:	09 fe                	or     %edi,%esi
  800912:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800918:	75 13                	jne    80092d <memmove+0x3b>
  80091a:	f6 c1 03             	test   $0x3,%cl
  80091d:	75 0e                	jne    80092d <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  80091f:	83 ef 04             	sub    $0x4,%edi
  800922:	8d 72 fc             	lea    -0x4(%edx),%esi
  800925:	c1 e9 02             	shr    $0x2,%ecx
  800928:	fd                   	std    
  800929:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80092b:	eb 07                	jmp    800934 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  80092d:	4f                   	dec    %edi
  80092e:	8d 72 ff             	lea    -0x1(%edx),%esi
  800931:	fd                   	std    
  800932:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800934:	fc                   	cld    
  800935:	eb 1d                	jmp    800954 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800937:	89 f2                	mov    %esi,%edx
  800939:	09 c2                	or     %eax,%edx
  80093b:	f6 c2 03             	test   $0x3,%dl
  80093e:	75 0f                	jne    80094f <memmove+0x5d>
  800940:	f6 c1 03             	test   $0x3,%cl
  800943:	75 0a                	jne    80094f <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800945:	c1 e9 02             	shr    $0x2,%ecx
  800948:	89 c7                	mov    %eax,%edi
  80094a:	fc                   	cld    
  80094b:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80094d:	eb 05                	jmp    800954 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  80094f:	89 c7                	mov    %eax,%edi
  800951:	fc                   	cld    
  800952:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800954:	5e                   	pop    %esi
  800955:	5f                   	pop    %edi
  800956:	5d                   	pop    %ebp
  800957:	c3                   	ret    

00800958 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800958:	55                   	push   %ebp
  800959:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  80095b:	ff 75 10             	pushl  0x10(%ebp)
  80095e:	ff 75 0c             	pushl  0xc(%ebp)
  800961:	ff 75 08             	pushl  0x8(%ebp)
  800964:	e8 89 ff ff ff       	call   8008f2 <memmove>
}
  800969:	c9                   	leave  
  80096a:	c3                   	ret    

0080096b <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  80096b:	55                   	push   %ebp
  80096c:	89 e5                	mov    %esp,%ebp
  80096e:	56                   	push   %esi
  80096f:	53                   	push   %ebx
  800970:	8b 45 08             	mov    0x8(%ebp),%eax
  800973:	8b 55 0c             	mov    0xc(%ebp),%edx
  800976:	89 c6                	mov    %eax,%esi
  800978:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80097b:	eb 14                	jmp    800991 <memcmp+0x26>
		if (*s1 != *s2)
  80097d:	8a 08                	mov    (%eax),%cl
  80097f:	8a 1a                	mov    (%edx),%bl
  800981:	38 d9                	cmp    %bl,%cl
  800983:	74 0a                	je     80098f <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800985:	0f b6 c1             	movzbl %cl,%eax
  800988:	0f b6 db             	movzbl %bl,%ebx
  80098b:	29 d8                	sub    %ebx,%eax
  80098d:	eb 0b                	jmp    80099a <memcmp+0x2f>
		s1++, s2++;
  80098f:	40                   	inc    %eax
  800990:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800991:	39 f0                	cmp    %esi,%eax
  800993:	75 e8                	jne    80097d <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800995:	b8 00 00 00 00       	mov    $0x0,%eax
}
  80099a:	5b                   	pop    %ebx
  80099b:	5e                   	pop    %esi
  80099c:	5d                   	pop    %ebp
  80099d:	c3                   	ret    

0080099e <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  80099e:	55                   	push   %ebp
  80099f:	89 e5                	mov    %esp,%ebp
  8009a1:	53                   	push   %ebx
  8009a2:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8009a5:	89 c1                	mov    %eax,%ecx
  8009a7:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8009aa:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009ae:	eb 08                	jmp    8009b8 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8009b0:	0f b6 10             	movzbl (%eax),%edx
  8009b3:	39 da                	cmp    %ebx,%edx
  8009b5:	74 05                	je     8009bc <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009b7:	40                   	inc    %eax
  8009b8:	39 c8                	cmp    %ecx,%eax
  8009ba:	72 f4                	jb     8009b0 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8009bc:	5b                   	pop    %ebx
  8009bd:	5d                   	pop    %ebp
  8009be:	c3                   	ret    

008009bf <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8009bf:	55                   	push   %ebp
  8009c0:	89 e5                	mov    %esp,%ebp
  8009c2:	57                   	push   %edi
  8009c3:	56                   	push   %esi
  8009c4:	53                   	push   %ebx
  8009c5:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009c8:	eb 01                	jmp    8009cb <strtol+0xc>
		s++;
  8009ca:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009cb:	8a 01                	mov    (%ecx),%al
  8009cd:	3c 20                	cmp    $0x20,%al
  8009cf:	74 f9                	je     8009ca <strtol+0xb>
  8009d1:	3c 09                	cmp    $0x9,%al
  8009d3:	74 f5                	je     8009ca <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009d5:	3c 2b                	cmp    $0x2b,%al
  8009d7:	75 08                	jne    8009e1 <strtol+0x22>
		s++;
  8009d9:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009da:	bf 00 00 00 00       	mov    $0x0,%edi
  8009df:	eb 11                	jmp    8009f2 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009e1:	3c 2d                	cmp    $0x2d,%al
  8009e3:	75 08                	jne    8009ed <strtol+0x2e>
		s++, neg = 1;
  8009e5:	41                   	inc    %ecx
  8009e6:	bf 01 00 00 00       	mov    $0x1,%edi
  8009eb:	eb 05                	jmp    8009f2 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009ed:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009f2:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009f6:	0f 84 87 00 00 00    	je     800a83 <strtol+0xc4>
  8009fc:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800a00:	75 27                	jne    800a29 <strtol+0x6a>
  800a02:	80 39 30             	cmpb   $0x30,(%ecx)
  800a05:	75 22                	jne    800a29 <strtol+0x6a>
  800a07:	e9 88 00 00 00       	jmp    800a94 <strtol+0xd5>
		s += 2, base = 16;
  800a0c:	83 c1 02             	add    $0x2,%ecx
  800a0f:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a16:	eb 11                	jmp    800a29 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a18:	41                   	inc    %ecx
  800a19:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a20:	eb 07                	jmp    800a29 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a22:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a29:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a2e:	8a 11                	mov    (%ecx),%dl
  800a30:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a33:	80 fb 09             	cmp    $0x9,%bl
  800a36:	77 08                	ja     800a40 <strtol+0x81>
			dig = *s - '0';
  800a38:	0f be d2             	movsbl %dl,%edx
  800a3b:	83 ea 30             	sub    $0x30,%edx
  800a3e:	eb 22                	jmp    800a62 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a40:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a43:	89 f3                	mov    %esi,%ebx
  800a45:	80 fb 19             	cmp    $0x19,%bl
  800a48:	77 08                	ja     800a52 <strtol+0x93>
			dig = *s - 'a' + 10;
  800a4a:	0f be d2             	movsbl %dl,%edx
  800a4d:	83 ea 57             	sub    $0x57,%edx
  800a50:	eb 10                	jmp    800a62 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a52:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a55:	89 f3                	mov    %esi,%ebx
  800a57:	80 fb 19             	cmp    $0x19,%bl
  800a5a:	77 14                	ja     800a70 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a5c:	0f be d2             	movsbl %dl,%edx
  800a5f:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a62:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a65:	7d 09                	jge    800a70 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a67:	41                   	inc    %ecx
  800a68:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a6c:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a6e:	eb be                	jmp    800a2e <strtol+0x6f>

	if (endptr)
  800a70:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a74:	74 05                	je     800a7b <strtol+0xbc>
		*endptr = (char *) s;
  800a76:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a79:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a7b:	85 ff                	test   %edi,%edi
  800a7d:	74 21                	je     800aa0 <strtol+0xe1>
  800a7f:	f7 d8                	neg    %eax
  800a81:	eb 1d                	jmp    800aa0 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a83:	80 39 30             	cmpb   $0x30,(%ecx)
  800a86:	75 9a                	jne    800a22 <strtol+0x63>
  800a88:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a8c:	0f 84 7a ff ff ff    	je     800a0c <strtol+0x4d>
  800a92:	eb 84                	jmp    800a18 <strtol+0x59>
  800a94:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a98:	0f 84 6e ff ff ff    	je     800a0c <strtol+0x4d>
  800a9e:	eb 89                	jmp    800a29 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800aa0:	5b                   	pop    %ebx
  800aa1:	5e                   	pop    %esi
  800aa2:	5f                   	pop    %edi
  800aa3:	5d                   	pop    %ebp
  800aa4:	c3                   	ret    
  800aa5:	66 90                	xchg   %ax,%ax
  800aa7:	90                   	nop

00800aa8 <__udivdi3>:
  800aa8:	55                   	push   %ebp
  800aa9:	57                   	push   %edi
  800aaa:	56                   	push   %esi
  800aab:	53                   	push   %ebx
  800aac:	83 ec 1c             	sub    $0x1c,%esp
  800aaf:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ab3:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800ab7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800abb:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800abf:	89 ca                	mov    %ecx,%edx
  800ac1:	89 f8                	mov    %edi,%eax
  800ac3:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800ac7:	85 f6                	test   %esi,%esi
  800ac9:	75 2d                	jne    800af8 <__udivdi3+0x50>
  800acb:	39 cf                	cmp    %ecx,%edi
  800acd:	77 65                	ja     800b34 <__udivdi3+0x8c>
  800acf:	89 fd                	mov    %edi,%ebp
  800ad1:	85 ff                	test   %edi,%edi
  800ad3:	75 0b                	jne    800ae0 <__udivdi3+0x38>
  800ad5:	b8 01 00 00 00       	mov    $0x1,%eax
  800ada:	31 d2                	xor    %edx,%edx
  800adc:	f7 f7                	div    %edi
  800ade:	89 c5                	mov    %eax,%ebp
  800ae0:	31 d2                	xor    %edx,%edx
  800ae2:	89 c8                	mov    %ecx,%eax
  800ae4:	f7 f5                	div    %ebp
  800ae6:	89 c1                	mov    %eax,%ecx
  800ae8:	89 d8                	mov    %ebx,%eax
  800aea:	f7 f5                	div    %ebp
  800aec:	89 cf                	mov    %ecx,%edi
  800aee:	89 fa                	mov    %edi,%edx
  800af0:	83 c4 1c             	add    $0x1c,%esp
  800af3:	5b                   	pop    %ebx
  800af4:	5e                   	pop    %esi
  800af5:	5f                   	pop    %edi
  800af6:	5d                   	pop    %ebp
  800af7:	c3                   	ret    
  800af8:	39 ce                	cmp    %ecx,%esi
  800afa:	77 28                	ja     800b24 <__udivdi3+0x7c>
  800afc:	0f bd fe             	bsr    %esi,%edi
  800aff:	83 f7 1f             	xor    $0x1f,%edi
  800b02:	75 40                	jne    800b44 <__udivdi3+0x9c>
  800b04:	39 ce                	cmp    %ecx,%esi
  800b06:	72 0a                	jb     800b12 <__udivdi3+0x6a>
  800b08:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b0c:	0f 87 9e 00 00 00    	ja     800bb0 <__udivdi3+0x108>
  800b12:	b8 01 00 00 00       	mov    $0x1,%eax
  800b17:	89 fa                	mov    %edi,%edx
  800b19:	83 c4 1c             	add    $0x1c,%esp
  800b1c:	5b                   	pop    %ebx
  800b1d:	5e                   	pop    %esi
  800b1e:	5f                   	pop    %edi
  800b1f:	5d                   	pop    %ebp
  800b20:	c3                   	ret    
  800b21:	8d 76 00             	lea    0x0(%esi),%esi
  800b24:	31 ff                	xor    %edi,%edi
  800b26:	31 c0                	xor    %eax,%eax
  800b28:	89 fa                	mov    %edi,%edx
  800b2a:	83 c4 1c             	add    $0x1c,%esp
  800b2d:	5b                   	pop    %ebx
  800b2e:	5e                   	pop    %esi
  800b2f:	5f                   	pop    %edi
  800b30:	5d                   	pop    %ebp
  800b31:	c3                   	ret    
  800b32:	66 90                	xchg   %ax,%ax
  800b34:	89 d8                	mov    %ebx,%eax
  800b36:	f7 f7                	div    %edi
  800b38:	31 ff                	xor    %edi,%edi
  800b3a:	89 fa                	mov    %edi,%edx
  800b3c:	83 c4 1c             	add    $0x1c,%esp
  800b3f:	5b                   	pop    %ebx
  800b40:	5e                   	pop    %esi
  800b41:	5f                   	pop    %edi
  800b42:	5d                   	pop    %ebp
  800b43:	c3                   	ret    
  800b44:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b49:	89 eb                	mov    %ebp,%ebx
  800b4b:	29 fb                	sub    %edi,%ebx
  800b4d:	89 f9                	mov    %edi,%ecx
  800b4f:	d3 e6                	shl    %cl,%esi
  800b51:	89 c5                	mov    %eax,%ebp
  800b53:	88 d9                	mov    %bl,%cl
  800b55:	d3 ed                	shr    %cl,%ebp
  800b57:	89 e9                	mov    %ebp,%ecx
  800b59:	09 f1                	or     %esi,%ecx
  800b5b:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b5f:	89 f9                	mov    %edi,%ecx
  800b61:	d3 e0                	shl    %cl,%eax
  800b63:	89 c5                	mov    %eax,%ebp
  800b65:	89 d6                	mov    %edx,%esi
  800b67:	88 d9                	mov    %bl,%cl
  800b69:	d3 ee                	shr    %cl,%esi
  800b6b:	89 f9                	mov    %edi,%ecx
  800b6d:	d3 e2                	shl    %cl,%edx
  800b6f:	8b 44 24 08          	mov    0x8(%esp),%eax
  800b73:	88 d9                	mov    %bl,%cl
  800b75:	d3 e8                	shr    %cl,%eax
  800b77:	09 c2                	or     %eax,%edx
  800b79:	89 d0                	mov    %edx,%eax
  800b7b:	89 f2                	mov    %esi,%edx
  800b7d:	f7 74 24 0c          	divl   0xc(%esp)
  800b81:	89 d6                	mov    %edx,%esi
  800b83:	89 c3                	mov    %eax,%ebx
  800b85:	f7 e5                	mul    %ebp
  800b87:	39 d6                	cmp    %edx,%esi
  800b89:	72 19                	jb     800ba4 <__udivdi3+0xfc>
  800b8b:	74 0b                	je     800b98 <__udivdi3+0xf0>
  800b8d:	89 d8                	mov    %ebx,%eax
  800b8f:	31 ff                	xor    %edi,%edi
  800b91:	e9 58 ff ff ff       	jmp    800aee <__udivdi3+0x46>
  800b96:	66 90                	xchg   %ax,%ax
  800b98:	8b 54 24 08          	mov    0x8(%esp),%edx
  800b9c:	89 f9                	mov    %edi,%ecx
  800b9e:	d3 e2                	shl    %cl,%edx
  800ba0:	39 c2                	cmp    %eax,%edx
  800ba2:	73 e9                	jae    800b8d <__udivdi3+0xe5>
  800ba4:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800ba7:	31 ff                	xor    %edi,%edi
  800ba9:	e9 40 ff ff ff       	jmp    800aee <__udivdi3+0x46>
  800bae:	66 90                	xchg   %ax,%ax
  800bb0:	31 c0                	xor    %eax,%eax
  800bb2:	e9 37 ff ff ff       	jmp    800aee <__udivdi3+0x46>
  800bb7:	90                   	nop

00800bb8 <__umoddi3>:
  800bb8:	55                   	push   %ebp
  800bb9:	57                   	push   %edi
  800bba:	56                   	push   %esi
  800bbb:	53                   	push   %ebx
  800bbc:	83 ec 1c             	sub    $0x1c,%esp
  800bbf:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bc3:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bc7:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800bcb:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800bcf:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800bd3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800bd7:	89 f3                	mov    %esi,%ebx
  800bd9:	89 fa                	mov    %edi,%edx
  800bdb:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800bdf:	89 34 24             	mov    %esi,(%esp)
  800be2:	85 c0                	test   %eax,%eax
  800be4:	75 1a                	jne    800c00 <__umoddi3+0x48>
  800be6:	39 f7                	cmp    %esi,%edi
  800be8:	0f 86 a2 00 00 00    	jbe    800c90 <__umoddi3+0xd8>
  800bee:	89 c8                	mov    %ecx,%eax
  800bf0:	89 f2                	mov    %esi,%edx
  800bf2:	f7 f7                	div    %edi
  800bf4:	89 d0                	mov    %edx,%eax
  800bf6:	31 d2                	xor    %edx,%edx
  800bf8:	83 c4 1c             	add    $0x1c,%esp
  800bfb:	5b                   	pop    %ebx
  800bfc:	5e                   	pop    %esi
  800bfd:	5f                   	pop    %edi
  800bfe:	5d                   	pop    %ebp
  800bff:	c3                   	ret    
  800c00:	39 f0                	cmp    %esi,%eax
  800c02:	0f 87 ac 00 00 00    	ja     800cb4 <__umoddi3+0xfc>
  800c08:	0f bd e8             	bsr    %eax,%ebp
  800c0b:	83 f5 1f             	xor    $0x1f,%ebp
  800c0e:	0f 84 ac 00 00 00    	je     800cc0 <__umoddi3+0x108>
  800c14:	bf 20 00 00 00       	mov    $0x20,%edi
  800c19:	29 ef                	sub    %ebp,%edi
  800c1b:	89 fe                	mov    %edi,%esi
  800c1d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c21:	89 e9                	mov    %ebp,%ecx
  800c23:	d3 e0                	shl    %cl,%eax
  800c25:	89 d7                	mov    %edx,%edi
  800c27:	89 f1                	mov    %esi,%ecx
  800c29:	d3 ef                	shr    %cl,%edi
  800c2b:	09 c7                	or     %eax,%edi
  800c2d:	89 e9                	mov    %ebp,%ecx
  800c2f:	d3 e2                	shl    %cl,%edx
  800c31:	89 14 24             	mov    %edx,(%esp)
  800c34:	89 d8                	mov    %ebx,%eax
  800c36:	d3 e0                	shl    %cl,%eax
  800c38:	89 c2                	mov    %eax,%edx
  800c3a:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c3e:	d3 e0                	shl    %cl,%eax
  800c40:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c44:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c48:	89 f1                	mov    %esi,%ecx
  800c4a:	d3 e8                	shr    %cl,%eax
  800c4c:	09 d0                	or     %edx,%eax
  800c4e:	d3 eb                	shr    %cl,%ebx
  800c50:	89 da                	mov    %ebx,%edx
  800c52:	f7 f7                	div    %edi
  800c54:	89 d3                	mov    %edx,%ebx
  800c56:	f7 24 24             	mull   (%esp)
  800c59:	89 c6                	mov    %eax,%esi
  800c5b:	89 d1                	mov    %edx,%ecx
  800c5d:	39 d3                	cmp    %edx,%ebx
  800c5f:	0f 82 87 00 00 00    	jb     800cec <__umoddi3+0x134>
  800c65:	0f 84 91 00 00 00    	je     800cfc <__umoddi3+0x144>
  800c6b:	8b 54 24 04          	mov    0x4(%esp),%edx
  800c6f:	29 f2                	sub    %esi,%edx
  800c71:	19 cb                	sbb    %ecx,%ebx
  800c73:	89 d8                	mov    %ebx,%eax
  800c75:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800c79:	d3 e0                	shl    %cl,%eax
  800c7b:	89 e9                	mov    %ebp,%ecx
  800c7d:	d3 ea                	shr    %cl,%edx
  800c7f:	09 d0                	or     %edx,%eax
  800c81:	89 e9                	mov    %ebp,%ecx
  800c83:	d3 eb                	shr    %cl,%ebx
  800c85:	89 da                	mov    %ebx,%edx
  800c87:	83 c4 1c             	add    $0x1c,%esp
  800c8a:	5b                   	pop    %ebx
  800c8b:	5e                   	pop    %esi
  800c8c:	5f                   	pop    %edi
  800c8d:	5d                   	pop    %ebp
  800c8e:	c3                   	ret    
  800c8f:	90                   	nop
  800c90:	89 fd                	mov    %edi,%ebp
  800c92:	85 ff                	test   %edi,%edi
  800c94:	75 0b                	jne    800ca1 <__umoddi3+0xe9>
  800c96:	b8 01 00 00 00       	mov    $0x1,%eax
  800c9b:	31 d2                	xor    %edx,%edx
  800c9d:	f7 f7                	div    %edi
  800c9f:	89 c5                	mov    %eax,%ebp
  800ca1:	89 f0                	mov    %esi,%eax
  800ca3:	31 d2                	xor    %edx,%edx
  800ca5:	f7 f5                	div    %ebp
  800ca7:	89 c8                	mov    %ecx,%eax
  800ca9:	f7 f5                	div    %ebp
  800cab:	89 d0                	mov    %edx,%eax
  800cad:	e9 44 ff ff ff       	jmp    800bf6 <__umoddi3+0x3e>
  800cb2:	66 90                	xchg   %ax,%ax
  800cb4:	89 c8                	mov    %ecx,%eax
  800cb6:	89 f2                	mov    %esi,%edx
  800cb8:	83 c4 1c             	add    $0x1c,%esp
  800cbb:	5b                   	pop    %ebx
  800cbc:	5e                   	pop    %esi
  800cbd:	5f                   	pop    %edi
  800cbe:	5d                   	pop    %ebp
  800cbf:	c3                   	ret    
  800cc0:	3b 04 24             	cmp    (%esp),%eax
  800cc3:	72 06                	jb     800ccb <__umoddi3+0x113>
  800cc5:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800cc9:	77 0f                	ja     800cda <__umoddi3+0x122>
  800ccb:	89 f2                	mov    %esi,%edx
  800ccd:	29 f9                	sub    %edi,%ecx
  800ccf:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800cd3:	89 14 24             	mov    %edx,(%esp)
  800cd6:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800cda:	8b 44 24 04          	mov    0x4(%esp),%eax
  800cde:	8b 14 24             	mov    (%esp),%edx
  800ce1:	83 c4 1c             	add    $0x1c,%esp
  800ce4:	5b                   	pop    %ebx
  800ce5:	5e                   	pop    %esi
  800ce6:	5f                   	pop    %edi
  800ce7:	5d                   	pop    %ebp
  800ce8:	c3                   	ret    
  800ce9:	8d 76 00             	lea    0x0(%esi),%esi
  800cec:	2b 04 24             	sub    (%esp),%eax
  800cef:	19 fa                	sbb    %edi,%edx
  800cf1:	89 d1                	mov    %edx,%ecx
  800cf3:	89 c6                	mov    %eax,%esi
  800cf5:	e9 71 ff ff ff       	jmp    800c6b <__umoddi3+0xb3>
  800cfa:	66 90                	xchg   %ax,%ax
  800cfc:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d00:	72 ea                	jb     800cec <__umoddi3+0x134>
  800d02:	89 d9                	mov    %ebx,%ecx
  800d04:	e9 62 ff ff ff       	jmp    800c6b <__umoddi3+0xb3>
