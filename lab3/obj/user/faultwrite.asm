
obj/user/faultwrite:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 11 00 00 00       	call   800042 <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
	*(unsigned*)0 = 0;
  800036:	c7 05 00 00 00 00 00 	movl   $0x0,0x0
  80003d:	00 00 00 
}
  800040:	5d                   	pop    %ebp
  800041:	c3                   	ret    

00800042 <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  800042:	55                   	push   %ebp
  800043:	89 e5                	mov    %esp,%ebp
  800045:	56                   	push   %esi
  800046:	53                   	push   %ebx
  800047:	8b 5d 08             	mov    0x8(%ebp),%ebx
  80004a:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  80004d:	e8 cb 00 00 00       	call   80011d <sys_getenvid>
  800052:	25 ff 03 00 00       	and    $0x3ff,%eax
  800057:	8d 14 00             	lea    (%eax,%eax,1),%edx
  80005a:	01 d0                	add    %edx,%eax
  80005c:	c1 e0 05             	shl    $0x5,%eax
  80005f:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800064:	a3 04 10 80 00       	mov    %eax,0x801004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800069:	85 db                	test   %ebx,%ebx
  80006b:	7e 07                	jle    800074 <libmain+0x32>
		binaryname = argv[0];
  80006d:	8b 06                	mov    (%esi),%eax
  80006f:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  800074:	83 ec 08             	sub    $0x8,%esp
  800077:	56                   	push   %esi
  800078:	53                   	push   %ebx
  800079:	e8 b5 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80007e:	e8 0a 00 00 00       	call   80008d <exit>
}
  800083:	83 c4 10             	add    $0x10,%esp
  800086:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800089:	5b                   	pop    %ebx
  80008a:	5e                   	pop    %esi
  80008b:	5d                   	pop    %ebp
  80008c:	c3                   	ret    

0080008d <exit>:

#include <inc/lib.h>

void
exit(void)
{
  80008d:	55                   	push   %ebp
  80008e:	89 e5                	mov    %esp,%ebp
  800090:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  800093:	6a 00                	push   $0x0
  800095:	e8 42 00 00 00       	call   8000dc <sys_env_destroy>
}
  80009a:	83 c4 10             	add    $0x10,%esp
  80009d:	c9                   	leave  
  80009e:	c3                   	ret    

0080009f <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  80009f:	55                   	push   %ebp
  8000a0:	89 e5                	mov    %esp,%ebp
  8000a2:	57                   	push   %edi
  8000a3:	56                   	push   %esi
  8000a4:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000a5:	b8 00 00 00 00       	mov    $0x0,%eax
  8000aa:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8000ad:	8b 55 08             	mov    0x8(%ebp),%edx
  8000b0:	89 c3                	mov    %eax,%ebx
  8000b2:	89 c7                	mov    %eax,%edi
  8000b4:	89 c6                	mov    %eax,%esi
  8000b6:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8000b8:	5b                   	pop    %ebx
  8000b9:	5e                   	pop    %esi
  8000ba:	5f                   	pop    %edi
  8000bb:	5d                   	pop    %ebp
  8000bc:	c3                   	ret    

008000bd <sys_cgetc>:

int
sys_cgetc(void)
{
  8000bd:	55                   	push   %ebp
  8000be:	89 e5                	mov    %esp,%ebp
  8000c0:	57                   	push   %edi
  8000c1:	56                   	push   %esi
  8000c2:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000c3:	ba 00 00 00 00       	mov    $0x0,%edx
  8000c8:	b8 01 00 00 00       	mov    $0x1,%eax
  8000cd:	89 d1                	mov    %edx,%ecx
  8000cf:	89 d3                	mov    %edx,%ebx
  8000d1:	89 d7                	mov    %edx,%edi
  8000d3:	89 d6                	mov    %edx,%esi
  8000d5:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  8000d7:	5b                   	pop    %ebx
  8000d8:	5e                   	pop    %esi
  8000d9:	5f                   	pop    %edi
  8000da:	5d                   	pop    %ebp
  8000db:	c3                   	ret    

008000dc <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  8000dc:	55                   	push   %ebp
  8000dd:	89 e5                	mov    %esp,%ebp
  8000df:	57                   	push   %edi
  8000e0:	56                   	push   %esi
  8000e1:	53                   	push   %ebx
  8000e2:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8000e5:	b9 00 00 00 00       	mov    $0x0,%ecx
  8000ea:	b8 03 00 00 00       	mov    $0x3,%eax
  8000ef:	8b 55 08             	mov    0x8(%ebp),%edx
  8000f2:	89 cb                	mov    %ecx,%ebx
  8000f4:	89 cf                	mov    %ecx,%edi
  8000f6:	89 ce                	mov    %ecx,%esi
  8000f8:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  8000fa:	85 c0                	test   %eax,%eax
  8000fc:	7e 17                	jle    800115 <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  8000fe:	83 ec 0c             	sub    $0xc,%esp
  800101:	50                   	push   %eax
  800102:	6a 03                	push   $0x3
  800104:	68 0e 0d 80 00       	push   $0x800d0e
  800109:	6a 23                	push   $0x23
  80010b:	68 2b 0d 80 00       	push   $0x800d2b
  800110:	e8 27 00 00 00       	call   80013c <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800115:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800118:	5b                   	pop    %ebx
  800119:	5e                   	pop    %esi
  80011a:	5f                   	pop    %edi
  80011b:	5d                   	pop    %ebp
  80011c:	c3                   	ret    

0080011d <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  80011d:	55                   	push   %ebp
  80011e:	89 e5                	mov    %esp,%ebp
  800120:	57                   	push   %edi
  800121:	56                   	push   %esi
  800122:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800123:	ba 00 00 00 00       	mov    $0x0,%edx
  800128:	b8 02 00 00 00       	mov    $0x2,%eax
  80012d:	89 d1                	mov    %edx,%ecx
  80012f:	89 d3                	mov    %edx,%ebx
  800131:	89 d7                	mov    %edx,%edi
  800133:	89 d6                	mov    %edx,%esi
  800135:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800137:	5b                   	pop    %ebx
  800138:	5e                   	pop    %esi
  800139:	5f                   	pop    %edi
  80013a:	5d                   	pop    %ebp
  80013b:	c3                   	ret    

0080013c <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  80013c:	55                   	push   %ebp
  80013d:	89 e5                	mov    %esp,%ebp
  80013f:	56                   	push   %esi
  800140:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800141:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800144:	8b 35 00 10 80 00    	mov    0x801000,%esi
  80014a:	e8 ce ff ff ff       	call   80011d <sys_getenvid>
  80014f:	83 ec 0c             	sub    $0xc,%esp
  800152:	ff 75 0c             	pushl  0xc(%ebp)
  800155:	ff 75 08             	pushl  0x8(%ebp)
  800158:	56                   	push   %esi
  800159:	50                   	push   %eax
  80015a:	68 3c 0d 80 00       	push   $0x800d3c
  80015f:	e8 b0 00 00 00       	call   800214 <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800164:	83 c4 18             	add    $0x18,%esp
  800167:	53                   	push   %ebx
  800168:	ff 75 10             	pushl  0x10(%ebp)
  80016b:	e8 53 00 00 00       	call   8001c3 <vcprintf>
	cprintf("\n");
  800170:	c7 04 24 60 0d 80 00 	movl   $0x800d60,(%esp)
  800177:	e8 98 00 00 00       	call   800214 <cprintf>
  80017c:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  80017f:	cc                   	int3   
  800180:	eb fd                	jmp    80017f <_panic+0x43>

00800182 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800182:	55                   	push   %ebp
  800183:	89 e5                	mov    %esp,%ebp
  800185:	53                   	push   %ebx
  800186:	83 ec 04             	sub    $0x4,%esp
  800189:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  80018c:	8b 13                	mov    (%ebx),%edx
  80018e:	8d 42 01             	lea    0x1(%edx),%eax
  800191:	89 03                	mov    %eax,(%ebx)
  800193:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800196:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  80019a:	3d ff 00 00 00       	cmp    $0xff,%eax
  80019f:	75 1a                	jne    8001bb <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8001a1:	83 ec 08             	sub    $0x8,%esp
  8001a4:	68 ff 00 00 00       	push   $0xff
  8001a9:	8d 43 08             	lea    0x8(%ebx),%eax
  8001ac:	50                   	push   %eax
  8001ad:	e8 ed fe ff ff       	call   80009f <sys_cputs>
		b->idx = 0;
  8001b2:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8001b8:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8001bb:	ff 43 04             	incl   0x4(%ebx)
}
  8001be:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001c1:	c9                   	leave  
  8001c2:	c3                   	ret    

008001c3 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8001c3:	55                   	push   %ebp
  8001c4:	89 e5                	mov    %esp,%ebp
  8001c6:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8001cc:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8001d3:	00 00 00 
	b.cnt = 0;
  8001d6:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001dd:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001e0:	ff 75 0c             	pushl  0xc(%ebp)
  8001e3:	ff 75 08             	pushl  0x8(%ebp)
  8001e6:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001ec:	50                   	push   %eax
  8001ed:	68 82 01 80 00       	push   $0x800182
  8001f2:	e8 51 01 00 00       	call   800348 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001f7:	83 c4 08             	add    $0x8,%esp
  8001fa:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800200:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  800206:	50                   	push   %eax
  800207:	e8 93 fe ff ff       	call   80009f <sys_cputs>

	return b.cnt;
}
  80020c:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  800212:	c9                   	leave  
  800213:	c3                   	ret    

00800214 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800214:	55                   	push   %ebp
  800215:	89 e5                	mov    %esp,%ebp
  800217:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80021a:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  80021d:	50                   	push   %eax
  80021e:	ff 75 08             	pushl  0x8(%ebp)
  800221:	e8 9d ff ff ff       	call   8001c3 <vcprintf>
	va_end(ap);

	return cnt;
}
  800226:	c9                   	leave  
  800227:	c3                   	ret    

00800228 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800228:	55                   	push   %ebp
  800229:	89 e5                	mov    %esp,%ebp
  80022b:	57                   	push   %edi
  80022c:	56                   	push   %esi
  80022d:	53                   	push   %ebx
  80022e:	83 ec 1c             	sub    $0x1c,%esp
  800231:	89 c7                	mov    %eax,%edi
  800233:	89 d6                	mov    %edx,%esi
  800235:	8b 45 08             	mov    0x8(%ebp),%eax
  800238:	8b 55 0c             	mov    0xc(%ebp),%edx
  80023b:	89 45 d8             	mov    %eax,-0x28(%ebp)
  80023e:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800241:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800244:	bb 00 00 00 00       	mov    $0x0,%ebx
  800249:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  80024c:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  80024f:	39 d3                	cmp    %edx,%ebx
  800251:	72 05                	jb     800258 <printnum+0x30>
  800253:	39 45 10             	cmp    %eax,0x10(%ebp)
  800256:	77 45                	ja     80029d <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800258:	83 ec 0c             	sub    $0xc,%esp
  80025b:	ff 75 18             	pushl  0x18(%ebp)
  80025e:	8b 45 14             	mov    0x14(%ebp),%eax
  800261:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800264:	53                   	push   %ebx
  800265:	ff 75 10             	pushl  0x10(%ebp)
  800268:	83 ec 08             	sub    $0x8,%esp
  80026b:	ff 75 e4             	pushl  -0x1c(%ebp)
  80026e:	ff 75 e0             	pushl  -0x20(%ebp)
  800271:	ff 75 dc             	pushl  -0x24(%ebp)
  800274:	ff 75 d8             	pushl  -0x28(%ebp)
  800277:	e8 24 08 00 00       	call   800aa0 <__udivdi3>
  80027c:	83 c4 18             	add    $0x18,%esp
  80027f:	52                   	push   %edx
  800280:	50                   	push   %eax
  800281:	89 f2                	mov    %esi,%edx
  800283:	89 f8                	mov    %edi,%eax
  800285:	e8 9e ff ff ff       	call   800228 <printnum>
  80028a:	83 c4 20             	add    $0x20,%esp
  80028d:	eb 16                	jmp    8002a5 <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80028f:	83 ec 08             	sub    $0x8,%esp
  800292:	56                   	push   %esi
  800293:	ff 75 18             	pushl  0x18(%ebp)
  800296:	ff d7                	call   *%edi
  800298:	83 c4 10             	add    $0x10,%esp
  80029b:	eb 03                	jmp    8002a0 <printnum+0x78>
  80029d:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8002a0:	4b                   	dec    %ebx
  8002a1:	85 db                	test   %ebx,%ebx
  8002a3:	7f ea                	jg     80028f <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8002a5:	83 ec 08             	sub    $0x8,%esp
  8002a8:	56                   	push   %esi
  8002a9:	83 ec 04             	sub    $0x4,%esp
  8002ac:	ff 75 e4             	pushl  -0x1c(%ebp)
  8002af:	ff 75 e0             	pushl  -0x20(%ebp)
  8002b2:	ff 75 dc             	pushl  -0x24(%ebp)
  8002b5:	ff 75 d8             	pushl  -0x28(%ebp)
  8002b8:	e8 f3 08 00 00       	call   800bb0 <__umoddi3>
  8002bd:	83 c4 14             	add    $0x14,%esp
  8002c0:	0f be 80 62 0d 80 00 	movsbl 0x800d62(%eax),%eax
  8002c7:	50                   	push   %eax
  8002c8:	ff d7                	call   *%edi
}
  8002ca:	83 c4 10             	add    $0x10,%esp
  8002cd:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8002d0:	5b                   	pop    %ebx
  8002d1:	5e                   	pop    %esi
  8002d2:	5f                   	pop    %edi
  8002d3:	5d                   	pop    %ebp
  8002d4:	c3                   	ret    

008002d5 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002d5:	55                   	push   %ebp
  8002d6:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002d8:	83 fa 01             	cmp    $0x1,%edx
  8002db:	7e 0e                	jle    8002eb <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  8002dd:	8b 10                	mov    (%eax),%edx
  8002df:	8d 4a 08             	lea    0x8(%edx),%ecx
  8002e2:	89 08                	mov    %ecx,(%eax)
  8002e4:	8b 02                	mov    (%edx),%eax
  8002e6:	8b 52 04             	mov    0x4(%edx),%edx
  8002e9:	eb 22                	jmp    80030d <getuint+0x38>
	else if (lflag)
  8002eb:	85 d2                	test   %edx,%edx
  8002ed:	74 10                	je     8002ff <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  8002ef:	8b 10                	mov    (%eax),%edx
  8002f1:	8d 4a 04             	lea    0x4(%edx),%ecx
  8002f4:	89 08                	mov    %ecx,(%eax)
  8002f6:	8b 02                	mov    (%edx),%eax
  8002f8:	ba 00 00 00 00       	mov    $0x0,%edx
  8002fd:	eb 0e                	jmp    80030d <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  8002ff:	8b 10                	mov    (%eax),%edx
  800301:	8d 4a 04             	lea    0x4(%edx),%ecx
  800304:	89 08                	mov    %ecx,(%eax)
  800306:	8b 02                	mov    (%edx),%eax
  800308:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80030d:	5d                   	pop    %ebp
  80030e:	c3                   	ret    

0080030f <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80030f:	55                   	push   %ebp
  800310:	89 e5                	mov    %esp,%ebp
  800312:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  800315:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800318:	8b 10                	mov    (%eax),%edx
  80031a:	3b 50 04             	cmp    0x4(%eax),%edx
  80031d:	73 0a                	jae    800329 <sprintputch+0x1a>
		*b->buf++ = ch;
  80031f:	8d 4a 01             	lea    0x1(%edx),%ecx
  800322:	89 08                	mov    %ecx,(%eax)
  800324:	8b 45 08             	mov    0x8(%ebp),%eax
  800327:	88 02                	mov    %al,(%edx)
}
  800329:	5d                   	pop    %ebp
  80032a:	c3                   	ret    

0080032b <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80032b:	55                   	push   %ebp
  80032c:	89 e5                	mov    %esp,%ebp
  80032e:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  800331:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  800334:	50                   	push   %eax
  800335:	ff 75 10             	pushl  0x10(%ebp)
  800338:	ff 75 0c             	pushl  0xc(%ebp)
  80033b:	ff 75 08             	pushl  0x8(%ebp)
  80033e:	e8 05 00 00 00       	call   800348 <vprintfmt>
	va_end(ap);
}
  800343:	83 c4 10             	add    $0x10,%esp
  800346:	c9                   	leave  
  800347:	c3                   	ret    

00800348 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800348:	55                   	push   %ebp
  800349:	89 e5                	mov    %esp,%ebp
  80034b:	57                   	push   %edi
  80034c:	56                   	push   %esi
  80034d:	53                   	push   %ebx
  80034e:	83 ec 2c             	sub    $0x2c,%esp
  800351:	8b 75 08             	mov    0x8(%ebp),%esi
  800354:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800357:	8b 7d 10             	mov    0x10(%ebp),%edi
  80035a:	eb 12                	jmp    80036e <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  80035c:	85 c0                	test   %eax,%eax
  80035e:	0f 84 68 03 00 00    	je     8006cc <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  800364:	83 ec 08             	sub    $0x8,%esp
  800367:	53                   	push   %ebx
  800368:	50                   	push   %eax
  800369:	ff d6                	call   *%esi
  80036b:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80036e:	47                   	inc    %edi
  80036f:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  800373:	83 f8 25             	cmp    $0x25,%eax
  800376:	75 e4                	jne    80035c <vprintfmt+0x14>
  800378:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  80037c:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  800383:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  80038a:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  800391:	ba 00 00 00 00       	mov    $0x0,%edx
  800396:	eb 07                	jmp    80039f <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800398:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  80039b:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80039f:	8d 47 01             	lea    0x1(%edi),%eax
  8003a2:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8003a5:	0f b6 0f             	movzbl (%edi),%ecx
  8003a8:	8a 07                	mov    (%edi),%al
  8003aa:	83 e8 23             	sub    $0x23,%eax
  8003ad:	3c 55                	cmp    $0x55,%al
  8003af:	0f 87 fe 02 00 00    	ja     8006b3 <vprintfmt+0x36b>
  8003b5:	0f b6 c0             	movzbl %al,%eax
  8003b8:	ff 24 85 f0 0d 80 00 	jmp    *0x800df0(,%eax,4)
  8003bf:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003c2:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8003c6:	eb d7                	jmp    80039f <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003c8:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003cb:	b8 00 00 00 00       	mov    $0x0,%eax
  8003d0:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8003d3:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8003d6:	01 c0                	add    %eax,%eax
  8003d8:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  8003dc:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  8003df:	8d 51 d0             	lea    -0x30(%ecx),%edx
  8003e2:	83 fa 09             	cmp    $0x9,%edx
  8003e5:	77 34                	ja     80041b <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003e7:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003e8:	eb e9                	jmp    8003d3 <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003ea:	8b 45 14             	mov    0x14(%ebp),%eax
  8003ed:	8d 48 04             	lea    0x4(%eax),%ecx
  8003f0:	89 4d 14             	mov    %ecx,0x14(%ebp)
  8003f3:	8b 00                	mov    (%eax),%eax
  8003f5:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003f8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  8003fb:	eb 24                	jmp    800421 <vprintfmt+0xd9>
  8003fd:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800401:	79 07                	jns    80040a <vprintfmt+0xc2>
  800403:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80040a:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80040d:	eb 90                	jmp    80039f <vprintfmt+0x57>
  80040f:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  800412:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800419:	eb 84                	jmp    80039f <vprintfmt+0x57>
  80041b:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80041e:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  800421:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800425:	0f 89 74 ff ff ff    	jns    80039f <vprintfmt+0x57>
				width = precision, precision = -1;
  80042b:	8b 45 d0             	mov    -0x30(%ebp),%eax
  80042e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800431:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800438:	e9 62 ff ff ff       	jmp    80039f <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80043d:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80043e:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  800441:	e9 59 ff ff ff       	jmp    80039f <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800446:	8b 45 14             	mov    0x14(%ebp),%eax
  800449:	8d 50 04             	lea    0x4(%eax),%edx
  80044c:	89 55 14             	mov    %edx,0x14(%ebp)
  80044f:	83 ec 08             	sub    $0x8,%esp
  800452:	53                   	push   %ebx
  800453:	ff 30                	pushl  (%eax)
  800455:	ff d6                	call   *%esi
			break;
  800457:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80045a:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  80045d:	e9 0c ff ff ff       	jmp    80036e <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800462:	8b 45 14             	mov    0x14(%ebp),%eax
  800465:	8d 50 04             	lea    0x4(%eax),%edx
  800468:	89 55 14             	mov    %edx,0x14(%ebp)
  80046b:	8b 00                	mov    (%eax),%eax
  80046d:	85 c0                	test   %eax,%eax
  80046f:	79 02                	jns    800473 <vprintfmt+0x12b>
  800471:	f7 d8                	neg    %eax
  800473:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  800475:	83 f8 06             	cmp    $0x6,%eax
  800478:	7f 0b                	jg     800485 <vprintfmt+0x13d>
  80047a:	8b 04 85 48 0f 80 00 	mov    0x800f48(,%eax,4),%eax
  800481:	85 c0                	test   %eax,%eax
  800483:	75 18                	jne    80049d <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  800485:	52                   	push   %edx
  800486:	68 7a 0d 80 00       	push   $0x800d7a
  80048b:	53                   	push   %ebx
  80048c:	56                   	push   %esi
  80048d:	e8 99 fe ff ff       	call   80032b <printfmt>
  800492:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800495:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  800498:	e9 d1 fe ff ff       	jmp    80036e <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  80049d:	50                   	push   %eax
  80049e:	68 83 0d 80 00       	push   $0x800d83
  8004a3:	53                   	push   %ebx
  8004a4:	56                   	push   %esi
  8004a5:	e8 81 fe ff ff       	call   80032b <printfmt>
  8004aa:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004ad:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004b0:	e9 b9 fe ff ff       	jmp    80036e <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004b5:	8b 45 14             	mov    0x14(%ebp),%eax
  8004b8:	8d 50 04             	lea    0x4(%eax),%edx
  8004bb:	89 55 14             	mov    %edx,0x14(%ebp)
  8004be:	8b 38                	mov    (%eax),%edi
  8004c0:	85 ff                	test   %edi,%edi
  8004c2:	75 05                	jne    8004c9 <vprintfmt+0x181>
				p = "(null)";
  8004c4:	bf 73 0d 80 00       	mov    $0x800d73,%edi
			if (width > 0 && padc != '-')
  8004c9:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004cd:	0f 8e 90 00 00 00    	jle    800563 <vprintfmt+0x21b>
  8004d3:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  8004d7:	0f 84 8e 00 00 00    	je     80056b <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004dd:	83 ec 08             	sub    $0x8,%esp
  8004e0:	ff 75 d0             	pushl  -0x30(%ebp)
  8004e3:	57                   	push   %edi
  8004e4:	e8 70 02 00 00       	call   800759 <strnlen>
  8004e9:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  8004ec:	29 c1                	sub    %eax,%ecx
  8004ee:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  8004f1:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  8004f4:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  8004f8:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004fb:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  8004fe:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800500:	eb 0d                	jmp    80050f <vprintfmt+0x1c7>
					putch(padc, putdat);
  800502:	83 ec 08             	sub    $0x8,%esp
  800505:	53                   	push   %ebx
  800506:	ff 75 e4             	pushl  -0x1c(%ebp)
  800509:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80050b:	4f                   	dec    %edi
  80050c:	83 c4 10             	add    $0x10,%esp
  80050f:	85 ff                	test   %edi,%edi
  800511:	7f ef                	jg     800502 <vprintfmt+0x1ba>
  800513:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  800516:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800519:	89 c8                	mov    %ecx,%eax
  80051b:	85 c9                	test   %ecx,%ecx
  80051d:	79 05                	jns    800524 <vprintfmt+0x1dc>
  80051f:	b8 00 00 00 00       	mov    $0x0,%eax
  800524:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800527:	29 c1                	sub    %eax,%ecx
  800529:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  80052c:	89 75 08             	mov    %esi,0x8(%ebp)
  80052f:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800532:	eb 3d                	jmp    800571 <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  800534:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800538:	74 19                	je     800553 <vprintfmt+0x20b>
  80053a:	0f be c0             	movsbl %al,%eax
  80053d:	83 e8 20             	sub    $0x20,%eax
  800540:	83 f8 5e             	cmp    $0x5e,%eax
  800543:	76 0e                	jbe    800553 <vprintfmt+0x20b>
					putch('?', putdat);
  800545:	83 ec 08             	sub    $0x8,%esp
  800548:	53                   	push   %ebx
  800549:	6a 3f                	push   $0x3f
  80054b:	ff 55 08             	call   *0x8(%ebp)
  80054e:	83 c4 10             	add    $0x10,%esp
  800551:	eb 0b                	jmp    80055e <vprintfmt+0x216>
				else
					putch(ch, putdat);
  800553:	83 ec 08             	sub    $0x8,%esp
  800556:	53                   	push   %ebx
  800557:	52                   	push   %edx
  800558:	ff 55 08             	call   *0x8(%ebp)
  80055b:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80055e:	ff 4d e4             	decl   -0x1c(%ebp)
  800561:	eb 0e                	jmp    800571 <vprintfmt+0x229>
  800563:	89 75 08             	mov    %esi,0x8(%ebp)
  800566:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800569:	eb 06                	jmp    800571 <vprintfmt+0x229>
  80056b:	89 75 08             	mov    %esi,0x8(%ebp)
  80056e:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800571:	47                   	inc    %edi
  800572:	8a 47 ff             	mov    -0x1(%edi),%al
  800575:	0f be d0             	movsbl %al,%edx
  800578:	85 d2                	test   %edx,%edx
  80057a:	74 1d                	je     800599 <vprintfmt+0x251>
  80057c:	85 f6                	test   %esi,%esi
  80057e:	78 b4                	js     800534 <vprintfmt+0x1ec>
  800580:	4e                   	dec    %esi
  800581:	79 b1                	jns    800534 <vprintfmt+0x1ec>
  800583:	8b 75 08             	mov    0x8(%ebp),%esi
  800586:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  800589:	eb 14                	jmp    80059f <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  80058b:	83 ec 08             	sub    $0x8,%esp
  80058e:	53                   	push   %ebx
  80058f:	6a 20                	push   $0x20
  800591:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800593:	4f                   	dec    %edi
  800594:	83 c4 10             	add    $0x10,%esp
  800597:	eb 06                	jmp    80059f <vprintfmt+0x257>
  800599:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  80059c:	8b 75 08             	mov    0x8(%ebp),%esi
  80059f:	85 ff                	test   %edi,%edi
  8005a1:	7f e8                	jg     80058b <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005a3:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005a6:	e9 c3 fd ff ff       	jmp    80036e <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8005ab:	83 fa 01             	cmp    $0x1,%edx
  8005ae:	7e 16                	jle    8005c6 <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8005b0:	8b 45 14             	mov    0x14(%ebp),%eax
  8005b3:	8d 50 08             	lea    0x8(%eax),%edx
  8005b6:	89 55 14             	mov    %edx,0x14(%ebp)
  8005b9:	8b 50 04             	mov    0x4(%eax),%edx
  8005bc:	8b 00                	mov    (%eax),%eax
  8005be:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005c1:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8005c4:	eb 32                	jmp    8005f8 <vprintfmt+0x2b0>
	else if (lflag)
  8005c6:	85 d2                	test   %edx,%edx
  8005c8:	74 18                	je     8005e2 <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8005ca:	8b 45 14             	mov    0x14(%ebp),%eax
  8005cd:	8d 50 04             	lea    0x4(%eax),%edx
  8005d0:	89 55 14             	mov    %edx,0x14(%ebp)
  8005d3:	8b 00                	mov    (%eax),%eax
  8005d5:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005d8:	89 c1                	mov    %eax,%ecx
  8005da:	c1 f9 1f             	sar    $0x1f,%ecx
  8005dd:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  8005e0:	eb 16                	jmp    8005f8 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  8005e2:	8b 45 14             	mov    0x14(%ebp),%eax
  8005e5:	8d 50 04             	lea    0x4(%eax),%edx
  8005e8:	89 55 14             	mov    %edx,0x14(%ebp)
  8005eb:	8b 00                	mov    (%eax),%eax
  8005ed:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8005f0:	89 c1                	mov    %eax,%ecx
  8005f2:	c1 f9 1f             	sar    $0x1f,%ecx
  8005f5:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005f8:	8b 45 d8             	mov    -0x28(%ebp),%eax
  8005fb:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  8005fe:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800602:	79 76                	jns    80067a <vprintfmt+0x332>
				putch('-', putdat);
  800604:	83 ec 08             	sub    $0x8,%esp
  800607:	53                   	push   %ebx
  800608:	6a 2d                	push   $0x2d
  80060a:	ff d6                	call   *%esi
				num = -(long long) num;
  80060c:	8b 45 d8             	mov    -0x28(%ebp),%eax
  80060f:	8b 55 dc             	mov    -0x24(%ebp),%edx
  800612:	f7 d8                	neg    %eax
  800614:	83 d2 00             	adc    $0x0,%edx
  800617:	f7 da                	neg    %edx
  800619:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  80061c:	b9 0a 00 00 00       	mov    $0xa,%ecx
  800621:	eb 5c                	jmp    80067f <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800623:	8d 45 14             	lea    0x14(%ebp),%eax
  800626:	e8 aa fc ff ff       	call   8002d5 <getuint>
			base = 10;
  80062b:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800630:	eb 4d                	jmp    80067f <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  800632:	8d 45 14             	lea    0x14(%ebp),%eax
  800635:	e8 9b fc ff ff       	call   8002d5 <getuint>
			base = 8;
  80063a:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  80063f:	eb 3e                	jmp    80067f <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  800641:	83 ec 08             	sub    $0x8,%esp
  800644:	53                   	push   %ebx
  800645:	6a 30                	push   $0x30
  800647:	ff d6                	call   *%esi
			putch('x', putdat);
  800649:	83 c4 08             	add    $0x8,%esp
  80064c:	53                   	push   %ebx
  80064d:	6a 78                	push   $0x78
  80064f:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  800651:	8b 45 14             	mov    0x14(%ebp),%eax
  800654:	8d 50 04             	lea    0x4(%eax),%edx
  800657:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  80065a:	8b 00                	mov    (%eax),%eax
  80065c:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  800661:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  800664:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800669:	eb 14                	jmp    80067f <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80066b:	8d 45 14             	lea    0x14(%ebp),%eax
  80066e:	e8 62 fc ff ff       	call   8002d5 <getuint>
			base = 16;
  800673:	b9 10 00 00 00       	mov    $0x10,%ecx
  800678:	eb 05                	jmp    80067f <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  80067a:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  80067f:	83 ec 0c             	sub    $0xc,%esp
  800682:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  800686:	57                   	push   %edi
  800687:	ff 75 e4             	pushl  -0x1c(%ebp)
  80068a:	51                   	push   %ecx
  80068b:	52                   	push   %edx
  80068c:	50                   	push   %eax
  80068d:	89 da                	mov    %ebx,%edx
  80068f:	89 f0                	mov    %esi,%eax
  800691:	e8 92 fb ff ff       	call   800228 <printnum>
			break;
  800696:	83 c4 20             	add    $0x20,%esp
  800699:	8b 7d e0             	mov    -0x20(%ebp),%edi
  80069c:	e9 cd fc ff ff       	jmp    80036e <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8006a1:	83 ec 08             	sub    $0x8,%esp
  8006a4:	53                   	push   %ebx
  8006a5:	51                   	push   %ecx
  8006a6:	ff d6                	call   *%esi
			break;
  8006a8:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8006ab:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8006ae:	e9 bb fc ff ff       	jmp    80036e <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006b3:	83 ec 08             	sub    $0x8,%esp
  8006b6:	53                   	push   %ebx
  8006b7:	6a 25                	push   $0x25
  8006b9:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006bb:	83 c4 10             	add    $0x10,%esp
  8006be:	eb 01                	jmp    8006c1 <vprintfmt+0x379>
  8006c0:	4f                   	dec    %edi
  8006c1:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8006c5:	75 f9                	jne    8006c0 <vprintfmt+0x378>
  8006c7:	e9 a2 fc ff ff       	jmp    80036e <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8006cc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8006cf:	5b                   	pop    %ebx
  8006d0:	5e                   	pop    %esi
  8006d1:	5f                   	pop    %edi
  8006d2:	5d                   	pop    %ebp
  8006d3:	c3                   	ret    

008006d4 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8006d4:	55                   	push   %ebp
  8006d5:	89 e5                	mov    %esp,%ebp
  8006d7:	83 ec 18             	sub    $0x18,%esp
  8006da:	8b 45 08             	mov    0x8(%ebp),%eax
  8006dd:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  8006e0:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8006e3:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  8006e7:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  8006ea:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8006f1:	85 c0                	test   %eax,%eax
  8006f3:	74 26                	je     80071b <vsnprintf+0x47>
  8006f5:	85 d2                	test   %edx,%edx
  8006f7:	7e 29                	jle    800722 <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8006f9:	ff 75 14             	pushl  0x14(%ebp)
  8006fc:	ff 75 10             	pushl  0x10(%ebp)
  8006ff:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800702:	50                   	push   %eax
  800703:	68 0f 03 80 00       	push   $0x80030f
  800708:	e8 3b fc ff ff       	call   800348 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80070d:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800710:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800713:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800716:	83 c4 10             	add    $0x10,%esp
  800719:	eb 0c                	jmp    800727 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  80071b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800720:	eb 05                	jmp    800727 <vsnprintf+0x53>
  800722:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800727:	c9                   	leave  
  800728:	c3                   	ret    

00800729 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800729:	55                   	push   %ebp
  80072a:	89 e5                	mov    %esp,%ebp
  80072c:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80072f:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  800732:	50                   	push   %eax
  800733:	ff 75 10             	pushl  0x10(%ebp)
  800736:	ff 75 0c             	pushl  0xc(%ebp)
  800739:	ff 75 08             	pushl  0x8(%ebp)
  80073c:	e8 93 ff ff ff       	call   8006d4 <vsnprintf>
	va_end(ap);

	return rc;
}
  800741:	c9                   	leave  
  800742:	c3                   	ret    

00800743 <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  800743:	55                   	push   %ebp
  800744:	89 e5                	mov    %esp,%ebp
  800746:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800749:	b8 00 00 00 00       	mov    $0x0,%eax
  80074e:	eb 01                	jmp    800751 <strlen+0xe>
		n++;
  800750:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800751:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  800755:	75 f9                	jne    800750 <strlen+0xd>
		n++;
	return n;
}
  800757:	5d                   	pop    %ebp
  800758:	c3                   	ret    

00800759 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800759:	55                   	push   %ebp
  80075a:	89 e5                	mov    %esp,%ebp
  80075c:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80075f:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800762:	ba 00 00 00 00       	mov    $0x0,%edx
  800767:	eb 01                	jmp    80076a <strnlen+0x11>
		n++;
  800769:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80076a:	39 c2                	cmp    %eax,%edx
  80076c:	74 08                	je     800776 <strnlen+0x1d>
  80076e:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  800772:	75 f5                	jne    800769 <strnlen+0x10>
  800774:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  800776:	5d                   	pop    %ebp
  800777:	c3                   	ret    

00800778 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800778:	55                   	push   %ebp
  800779:	89 e5                	mov    %esp,%ebp
  80077b:	53                   	push   %ebx
  80077c:	8b 45 08             	mov    0x8(%ebp),%eax
  80077f:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  800782:	89 c2                	mov    %eax,%edx
  800784:	42                   	inc    %edx
  800785:	41                   	inc    %ecx
  800786:	8a 59 ff             	mov    -0x1(%ecx),%bl
  800789:	88 5a ff             	mov    %bl,-0x1(%edx)
  80078c:	84 db                	test   %bl,%bl
  80078e:	75 f4                	jne    800784 <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  800790:	5b                   	pop    %ebx
  800791:	5d                   	pop    %ebp
  800792:	c3                   	ret    

00800793 <strcat>:

char *
strcat(char *dst, const char *src)
{
  800793:	55                   	push   %ebp
  800794:	89 e5                	mov    %esp,%ebp
  800796:	53                   	push   %ebx
  800797:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  80079a:	53                   	push   %ebx
  80079b:	e8 a3 ff ff ff       	call   800743 <strlen>
  8007a0:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8007a3:	ff 75 0c             	pushl  0xc(%ebp)
  8007a6:	01 d8                	add    %ebx,%eax
  8007a8:	50                   	push   %eax
  8007a9:	e8 ca ff ff ff       	call   800778 <strcpy>
	return dst;
}
  8007ae:	89 d8                	mov    %ebx,%eax
  8007b0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8007b3:	c9                   	leave  
  8007b4:	c3                   	ret    

008007b5 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8007b5:	55                   	push   %ebp
  8007b6:	89 e5                	mov    %esp,%ebp
  8007b8:	56                   	push   %esi
  8007b9:	53                   	push   %ebx
  8007ba:	8b 75 08             	mov    0x8(%ebp),%esi
  8007bd:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007c0:	89 f3                	mov    %esi,%ebx
  8007c2:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007c5:	89 f2                	mov    %esi,%edx
  8007c7:	eb 0c                	jmp    8007d5 <strncpy+0x20>
		*dst++ = *src;
  8007c9:	42                   	inc    %edx
  8007ca:	8a 01                	mov    (%ecx),%al
  8007cc:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8007cf:	80 39 01             	cmpb   $0x1,(%ecx)
  8007d2:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8007d5:	39 da                	cmp    %ebx,%edx
  8007d7:	75 f0                	jne    8007c9 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  8007d9:	89 f0                	mov    %esi,%eax
  8007db:	5b                   	pop    %ebx
  8007dc:	5e                   	pop    %esi
  8007dd:	5d                   	pop    %ebp
  8007de:	c3                   	ret    

008007df <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  8007df:	55                   	push   %ebp
  8007e0:	89 e5                	mov    %esp,%ebp
  8007e2:	56                   	push   %esi
  8007e3:	53                   	push   %ebx
  8007e4:	8b 75 08             	mov    0x8(%ebp),%esi
  8007e7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8007ea:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  8007ed:	85 c0                	test   %eax,%eax
  8007ef:	74 1e                	je     80080f <strlcpy+0x30>
  8007f1:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  8007f5:	89 f2                	mov    %esi,%edx
  8007f7:	eb 05                	jmp    8007fe <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  8007f9:	42                   	inc    %edx
  8007fa:	41                   	inc    %ecx
  8007fb:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8007fe:	39 c2                	cmp    %eax,%edx
  800800:	74 08                	je     80080a <strlcpy+0x2b>
  800802:	8a 19                	mov    (%ecx),%bl
  800804:	84 db                	test   %bl,%bl
  800806:	75 f1                	jne    8007f9 <strlcpy+0x1a>
  800808:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  80080a:	c6 00 00             	movb   $0x0,(%eax)
  80080d:	eb 02                	jmp    800811 <strlcpy+0x32>
  80080f:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  800811:	29 f0                	sub    %esi,%eax
}
  800813:	5b                   	pop    %ebx
  800814:	5e                   	pop    %esi
  800815:	5d                   	pop    %ebp
  800816:	c3                   	ret    

00800817 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800817:	55                   	push   %ebp
  800818:	89 e5                	mov    %esp,%ebp
  80081a:	8b 4d 08             	mov    0x8(%ebp),%ecx
  80081d:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800820:	eb 02                	jmp    800824 <strcmp+0xd>
		p++, q++;
  800822:	41                   	inc    %ecx
  800823:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800824:	8a 01                	mov    (%ecx),%al
  800826:	84 c0                	test   %al,%al
  800828:	74 04                	je     80082e <strcmp+0x17>
  80082a:	3a 02                	cmp    (%edx),%al
  80082c:	74 f4                	je     800822 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80082e:	0f b6 c0             	movzbl %al,%eax
  800831:	0f b6 12             	movzbl (%edx),%edx
  800834:	29 d0                	sub    %edx,%eax
}
  800836:	5d                   	pop    %ebp
  800837:	c3                   	ret    

00800838 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800838:	55                   	push   %ebp
  800839:	89 e5                	mov    %esp,%ebp
  80083b:	53                   	push   %ebx
  80083c:	8b 45 08             	mov    0x8(%ebp),%eax
  80083f:	8b 55 0c             	mov    0xc(%ebp),%edx
  800842:	89 c3                	mov    %eax,%ebx
  800844:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800847:	eb 02                	jmp    80084b <strncmp+0x13>
		n--, p++, q++;
  800849:	40                   	inc    %eax
  80084a:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  80084b:	39 d8                	cmp    %ebx,%eax
  80084d:	74 14                	je     800863 <strncmp+0x2b>
  80084f:	8a 08                	mov    (%eax),%cl
  800851:	84 c9                	test   %cl,%cl
  800853:	74 04                	je     800859 <strncmp+0x21>
  800855:	3a 0a                	cmp    (%edx),%cl
  800857:	74 f0                	je     800849 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800859:	0f b6 00             	movzbl (%eax),%eax
  80085c:	0f b6 12             	movzbl (%edx),%edx
  80085f:	29 d0                	sub    %edx,%eax
  800861:	eb 05                	jmp    800868 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  800863:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800868:	5b                   	pop    %ebx
  800869:	5d                   	pop    %ebp
  80086a:	c3                   	ret    

0080086b <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  80086b:	55                   	push   %ebp
  80086c:	89 e5                	mov    %esp,%ebp
  80086e:	8b 45 08             	mov    0x8(%ebp),%eax
  800871:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800874:	eb 05                	jmp    80087b <strchr+0x10>
		if (*s == c)
  800876:	38 ca                	cmp    %cl,%dl
  800878:	74 0c                	je     800886 <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  80087a:	40                   	inc    %eax
  80087b:	8a 10                	mov    (%eax),%dl
  80087d:	84 d2                	test   %dl,%dl
  80087f:	75 f5                	jne    800876 <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  800881:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800886:	5d                   	pop    %ebp
  800887:	c3                   	ret    

00800888 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800888:	55                   	push   %ebp
  800889:	89 e5                	mov    %esp,%ebp
  80088b:	8b 45 08             	mov    0x8(%ebp),%eax
  80088e:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  800891:	eb 05                	jmp    800898 <strfind+0x10>
		if (*s == c)
  800893:	38 ca                	cmp    %cl,%dl
  800895:	74 07                	je     80089e <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800897:	40                   	inc    %eax
  800898:	8a 10                	mov    (%eax),%dl
  80089a:	84 d2                	test   %dl,%dl
  80089c:	75 f5                	jne    800893 <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  80089e:	5d                   	pop    %ebp
  80089f:	c3                   	ret    

008008a0 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8008a0:	55                   	push   %ebp
  8008a1:	89 e5                	mov    %esp,%ebp
  8008a3:	57                   	push   %edi
  8008a4:	56                   	push   %esi
  8008a5:	53                   	push   %ebx
  8008a6:	8b 7d 08             	mov    0x8(%ebp),%edi
  8008a9:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8008ac:	85 c9                	test   %ecx,%ecx
  8008ae:	74 36                	je     8008e6 <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8008b0:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8008b6:	75 28                	jne    8008e0 <memset+0x40>
  8008b8:	f6 c1 03             	test   $0x3,%cl
  8008bb:	75 23                	jne    8008e0 <memset+0x40>
		c &= 0xFF;
  8008bd:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8008c1:	89 d3                	mov    %edx,%ebx
  8008c3:	c1 e3 08             	shl    $0x8,%ebx
  8008c6:	89 d6                	mov    %edx,%esi
  8008c8:	c1 e6 18             	shl    $0x18,%esi
  8008cb:	89 d0                	mov    %edx,%eax
  8008cd:	c1 e0 10             	shl    $0x10,%eax
  8008d0:	09 f0                	or     %esi,%eax
  8008d2:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8008d4:	89 d8                	mov    %ebx,%eax
  8008d6:	09 d0                	or     %edx,%eax
  8008d8:	c1 e9 02             	shr    $0x2,%ecx
  8008db:	fc                   	cld    
  8008dc:	f3 ab                	rep stos %eax,%es:(%edi)
  8008de:	eb 06                	jmp    8008e6 <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  8008e0:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008e3:	fc                   	cld    
  8008e4:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  8008e6:	89 f8                	mov    %edi,%eax
  8008e8:	5b                   	pop    %ebx
  8008e9:	5e                   	pop    %esi
  8008ea:	5f                   	pop    %edi
  8008eb:	5d                   	pop    %ebp
  8008ec:	c3                   	ret    

008008ed <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  8008ed:	55                   	push   %ebp
  8008ee:	89 e5                	mov    %esp,%ebp
  8008f0:	57                   	push   %edi
  8008f1:	56                   	push   %esi
  8008f2:	8b 45 08             	mov    0x8(%ebp),%eax
  8008f5:	8b 75 0c             	mov    0xc(%ebp),%esi
  8008f8:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  8008fb:	39 c6                	cmp    %eax,%esi
  8008fd:	73 33                	jae    800932 <memmove+0x45>
  8008ff:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  800902:	39 d0                	cmp    %edx,%eax
  800904:	73 2c                	jae    800932 <memmove+0x45>
		s += n;
		d += n;
  800906:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800909:	89 d6                	mov    %edx,%esi
  80090b:	09 fe                	or     %edi,%esi
  80090d:	f7 c6 03 00 00 00    	test   $0x3,%esi
  800913:	75 13                	jne    800928 <memmove+0x3b>
  800915:	f6 c1 03             	test   $0x3,%cl
  800918:	75 0e                	jne    800928 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  80091a:	83 ef 04             	sub    $0x4,%edi
  80091d:	8d 72 fc             	lea    -0x4(%edx),%esi
  800920:	c1 e9 02             	shr    $0x2,%ecx
  800923:	fd                   	std    
  800924:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800926:	eb 07                	jmp    80092f <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800928:	4f                   	dec    %edi
  800929:	8d 72 ff             	lea    -0x1(%edx),%esi
  80092c:	fd                   	std    
  80092d:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  80092f:	fc                   	cld    
  800930:	eb 1d                	jmp    80094f <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800932:	89 f2                	mov    %esi,%edx
  800934:	09 c2                	or     %eax,%edx
  800936:	f6 c2 03             	test   $0x3,%dl
  800939:	75 0f                	jne    80094a <memmove+0x5d>
  80093b:	f6 c1 03             	test   $0x3,%cl
  80093e:	75 0a                	jne    80094a <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800940:	c1 e9 02             	shr    $0x2,%ecx
  800943:	89 c7                	mov    %eax,%edi
  800945:	fc                   	cld    
  800946:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800948:	eb 05                	jmp    80094f <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  80094a:	89 c7                	mov    %eax,%edi
  80094c:	fc                   	cld    
  80094d:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  80094f:	5e                   	pop    %esi
  800950:	5f                   	pop    %edi
  800951:	5d                   	pop    %ebp
  800952:	c3                   	ret    

00800953 <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  800953:	55                   	push   %ebp
  800954:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  800956:	ff 75 10             	pushl  0x10(%ebp)
  800959:	ff 75 0c             	pushl  0xc(%ebp)
  80095c:	ff 75 08             	pushl  0x8(%ebp)
  80095f:	e8 89 ff ff ff       	call   8008ed <memmove>
}
  800964:	c9                   	leave  
  800965:	c3                   	ret    

00800966 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  800966:	55                   	push   %ebp
  800967:	89 e5                	mov    %esp,%ebp
  800969:	56                   	push   %esi
  80096a:	53                   	push   %ebx
  80096b:	8b 45 08             	mov    0x8(%ebp),%eax
  80096e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800971:	89 c6                	mov    %eax,%esi
  800973:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  800976:	eb 14                	jmp    80098c <memcmp+0x26>
		if (*s1 != *s2)
  800978:	8a 08                	mov    (%eax),%cl
  80097a:	8a 1a                	mov    (%edx),%bl
  80097c:	38 d9                	cmp    %bl,%cl
  80097e:	74 0a                	je     80098a <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  800980:	0f b6 c1             	movzbl %cl,%eax
  800983:	0f b6 db             	movzbl %bl,%ebx
  800986:	29 d8                	sub    %ebx,%eax
  800988:	eb 0b                	jmp    800995 <memcmp+0x2f>
		s1++, s2++;
  80098a:	40                   	inc    %eax
  80098b:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80098c:	39 f0                	cmp    %esi,%eax
  80098e:	75 e8                	jne    800978 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800990:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800995:	5b                   	pop    %ebx
  800996:	5e                   	pop    %esi
  800997:	5d                   	pop    %ebp
  800998:	c3                   	ret    

00800999 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  800999:	55                   	push   %ebp
  80099a:	89 e5                	mov    %esp,%ebp
  80099c:	53                   	push   %ebx
  80099d:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8009a0:	89 c1                	mov    %eax,%ecx
  8009a2:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8009a5:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009a9:	eb 08                	jmp    8009b3 <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8009ab:	0f b6 10             	movzbl (%eax),%edx
  8009ae:	39 da                	cmp    %ebx,%edx
  8009b0:	74 05                	je     8009b7 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8009b2:	40                   	inc    %eax
  8009b3:	39 c8                	cmp    %ecx,%eax
  8009b5:	72 f4                	jb     8009ab <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8009b7:	5b                   	pop    %ebx
  8009b8:	5d                   	pop    %ebp
  8009b9:	c3                   	ret    

008009ba <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8009ba:	55                   	push   %ebp
  8009bb:	89 e5                	mov    %esp,%ebp
  8009bd:	57                   	push   %edi
  8009be:	56                   	push   %esi
  8009bf:	53                   	push   %ebx
  8009c0:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009c3:	eb 01                	jmp    8009c6 <strtol+0xc>
		s++;
  8009c5:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8009c6:	8a 01                	mov    (%ecx),%al
  8009c8:	3c 20                	cmp    $0x20,%al
  8009ca:	74 f9                	je     8009c5 <strtol+0xb>
  8009cc:	3c 09                	cmp    $0x9,%al
  8009ce:	74 f5                	je     8009c5 <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8009d0:	3c 2b                	cmp    $0x2b,%al
  8009d2:	75 08                	jne    8009dc <strtol+0x22>
		s++;
  8009d4:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009d5:	bf 00 00 00 00       	mov    $0x0,%edi
  8009da:	eb 11                	jmp    8009ed <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  8009dc:	3c 2d                	cmp    $0x2d,%al
  8009de:	75 08                	jne    8009e8 <strtol+0x2e>
		s++, neg = 1;
  8009e0:	41                   	inc    %ecx
  8009e1:	bf 01 00 00 00       	mov    $0x1,%edi
  8009e6:	eb 05                	jmp    8009ed <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8009e8:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009ed:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009f1:	0f 84 87 00 00 00    	je     800a7e <strtol+0xc4>
  8009f7:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  8009fb:	75 27                	jne    800a24 <strtol+0x6a>
  8009fd:	80 39 30             	cmpb   $0x30,(%ecx)
  800a00:	75 22                	jne    800a24 <strtol+0x6a>
  800a02:	e9 88 00 00 00       	jmp    800a8f <strtol+0xd5>
		s += 2, base = 16;
  800a07:	83 c1 02             	add    $0x2,%ecx
  800a0a:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800a11:	eb 11                	jmp    800a24 <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  800a13:	41                   	inc    %ecx
  800a14:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800a1b:	eb 07                	jmp    800a24 <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800a1d:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  800a24:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800a29:	8a 11                	mov    (%ecx),%dl
  800a2b:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800a2e:	80 fb 09             	cmp    $0x9,%bl
  800a31:	77 08                	ja     800a3b <strtol+0x81>
			dig = *s - '0';
  800a33:	0f be d2             	movsbl %dl,%edx
  800a36:	83 ea 30             	sub    $0x30,%edx
  800a39:	eb 22                	jmp    800a5d <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800a3b:	8d 72 9f             	lea    -0x61(%edx),%esi
  800a3e:	89 f3                	mov    %esi,%ebx
  800a40:	80 fb 19             	cmp    $0x19,%bl
  800a43:	77 08                	ja     800a4d <strtol+0x93>
			dig = *s - 'a' + 10;
  800a45:	0f be d2             	movsbl %dl,%edx
  800a48:	83 ea 57             	sub    $0x57,%edx
  800a4b:	eb 10                	jmp    800a5d <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800a4d:	8d 72 bf             	lea    -0x41(%edx),%esi
  800a50:	89 f3                	mov    %esi,%ebx
  800a52:	80 fb 19             	cmp    $0x19,%bl
  800a55:	77 14                	ja     800a6b <strtol+0xb1>
			dig = *s - 'A' + 10;
  800a57:	0f be d2             	movsbl %dl,%edx
  800a5a:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800a5d:	3b 55 10             	cmp    0x10(%ebp),%edx
  800a60:	7d 09                	jge    800a6b <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  800a62:	41                   	inc    %ecx
  800a63:	0f af 45 10          	imul   0x10(%ebp),%eax
  800a67:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800a69:	eb be                	jmp    800a29 <strtol+0x6f>

	if (endptr)
  800a6b:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a6f:	74 05                	je     800a76 <strtol+0xbc>
		*endptr = (char *) s;
  800a71:	8b 75 0c             	mov    0xc(%ebp),%esi
  800a74:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  800a76:	85 ff                	test   %edi,%edi
  800a78:	74 21                	je     800a9b <strtol+0xe1>
  800a7a:	f7 d8                	neg    %eax
  800a7c:	eb 1d                	jmp    800a9b <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800a7e:	80 39 30             	cmpb   $0x30,(%ecx)
  800a81:	75 9a                	jne    800a1d <strtol+0x63>
  800a83:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a87:	0f 84 7a ff ff ff    	je     800a07 <strtol+0x4d>
  800a8d:	eb 84                	jmp    800a13 <strtol+0x59>
  800a8f:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  800a93:	0f 84 6e ff ff ff    	je     800a07 <strtol+0x4d>
  800a99:	eb 89                	jmp    800a24 <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  800a9b:	5b                   	pop    %ebx
  800a9c:	5e                   	pop    %esi
  800a9d:	5f                   	pop    %edi
  800a9e:	5d                   	pop    %ebp
  800a9f:	c3                   	ret    

00800aa0 <__udivdi3>:
  800aa0:	55                   	push   %ebp
  800aa1:	57                   	push   %edi
  800aa2:	56                   	push   %esi
  800aa3:	53                   	push   %ebx
  800aa4:	83 ec 1c             	sub    $0x1c,%esp
  800aa7:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800aab:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800aaf:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800ab3:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800ab7:	89 ca                	mov    %ecx,%edx
  800ab9:	89 f8                	mov    %edi,%eax
  800abb:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800abf:	85 f6                	test   %esi,%esi
  800ac1:	75 2d                	jne    800af0 <__udivdi3+0x50>
  800ac3:	39 cf                	cmp    %ecx,%edi
  800ac5:	77 65                	ja     800b2c <__udivdi3+0x8c>
  800ac7:	89 fd                	mov    %edi,%ebp
  800ac9:	85 ff                	test   %edi,%edi
  800acb:	75 0b                	jne    800ad8 <__udivdi3+0x38>
  800acd:	b8 01 00 00 00       	mov    $0x1,%eax
  800ad2:	31 d2                	xor    %edx,%edx
  800ad4:	f7 f7                	div    %edi
  800ad6:	89 c5                	mov    %eax,%ebp
  800ad8:	31 d2                	xor    %edx,%edx
  800ada:	89 c8                	mov    %ecx,%eax
  800adc:	f7 f5                	div    %ebp
  800ade:	89 c1                	mov    %eax,%ecx
  800ae0:	89 d8                	mov    %ebx,%eax
  800ae2:	f7 f5                	div    %ebp
  800ae4:	89 cf                	mov    %ecx,%edi
  800ae6:	89 fa                	mov    %edi,%edx
  800ae8:	83 c4 1c             	add    $0x1c,%esp
  800aeb:	5b                   	pop    %ebx
  800aec:	5e                   	pop    %esi
  800aed:	5f                   	pop    %edi
  800aee:	5d                   	pop    %ebp
  800aef:	c3                   	ret    
  800af0:	39 ce                	cmp    %ecx,%esi
  800af2:	77 28                	ja     800b1c <__udivdi3+0x7c>
  800af4:	0f bd fe             	bsr    %esi,%edi
  800af7:	83 f7 1f             	xor    $0x1f,%edi
  800afa:	75 40                	jne    800b3c <__udivdi3+0x9c>
  800afc:	39 ce                	cmp    %ecx,%esi
  800afe:	72 0a                	jb     800b0a <__udivdi3+0x6a>
  800b00:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b04:	0f 87 9e 00 00 00    	ja     800ba8 <__udivdi3+0x108>
  800b0a:	b8 01 00 00 00       	mov    $0x1,%eax
  800b0f:	89 fa                	mov    %edi,%edx
  800b11:	83 c4 1c             	add    $0x1c,%esp
  800b14:	5b                   	pop    %ebx
  800b15:	5e                   	pop    %esi
  800b16:	5f                   	pop    %edi
  800b17:	5d                   	pop    %ebp
  800b18:	c3                   	ret    
  800b19:	8d 76 00             	lea    0x0(%esi),%esi
  800b1c:	31 ff                	xor    %edi,%edi
  800b1e:	31 c0                	xor    %eax,%eax
  800b20:	89 fa                	mov    %edi,%edx
  800b22:	83 c4 1c             	add    $0x1c,%esp
  800b25:	5b                   	pop    %ebx
  800b26:	5e                   	pop    %esi
  800b27:	5f                   	pop    %edi
  800b28:	5d                   	pop    %ebp
  800b29:	c3                   	ret    
  800b2a:	66 90                	xchg   %ax,%ax
  800b2c:	89 d8                	mov    %ebx,%eax
  800b2e:	f7 f7                	div    %edi
  800b30:	31 ff                	xor    %edi,%edi
  800b32:	89 fa                	mov    %edi,%edx
  800b34:	83 c4 1c             	add    $0x1c,%esp
  800b37:	5b                   	pop    %ebx
  800b38:	5e                   	pop    %esi
  800b39:	5f                   	pop    %edi
  800b3a:	5d                   	pop    %ebp
  800b3b:	c3                   	ret    
  800b3c:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b41:	89 eb                	mov    %ebp,%ebx
  800b43:	29 fb                	sub    %edi,%ebx
  800b45:	89 f9                	mov    %edi,%ecx
  800b47:	d3 e6                	shl    %cl,%esi
  800b49:	89 c5                	mov    %eax,%ebp
  800b4b:	88 d9                	mov    %bl,%cl
  800b4d:	d3 ed                	shr    %cl,%ebp
  800b4f:	89 e9                	mov    %ebp,%ecx
  800b51:	09 f1                	or     %esi,%ecx
  800b53:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b57:	89 f9                	mov    %edi,%ecx
  800b59:	d3 e0                	shl    %cl,%eax
  800b5b:	89 c5                	mov    %eax,%ebp
  800b5d:	89 d6                	mov    %edx,%esi
  800b5f:	88 d9                	mov    %bl,%cl
  800b61:	d3 ee                	shr    %cl,%esi
  800b63:	89 f9                	mov    %edi,%ecx
  800b65:	d3 e2                	shl    %cl,%edx
  800b67:	8b 44 24 08          	mov    0x8(%esp),%eax
  800b6b:	88 d9                	mov    %bl,%cl
  800b6d:	d3 e8                	shr    %cl,%eax
  800b6f:	09 c2                	or     %eax,%edx
  800b71:	89 d0                	mov    %edx,%eax
  800b73:	89 f2                	mov    %esi,%edx
  800b75:	f7 74 24 0c          	divl   0xc(%esp)
  800b79:	89 d6                	mov    %edx,%esi
  800b7b:	89 c3                	mov    %eax,%ebx
  800b7d:	f7 e5                	mul    %ebp
  800b7f:	39 d6                	cmp    %edx,%esi
  800b81:	72 19                	jb     800b9c <__udivdi3+0xfc>
  800b83:	74 0b                	je     800b90 <__udivdi3+0xf0>
  800b85:	89 d8                	mov    %ebx,%eax
  800b87:	31 ff                	xor    %edi,%edi
  800b89:	e9 58 ff ff ff       	jmp    800ae6 <__udivdi3+0x46>
  800b8e:	66 90                	xchg   %ax,%ax
  800b90:	8b 54 24 08          	mov    0x8(%esp),%edx
  800b94:	89 f9                	mov    %edi,%ecx
  800b96:	d3 e2                	shl    %cl,%edx
  800b98:	39 c2                	cmp    %eax,%edx
  800b9a:	73 e9                	jae    800b85 <__udivdi3+0xe5>
  800b9c:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800b9f:	31 ff                	xor    %edi,%edi
  800ba1:	e9 40 ff ff ff       	jmp    800ae6 <__udivdi3+0x46>
  800ba6:	66 90                	xchg   %ax,%ax
  800ba8:	31 c0                	xor    %eax,%eax
  800baa:	e9 37 ff ff ff       	jmp    800ae6 <__udivdi3+0x46>
  800baf:	90                   	nop

00800bb0 <__umoddi3>:
  800bb0:	55                   	push   %ebp
  800bb1:	57                   	push   %edi
  800bb2:	56                   	push   %esi
  800bb3:	53                   	push   %ebx
  800bb4:	83 ec 1c             	sub    $0x1c,%esp
  800bb7:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bbb:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bbf:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800bc3:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800bc7:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800bcb:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800bcf:	89 f3                	mov    %esi,%ebx
  800bd1:	89 fa                	mov    %edi,%edx
  800bd3:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800bd7:	89 34 24             	mov    %esi,(%esp)
  800bda:	85 c0                	test   %eax,%eax
  800bdc:	75 1a                	jne    800bf8 <__umoddi3+0x48>
  800bde:	39 f7                	cmp    %esi,%edi
  800be0:	0f 86 a2 00 00 00    	jbe    800c88 <__umoddi3+0xd8>
  800be6:	89 c8                	mov    %ecx,%eax
  800be8:	89 f2                	mov    %esi,%edx
  800bea:	f7 f7                	div    %edi
  800bec:	89 d0                	mov    %edx,%eax
  800bee:	31 d2                	xor    %edx,%edx
  800bf0:	83 c4 1c             	add    $0x1c,%esp
  800bf3:	5b                   	pop    %ebx
  800bf4:	5e                   	pop    %esi
  800bf5:	5f                   	pop    %edi
  800bf6:	5d                   	pop    %ebp
  800bf7:	c3                   	ret    
  800bf8:	39 f0                	cmp    %esi,%eax
  800bfa:	0f 87 ac 00 00 00    	ja     800cac <__umoddi3+0xfc>
  800c00:	0f bd e8             	bsr    %eax,%ebp
  800c03:	83 f5 1f             	xor    $0x1f,%ebp
  800c06:	0f 84 ac 00 00 00    	je     800cb8 <__umoddi3+0x108>
  800c0c:	bf 20 00 00 00       	mov    $0x20,%edi
  800c11:	29 ef                	sub    %ebp,%edi
  800c13:	89 fe                	mov    %edi,%esi
  800c15:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c19:	89 e9                	mov    %ebp,%ecx
  800c1b:	d3 e0                	shl    %cl,%eax
  800c1d:	89 d7                	mov    %edx,%edi
  800c1f:	89 f1                	mov    %esi,%ecx
  800c21:	d3 ef                	shr    %cl,%edi
  800c23:	09 c7                	or     %eax,%edi
  800c25:	89 e9                	mov    %ebp,%ecx
  800c27:	d3 e2                	shl    %cl,%edx
  800c29:	89 14 24             	mov    %edx,(%esp)
  800c2c:	89 d8                	mov    %ebx,%eax
  800c2e:	d3 e0                	shl    %cl,%eax
  800c30:	89 c2                	mov    %eax,%edx
  800c32:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c36:	d3 e0                	shl    %cl,%eax
  800c38:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c3c:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c40:	89 f1                	mov    %esi,%ecx
  800c42:	d3 e8                	shr    %cl,%eax
  800c44:	09 d0                	or     %edx,%eax
  800c46:	d3 eb                	shr    %cl,%ebx
  800c48:	89 da                	mov    %ebx,%edx
  800c4a:	f7 f7                	div    %edi
  800c4c:	89 d3                	mov    %edx,%ebx
  800c4e:	f7 24 24             	mull   (%esp)
  800c51:	89 c6                	mov    %eax,%esi
  800c53:	89 d1                	mov    %edx,%ecx
  800c55:	39 d3                	cmp    %edx,%ebx
  800c57:	0f 82 87 00 00 00    	jb     800ce4 <__umoddi3+0x134>
  800c5d:	0f 84 91 00 00 00    	je     800cf4 <__umoddi3+0x144>
  800c63:	8b 54 24 04          	mov    0x4(%esp),%edx
  800c67:	29 f2                	sub    %esi,%edx
  800c69:	19 cb                	sbb    %ecx,%ebx
  800c6b:	89 d8                	mov    %ebx,%eax
  800c6d:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800c71:	d3 e0                	shl    %cl,%eax
  800c73:	89 e9                	mov    %ebp,%ecx
  800c75:	d3 ea                	shr    %cl,%edx
  800c77:	09 d0                	or     %edx,%eax
  800c79:	89 e9                	mov    %ebp,%ecx
  800c7b:	d3 eb                	shr    %cl,%ebx
  800c7d:	89 da                	mov    %ebx,%edx
  800c7f:	83 c4 1c             	add    $0x1c,%esp
  800c82:	5b                   	pop    %ebx
  800c83:	5e                   	pop    %esi
  800c84:	5f                   	pop    %edi
  800c85:	5d                   	pop    %ebp
  800c86:	c3                   	ret    
  800c87:	90                   	nop
  800c88:	89 fd                	mov    %edi,%ebp
  800c8a:	85 ff                	test   %edi,%edi
  800c8c:	75 0b                	jne    800c99 <__umoddi3+0xe9>
  800c8e:	b8 01 00 00 00       	mov    $0x1,%eax
  800c93:	31 d2                	xor    %edx,%edx
  800c95:	f7 f7                	div    %edi
  800c97:	89 c5                	mov    %eax,%ebp
  800c99:	89 f0                	mov    %esi,%eax
  800c9b:	31 d2                	xor    %edx,%edx
  800c9d:	f7 f5                	div    %ebp
  800c9f:	89 c8                	mov    %ecx,%eax
  800ca1:	f7 f5                	div    %ebp
  800ca3:	89 d0                	mov    %edx,%eax
  800ca5:	e9 44 ff ff ff       	jmp    800bee <__umoddi3+0x3e>
  800caa:	66 90                	xchg   %ax,%ax
  800cac:	89 c8                	mov    %ecx,%eax
  800cae:	89 f2                	mov    %esi,%edx
  800cb0:	83 c4 1c             	add    $0x1c,%esp
  800cb3:	5b                   	pop    %ebx
  800cb4:	5e                   	pop    %esi
  800cb5:	5f                   	pop    %edi
  800cb6:	5d                   	pop    %ebp
  800cb7:	c3                   	ret    
  800cb8:	3b 04 24             	cmp    (%esp),%eax
  800cbb:	72 06                	jb     800cc3 <__umoddi3+0x113>
  800cbd:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800cc1:	77 0f                	ja     800cd2 <__umoddi3+0x122>
  800cc3:	89 f2                	mov    %esi,%edx
  800cc5:	29 f9                	sub    %edi,%ecx
  800cc7:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800ccb:	89 14 24             	mov    %edx,(%esp)
  800cce:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800cd2:	8b 44 24 04          	mov    0x4(%esp),%eax
  800cd6:	8b 14 24             	mov    (%esp),%edx
  800cd9:	83 c4 1c             	add    $0x1c,%esp
  800cdc:	5b                   	pop    %ebx
  800cdd:	5e                   	pop    %esi
  800cde:	5f                   	pop    %edi
  800cdf:	5d                   	pop    %ebp
  800ce0:	c3                   	ret    
  800ce1:	8d 76 00             	lea    0x0(%esi),%esi
  800ce4:	2b 04 24             	sub    (%esp),%eax
  800ce7:	19 fa                	sbb    %edi,%edx
  800ce9:	89 d1                	mov    %edx,%ecx
  800ceb:	89 c6                	mov    %eax,%esi
  800ced:	e9 71 ff ff ff       	jmp    800c63 <__umoddi3+0xb3>
  800cf2:	66 90                	xchg   %ax,%ax
  800cf4:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800cf8:	72 ea                	jb     800ce4 <__umoddi3+0x134>
  800cfa:	89 d9                	mov    %ebx,%ecx
  800cfc:	e9 62 ff ff ff       	jmp    800c63 <__umoddi3+0xb3>
