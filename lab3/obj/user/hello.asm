
obj/user/hello:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 2d 00 00 00       	call   80005e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:
// hello, world
#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 14             	sub    $0x14,%esp
	cprintf("hello, world\n");
  800039:	68 20 0d 80 00       	push   $0x800d20
  80003e:	e8 0a 01 00 00       	call   80014d <cprintf>
	cprintf("i am environment %08x\n", thisenv->env_id);
  800043:	a1 04 10 80 00       	mov    0x801004,%eax
  800048:	8b 40 48             	mov    0x48(%eax),%eax
  80004b:	83 c4 08             	add    $0x8,%esp
  80004e:	50                   	push   %eax
  80004f:	68 2e 0d 80 00       	push   $0x800d2e
  800054:	e8 f4 00 00 00       	call   80014d <cprintf>
}
  800059:	83 c4 10             	add    $0x10,%esp
  80005c:	c9                   	leave  
  80005d:	c3                   	ret    

0080005e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80005e:	55                   	push   %ebp
  80005f:	89 e5                	mov    %esp,%ebp
  800061:	56                   	push   %esi
  800062:	53                   	push   %ebx
  800063:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800066:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800069:	e8 e9 09 00 00       	call   800a57 <sys_getenvid>
  80006e:	25 ff 03 00 00       	and    $0x3ff,%eax
  800073:	8d 14 00             	lea    (%eax,%eax,1),%edx
  800076:	01 d0                	add    %edx,%eax
  800078:	c1 e0 05             	shl    $0x5,%eax
  80007b:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800080:	a3 04 10 80 00       	mov    %eax,0x801004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800085:	85 db                	test   %ebx,%ebx
  800087:	7e 07                	jle    800090 <libmain+0x32>
		binaryname = argv[0];
  800089:	8b 06                	mov    (%esi),%eax
  80008b:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  800090:	83 ec 08             	sub    $0x8,%esp
  800093:	56                   	push   %esi
  800094:	53                   	push   %ebx
  800095:	e8 99 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80009a:	e8 0a 00 00 00       	call   8000a9 <exit>
}
  80009f:	83 c4 10             	add    $0x10,%esp
  8000a2:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8000a5:	5b                   	pop    %ebx
  8000a6:	5e                   	pop    %esi
  8000a7:	5d                   	pop    %ebp
  8000a8:	c3                   	ret    

008000a9 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000a9:	55                   	push   %ebp
  8000aa:	89 e5                	mov    %esp,%ebp
  8000ac:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  8000af:	6a 00                	push   $0x0
  8000b1:	e8 60 09 00 00       	call   800a16 <sys_env_destroy>
}
  8000b6:	83 c4 10             	add    $0x10,%esp
  8000b9:	c9                   	leave  
  8000ba:	c3                   	ret    

008000bb <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000bb:	55                   	push   %ebp
  8000bc:	89 e5                	mov    %esp,%ebp
  8000be:	53                   	push   %ebx
  8000bf:	83 ec 04             	sub    $0x4,%esp
  8000c2:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000c5:	8b 13                	mov    (%ebx),%edx
  8000c7:	8d 42 01             	lea    0x1(%edx),%eax
  8000ca:	89 03                	mov    %eax,(%ebx)
  8000cc:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000cf:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8000d3:	3d ff 00 00 00       	cmp    $0xff,%eax
  8000d8:	75 1a                	jne    8000f4 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8000da:	83 ec 08             	sub    $0x8,%esp
  8000dd:	68 ff 00 00 00       	push   $0xff
  8000e2:	8d 43 08             	lea    0x8(%ebx),%eax
  8000e5:	50                   	push   %eax
  8000e6:	e8 ee 08 00 00       	call   8009d9 <sys_cputs>
		b->idx = 0;
  8000eb:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8000f1:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8000f4:	ff 43 04             	incl   0x4(%ebx)
}
  8000f7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8000fa:	c9                   	leave  
  8000fb:	c3                   	ret    

008000fc <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8000fc:	55                   	push   %ebp
  8000fd:	89 e5                	mov    %esp,%ebp
  8000ff:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800105:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80010c:	00 00 00 
	b.cnt = 0;
  80010f:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800116:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800119:	ff 75 0c             	pushl  0xc(%ebp)
  80011c:	ff 75 08             	pushl  0x8(%ebp)
  80011f:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800125:	50                   	push   %eax
  800126:	68 bb 00 80 00       	push   $0x8000bb
  80012b:	e8 51 01 00 00       	call   800281 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800130:	83 c4 08             	add    $0x8,%esp
  800133:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800139:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80013f:	50                   	push   %eax
  800140:	e8 94 08 00 00       	call   8009d9 <sys_cputs>

	return b.cnt;
}
  800145:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80014b:	c9                   	leave  
  80014c:	c3                   	ret    

0080014d <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80014d:	55                   	push   %ebp
  80014e:	89 e5                	mov    %esp,%ebp
  800150:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800153:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800156:	50                   	push   %eax
  800157:	ff 75 08             	pushl  0x8(%ebp)
  80015a:	e8 9d ff ff ff       	call   8000fc <vcprintf>
	va_end(ap);

	return cnt;
}
  80015f:	c9                   	leave  
  800160:	c3                   	ret    

00800161 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800161:	55                   	push   %ebp
  800162:	89 e5                	mov    %esp,%ebp
  800164:	57                   	push   %edi
  800165:	56                   	push   %esi
  800166:	53                   	push   %ebx
  800167:	83 ec 1c             	sub    $0x1c,%esp
  80016a:	89 c7                	mov    %eax,%edi
  80016c:	89 d6                	mov    %edx,%esi
  80016e:	8b 45 08             	mov    0x8(%ebp),%eax
  800171:	8b 55 0c             	mov    0xc(%ebp),%edx
  800174:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800177:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80017a:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80017d:	bb 00 00 00 00       	mov    $0x0,%ebx
  800182:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800185:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800188:	39 d3                	cmp    %edx,%ebx
  80018a:	72 05                	jb     800191 <printnum+0x30>
  80018c:	39 45 10             	cmp    %eax,0x10(%ebp)
  80018f:	77 45                	ja     8001d6 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800191:	83 ec 0c             	sub    $0xc,%esp
  800194:	ff 75 18             	pushl  0x18(%ebp)
  800197:	8b 45 14             	mov    0x14(%ebp),%eax
  80019a:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80019d:	53                   	push   %ebx
  80019e:	ff 75 10             	pushl  0x10(%ebp)
  8001a1:	83 ec 08             	sub    $0x8,%esp
  8001a4:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001a7:	ff 75 e0             	pushl  -0x20(%ebp)
  8001aa:	ff 75 dc             	pushl  -0x24(%ebp)
  8001ad:	ff 75 d8             	pushl  -0x28(%ebp)
  8001b0:	e8 07 09 00 00       	call   800abc <__udivdi3>
  8001b5:	83 c4 18             	add    $0x18,%esp
  8001b8:	52                   	push   %edx
  8001b9:	50                   	push   %eax
  8001ba:	89 f2                	mov    %esi,%edx
  8001bc:	89 f8                	mov    %edi,%eax
  8001be:	e8 9e ff ff ff       	call   800161 <printnum>
  8001c3:	83 c4 20             	add    $0x20,%esp
  8001c6:	eb 16                	jmp    8001de <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001c8:	83 ec 08             	sub    $0x8,%esp
  8001cb:	56                   	push   %esi
  8001cc:	ff 75 18             	pushl  0x18(%ebp)
  8001cf:	ff d7                	call   *%edi
  8001d1:	83 c4 10             	add    $0x10,%esp
  8001d4:	eb 03                	jmp    8001d9 <printnum+0x78>
  8001d6:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8001d9:	4b                   	dec    %ebx
  8001da:	85 db                	test   %ebx,%ebx
  8001dc:	7f ea                	jg     8001c8 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8001de:	83 ec 08             	sub    $0x8,%esp
  8001e1:	56                   	push   %esi
  8001e2:	83 ec 04             	sub    $0x4,%esp
  8001e5:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001e8:	ff 75 e0             	pushl  -0x20(%ebp)
  8001eb:	ff 75 dc             	pushl  -0x24(%ebp)
  8001ee:	ff 75 d8             	pushl  -0x28(%ebp)
  8001f1:	e8 d6 09 00 00       	call   800bcc <__umoddi3>
  8001f6:	83 c4 14             	add    $0x14,%esp
  8001f9:	0f be 80 4f 0d 80 00 	movsbl 0x800d4f(%eax),%eax
  800200:	50                   	push   %eax
  800201:	ff d7                	call   *%edi
}
  800203:	83 c4 10             	add    $0x10,%esp
  800206:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800209:	5b                   	pop    %ebx
  80020a:	5e                   	pop    %esi
  80020b:	5f                   	pop    %edi
  80020c:	5d                   	pop    %ebp
  80020d:	c3                   	ret    

0080020e <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80020e:	55                   	push   %ebp
  80020f:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800211:	83 fa 01             	cmp    $0x1,%edx
  800214:	7e 0e                	jle    800224 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800216:	8b 10                	mov    (%eax),%edx
  800218:	8d 4a 08             	lea    0x8(%edx),%ecx
  80021b:	89 08                	mov    %ecx,(%eax)
  80021d:	8b 02                	mov    (%edx),%eax
  80021f:	8b 52 04             	mov    0x4(%edx),%edx
  800222:	eb 22                	jmp    800246 <getuint+0x38>
	else if (lflag)
  800224:	85 d2                	test   %edx,%edx
  800226:	74 10                	je     800238 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800228:	8b 10                	mov    (%eax),%edx
  80022a:	8d 4a 04             	lea    0x4(%edx),%ecx
  80022d:	89 08                	mov    %ecx,(%eax)
  80022f:	8b 02                	mov    (%edx),%eax
  800231:	ba 00 00 00 00       	mov    $0x0,%edx
  800236:	eb 0e                	jmp    800246 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800238:	8b 10                	mov    (%eax),%edx
  80023a:	8d 4a 04             	lea    0x4(%edx),%ecx
  80023d:	89 08                	mov    %ecx,(%eax)
  80023f:	8b 02                	mov    (%edx),%eax
  800241:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800246:	5d                   	pop    %ebp
  800247:	c3                   	ret    

00800248 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800248:	55                   	push   %ebp
  800249:	89 e5                	mov    %esp,%ebp
  80024b:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80024e:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800251:	8b 10                	mov    (%eax),%edx
  800253:	3b 50 04             	cmp    0x4(%eax),%edx
  800256:	73 0a                	jae    800262 <sprintputch+0x1a>
		*b->buf++ = ch;
  800258:	8d 4a 01             	lea    0x1(%edx),%ecx
  80025b:	89 08                	mov    %ecx,(%eax)
  80025d:	8b 45 08             	mov    0x8(%ebp),%eax
  800260:	88 02                	mov    %al,(%edx)
}
  800262:	5d                   	pop    %ebp
  800263:	c3                   	ret    

00800264 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800264:	55                   	push   %ebp
  800265:	89 e5                	mov    %esp,%ebp
  800267:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80026a:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80026d:	50                   	push   %eax
  80026e:	ff 75 10             	pushl  0x10(%ebp)
  800271:	ff 75 0c             	pushl  0xc(%ebp)
  800274:	ff 75 08             	pushl  0x8(%ebp)
  800277:	e8 05 00 00 00       	call   800281 <vprintfmt>
	va_end(ap);
}
  80027c:	83 c4 10             	add    $0x10,%esp
  80027f:	c9                   	leave  
  800280:	c3                   	ret    

00800281 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800281:	55                   	push   %ebp
  800282:	89 e5                	mov    %esp,%ebp
  800284:	57                   	push   %edi
  800285:	56                   	push   %esi
  800286:	53                   	push   %ebx
  800287:	83 ec 2c             	sub    $0x2c,%esp
  80028a:	8b 75 08             	mov    0x8(%ebp),%esi
  80028d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800290:	8b 7d 10             	mov    0x10(%ebp),%edi
  800293:	eb 12                	jmp    8002a7 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800295:	85 c0                	test   %eax,%eax
  800297:	0f 84 68 03 00 00    	je     800605 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80029d:	83 ec 08             	sub    $0x8,%esp
  8002a0:	53                   	push   %ebx
  8002a1:	50                   	push   %eax
  8002a2:	ff d6                	call   *%esi
  8002a4:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8002a7:	47                   	inc    %edi
  8002a8:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  8002ac:	83 f8 25             	cmp    $0x25,%eax
  8002af:	75 e4                	jne    800295 <vprintfmt+0x14>
  8002b1:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002b5:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002bc:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002c3:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002ca:	ba 00 00 00 00       	mov    $0x0,%edx
  8002cf:	eb 07                	jmp    8002d8 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002d1:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8002d4:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002d8:	8d 47 01             	lea    0x1(%edi),%eax
  8002db:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8002de:	0f b6 0f             	movzbl (%edi),%ecx
  8002e1:	8a 07                	mov    (%edi),%al
  8002e3:	83 e8 23             	sub    $0x23,%eax
  8002e6:	3c 55                	cmp    $0x55,%al
  8002e8:	0f 87 fe 02 00 00    	ja     8005ec <vprintfmt+0x36b>
  8002ee:	0f b6 c0             	movzbl %al,%eax
  8002f1:	ff 24 85 dc 0d 80 00 	jmp    *0x800ddc(,%eax,4)
  8002f8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8002fb:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8002ff:	eb d7                	jmp    8002d8 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800301:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800304:	b8 00 00 00 00       	mov    $0x0,%eax
  800309:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  80030c:	8d 04 80             	lea    (%eax,%eax,4),%eax
  80030f:	01 c0                	add    %eax,%eax
  800311:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800315:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800318:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80031b:	83 fa 09             	cmp    $0x9,%edx
  80031e:	77 34                	ja     800354 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800320:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800321:	eb e9                	jmp    80030c <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800323:	8b 45 14             	mov    0x14(%ebp),%eax
  800326:	8d 48 04             	lea    0x4(%eax),%ecx
  800329:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80032c:	8b 00                	mov    (%eax),%eax
  80032e:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800331:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800334:	eb 24                	jmp    80035a <vprintfmt+0xd9>
  800336:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80033a:	79 07                	jns    800343 <vprintfmt+0xc2>
  80033c:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800343:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800346:	eb 90                	jmp    8002d8 <vprintfmt+0x57>
  800348:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80034b:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800352:	eb 84                	jmp    8002d8 <vprintfmt+0x57>
  800354:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800357:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80035a:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80035e:	0f 89 74 ff ff ff    	jns    8002d8 <vprintfmt+0x57>
				width = precision, precision = -1;
  800364:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800367:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80036a:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800371:	e9 62 ff ff ff       	jmp    8002d8 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800376:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800377:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80037a:	e9 59 ff ff ff       	jmp    8002d8 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80037f:	8b 45 14             	mov    0x14(%ebp),%eax
  800382:	8d 50 04             	lea    0x4(%eax),%edx
  800385:	89 55 14             	mov    %edx,0x14(%ebp)
  800388:	83 ec 08             	sub    $0x8,%esp
  80038b:	53                   	push   %ebx
  80038c:	ff 30                	pushl  (%eax)
  80038e:	ff d6                	call   *%esi
			break;
  800390:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800393:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800396:	e9 0c ff ff ff       	jmp    8002a7 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80039b:	8b 45 14             	mov    0x14(%ebp),%eax
  80039e:	8d 50 04             	lea    0x4(%eax),%edx
  8003a1:	89 55 14             	mov    %edx,0x14(%ebp)
  8003a4:	8b 00                	mov    (%eax),%eax
  8003a6:	85 c0                	test   %eax,%eax
  8003a8:	79 02                	jns    8003ac <vprintfmt+0x12b>
  8003aa:	f7 d8                	neg    %eax
  8003ac:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  8003ae:	83 f8 06             	cmp    $0x6,%eax
  8003b1:	7f 0b                	jg     8003be <vprintfmt+0x13d>
  8003b3:	8b 04 85 34 0f 80 00 	mov    0x800f34(,%eax,4),%eax
  8003ba:	85 c0                	test   %eax,%eax
  8003bc:	75 18                	jne    8003d6 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003be:	52                   	push   %edx
  8003bf:	68 67 0d 80 00       	push   $0x800d67
  8003c4:	53                   	push   %ebx
  8003c5:	56                   	push   %esi
  8003c6:	e8 99 fe ff ff       	call   800264 <printfmt>
  8003cb:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003ce:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8003d1:	e9 d1 fe ff ff       	jmp    8002a7 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8003d6:	50                   	push   %eax
  8003d7:	68 70 0d 80 00       	push   $0x800d70
  8003dc:	53                   	push   %ebx
  8003dd:	56                   	push   %esi
  8003de:	e8 81 fe ff ff       	call   800264 <printfmt>
  8003e3:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003e6:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003e9:	e9 b9 fe ff ff       	jmp    8002a7 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8003ee:	8b 45 14             	mov    0x14(%ebp),%eax
  8003f1:	8d 50 04             	lea    0x4(%eax),%edx
  8003f4:	89 55 14             	mov    %edx,0x14(%ebp)
  8003f7:	8b 38                	mov    (%eax),%edi
  8003f9:	85 ff                	test   %edi,%edi
  8003fb:	75 05                	jne    800402 <vprintfmt+0x181>
				p = "(null)";
  8003fd:	bf 60 0d 80 00       	mov    $0x800d60,%edi
			if (width > 0 && padc != '-')
  800402:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800406:	0f 8e 90 00 00 00    	jle    80049c <vprintfmt+0x21b>
  80040c:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800410:	0f 84 8e 00 00 00    	je     8004a4 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800416:	83 ec 08             	sub    $0x8,%esp
  800419:	ff 75 d0             	pushl  -0x30(%ebp)
  80041c:	57                   	push   %edi
  80041d:	e8 70 02 00 00       	call   800692 <strnlen>
  800422:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800425:	29 c1                	sub    %eax,%ecx
  800427:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80042a:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80042d:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800431:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800434:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800437:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800439:	eb 0d                	jmp    800448 <vprintfmt+0x1c7>
					putch(padc, putdat);
  80043b:	83 ec 08             	sub    $0x8,%esp
  80043e:	53                   	push   %ebx
  80043f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800442:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800444:	4f                   	dec    %edi
  800445:	83 c4 10             	add    $0x10,%esp
  800448:	85 ff                	test   %edi,%edi
  80044a:	7f ef                	jg     80043b <vprintfmt+0x1ba>
  80044c:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80044f:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800452:	89 c8                	mov    %ecx,%eax
  800454:	85 c9                	test   %ecx,%ecx
  800456:	79 05                	jns    80045d <vprintfmt+0x1dc>
  800458:	b8 00 00 00 00       	mov    $0x0,%eax
  80045d:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800460:	29 c1                	sub    %eax,%ecx
  800462:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800465:	89 75 08             	mov    %esi,0x8(%ebp)
  800468:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80046b:	eb 3d                	jmp    8004aa <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80046d:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800471:	74 19                	je     80048c <vprintfmt+0x20b>
  800473:	0f be c0             	movsbl %al,%eax
  800476:	83 e8 20             	sub    $0x20,%eax
  800479:	83 f8 5e             	cmp    $0x5e,%eax
  80047c:	76 0e                	jbe    80048c <vprintfmt+0x20b>
					putch('?', putdat);
  80047e:	83 ec 08             	sub    $0x8,%esp
  800481:	53                   	push   %ebx
  800482:	6a 3f                	push   $0x3f
  800484:	ff 55 08             	call   *0x8(%ebp)
  800487:	83 c4 10             	add    $0x10,%esp
  80048a:	eb 0b                	jmp    800497 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80048c:	83 ec 08             	sub    $0x8,%esp
  80048f:	53                   	push   %ebx
  800490:	52                   	push   %edx
  800491:	ff 55 08             	call   *0x8(%ebp)
  800494:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800497:	ff 4d e4             	decl   -0x1c(%ebp)
  80049a:	eb 0e                	jmp    8004aa <vprintfmt+0x229>
  80049c:	89 75 08             	mov    %esi,0x8(%ebp)
  80049f:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004a2:	eb 06                	jmp    8004aa <vprintfmt+0x229>
  8004a4:	89 75 08             	mov    %esi,0x8(%ebp)
  8004a7:	8b 75 d0             	mov    -0x30(%ebp),%esi
  8004aa:	47                   	inc    %edi
  8004ab:	8a 47 ff             	mov    -0x1(%edi),%al
  8004ae:	0f be d0             	movsbl %al,%edx
  8004b1:	85 d2                	test   %edx,%edx
  8004b3:	74 1d                	je     8004d2 <vprintfmt+0x251>
  8004b5:	85 f6                	test   %esi,%esi
  8004b7:	78 b4                	js     80046d <vprintfmt+0x1ec>
  8004b9:	4e                   	dec    %esi
  8004ba:	79 b1                	jns    80046d <vprintfmt+0x1ec>
  8004bc:	8b 75 08             	mov    0x8(%ebp),%esi
  8004bf:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004c2:	eb 14                	jmp    8004d8 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004c4:	83 ec 08             	sub    $0x8,%esp
  8004c7:	53                   	push   %ebx
  8004c8:	6a 20                	push   $0x20
  8004ca:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004cc:	4f                   	dec    %edi
  8004cd:	83 c4 10             	add    $0x10,%esp
  8004d0:	eb 06                	jmp    8004d8 <vprintfmt+0x257>
  8004d2:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004d5:	8b 75 08             	mov    0x8(%ebp),%esi
  8004d8:	85 ff                	test   %edi,%edi
  8004da:	7f e8                	jg     8004c4 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004dc:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004df:	e9 c3 fd ff ff       	jmp    8002a7 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8004e4:	83 fa 01             	cmp    $0x1,%edx
  8004e7:	7e 16                	jle    8004ff <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8004e9:	8b 45 14             	mov    0x14(%ebp),%eax
  8004ec:	8d 50 08             	lea    0x8(%eax),%edx
  8004ef:	89 55 14             	mov    %edx,0x14(%ebp)
  8004f2:	8b 50 04             	mov    0x4(%eax),%edx
  8004f5:	8b 00                	mov    (%eax),%eax
  8004f7:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8004fa:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8004fd:	eb 32                	jmp    800531 <vprintfmt+0x2b0>
	else if (lflag)
  8004ff:	85 d2                	test   %edx,%edx
  800501:	74 18                	je     80051b <vprintfmt+0x29a>
		return va_arg(*ap, long);
  800503:	8b 45 14             	mov    0x14(%ebp),%eax
  800506:	8d 50 04             	lea    0x4(%eax),%edx
  800509:	89 55 14             	mov    %edx,0x14(%ebp)
  80050c:	8b 00                	mov    (%eax),%eax
  80050e:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800511:	89 c1                	mov    %eax,%ecx
  800513:	c1 f9 1f             	sar    $0x1f,%ecx
  800516:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800519:	eb 16                	jmp    800531 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80051b:	8b 45 14             	mov    0x14(%ebp),%eax
  80051e:	8d 50 04             	lea    0x4(%eax),%edx
  800521:	89 55 14             	mov    %edx,0x14(%ebp)
  800524:	8b 00                	mov    (%eax),%eax
  800526:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800529:	89 c1                	mov    %eax,%ecx
  80052b:	c1 f9 1f             	sar    $0x1f,%ecx
  80052e:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800531:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800534:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800537:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80053b:	79 76                	jns    8005b3 <vprintfmt+0x332>
				putch('-', putdat);
  80053d:	83 ec 08             	sub    $0x8,%esp
  800540:	53                   	push   %ebx
  800541:	6a 2d                	push   $0x2d
  800543:	ff d6                	call   *%esi
				num = -(long long) num;
  800545:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800548:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80054b:	f7 d8                	neg    %eax
  80054d:	83 d2 00             	adc    $0x0,%edx
  800550:	f7 da                	neg    %edx
  800552:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800555:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80055a:	eb 5c                	jmp    8005b8 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80055c:	8d 45 14             	lea    0x14(%ebp),%eax
  80055f:	e8 aa fc ff ff       	call   80020e <getuint>
			base = 10;
  800564:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800569:	eb 4d                	jmp    8005b8 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80056b:	8d 45 14             	lea    0x14(%ebp),%eax
  80056e:	e8 9b fc ff ff       	call   80020e <getuint>
			base = 8;
  800573:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800578:	eb 3e                	jmp    8005b8 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80057a:	83 ec 08             	sub    $0x8,%esp
  80057d:	53                   	push   %ebx
  80057e:	6a 30                	push   $0x30
  800580:	ff d6                	call   *%esi
			putch('x', putdat);
  800582:	83 c4 08             	add    $0x8,%esp
  800585:	53                   	push   %ebx
  800586:	6a 78                	push   $0x78
  800588:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80058a:	8b 45 14             	mov    0x14(%ebp),%eax
  80058d:	8d 50 04             	lea    0x4(%eax),%edx
  800590:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800593:	8b 00                	mov    (%eax),%eax
  800595:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80059a:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80059d:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  8005a2:	eb 14                	jmp    8005b8 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8005a4:	8d 45 14             	lea    0x14(%ebp),%eax
  8005a7:	e8 62 fc ff ff       	call   80020e <getuint>
			base = 16;
  8005ac:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005b1:	eb 05                	jmp    8005b8 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005b3:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005b8:	83 ec 0c             	sub    $0xc,%esp
  8005bb:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005bf:	57                   	push   %edi
  8005c0:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005c3:	51                   	push   %ecx
  8005c4:	52                   	push   %edx
  8005c5:	50                   	push   %eax
  8005c6:	89 da                	mov    %ebx,%edx
  8005c8:	89 f0                	mov    %esi,%eax
  8005ca:	e8 92 fb ff ff       	call   800161 <printnum>
			break;
  8005cf:	83 c4 20             	add    $0x20,%esp
  8005d2:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005d5:	e9 cd fc ff ff       	jmp    8002a7 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8005da:	83 ec 08             	sub    $0x8,%esp
  8005dd:	53                   	push   %ebx
  8005de:	51                   	push   %ecx
  8005df:	ff d6                	call   *%esi
			break;
  8005e1:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005e4:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8005e7:	e9 bb fc ff ff       	jmp    8002a7 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8005ec:	83 ec 08             	sub    $0x8,%esp
  8005ef:	53                   	push   %ebx
  8005f0:	6a 25                	push   $0x25
  8005f2:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8005f4:	83 c4 10             	add    $0x10,%esp
  8005f7:	eb 01                	jmp    8005fa <vprintfmt+0x379>
  8005f9:	4f                   	dec    %edi
  8005fa:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8005fe:	75 f9                	jne    8005f9 <vprintfmt+0x378>
  800600:	e9 a2 fc ff ff       	jmp    8002a7 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  800605:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800608:	5b                   	pop    %ebx
  800609:	5e                   	pop    %esi
  80060a:	5f                   	pop    %edi
  80060b:	5d                   	pop    %ebp
  80060c:	c3                   	ret    

0080060d <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80060d:	55                   	push   %ebp
  80060e:	89 e5                	mov    %esp,%ebp
  800610:	83 ec 18             	sub    $0x18,%esp
  800613:	8b 45 08             	mov    0x8(%ebp),%eax
  800616:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800619:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80061c:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800620:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800623:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80062a:	85 c0                	test   %eax,%eax
  80062c:	74 26                	je     800654 <vsnprintf+0x47>
  80062e:	85 d2                	test   %edx,%edx
  800630:	7e 29                	jle    80065b <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800632:	ff 75 14             	pushl  0x14(%ebp)
  800635:	ff 75 10             	pushl  0x10(%ebp)
  800638:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80063b:	50                   	push   %eax
  80063c:	68 48 02 80 00       	push   $0x800248
  800641:	e8 3b fc ff ff       	call   800281 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800646:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800649:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80064c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80064f:	83 c4 10             	add    $0x10,%esp
  800652:	eb 0c                	jmp    800660 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800654:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800659:	eb 05                	jmp    800660 <vsnprintf+0x53>
  80065b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800660:	c9                   	leave  
  800661:	c3                   	ret    

00800662 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800662:	55                   	push   %ebp
  800663:	89 e5                	mov    %esp,%ebp
  800665:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800668:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80066b:	50                   	push   %eax
  80066c:	ff 75 10             	pushl  0x10(%ebp)
  80066f:	ff 75 0c             	pushl  0xc(%ebp)
  800672:	ff 75 08             	pushl  0x8(%ebp)
  800675:	e8 93 ff ff ff       	call   80060d <vsnprintf>
	va_end(ap);

	return rc;
}
  80067a:	c9                   	leave  
  80067b:	c3                   	ret    

0080067c <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80067c:	55                   	push   %ebp
  80067d:	89 e5                	mov    %esp,%ebp
  80067f:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800682:	b8 00 00 00 00       	mov    $0x0,%eax
  800687:	eb 01                	jmp    80068a <strlen+0xe>
		n++;
  800689:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80068a:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80068e:	75 f9                	jne    800689 <strlen+0xd>
		n++;
	return n;
}
  800690:	5d                   	pop    %ebp
  800691:	c3                   	ret    

00800692 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800692:	55                   	push   %ebp
  800693:	89 e5                	mov    %esp,%ebp
  800695:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800698:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80069b:	ba 00 00 00 00       	mov    $0x0,%edx
  8006a0:	eb 01                	jmp    8006a3 <strnlen+0x11>
		n++;
  8006a2:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8006a3:	39 c2                	cmp    %eax,%edx
  8006a5:	74 08                	je     8006af <strnlen+0x1d>
  8006a7:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  8006ab:	75 f5                	jne    8006a2 <strnlen+0x10>
  8006ad:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  8006af:	5d                   	pop    %ebp
  8006b0:	c3                   	ret    

008006b1 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006b1:	55                   	push   %ebp
  8006b2:	89 e5                	mov    %esp,%ebp
  8006b4:	53                   	push   %ebx
  8006b5:	8b 45 08             	mov    0x8(%ebp),%eax
  8006b8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006bb:	89 c2                	mov    %eax,%edx
  8006bd:	42                   	inc    %edx
  8006be:	41                   	inc    %ecx
  8006bf:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006c2:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006c5:	84 db                	test   %bl,%bl
  8006c7:	75 f4                	jne    8006bd <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006c9:	5b                   	pop    %ebx
  8006ca:	5d                   	pop    %ebp
  8006cb:	c3                   	ret    

008006cc <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006cc:	55                   	push   %ebp
  8006cd:	89 e5                	mov    %esp,%ebp
  8006cf:	53                   	push   %ebx
  8006d0:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8006d3:	53                   	push   %ebx
  8006d4:	e8 a3 ff ff ff       	call   80067c <strlen>
  8006d9:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8006dc:	ff 75 0c             	pushl  0xc(%ebp)
  8006df:	01 d8                	add    %ebx,%eax
  8006e1:	50                   	push   %eax
  8006e2:	e8 ca ff ff ff       	call   8006b1 <strcpy>
	return dst;
}
  8006e7:	89 d8                	mov    %ebx,%eax
  8006e9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8006ec:	c9                   	leave  
  8006ed:	c3                   	ret    

008006ee <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8006ee:	55                   	push   %ebp
  8006ef:	89 e5                	mov    %esp,%ebp
  8006f1:	56                   	push   %esi
  8006f2:	53                   	push   %ebx
  8006f3:	8b 75 08             	mov    0x8(%ebp),%esi
  8006f6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8006f9:	89 f3                	mov    %esi,%ebx
  8006fb:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8006fe:	89 f2                	mov    %esi,%edx
  800700:	eb 0c                	jmp    80070e <strncpy+0x20>
		*dst++ = *src;
  800702:	42                   	inc    %edx
  800703:	8a 01                	mov    (%ecx),%al
  800705:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  800708:	80 39 01             	cmpb   $0x1,(%ecx)
  80070b:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80070e:	39 da                	cmp    %ebx,%edx
  800710:	75 f0                	jne    800702 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800712:	89 f0                	mov    %esi,%eax
  800714:	5b                   	pop    %ebx
  800715:	5e                   	pop    %esi
  800716:	5d                   	pop    %ebp
  800717:	c3                   	ret    

00800718 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800718:	55                   	push   %ebp
  800719:	89 e5                	mov    %esp,%ebp
  80071b:	56                   	push   %esi
  80071c:	53                   	push   %ebx
  80071d:	8b 75 08             	mov    0x8(%ebp),%esi
  800720:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800723:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800726:	85 c0                	test   %eax,%eax
  800728:	74 1e                	je     800748 <strlcpy+0x30>
  80072a:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  80072e:	89 f2                	mov    %esi,%edx
  800730:	eb 05                	jmp    800737 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800732:	42                   	inc    %edx
  800733:	41                   	inc    %ecx
  800734:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800737:	39 c2                	cmp    %eax,%edx
  800739:	74 08                	je     800743 <strlcpy+0x2b>
  80073b:	8a 19                	mov    (%ecx),%bl
  80073d:	84 db                	test   %bl,%bl
  80073f:	75 f1                	jne    800732 <strlcpy+0x1a>
  800741:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800743:	c6 00 00             	movb   $0x0,(%eax)
  800746:	eb 02                	jmp    80074a <strlcpy+0x32>
  800748:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  80074a:	29 f0                	sub    %esi,%eax
}
  80074c:	5b                   	pop    %ebx
  80074d:	5e                   	pop    %esi
  80074e:	5d                   	pop    %ebp
  80074f:	c3                   	ret    

00800750 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800750:	55                   	push   %ebp
  800751:	89 e5                	mov    %esp,%ebp
  800753:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800756:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800759:	eb 02                	jmp    80075d <strcmp+0xd>
		p++, q++;
  80075b:	41                   	inc    %ecx
  80075c:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80075d:	8a 01                	mov    (%ecx),%al
  80075f:	84 c0                	test   %al,%al
  800761:	74 04                	je     800767 <strcmp+0x17>
  800763:	3a 02                	cmp    (%edx),%al
  800765:	74 f4                	je     80075b <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800767:	0f b6 c0             	movzbl %al,%eax
  80076a:	0f b6 12             	movzbl (%edx),%edx
  80076d:	29 d0                	sub    %edx,%eax
}
  80076f:	5d                   	pop    %ebp
  800770:	c3                   	ret    

00800771 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800771:	55                   	push   %ebp
  800772:	89 e5                	mov    %esp,%ebp
  800774:	53                   	push   %ebx
  800775:	8b 45 08             	mov    0x8(%ebp),%eax
  800778:	8b 55 0c             	mov    0xc(%ebp),%edx
  80077b:	89 c3                	mov    %eax,%ebx
  80077d:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800780:	eb 02                	jmp    800784 <strncmp+0x13>
		n--, p++, q++;
  800782:	40                   	inc    %eax
  800783:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800784:	39 d8                	cmp    %ebx,%eax
  800786:	74 14                	je     80079c <strncmp+0x2b>
  800788:	8a 08                	mov    (%eax),%cl
  80078a:	84 c9                	test   %cl,%cl
  80078c:	74 04                	je     800792 <strncmp+0x21>
  80078e:	3a 0a                	cmp    (%edx),%cl
  800790:	74 f0                	je     800782 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800792:	0f b6 00             	movzbl (%eax),%eax
  800795:	0f b6 12             	movzbl (%edx),%edx
  800798:	29 d0                	sub    %edx,%eax
  80079a:	eb 05                	jmp    8007a1 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80079c:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  8007a1:	5b                   	pop    %ebx
  8007a2:	5d                   	pop    %ebp
  8007a3:	c3                   	ret    

008007a4 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8007a4:	55                   	push   %ebp
  8007a5:	89 e5                	mov    %esp,%ebp
  8007a7:	8b 45 08             	mov    0x8(%ebp),%eax
  8007aa:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007ad:	eb 05                	jmp    8007b4 <strchr+0x10>
		if (*s == c)
  8007af:	38 ca                	cmp    %cl,%dl
  8007b1:	74 0c                	je     8007bf <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007b3:	40                   	inc    %eax
  8007b4:	8a 10                	mov    (%eax),%dl
  8007b6:	84 d2                	test   %dl,%dl
  8007b8:	75 f5                	jne    8007af <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007ba:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007bf:	5d                   	pop    %ebp
  8007c0:	c3                   	ret    

008007c1 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007c1:	55                   	push   %ebp
  8007c2:	89 e5                	mov    %esp,%ebp
  8007c4:	8b 45 08             	mov    0x8(%ebp),%eax
  8007c7:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007ca:	eb 05                	jmp    8007d1 <strfind+0x10>
		if (*s == c)
  8007cc:	38 ca                	cmp    %cl,%dl
  8007ce:	74 07                	je     8007d7 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007d0:	40                   	inc    %eax
  8007d1:	8a 10                	mov    (%eax),%dl
  8007d3:	84 d2                	test   %dl,%dl
  8007d5:	75 f5                	jne    8007cc <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8007d7:	5d                   	pop    %ebp
  8007d8:	c3                   	ret    

008007d9 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8007d9:	55                   	push   %ebp
  8007da:	89 e5                	mov    %esp,%ebp
  8007dc:	57                   	push   %edi
  8007dd:	56                   	push   %esi
  8007de:	53                   	push   %ebx
  8007df:	8b 7d 08             	mov    0x8(%ebp),%edi
  8007e2:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8007e5:	85 c9                	test   %ecx,%ecx
  8007e7:	74 36                	je     80081f <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8007e9:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8007ef:	75 28                	jne    800819 <memset+0x40>
  8007f1:	f6 c1 03             	test   $0x3,%cl
  8007f4:	75 23                	jne    800819 <memset+0x40>
		c &= 0xFF;
  8007f6:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8007fa:	89 d3                	mov    %edx,%ebx
  8007fc:	c1 e3 08             	shl    $0x8,%ebx
  8007ff:	89 d6                	mov    %edx,%esi
  800801:	c1 e6 18             	shl    $0x18,%esi
  800804:	89 d0                	mov    %edx,%eax
  800806:	c1 e0 10             	shl    $0x10,%eax
  800809:	09 f0                	or     %esi,%eax
  80080b:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  80080d:	89 d8                	mov    %ebx,%eax
  80080f:	09 d0                	or     %edx,%eax
  800811:	c1 e9 02             	shr    $0x2,%ecx
  800814:	fc                   	cld    
  800815:	f3 ab                	rep stos %eax,%es:(%edi)
  800817:	eb 06                	jmp    80081f <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800819:	8b 45 0c             	mov    0xc(%ebp),%eax
  80081c:	fc                   	cld    
  80081d:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  80081f:	89 f8                	mov    %edi,%eax
  800821:	5b                   	pop    %ebx
  800822:	5e                   	pop    %esi
  800823:	5f                   	pop    %edi
  800824:	5d                   	pop    %ebp
  800825:	c3                   	ret    

00800826 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800826:	55                   	push   %ebp
  800827:	89 e5                	mov    %esp,%ebp
  800829:	57                   	push   %edi
  80082a:	56                   	push   %esi
  80082b:	8b 45 08             	mov    0x8(%ebp),%eax
  80082e:	8b 75 0c             	mov    0xc(%ebp),%esi
  800831:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800834:	39 c6                	cmp    %eax,%esi
  800836:	73 33                	jae    80086b <memmove+0x45>
  800838:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80083b:	39 d0                	cmp    %edx,%eax
  80083d:	73 2c                	jae    80086b <memmove+0x45>
		s += n;
		d += n;
  80083f:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800842:	89 d6                	mov    %edx,%esi
  800844:	09 fe                	or     %edi,%esi
  800846:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80084c:	75 13                	jne    800861 <memmove+0x3b>
  80084e:	f6 c1 03             	test   $0x3,%cl
  800851:	75 0e                	jne    800861 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800853:	83 ef 04             	sub    $0x4,%edi
  800856:	8d 72 fc             	lea    -0x4(%edx),%esi
  800859:	c1 e9 02             	shr    $0x2,%ecx
  80085c:	fd                   	std    
  80085d:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80085f:	eb 07                	jmp    800868 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800861:	4f                   	dec    %edi
  800862:	8d 72 ff             	lea    -0x1(%edx),%esi
  800865:	fd                   	std    
  800866:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800868:	fc                   	cld    
  800869:	eb 1d                	jmp    800888 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80086b:	89 f2                	mov    %esi,%edx
  80086d:	09 c2                	or     %eax,%edx
  80086f:	f6 c2 03             	test   $0x3,%dl
  800872:	75 0f                	jne    800883 <memmove+0x5d>
  800874:	f6 c1 03             	test   $0x3,%cl
  800877:	75 0a                	jne    800883 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800879:	c1 e9 02             	shr    $0x2,%ecx
  80087c:	89 c7                	mov    %eax,%edi
  80087e:	fc                   	cld    
  80087f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800881:	eb 05                	jmp    800888 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800883:	89 c7                	mov    %eax,%edi
  800885:	fc                   	cld    
  800886:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800888:	5e                   	pop    %esi
  800889:	5f                   	pop    %edi
  80088a:	5d                   	pop    %ebp
  80088b:	c3                   	ret    

0080088c <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80088c:	55                   	push   %ebp
  80088d:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  80088f:	ff 75 10             	pushl  0x10(%ebp)
  800892:	ff 75 0c             	pushl  0xc(%ebp)
  800895:	ff 75 08             	pushl  0x8(%ebp)
  800898:	e8 89 ff ff ff       	call   800826 <memmove>
}
  80089d:	c9                   	leave  
  80089e:	c3                   	ret    

0080089f <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  80089f:	55                   	push   %ebp
  8008a0:	89 e5                	mov    %esp,%ebp
  8008a2:	56                   	push   %esi
  8008a3:	53                   	push   %ebx
  8008a4:	8b 45 08             	mov    0x8(%ebp),%eax
  8008a7:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008aa:	89 c6                	mov    %eax,%esi
  8008ac:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008af:	eb 14                	jmp    8008c5 <memcmp+0x26>
		if (*s1 != *s2)
  8008b1:	8a 08                	mov    (%eax),%cl
  8008b3:	8a 1a                	mov    (%edx),%bl
  8008b5:	38 d9                	cmp    %bl,%cl
  8008b7:	74 0a                	je     8008c3 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008b9:	0f b6 c1             	movzbl %cl,%eax
  8008bc:	0f b6 db             	movzbl %bl,%ebx
  8008bf:	29 d8                	sub    %ebx,%eax
  8008c1:	eb 0b                	jmp    8008ce <memcmp+0x2f>
		s1++, s2++;
  8008c3:	40                   	inc    %eax
  8008c4:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008c5:	39 f0                	cmp    %esi,%eax
  8008c7:	75 e8                	jne    8008b1 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008c9:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008ce:	5b                   	pop    %ebx
  8008cf:	5e                   	pop    %esi
  8008d0:	5d                   	pop    %ebp
  8008d1:	c3                   	ret    

008008d2 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8008d2:	55                   	push   %ebp
  8008d3:	89 e5                	mov    %esp,%ebp
  8008d5:	53                   	push   %ebx
  8008d6:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8008d9:	89 c1                	mov    %eax,%ecx
  8008db:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8008de:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008e2:	eb 08                	jmp    8008ec <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8008e4:	0f b6 10             	movzbl (%eax),%edx
  8008e7:	39 da                	cmp    %ebx,%edx
  8008e9:	74 05                	je     8008f0 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008eb:	40                   	inc    %eax
  8008ec:	39 c8                	cmp    %ecx,%eax
  8008ee:	72 f4                	jb     8008e4 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8008f0:	5b                   	pop    %ebx
  8008f1:	5d                   	pop    %ebp
  8008f2:	c3                   	ret    

008008f3 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8008f3:	55                   	push   %ebp
  8008f4:	89 e5                	mov    %esp,%ebp
  8008f6:	57                   	push   %edi
  8008f7:	56                   	push   %esi
  8008f8:	53                   	push   %ebx
  8008f9:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8008fc:	eb 01                	jmp    8008ff <strtol+0xc>
		s++;
  8008fe:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8008ff:	8a 01                	mov    (%ecx),%al
  800901:	3c 20                	cmp    $0x20,%al
  800903:	74 f9                	je     8008fe <strtol+0xb>
  800905:	3c 09                	cmp    $0x9,%al
  800907:	74 f5                	je     8008fe <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  800909:	3c 2b                	cmp    $0x2b,%al
  80090b:	75 08                	jne    800915 <strtol+0x22>
		s++;
  80090d:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  80090e:	bf 00 00 00 00       	mov    $0x0,%edi
  800913:	eb 11                	jmp    800926 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800915:	3c 2d                	cmp    $0x2d,%al
  800917:	75 08                	jne    800921 <strtol+0x2e>
		s++, neg = 1;
  800919:	41                   	inc    %ecx
  80091a:	bf 01 00 00 00       	mov    $0x1,%edi
  80091f:	eb 05                	jmp    800926 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800921:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800926:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80092a:	0f 84 87 00 00 00    	je     8009b7 <strtol+0xc4>
  800930:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800934:	75 27                	jne    80095d <strtol+0x6a>
  800936:	80 39 30             	cmpb   $0x30,(%ecx)
  800939:	75 22                	jne    80095d <strtol+0x6a>
  80093b:	e9 88 00 00 00       	jmp    8009c8 <strtol+0xd5>
		s += 2, base = 16;
  800940:	83 c1 02             	add    $0x2,%ecx
  800943:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  80094a:	eb 11                	jmp    80095d <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  80094c:	41                   	inc    %ecx
  80094d:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800954:	eb 07                	jmp    80095d <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800956:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  80095d:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800962:	8a 11                	mov    (%ecx),%dl
  800964:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800967:	80 fb 09             	cmp    $0x9,%bl
  80096a:	77 08                	ja     800974 <strtol+0x81>
			dig = *s - '0';
  80096c:	0f be d2             	movsbl %dl,%edx
  80096f:	83 ea 30             	sub    $0x30,%edx
  800972:	eb 22                	jmp    800996 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800974:	8d 72 9f             	lea    -0x61(%edx),%esi
  800977:	89 f3                	mov    %esi,%ebx
  800979:	80 fb 19             	cmp    $0x19,%bl
  80097c:	77 08                	ja     800986 <strtol+0x93>
			dig = *s - 'a' + 10;
  80097e:	0f be d2             	movsbl %dl,%edx
  800981:	83 ea 57             	sub    $0x57,%edx
  800984:	eb 10                	jmp    800996 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800986:	8d 72 bf             	lea    -0x41(%edx),%esi
  800989:	89 f3                	mov    %esi,%ebx
  80098b:	80 fb 19             	cmp    $0x19,%bl
  80098e:	77 14                	ja     8009a4 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800990:	0f be d2             	movsbl %dl,%edx
  800993:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800996:	3b 55 10             	cmp    0x10(%ebp),%edx
  800999:	7d 09                	jge    8009a4 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  80099b:	41                   	inc    %ecx
  80099c:	0f af 45 10          	imul   0x10(%ebp),%eax
  8009a0:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  8009a2:	eb be                	jmp    800962 <strtol+0x6f>

	if (endptr)
  8009a4:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009a8:	74 05                	je     8009af <strtol+0xbc>
		*endptr = (char *) s;
  8009aa:	8b 75 0c             	mov    0xc(%ebp),%esi
  8009ad:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  8009af:	85 ff                	test   %edi,%edi
  8009b1:	74 21                	je     8009d4 <strtol+0xe1>
  8009b3:	f7 d8                	neg    %eax
  8009b5:	eb 1d                	jmp    8009d4 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009b7:	80 39 30             	cmpb   $0x30,(%ecx)
  8009ba:	75 9a                	jne    800956 <strtol+0x63>
  8009bc:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009c0:	0f 84 7a ff ff ff    	je     800940 <strtol+0x4d>
  8009c6:	eb 84                	jmp    80094c <strtol+0x59>
  8009c8:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009cc:	0f 84 6e ff ff ff    	je     800940 <strtol+0x4d>
  8009d2:	eb 89                	jmp    80095d <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  8009d4:	5b                   	pop    %ebx
  8009d5:	5e                   	pop    %esi
  8009d6:	5f                   	pop    %edi
  8009d7:	5d                   	pop    %ebp
  8009d8:	c3                   	ret    

008009d9 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8009d9:	55                   	push   %ebp
  8009da:	89 e5                	mov    %esp,%ebp
  8009dc:	57                   	push   %edi
  8009dd:	56                   	push   %esi
  8009de:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8009df:	b8 00 00 00 00       	mov    $0x0,%eax
  8009e4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009e7:	8b 55 08             	mov    0x8(%ebp),%edx
  8009ea:	89 c3                	mov    %eax,%ebx
  8009ec:	89 c7                	mov    %eax,%edi
  8009ee:	89 c6                	mov    %eax,%esi
  8009f0:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8009f2:	5b                   	pop    %ebx
  8009f3:	5e                   	pop    %esi
  8009f4:	5f                   	pop    %edi
  8009f5:	5d                   	pop    %ebp
  8009f6:	c3                   	ret    

008009f7 <sys_cgetc>:

int
sys_cgetc(void)
{
  8009f7:	55                   	push   %ebp
  8009f8:	89 e5                	mov    %esp,%ebp
  8009fa:	57                   	push   %edi
  8009fb:	56                   	push   %esi
  8009fc:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8009fd:	ba 00 00 00 00       	mov    $0x0,%edx
  800a02:	b8 01 00 00 00       	mov    $0x1,%eax
  800a07:	89 d1                	mov    %edx,%ecx
  800a09:	89 d3                	mov    %edx,%ebx
  800a0b:	89 d7                	mov    %edx,%edi
  800a0d:	89 d6                	mov    %edx,%esi
  800a0f:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a11:	5b                   	pop    %ebx
  800a12:	5e                   	pop    %esi
  800a13:	5f                   	pop    %edi
  800a14:	5d                   	pop    %ebp
  800a15:	c3                   	ret    

00800a16 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a16:	55                   	push   %ebp
  800a17:	89 e5                	mov    %esp,%ebp
  800a19:	57                   	push   %edi
  800a1a:	56                   	push   %esi
  800a1b:	53                   	push   %ebx
  800a1c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a1f:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a24:	b8 03 00 00 00       	mov    $0x3,%eax
  800a29:	8b 55 08             	mov    0x8(%ebp),%edx
  800a2c:	89 cb                	mov    %ecx,%ebx
  800a2e:	89 cf                	mov    %ecx,%edi
  800a30:	89 ce                	mov    %ecx,%esi
  800a32:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a34:	85 c0                	test   %eax,%eax
  800a36:	7e 17                	jle    800a4f <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a38:	83 ec 0c             	sub    $0xc,%esp
  800a3b:	50                   	push   %eax
  800a3c:	6a 03                	push   $0x3
  800a3e:	68 50 0f 80 00       	push   $0x800f50
  800a43:	6a 23                	push   $0x23
  800a45:	68 6d 0f 80 00       	push   $0x800f6d
  800a4a:	e8 27 00 00 00       	call   800a76 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a4f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a52:	5b                   	pop    %ebx
  800a53:	5e                   	pop    %esi
  800a54:	5f                   	pop    %edi
  800a55:	5d                   	pop    %ebp
  800a56:	c3                   	ret    

00800a57 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a57:	55                   	push   %ebp
  800a58:	89 e5                	mov    %esp,%ebp
  800a5a:	57                   	push   %edi
  800a5b:	56                   	push   %esi
  800a5c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a5d:	ba 00 00 00 00       	mov    $0x0,%edx
  800a62:	b8 02 00 00 00       	mov    $0x2,%eax
  800a67:	89 d1                	mov    %edx,%ecx
  800a69:	89 d3                	mov    %edx,%ebx
  800a6b:	89 d7                	mov    %edx,%edi
  800a6d:	89 d6                	mov    %edx,%esi
  800a6f:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800a71:	5b                   	pop    %ebx
  800a72:	5e                   	pop    %esi
  800a73:	5f                   	pop    %edi
  800a74:	5d                   	pop    %ebp
  800a75:	c3                   	ret    

00800a76 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800a76:	55                   	push   %ebp
  800a77:	89 e5                	mov    %esp,%ebp
  800a79:	56                   	push   %esi
  800a7a:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800a7b:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800a7e:	8b 35 00 10 80 00    	mov    0x801000,%esi
  800a84:	e8 ce ff ff ff       	call   800a57 <sys_getenvid>
  800a89:	83 ec 0c             	sub    $0xc,%esp
  800a8c:	ff 75 0c             	pushl  0xc(%ebp)
  800a8f:	ff 75 08             	pushl  0x8(%ebp)
  800a92:	56                   	push   %esi
  800a93:	50                   	push   %eax
  800a94:	68 7c 0f 80 00       	push   $0x800f7c
  800a99:	e8 af f6 ff ff       	call   80014d <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800a9e:	83 c4 18             	add    $0x18,%esp
  800aa1:	53                   	push   %ebx
  800aa2:	ff 75 10             	pushl  0x10(%ebp)
  800aa5:	e8 52 f6 ff ff       	call   8000fc <vcprintf>
	cprintf("\n");
  800aaa:	c7 04 24 2c 0d 80 00 	movl   $0x800d2c,(%esp)
  800ab1:	e8 97 f6 ff ff       	call   80014d <cprintf>
  800ab6:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800ab9:	cc                   	int3   
  800aba:	eb fd                	jmp    800ab9 <_panic+0x43>

00800abc <__udivdi3>:
  800abc:	55                   	push   %ebp
  800abd:	57                   	push   %edi
  800abe:	56                   	push   %esi
  800abf:	53                   	push   %ebx
  800ac0:	83 ec 1c             	sub    $0x1c,%esp
  800ac3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ac7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800acb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800acf:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800ad3:	89 ca                	mov    %ecx,%edx
  800ad5:	89 f8                	mov    %edi,%eax
  800ad7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800adb:	85 f6                	test   %esi,%esi
  800add:	75 2d                	jne    800b0c <__udivdi3+0x50>
  800adf:	39 cf                	cmp    %ecx,%edi
  800ae1:	77 65                	ja     800b48 <__udivdi3+0x8c>
  800ae3:	89 fd                	mov    %edi,%ebp
  800ae5:	85 ff                	test   %edi,%edi
  800ae7:	75 0b                	jne    800af4 <__udivdi3+0x38>
  800ae9:	b8 01 00 00 00       	mov    $0x1,%eax
  800aee:	31 d2                	xor    %edx,%edx
  800af0:	f7 f7                	div    %edi
  800af2:	89 c5                	mov    %eax,%ebp
  800af4:	31 d2                	xor    %edx,%edx
  800af6:	89 c8                	mov    %ecx,%eax
  800af8:	f7 f5                	div    %ebp
  800afa:	89 c1                	mov    %eax,%ecx
  800afc:	89 d8                	mov    %ebx,%eax
  800afe:	f7 f5                	div    %ebp
  800b00:	89 cf                	mov    %ecx,%edi
  800b02:	89 fa                	mov    %edi,%edx
  800b04:	83 c4 1c             	add    $0x1c,%esp
  800b07:	5b                   	pop    %ebx
  800b08:	5e                   	pop    %esi
  800b09:	5f                   	pop    %edi
  800b0a:	5d                   	pop    %ebp
  800b0b:	c3                   	ret    
  800b0c:	39 ce                	cmp    %ecx,%esi
  800b0e:	77 28                	ja     800b38 <__udivdi3+0x7c>
  800b10:	0f bd fe             	bsr    %esi,%edi
  800b13:	83 f7 1f             	xor    $0x1f,%edi
  800b16:	75 40                	jne    800b58 <__udivdi3+0x9c>
  800b18:	39 ce                	cmp    %ecx,%esi
  800b1a:	72 0a                	jb     800b26 <__udivdi3+0x6a>
  800b1c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b20:	0f 87 9e 00 00 00    	ja     800bc4 <__udivdi3+0x108>
  800b26:	b8 01 00 00 00       	mov    $0x1,%eax
  800b2b:	89 fa                	mov    %edi,%edx
  800b2d:	83 c4 1c             	add    $0x1c,%esp
  800b30:	5b                   	pop    %ebx
  800b31:	5e                   	pop    %esi
  800b32:	5f                   	pop    %edi
  800b33:	5d                   	pop    %ebp
  800b34:	c3                   	ret    
  800b35:	8d 76 00             	lea    0x0(%esi),%esi
  800b38:	31 ff                	xor    %edi,%edi
  800b3a:	31 c0                	xor    %eax,%eax
  800b3c:	89 fa                	mov    %edi,%edx
  800b3e:	83 c4 1c             	add    $0x1c,%esp
  800b41:	5b                   	pop    %ebx
  800b42:	5e                   	pop    %esi
  800b43:	5f                   	pop    %edi
  800b44:	5d                   	pop    %ebp
  800b45:	c3                   	ret    
  800b46:	66 90                	xchg   %ax,%ax
  800b48:	89 d8                	mov    %ebx,%eax
  800b4a:	f7 f7                	div    %edi
  800b4c:	31 ff                	xor    %edi,%edi
  800b4e:	89 fa                	mov    %edi,%edx
  800b50:	83 c4 1c             	add    $0x1c,%esp
  800b53:	5b                   	pop    %ebx
  800b54:	5e                   	pop    %esi
  800b55:	5f                   	pop    %edi
  800b56:	5d                   	pop    %ebp
  800b57:	c3                   	ret    
  800b58:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b5d:	89 eb                	mov    %ebp,%ebx
  800b5f:	29 fb                	sub    %edi,%ebx
  800b61:	89 f9                	mov    %edi,%ecx
  800b63:	d3 e6                	shl    %cl,%esi
  800b65:	89 c5                	mov    %eax,%ebp
  800b67:	88 d9                	mov    %bl,%cl
  800b69:	d3 ed                	shr    %cl,%ebp
  800b6b:	89 e9                	mov    %ebp,%ecx
  800b6d:	09 f1                	or     %esi,%ecx
  800b6f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b73:	89 f9                	mov    %edi,%ecx
  800b75:	d3 e0                	shl    %cl,%eax
  800b77:	89 c5                	mov    %eax,%ebp
  800b79:	89 d6                	mov    %edx,%esi
  800b7b:	88 d9                	mov    %bl,%cl
  800b7d:	d3 ee                	shr    %cl,%esi
  800b7f:	89 f9                	mov    %edi,%ecx
  800b81:	d3 e2                	shl    %cl,%edx
  800b83:	8b 44 24 08          	mov    0x8(%esp),%eax
  800b87:	88 d9                	mov    %bl,%cl
  800b89:	d3 e8                	shr    %cl,%eax
  800b8b:	09 c2                	or     %eax,%edx
  800b8d:	89 d0                	mov    %edx,%eax
  800b8f:	89 f2                	mov    %esi,%edx
  800b91:	f7 74 24 0c          	divl   0xc(%esp)
  800b95:	89 d6                	mov    %edx,%esi
  800b97:	89 c3                	mov    %eax,%ebx
  800b99:	f7 e5                	mul    %ebp
  800b9b:	39 d6                	cmp    %edx,%esi
  800b9d:	72 19                	jb     800bb8 <__udivdi3+0xfc>
  800b9f:	74 0b                	je     800bac <__udivdi3+0xf0>
  800ba1:	89 d8                	mov    %ebx,%eax
  800ba3:	31 ff                	xor    %edi,%edi
  800ba5:	e9 58 ff ff ff       	jmp    800b02 <__udivdi3+0x46>
  800baa:	66 90                	xchg   %ax,%ax
  800bac:	8b 54 24 08          	mov    0x8(%esp),%edx
  800bb0:	89 f9                	mov    %edi,%ecx
  800bb2:	d3 e2                	shl    %cl,%edx
  800bb4:	39 c2                	cmp    %eax,%edx
  800bb6:	73 e9                	jae    800ba1 <__udivdi3+0xe5>
  800bb8:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800bbb:	31 ff                	xor    %edi,%edi
  800bbd:	e9 40 ff ff ff       	jmp    800b02 <__udivdi3+0x46>
  800bc2:	66 90                	xchg   %ax,%ax
  800bc4:	31 c0                	xor    %eax,%eax
  800bc6:	e9 37 ff ff ff       	jmp    800b02 <__udivdi3+0x46>
  800bcb:	90                   	nop

00800bcc <__umoddi3>:
  800bcc:	55                   	push   %ebp
  800bcd:	57                   	push   %edi
  800bce:	56                   	push   %esi
  800bcf:	53                   	push   %ebx
  800bd0:	83 ec 1c             	sub    $0x1c,%esp
  800bd3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bd7:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bdb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800bdf:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800be3:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800be7:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800beb:	89 f3                	mov    %esi,%ebx
  800bed:	89 fa                	mov    %edi,%edx
  800bef:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800bf3:	89 34 24             	mov    %esi,(%esp)
  800bf6:	85 c0                	test   %eax,%eax
  800bf8:	75 1a                	jne    800c14 <__umoddi3+0x48>
  800bfa:	39 f7                	cmp    %esi,%edi
  800bfc:	0f 86 a2 00 00 00    	jbe    800ca4 <__umoddi3+0xd8>
  800c02:	89 c8                	mov    %ecx,%eax
  800c04:	89 f2                	mov    %esi,%edx
  800c06:	f7 f7                	div    %edi
  800c08:	89 d0                	mov    %edx,%eax
  800c0a:	31 d2                	xor    %edx,%edx
  800c0c:	83 c4 1c             	add    $0x1c,%esp
  800c0f:	5b                   	pop    %ebx
  800c10:	5e                   	pop    %esi
  800c11:	5f                   	pop    %edi
  800c12:	5d                   	pop    %ebp
  800c13:	c3                   	ret    
  800c14:	39 f0                	cmp    %esi,%eax
  800c16:	0f 87 ac 00 00 00    	ja     800cc8 <__umoddi3+0xfc>
  800c1c:	0f bd e8             	bsr    %eax,%ebp
  800c1f:	83 f5 1f             	xor    $0x1f,%ebp
  800c22:	0f 84 ac 00 00 00    	je     800cd4 <__umoddi3+0x108>
  800c28:	bf 20 00 00 00       	mov    $0x20,%edi
  800c2d:	29 ef                	sub    %ebp,%edi
  800c2f:	89 fe                	mov    %edi,%esi
  800c31:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c35:	89 e9                	mov    %ebp,%ecx
  800c37:	d3 e0                	shl    %cl,%eax
  800c39:	89 d7                	mov    %edx,%edi
  800c3b:	89 f1                	mov    %esi,%ecx
  800c3d:	d3 ef                	shr    %cl,%edi
  800c3f:	09 c7                	or     %eax,%edi
  800c41:	89 e9                	mov    %ebp,%ecx
  800c43:	d3 e2                	shl    %cl,%edx
  800c45:	89 14 24             	mov    %edx,(%esp)
  800c48:	89 d8                	mov    %ebx,%eax
  800c4a:	d3 e0                	shl    %cl,%eax
  800c4c:	89 c2                	mov    %eax,%edx
  800c4e:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c52:	d3 e0                	shl    %cl,%eax
  800c54:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c58:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c5c:	89 f1                	mov    %esi,%ecx
  800c5e:	d3 e8                	shr    %cl,%eax
  800c60:	09 d0                	or     %edx,%eax
  800c62:	d3 eb                	shr    %cl,%ebx
  800c64:	89 da                	mov    %ebx,%edx
  800c66:	f7 f7                	div    %edi
  800c68:	89 d3                	mov    %edx,%ebx
  800c6a:	f7 24 24             	mull   (%esp)
  800c6d:	89 c6                	mov    %eax,%esi
  800c6f:	89 d1                	mov    %edx,%ecx
  800c71:	39 d3                	cmp    %edx,%ebx
  800c73:	0f 82 87 00 00 00    	jb     800d00 <__umoddi3+0x134>
  800c79:	0f 84 91 00 00 00    	je     800d10 <__umoddi3+0x144>
  800c7f:	8b 54 24 04          	mov    0x4(%esp),%edx
  800c83:	29 f2                	sub    %esi,%edx
  800c85:	19 cb                	sbb    %ecx,%ebx
  800c87:	89 d8                	mov    %ebx,%eax
  800c89:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800c8d:	d3 e0                	shl    %cl,%eax
  800c8f:	89 e9                	mov    %ebp,%ecx
  800c91:	d3 ea                	shr    %cl,%edx
  800c93:	09 d0                	or     %edx,%eax
  800c95:	89 e9                	mov    %ebp,%ecx
  800c97:	d3 eb                	shr    %cl,%ebx
  800c99:	89 da                	mov    %ebx,%edx
  800c9b:	83 c4 1c             	add    $0x1c,%esp
  800c9e:	5b                   	pop    %ebx
  800c9f:	5e                   	pop    %esi
  800ca0:	5f                   	pop    %edi
  800ca1:	5d                   	pop    %ebp
  800ca2:	c3                   	ret    
  800ca3:	90                   	nop
  800ca4:	89 fd                	mov    %edi,%ebp
  800ca6:	85 ff                	test   %edi,%edi
  800ca8:	75 0b                	jne    800cb5 <__umoddi3+0xe9>
  800caa:	b8 01 00 00 00       	mov    $0x1,%eax
  800caf:	31 d2                	xor    %edx,%edx
  800cb1:	f7 f7                	div    %edi
  800cb3:	89 c5                	mov    %eax,%ebp
  800cb5:	89 f0                	mov    %esi,%eax
  800cb7:	31 d2                	xor    %edx,%edx
  800cb9:	f7 f5                	div    %ebp
  800cbb:	89 c8                	mov    %ecx,%eax
  800cbd:	f7 f5                	div    %ebp
  800cbf:	89 d0                	mov    %edx,%eax
  800cc1:	e9 44 ff ff ff       	jmp    800c0a <__umoddi3+0x3e>
  800cc6:	66 90                	xchg   %ax,%ax
  800cc8:	89 c8                	mov    %ecx,%eax
  800cca:	89 f2                	mov    %esi,%edx
  800ccc:	83 c4 1c             	add    $0x1c,%esp
  800ccf:	5b                   	pop    %ebx
  800cd0:	5e                   	pop    %esi
  800cd1:	5f                   	pop    %edi
  800cd2:	5d                   	pop    %ebp
  800cd3:	c3                   	ret    
  800cd4:	3b 04 24             	cmp    (%esp),%eax
  800cd7:	72 06                	jb     800cdf <__umoddi3+0x113>
  800cd9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800cdd:	77 0f                	ja     800cee <__umoddi3+0x122>
  800cdf:	89 f2                	mov    %esi,%edx
  800ce1:	29 f9                	sub    %edi,%ecx
  800ce3:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800ce7:	89 14 24             	mov    %edx,(%esp)
  800cea:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800cee:	8b 44 24 04          	mov    0x4(%esp),%eax
  800cf2:	8b 14 24             	mov    (%esp),%edx
  800cf5:	83 c4 1c             	add    $0x1c,%esp
  800cf8:	5b                   	pop    %ebx
  800cf9:	5e                   	pop    %esi
  800cfa:	5f                   	pop    %edi
  800cfb:	5d                   	pop    %ebp
  800cfc:	c3                   	ret    
  800cfd:	8d 76 00             	lea    0x0(%esi),%esi
  800d00:	2b 04 24             	sub    (%esp),%eax
  800d03:	19 fa                	sbb    %edi,%edx
  800d05:	89 d1                	mov    %edx,%ecx
  800d07:	89 c6                	mov    %eax,%esi
  800d09:	e9 71 ff ff ff       	jmp    800c7f <__umoddi3+0xb3>
  800d0e:	66 90                	xchg   %ax,%ax
  800d10:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d14:	72 ea                	jb     800d00 <__umoddi3+0x134>
  800d16:	89 d9                	mov    %ebx,%ecx
  800d18:	e9 62 ff ff ff       	jmp    800c7f <__umoddi3+0xb3>
