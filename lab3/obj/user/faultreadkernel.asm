
obj/user/faultreadkernel:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	cmpl $USTACKTOP, %esp
  800020:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  800026:	75 04                	jne    80002c <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  800028:	6a 00                	push   $0x0
	pushl $0
  80002a:	6a 00                	push   $0x0

0080002c <args_exist>:

args_exist:
	call libmain
  80002c:	e8 1d 00 00 00       	call   80004e <libmain>
1:	jmp 1b
  800031:	eb fe                	jmp    800031 <args_exist+0x5>

00800033 <umain>:

#include <inc/lib.h>

void
umain(int argc, char **argv)
{
  800033:	55                   	push   %ebp
  800034:	89 e5                	mov    %esp,%ebp
  800036:	83 ec 10             	sub    $0x10,%esp
	cprintf("I read %08x from location 0xf0100000!\n", *(unsigned*)0xf0100000);
  800039:	ff 35 00 00 10 f0    	pushl  0xf0100000
  80003f:	68 10 0d 80 00       	push   $0x800d10
  800044:	e8 f4 00 00 00       	call   80013d <cprintf>
}
  800049:	83 c4 10             	add    $0x10,%esp
  80004c:	c9                   	leave  
  80004d:	c3                   	ret    

0080004e <libmain>:
const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

void
libmain(int argc, char **argv)
{
  80004e:	55                   	push   %ebp
  80004f:	89 e5                	mov    %esp,%ebp
  800051:	56                   	push   %esi
  800052:	53                   	push   %ebx
  800053:	8b 5d 08             	mov    0x8(%ebp),%ebx
  800056:	8b 75 0c             	mov    0xc(%ebp),%esi
	//int32_t env_Index1 = (int32_t)sys_getenvid();
	//cprintf("printing env_Index1: %d\n", env_Index1);
	//int32_t env_Index2 = (int32_t)ENVX(env_Index1);
	//cprintf("printing env_Index2: %d\n", env_Index2);

	thisenv = &envs[ENVX(sys_getenvid())];
  800059:	e8 e9 09 00 00       	call   800a47 <sys_getenvid>
  80005e:	25 ff 03 00 00       	and    $0x3ff,%eax
  800063:	8d 14 00             	lea    (%eax,%eax,1),%edx
  800066:	01 d0                	add    %edx,%eax
  800068:	c1 e0 05             	shl    $0x5,%eax
  80006b:	05 00 00 c0 ee       	add    $0xeec00000,%eax
  800070:	a3 04 10 80 00       	mov    %eax,0x801004
	//thisenv->env_id = (envs[ENVX(sys_getenvid())]).env_id;
	//cprintf("before printing env_ID\n");
	//int32_t env_ID = (int32_t)(thisenv->env_id);
	//cprintf("env_ID: %d\n", env_ID);
	// save the name of the program so that panic() can use it
	if (argc > 0)
  800075:	85 db                	test   %ebx,%ebx
  800077:	7e 07                	jle    800080 <libmain+0x32>
		binaryname = argv[0];
  800079:	8b 06                	mov    (%esi),%eax
  80007b:	a3 00 10 80 00       	mov    %eax,0x801000

	// call user main routine
	umain(argc, argv);
  800080:	83 ec 08             	sub    $0x8,%esp
  800083:	56                   	push   %esi
  800084:	53                   	push   %ebx
  800085:	e8 a9 ff ff ff       	call   800033 <umain>

	// exit gracefully
	exit();
  80008a:	e8 0a 00 00 00       	call   800099 <exit>
}
  80008f:	83 c4 10             	add    $0x10,%esp
  800092:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800095:	5b                   	pop    %ebx
  800096:	5e                   	pop    %esi
  800097:	5d                   	pop    %ebp
  800098:	c3                   	ret    

00800099 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800099:	55                   	push   %ebp
  80009a:	89 e5                	mov    %esp,%ebp
  80009c:	83 ec 14             	sub    $0x14,%esp
	sys_env_destroy(0);
  80009f:	6a 00                	push   $0x0
  8000a1:	e8 60 09 00 00       	call   800a06 <sys_env_destroy>
}
  8000a6:	83 c4 10             	add    $0x10,%esp
  8000a9:	c9                   	leave  
  8000aa:	c3                   	ret    

008000ab <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000ab:	55                   	push   %ebp
  8000ac:	89 e5                	mov    %esp,%ebp
  8000ae:	53                   	push   %ebx
  8000af:	83 ec 04             	sub    $0x4,%esp
  8000b2:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	b->buf[b->idx++] = ch;
  8000b5:	8b 13                	mov    (%ebx),%edx
  8000b7:	8d 42 01             	lea    0x1(%edx),%eax
  8000ba:	89 03                	mov    %eax,(%ebx)
  8000bc:	8b 4d 08             	mov    0x8(%ebp),%ecx
  8000bf:	88 4c 13 08          	mov    %cl,0x8(%ebx,%edx,1)
	if (b->idx == 256-1) {
  8000c3:	3d ff 00 00 00       	cmp    $0xff,%eax
  8000c8:	75 1a                	jne    8000e4 <putch+0x39>
		sys_cputs(b->buf, b->idx);
  8000ca:	83 ec 08             	sub    $0x8,%esp
  8000cd:	68 ff 00 00 00       	push   $0xff
  8000d2:	8d 43 08             	lea    0x8(%ebx),%eax
  8000d5:	50                   	push   %eax
  8000d6:	e8 ee 08 00 00       	call   8009c9 <sys_cputs>
		b->idx = 0;
  8000db:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  8000e1:	83 c4 10             	add    $0x10,%esp
	}
	b->cnt++;
  8000e4:	ff 43 04             	incl   0x4(%ebx)
}
  8000e7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8000ea:	c9                   	leave  
  8000eb:	c3                   	ret    

008000ec <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8000ec:	55                   	push   %ebp
  8000ed:	89 e5                	mov    %esp,%ebp
  8000ef:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8000f5:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8000fc:	00 00 00 
	b.cnt = 0;
  8000ff:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800106:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800109:	ff 75 0c             	pushl  0xc(%ebp)
  80010c:	ff 75 08             	pushl  0x8(%ebp)
  80010f:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800115:	50                   	push   %eax
  800116:	68 ab 00 80 00       	push   $0x8000ab
  80011b:	e8 51 01 00 00       	call   800271 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800120:	83 c4 08             	add    $0x8,%esp
  800123:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  800129:	8d 85 f8 fe ff ff    	lea    -0x108(%ebp),%eax
  80012f:	50                   	push   %eax
  800130:	e8 94 08 00 00       	call   8009c9 <sys_cputs>

	return b.cnt;
}
  800135:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
  80013b:	c9                   	leave  
  80013c:	c3                   	ret    

0080013d <cprintf>:

int
cprintf(const char *fmt, ...)
{
  80013d:	55                   	push   %ebp
  80013e:	89 e5                	mov    %esp,%ebp
  800140:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  800143:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
  800146:	50                   	push   %eax
  800147:	ff 75 08             	pushl  0x8(%ebp)
  80014a:	e8 9d ff ff ff       	call   8000ec <vcprintf>
	va_end(ap);

	return cnt;
}
  80014f:	c9                   	leave  
  800150:	c3                   	ret    

00800151 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800151:	55                   	push   %ebp
  800152:	89 e5                	mov    %esp,%ebp
  800154:	57                   	push   %edi
  800155:	56                   	push   %esi
  800156:	53                   	push   %ebx
  800157:	83 ec 1c             	sub    $0x1c,%esp
  80015a:	89 c7                	mov    %eax,%edi
  80015c:	89 d6                	mov    %edx,%esi
  80015e:	8b 45 08             	mov    0x8(%ebp),%eax
  800161:	8b 55 0c             	mov    0xc(%ebp),%edx
  800164:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800167:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80016a:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80016d:	bb 00 00 00 00       	mov    $0x0,%ebx
  800172:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  800175:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  800178:	39 d3                	cmp    %edx,%ebx
  80017a:	72 05                	jb     800181 <printnum+0x30>
  80017c:	39 45 10             	cmp    %eax,0x10(%ebp)
  80017f:	77 45                	ja     8001c6 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800181:	83 ec 0c             	sub    $0xc,%esp
  800184:	ff 75 18             	pushl  0x18(%ebp)
  800187:	8b 45 14             	mov    0x14(%ebp),%eax
  80018a:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80018d:	53                   	push   %ebx
  80018e:	ff 75 10             	pushl  0x10(%ebp)
  800191:	83 ec 08             	sub    $0x8,%esp
  800194:	ff 75 e4             	pushl  -0x1c(%ebp)
  800197:	ff 75 e0             	pushl  -0x20(%ebp)
  80019a:	ff 75 dc             	pushl  -0x24(%ebp)
  80019d:	ff 75 d8             	pushl  -0x28(%ebp)
  8001a0:	e8 07 09 00 00       	call   800aac <__udivdi3>
  8001a5:	83 c4 18             	add    $0x18,%esp
  8001a8:	52                   	push   %edx
  8001a9:	50                   	push   %eax
  8001aa:	89 f2                	mov    %esi,%edx
  8001ac:	89 f8                	mov    %edi,%eax
  8001ae:	e8 9e ff ff ff       	call   800151 <printnum>
  8001b3:	83 c4 20             	add    $0x20,%esp
  8001b6:	eb 16                	jmp    8001ce <printnum+0x7d>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001b8:	83 ec 08             	sub    $0x8,%esp
  8001bb:	56                   	push   %esi
  8001bc:	ff 75 18             	pushl  0x18(%ebp)
  8001bf:	ff d7                	call   *%edi
  8001c1:	83 c4 10             	add    $0x10,%esp
  8001c4:	eb 03                	jmp    8001c9 <printnum+0x78>
  8001c6:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8001c9:	4b                   	dec    %ebx
  8001ca:	85 db                	test   %ebx,%ebx
  8001cc:	7f ea                	jg     8001b8 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8001ce:	83 ec 08             	sub    $0x8,%esp
  8001d1:	56                   	push   %esi
  8001d2:	83 ec 04             	sub    $0x4,%esp
  8001d5:	ff 75 e4             	pushl  -0x1c(%ebp)
  8001d8:	ff 75 e0             	pushl  -0x20(%ebp)
  8001db:	ff 75 dc             	pushl  -0x24(%ebp)
  8001de:	ff 75 d8             	pushl  -0x28(%ebp)
  8001e1:	e8 d6 09 00 00       	call   800bbc <__umoddi3>
  8001e6:	83 c4 14             	add    $0x14,%esp
  8001e9:	0f be 80 41 0d 80 00 	movsbl 0x800d41(%eax),%eax
  8001f0:	50                   	push   %eax
  8001f1:	ff d7                	call   *%edi
}
  8001f3:	83 c4 10             	add    $0x10,%esp
  8001f6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8001f9:	5b                   	pop    %ebx
  8001fa:	5e                   	pop    %esi
  8001fb:	5f                   	pop    %edi
  8001fc:	5d                   	pop    %ebp
  8001fd:	c3                   	ret    

008001fe <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8001fe:	55                   	push   %ebp
  8001ff:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800201:	83 fa 01             	cmp    $0x1,%edx
  800204:	7e 0e                	jle    800214 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
  800206:	8b 10                	mov    (%eax),%edx
  800208:	8d 4a 08             	lea    0x8(%edx),%ecx
  80020b:	89 08                	mov    %ecx,(%eax)
  80020d:	8b 02                	mov    (%edx),%eax
  80020f:	8b 52 04             	mov    0x4(%edx),%edx
  800212:	eb 22                	jmp    800236 <getuint+0x38>
	else if (lflag)
  800214:	85 d2                	test   %edx,%edx
  800216:	74 10                	je     800228 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
  800218:	8b 10                	mov    (%eax),%edx
  80021a:	8d 4a 04             	lea    0x4(%edx),%ecx
  80021d:	89 08                	mov    %ecx,(%eax)
  80021f:	8b 02                	mov    (%edx),%eax
  800221:	ba 00 00 00 00       	mov    $0x0,%edx
  800226:	eb 0e                	jmp    800236 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
  800228:	8b 10                	mov    (%eax),%edx
  80022a:	8d 4a 04             	lea    0x4(%edx),%ecx
  80022d:	89 08                	mov    %ecx,(%eax)
  80022f:	8b 02                	mov    (%edx),%eax
  800231:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800236:	5d                   	pop    %ebp
  800237:	c3                   	ret    

00800238 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800238:	55                   	push   %ebp
  800239:	89 e5                	mov    %esp,%ebp
  80023b:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
  80023e:	ff 40 08             	incl   0x8(%eax)
	if (b->buf < b->ebuf)
  800241:	8b 10                	mov    (%eax),%edx
  800243:	3b 50 04             	cmp    0x4(%eax),%edx
  800246:	73 0a                	jae    800252 <sprintputch+0x1a>
		*b->buf++ = ch;
  800248:	8d 4a 01             	lea    0x1(%edx),%ecx
  80024b:	89 08                	mov    %ecx,(%eax)
  80024d:	8b 45 08             	mov    0x8(%ebp),%eax
  800250:	88 02                	mov    %al,(%edx)
}
  800252:	5d                   	pop    %ebp
  800253:	c3                   	ret    

00800254 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800254:	55                   	push   %ebp
  800255:	89 e5                	mov    %esp,%ebp
  800257:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
  80025a:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
  80025d:	50                   	push   %eax
  80025e:	ff 75 10             	pushl  0x10(%ebp)
  800261:	ff 75 0c             	pushl  0xc(%ebp)
  800264:	ff 75 08             	pushl  0x8(%ebp)
  800267:	e8 05 00 00 00       	call   800271 <vprintfmt>
	va_end(ap);
}
  80026c:	83 c4 10             	add    $0x10,%esp
  80026f:	c9                   	leave  
  800270:	c3                   	ret    

00800271 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800271:	55                   	push   %ebp
  800272:	89 e5                	mov    %esp,%ebp
  800274:	57                   	push   %edi
  800275:	56                   	push   %esi
  800276:	53                   	push   %ebx
  800277:	83 ec 2c             	sub    $0x2c,%esp
  80027a:	8b 75 08             	mov    0x8(%ebp),%esi
  80027d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  800280:	8b 7d 10             	mov    0x10(%ebp),%edi
  800283:	eb 12                	jmp    800297 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
  800285:	85 c0                	test   %eax,%eax
  800287:	0f 84 68 03 00 00    	je     8005f5 <vprintfmt+0x384>
				return;
			putch(ch, putdat);
  80028d:	83 ec 08             	sub    $0x8,%esp
  800290:	53                   	push   %ebx
  800291:	50                   	push   %eax
  800292:	ff d6                	call   *%esi
  800294:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800297:	47                   	inc    %edi
  800298:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
  80029c:	83 f8 25             	cmp    $0x25,%eax
  80029f:	75 e4                	jne    800285 <vprintfmt+0x14>
  8002a1:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
  8002a5:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
  8002ac:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  8002b3:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  8002ba:	ba 00 00 00 00       	mov    $0x0,%edx
  8002bf:	eb 07                	jmp    8002c8 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002c1:	8b 7d e0             	mov    -0x20(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
  8002c4:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002c8:	8d 47 01             	lea    0x1(%edi),%eax
  8002cb:	89 45 e0             	mov    %eax,-0x20(%ebp)
  8002ce:	0f b6 0f             	movzbl (%edi),%ecx
  8002d1:	8a 07                	mov    (%edi),%al
  8002d3:	83 e8 23             	sub    $0x23,%eax
  8002d6:	3c 55                	cmp    $0x55,%al
  8002d8:	0f 87 fe 02 00 00    	ja     8005dc <vprintfmt+0x36b>
  8002de:	0f b6 c0             	movzbl %al,%eax
  8002e1:	ff 24 85 d0 0d 80 00 	jmp    *0x800dd0(,%eax,4)
  8002e8:	8b 7d e0             	mov    -0x20(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8002eb:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
  8002ef:	eb d7                	jmp    8002c8 <vprintfmt+0x57>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8002f1:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8002f4:	b8 00 00 00 00       	mov    $0x0,%eax
  8002f9:	89 55 e0             	mov    %edx,-0x20(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
  8002fc:	8d 04 80             	lea    (%eax,%eax,4),%eax
  8002ff:	01 c0                	add    %eax,%eax
  800301:	8d 44 01 d0          	lea    -0x30(%ecx,%eax,1),%eax
				ch = *fmt;
  800305:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
  800308:	8d 51 d0             	lea    -0x30(%ecx),%edx
  80030b:	83 fa 09             	cmp    $0x9,%edx
  80030e:	77 34                	ja     800344 <vprintfmt+0xd3>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800310:	47                   	inc    %edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800311:	eb e9                	jmp    8002fc <vprintfmt+0x8b>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800313:	8b 45 14             	mov    0x14(%ebp),%eax
  800316:	8d 48 04             	lea    0x4(%eax),%ecx
  800319:	89 4d 14             	mov    %ecx,0x14(%ebp)
  80031c:	8b 00                	mov    (%eax),%eax
  80031e:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800321:	8b 7d e0             	mov    -0x20(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
  800324:	eb 24                	jmp    80034a <vprintfmt+0xd9>
  800326:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80032a:	79 07                	jns    800333 <vprintfmt+0xc2>
  80032c:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800333:	8b 7d e0             	mov    -0x20(%ebp),%edi
  800336:	eb 90                	jmp    8002c8 <vprintfmt+0x57>
  800338:	8b 7d e0             	mov    -0x20(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
  80033b:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
  800342:	eb 84                	jmp    8002c8 <vprintfmt+0x57>
  800344:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800347:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
  80034a:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80034e:	0f 89 74 ff ff ff    	jns    8002c8 <vprintfmt+0x57>
				width = precision, precision = -1;
  800354:	8b 45 d0             	mov    -0x30(%ebp),%eax
  800357:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80035a:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
  800361:	e9 62 ff ff ff       	jmp    8002c8 <vprintfmt+0x57>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800366:	42                   	inc    %edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800367:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
  80036a:	e9 59 ff ff ff       	jmp    8002c8 <vprintfmt+0x57>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80036f:	8b 45 14             	mov    0x14(%ebp),%eax
  800372:	8d 50 04             	lea    0x4(%eax),%edx
  800375:	89 55 14             	mov    %edx,0x14(%ebp)
  800378:	83 ec 08             	sub    $0x8,%esp
  80037b:	53                   	push   %ebx
  80037c:	ff 30                	pushl  (%eax)
  80037e:	ff d6                	call   *%esi
			break;
  800380:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800383:	8b 7d e0             	mov    -0x20(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
  800386:	e9 0c ff ff ff       	jmp    800297 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80038b:	8b 45 14             	mov    0x14(%ebp),%eax
  80038e:	8d 50 04             	lea    0x4(%eax),%edx
  800391:	89 55 14             	mov    %edx,0x14(%ebp)
  800394:	8b 00                	mov    (%eax),%eax
  800396:	85 c0                	test   %eax,%eax
  800398:	79 02                	jns    80039c <vprintfmt+0x12b>
  80039a:	f7 d8                	neg    %eax
  80039c:	89 c2                	mov    %eax,%edx
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
  80039e:	83 f8 06             	cmp    $0x6,%eax
  8003a1:	7f 0b                	jg     8003ae <vprintfmt+0x13d>
  8003a3:	8b 04 85 28 0f 80 00 	mov    0x800f28(,%eax,4),%eax
  8003aa:	85 c0                	test   %eax,%eax
  8003ac:	75 18                	jne    8003c6 <vprintfmt+0x155>
				printfmt(putch, putdat, "error %d", err);
  8003ae:	52                   	push   %edx
  8003af:	68 59 0d 80 00       	push   $0x800d59
  8003b4:	53                   	push   %ebx
  8003b5:	56                   	push   %esi
  8003b6:	e8 99 fe ff ff       	call   800254 <printfmt>
  8003bb:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003be:	8b 7d e0             	mov    -0x20(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
  8003c1:	e9 d1 fe ff ff       	jmp    800297 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
  8003c6:	50                   	push   %eax
  8003c7:	68 62 0d 80 00       	push   $0x800d62
  8003cc:	53                   	push   %ebx
  8003cd:	56                   	push   %esi
  8003ce:	e8 81 fe ff ff       	call   800254 <printfmt>
  8003d3:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003d6:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8003d9:	e9 b9 fe ff ff       	jmp    800297 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8003de:	8b 45 14             	mov    0x14(%ebp),%eax
  8003e1:	8d 50 04             	lea    0x4(%eax),%edx
  8003e4:	89 55 14             	mov    %edx,0x14(%ebp)
  8003e7:	8b 38                	mov    (%eax),%edi
  8003e9:	85 ff                	test   %edi,%edi
  8003eb:	75 05                	jne    8003f2 <vprintfmt+0x181>
				p = "(null)";
  8003ed:	bf 52 0d 80 00       	mov    $0x800d52,%edi
			if (width > 0 && padc != '-')
  8003f2:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003f6:	0f 8e 90 00 00 00    	jle    80048c <vprintfmt+0x21b>
  8003fc:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
  800400:	0f 84 8e 00 00 00    	je     800494 <vprintfmt+0x223>
				for (width -= strnlen(p, precision); width > 0; width--)
  800406:	83 ec 08             	sub    $0x8,%esp
  800409:	ff 75 d0             	pushl  -0x30(%ebp)
  80040c:	57                   	push   %edi
  80040d:	e8 70 02 00 00       	call   800682 <strnlen>
  800412:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  800415:	29 c1                	sub    %eax,%ecx
  800417:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  80041a:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
  80041d:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
  800421:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800424:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  800427:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800429:	eb 0d                	jmp    800438 <vprintfmt+0x1c7>
					putch(padc, putdat);
  80042b:	83 ec 08             	sub    $0x8,%esp
  80042e:	53                   	push   %ebx
  80042f:	ff 75 e4             	pushl  -0x1c(%ebp)
  800432:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800434:	4f                   	dec    %edi
  800435:	83 c4 10             	add    $0x10,%esp
  800438:	85 ff                	test   %edi,%edi
  80043a:	7f ef                	jg     80042b <vprintfmt+0x1ba>
  80043c:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  80043f:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800442:	89 c8                	mov    %ecx,%eax
  800444:	85 c9                	test   %ecx,%ecx
  800446:	79 05                	jns    80044d <vprintfmt+0x1dc>
  800448:	b8 00 00 00 00       	mov    $0x0,%eax
  80044d:	8b 4d cc             	mov    -0x34(%ebp),%ecx
  800450:	29 c1                	sub    %eax,%ecx
  800452:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  800455:	89 75 08             	mov    %esi,0x8(%ebp)
  800458:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80045b:	eb 3d                	jmp    80049a <vprintfmt+0x229>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
  80045d:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  800461:	74 19                	je     80047c <vprintfmt+0x20b>
  800463:	0f be c0             	movsbl %al,%eax
  800466:	83 e8 20             	sub    $0x20,%eax
  800469:	83 f8 5e             	cmp    $0x5e,%eax
  80046c:	76 0e                	jbe    80047c <vprintfmt+0x20b>
					putch('?', putdat);
  80046e:	83 ec 08             	sub    $0x8,%esp
  800471:	53                   	push   %ebx
  800472:	6a 3f                	push   $0x3f
  800474:	ff 55 08             	call   *0x8(%ebp)
  800477:	83 c4 10             	add    $0x10,%esp
  80047a:	eb 0b                	jmp    800487 <vprintfmt+0x216>
				else
					putch(ch, putdat);
  80047c:	83 ec 08             	sub    $0x8,%esp
  80047f:	53                   	push   %ebx
  800480:	52                   	push   %edx
  800481:	ff 55 08             	call   *0x8(%ebp)
  800484:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800487:	ff 4d e4             	decl   -0x1c(%ebp)
  80048a:	eb 0e                	jmp    80049a <vprintfmt+0x229>
  80048c:	89 75 08             	mov    %esi,0x8(%ebp)
  80048f:	8b 75 d0             	mov    -0x30(%ebp),%esi
  800492:	eb 06                	jmp    80049a <vprintfmt+0x229>
  800494:	89 75 08             	mov    %esi,0x8(%ebp)
  800497:	8b 75 d0             	mov    -0x30(%ebp),%esi
  80049a:	47                   	inc    %edi
  80049b:	8a 47 ff             	mov    -0x1(%edi),%al
  80049e:	0f be d0             	movsbl %al,%edx
  8004a1:	85 d2                	test   %edx,%edx
  8004a3:	74 1d                	je     8004c2 <vprintfmt+0x251>
  8004a5:	85 f6                	test   %esi,%esi
  8004a7:	78 b4                	js     80045d <vprintfmt+0x1ec>
  8004a9:	4e                   	dec    %esi
  8004aa:	79 b1                	jns    80045d <vprintfmt+0x1ec>
  8004ac:	8b 75 08             	mov    0x8(%ebp),%esi
  8004af:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004b2:	eb 14                	jmp    8004c8 <vprintfmt+0x257>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
  8004b4:	83 ec 08             	sub    $0x8,%esp
  8004b7:	53                   	push   %ebx
  8004b8:	6a 20                	push   $0x20
  8004ba:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8004bc:	4f                   	dec    %edi
  8004bd:	83 c4 10             	add    $0x10,%esp
  8004c0:	eb 06                	jmp    8004c8 <vprintfmt+0x257>
  8004c2:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  8004c5:	8b 75 08             	mov    0x8(%ebp),%esi
  8004c8:	85 ff                	test   %edi,%edi
  8004ca:	7f e8                	jg     8004b4 <vprintfmt+0x243>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8004cc:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8004cf:	e9 c3 fd ff ff       	jmp    800297 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
  8004d4:	83 fa 01             	cmp    $0x1,%edx
  8004d7:	7e 16                	jle    8004ef <vprintfmt+0x27e>
		return va_arg(*ap, long long);
  8004d9:	8b 45 14             	mov    0x14(%ebp),%eax
  8004dc:	8d 50 08             	lea    0x8(%eax),%edx
  8004df:	89 55 14             	mov    %edx,0x14(%ebp)
  8004e2:	8b 50 04             	mov    0x4(%eax),%edx
  8004e5:	8b 00                	mov    (%eax),%eax
  8004e7:	89 45 d8             	mov    %eax,-0x28(%ebp)
  8004ea:	89 55 dc             	mov    %edx,-0x24(%ebp)
  8004ed:	eb 32                	jmp    800521 <vprintfmt+0x2b0>
	else if (lflag)
  8004ef:	85 d2                	test   %edx,%edx
  8004f1:	74 18                	je     80050b <vprintfmt+0x29a>
		return va_arg(*ap, long);
  8004f3:	8b 45 14             	mov    0x14(%ebp),%eax
  8004f6:	8d 50 04             	lea    0x4(%eax),%edx
  8004f9:	89 55 14             	mov    %edx,0x14(%ebp)
  8004fc:	8b 00                	mov    (%eax),%eax
  8004fe:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800501:	89 c1                	mov    %eax,%ecx
  800503:	c1 f9 1f             	sar    $0x1f,%ecx
  800506:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  800509:	eb 16                	jmp    800521 <vprintfmt+0x2b0>
	else
		return va_arg(*ap, int);
  80050b:	8b 45 14             	mov    0x14(%ebp),%eax
  80050e:	8d 50 04             	lea    0x4(%eax),%edx
  800511:	89 55 14             	mov    %edx,0x14(%ebp)
  800514:	8b 00                	mov    (%eax),%eax
  800516:	89 45 d8             	mov    %eax,-0x28(%ebp)
  800519:	89 c1                	mov    %eax,%ecx
  80051b:	c1 f9 1f             	sar    $0x1f,%ecx
  80051e:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800521:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800524:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
  800527:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80052b:	79 76                	jns    8005a3 <vprintfmt+0x332>
				putch('-', putdat);
  80052d:	83 ec 08             	sub    $0x8,%esp
  800530:	53                   	push   %ebx
  800531:	6a 2d                	push   $0x2d
  800533:	ff d6                	call   *%esi
				num = -(long long) num;
  800535:	8b 45 d8             	mov    -0x28(%ebp),%eax
  800538:	8b 55 dc             	mov    -0x24(%ebp),%edx
  80053b:	f7 d8                	neg    %eax
  80053d:	83 d2 00             	adc    $0x0,%edx
  800540:	f7 da                	neg    %edx
  800542:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
  800545:	b9 0a 00 00 00       	mov    $0xa,%ecx
  80054a:	eb 5c                	jmp    8005a8 <vprintfmt+0x337>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80054c:	8d 45 14             	lea    0x14(%ebp),%eax
  80054f:	e8 aa fc ff ff       	call   8001fe <getuint>
			base = 10;
  800554:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
  800559:	eb 4d                	jmp    8005a8 <vprintfmt+0x337>
			// Replace this with your code.
			/*putch('X', putdat);
			putch('X', putdat);
			putch('X', putdat);
			break;*/
			num = getuint(&ap, lflag);
  80055b:	8d 45 14             	lea    0x14(%ebp),%eax
  80055e:	e8 9b fc ff ff       	call   8001fe <getuint>
			base = 8;
  800563:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
  800568:	eb 3e                	jmp    8005a8 <vprintfmt+0x337>

		// pointer
		case 'p':
			putch('0', putdat);
  80056a:	83 ec 08             	sub    $0x8,%esp
  80056d:	53                   	push   %ebx
  80056e:	6a 30                	push   $0x30
  800570:	ff d6                	call   *%esi
			putch('x', putdat);
  800572:	83 c4 08             	add    $0x8,%esp
  800575:	53                   	push   %ebx
  800576:	6a 78                	push   $0x78
  800578:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
  80057a:	8b 45 14             	mov    0x14(%ebp),%eax
  80057d:	8d 50 04             	lea    0x4(%eax),%edx
  800580:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800583:	8b 00                	mov    (%eax),%eax
  800585:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
  80058a:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
  80058d:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
  800592:	eb 14                	jmp    8005a8 <vprintfmt+0x337>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800594:	8d 45 14             	lea    0x14(%ebp),%eax
  800597:	e8 62 fc ff ff       	call   8001fe <getuint>
			base = 16;
  80059c:	b9 10 00 00 00       	mov    $0x10,%ecx
  8005a1:	eb 05                	jmp    8005a8 <vprintfmt+0x337>
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
  8005a3:	b9 0a 00 00 00       	mov    $0xa,%ecx
		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
		number:
			printnum(putch, putdat, num, base, width, padc);
  8005a8:	83 ec 0c             	sub    $0xc,%esp
  8005ab:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
  8005af:	57                   	push   %edi
  8005b0:	ff 75 e4             	pushl  -0x1c(%ebp)
  8005b3:	51                   	push   %ecx
  8005b4:	52                   	push   %edx
  8005b5:	50                   	push   %eax
  8005b6:	89 da                	mov    %ebx,%edx
  8005b8:	89 f0                	mov    %esi,%eax
  8005ba:	e8 92 fb ff ff       	call   800151 <printnum>
			break;
  8005bf:	83 c4 20             	add    $0x20,%esp
  8005c2:	8b 7d e0             	mov    -0x20(%ebp),%edi
  8005c5:	e9 cd fc ff ff       	jmp    800297 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8005ca:	83 ec 08             	sub    $0x8,%esp
  8005cd:	53                   	push   %ebx
  8005ce:	51                   	push   %ecx
  8005cf:	ff d6                	call   *%esi
			break;
  8005d1:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8005d4:	8b 7d e0             	mov    -0x20(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
  8005d7:	e9 bb fc ff ff       	jmp    800297 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8005dc:	83 ec 08             	sub    $0x8,%esp
  8005df:	53                   	push   %ebx
  8005e0:	6a 25                	push   $0x25
  8005e2:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
  8005e4:	83 c4 10             	add    $0x10,%esp
  8005e7:	eb 01                	jmp    8005ea <vprintfmt+0x379>
  8005e9:	4f                   	dec    %edi
  8005ea:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
  8005ee:	75 f9                	jne    8005e9 <vprintfmt+0x378>
  8005f0:	e9 a2 fc ff ff       	jmp    800297 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
  8005f5:	8d 65 f4             	lea    -0xc(%ebp),%esp
  8005f8:	5b                   	pop    %ebx
  8005f9:	5e                   	pop    %esi
  8005fa:	5f                   	pop    %edi
  8005fb:	5d                   	pop    %ebp
  8005fc:	c3                   	ret    

008005fd <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8005fd:	55                   	push   %ebp
  8005fe:	89 e5                	mov    %esp,%ebp
  800600:	83 ec 18             	sub    $0x18,%esp
  800603:	8b 45 08             	mov    0x8(%ebp),%eax
  800606:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
  800609:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80060c:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
  800610:	89 4d f0             	mov    %ecx,-0x10(%ebp)
  800613:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80061a:	85 c0                	test   %eax,%eax
  80061c:	74 26                	je     800644 <vsnprintf+0x47>
  80061e:	85 d2                	test   %edx,%edx
  800620:	7e 29                	jle    80064b <vsnprintf+0x4e>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800622:	ff 75 14             	pushl  0x14(%ebp)
  800625:	ff 75 10             	pushl  0x10(%ebp)
  800628:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80062b:	50                   	push   %eax
  80062c:	68 38 02 80 00       	push   $0x800238
  800631:	e8 3b fc ff ff       	call   800271 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800636:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800639:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80063c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80063f:	83 c4 10             	add    $0x10,%esp
  800642:	eb 0c                	jmp    800650 <vsnprintf+0x53>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
  800644:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  800649:	eb 05                	jmp    800650 <vsnprintf+0x53>
  80064b:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
  800650:	c9                   	leave  
  800651:	c3                   	ret    

00800652 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800652:	55                   	push   %ebp
  800653:	89 e5                	mov    %esp,%ebp
  800655:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800658:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
  80065b:	50                   	push   %eax
  80065c:	ff 75 10             	pushl  0x10(%ebp)
  80065f:	ff 75 0c             	pushl  0xc(%ebp)
  800662:	ff 75 08             	pushl  0x8(%ebp)
  800665:	e8 93 ff ff ff       	call   8005fd <vsnprintf>
	va_end(ap);

	return rc;
}
  80066a:	c9                   	leave  
  80066b:	c3                   	ret    

0080066c <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
  80066c:	55                   	push   %ebp
  80066d:	89 e5                	mov    %esp,%ebp
  80066f:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
  800672:	b8 00 00 00 00       	mov    $0x0,%eax
  800677:	eb 01                	jmp    80067a <strlen+0xe>
		n++;
  800679:	40                   	inc    %eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80067a:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  80067e:	75 f9                	jne    800679 <strlen+0xd>
		n++;
	return n;
}
  800680:	5d                   	pop    %ebp
  800681:	c3                   	ret    

00800682 <strnlen>:

int
strnlen(const char *s, size_t size)
{
  800682:	55                   	push   %ebp
  800683:	89 e5                	mov    %esp,%ebp
  800685:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800688:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80068b:	ba 00 00 00 00       	mov    $0x0,%edx
  800690:	eb 01                	jmp    800693 <strnlen+0x11>
		n++;
  800692:	42                   	inc    %edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800693:	39 c2                	cmp    %eax,%edx
  800695:	74 08                	je     80069f <strnlen+0x1d>
  800697:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  80069b:	75 f5                	jne    800692 <strnlen+0x10>
  80069d:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
  80069f:	5d                   	pop    %ebp
  8006a0:	c3                   	ret    

008006a1 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8006a1:	55                   	push   %ebp
  8006a2:	89 e5                	mov    %esp,%ebp
  8006a4:	53                   	push   %ebx
  8006a5:	8b 45 08             	mov    0x8(%ebp),%eax
  8006a8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
  8006ab:	89 c2                	mov    %eax,%edx
  8006ad:	42                   	inc    %edx
  8006ae:	41                   	inc    %ecx
  8006af:	8a 59 ff             	mov    -0x1(%ecx),%bl
  8006b2:	88 5a ff             	mov    %bl,-0x1(%edx)
  8006b5:	84 db                	test   %bl,%bl
  8006b7:	75 f4                	jne    8006ad <strcpy+0xc>
		/* do nothing */;
	return ret;
}
  8006b9:	5b                   	pop    %ebx
  8006ba:	5d                   	pop    %ebp
  8006bb:	c3                   	ret    

008006bc <strcat>:

char *
strcat(char *dst, const char *src)
{
  8006bc:	55                   	push   %ebp
  8006bd:	89 e5                	mov    %esp,%ebp
  8006bf:	53                   	push   %ebx
  8006c0:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
  8006c3:	53                   	push   %ebx
  8006c4:	e8 a3 ff ff ff       	call   80066c <strlen>
  8006c9:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
  8006cc:	ff 75 0c             	pushl  0xc(%ebp)
  8006cf:	01 d8                	add    %ebx,%eax
  8006d1:	50                   	push   %eax
  8006d2:	e8 ca ff ff ff       	call   8006a1 <strcpy>
	return dst;
}
  8006d7:	89 d8                	mov    %ebx,%eax
  8006d9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8006dc:	c9                   	leave  
  8006dd:	c3                   	ret    

008006de <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
  8006de:	55                   	push   %ebp
  8006df:	89 e5                	mov    %esp,%ebp
  8006e1:	56                   	push   %esi
  8006e2:	53                   	push   %ebx
  8006e3:	8b 75 08             	mov    0x8(%ebp),%esi
  8006e6:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8006e9:	89 f3                	mov    %esi,%ebx
  8006eb:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8006ee:	89 f2                	mov    %esi,%edx
  8006f0:	eb 0c                	jmp    8006fe <strncpy+0x20>
		*dst++ = *src;
  8006f2:	42                   	inc    %edx
  8006f3:	8a 01                	mov    (%ecx),%al
  8006f5:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
  8006f8:	80 39 01             	cmpb   $0x1,(%ecx)
  8006fb:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8006fe:	39 da                	cmp    %ebx,%edx
  800700:	75 f0                	jne    8006f2 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
  800702:	89 f0                	mov    %esi,%eax
  800704:	5b                   	pop    %ebx
  800705:	5e                   	pop    %esi
  800706:	5d                   	pop    %ebp
  800707:	c3                   	ret    

00800708 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
  800708:	55                   	push   %ebp
  800709:	89 e5                	mov    %esp,%ebp
  80070b:	56                   	push   %esi
  80070c:	53                   	push   %ebx
  80070d:	8b 75 08             	mov    0x8(%ebp),%esi
  800710:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  800713:	8b 45 10             	mov    0x10(%ebp),%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
  800716:	85 c0                	test   %eax,%eax
  800718:	74 1e                	je     800738 <strlcpy+0x30>
  80071a:	8d 44 06 ff          	lea    -0x1(%esi,%eax,1),%eax
  80071e:	89 f2                	mov    %esi,%edx
  800720:	eb 05                	jmp    800727 <strlcpy+0x1f>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
  800722:	42                   	inc    %edx
  800723:	41                   	inc    %ecx
  800724:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800727:	39 c2                	cmp    %eax,%edx
  800729:	74 08                	je     800733 <strlcpy+0x2b>
  80072b:	8a 19                	mov    (%ecx),%bl
  80072d:	84 db                	test   %bl,%bl
  80072f:	75 f1                	jne    800722 <strlcpy+0x1a>
  800731:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
  800733:	c6 00 00             	movb   $0x0,(%eax)
  800736:	eb 02                	jmp    80073a <strlcpy+0x32>
  800738:	89 f0                	mov    %esi,%eax
	}
	return dst - dst_in;
  80073a:	29 f0                	sub    %esi,%eax
}
  80073c:	5b                   	pop    %ebx
  80073d:	5e                   	pop    %esi
  80073e:	5d                   	pop    %ebp
  80073f:	c3                   	ret    

00800740 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800740:	55                   	push   %ebp
  800741:	89 e5                	mov    %esp,%ebp
  800743:	8b 4d 08             	mov    0x8(%ebp),%ecx
  800746:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
  800749:	eb 02                	jmp    80074d <strcmp+0xd>
		p++, q++;
  80074b:	41                   	inc    %ecx
  80074c:	42                   	inc    %edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  80074d:	8a 01                	mov    (%ecx),%al
  80074f:	84 c0                	test   %al,%al
  800751:	74 04                	je     800757 <strcmp+0x17>
  800753:	3a 02                	cmp    (%edx),%al
  800755:	74 f4                	je     80074b <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800757:	0f b6 c0             	movzbl %al,%eax
  80075a:	0f b6 12             	movzbl (%edx),%edx
  80075d:	29 d0                	sub    %edx,%eax
}
  80075f:	5d                   	pop    %ebp
  800760:	c3                   	ret    

00800761 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
  800761:	55                   	push   %ebp
  800762:	89 e5                	mov    %esp,%ebp
  800764:	53                   	push   %ebx
  800765:	8b 45 08             	mov    0x8(%ebp),%eax
  800768:	8b 55 0c             	mov    0xc(%ebp),%edx
  80076b:	89 c3                	mov    %eax,%ebx
  80076d:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
  800770:	eb 02                	jmp    800774 <strncmp+0x13>
		n--, p++, q++;
  800772:	40                   	inc    %eax
  800773:	42                   	inc    %edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
  800774:	39 d8                	cmp    %ebx,%eax
  800776:	74 14                	je     80078c <strncmp+0x2b>
  800778:	8a 08                	mov    (%eax),%cl
  80077a:	84 c9                	test   %cl,%cl
  80077c:	74 04                	je     800782 <strncmp+0x21>
  80077e:	3a 0a                	cmp    (%edx),%cl
  800780:	74 f0                	je     800772 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800782:	0f b6 00             	movzbl (%eax),%eax
  800785:	0f b6 12             	movzbl (%edx),%edx
  800788:	29 d0                	sub    %edx,%eax
  80078a:	eb 05                	jmp    800791 <strncmp+0x30>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
  80078c:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
  800791:	5b                   	pop    %ebx
  800792:	5d                   	pop    %ebp
  800793:	c3                   	ret    

00800794 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800794:	55                   	push   %ebp
  800795:	89 e5                	mov    %esp,%ebp
  800797:	8b 45 08             	mov    0x8(%ebp),%eax
  80079a:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  80079d:	eb 05                	jmp    8007a4 <strchr+0x10>
		if (*s == c)
  80079f:	38 ca                	cmp    %cl,%dl
  8007a1:	74 0c                	je     8007af <strchr+0x1b>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8007a3:	40                   	inc    %eax
  8007a4:	8a 10                	mov    (%eax),%dl
  8007a6:	84 d2                	test   %dl,%dl
  8007a8:	75 f5                	jne    80079f <strchr+0xb>
		if (*s == c)
			return (char *) s;
	return 0;
  8007aa:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8007af:	5d                   	pop    %ebp
  8007b0:	c3                   	ret    

008007b1 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8007b1:	55                   	push   %ebp
  8007b2:	89 e5                	mov    %esp,%ebp
  8007b4:	8b 45 08             	mov    0x8(%ebp),%eax
  8007b7:	8a 4d 0c             	mov    0xc(%ebp),%cl
	for (; *s; s++)
  8007ba:	eb 05                	jmp    8007c1 <strfind+0x10>
		if (*s == c)
  8007bc:	38 ca                	cmp    %cl,%dl
  8007be:	74 07                	je     8007c7 <strfind+0x16>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8007c0:	40                   	inc    %eax
  8007c1:	8a 10                	mov    (%eax),%dl
  8007c3:	84 d2                	test   %dl,%dl
  8007c5:	75 f5                	jne    8007bc <strfind+0xb>
		if (*s == c)
			break;
	return (char *) s;
}
  8007c7:	5d                   	pop    %ebp
  8007c8:	c3                   	ret    

008007c9 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
  8007c9:	55                   	push   %ebp
  8007ca:	89 e5                	mov    %esp,%ebp
  8007cc:	57                   	push   %edi
  8007cd:	56                   	push   %esi
  8007ce:	53                   	push   %ebx
  8007cf:	8b 7d 08             	mov    0x8(%ebp),%edi
  8007d2:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
  8007d5:	85 c9                	test   %ecx,%ecx
  8007d7:	74 36                	je     80080f <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
  8007d9:	f7 c7 03 00 00 00    	test   $0x3,%edi
  8007df:	75 28                	jne    800809 <memset+0x40>
  8007e1:	f6 c1 03             	test   $0x3,%cl
  8007e4:	75 23                	jne    800809 <memset+0x40>
		c &= 0xFF;
  8007e6:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
  8007ea:	89 d3                	mov    %edx,%ebx
  8007ec:	c1 e3 08             	shl    $0x8,%ebx
  8007ef:	89 d6                	mov    %edx,%esi
  8007f1:	c1 e6 18             	shl    $0x18,%esi
  8007f4:	89 d0                	mov    %edx,%eax
  8007f6:	c1 e0 10             	shl    $0x10,%eax
  8007f9:	09 f0                	or     %esi,%eax
  8007fb:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
  8007fd:	89 d8                	mov    %ebx,%eax
  8007ff:	09 d0                	or     %edx,%eax
  800801:	c1 e9 02             	shr    $0x2,%ecx
  800804:	fc                   	cld    
  800805:	f3 ab                	rep stos %eax,%es:(%edi)
  800807:	eb 06                	jmp    80080f <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
  800809:	8b 45 0c             	mov    0xc(%ebp),%eax
  80080c:	fc                   	cld    
  80080d:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
  80080f:	89 f8                	mov    %edi,%eax
  800811:	5b                   	pop    %ebx
  800812:	5e                   	pop    %esi
  800813:	5f                   	pop    %edi
  800814:	5d                   	pop    %ebp
  800815:	c3                   	ret    

00800816 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
  800816:	55                   	push   %ebp
  800817:	89 e5                	mov    %esp,%ebp
  800819:	57                   	push   %edi
  80081a:	56                   	push   %esi
  80081b:	8b 45 08             	mov    0x8(%ebp),%eax
  80081e:	8b 75 0c             	mov    0xc(%ebp),%esi
  800821:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800824:	39 c6                	cmp    %eax,%esi
  800826:	73 33                	jae    80085b <memmove+0x45>
  800828:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
  80082b:	39 d0                	cmp    %edx,%eax
  80082d:	73 2c                	jae    80085b <memmove+0x45>
		s += n;
		d += n;
  80082f:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  800832:	89 d6                	mov    %edx,%esi
  800834:	09 fe                	or     %edi,%esi
  800836:	f7 c6 03 00 00 00    	test   $0x3,%esi
  80083c:	75 13                	jne    800851 <memmove+0x3b>
  80083e:	f6 c1 03             	test   $0x3,%cl
  800841:	75 0e                	jne    800851 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
  800843:	83 ef 04             	sub    $0x4,%edi
  800846:	8d 72 fc             	lea    -0x4(%edx),%esi
  800849:	c1 e9 02             	shr    $0x2,%ecx
  80084c:	fd                   	std    
  80084d:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  80084f:	eb 07                	jmp    800858 <memmove+0x42>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
  800851:	4f                   	dec    %edi
  800852:	8d 72 ff             	lea    -0x1(%edx),%esi
  800855:	fd                   	std    
  800856:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
  800858:	fc                   	cld    
  800859:	eb 1d                	jmp    800878 <memmove+0x62>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
  80085b:	89 f2                	mov    %esi,%edx
  80085d:	09 c2                	or     %eax,%edx
  80085f:	f6 c2 03             	test   $0x3,%dl
  800862:	75 0f                	jne    800873 <memmove+0x5d>
  800864:	f6 c1 03             	test   $0x3,%cl
  800867:	75 0a                	jne    800873 <memmove+0x5d>
			asm volatile("cld; rep movsl\n"
  800869:	c1 e9 02             	shr    $0x2,%ecx
  80086c:	89 c7                	mov    %eax,%edi
  80086e:	fc                   	cld    
  80086f:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  800871:	eb 05                	jmp    800878 <memmove+0x62>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
  800873:	89 c7                	mov    %eax,%edi
  800875:	fc                   	cld    
  800876:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
  800878:	5e                   	pop    %esi
  800879:	5f                   	pop    %edi
  80087a:	5d                   	pop    %ebp
  80087b:	c3                   	ret    

0080087c <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
  80087c:	55                   	push   %ebp
  80087d:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
  80087f:	ff 75 10             	pushl  0x10(%ebp)
  800882:	ff 75 0c             	pushl  0xc(%ebp)
  800885:	ff 75 08             	pushl  0x8(%ebp)
  800888:	e8 89 ff ff ff       	call   800816 <memmove>
}
  80088d:	c9                   	leave  
  80088e:	c3                   	ret    

0080088f <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
  80088f:	55                   	push   %ebp
  800890:	89 e5                	mov    %esp,%ebp
  800892:	56                   	push   %esi
  800893:	53                   	push   %ebx
  800894:	8b 45 08             	mov    0x8(%ebp),%eax
  800897:	8b 55 0c             	mov    0xc(%ebp),%edx
  80089a:	89 c6                	mov    %eax,%esi
  80089c:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  80089f:	eb 14                	jmp    8008b5 <memcmp+0x26>
		if (*s1 != *s2)
  8008a1:	8a 08                	mov    (%eax),%cl
  8008a3:	8a 1a                	mov    (%edx),%bl
  8008a5:	38 d9                	cmp    %bl,%cl
  8008a7:	74 0a                	je     8008b3 <memcmp+0x24>
			return (int) *s1 - (int) *s2;
  8008a9:	0f b6 c1             	movzbl %cl,%eax
  8008ac:	0f b6 db             	movzbl %bl,%ebx
  8008af:	29 d8                	sub    %ebx,%eax
  8008b1:	eb 0b                	jmp    8008be <memcmp+0x2f>
		s1++, s2++;
  8008b3:	40                   	inc    %eax
  8008b4:	42                   	inc    %edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
  8008b5:	39 f0                	cmp    %esi,%eax
  8008b7:	75 e8                	jne    8008a1 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  8008b9:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8008be:	5b                   	pop    %ebx
  8008bf:	5e                   	pop    %esi
  8008c0:	5d                   	pop    %ebp
  8008c1:	c3                   	ret    

008008c2 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
  8008c2:	55                   	push   %ebp
  8008c3:	89 e5                	mov    %esp,%ebp
  8008c5:	53                   	push   %ebx
  8008c6:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
  8008c9:	89 c1                	mov    %eax,%ecx
  8008cb:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
  8008ce:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008d2:	eb 08                	jmp    8008dc <memfind+0x1a>
		if (*(const unsigned char *) s == (unsigned char) c)
  8008d4:	0f b6 10             	movzbl (%eax),%edx
  8008d7:	39 da                	cmp    %ebx,%edx
  8008d9:	74 05                	je     8008e0 <memfind+0x1e>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  8008db:	40                   	inc    %eax
  8008dc:	39 c8                	cmp    %ecx,%eax
  8008de:	72 f4                	jb     8008d4 <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
  8008e0:	5b                   	pop    %ebx
  8008e1:	5d                   	pop    %ebp
  8008e2:	c3                   	ret    

008008e3 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  8008e3:	55                   	push   %ebp
  8008e4:	89 e5                	mov    %esp,%ebp
  8008e6:	57                   	push   %edi
  8008e7:	56                   	push   %esi
  8008e8:	53                   	push   %ebx
  8008e9:	8b 4d 08             	mov    0x8(%ebp),%ecx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8008ec:	eb 01                	jmp    8008ef <strtol+0xc>
		s++;
  8008ee:	41                   	inc    %ecx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  8008ef:	8a 01                	mov    (%ecx),%al
  8008f1:	3c 20                	cmp    $0x20,%al
  8008f3:	74 f9                	je     8008ee <strtol+0xb>
  8008f5:	3c 09                	cmp    $0x9,%al
  8008f7:	74 f5                	je     8008ee <strtol+0xb>
		s++;

	// plus/minus sign
	if (*s == '+')
  8008f9:	3c 2b                	cmp    $0x2b,%al
  8008fb:	75 08                	jne    800905 <strtol+0x22>
		s++;
  8008fd:	41                   	inc    %ecx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  8008fe:	bf 00 00 00 00       	mov    $0x0,%edi
  800903:	eb 11                	jmp    800916 <strtol+0x33>
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
  800905:	3c 2d                	cmp    $0x2d,%al
  800907:	75 08                	jne    800911 <strtol+0x2e>
		s++, neg = 1;
  800909:	41                   	inc    %ecx
  80090a:	bf 01 00 00 00       	mov    $0x1,%edi
  80090f:	eb 05                	jmp    800916 <strtol+0x33>
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
  800911:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800916:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80091a:	0f 84 87 00 00 00    	je     8009a7 <strtol+0xc4>
  800920:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800924:	75 27                	jne    80094d <strtol+0x6a>
  800926:	80 39 30             	cmpb   $0x30,(%ecx)
  800929:	75 22                	jne    80094d <strtol+0x6a>
  80092b:	e9 88 00 00 00       	jmp    8009b8 <strtol+0xd5>
		s += 2, base = 16;
  800930:	83 c1 02             	add    $0x2,%ecx
  800933:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  80093a:	eb 11                	jmp    80094d <strtol+0x6a>
	else if (base == 0 && s[0] == '0')
		s++, base = 8;
  80093c:	41                   	inc    %ecx
  80093d:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800944:	eb 07                	jmp    80094d <strtol+0x6a>
	else if (base == 0)
		base = 10;
  800946:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)
  80094d:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800952:	8a 11                	mov    (%ecx),%dl
  800954:	8d 5a d0             	lea    -0x30(%edx),%ebx
  800957:	80 fb 09             	cmp    $0x9,%bl
  80095a:	77 08                	ja     800964 <strtol+0x81>
			dig = *s - '0';
  80095c:	0f be d2             	movsbl %dl,%edx
  80095f:	83 ea 30             	sub    $0x30,%edx
  800962:	eb 22                	jmp    800986 <strtol+0xa3>
		else if (*s >= 'a' && *s <= 'z')
  800964:	8d 72 9f             	lea    -0x61(%edx),%esi
  800967:	89 f3                	mov    %esi,%ebx
  800969:	80 fb 19             	cmp    $0x19,%bl
  80096c:	77 08                	ja     800976 <strtol+0x93>
			dig = *s - 'a' + 10;
  80096e:	0f be d2             	movsbl %dl,%edx
  800971:	83 ea 57             	sub    $0x57,%edx
  800974:	eb 10                	jmp    800986 <strtol+0xa3>
		else if (*s >= 'A' && *s <= 'Z')
  800976:	8d 72 bf             	lea    -0x41(%edx),%esi
  800979:	89 f3                	mov    %esi,%ebx
  80097b:	80 fb 19             	cmp    $0x19,%bl
  80097e:	77 14                	ja     800994 <strtol+0xb1>
			dig = *s - 'A' + 10;
  800980:	0f be d2             	movsbl %dl,%edx
  800983:	83 ea 37             	sub    $0x37,%edx
		else
			break;
		if (dig >= base)
  800986:	3b 55 10             	cmp    0x10(%ebp),%edx
  800989:	7d 09                	jge    800994 <strtol+0xb1>
			break;
		s++, val = (val * base) + dig;
  80098b:	41                   	inc    %ecx
  80098c:	0f af 45 10          	imul   0x10(%ebp),%eax
  800990:	01 d0                	add    %edx,%eax
		// we don't properly detect overflow!
	}
  800992:	eb be                	jmp    800952 <strtol+0x6f>

	if (endptr)
  800994:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800998:	74 05                	je     80099f <strtol+0xbc>
		*endptr = (char *) s;
  80099a:	8b 75 0c             	mov    0xc(%ebp),%esi
  80099d:	89 0e                	mov    %ecx,(%esi)
	return (neg ? -val : val);
  80099f:	85 ff                	test   %edi,%edi
  8009a1:	74 21                	je     8009c4 <strtol+0xe1>
  8009a3:	f7 d8                	neg    %eax
  8009a5:	eb 1d                	jmp    8009c4 <strtol+0xe1>
		s++;
	else if (*s == '-')
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  8009a7:	80 39 30             	cmpb   $0x30,(%ecx)
  8009aa:	75 9a                	jne    800946 <strtol+0x63>
  8009ac:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009b0:	0f 84 7a ff ff ff    	je     800930 <strtol+0x4d>
  8009b6:	eb 84                	jmp    80093c <strtol+0x59>
  8009b8:	80 79 01 78          	cmpb   $0x78,0x1(%ecx)
  8009bc:	0f 84 6e ff ff ff    	je     800930 <strtol+0x4d>
  8009c2:	eb 89                	jmp    80094d <strtol+0x6a>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
  8009c4:	5b                   	pop    %ebx
  8009c5:	5e                   	pop    %esi
  8009c6:	5f                   	pop    %edi
  8009c7:	5d                   	pop    %ebp
  8009c8:	c3                   	ret    

008009c9 <sys_cputs>:
	return ret;
}

void
sys_cputs(const char *s, size_t len)
{
  8009c9:	55                   	push   %ebp
  8009ca:	89 e5                	mov    %esp,%ebp
  8009cc:	57                   	push   %edi
  8009cd:	56                   	push   %esi
  8009ce:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8009cf:	b8 00 00 00 00       	mov    $0x0,%eax
  8009d4:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  8009d7:	8b 55 08             	mov    0x8(%ebp),%edx
  8009da:	89 c3                	mov    %eax,%ebx
  8009dc:	89 c7                	mov    %eax,%edi
  8009de:	89 c6                	mov    %eax,%esi
  8009e0:	cd 30                	int    $0x30

void
sys_cputs(const char *s, size_t len)
{
	syscall(SYS_cputs, 0, (uint32_t)s, len, 0, 0, 0);
}
  8009e2:	5b                   	pop    %ebx
  8009e3:	5e                   	pop    %esi
  8009e4:	5f                   	pop    %edi
  8009e5:	5d                   	pop    %ebp
  8009e6:	c3                   	ret    

008009e7 <sys_cgetc>:

int
sys_cgetc(void)
{
  8009e7:	55                   	push   %ebp
  8009e8:	89 e5                	mov    %esp,%ebp
  8009ea:	57                   	push   %edi
  8009eb:	56                   	push   %esi
  8009ec:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  8009ed:	ba 00 00 00 00       	mov    $0x0,%edx
  8009f2:	b8 01 00 00 00       	mov    $0x1,%eax
  8009f7:	89 d1                	mov    %edx,%ecx
  8009f9:	89 d3                	mov    %edx,%ebx
  8009fb:	89 d7                	mov    %edx,%edi
  8009fd:	89 d6                	mov    %edx,%esi
  8009ff:	cd 30                	int    $0x30

int
sys_cgetc(void)
{
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0, 0);
}
  800a01:	5b                   	pop    %ebx
  800a02:	5e                   	pop    %esi
  800a03:	5f                   	pop    %edi
  800a04:	5d                   	pop    %ebp
  800a05:	c3                   	ret    

00800a06 <sys_env_destroy>:

int
sys_env_destroy(envid_t envid)
{
  800a06:	55                   	push   %ebp
  800a07:	89 e5                	mov    %esp,%ebp
  800a09:	57                   	push   %edi
  800a0a:	56                   	push   %esi
  800a0b:	53                   	push   %ebx
  800a0c:	83 ec 0c             	sub    $0xc,%esp
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a0f:	b9 00 00 00 00       	mov    $0x0,%ecx
  800a14:	b8 03 00 00 00       	mov    $0x3,%eax
  800a19:	8b 55 08             	mov    0x8(%ebp),%edx
  800a1c:	89 cb                	mov    %ecx,%ebx
  800a1e:	89 cf                	mov    %ecx,%edi
  800a20:	89 ce                	mov    %ecx,%esi
  800a22:	cd 30                	int    $0x30
		       "b" (a3),
		       "D" (a4),
		       "S" (a5)
		     : "cc", "memory");

	if(check && ret > 0)
  800a24:	85 c0                	test   %eax,%eax
  800a26:	7e 17                	jle    800a3f <sys_env_destroy+0x39>
		panic("syscall %d returned %d (> 0)", num, ret);
  800a28:	83 ec 0c             	sub    $0xc,%esp
  800a2b:	50                   	push   %eax
  800a2c:	6a 03                	push   $0x3
  800a2e:	68 44 0f 80 00       	push   $0x800f44
  800a33:	6a 23                	push   $0x23
  800a35:	68 61 0f 80 00       	push   $0x800f61
  800a3a:	e8 27 00 00 00       	call   800a66 <_panic>

int
sys_env_destroy(envid_t envid)
{
	return syscall(SYS_env_destroy, 1, envid, 0, 0, 0, 0);
}
  800a3f:	8d 65 f4             	lea    -0xc(%ebp),%esp
  800a42:	5b                   	pop    %ebx
  800a43:	5e                   	pop    %esi
  800a44:	5f                   	pop    %edi
  800a45:	5d                   	pop    %ebp
  800a46:	c3                   	ret    

00800a47 <sys_getenvid>:

envid_t
sys_getenvid(void)
{
  800a47:	55                   	push   %ebp
  800a48:	89 e5                	mov    %esp,%ebp
  800a4a:	57                   	push   %edi
  800a4b:	56                   	push   %esi
  800a4c:	53                   	push   %ebx
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800a4d:	ba 00 00 00 00       	mov    $0x0,%edx
  800a52:	b8 02 00 00 00       	mov    $0x2,%eax
  800a57:	89 d1                	mov    %edx,%ecx
  800a59:	89 d3                	mov    %edx,%ebx
  800a5b:	89 d7                	mov    %edx,%edi
  800a5d:	89 d6                	mov    %edx,%esi
  800a5f:	cd 30                	int    $0x30

envid_t
sys_getenvid(void)
{
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0, 0);
}
  800a61:	5b                   	pop    %ebx
  800a62:	5e                   	pop    %esi
  800a63:	5f                   	pop    %edi
  800a64:	5d                   	pop    %ebp
  800a65:	c3                   	ret    

00800a66 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes JOS to enter the JOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
  800a66:	55                   	push   %ebp
  800a67:	89 e5                	mov    %esp,%ebp
  800a69:	56                   	push   %esi
  800a6a:	53                   	push   %ebx
	va_list ap;

	va_start(ap, fmt);
  800a6b:	8d 5d 14             	lea    0x14(%ebp),%ebx

	// Print the panic message
	cprintf("[%08x] user panic in %s at %s:%d: ",
  800a6e:	8b 35 00 10 80 00    	mov    0x801000,%esi
  800a74:	e8 ce ff ff ff       	call   800a47 <sys_getenvid>
  800a79:	83 ec 0c             	sub    $0xc,%esp
  800a7c:	ff 75 0c             	pushl  0xc(%ebp)
  800a7f:	ff 75 08             	pushl  0x8(%ebp)
  800a82:	56                   	push   %esi
  800a83:	50                   	push   %eax
  800a84:	68 70 0f 80 00       	push   $0x800f70
  800a89:	e8 af f6 ff ff       	call   80013d <cprintf>
		sys_getenvid(), binaryname, file, line);
	vcprintf(fmt, ap);
  800a8e:	83 c4 18             	add    $0x18,%esp
  800a91:	53                   	push   %ebx
  800a92:	ff 75 10             	pushl  0x10(%ebp)
  800a95:	e8 52 f6 ff ff       	call   8000ec <vcprintf>
	cprintf("\n");
  800a9a:	c7 04 24 94 0f 80 00 	movl   $0x800f94,(%esp)
  800aa1:	e8 97 f6 ff ff       	call   80013d <cprintf>
  800aa6:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  800aa9:	cc                   	int3   
  800aaa:	eb fd                	jmp    800aa9 <_panic+0x43>

00800aac <__udivdi3>:
  800aac:	55                   	push   %ebp
  800aad:	57                   	push   %edi
  800aae:	56                   	push   %esi
  800aaf:	53                   	push   %ebx
  800ab0:	83 ec 1c             	sub    $0x1c,%esp
  800ab3:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800ab7:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800abb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800abf:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800ac3:	89 ca                	mov    %ecx,%edx
  800ac5:	89 f8                	mov    %edi,%eax
  800ac7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800acb:	85 f6                	test   %esi,%esi
  800acd:	75 2d                	jne    800afc <__udivdi3+0x50>
  800acf:	39 cf                	cmp    %ecx,%edi
  800ad1:	77 65                	ja     800b38 <__udivdi3+0x8c>
  800ad3:	89 fd                	mov    %edi,%ebp
  800ad5:	85 ff                	test   %edi,%edi
  800ad7:	75 0b                	jne    800ae4 <__udivdi3+0x38>
  800ad9:	b8 01 00 00 00       	mov    $0x1,%eax
  800ade:	31 d2                	xor    %edx,%edx
  800ae0:	f7 f7                	div    %edi
  800ae2:	89 c5                	mov    %eax,%ebp
  800ae4:	31 d2                	xor    %edx,%edx
  800ae6:	89 c8                	mov    %ecx,%eax
  800ae8:	f7 f5                	div    %ebp
  800aea:	89 c1                	mov    %eax,%ecx
  800aec:	89 d8                	mov    %ebx,%eax
  800aee:	f7 f5                	div    %ebp
  800af0:	89 cf                	mov    %ecx,%edi
  800af2:	89 fa                	mov    %edi,%edx
  800af4:	83 c4 1c             	add    $0x1c,%esp
  800af7:	5b                   	pop    %ebx
  800af8:	5e                   	pop    %esi
  800af9:	5f                   	pop    %edi
  800afa:	5d                   	pop    %ebp
  800afb:	c3                   	ret    
  800afc:	39 ce                	cmp    %ecx,%esi
  800afe:	77 28                	ja     800b28 <__udivdi3+0x7c>
  800b00:	0f bd fe             	bsr    %esi,%edi
  800b03:	83 f7 1f             	xor    $0x1f,%edi
  800b06:	75 40                	jne    800b48 <__udivdi3+0x9c>
  800b08:	39 ce                	cmp    %ecx,%esi
  800b0a:	72 0a                	jb     800b16 <__udivdi3+0x6a>
  800b0c:	3b 44 24 08          	cmp    0x8(%esp),%eax
  800b10:	0f 87 9e 00 00 00    	ja     800bb4 <__udivdi3+0x108>
  800b16:	b8 01 00 00 00       	mov    $0x1,%eax
  800b1b:	89 fa                	mov    %edi,%edx
  800b1d:	83 c4 1c             	add    $0x1c,%esp
  800b20:	5b                   	pop    %ebx
  800b21:	5e                   	pop    %esi
  800b22:	5f                   	pop    %edi
  800b23:	5d                   	pop    %ebp
  800b24:	c3                   	ret    
  800b25:	8d 76 00             	lea    0x0(%esi),%esi
  800b28:	31 ff                	xor    %edi,%edi
  800b2a:	31 c0                	xor    %eax,%eax
  800b2c:	89 fa                	mov    %edi,%edx
  800b2e:	83 c4 1c             	add    $0x1c,%esp
  800b31:	5b                   	pop    %ebx
  800b32:	5e                   	pop    %esi
  800b33:	5f                   	pop    %edi
  800b34:	5d                   	pop    %ebp
  800b35:	c3                   	ret    
  800b36:	66 90                	xchg   %ax,%ax
  800b38:	89 d8                	mov    %ebx,%eax
  800b3a:	f7 f7                	div    %edi
  800b3c:	31 ff                	xor    %edi,%edi
  800b3e:	89 fa                	mov    %edi,%edx
  800b40:	83 c4 1c             	add    $0x1c,%esp
  800b43:	5b                   	pop    %ebx
  800b44:	5e                   	pop    %esi
  800b45:	5f                   	pop    %edi
  800b46:	5d                   	pop    %ebp
  800b47:	c3                   	ret    
  800b48:	bd 20 00 00 00       	mov    $0x20,%ebp
  800b4d:	89 eb                	mov    %ebp,%ebx
  800b4f:	29 fb                	sub    %edi,%ebx
  800b51:	89 f9                	mov    %edi,%ecx
  800b53:	d3 e6                	shl    %cl,%esi
  800b55:	89 c5                	mov    %eax,%ebp
  800b57:	88 d9                	mov    %bl,%cl
  800b59:	d3 ed                	shr    %cl,%ebp
  800b5b:	89 e9                	mov    %ebp,%ecx
  800b5d:	09 f1                	or     %esi,%ecx
  800b5f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  800b63:	89 f9                	mov    %edi,%ecx
  800b65:	d3 e0                	shl    %cl,%eax
  800b67:	89 c5                	mov    %eax,%ebp
  800b69:	89 d6                	mov    %edx,%esi
  800b6b:	88 d9                	mov    %bl,%cl
  800b6d:	d3 ee                	shr    %cl,%esi
  800b6f:	89 f9                	mov    %edi,%ecx
  800b71:	d3 e2                	shl    %cl,%edx
  800b73:	8b 44 24 08          	mov    0x8(%esp),%eax
  800b77:	88 d9                	mov    %bl,%cl
  800b79:	d3 e8                	shr    %cl,%eax
  800b7b:	09 c2                	or     %eax,%edx
  800b7d:	89 d0                	mov    %edx,%eax
  800b7f:	89 f2                	mov    %esi,%edx
  800b81:	f7 74 24 0c          	divl   0xc(%esp)
  800b85:	89 d6                	mov    %edx,%esi
  800b87:	89 c3                	mov    %eax,%ebx
  800b89:	f7 e5                	mul    %ebp
  800b8b:	39 d6                	cmp    %edx,%esi
  800b8d:	72 19                	jb     800ba8 <__udivdi3+0xfc>
  800b8f:	74 0b                	je     800b9c <__udivdi3+0xf0>
  800b91:	89 d8                	mov    %ebx,%eax
  800b93:	31 ff                	xor    %edi,%edi
  800b95:	e9 58 ff ff ff       	jmp    800af2 <__udivdi3+0x46>
  800b9a:	66 90                	xchg   %ax,%ax
  800b9c:	8b 54 24 08          	mov    0x8(%esp),%edx
  800ba0:	89 f9                	mov    %edi,%ecx
  800ba2:	d3 e2                	shl    %cl,%edx
  800ba4:	39 c2                	cmp    %eax,%edx
  800ba6:	73 e9                	jae    800b91 <__udivdi3+0xe5>
  800ba8:	8d 43 ff             	lea    -0x1(%ebx),%eax
  800bab:	31 ff                	xor    %edi,%edi
  800bad:	e9 40 ff ff ff       	jmp    800af2 <__udivdi3+0x46>
  800bb2:	66 90                	xchg   %ax,%ax
  800bb4:	31 c0                	xor    %eax,%eax
  800bb6:	e9 37 ff ff ff       	jmp    800af2 <__udivdi3+0x46>
  800bbb:	90                   	nop

00800bbc <__umoddi3>:
  800bbc:	55                   	push   %ebp
  800bbd:	57                   	push   %edi
  800bbe:	56                   	push   %esi
  800bbf:	53                   	push   %ebx
  800bc0:	83 ec 1c             	sub    $0x1c,%esp
  800bc3:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  800bc7:	8b 74 24 34          	mov    0x34(%esp),%esi
  800bcb:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800bcf:	8b 44 24 3c          	mov    0x3c(%esp),%eax
  800bd3:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800bd7:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800bdb:	89 f3                	mov    %esi,%ebx
  800bdd:	89 fa                	mov    %edi,%edx
  800bdf:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800be3:	89 34 24             	mov    %esi,(%esp)
  800be6:	85 c0                	test   %eax,%eax
  800be8:	75 1a                	jne    800c04 <__umoddi3+0x48>
  800bea:	39 f7                	cmp    %esi,%edi
  800bec:	0f 86 a2 00 00 00    	jbe    800c94 <__umoddi3+0xd8>
  800bf2:	89 c8                	mov    %ecx,%eax
  800bf4:	89 f2                	mov    %esi,%edx
  800bf6:	f7 f7                	div    %edi
  800bf8:	89 d0                	mov    %edx,%eax
  800bfa:	31 d2                	xor    %edx,%edx
  800bfc:	83 c4 1c             	add    $0x1c,%esp
  800bff:	5b                   	pop    %ebx
  800c00:	5e                   	pop    %esi
  800c01:	5f                   	pop    %edi
  800c02:	5d                   	pop    %ebp
  800c03:	c3                   	ret    
  800c04:	39 f0                	cmp    %esi,%eax
  800c06:	0f 87 ac 00 00 00    	ja     800cb8 <__umoddi3+0xfc>
  800c0c:	0f bd e8             	bsr    %eax,%ebp
  800c0f:	83 f5 1f             	xor    $0x1f,%ebp
  800c12:	0f 84 ac 00 00 00    	je     800cc4 <__umoddi3+0x108>
  800c18:	bf 20 00 00 00       	mov    $0x20,%edi
  800c1d:	29 ef                	sub    %ebp,%edi
  800c1f:	89 fe                	mov    %edi,%esi
  800c21:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  800c25:	89 e9                	mov    %ebp,%ecx
  800c27:	d3 e0                	shl    %cl,%eax
  800c29:	89 d7                	mov    %edx,%edi
  800c2b:	89 f1                	mov    %esi,%ecx
  800c2d:	d3 ef                	shr    %cl,%edi
  800c2f:	09 c7                	or     %eax,%edi
  800c31:	89 e9                	mov    %ebp,%ecx
  800c33:	d3 e2                	shl    %cl,%edx
  800c35:	89 14 24             	mov    %edx,(%esp)
  800c38:	89 d8                	mov    %ebx,%eax
  800c3a:	d3 e0                	shl    %cl,%eax
  800c3c:	89 c2                	mov    %eax,%edx
  800c3e:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c42:	d3 e0                	shl    %cl,%eax
  800c44:	89 44 24 04          	mov    %eax,0x4(%esp)
  800c48:	8b 44 24 08          	mov    0x8(%esp),%eax
  800c4c:	89 f1                	mov    %esi,%ecx
  800c4e:	d3 e8                	shr    %cl,%eax
  800c50:	09 d0                	or     %edx,%eax
  800c52:	d3 eb                	shr    %cl,%ebx
  800c54:	89 da                	mov    %ebx,%edx
  800c56:	f7 f7                	div    %edi
  800c58:	89 d3                	mov    %edx,%ebx
  800c5a:	f7 24 24             	mull   (%esp)
  800c5d:	89 c6                	mov    %eax,%esi
  800c5f:	89 d1                	mov    %edx,%ecx
  800c61:	39 d3                	cmp    %edx,%ebx
  800c63:	0f 82 87 00 00 00    	jb     800cf0 <__umoddi3+0x134>
  800c69:	0f 84 91 00 00 00    	je     800d00 <__umoddi3+0x144>
  800c6f:	8b 54 24 04          	mov    0x4(%esp),%edx
  800c73:	29 f2                	sub    %esi,%edx
  800c75:	19 cb                	sbb    %ecx,%ebx
  800c77:	89 d8                	mov    %ebx,%eax
  800c79:	8a 4c 24 0c          	mov    0xc(%esp),%cl
  800c7d:	d3 e0                	shl    %cl,%eax
  800c7f:	89 e9                	mov    %ebp,%ecx
  800c81:	d3 ea                	shr    %cl,%edx
  800c83:	09 d0                	or     %edx,%eax
  800c85:	89 e9                	mov    %ebp,%ecx
  800c87:	d3 eb                	shr    %cl,%ebx
  800c89:	89 da                	mov    %ebx,%edx
  800c8b:	83 c4 1c             	add    $0x1c,%esp
  800c8e:	5b                   	pop    %ebx
  800c8f:	5e                   	pop    %esi
  800c90:	5f                   	pop    %edi
  800c91:	5d                   	pop    %ebp
  800c92:	c3                   	ret    
  800c93:	90                   	nop
  800c94:	89 fd                	mov    %edi,%ebp
  800c96:	85 ff                	test   %edi,%edi
  800c98:	75 0b                	jne    800ca5 <__umoddi3+0xe9>
  800c9a:	b8 01 00 00 00       	mov    $0x1,%eax
  800c9f:	31 d2                	xor    %edx,%edx
  800ca1:	f7 f7                	div    %edi
  800ca3:	89 c5                	mov    %eax,%ebp
  800ca5:	89 f0                	mov    %esi,%eax
  800ca7:	31 d2                	xor    %edx,%edx
  800ca9:	f7 f5                	div    %ebp
  800cab:	89 c8                	mov    %ecx,%eax
  800cad:	f7 f5                	div    %ebp
  800caf:	89 d0                	mov    %edx,%eax
  800cb1:	e9 44 ff ff ff       	jmp    800bfa <__umoddi3+0x3e>
  800cb6:	66 90                	xchg   %ax,%ax
  800cb8:	89 c8                	mov    %ecx,%eax
  800cba:	89 f2                	mov    %esi,%edx
  800cbc:	83 c4 1c             	add    $0x1c,%esp
  800cbf:	5b                   	pop    %ebx
  800cc0:	5e                   	pop    %esi
  800cc1:	5f                   	pop    %edi
  800cc2:	5d                   	pop    %ebp
  800cc3:	c3                   	ret    
  800cc4:	3b 04 24             	cmp    (%esp),%eax
  800cc7:	72 06                	jb     800ccf <__umoddi3+0x113>
  800cc9:	3b 7c 24 04          	cmp    0x4(%esp),%edi
  800ccd:	77 0f                	ja     800cde <__umoddi3+0x122>
  800ccf:	89 f2                	mov    %esi,%edx
  800cd1:	29 f9                	sub    %edi,%ecx
  800cd3:	1b 54 24 0c          	sbb    0xc(%esp),%edx
  800cd7:	89 14 24             	mov    %edx,(%esp)
  800cda:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  800cde:	8b 44 24 04          	mov    0x4(%esp),%eax
  800ce2:	8b 14 24             	mov    (%esp),%edx
  800ce5:	83 c4 1c             	add    $0x1c,%esp
  800ce8:	5b                   	pop    %ebx
  800ce9:	5e                   	pop    %esi
  800cea:	5f                   	pop    %edi
  800ceb:	5d                   	pop    %ebp
  800cec:	c3                   	ret    
  800ced:	8d 76 00             	lea    0x0(%esi),%esi
  800cf0:	2b 04 24             	sub    (%esp),%eax
  800cf3:	19 fa                	sbb    %edi,%edx
  800cf5:	89 d1                	mov    %edx,%ecx
  800cf7:	89 c6                	mov    %eax,%esi
  800cf9:	e9 71 ff ff ff       	jmp    800c6f <__umoddi3+0xb3>
  800cfe:	66 90                	xchg   %ax,%ax
  800d00:	39 44 24 04          	cmp    %eax,0x4(%esp)
  800d04:	72 ea                	jb     800cf0 <__umoddi3+0x134>
  800d06:	89 d9                	mov    %ebx,%ecx
  800d08:	e9 62 ff ff ff       	jmp    800c6f <__umoddi3+0xb3>
